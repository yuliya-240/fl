-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Ноя 09 2017 г., 09:18
-- Версия сервера: 5.6.38
-- Версия PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `floctopu_main`
--

-- --------------------------------------------------------

--
-- Структура таблицы `antispam`
--

CREATE TABLE `antispam` (
  `as_id` int(11) UNSIGNED NOT NULL,
  `as_user_id` int(11) NOT NULL DEFAULT '0',
  `as_object` varchar(100) NOT NULL DEFAULT '0',
  `as_object_id` int(11) NOT NULL DEFAULT '0',
  `as_email` varchar(200) DEFAULT NULL,
  `as_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `antispam`
--

INSERT INTO `antispam` (`as_id`, `as_user_id`, `as_object`, `as_object_id`, `as_email`, `as_date`) VALUES
(1, 3, 'estimate', 1, 'ugoloto@gmail.com', '2017-10-03 17:50:56'),
(2, 3, 'estimate', 2, 'zavhozmain@gmail.com', '2017-10-03 17:56:08'),
(3, 3, 'estimate', 2, 'zavhozmain@gmail.com', '2017-10-03 17:56:08'),
(4, 3, 'invoice', 1, 'zavhozmain@gmail.com', '2017-10-03 18:00:23'),
(5, 3, 'invoice', 1, 'zavhozmain@gmail.com', '2017-10-03 18:00:23'),
(6, 3, 'invoice', 2, 'zavhozmain@gmail.com', '2017-10-03 18:47:54'),
(7, 16, 'invoice', 6, 'pr.idea.pr@gmail.com', '2017-10-31 07:45:37');

-- --------------------------------------------------------

--
-- Структура таблицы `chats`
--

CREATE TABLE `chats` (
  `chat_id` int(11) UNSIGNED NOT NULL,
  `chat_from` varchar(200) DEFAULT '',
  `chat_to` varchar(200) DEFAULT '',
  `chat_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `chat_msg` text,
  `chat_status` tinyint(1) NOT NULL DEFAULT '0',
  `chat_attach` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `chats`
--

INSERT INTO `chats` (`chat_id`, `chat_from`, `chat_to`, `chat_date`, `chat_msg`, `chat_status`, `chat_attach`) VALUES
(1, 'private-59c39df4158ec', 'floctoid', '2017-09-21 11:11:06', 'hello', 0, NULL),
(2, 'floctoid', 'private-59c39df4158ec', '2017-09-21 11:11:06', 'You said: \'hello\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(3, 'private-59c9ed334c09f', 'floctoid', '2017-10-03 16:29:17', 'привет', 0, NULL),
(4, 'floctoid', 'private-59c9ed334c09f', '2017-10-03 16:29:17', 'You said: \'привет\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(5, 'private-59c9ed334c09f', 'floctoid', '2017-10-03 16:29:35', 'Hello', 0, NULL),
(6, 'floctoid', 'private-59c9ed334c09f', '2017-10-03 16:29:35', 'You said: \'Hello\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(7, 'private-59c9ed334c09f', '', '2017-10-03 18:54:13', 'ррпороложє', 0, NULL),
(8, 'private-59c9ed334c09f', '', '2017-10-03 18:54:26', 'ллдлллдллдлддл', 0, NULL),
(9, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-12 10:33:22', 'hello !', 1, NULL),
(10, 'private-59c9ed334c09f', '', '2017-10-12 10:33:29', 'ghghhjhj', 0, NULL),
(11, 'private-59c3a3ba46526', '', '2017-10-12 13:23:43', 'dssds', 0, NULL),
(12, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-15 08:14:39', '<img class=\"img-responsive\" src=\"/chat/pic_o_1bsfhijof2uknjr1kdp1jvcd1bd.png\">', 1, NULL),
(13, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-15 08:15:50', 'мне пришло три внетренних уведомления о том что меня добавили в проект - это ты тестировал и три раза меня добавлял или это баг?', 1, NULL),
(14, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-15 13:12:11', 'Да то я тестил', 1, NULL),
(15, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-18 11:20:37', 'ты тут?', 1, NULL),
(16, 'private-59c3a3ba46526', 'private-59e74a5cce2b8', '2017-10-18 12:40:07', 'bhey hey', 1, NULL),
(17, 'private-59e74a5cce2b8', 'private-59c3a3ba46526', '2017-10-18 12:40:27', 'привет', 1, NULL),
(18, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-18 12:52:00', 'здесь я', 1, NULL),
(19, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-19 09:51:57', 'проверь на secure.floctopus.info вроде слели и залил', 1, NULL),
(20, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-19 10:19:21', 'когда отправляешь приглашение пользователю может тоже нужно сделать возможность просмотра профиля', 1, NULL),
(21, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-19 10:22:53', '<img class=\"img-responsive\" src=\"/chat/pic_o_1bsq2gcru149k60m1mal5kdul88.png\">', 1, NULL),
(22, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-19 10:34:21', 'да, я уже добавил это в таск', 1, NULL),
(23, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-19 10:34:40', 'ок', 1, NULL),
(24, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-19 10:35:03', 'Множественное удаление портфолио работает отлично', 1, NULL),
(25, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-19 10:35:19', 'Сделай такое же для заметок плиз', 1, NULL),
(26, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-19 10:35:37', 'хорошо)))', 1, NULL),
(27, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-19 10:36:01', 'а чего у нас в месседжере нет смайликов)))', 1, NULL),
(28, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-19 10:36:15', 'не все сразу ! )', 1, NULL),
(29, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-19 10:37:21', 'я сейчас пробегаюсь по языковым файлам, потом сделаю заметки и напишу тебе', 1, NULL),
(30, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-19 10:38:34', 'ок', 1, NULL),
(31, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-19 14:34:54', 'всё скинула в свою ветку', 1, NULL),
(32, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-20 10:09:43', 'я слила твои изменения себе в ветку пошли конфликты по языковым файлам, я их исправила', 1, NULL),
(33, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:09:54', 'да', 1, NULL),
(34, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:10:02', 'есть такое дело', 1, NULL),
(35, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:10:19', 'я исправлю', 1, NULL),
(36, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-20 10:10:32', 'я уже исправила', 1, NULL),
(37, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:10:43', 'ай молодца !!!', 1, NULL),
(38, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-20 10:14:53', 'немного изменила заметки, что дальше делать?', 1, NULL),
(39, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:18:12', 'у тебя фейсбук эккаунт есть?', 1, NULL),
(40, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:25:28', '<img class=\"img-responsive\" src=\"/chat/pic_o_1bssl1r5r1p661hdkk11cqb4n3d.png\">', 1, NULL),
(41, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:26:19', 'поля теже', 1, NULL),
(42, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:26:46', 'Даты с по на разных строчках', 1, NULL),
(43, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:28:01', 'и там где дата ДО - надо что бы был чекбокс, если чекбокс отмечен то показывает \"I currently work here\"', 1, NULL),
(44, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:28:16', 'если чекбокс снят, то появляется выбор даты', 1, NULL),
(45, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:28:58', 'и вместо City / Town надо Location', 1, NULL),
(46, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:29:37', 'я имею ввиду название \"City / Town\" надо поменять на \"Location\"', 1, NULL),
(47, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-20 10:39:32', 'а вкладку паблик тоже делать? там указывается кому его показывать', 1, NULL),
(48, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:40:06', 'да, сделай - переключатель on/off', 1, NULL),
(49, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-20 10:40:22', 'ок', 1, NULL),
(50, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-20 10:41:55', 'я еще гляну на сайтах для поиска работы, ворк юа и работа юа, как там сделано', 1, NULL),
(51, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-20 10:43:04', 'да', 1, NULL),
(52, 'private-59c3a3ba46526', 'floctoid', '2017-10-20 12:49:16', 'hello', 0, NULL),
(53, 'floctoid', 'private-59c3a3ba46526', '2017-10-20 12:49:16', 'You said: \'hello\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(54, 'private-59e74a5cce2b8', 'private-59c3a3ba46526', '2017-10-20 13:01:59', 'Валер, привет, я тебе на почту ivafirst@gmail.com  переслала отклик на вакансию, резюме. посмотри, пришло ли', 1, NULL),
(55, 'private-59e74a5cce2b8', 'private-59c3a3ba46526', '2017-10-20 13:02:32', 'а можно выставить, чтобы сообщений отправлялись в этом чате клавишей Ентер, а не наживать курсоро - \"отправить?\"', 1, NULL),
(56, 'private-59c3a3ba46526', 'private-59e74a5cce2b8', '2017-10-20 13:14:29', 'можно', 1, NULL),
(57, 'private-59e74a5cce2b8', 'private-59c3a3ba46526', '2017-10-20 13:21:25', 'а посмотри, письмо доходит? или в спам попадает?', 1, NULL),
(58, 'private-59e74a5cce2b8', 'private-59c3a3ba46526', '2017-10-20 13:28:17', 'не дает возможности поставить как задачу - пишу сюда: не могу тебе вордовский файл переслать через чат этот (((( видит  в прикреплениях только картинки. ПОэтому перешлю еще одно резюме на ту же почту', 1, NULL),
(59, 'private-59c3a3ba46526', 'private-59e9ef9538352', '2017-10-20 13:30:31', 'hello!', 1, NULL),
(60, 'private-59e9ef9538352', 'private-59c3a3ba46526', '2017-10-20 13:35:22', 'приветик', 1, NULL),
(61, 'private-59e9ef9538352', 'private-59e74a5cce2b8', '2017-10-20 13:35:41', 'приветик', 1, NULL),
(62, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:37:39', '))) привет!', 1, NULL),
(63, 'private-59e9ef9538352', 'private-59e74a5cce2b8', '2017-10-20 13:38:46', '<img class=\"img-responsive\" src=\"/chat/pic_o_1bst03lq459jqcu1ruj85619p9d.jpg\">', 1, NULL),
(64, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:39:06', 'СЕгодня уже подключили объявлений на всех ресурсах, они видны', 1, NULL),
(65, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:39:10', 'вот отчет', 1, NULL),
(66, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:39:14', 'Размещения объявлений: \nHh\n<a target=\"_blank\" href=\"https://hh.ua/vacancy/23180631\">https://hh.ua/vacancy/23180631</a>  -об 1\n<a target=\"_blank\" href=\"https://hh.ua/vacancy/23180710?message=add_change\">https://hh.ua/vacancy/23180710?message=add_change</a>  - об 2\nRabota Ua\n<a target=\"_blank\" href=\"https://rabota.ua/company3340483/vacancy6945474\">https://rabota.ua/company3340483/vacancy6945474</a>  - об 1\n<a target=\"_blank\" href=\"https://rabota.ua/company3340483/vacancy6945479\">https://rabota.ua/company3340483/vacancy6945479</a>  - об 2\nНова Работа\n<a target=\"_blank\" href=\"http://novarobota.ua/ru/profail_kompanii/moi_vakansii.htm\">http://novarobota.ua/ru/profail_kompanii/moi_vakansii.htm</a>  - об 1,2\nWork.ua\n<a target=\"_blank\" href=\"https://www.work.ua/jobs/2917400/\">https://www.work.ua/jobs/2917400/</a>  - об 1\n<a target=\"_blank\" href=\"https://www.work.ua/employer/my/jobs/view/2917406/\">https://www.work.ua/employer/my/jobs/view/2917406/</a>  - об 2\nSlando\n<a target=\"_blank\" href=\"https://www.olx.ua/obyavlenie/na-postoyannuyu-zanyatost-trebuetsya-junior-web-full-stack-developer-IDwz0Di.html\">https://www.olx.ua/obyavlenie/na-postoyannuyu-zanyatost-trebuetsya-junior-web-full-stack-developer-IDwz0Di.html</a>  - об 1\n<a target=\"_blank\" href=\"https://www.olx.ua/obyavlenie/ischem-razrabotchika-po-mobilnym-ustroystvam-junior-ios-developer-IDwz412.html\">https://www.olx.ua/obyavlenie/ischem-razrabotchika-po-mobilnym-ustroystvam-junior-ios-developer-IDwz412.html</a>  - об 2', 1, NULL),
(67, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:39:29', 'ой как плохо тут отображается((((', 1, NULL),
(68, 'private-59e9ef9538352', 'private-59e74a5cce2b8', '2017-10-20 13:39:35', 'класс! пасибо', 1, NULL),
(69, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:39:39', '<img class=\"img-responsive\" src=\"/chat/pic_o_1bst05hfeu08uuc8o8vsj1r5jd.png\">', 1, NULL),
(70, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:39:46', '<img class=\"img-responsive\" src=\"/chat/pic_o_1bst05ob7v6p9vjvf1j6sms8i.png\">', 1, NULL),
(71, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:40:00', '<a target=\"_blank\" href=\"https://www.facebook.com/groups/kyiv.trud/?ref=br_rs\">https://www.facebook.com/groups/kyiv.trud/?ref=br_rs</a>', 1, NULL),
(72, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:40:49', 'в Фб пока только в 1 группе добавила, там в большинстве надо ждать, пока администратор тебя добавит в группу. как только меня активируют, добавлю в остальные\nВалдере уже 2 резюме отправила - пусть смотрит', 1, NULL),
(73, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-20 13:41:40', 'Я пытаюсь поставить задачу во FL - не могу - не работает ниче пока(((((\nМОж у вас получится?', 1, NULL),
(74, 'private-59e9ef9538352', 'private-59e74a5cce2b8', '2017-10-20 13:43:03', 'пока не работает. Все в процессе.\nпока такой момент. Стабильные версия обновляется примерно в 20 ежедневно', 1, NULL),
(75, 'private-59e9ef9538352', 'private-59e74a5cce2b8', '2017-10-20 13:43:51', 'по крайем мере понятно- что мы маемо', 1, NULL),
(76, 'private-59e9ef9538352', 'private-59e74a5cce2b8', '2017-10-20 13:44:27', 'я 3 раза создавал проект и включал тебя. По-моему только 1 раз он сохранился', 1, NULL),
(77, 'private-59e74a5cce2b8', 'floctoid', '2017-10-20 15:26:09', 'привет', 0, NULL),
(78, 'floctoid', 'private-59e74a5cce2b8', '2017-10-20 15:26:09', 'You said: \'привет\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(79, 'private-59e74a5cce2b8', 'floctoid', '2017-10-20 15:26:24', 'hi', 0, NULL),
(80, 'floctoid', 'private-59e74a5cce2b8', '2017-10-20 15:26:24', 'You said: \'hi\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(81, 'private-59c3a3ba46526', 'floctoid', '2017-10-21 03:02:34', 'adadad', 0, NULL),
(82, 'floctoid', 'private-59c3a3ba46526', '2017-10-21 03:02:34', 'You said: \'adadad\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(83, 'private-59c3a3ba46526', 'floctoid', '2017-10-21 03:02:42', 'adadaddadad adadad adadad adad', 0, NULL),
(84, 'floctoid', 'private-59c3a3ba46526', '2017-10-21 03:02:42', 'You said: \'adadaddadad adadad adadad adad\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(85, 'private-59c3a3ba46526', 'floctoid', '2017-10-21 03:02:43', 'adada', 0, NULL),
(86, 'floctoid', 'private-59c3a3ba46526', '2017-10-21 03:02:43', 'You said: \'adada\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(87, 'private-59c3a3ba46526', 'floctoid', '2017-10-21 03:02:52', 'dadad', 0, NULL),
(88, 'floctoid', 'private-59c3a3ba46526', '2017-10-21 03:02:52', 'You said: \'dadad\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(89, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-21 06:47:08', '-- phpMyAdmin SQL Dump\n-- version 4.5.4.1deb2ubuntu2\n-- <a target=\"_blank\" href=\"http://www.phpmyadmin.net\">http://www.phpmyadmin.net</a>\n--\n-- Хост: localhost\n-- Время создания: Окт 21 2017 г., 09:43\n-- Версия сервера: 5.7.19-0ubuntu0.16.04.1\n-- Версия PHP: 7.0.22-0ubuntu0.16.04.1\n\nSET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";\nSET time_zone = \"+00:00\";\n\n\n/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\n/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\n/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\n/*!40101 SET NAMES utf8mb4 */;\n\n--\n-- База данных: `floctopus`\n--\n\n-- --------------------------------------------------------\n\n--\n-- Структура таблицы `user_work`\n--\n\nCREATE TABLE `user_work` (\n  `work_id` int(11) NOT NULL,\n  `work_user_id` int(11) NOT NULL DEFAULT \'0\',\n  `work_company` varchar(255) DEFAULT NULL,\n  `work_position` varchar(255) DEFAULT NULL,\n  `work_location` varchar(255) DEFAULT NULL,\n  `work_desc` text,\n  `work_year_from` int(4) NOT NULL DEFAULT \'0\',\n  `work_year_to` int(4) NOT NULL DEFAULT \'0\',\n  `work_month_from` varchar(100) DEFAULT \'0\',\n  `work_month_to` varchar(100) DEFAULT NULL,\n  `work_trash` int(11) NOT NULL DEFAULT \'0\'\n) ENGINE=InnoDB DEFAULT CHARSET=utf8;\n\n--\n-- Дамп данных таблицы `user_work`\n--\n\nINSERT INTO `user_work` (`work_id`, `work_user_id`, `work_company`, `work_position`, `work_location`, `work_desc`, `work_year_from`, `work_year_to`, `work_month_from`, `work_month_to`, `work_trash`) VALUES\n(1, 3, \'Floctopus\', \'Web developer\', \'Sumy\', \'I am Cool\', 2017, 2017, \'Июнь\', \'Ноябрь\', 1),\n(2, 3, \'first and best company for freelanser Floctopus\', \'Web developer\', \'Sumy\', \'I am Cool\', 2017, 2017, \'Январь\', \'Январь\', 1),\n(3, 3, \'first and best company for freelanser Floctopus\', \'Web developer\', \'Sumy\', \'I am Cool\', 2016, 2017, \'Январь\', \'Январь\', 0);\n\n--\n-- Индексы сохранённых таблиц\n--\n\n--\n-- Индексы таблицы `user_work`\n--\nALTER TABLE `user_work`\n  ADD PRIMARY KEY (`work_id`);\n\n--\n-- AUTO_INCREMENT для сохранённых таблиц\n--\n\n--\n-- AUTO_INCREMENT для таблицы `user_work`\n--\nALTER TABLE `user_work`\n  MODIFY `work_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;\n/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\n/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\n/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;', 1, NULL),
(90, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-21 08:55:48', 'я тебе документ с таблицей расшарила', 1, NULL),
(91, 'private-59c9ed334c09f', 'private-59c3a3ba46526', '2017-10-21 09:27:46', 'Не работало удаление нескольких клиентов я поправила и закинула изменения в свою ветку', 1, NULL),
(92, 'private-59c3a3ba46526', 'private-59c9ed334c09f', '2017-10-22 20:45:41', 'ok', 1, NULL),
(93, 'private-59ed96f6865cf', 'private-59e74a5cce2b8', '2017-10-23 08:47:13', 'Детка, жду инфо )', 1, NULL),
(94, 'private-59e74a5cce2b8', 'private-59ed96f6865cf', '2017-10-23 09:24:06', 'привет, а в задачах не видно тебе?', 1, NULL),
(95, 'private-59e74a5cce2b8', 'private-59ed96f6865cf', '2017-10-23 10:18:53', 'ау, скажи шо нить', 1, NULL),
(96, 'private-59e74a5cce2b8', 'private-59e9ef9538352', '2017-10-23 10:19:07', 'добрый день, вы тут?', 1, NULL),
(97, 'private-59ed96f6865cf', 'private-59e74a5cce2b8', '2017-10-23 10:48:55', 'выходила на обед', 1, NULL),
(98, 'private-59ed96f6865cf', 'private-59e74a5cce2b8', '2017-10-23 10:48:58', 'ничего ет', 1, NULL),
(99, 'private-59e74a5cce2b8', 'private-59ed96f6865cf', '2017-10-23 10:49:15', 'зайди в проекты - задачи', 1, NULL),
(100, 'private-59e74a5cce2b8', 'private-59ed96f6865cf', '2017-10-23 10:50:19', 'блин, а тебя нет в списке выпадающих...хмм...ладно кину сюда', 1, NULL),
(101, 'private-59e74a5cce2b8', 'private-59ed96f6865cf', '2017-10-23 10:50:50', 'не, дай свой скайп', 1, NULL),
(102, 'private-59e74a5cce2b8', 'private-59ed96f6865cf', '2017-10-23 10:51:07', 'какая у тебя почта', 1, NULL),
(103, 'private-59ed96f6865cf', 'private-59e74a5cce2b8', '2017-10-23 10:57:48', 'kramar.anna@alliancepack.com.ua', 1, NULL),
(104, 'private-59ed96f6865cf', 'private-59e74a5cce2b8', '2017-10-23 10:58:02', 'skype: kramar.anna', 1, NULL),
(105, 'private-59e74a5cce2b8', 'private-59ed96f6865cf', '2017-10-23 11:00:01', 'отправила на почту', 1, NULL),
(106, 'private-59e9ef9538352', 'private-59e74a5cce2b8', '2017-10-24 13:05:00', 'привет', 1, NULL),
(107, 'private-59f10573a3585', 'private-59c3a3ba46526', '2017-10-25 22:11:02', 'Валера, привет!', 1, NULL),
(108, 'private-59f10573a3585', 'private-59c3a3ba46526', '2017-10-25 22:11:25', 'Юля, привет!', 1, NULL),
(109, 'private-59c3a3ba46526', 'private-59f10573a3585', '2017-10-26 08:25:32', 'Всем привет', 1, NULL),
(110, 'private-59f10573a3585', 'private-59c3a3ba46526', '2017-10-26 08:35:48', 'ура!', 1, NULL),
(111, 'private-59f19c663eb8d', 'floctoid', '2017-10-26 08:36:30', 'привет', 0, NULL),
(112, 'floctoid', 'private-59f19c663eb8d', '2017-10-26 08:36:30', 'You said: \'привет\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(113, 'private-59f19c663eb8d', 'floctoid', '2017-10-26 08:36:40', 'Hi there', 0, NULL),
(114, 'floctoid', 'private-59f19c663eb8d', '2017-10-26 08:36:40', 'You said: \'Hi there\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(115, 'private-59f19c663eb8d', 'floctoid', '2017-10-26 08:37:02', 'When your brains will be updated?', 0, NULL),
(116, 'floctoid', 'private-59f19c663eb8d', '2017-10-26 08:37:02', 'You said: \'When your brains will be updated?\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(117, 'private-59c3a3ba46526', 'private-59f10573a3585', '2017-10-26 08:37:22', 'ура ура', 1, NULL),
(118, 'private-59e74a5cce2b8', 'private-59ed96f6865cf', '2017-11-07 05:52:59', 'привет', 0, NULL),
(119, 'private-5a019ab4e043f', 'floctoid', '2017-11-07 11:54:42', 'hi', 0, NULL),
(120, 'floctoid', 'private-5a019ab4e043f', '2017-11-07 11:54:42', 'You said: \'hi\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL),
(121, 'private-5a019ab4e043f', 'floctoid', '2017-11-07 11:54:59', '=)', 0, NULL),
(122, 'floctoid', 'private-5a019ab4e043f', '2017-11-07 11:54:59', 'You said: \'=)\', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `client_id` int(11) UNSIGNED NOT NULL,
  `client_user_id` int(11) DEFAULT NULL,
  `client_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_country` varchar(3) CHARACTER SET utf8 NOT NULL DEFAULT 'US',
  `client_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_street` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_zip` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `client_phone` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `client_mobile` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_ccemail` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_key` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_notes` text CHARACTER SET utf8,
  `client_tags` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_skype` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_status` tinyint(1) NOT NULL DEFAULT '1',
  `client_since` date DEFAULT NULL,
  `client_bday` date DEFAULT NULL,
  `client_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `client_currency` varchar(4) CHARACTER SET utf8 NOT NULL DEFAULT 'USD',
  `client_collab` int(11) NOT NULL DEFAULT '0',
  `client_pusher_channel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `client_credit` decimal(10,2) NOT NULL DEFAULT '0.00',
  `client_type` smallint(3) NOT NULL DEFAULT '1',
  `client_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`client_id`, `client_user_id`, `client_name`, `client_country`, `client_city`, `client_state`, `client_street`, `client_zip`, `client_phone`, `client_mobile`, `client_email`, `client_ccemail`, `client_key`, `client_notes`, `client_tags`, `client_skype`, `client_status`, `client_since`, `client_bday`, `client_discount`, `client_currency`, `client_collab`, `client_pusher_channel`, `client_credit`, `client_type`, `client_trash`) VALUES
(1, 3, 'Сергей', 'US', '', 'AK', '', '', '', NULL, 'zavhozmain@gmail.com', '', '59d3c98686621', NULL, 'zavhozmain@gmail.com Сергей    ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0),
(2, 3, 'Yuliya', 'US', '', 'AK', '', '', '', NULL, 'ugoloto@gmail.com', NULL, NULL, NULL, 'ugoloto@gmail.com Yuliya    ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 1),
(3, 1, 'Whatsuap', 'DE', '', '', '', '', '', NULL, '', '', '59df4b2990a19', NULL, ' Whatsuap    ', NULL, 1, NULL, NULL, '0.00', 'EUR', 0, NULL, '0.00', 1, 1),
(4, 1, 'Jack Ermolenko', 'UA', 'Киев', '', 'Малиновского 27/23 КВ 402', '10000', '0504750525', NULL, 'jack69@ukr.net', '', '59df4d10b8542', NULL, 'jack69@ukr.net Jack Ermolenko 0504750525 Малиновского 27/23 КВ 402 Киев 10000', NULL, 1, NULL, NULL, '10.00', 'UAH', 0, NULL, '0.00', 1, 0),
(5, 4, 'кенкенке', 'US', 'вапвапва', 'AK', 'вапавпвап', '43534', '345435345', NULL, 'jkhh@jhk.com', 'sdfsdf@ddd.com', '59df4d27a4128', NULL, 'jkhh@jhk.com кенкенке 345435345 вапавпвап вапвапва 43534', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0),
(6, 3, 'кееекsdfd', 'US', 'екекк', 'AK', 'еккеекdsssssssssss', 'кеке', '21212323', NULL, '', '', '59e0b51f3c075', NULL, ' кееекsdfd 21212323 еккеекdsssssssssss екекк кеке', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0),
(7, 1, 'just client', 'US', '', 'AK', '', '', '', NULL, '', NULL, NULL, NULL, ' just client    ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 1),
(8, 1, 'dfdsfsd', 'US', '', 'AK', '', '', '', NULL, '', NULL, NULL, NULL, ' dfdsfsd    ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 1),
(9, 1, 'dfgdfg', 'US', '', 'AK', '', '', '', NULL, '', NULL, NULL, NULL, ' dfgdfg    ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 1),
(10, 11, 'Алла', 'US', 'Киев', 'TX', 'Киев', '', '', NULL, '', '', '59e74c0c6e30d', NULL, ' Алла  Киев Киев ', NULL, 1, NULL, NULL, '10.00', 'USD', 0, NULL, '0.00', 1, 0),
(11, 11, 'Алла Моисеева', 'UA', 'Kiev', 'Kiev', 'Автозаводская 43', '', '0957170488', NULL, 'Moisa30@gmail.com', NULL, NULL, NULL, 'Moisa30@gmail.com Алла Моисеева 0957170488 Автозаводская 43 Kiev ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0),
(12, 1, 'нака', 'DE', 'Berlin', '', '', '', '', NULL, '', '', '59e912ae97a5d', NULL, ' нака   Berlin ', NULL, 1, NULL, NULL, '0.00', 'EUR', 0, NULL, '0.00', 1, 0),
(13, 1, 'ertretter', 'US', '', 'AK', '', '', '', NULL, '', '', '59ed8cef3d139', NULL, ' ertretter    ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0),
(14, 13, 'Anna', 'UA', '', '', '', '', '', NULL, '', '', '59edf2674467b', NULL, ' Anna    ', NULL, 1, NULL, NULL, '5.00', 'USD', 0, NULL, '0.00', 1, 0),
(15, 13, 'ANN', 'UA', '', '', '', '', '', NULL, '', '', '59edf3b3ee39e', NULL, ' ANN    ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0),
(16, 13, 'julia', 'UA', '', '', '', '', '', NULL, '', '', '59edf3cdc38f6', NULL, ' julia    ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0),
(17, 13, 'Anika', 'UA', '', '', '', '', '123123', NULL, '', '', '59edf403ae5f3', NULL, ' Anika 123123   ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0),
(18, 15, 'амплеев', 'US', '', 'AK', '', '', '', NULL, '', NULL, NULL, NULL, ' амплеев    ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0),
(19, 11, 'Элла', 'US', 'Лос-Анджелес', 'CA', '', '', '', NULL, 'tls22@ukr.net', NULL, NULL, NULL, 'tls22@ukr.net Элла   Лос-Анджелес ', NULL, 1, NULL, NULL, '0.00', 'USD', 0, NULL, '0.00', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `clients_types`
--

CREATE TABLE `clients_types` (
  `clienttype_id` int(11) UNSIGNED NOT NULL,
  `clienttype_type` smallint(2) NOT NULL DEFAULT '1',
  `clienttype_name` varchar(255) DEFAULT NULL,
  `clienttype_lang` varchar(4) NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clients_types`
--

INSERT INTO `clients_types` (`clienttype_id`, `clienttype_type`, `clienttype_name`, `clienttype_lang`) VALUES
(1, 1, 'client', 'en'),
(2, 2, 'vendor', 'en'),
(3, 1, 'Клиент', 'ru'),
(4, 2, 'Продавец', 'ru'),
(5, 1, 'Клiєнт', 'ua'),
(6, 2, 'Продавец', 'ua');

-- --------------------------------------------------------

--
-- Структура таблицы `collaborator`
--

CREATE TABLE `collaborator` (
  `cb_id` int(11) UNSIGNED NOT NULL,
  `cb_user_id` int(11) NOT NULL,
  `cb_collobarator_id` int(11) NOT NULL,
  `cb_status` int(11) NOT NULL DEFAULT '1',
  `cb_billing_name` varchar(255) DEFAULT NULL,
  `cb_billing_country` varchar(255) DEFAULT NULL,
  `cb_billing_city` varchar(255) DEFAULT NULL,
  `cb_billing_state` varchar(255) DEFAULT NULL,
  `cb_billing_street` varchar(255) DEFAULT NULL,
  `cb_billing_zip` varchar(255) DEFAULT NULL,
  `cb_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cb_credit` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `collaborator`
--

INSERT INTO `collaborator` (`cb_id`, `cb_user_id`, `cb_collobarator_id`, `cb_status`, `cb_billing_name`, `cb_billing_country`, `cb_billing_city`, `cb_billing_state`, `cb_billing_street`, `cb_billing_zip`, `cb_discount`, `cb_credit`) VALUES
(9, 3, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(10, 1, 3, 3, 'Juliya', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(31, 5, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(32, 1, 5, 3, 'Alan Harper', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(37, 1, 6, 3, 'Bob Martin', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(38, 6, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(39, 7, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(40, 1, 7, 3, 'Юля', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(41, 8, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(42, 1, 8, 3, 'Vlad Skotsenko', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(43, 9, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(44, 1, 9, 3, 'Pavel Polyakov', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(45, 10, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(46, 1, 10, 3, 'Sergey Goloto', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(47, 11, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(48, 1, 11, 3, 'Mila Tretyak', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(51, 4, 3, 3, 'Juliya', 'US', '', '', '', '', '0.00', '0.00'),
(52, 3, 4, 3, 'Сергей Васильевич', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(53, 1, 12, 3, 'Спасский Александр', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(54, 12, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(55, 12, 11, 3, 'Mila Tretyak', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(56, 11, 12, 3, 'Спасский Александр', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(57, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00'),
(58, 2, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00'),
(59, 13, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(60, 1, 13, 3, 'Anna Kramar', 'UA', '', '', '', '', '0.00', '0.00'),
(61, 13, 12, 3, 'Спасский Александр', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(62, 12, 13, 3, 'Anna Kramar', 'UA', '', '', '', '', '0.00', '0.00'),
(63, 13, 11, 3, 'Mila Tretyak', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(64, 11, 13, 3, 'Anna Kramar', 'UA', '', '', '', '', '0.00', '0.00'),
(65, 14, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(66, 1, 14, 3, 'Sascha', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(67, 14, 7, 3, 'Юля', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(68, 7, 14, 3, 'Sascha', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(69, 11, 16, 3, 'Мила Моисеева', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(70, 16, 11, 3, 'Mila Tretyak', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(71, 11, 17, 3, 'Нина Ярославская', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(72, 17, 11, 3, 'Mila Tretyak', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(73, 1, 18, 3, 'Sergei Shamshin', 'US', NULL, NULL, NULL, NULL, '0.00', '0.00'),
(74, 18, 1, 3, 'Valerii Igumentsev', 'US', '', '', '', '', '0.00', '0.00'),
(75, 19, 16, 1, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00'),
(76, 16, 19, 2, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', '0.00');

-- --------------------------------------------------------

--
-- Структура таблицы `collaborator_statuses`
--

CREATE TABLE `collaborator_statuses` (
  `cbs_id` int(11) UNSIGNED NOT NULL,
  `cbs_status_id` int(11) DEFAULT NULL,
  `cbs_name` varchar(250) DEFAULT NULL,
  `cbs_lang` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `collaborator_statuses`
--

INSERT INTO `collaborator_statuses` (`cbs_id`, `cbs_status_id`, `cbs_name`, `cbs_lang`) VALUES
(1, 1, 'Invited', 'en'),
(2, 1, 'Приглашен', 'ru'),
(3, 3, 'Accepted', 'en'),
(4, 3, 'Принят', 'ru'),
(5, 2, 'Incoming invite', 'en'),
(6, 2, 'Приглашение к сотрудничеству', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `colors`
--

CREATE TABLE `colors` (
  `color_id` int(11) UNSIGNED NOT NULL,
  `color_hex` varchar(10) DEFAULT NULL,
  `color_text` varchar(10) NOT NULL DEFAULT '#ffffff'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `colors`
--

INSERT INTO `colors` (`color_id`, `color_hex`, `color_text`) VALUES
(1, '#5f7f94', '#ffffff'),
(2, '#dfe134', '#ffffff'),
(3, '#e6323d', '#ffffff'),
(4, '#e9552c', '#ffffff'),
(5, '#59b8e6', '#ffffff'),
(6, '#ea2e95', '#ffffff'),
(7, '#3fac54', '#ffffff'),
(8, '#0e2f44', '#ffffff'),
(9, '#094eac', '#ffffff'),
(10, '#793939', '#ffffff'),
(11, '#008b8b', '#ffffff'),
(12, '#620338', '#ffffff'),
(13, '#aa94f2', '#ffffff'),
(14, '#f294cd', '#ffffff'),
(15, '#f4d69e', '#ffffff'),
(16, '#094eac', '#ffffff'),
(17, '#e9552c', '#ffffff'),
(18, '#0e2f44', '#ffffff'),
(19, '#cc0000', '#ffffff'),
(20, '#599346', '#ffffff'),
(21, '#094bde', '#ffffff');

-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `country_iso` char(2) CHARACTER SET utf8 NOT NULL,
  `country_name` varchar(80) CHARACTER SET utf8 NOT NULL,
  `country_printable_name` varchar(80) CHARACTER SET utf8 NOT NULL,
  `country_iso3` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `country_numcode` smallint(6) DEFAULT NULL,
  `country_phone_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Countries List';

--
-- Дамп данных таблицы `countries`
--

INSERT INTO `countries` (`country_id`, `country_iso`, `country_name`, `country_printable_name`, `country_iso3`, `country_numcode`, `country_phone_code`) VALUES
(2, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(3, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(4, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(5, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(6, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(7, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 0),
(8, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 0),
(9, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 0),
(10, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 0),
(11, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 0),
(12, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 0),
(13, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 0),
(15, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 0),
(16, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 0),
(17, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 0),
(18, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 0),
(19, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 0),
(20, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 0),
(21, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 0),
(22, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 0),
(23, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 0),
(24, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 0),
(25, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 0),
(26, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 0),
(27, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 0),
(28, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 0),
(29, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 0),
(30, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 0),
(31, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 0),
(32, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 0),
(33, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 0),
(34, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(35, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 0),
(36, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 0),
(37, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 0),
(38, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 0),
(39, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 0),
(40, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 0),
(41, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 0),
(42, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 0),
(43, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 0),
(44, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 0),
(45, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 0),
(46, 'TD', 'CHAD', 'Chad', 'TCD', 148, 0),
(47, 'CL', 'CHILE', 'Chile', 'CHL', 152, 0),
(48, 'CN', 'CHINA', 'China', 'CHN', 156, 0),
(49, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 0),
(50, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 0),
(51, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 0),
(52, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 0),
(53, 'CG', 'CONGO', 'Congo', 'COG', 178, 0),
(54, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 0),
(55, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 0),
(56, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 0),
(57, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 0),
(58, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 0),
(59, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 0),
(60, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 0),
(61, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 0),
(62, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 0),
(63, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 0),
(64, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 0),
(65, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 0),
(66, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 0),
(67, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 0),
(68, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 0),
(69, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 0),
(70, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 0),
(71, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 0),
(72, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 0),
(73, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 0),
(74, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 0),
(75, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 0),
(76, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 0),
(77, 'FR', 'FRANCE', 'France', 'FRA', 250, 0),
(78, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 0),
(79, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 0),
(80, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(81, 'GA', 'GABON', 'Gabon', 'GAB', 266, 0),
(82, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 0),
(83, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 0),
(84, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 0),
(85, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 0),
(86, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 0),
(87, 'GR', 'GREECE', 'Greece', 'GRC', 300, 0),
(88, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 0),
(89, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 0),
(90, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 0),
(91, 'GU', 'GUAM', 'Guam', 'GUM', 316, 0),
(92, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 0),
(93, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 0),
(94, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 0),
(95, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 0),
(96, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 0),
(97, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(98, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 0),
(99, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 0),
(100, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 0),
(101, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 0),
(102, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 0),
(103, 'IN', 'INDIA', 'India', 'IND', 356, 0),
(104, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 0),
(105, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 0),
(106, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 0),
(107, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 0),
(108, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 0),
(109, 'IT', 'ITALY', 'Italy', 'ITA', 380, 0),
(110, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 0),
(111, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 0),
(112, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 0),
(113, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 0),
(114, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 0),
(115, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 0),
(116, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 0),
(117, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 0),
(118, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 0),
(119, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 0),
(120, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 0),
(121, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 0),
(122, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 0),
(123, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 0),
(124, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 0),
(125, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 0),
(126, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 0),
(127, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 0),
(128, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 0),
(129, 'MO', 'MACAO', 'Macao', 'MAC', 446, 0),
(130, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 0),
(131, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 0),
(132, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 0),
(133, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 0),
(134, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 0),
(135, 'ML', 'MALI', 'Mali', 'MLI', 466, 0),
(136, 'MT', 'MALTA', 'Malta', 'MLT', 470, 0),
(137, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 0),
(138, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 0),
(139, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 0),
(140, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 0),
(141, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 0),
(142, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 0),
(143, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 0),
(144, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 0),
(145, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 0),
(146, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 0),
(147, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 0),
(148, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 0),
(149, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 0),
(150, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 0),
(151, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 0),
(152, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 0),
(153, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 0),
(154, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 0),
(155, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 0),
(156, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 0),
(157, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 0),
(158, 'NE', 'NIGER', 'Niger', 'NER', 562, 0),
(159, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 0),
(160, 'NU', 'NIUE', 'Niue', 'NIU', 570, 0),
(161, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 0),
(162, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 0),
(163, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 0),
(164, 'OM', 'OMAN', 'Oman', 'OMN', 512, 0),
(165, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 0),
(166, 'PW', 'PALAU', 'Palau', 'PLW', 585, 0),
(167, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 0),
(168, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 0),
(169, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 0),
(170, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 0),
(171, 'PE', 'PERU', 'Peru', 'PER', 604, 0),
(172, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 0),
(173, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(174, 'PL', 'POLAND', 'Poland', 'POL', 616, 0),
(175, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 0),
(176, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 0),
(177, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 0),
(178, 'RE', 'REUNION', 'Reunion', 'REU', 638, 0),
(179, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 0),
(180, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 0),
(181, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 0),
(182, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 0),
(183, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 0),
(184, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 0),
(185, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 0),
(186, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 0),
(187, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 0),
(188, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 0),
(189, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 0),
(190, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 0),
(191, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 0),
(192, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 0),
(193, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 0),
(194, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 0),
(195, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 0),
(196, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 0),
(197, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 0),
(198, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 0),
(199, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 0),
(200, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 0),
(201, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(202, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 0),
(203, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 0),
(204, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 0),
(205, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 0),
(206, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 0),
(207, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 0),
(208, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 0),
(209, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 0),
(210, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 0),
(211, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 0),
(212, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 0),
(213, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 0),
(214, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 0),
(215, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 0),
(216, 'TG', 'TOGO', 'Togo', 'TGO', 768, 0),
(217, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 0),
(218, 'TO', 'TONGA', 'Tonga', 'TON', 776, 0),
(219, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 0),
(220, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 0),
(221, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 0),
(222, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 0),
(223, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 0),
(224, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 0),
(225, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 0),
(226, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 38),
(227, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 0),
(228, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 0),
(229, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 0),
(230, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 0),
(231, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 0),
(232, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 0),
(233, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 0),
(234, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 0),
(235, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 0),
(236, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 0),
(237, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 0),
(238, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 0),
(239, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 0),
(240, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `cronemail`
--

CREATE TABLE `cronemail` (
  `cronemail_id` int(11) UNSIGNED NOT NULL,
  `cronemail_user_id` int(11) DEFAULT NULL,
  `cronemail_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cronemail_status` tinyint(1) NOT NULL DEFAULT '0',
  `cronemail_object` varchar(100) DEFAULT NULL,
  `cronemail_object_id` int(11) NOT NULL DEFAULT '0',
  `cronemail_temail_to` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cronemail`
--

INSERT INTO `cronemail` (`cronemail_id`, `cronemail_user_id`, `cronemail_create`, `cronemail_status`, `cronemail_object`, `cronemail_object_id`, `cronemail_temail_to`) VALUES
(1, 3, '2017-10-03 17:50:56', 0, 'estimate', 1, 'ugoloto@gmail.com'),
(2, 3, '2017-10-03 17:56:08', 0, 'estimate', 2, 'zavhozmain@gmail.com'),
(3, 3, '2017-10-03 17:56:08', 0, 'estimate', 2, 'zavhozmain@gmail.com'),
(4, 3, '2017-10-03 18:00:23', 0, 'invoice', 1, 'zavhozmain@gmail.com'),
(5, 3, '2017-10-03 18:00:23', 0, 'invoice', 1, 'zavhozmain@gmail.com'),
(6, 3, '2017-10-03 18:47:54', 0, 'invoice', 2, 'zavhozmain@gmail.com'),
(7, 16, '2017-10-31 07:45:37', 0, 'invoice', 6, 'pr.idea.pr@gmail.com');

-- --------------------------------------------------------

--
-- Структура таблицы `currencies`
--

CREATE TABLE `currencies` (
  `currency_id` int(11) NOT NULL,
  `currency_iso` varchar(5) CHARACTER SET utf8 NOT NULL,
  `currency_name` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `currencies`
--

INSERT INTO `currencies` (`currency_id`, `currency_iso`, `currency_name`) VALUES
(1, 'USD', 'United States Dollar'),
(2, 'CAD', 'Canada Dollar'),
(3, 'EUR', 'Euro'),
(4, 'GBP', 'United Kingdom Pound'),
(5, 'AUD', 'Australia Dollar'),
(6, 'CHF', 'Switzerland Franc'),
(7, 'CZK', 'Czech Republic Koruna'),
(8, 'DKK', 'Denmark Krone'),
(9, 'HKD', 'Hong Kong Dollar'),
(10, 'HUF', 'Hungary Forint'),
(11, 'JPY', 'Japan Yen'),
(12, 'NOK', 'Norway Krone'),
(13, 'NZD', 'New Zealand Dollar'),
(14, 'PLN', 'Poland Zloty'),
(15, 'SEK', 'Sweden Krona'),
(16, 'SGD', 'Singapore Dollar'),
(17, 'RUB', 'Russia Ruble'),
(18, 'UAH', 'Ukraine Hryvna'),
(19, 'ZAR', 'South African rand'),
(20, 'CNY', 'Chinese Yuan'),
(21, 'INR', 'Indian Rupee'),
(22, 'MXN', 'Mexican Peso'),
(23, 'KZT', 'Kazakh Tenge'),
(24, 'LKR', 'Sri Lankan Rupee');

-- --------------------------------------------------------

--
-- Структура таблицы `docs`
--

CREATE TABLE `docs` (
  `doc_id` int(11) NOT NULL,
  `doc_user_id` int(11) NOT NULL,
  `doc_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - file, 2 - folder',
  `doc_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `doc_changed` timestamp NULL DEFAULT NULL,
  `doc_name` varchar(255) DEFAULT NULL,
  `doc_text` text,
  `doc_folder` int(11) NOT NULL DEFAULT '0',
  `doc_shared` tinyint(4) NOT NULL DEFAULT '0',
  `doc_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `docs`
--

INSERT INTO `docs` (`doc_id`, `doc_user_id`, `doc_type`, `doc_created`, `doc_changed`, `doc_name`, `doc_text`, `doc_folder`, `doc_shared`, `doc_trash`) VALUES
(1, 3, 1, '2017-10-03 16:00:24', '2017-10-13 15:37:11', 'Регистрация', '<p>После регистрации в заполнении формы, можно сделать чтобы и язык менялся динамически без перезагрузки страницы, выбор форматов меняется динамически без перезагрузки и если делать мультиязычность на многих языках, должно быть динамически и сразу.</p>\n', 0, 1, 0),
(2, 3, 1, '2017-10-15 08:26:10', '2017-10-15 08:26:22', 'Новый документ', '', 0, 0, 1),
(3, 3, 1, '2017-10-15 08:27:21', '2017-10-15 08:27:54', 'Новый документ', '', 0, 0, 1),
(4, 3, 2, '2017-10-15 08:28:23', '2017-10-15 08:28:23', 'папка', NULL, 0, 0, 0),
(5, 3, 1, '2017-10-21 06:47:37', '2017-10-21 06:48:14', 'Новый документ', '<p>-- phpMyAdmin SQL Dump<br />\n-- version 4.5.4.1deb2ubuntu2<br />\n-- http://www.phpmyadmin.net<br />\n--<br />\n-- Хост: localhost<br />\n-- Время создания: Окт 21 2017 г., 09:43<br />\n-- Версия сервера: 5.7.19-0ubuntu0.16.04.1<br />\n-- Версия PHP: 7.0.22-0ubuntu0.16.04.1</p>\n\n<p>SET SQL_MODE = &quot;NO_AUTO_VALUE_ON_ZERO&quot;;<br />\nSET time_zone = &quot;+00:00&quot;;</p>\n\n<p><br />\n/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;<br />\n/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;<br />\n/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;<br />\n/*!40101 SET NAMES utf8mb4 */;</p>\n\n<p>--<br />\n-- База данных: `floctopus`<br />\n--</p>\n\n<p>-- --------------------------------------------------------</p>\n\n<p>--<br />\n-- Структура таблицы `user_work`<br />\n--</p>\n\n<p>CREATE TABLE `user_work` (<br />\n&nbsp; `work_id` int(11) NOT NULL,<br />\n&nbsp; `work_user_id` int(11) NOT NULL DEFAULT &#39;0&#39;,<br />\n&nbsp; `work_company` varchar(255) DEFAULT NULL,<br />\n&nbsp; `work_position` varchar(255) DEFAULT NULL,<br />\n&nbsp; `work_location` varchar(255) DEFAULT NULL,<br />\n&nbsp; `work_desc` text,<br />\n&nbsp; `work_year_from` int(4) NOT NULL DEFAULT &#39;0&#39;,<br />\n&nbsp; `work_year_to` int(4) NOT NULL DEFAULT &#39;0&#39;,<br />\n&nbsp; `work_month_from` varchar(100) DEFAULT &#39;0&#39;,<br />\n&nbsp; `work_month_to` varchar(100) DEFAULT NULL,<br />\n&nbsp; `work_trash` int(11) NOT NULL DEFAULT &#39;0&#39;<br />\n) ENGINE=InnoDB DEFAULT CHARSET=utf8;</p>\n\n<p>--<br />\n-- Дамп данных таблицы `user_work`<br />\n--</p>\n\n<p>INSERT INTO `user_work` (`work_id`, `work_user_id`, `work_company`, `work_position`, `work_location`, `work_desc`, `work_year_from`, `work_year_to`, `work_month_from`, `work_month_to`, `work_trash`) VALUES<br />\n(1, 3, &#39;Floctopus&#39;, &#39;Web developer&#39;, &#39;Sumy&#39;, &#39;I am Cool&#39;, 2017, 2017, &#39;Июнь&#39;, &#39;Ноябрь&#39;, 1),<br />\n(2, 3, &#39;first and best company for freelanser Floctopus&#39;, &#39;Web developer&#39;, &#39;Sumy&#39;, &#39;I am Cool&#39;, 2017, 2017, &#39;Январь&#39;, &#39;Январь&#39;, 1),<br />\n(3, 3, &#39;first and best company for freelanser Floctopus&#39;, &#39;Web developer&#39;, &#39;Sumy&#39;, &#39;I am Cool&#39;, 2016, 2017, &#39;Январь&#39;, &#39;Январь&#39;, 0);</p>\n\n<p>--<br />\n-- Индексы сохранённых таблиц<br />\n--</p>\n\n<p>--<br />\n-- Индексы таблицы `user_work`<br />\n--<br />\nALTER TABLE `user_work`<br />\n&nbsp; ADD PRIMARY KEY (`work_id`);</p>\n\n<p>--<br />\n-- AUTO_INCREMENT для сохранённых таблиц<br />\n--</p>\n\n<p>--<br />\n-- AUTO_INCREMENT для таблицы `user_work`<br />\n--<br />\nALTER TABLE `user_work`<br />\n&nbsp; MODIFY `work_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;<br />\n/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;<br />\n/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;<br />\n/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;</p>\n', 0, 1, 0),
(6, 1, 1, '2017-10-23 06:43:27', '2017-11-01 09:35:09', 'Новый документ', '', 0, 0, 0),
(7, 1, 1, '2017-10-23 06:44:39', '2017-10-23 06:47:00', 'Новый документ', '<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n	<tbody>\n		<tr>\n			<td>&nbsp;</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>&nbsp;</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>&nbsp;</td>\n			<td>&nbsp;</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n', 0, 0, 0),
(8, 13, 1, '2017-10-23 10:55:19', '2017-10-24 13:17:41', 'Новый документ', '<p><span style=\"color:#e74c3c\">оаоралор</span></p>\n\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n	<tbody>\n		<tr>\n			<td>&nbsp;</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>&nbsp;</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>&nbsp;</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>&nbsp;</td>\n			<td>&nbsp;</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', 0, 0, 0),
(9, 13, 1, '2017-10-24 09:19:46', '2017-10-24 09:22:37', 'Новый документ', '', 0, 0, 0),
(10, 13, 2, '2017-10-24 09:22:57', '2017-10-24 09:22:57', 'п', NULL, 0, 0, 0),
(11, 13, 2, '2017-10-24 09:24:27', '2017-10-24 09:24:27', 'тест', NULL, 0, 0, 0),
(12, 19, 1, '2017-11-07 12:05:40', '2017-11-07 12:08:03', '88', '<p>ПРИВЕТ ОКТОПУЛУС</p>\n', 0, 0, 0),
(13, 19, 2, '2017-11-07 12:08:42', '2017-11-07 12:08:42', '7', NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `doc_collaborators`
--

CREATE TABLE `doc_collaborators` (
  `dc_id` int(11) UNSIGNED NOT NULL,
  `dc_user_id` int(11) NOT NULL DEFAULT '0',
  `dc_collab_id` int(11) NOT NULL DEFAULT '0',
  `dc_doc_id` int(11) NOT NULL DEFAULT '0',
  `dc_trash` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `doc_collaborators`
--

INSERT INTO `doc_collaborators` (`dc_id`, `dc_user_id`, `dc_collab_id`, `dc_doc_id`, `dc_trash`) VALUES
(2, 3, 4, 1, 0),
(3, 3, 1, 1, 0),
(4, 3, 1, 5, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `estimates`
--

CREATE TABLE `estimates` (
  `est_id` int(11) NOT NULL,
  `est_user_id` int(11) NOT NULL DEFAULT '0',
  `est_client_id` int(11) NOT NULL DEFAULT '0',
  `est_collab_id` int(11) NOT NULL DEFAULT '0',
  `est_date` date DEFAULT NULL,
  `est_num` int(11) NOT NULL DEFAULT '0',
  `est_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `est_street` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `est_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `est_state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `est_zip` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `est_country` varchar(3) CHARACTER SET utf8 NOT NULL DEFAULT 'US',
  `est_discount_percentage` decimal(10,2) NOT NULL DEFAULT '0.00',
  `est_discount_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `est_subtotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `est_taxes_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `est_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `est_total_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `est_total_due` decimal(10,2) NOT NULL DEFAULT '0.00',
  `est_total_due_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `est_recurring` int(11) NOT NULL DEFAULT '0',
  `est_attach` tinyint(1) NOT NULL DEFAULT '0',
  `est_status` tinyint(2) NOT NULL DEFAULT '1',
  `est_sent_email` tinyint(1) NOT NULL DEFAULT '0',
  `est_arc` tinyint(1) NOT NULL DEFAULT '0',
  `est_currency` varchar(4) CHARACTER SET utf8 NOT NULL DEFAULT 'USD',
  `est_tags` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `est_prj` int(11) NOT NULL DEFAULT '0',
  `est_exchange_rate` decimal(10,4) NOT NULL DEFAULT '1.0000',
  `est_terms` text CHARACTER SET utf8,
  `est_notes` text CHARACTER SET utf8,
  `est_view` tinyint(1) NOT NULL DEFAULT '0',
  `est_lang` varchar(3) NOT NULL DEFAULT 'en',
  `est_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `estimates`
--

INSERT INTO `estimates` (`est_id`, `est_user_id`, `est_client_id`, `est_collab_id`, `est_date`, `est_num`, `est_name`, `est_street`, `est_city`, `est_state`, `est_zip`, `est_country`, `est_discount_percentage`, `est_discount_cash`, `est_subtotal`, `est_taxes_cash`, `est_total`, `est_total_base`, `est_total_due`, `est_total_due_base`, `est_recurring`, `est_attach`, `est_status`, `est_sent_email`, `est_arc`, `est_currency`, `est_tags`, `est_prj`, `est_exchange_rate`, `est_terms`, `est_notes`, `est_view`, `est_lang`, `est_trash`) VALUES
(1, 3, 2, 0, '2017-10-03', 1, 'Yuliya', '', '', 'AK', '', 'US', '0.00', '0.00', '100.00', '0.00', '100.00', '100.00', '100.00', '100.00', 0, 1, 5, 1, 0, 'USD', 'Yuliya 1 100.00', 4, '1.0000', '', '', 0, 'ru', 0),
(2, 3, 2, 0, '2017-10-03', 2, 'Yuliya', '', '', 'AK', '', 'US', '0.00', '0.00', '100.00', '0.00', '100.00', '100.00', '100.00', '100.00', 0, 1, 5, 1, 0, 'USD', 'Yuliya 2 100.00', 0, '1.0000', '', '', 0, 'ru', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `estimates_attach`
--

CREATE TABLE `estimates_attach` (
  `estatt_id` int(11) UNSIGNED NOT NULL,
  `estatt_user_id` int(11) NOT NULL DEFAULT '0',
  `estatt_est_id` int(11) NOT NULL DEFAULT '0',
  `estatt_fname` varchar(255) DEFAULT NULL,
  `estatt_type` varchar(100) DEFAULT NULL,
  `estatt_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `estimates_attach`
--

INSERT INTO `estimates_attach` (`estatt_id`, `estatt_user_id`, `estatt_est_id`, `estatt_fname`, `estatt_type`, `estatt_trash`) VALUES
(1, 3, 1, '_2017_09_26_09_04_56.png', 'image', 0),
(2, 3, 1, '_2017_09_26_09_09_34.png', 'image', 0),
(3, 3, 1, '_2017_09_26_09_39_56.png', 'image', 0),
(4, 3, 1, '_2017_09_26_09_43_16.png', 'image', 0),
(5, 3, 1, '_2017_09_26_14_43_26.png', 'image', 0),
(6, 3, 1, '_2017_09_26_14_43_56.png', 'image', 0),
(7, 3, 1, '_2017_10_02_14_35_59.png', 'image', 0),
(8, 3, 1, '_2017_10_03_19_03_52.png', 'image', 0),
(9, 3, 1, '_2017_10_03_19_03_57.png', 'image', 0),
(10, 3, 1, '_2017_10_03_19_04_08.png', 'image', 0),
(21, 3, 2, '_2017_09_26_09_04_56.png', 'image', 0),
(22, 3, 2, '_2017_09_26_09_09_34.png', 'image', 0),
(23, 3, 2, '_2017_09_26_09_39_56.png', 'image', 0),
(24, 3, 2, '_2017_09_26_09_43_16.png', 'image', 0),
(25, 3, 2, '_2017_09_26_14_43_26.png', 'image', 0),
(26, 3, 2, '_2017_09_26_14_43_56.png', 'image', 0),
(27, 3, 2, '_2017_10_02_14_35_59.png', 'image', 0),
(28, 3, 2, '_2017_10_03_19_03_52.png', 'image', 0),
(29, 3, 2, '_2017_10_03_19_03_57.png', 'image', 0),
(30, 3, 2, '_2017_10_03_19_04_08.png', 'image', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `estimates_lines`
--

CREATE TABLE `estimates_lines` (
  `estline_id` int(11) NOT NULL,
  `estline_user_id` int(11) NOT NULL DEFAULT '0',
  `estline_est_id` int(11) NOT NULL DEFAULT '0',
  `estline_srv` varchar(255) DEFAULT NULL,
  `estline_srv_id` int(11) NOT NULL DEFAULT '0',
  `estline_desc` text,
  `estline_rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estline_qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estline_tax1_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estline_tax1_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estline_tax2_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estline_tax2_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estline_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estline_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `estimates_lines`
--

INSERT INTO `estimates_lines` (`estline_id`, `estline_user_id`, `estline_est_id`, `estline_srv`, `estline_srv_id`, `estline_desc`, `estline_rate`, `estline_qty`, `estline_tax1_percent`, `estline_tax1_cash`, `estline_tax2_percent`, `estline_tax2_cash`, `estline_total`, `estline_trash`) VALUES
(1, 3, 1, 'Floctopus', 1, '', '100.00', '1.00', '0.00', '0.00', '0.00', '0.00', '100.00', 0),
(3, 3, 2, 'Floctopus', 1, '', '100.00', '1.00', '0.00', '0.00', '0.00', '0.00', '100.00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `estimates_statuses`
--

CREATE TABLE `estimates_statuses` (
  `eststatus_id` int(11) UNSIGNED NOT NULL,
  `eststatus_status` tinyint(4) DEFAULT NULL,
  `eststatus_name` varchar(255) DEFAULT NULL,
  `eststatus_lang` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `estimates_statuses`
--

INSERT INTO `estimates_statuses` (`eststatus_id`, `eststatus_status`, `eststatus_name`, `eststatus_lang`) VALUES
(1, 1, 'Draft', 'en'),
(2, 2, 'Accepted', 'en'),
(3, 3, 'Declined', 'en'),
(4, 5, 'Pending', 'en'),
(5, 4, 'Invoiced', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `estimate_actions`
--

CREATE TABLE `estimate_actions` (
  `estact_id` int(11) UNSIGNED NOT NULL,
  `estact_action` varchar(50) DEFAULT NULL,
  `estact_desc` varchar(255) DEFAULT NULL,
  `estact_lang` varchar(4) NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `estimate_actions`
--

INSERT INTO `estimate_actions` (`estact_id`, `estact_action`, `estact_desc`, `estact_lang`) VALUES
(1, 'create', 'Estimate Created', 'en'),
(2, 'update', 'Estimate Updated', 'en'),
(3, 'send', 'Estimate Sent', 'en'),
(4, 'accept', 'Estimate Accepted', 'en'),
(5, 'decline', 'Estimate Declined', 'en'),
(6, 'invoice', 'Estimate Invoiced', 'en'),
(7, 'pending', 'Estimate Marked as Pending', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `estimate_history`
--

CREATE TABLE `estimate_history` (
  `esthist_id` int(11) UNSIGNED NOT NULL,
  `esthist_user_id` int(11) NOT NULL DEFAULT '0',
  `esthist_est_id` int(11) NOT NULL DEFAULT '0',
  `esthist_action` varchar(255) DEFAULT NULL,
  `esthist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `esthist_email` varchar(255) DEFAULT NULL,
  `esthist_cc` varchar(255) DEFAULT NULL,
  `esthist_desc` varchar(255) DEFAULT NULL,
  `esthist_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `esthist_currency` varchar(4) NOT NULL DEFAULT 'USD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `estimate_history`
--

INSERT INTO `estimate_history` (`esthist_id`, `esthist_user_id`, `esthist_est_id`, `esthist_action`, `esthist_date`, `esthist_email`, `esthist_cc`, `esthist_desc`, `esthist_amount`, `esthist_currency`) VALUES
(1, 3, 1, 'create', '2017-10-03 17:50:56', NULL, NULL, NULL, '0.00', 'USD'),
(2, 3, 1, 'email', '2017-10-03 17:50:56', 'ugoloto@gmail.com', NULL, NULL, '0.00', 'USD'),
(3, 3, 1, 'accept', '2017-10-03 17:54:33', NULL, NULL, NULL, '0.00', 'USD'),
(4, 3, 1, 'pending', '2017-10-03 17:54:39', NULL, NULL, NULL, '0.00', 'USD'),
(5, 3, 2, 'create', '2017-10-03 17:55:28', NULL, NULL, NULL, '0.00', 'USD'),
(6, 3, 2, 'email', '2017-10-03 17:56:08', 'zavhozmain@gmail.com', 'zavhozmain@gmail.com', NULL, '0.00', 'USD'),
(7, 3, 2, 'update', '2017-10-03 18:44:47', NULL, NULL, NULL, '0.00', 'USD');

-- --------------------------------------------------------

--
-- Структура таблицы `events`
--

CREATE TABLE `events` (
  `e_id` int(11) UNSIGNED NOT NULL,
  `e_user_id` int(11) NOT NULL DEFAULT '0',
  `e_client_id` int(11) NOT NULL DEFAULT '0',
  `e_collab_id` int(11) NOT NULL DEFAULT '0',
  `e_type` int(11) NOT NULL DEFAULT '0',
  `e_date` timestamp NULL DEFAULT NULL,
  `e_allday` tinyint(1) NOT NULL DEFAULT '1',
  `e_title` varchar(255) DEFAULT NULL,
  `e_object_id` int(11) NOT NULL DEFAULT '0',
  `e_location` varchar(255) DEFAULT NULL,
  `e_close` tinyint(4) NOT NULL DEFAULT '0',
  `e_recurring_id` int(11) NOT NULL DEFAULT '0',
  `e_invoice_id` int(11) NOT NULL DEFAULT '0',
  `e_send_auto` tinyint(1) NOT NULL DEFAULT '0',
  `e_send_email` varchar(255) DEFAULT NULL,
  `e_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `events`
--

INSERT INTO `events` (`e_id`, `e_user_id`, `e_client_id`, `e_collab_id`, `e_type`, `e_date`, `e_allday`, `e_title`, `e_object_id`, `e_location`, `e_close`, `e_recurring_id`, `e_invoice_id`, `e_send_auto`, `e_send_email`, `e_trash`) VALUES
(1, 1, 0, 0, 7, '2017-11-09 12:27:00', 1, 'wsdsd', 0, '', 0, 0, 0, 0, NULL, 0),
(2, 1, 0, 0, 7, '2017-11-09 12:29:00', 1, 'xzzxczxczxczxc dasjhasgd jsdgasjdhg asjdashgd asjdg asjdgasjhd asjdhgas djgas', 0, '', 0, 0, 0, 0, NULL, 0),
(3, 1, 0, 0, 7, '2017-11-09 12:29:00', 1, 'szdasd sdfsdfds sdfdsf sdfdsfds sdfdsf', 0, '', 0, 0, 0, 0, NULL, 0),
(4, 1, 0, 0, 7, '2017-11-10 12:29:00', 1, 'cxvxcxcvxcv', 0, '', 0, 0, 0, 0, NULL, 0),
(5, 1, 0, 0, 7, '2017-11-10 12:30:00', 1, 'cxvcxvxc', 0, '', 0, 0, 0, 0, NULL, 0),
(6, 1, 0, 0, 4, '2017-11-10 12:31:00', 1, 'sadasdasdasd sdasd sad', 0, '', 0, 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `events_recurring`
--

CREATE TABLE `events_recurring` (
  `er_id` int(11) NOT NULL,
  `er_user_id` int(11) NOT NULL DEFAULT '0',
  `er_client_id` int(11) NOT NULL DEFAULT '0',
  `er_collab_id` int(11) NOT NULL DEFAULT '0',
  `er_title` varchar(256) DEFAULT NULL,
  `er_location` varchar(256) DEFAULT NULL,
  `er_freq` varchar(1) NOT NULL DEFAULT 'd' COMMENT 'd - day, w - week, m - month, y - year',
  `er_daily_option` tinyint(1) NOT NULL DEFAULT '0',
  `er_daily_days_count` tinyint(4) NOT NULL DEFAULT '1',
  `er_weekly_count` tinyint(4) NOT NULL DEFAULT '1',
  `er_w1` tinyint(1) NOT NULL DEFAULT '0',
  `er_w2` tinyint(4) NOT NULL DEFAULT '0',
  `er_w3` tinyint(4) NOT NULL DEFAULT '0',
  `er_w4` tinyint(4) NOT NULL DEFAULT '0',
  `er_w5` tinyint(4) NOT NULL DEFAULT '0',
  `er_w6` tinyint(4) NOT NULL DEFAULT '0',
  `er_w7` tinyint(4) NOT NULL DEFAULT '0',
  `er_monthly_option` tinyint(1) NOT NULL DEFAULT '0',
  `er_month_day` tinyint(3) NOT NULL DEFAULT '1',
  `er_month_count1` tinyint(4) NOT NULL DEFAULT '1',
  `er_month_day_number` varchar(20) NOT NULL DEFAULT 'first',
  `er_month_week_day` varchar(10) NOT NULL DEFAULT 'mon',
  `er_month_count2` tinyint(3) NOT NULL DEFAULT '1',
  `er_yarly_option` tinyint(1) NOT NULL DEFAULT '0',
  `er_year_month1` varchar(10) NOT NULL DEFAULT 'jan',
  `er_year_day` tinyint(4) NOT NULL DEFAULT '1',
  `er_year_day_number` varchar(10) NOT NULL DEFAULT 'first',
  `er_year_week_day` varchar(10) NOT NULL DEFAULT 'mon',
  `er_year_month2` varchar(10) NOT NULL DEFAULT 'jan',
  `er_date_start` date NOT NULL,
  `er_end` smallint(6) NOT NULL DEFAULT '1' COMMENT '1 - never, 2 - date,3 - count',
  `er_end_count` int(11) NOT NULL DEFAULT '0',
  `er_end_date` date DEFAULT NULL,
  `er_type` smallint(6) NOT NULL DEFAULT '1',
  `er_invoice_id` int(11) NOT NULL DEFAULT '0',
  `er_send_auto` tinyint(4) NOT NULL DEFAULT '0',
  `er_send_email` varchar(255) DEFAULT NULL,
  `er_trash` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `events_types`
--

CREATE TABLE `events_types` (
  `etype_id` int(11) UNSIGNED NOT NULL,
  `etype_type` int(11) NOT NULL DEFAULT '0',
  `etype_name` varchar(100) DEFAULT '',
  `etype_lang` varchar(3) NOT NULL DEFAULT 'en',
  `etype_order` int(11) NOT NULL DEFAULT '9999999',
  `etype_cal` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `events_types`
--

INSERT INTO `events_types` (`etype_id`, `etype_type`, `etype_name`, `etype_lang`, `etype_order`, `etype_cal`) VALUES
(1, 1, 'Task', 'en', 9999999, 0),
(2, 2, 'Milestone', 'en', 9999999, 0),
(3, 3, 'Issue', 'en', 9999999, 0),
(4, 4, 'Meeting', 'en', 20, 1),
(5, 5, 'Phone Call', 'en', 30, 1),
(6, 9999, 'Other', 'en', 40, 1),
(7, 6, 'Invoice', 'en', 9999999, 0),
(8, 7, 'Event', 'en', 10, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `expenses`
--

CREATE TABLE `expenses` (
  `exp_id` int(11) UNSIGNED NOT NULL,
  `exp_user_id` int(11) NOT NULL DEFAULT '0',
  `exp_date` date DEFAULT NULL,
  `exp_desc` varchar(255) DEFAULT NULL,
  `exp_vendor` varchar(255) DEFAULT NULL,
  `exp_vendor_id` int(11) NOT NULL DEFAULT '0',
  `exp_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `exp_amount_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `exp_exchange_rate` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `exp_tax1_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `exp_tax2_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `exp_category` int(11) NOT NULL DEFAULT '0',
  `exp_category_parent` int(11) NOT NULL DEFAULT '0',
  `exp_pic` varchar(255) NOT NULL DEFAULT 'nopic.png',
  `exp_recurring` int(11) NOT NULL DEFAULT '0',
  `exp_currency` varchar(4) NOT NULL DEFAULT 'usd',
  `exp_method` tinyint(2) NOT NULL DEFAULT '1',
  `exp_trash` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `expenses`
--

INSERT INTO `expenses` (`exp_id`, `exp_user_id`, `exp_date`, `exp_desc`, `exp_vendor`, `exp_vendor_id`, `exp_amount`, `exp_amount_base`, `exp_exchange_rate`, `exp_tax1_cash`, `exp_tax2_cash`, `exp_category`, `exp_category_parent`, `exp_pic`, `exp_recurring`, `exp_currency`, `exp_method`, `exp_trash`) VALUES
(1, 3, '2017-10-18', 'dfdddf', '', 0, '0.00', '0.00', '1.0000', '0.00', '0.00', 3, 0, 'nopic.png', 0, 'USD', 1, 1),
(2, 3, '2017-10-18', 'dffdfgfg', 'fddffgd', 0, '0.00', '0.00', '1.0000', '0.00', '0.00', 1, 0, 'nopic.png', 0, 'USD', 1, 1),
(3, 1, '2017-11-09', 'Набор гелевых ручек', 'Сельпо', 13, '25.34', '25.34', '1.0000', '0.00', '0.00', 12, 0, 'nopic.png', 0, 'UAH', 1, 0),
(4, 1, '2017-11-09', 'Офисный канцелярский набор', 'Сельпо', 13, '119.99', '119.99', '1.0000', '0.00', '0.00', 12, 0, 'nopic.png', 0, 'UAH', 1, 0),
(5, 1, '2017-11-09', 'Бумага для заметок в коробке', 'Сельпо', 13, '59.99', '59.99', '1.0000', '0.00', '0.00', 12, 0, 'nopic.png', 0, 'UAH', 1, 0),
(6, 1, '2017-11-09', 'Тетрадь для записей', 'Сельпо', 0, '57.99', '57.99', '1.0000', '0.00', '0.00', 12, 0, 'nopic.png', 0, 'UAH', 1, 0),
(7, 1, '2017-11-09', 'Мышка проводная Piko', 'Фокстрот', 0, '89.91', '89.91', '1.0000', '0.00', '0.00', 12, 0, 'nopic.png', 0, 'UAH', 1, 0),
(8, 1, '2017-11-09', 'Интернет', 'Телегруп Украина', 0, '400.00', '400.00', '1.0000', '0.00', '0.00', 12, 0, 'nopic.png', 0, 'UAH', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `expenses_category`
--

CREATE TABLE `expenses_category` (
  `expcat_id` int(11) UNSIGNED NOT NULL,
  `expcat_user_id` int(11) NOT NULL DEFAULT '0',
  `expcat_name` varchar(200) DEFAULT NULL,
  `expcat_parent_id` int(11) NOT NULL DEFAULT '0',
  `expcat_default` tinyint(4) NOT NULL DEFAULT '0',
  `expcat_trash` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `expenses_category`
--

INSERT INTO `expenses_category` (`expcat_id`, `expcat_user_id`, `expcat_name`, `expcat_parent_id`, `expcat_default`, `expcat_trash`) VALUES
(1, 3, 'Разное', 0, 1, 0),
(2, 3, 'оолллд', 0, 0, 1),
(3, 3, 'kklkl', 0, 0, 0),
(4, 11, 'Разное', 0, 1, 0),
(5, 3, 'gfghhjj', 0, 0, 0),
(6, 3, 'gfghhjj', 0, 0, 0),
(7, 3, 'gfghhjj', 0, 0, 0),
(8, 3, 'gfghhjj', 0, 0, 0),
(9, 13, 'Разное', 0, 1, 0),
(10, 19, 'Разное', 0, 1, 0),
(11, 1, 'Miscellaneous', 0, 1, 0),
(12, 1, 'Офис Калнышевского', 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `expenses_vendors`
--

CREATE TABLE `expenses_vendors` (
  `vendor_id` int(11) UNSIGNED NOT NULL,
  `vendor_user_id` int(11) NOT NULL DEFAULT '0',
  `vendor_name` varchar(255) DEFAULT NULL,
  `vendor_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `exrates`
--

CREATE TABLE `exrates` (
  `exr_id` int(11) UNSIGNED NOT NULL,
  `exr_base` varchar(4) DEFAULT NULL,
  `exr_currency` varchar(4) DEFAULT NULL,
  `exr_rate` decimal(10,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `exrates`
--

INSERT INTO `exrates` (`exr_id`, `exr_base`, `exr_currency`, `exr_rate`) VALUES
(626, 'USD', 'USD', '1.0000'),
(627, 'USD', 'CAD', '1.2672'),
(628, 'USD', 'EUR', '0.8453'),
(629, 'USD', 'GBP', '0.7686'),
(630, 'USD', 'AUD', '1.2649'),
(631, 'USD', 'CHF', '0.9612'),
(632, 'USD', 'CZK', '22.1140'),
(633, 'USD', 'DKK', '6.3282'),
(634, 'USD', 'HKD', '7.8181'),
(635, 'USD', 'HUF', '257.3900'),
(636, 'USD', 'JPY', '109.0500'),
(637, 'USD', 'NOK', '7.9234'),
(638, 'USD', 'NZD', '1.3656'),
(639, 'USD', 'PLN', '3.6242'),
(640, 'USD', 'SEK', '8.1397'),
(641, 'USD', 'SGD', '1.3592'),
(642, 'USD', 'RUB', '59.8150'),
(643, 'USD', 'UAH', '25.6504'),
(644, 'USD', 'ZAR', '13.4295'),
(645, 'USD', 'CNY', '6.6611'),
(646, 'USD', 'INR', '64.1300'),
(647, 'USD', 'MXN', '17.8220'),
(648, 'USD', 'BYR', '20020.0000'),
(649, 'USD', 'KZT', '332.6600'),
(650, 'USD', 'LKR', '153.0003'),
(651, 'CAD', 'USD', '0.7891'),
(652, 'CAD', 'CAD', '1.0000'),
(653, 'CAD', 'EUR', '0.6671'),
(654, 'CAD', 'GBP', '0.6065'),
(655, 'CAD', 'AUD', '0.9982'),
(656, 'CAD', 'CHF', '0.7585'),
(657, 'CAD', 'CZK', '17.4510'),
(658, 'CAD', 'DKK', '4.9938'),
(659, 'CAD', 'HKD', '6.1696'),
(660, 'CAD', 'HUF', '203.1165'),
(661, 'CAD', 'JPY', '86.0556'),
(662, 'CAD', 'NOK', '6.2526'),
(663, 'CAD', 'NZD', '1.0777'),
(664, 'CAD', 'PLN', '2.8600'),
(665, 'CAD', 'SEK', '6.4234'),
(666, 'CAD', 'SGD', '1.0726'),
(667, 'CAD', 'RUB', '47.2023'),
(668, 'CAD', 'UAH', '20.2417'),
(669, 'CAD', 'ZAR', '10.5977'),
(670, 'CAD', 'CNY', '5.2565'),
(671, 'CAD', 'INR', '50.6075'),
(672, 'CAD', 'MXN', '14.0640'),
(673, 'CAD', 'BYR', '15798.5593'),
(674, 'CAD', 'KZT', '262.5149'),
(675, 'CAD', 'LKR', '120.7385'),
(676, 'EUR', 'USD', '1.1830'),
(677, 'EUR', 'CAD', '1.4991'),
(678, 'EUR', 'EUR', '1.0000'),
(679, 'EUR', 'GBP', '0.9092'),
(680, 'EUR', 'AUD', '1.4964'),
(681, 'EUR', 'CHF', '1.1371'),
(682, 'EUR', 'CZK', '26.1610'),
(683, 'EUR', 'DKK', '7.4863'),
(684, 'EUR', 'HKD', '9.2489'),
(685, 'EUR', 'HUF', '304.4940'),
(686, 'EUR', 'JPY', '129.0068'),
(687, 'EUR', 'NOK', '9.3734'),
(688, 'EUR', 'NZD', '1.6155'),
(689, 'EUR', 'PLN', '4.2875'),
(690, 'EUR', 'SEK', '9.6293'),
(691, 'EUR', 'SGD', '1.6079'),
(692, 'EUR', 'RUB', '70.7615'),
(693, 'EUR', 'UAH', '30.3445'),
(694, 'EUR', 'ZAR', '15.8872'),
(695, 'EUR', 'CNY', '7.8801'),
(696, 'EUR', 'INR', '75.8662'),
(697, 'EUR', 'MXN', '21.0835'),
(698, 'EUR', 'BYR', '23683.7842'),
(699, 'EUR', 'KZT', '393.5388'),
(700, 'EUR', 'LKR', '181.0004'),
(701, 'GBP', 'USD', '1.3011'),
(702, 'GBP', 'CAD', '1.6487'),
(703, 'GBP', 'EUR', '1.0998'),
(704, 'GBP', 'GBP', '1.0000'),
(705, 'GBP', 'AUD', '1.6457'),
(706, 'GBP', 'CHF', '1.2506'),
(707, 'GBP', 'CZK', '28.7722'),
(708, 'GBP', 'DKK', '8.2335'),
(709, 'GBP', 'HKD', '10.1720'),
(710, 'GBP', 'HUF', '334.8858'),
(711, 'GBP', 'JPY', '141.8831'),
(712, 'GBP', 'NOK', '10.3089'),
(713, 'GBP', 'NZD', '1.7768'),
(714, 'GBP', 'PLN', '4.7154'),
(715, 'GBP', 'SEK', '10.5904'),
(716, 'GBP', 'SGD', '1.7684'),
(717, 'GBP', 'RUB', '77.8243'),
(718, 'GBP', 'UAH', '33.3733'),
(719, 'GBP', 'ZAR', '17.4729'),
(720, 'GBP', 'CNY', '8.6667'),
(721, 'GBP', 'INR', '83.4385'),
(722, 'GBP', 'MXN', '23.1879'),
(723, 'GBP', 'BYR', '26047.6836'),
(724, 'GBP', 'KZT', '432.8183'),
(725, 'GBP', 'LKR', '199.0662'),
(726, 'AUD', 'USD', '0.7906'),
(727, 'AUD', 'CAD', '1.0018'),
(728, 'AUD', 'EUR', '0.6683'),
(729, 'AUD', 'GBP', '0.6076'),
(730, 'AUD', 'AUD', '1.0000'),
(731, 'AUD', 'CHF', '0.7599'),
(732, 'AUD', 'CZK', '17.4828'),
(733, 'AUD', 'DKK', '5.0029'),
(734, 'AUD', 'HKD', '6.1808'),
(735, 'AUD', 'HUF', '203.4858'),
(736, 'AUD', 'JPY', '86.2121'),
(737, 'AUD', 'NOK', '6.2640'),
(738, 'AUD', 'NZD', '1.0796'),
(739, 'AUD', 'PLN', '2.8652'),
(740, 'AUD', 'SEK', '6.4350'),
(741, 'AUD', 'SGD', '1.0746'),
(742, 'AUD', 'RUB', '47.2882'),
(743, 'AUD', 'UAH', '20.2785'),
(744, 'AUD', 'ZAR', '10.6170'),
(745, 'AUD', 'CNY', '5.2661'),
(746, 'AUD', 'INR', '50.6995'),
(747, 'AUD', 'MXN', '14.0896'),
(748, 'AUD', 'BYR', '15827.2854'),
(749, 'AUD', 'KZT', '262.9922'),
(750, 'AUD', 'LKR', '120.9581'),
(751, 'CHF', 'USD', '1.0404'),
(752, 'CHF', 'CAD', '1.3184'),
(753, 'CHF', 'EUR', '0.8794'),
(754, 'CHF', 'GBP', '0.7996'),
(755, 'CHF', 'AUD', '1.3160'),
(756, 'CHF', 'CHF', '1.0000'),
(757, 'CHF', 'CZK', '23.0066'),
(758, 'CHF', 'DKK', '6.5836'),
(759, 'CHF', 'HKD', '8.1337'),
(760, 'CHF', 'HUF', '267.7787'),
(761, 'CHF', 'JPY', '113.4514'),
(762, 'CHF', 'NOK', '8.2432'),
(763, 'CHF', 'NZD', '1.4207'),
(764, 'CHF', 'PLN', '3.7705'),
(765, 'CHF', 'SEK', '8.4682'),
(766, 'CHF', 'SGD', '1.4141'),
(767, 'CHF', 'RUB', '62.2292'),
(768, 'CHF', 'UAH', '26.6857'),
(769, 'CHF', 'ZAR', '13.9715'),
(770, 'CHF', 'CNY', '6.9300'),
(771, 'CHF', 'INR', '66.7184'),
(772, 'CHF', 'MXN', '18.5413'),
(773, 'CHF', 'BYR', '20828.0415'),
(774, 'CHF', 'KZT', '346.0867'),
(775, 'CHF', 'LKR', '159.1757'),
(776, 'CZK', 'USD', '0.0452'),
(777, 'CZK', 'CAD', '0.0573'),
(778, 'CZK', 'EUR', '0.0382'),
(779, 'CZK', 'GBP', '0.0348'),
(780, 'CZK', 'AUD', '0.0572'),
(781, 'CZK', 'CHF', '0.0435'),
(782, 'CZK', 'CZK', '1.0000'),
(783, 'CZK', 'DKK', '0.2862'),
(784, 'CZK', 'HKD', '0.3535'),
(785, 'CZK', 'HUF', '11.6392'),
(786, 'CZK', 'JPY', '4.9313'),
(787, 'CZK', 'NOK', '0.3583'),
(788, 'CZK', 'NZD', '0.0618'),
(789, 'CZK', 'PLN', '0.1639'),
(790, 'CZK', 'SEK', '0.3681'),
(791, 'CZK', 'SGD', '0.0615'),
(792, 'CZK', 'RUB', '2.7048'),
(793, 'CZK', 'UAH', '1.1599'),
(794, 'CZK', 'ZAR', '0.6073'),
(795, 'CZK', 'CNY', '0.3012'),
(796, 'CZK', 'INR', '2.9000'),
(797, 'CZK', 'MXN', '0.8059'),
(798, 'CZK', 'BYR', '905.3072'),
(799, 'CZK', 'KZT', '15.0429'),
(800, 'CZK', 'LKR', '6.9187'),
(801, 'DKK', 'USD', '0.1580'),
(802, 'DKK', 'CAD', '0.2002'),
(803, 'DKK', 'EUR', '0.1336'),
(804, 'DKK', 'GBP', '0.1215'),
(805, 'DKK', 'AUD', '0.1999'),
(806, 'DKK', 'CHF', '0.1519'),
(807, 'DKK', 'CZK', '3.4945'),
(808, 'DKK', 'DKK', '1.0000'),
(809, 'DKK', 'HKD', '1.2354'),
(810, 'DKK', 'HUF', '40.6735'),
(811, 'DKK', 'JPY', '17.2324'),
(812, 'DKK', 'NOK', '1.2521'),
(813, 'DKK', 'NZD', '0.2158'),
(814, 'DKK', 'PLN', '0.5727'),
(815, 'DKK', 'SEK', '1.2863'),
(816, 'DKK', 'SGD', '0.2148'),
(817, 'DKK', 'RUB', '9.4521'),
(818, 'DKK', 'UAH', '4.0533'),
(819, 'DKK', 'ZAR', '2.1222'),
(820, 'DKK', 'CNY', '1.0526'),
(821, 'DKK', 'INR', '10.1340'),
(822, 'DKK', 'MXN', '2.8163'),
(823, 'DKK', 'BYR', '3163.6148'),
(824, 'DKK', 'KZT', '52.5678'),
(825, 'DKK', 'LKR', '24.1775'),
(826, 'HKD', 'USD', '0.1279'),
(827, 'HKD', 'CAD', '0.1621'),
(828, 'HKD', 'EUR', '0.1081'),
(829, 'HKD', 'GBP', '0.0983'),
(830, 'HKD', 'AUD', '0.1618'),
(831, 'HKD', 'CHF', '0.1229'),
(832, 'HKD', 'CZK', '2.8286'),
(833, 'HKD', 'DKK', '0.8094'),
(834, 'HKD', 'HKD', '1.0000'),
(835, 'HKD', 'HUF', '32.9223'),
(836, 'HKD', 'JPY', '13.9484'),
(837, 'HKD', 'NOK', '1.0135'),
(838, 'HKD', 'NZD', '0.1747'),
(839, 'HKD', 'PLN', '0.4636'),
(840, 'HKD', 'SEK', '1.0411'),
(841, 'HKD', 'SGD', '0.1739'),
(842, 'HKD', 'RUB', '7.6508'),
(843, 'HKD', 'UAH', '3.2809'),
(844, 'HKD', 'ZAR', '1.7177'),
(845, 'HKD', 'CNY', '0.8520'),
(846, 'HKD', 'INR', '8.2028'),
(847, 'HKD', 'MXN', '2.2796'),
(848, 'HKD', 'BYR', '2560.7231'),
(849, 'HKD', 'KZT', '42.5500'),
(850, 'HKD', 'LKR', '19.5700'),
(851, 'HUF', 'USD', '0.0039'),
(852, 'HUF', 'CAD', '0.0049'),
(853, 'HUF', 'EUR', '0.0033'),
(854, 'HUF', 'GBP', '0.0030'),
(855, 'HUF', 'AUD', '0.0049'),
(856, 'HUF', 'CHF', '0.0037'),
(857, 'HUF', 'CZK', '0.0859'),
(858, 'HUF', 'DKK', '0.0246'),
(859, 'HUF', 'HKD', '0.0304'),
(860, 'HUF', 'HUF', '1.0000'),
(861, 'HUF', 'JPY', '0.4237'),
(862, 'HUF', 'NOK', '0.0308'),
(863, 'HUF', 'NZD', '0.0053'),
(864, 'HUF', 'PLN', '0.0141'),
(865, 'HUF', 'SEK', '0.0316'),
(866, 'HUF', 'SGD', '0.0053'),
(867, 'HUF', 'RUB', '0.2324'),
(868, 'HUF', 'UAH', '0.0997'),
(869, 'HUF', 'ZAR', '0.0522'),
(870, 'HUF', 'CNY', '0.0259'),
(871, 'HUF', 'INR', '0.2492'),
(872, 'HUF', 'MXN', '0.0692'),
(873, 'HUF', 'BYR', '77.7808'),
(874, 'HUF', 'KZT', '1.2924'),
(875, 'HUF', 'LKR', '0.5944'),
(876, 'JPY', 'USD', '0.0092'),
(877, 'JPY', 'CAD', '0.0116'),
(878, 'JPY', 'EUR', '0.0078'),
(879, 'JPY', 'GBP', '0.0070'),
(880, 'JPY', 'AUD', '0.0116'),
(881, 'JPY', 'CHF', '0.0088'),
(882, 'JPY', 'CZK', '0.2028'),
(883, 'JPY', 'DKK', '0.0580'),
(884, 'JPY', 'HKD', '0.0717'),
(885, 'JPY', 'HUF', '2.3603'),
(886, 'JPY', 'JPY', '1.0000'),
(887, 'JPY', 'NOK', '0.0727'),
(888, 'JPY', 'NZD', '0.0125'),
(889, 'JPY', 'PLN', '0.0332'),
(890, 'JPY', 'SEK', '0.0746'),
(891, 'JPY', 'SGD', '0.0125'),
(892, 'JPY', 'RUB', '0.5485'),
(893, 'JPY', 'UAH', '0.2352'),
(894, 'JPY', 'ZAR', '0.1232'),
(895, 'JPY', 'CNY', '0.0611'),
(896, 'JPY', 'INR', '0.5881'),
(897, 'JPY', 'MXN', '0.1634'),
(898, 'JPY', 'BYR', '183.5855'),
(899, 'JPY', 'KZT', '3.0505'),
(900, 'JPY', 'LKR', '1.4030'),
(901, 'NOK', 'USD', '0.1262'),
(902, 'NOK', 'CAD', '0.1599'),
(903, 'NOK', 'EUR', '0.1067'),
(904, 'NOK', 'GBP', '0.0970'),
(905, 'NOK', 'AUD', '0.1596'),
(906, 'NOK', 'CHF', '0.1213'),
(907, 'NOK', 'CZK', '2.7910'),
(908, 'NOK', 'DKK', '0.7987'),
(909, 'NOK', 'HKD', '0.9867'),
(910, 'NOK', 'HUF', '32.4850'),
(911, 'NOK', 'JPY', '13.7631'),
(912, 'NOK', 'NOK', '1.0000'),
(913, 'NOK', 'NZD', '0.1724'),
(914, 'NOK', 'PLN', '0.4574'),
(915, 'NOK', 'SEK', '1.0273'),
(916, 'NOK', 'SGD', '0.1715'),
(917, 'NOK', 'RUB', '7.5492'),
(918, 'NOK', 'UAH', '3.2373'),
(919, 'NOK', 'ZAR', '1.6949'),
(920, 'NOK', 'CNY', '0.8407'),
(921, 'NOK', 'INR', '8.0938'),
(922, 'NOK', 'MXN', '2.2493'),
(923, 'NOK', 'BYR', '2526.7089'),
(924, 'NOK', 'KZT', '41.9848'),
(925, 'NOK', 'LKR', '19.3101'),
(926, 'NZD', 'USD', '0.7323'),
(927, 'NZD', 'CAD', '0.9279'),
(928, 'NZD', 'EUR', '0.6190'),
(929, 'NZD', 'GBP', '0.5628'),
(930, 'NZD', 'AUD', '0.9263'),
(931, 'NZD', 'CHF', '0.7039'),
(932, 'NZD', 'CZK', '16.1936'),
(933, 'NZD', 'DKK', '4.6340'),
(934, 'NZD', 'HKD', '5.7250'),
(935, 'NZD', 'HUF', '188.4807'),
(936, 'NZD', 'JPY', '79.8548'),
(937, 'NZD', 'NOK', '5.8021'),
(938, 'NZD', 'NZD', '1.0000'),
(939, 'NZD', 'PLN', '2.6539'),
(940, 'NZD', 'SEK', '5.9605'),
(941, 'NZD', 'SGD', '0.9953'),
(942, 'NZD', 'RUB', '43.8011'),
(943, 'NZD', 'UAH', '18.7832'),
(944, 'NZD', 'ZAR', '9.8341'),
(945, 'NZD', 'CNY', '4.8778'),
(946, 'NZD', 'INR', '46.9609'),
(947, 'NZD', 'MXN', '13.0506'),
(948, 'NZD', 'BYR', '14660.1803'),
(949, 'NZD', 'KZT', '243.5992'),
(950, 'NZD', 'LKR', '112.0386'),
(951, 'PLN', 'USD', '0.2759'),
(952, 'PLN', 'CAD', '0.3497'),
(953, 'PLN', 'EUR', '0.2332'),
(954, 'PLN', 'GBP', '0.2121'),
(955, 'PLN', 'AUD', '0.3490'),
(956, 'PLN', 'CHF', '0.2652'),
(957, 'PLN', 'CZK', '6.1018'),
(958, 'PLN', 'DKK', '1.7461'),
(959, 'PLN', 'HKD', '2.1572'),
(960, 'PLN', 'HUF', '71.0197'),
(961, 'PLN', 'JPY', '30.0894'),
(962, 'PLN', 'NOK', '2.1862'),
(963, 'PLN', 'NZD', '0.3768'),
(964, 'PLN', 'PLN', '1.0000'),
(965, 'PLN', 'SEK', '2.2459'),
(966, 'PLN', 'SGD', '0.3750'),
(967, 'PLN', 'RUB', '16.5043'),
(968, 'PLN', 'UAH', '7.0775'),
(969, 'PLN', 'ZAR', '3.7055'),
(970, 'PLN', 'CNY', '1.8380'),
(971, 'PLN', 'INR', '17.6949'),
(972, 'PLN', 'MXN', '4.9175'),
(973, 'PLN', 'BYR', '5523.9717'),
(974, 'PLN', 'KZT', '91.7884'),
(975, 'PLN', 'LKR', '42.2163'),
(976, 'SEK', 'USD', '0.1229'),
(977, 'SEK', 'CAD', '0.1557'),
(978, 'SEK', 'EUR', '0.1038'),
(979, 'SEK', 'GBP', '0.0944'),
(980, 'SEK', 'AUD', '0.1554'),
(981, 'SEK', 'CHF', '0.1181'),
(982, 'SEK', 'CZK', '2.7168'),
(983, 'SEK', 'DKK', '0.7774'),
(984, 'SEK', 'HKD', '0.9605'),
(985, 'SEK', 'HUF', '31.6215'),
(986, 'SEK', 'JPY', '13.3973'),
(987, 'SEK', 'NOK', '0.9734'),
(988, 'SEK', 'NZD', '0.1678'),
(989, 'SEK', 'PLN', '0.4453'),
(990, 'SEK', 'SEK', '1.0000'),
(991, 'SEK', 'SGD', '0.1670'),
(992, 'SEK', 'RUB', '7.3485'),
(993, 'SEK', 'UAH', '3.1513'),
(994, 'SEK', 'ZAR', '1.6499'),
(995, 'SEK', 'CNY', '0.8183'),
(996, 'SEK', 'INR', '7.8787'),
(997, 'SEK', 'MXN', '2.1895'),
(998, 'SEK', 'BYR', '2459.5489'),
(999, 'SEK', 'KZT', '40.8688'),
(1000, 'SEK', 'LKR', '18.7968'),
(1001, 'SGD', 'USD', '0.7357'),
(1002, 'SGD', 'CAD', '0.9323'),
(1003, 'SGD', 'EUR', '0.6219'),
(1004, 'SGD', 'GBP', '0.5655'),
(1005, 'SGD', 'AUD', '0.9306'),
(1006, 'SGD', 'CHF', '0.7072'),
(1007, 'SGD', 'CZK', '16.2698'),
(1008, 'SGD', 'DKK', '4.6558'),
(1009, 'SGD', 'HKD', '5.7520'),
(1010, 'SGD', 'HUF', '189.3682'),
(1011, 'SGD', 'JPY', '80.2308'),
(1012, 'SGD', 'NOK', '5.8294'),
(1013, 'SGD', 'NZD', '1.0047'),
(1014, 'SGD', 'PLN', '2.6664'),
(1015, 'SGD', 'SEK', '5.9886'),
(1016, 'SGD', 'SGD', '1.0000'),
(1017, 'SGD', 'RUB', '44.0074'),
(1018, 'SGD', 'UAH', '18.8716'),
(1019, 'SGD', 'ZAR', '9.8804'),
(1020, 'SGD', 'CNY', '4.9007'),
(1021, 'SGD', 'INR', '47.1820'),
(1022, 'SGD', 'MXN', '13.1121'),
(1023, 'SGD', 'BYR', '14729.2103'),
(1024, 'SGD', 'KZT', '244.7462'),
(1025, 'SGD', 'LKR', '112.5661'),
(1026, 'RUB', 'USD', '0.0167'),
(1027, 'RUB', 'CAD', '0.0212'),
(1028, 'RUB', 'EUR', '0.0141'),
(1029, 'RUB', 'GBP', '0.0128'),
(1030, 'RUB', 'AUD', '0.0211'),
(1031, 'RUB', 'CHF', '0.0161'),
(1032, 'RUB', 'CZK', '0.3697'),
(1033, 'RUB', 'DKK', '0.1058'),
(1034, 'RUB', 'HKD', '0.1307'),
(1035, 'RUB', 'HUF', '4.3031'),
(1036, 'RUB', 'JPY', '1.8231'),
(1037, 'RUB', 'NOK', '0.1325'),
(1038, 'RUB', 'NZD', '0.0228'),
(1039, 'RUB', 'PLN', '0.0606'),
(1040, 'RUB', 'SEK', '0.1361'),
(1041, 'RUB', 'SGD', '0.0227'),
(1042, 'RUB', 'RUB', '1.0000'),
(1043, 'RUB', 'UAH', '0.4288'),
(1044, 'RUB', 'ZAR', '0.2245'),
(1045, 'RUB', 'CNY', '0.1114'),
(1046, 'RUB', 'INR', '1.0721'),
(1047, 'RUB', 'MXN', '0.2980'),
(1048, 'RUB', 'BYR', '334.6987'),
(1049, 'RUB', 'KZT', '5.5615'),
(1050, 'RUB', 'LKR', '2.5579'),
(1051, 'UAH', 'USD', '0.0390'),
(1052, 'UAH', 'CAD', '0.0494'),
(1053, 'UAH', 'EUR', '0.0330'),
(1054, 'UAH', 'GBP', '0.0300'),
(1055, 'UAH', 'AUD', '0.0493'),
(1056, 'UAH', 'CHF', '0.0375'),
(1057, 'UAH', 'CZK', '0.8621'),
(1058, 'UAH', 'DKK', '0.2467'),
(1059, 'UAH', 'HKD', '0.3048'),
(1060, 'UAH', 'HUF', '10.0346'),
(1061, 'UAH', 'JPY', '4.2514'),
(1062, 'UAH', 'NOK', '0.3089'),
(1063, 'UAH', 'NZD', '0.0532'),
(1064, 'UAH', 'PLN', '0.1413'),
(1065, 'UAH', 'SEK', '0.3173'),
(1066, 'UAH', 'SGD', '0.0530'),
(1067, 'UAH', 'RUB', '2.3319'),
(1068, 'UAH', 'UAH', '1.0000'),
(1069, 'UAH', 'ZAR', '0.5236'),
(1070, 'UAH', 'CNY', '0.2597'),
(1071, 'UAH', 'INR', '2.5002'),
(1072, 'UAH', 'MXN', '0.6948'),
(1073, 'UAH', 'BYR', '780.4956'),
(1074, 'UAH', 'KZT', '12.9690'),
(1075, 'UAH', 'LKR', '5.9648'),
(1076, 'ZAR', 'USD', '0.0745'),
(1077, 'ZAR', 'CAD', '0.0944'),
(1078, 'ZAR', 'EUR', '0.0629'),
(1079, 'ZAR', 'GBP', '0.0572'),
(1080, 'ZAR', 'AUD', '0.0942'),
(1081, 'ZAR', 'CHF', '0.0716'),
(1082, 'ZAR', 'CZK', '1.6467'),
(1083, 'ZAR', 'DKK', '0.4712'),
(1084, 'ZAR', 'HKD', '0.5822'),
(1085, 'ZAR', 'HUF', '19.1660'),
(1086, 'ZAR', 'JPY', '8.1202'),
(1087, 'ZAR', 'NOK', '0.5900'),
(1088, 'ZAR', 'NZD', '0.1017'),
(1089, 'ZAR', 'PLN', '0.2699'),
(1090, 'ZAR', 'SEK', '0.6061'),
(1091, 'ZAR', 'SGD', '0.1012'),
(1092, 'ZAR', 'RUB', '4.4540'),
(1093, 'ZAR', 'UAH', '1.9100'),
(1094, 'ZAR', 'ZAR', '1.0000'),
(1095, 'ZAR', 'CNY', '0.4960'),
(1096, 'ZAR', 'INR', '4.7753'),
(1097, 'ZAR', 'MXN', '1.3271'),
(1098, 'ZAR', 'BYR', '1490.7476'),
(1099, 'ZAR', 'KZT', '24.7708'),
(1100, 'ZAR', 'LKR', '11.3929'),
(1101, 'CNY', 'USD', '0.1501'),
(1102, 'CNY', 'CAD', '0.1902'),
(1103, 'CNY', 'EUR', '0.1269'),
(1104, 'CNY', 'GBP', '0.1154'),
(1105, 'CNY', 'AUD', '0.1899'),
(1106, 'CNY', 'CHF', '0.1443'),
(1107, 'CNY', 'CZK', '3.3199'),
(1108, 'CNY', 'DKK', '0.9500'),
(1109, 'CNY', 'HKD', '1.1737'),
(1110, 'CNY', 'HUF', '38.6407'),
(1111, 'CNY', 'JPY', '16.3712'),
(1112, 'CNY', 'NOK', '1.1895'),
(1113, 'CNY', 'NZD', '0.2050'),
(1114, 'CNY', 'PLN', '0.5441'),
(1115, 'CNY', 'SEK', '1.2220'),
(1116, 'CNY', 'SGD', '0.2041'),
(1117, 'CNY', 'RUB', '8.9797'),
(1118, 'CNY', 'UAH', '3.8508'),
(1119, 'CNY', 'ZAR', '2.0161'),
(1120, 'CNY', 'CNY', '1.0000'),
(1121, 'CNY', 'INR', '9.6275'),
(1122, 'CNY', 'MXN', '2.6755'),
(1123, 'CNY', 'BYR', '3005.5077'),
(1124, 'CNY', 'KZT', '49.9407'),
(1125, 'CNY', 'LKR', '22.9692'),
(1126, 'INR', 'USD', '0.0156'),
(1127, 'INR', 'CAD', '0.0198'),
(1128, 'INR', 'EUR', '0.0132'),
(1129, 'INR', 'GBP', '0.0120'),
(1130, 'INR', 'AUD', '0.0197'),
(1131, 'INR', 'CHF', '0.0150'),
(1132, 'INR', 'CZK', '0.3448'),
(1133, 'INR', 'DKK', '0.0987'),
(1134, 'INR', 'HKD', '0.1219'),
(1135, 'INR', 'HUF', '4.0136'),
(1136, 'INR', 'JPY', '1.7005'),
(1137, 'INR', 'NOK', '0.1236'),
(1138, 'INR', 'NZD', '0.0213'),
(1139, 'INR', 'PLN', '0.0565'),
(1140, 'INR', 'SEK', '0.1269'),
(1141, 'INR', 'SGD', '0.0212'),
(1142, 'INR', 'RUB', '0.9327'),
(1143, 'INR', 'UAH', '0.4000'),
(1144, 'INR', 'ZAR', '0.2094'),
(1145, 'INR', 'CNY', '0.1039'),
(1146, 'INR', 'INR', '1.0000'),
(1147, 'INR', 'MXN', '0.2779'),
(1148, 'INR', 'BYR', '312.1784'),
(1149, 'INR', 'KZT', '5.1873'),
(1150, 'INR', 'LKR', '2.3858'),
(1151, 'MXN', 'USD', '0.0561'),
(1152, 'MXN', 'CAD', '0.0711'),
(1153, 'MXN', 'EUR', '0.0474'),
(1154, 'MXN', 'GBP', '0.0431'),
(1155, 'MXN', 'AUD', '0.0710'),
(1156, 'MXN', 'CHF', '0.0539'),
(1157, 'MXN', 'CZK', '1.2408'),
(1158, 'MXN', 'DKK', '0.3551'),
(1159, 'MXN', 'HKD', '0.4387'),
(1160, 'MXN', 'HUF', '14.4423'),
(1161, 'MXN', 'JPY', '6.1188'),
(1162, 'MXN', 'NOK', '0.4446'),
(1163, 'MXN', 'NZD', '0.0766'),
(1164, 'MXN', 'PLN', '0.2034'),
(1165, 'MXN', 'SEK', '0.4567'),
(1166, 'MXN', 'SGD', '0.0763'),
(1167, 'MXN', 'RUB', '3.3562'),
(1168, 'MXN', 'UAH', '1.4393'),
(1169, 'MXN', 'ZAR', '0.7535'),
(1170, 'MXN', 'CNY', '0.3738'),
(1171, 'MXN', 'INR', '3.5984'),
(1172, 'MXN', 'MXN', '1.0000'),
(1173, 'MXN', 'BYR', '1123.3307'),
(1174, 'MXN', 'KZT', '18.6657'),
(1175, 'MXN', 'LKR', '8.5849'),
(1176, 'BYR', 'USD', '0.0001'),
(1177, 'BYR', 'CAD', '0.0001'),
(1178, 'BYR', 'EUR', '0.0000'),
(1179, 'BYR', 'GBP', '0.0000'),
(1180, 'BYR', 'AUD', '0.0001'),
(1181, 'BYR', 'CHF', '0.0000'),
(1182, 'BYR', 'CZK', '0.0011'),
(1183, 'BYR', 'DKK', '0.0003'),
(1184, 'BYR', 'HKD', '0.0004'),
(1185, 'BYR', 'HUF', '0.0129'),
(1186, 'BYR', 'JPY', '0.0054'),
(1187, 'BYR', 'NOK', '0.0004'),
(1188, 'BYR', 'NZD', '0.0001'),
(1189, 'BYR', 'PLN', '0.0002'),
(1190, 'BYR', 'SEK', '0.0004'),
(1191, 'BYR', 'SGD', '0.0001'),
(1192, 'BYR', 'RUB', '0.0030'),
(1193, 'BYR', 'UAH', '0.0013'),
(1194, 'BYR', 'ZAR', '0.0007'),
(1195, 'BYR', 'CNY', '0.0003'),
(1196, 'BYR', 'INR', '0.0032'),
(1197, 'BYR', 'MXN', '0.0009'),
(1198, 'BYR', 'BYR', '1.0000'),
(1199, 'BYR', 'KZT', '0.0166'),
(1200, 'BYR', 'LKR', '0.0076'),
(1201, 'KZT', 'USD', '0.0030'),
(1202, 'KZT', 'CAD', '0.0038'),
(1203, 'KZT', 'EUR', '0.0025'),
(1204, 'KZT', 'GBP', '0.0023'),
(1205, 'KZT', 'AUD', '0.0038'),
(1206, 'KZT', 'CHF', '0.0029'),
(1207, 'KZT', 'CZK', '0.0665'),
(1208, 'KZT', 'DKK', '0.0190'),
(1209, 'KZT', 'HKD', '0.0235'),
(1210, 'KZT', 'HUF', '0.7737'),
(1211, 'KZT', 'JPY', '0.3278'),
(1212, 'KZT', 'NOK', '0.0238'),
(1213, 'KZT', 'NZD', '0.0041'),
(1214, 'KZT', 'PLN', '0.0109'),
(1215, 'KZT', 'SEK', '0.0245'),
(1216, 'KZT', 'SGD', '0.0041'),
(1217, 'KZT', 'RUB', '0.1798'),
(1218, 'KZT', 'UAH', '0.0771'),
(1219, 'KZT', 'ZAR', '0.0404'),
(1220, 'KZT', 'CNY', '0.0200'),
(1221, 'KZT', 'INR', '0.1928'),
(1222, 'KZT', 'MXN', '0.0536'),
(1223, 'KZT', 'BYR', '60.1816'),
(1224, 'KZT', 'KZT', '1.0000'),
(1225, 'KZT', 'LKR', '0.4599'),
(1226, 'LKR', 'USD', '0.0065'),
(1227, 'LKR', 'CAD', '0.0083'),
(1228, 'LKR', 'EUR', '0.0055'),
(1229, 'LKR', 'GBP', '0.0050'),
(1230, 'LKR', 'AUD', '0.0083'),
(1231, 'LKR', 'CHF', '0.0063'),
(1232, 'LKR', 'CZK', '0.1445'),
(1233, 'LKR', 'DKK', '0.0414'),
(1234, 'LKR', 'HKD', '0.0511'),
(1235, 'LKR', 'HUF', '1.6823'),
(1236, 'LKR', 'JPY', '0.7127'),
(1237, 'LKR', 'NOK', '0.0518'),
(1238, 'LKR', 'NZD', '0.0089'),
(1239, 'LKR', 'PLN', '0.0237'),
(1240, 'LKR', 'SEK', '0.0532'),
(1241, 'LKR', 'SGD', '0.0089'),
(1242, 'LKR', 'RUB', '0.3909'),
(1243, 'LKR', 'UAH', '0.1676'),
(1244, 'LKR', 'ZAR', '0.0878'),
(1245, 'LKR', 'CNY', '0.0435'),
(1246, 'LKR', 'INR', '0.4191'),
(1247, 'LKR', 'MXN', '0.1165'),
(1248, 'LKR', 'BYR', '130.8494'),
(1249, 'LKR', 'KZT', '2.1742'),
(1250, 'LKR', 'LKR', '1.0000');

-- --------------------------------------------------------

--
-- Структура таблицы `help_docs`
--

CREATE TABLE `help_docs` (
  `hdoc_id` int(11) UNSIGNED NOT NULL,
  `hdoc_subj` varchar(255) DEFAULT NULL,
  `hdoc_text` text,
  `hdoc_cat` int(11) NOT NULL DEFAULT '0',
  `hdoc_publish` tinyint(1) NOT NULL DEFAULT '0',
  `hdoc_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `help_docs`
--

INSERT INTO `help_docs` (`hdoc_id`, `hdoc_subj`, `hdoc_text`, `hdoc_cat`, `hdoc_publish`, `hdoc_trash`) VALUES
(1, 'asdasas', '<p>asdasd</p>', 5, 0, 1),
(2, 'Clients', '<p><img src=\"http://doc.floctopus.info/redactor/t_da1d15045388ac91c1caee24f7570436.jpeg\" class=\"img-responsive\"></p><p><br></p><p>Hokkey!!! </p>', 5, 1, 1),
(3, 'How to add new client', '<p>To create new client you have to go People-&gt;Clients.</p><p>On Clients page click on \"New Client\" button (Fig. 1 )</p><p><img src=\"https://doc.floctopus.com/redactor/t_b738a7e487f3a3deee5237cb1d14775d.png\" class=\"img-responsive\" alt=\"\" style=\"display: block; margin: auto;\"></p><p style=\"text-align: center;\">Fig. 1</p><p>There are two sections on  \"New Client\" page: \"Details\" and \"Contacts\". There is just one required field marked with red star ( fig.2)</p><p><img src=\"https://doc.floctopus.com/redactor/67e6f3f7f2517a900989097507926401.png\" class=\"img-responsive\" alt=\"\" style=\"display: block; margin: auto;\"></p><p>Fill out as many fields as you need and click on \"Save\" button. After form validation all entered information will be stored in database and you will be redirected to \"Clients List\" page.</p><p><img src=\"http://doc.floctopus.info/redactor/t_77cd217f5cf6345915a062985b20a6ef.png\" class=\"img-responsive\"></p><p><br></p>', 6, 1, 0),
(4, 'asdasd', '', 5, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `help_docs_category`
--

CREATE TABLE `help_docs_category` (
  `hdoccat_id` int(11) UNSIGNED NOT NULL,
  `hdoccat_name` varchar(255) DEFAULT NULL,
  `hdoccat_desc` text,
  `hdoccat_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `help_docs_category`
--

INSERT INTO `help_docs_category` (`hdoccat_id`, `hdoccat_name`, `hdoccat_desc`, `hdoccat_trash`) VALUES
(1, 'People', '', 1),
(2, 'Services', '', 1),
(3, 'Portfolio', 'Portfilio Management Module', 0),
(4, 'Invoices', 'Invoices Management Module', 0),
(5, 'Estimates', 'Estimate Management Module', 0),
(6, 'People', 'Clients, Collaborates, Invites', 0),
(7, 'Clients', 'Collaborates Management Module', 1),
(8, 'Finance', 'Expenses, Transactions', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `invites`
--

CREATE TABLE `invites` (
  `invite_id` int(11) UNSIGNED NOT NULL,
  `invite_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `invite_from` int(11) DEFAULT NULL,
  `invite_email` varchar(200) DEFAULT NULL,
  `invite_key` varchar(200) DEFAULT NULL,
  `invite_status` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `invites`
--

INSERT INTO `invites` (`invite_id`, `invite_date`, `invite_from`, `invite_email`, `invite_key`, `invite_status`) VALUES
(1, '2017-10-14 22:21:15', 1, 'sgoloto@ukr.net', '59e28ddb525f2', 1),
(2, '2017-10-20 12:41:21', 1, 'a.spassky@alliancepack.com.ua', '59e9eef09fefe', 2),
(3, '2017-10-29 11:53:10', 1, 'lawnpaul@gmail.com', '59f5c12668c4b', 1),
(4, '2017-10-29 17:06:19', 1, 'sergeyshamshin94@gmail.com', '59f60a8b4d111', 2),
(5, '2017-10-29 17:06:59', 1, 'support@managemart.com', '59f60ab2a3f1c', 1),
(6, '2017-10-31 07:00:20', 11, 'floct.project@gmail.com', '59f81f83bee0d', 2),
(7, '2017-10-31 07:31:47', 11, 'prmuseforyouth@gmail.com', '59f826e3799ff', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `invoices`
--

CREATE TABLE `invoices` (
  `inv_id` int(11) NOT NULL,
  `inv_user_id` int(11) NOT NULL DEFAULT '0',
  `inv_client_id` int(11) NOT NULL DEFAULT '0',
  `inv_collab_id` int(11) NOT NULL DEFAULT '0',
  `inv_date` date DEFAULT NULL,
  `inv_num` int(11) NOT NULL DEFAULT '0',
  `inv_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_street` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_zip` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_country` varchar(3) CHARACTER SET utf8 NOT NULL DEFAULT 'US',
  `inv_discount_percentage` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_discount_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_subtotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_taxes_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_total_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_paid_amount_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_total_due` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_total_due_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_recurring` int(11) NOT NULL DEFAULT '0',
  `inv_attach` tinyint(1) NOT NULL DEFAULT '0',
  `inv_status` tinyint(2) NOT NULL DEFAULT '1',
  `inv_sent_email` tinyint(1) NOT NULL DEFAULT '0',
  `inv_arc` tinyint(1) NOT NULL DEFAULT '0',
  `inv_currency` varchar(4) CHARACTER SET utf8 NOT NULL DEFAULT 'USD',
  `inv_tags` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_prj` int(11) NOT NULL DEFAULT '0',
  `inv_exchange_rate` decimal(10,4) NOT NULL DEFAULT '1.0000',
  `inv_terms` text CHARACTER SET utf8,
  `inv_notes` text CHARACTER SET utf8,
  `inv_view` tinyint(1) NOT NULL DEFAULT '0',
  `inv_lang` varchar(4) NOT NULL DEFAULT 'en',
  `inv_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `invoices`
--

INSERT INTO `invoices` (`inv_id`, `inv_user_id`, `inv_client_id`, `inv_collab_id`, `inv_date`, `inv_num`, `inv_name`, `inv_street`, `inv_city`, `inv_state`, `inv_zip`, `inv_country`, `inv_discount_percentage`, `inv_discount_cash`, `inv_subtotal`, `inv_taxes_cash`, `inv_total`, `inv_total_base`, `inv_paid_amount`, `inv_paid_amount_base`, `inv_total_due`, `inv_total_due_base`, `inv_recurring`, `inv_attach`, `inv_status`, `inv_sent_email`, `inv_arc`, `inv_currency`, `inv_tags`, `inv_prj`, `inv_exchange_rate`, `inv_terms`, `inv_notes`, `inv_view`, `inv_lang`, `inv_trash`) VALUES
(1, 1, 12, 0, '2017-10-29', 1, 'нака', '', 'Berlin', '', '', 'DE', '0.00', '0.00', '123.00', '0.00', '123.00', '123.00', '0.00', '0.00', '123.00', '123.00', 0, 0, 1, 0, 0, 'USD', 'нака 1 123.00', 0, '1.0000', '', '', 0, 'en', 0),
(2, 1, 4, 0, '2017-10-29', 2, 'Jack Ermolenko', 'Малиновского 27/23 КВ 402', 'Киев', '', '10000', 'UA', '10.00', '2.30', '23.00', '0.00', '20.70', '20.70', '0.00', '0.00', '20.70', '20.70', 0, 0, 1, 0, 0, 'USD', 'Jack Ermolenko 2 20.70', 0, '1.0000', '', '', 0, 'en', 0),
(3, 1, 0, 60, '2017-10-29', 3, 'Anna Kramar', '', '', '', '', 'UA', '0.00', '0.00', '23.00', '0.00', '23.00', '23.00', '0.00', '0.00', '23.00', '23.00', 0, 0, 1, 0, 0, 'USD', 'Anna Kramar 3 23.00', 0, '1.0000', '', '', 0, 'en', 0),
(4, 1, 13, 0, '2017-10-29', 4, 'ertretter', '', '', 'AK', '', 'US', '0.00', '0.00', '23.00', '0.00', '23.00', '23.00', '0.00', '0.00', '23.00', '23.00', 0, 0, 1, 0, 0, 'USD', 'ertretter 4 23.00', 0, '1.0000', '', '', 0, 'en', 0),
(5, 1, 13, 0, '2017-10-29', 5, 'ertretter', '', '', 'AK', '', 'US', '0.00', '0.00', '23.00', '0.00', '23.00', '23.00', '0.00', '0.00', '23.00', '23.00', 0, 0, 1, 0, 0, 'USD', 'ertretter 5 23.00', 0, '1.0000', '', '', 0, 'en', 0),
(6, 16, 0, 70, '2017-10-31', 1, 'Mila Tretyak', '', '', '', '', 'US', '0.00', '0.00', '100.00', '5.00', '105.00', '105.00', '0.00', '0.00', '105.00', '105.00', 0, 0, 5, 1, 0, 'USD', 'Mila Tretyak 1 105.00', 0, '1.0000', 'пропро', '1212121', 0, 'ru', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `invoices_attach`
--

CREATE TABLE `invoices_attach` (
  `invatt_id` int(11) UNSIGNED NOT NULL,
  `invatt_user_id` int(11) NOT NULL DEFAULT '0',
  `invatt_inv_id` int(11) NOT NULL DEFAULT '0',
  `invatt_fname` varchar(255) DEFAULT NULL,
  `invatt_type` varchar(100) DEFAULT NULL,
  `invatt_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `invoices_attach_recurring`
--

CREATE TABLE `invoices_attach_recurring` (
  `invatt_id` int(11) UNSIGNED NOT NULL,
  `invatt_user_id` int(11) NOT NULL DEFAULT '0',
  `invatt_inv_id` int(11) NOT NULL DEFAULT '0',
  `invatt_fname` varchar(255) DEFAULT NULL,
  `invatt_type` varchar(100) DEFAULT NULL,
  `invatt_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `invoices_lines`
--

CREATE TABLE `invoices_lines` (
  `invline_id` int(11) NOT NULL,
  `invline_user_id` int(11) NOT NULL DEFAULT '0',
  `invline_inv_id` int(11) NOT NULL DEFAULT '0',
  `invline_srv` varchar(255) DEFAULT NULL,
  `invline_srv_id` int(11) NOT NULL DEFAULT '0',
  `invline_desc` text,
  `invline_rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_tax1_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_tax1_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_tax2_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_tax2_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `invoices_lines`
--

INSERT INTO `invoices_lines` (`invline_id`, `invline_user_id`, `invline_inv_id`, `invline_srv`, `invline_srv_id`, `invline_desc`, `invline_rate`, `invline_qty`, `invline_tax1_percent`, `invline_tax1_cash`, `invline_tax2_percent`, `invline_tax2_cash`, `invline_total`, `invline_trash`) VALUES
(1, 1, 1, 'ff', 10, '', '123.00', '1.00', '0.00', '0.00', '0.00', '0.00', '123.00', 0),
(2, 1, 2, 'dd', 9, '', '23.00', '1.00', '0.00', '0.00', '0.00', '0.00', '23.00', 0),
(3, 1, 3, 'ggg', 11, 'f', '23.00', '1.00', '0.00', '0.00', '0.00', '0.00', '23.00', 0),
(4, 1, 4, 'ggg', 11, 'f', '23.00', '1.00', '0.00', '0.00', '0.00', '0.00', '23.00', 0),
(5, 1, 5, 'ggg', 11, 'f', '23.00', '1.00', '0.00', '0.00', '0.00', '0.00', '23.00', 0),
(6, 16, 6, 'создание брифа', 12, '', '100.00', '1.00', '5.00', '5.00', '0.00', '0.00', '100.00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `invoices_lines_recurring`
--

CREATE TABLE `invoices_lines_recurring` (
  `invline_id` int(11) NOT NULL,
  `invline_user_id` int(11) NOT NULL DEFAULT '0',
  `invline_inv_id` int(11) NOT NULL DEFAULT '0',
  `invline_srv` varchar(255) DEFAULT NULL,
  `invline_srv_id` int(11) NOT NULL DEFAULT '0',
  `invline_desc` text,
  `invline_rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_tax1_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_tax1_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_tax2_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_tax2_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invline_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `invoices_lines_recurring`
--

INSERT INTO `invoices_lines_recurring` (`invline_id`, `invline_user_id`, `invline_inv_id`, `invline_srv`, `invline_srv_id`, `invline_desc`, `invline_rate`, `invline_qty`, `invline_tax1_percent`, `invline_tax1_cash`, `invline_tax2_percent`, `invline_tax2_cash`, `invline_total`, `invline_trash`) VALUES
(1, 1, 1, 'dd', 9, '', '23.00', '1.00', '0.00', '0.00', '0.00', '0.00', '23.00', 0),
(2, 1, 2, 'ff', 10, '', '123.00', '1.00', '0.00', '0.00', '0.00', '0.00', '123.00', 0),
(3, 1, 3, 'dd', 9, '', '23.00', '1.00', '0.00', '0.00', '0.00', '0.00', '23.00', 0),
(4, 1, 4, 'ggg', 11, 'f', '23.00', '1.00', '0.00', '0.00', '0.00', '0.00', '23.00', 0),
(5, 1, 5, 'ggg', 11, 'f', '23.00', '1.00', '0.00', '0.00', '0.00', '0.00', '23.00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `invoices_recurring`
--

CREATE TABLE `invoices_recurring` (
  `inv_id` int(11) NOT NULL,
  `inv_user_id` int(11) NOT NULL DEFAULT '0',
  `inv_client_id` int(11) NOT NULL DEFAULT '0',
  `inv_collab_id` int(11) NOT NULL DEFAULT '0',
  `inv_date` date DEFAULT NULL,
  `inv_num` int(11) NOT NULL DEFAULT '0',
  `inv_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_street` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_zip` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_country` varchar(3) CHARACTER SET utf8 NOT NULL DEFAULT 'US',
  `inv_discount_percentage` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_discount_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_subtotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_taxes_cash` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_total_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_paid_amount_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_total_due` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_total_due_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inv_recurring` int(11) NOT NULL DEFAULT '0',
  `inv_attach` tinyint(1) NOT NULL DEFAULT '0',
  `inv_status` tinyint(2) NOT NULL DEFAULT '1',
  `inv_sent_email` tinyint(1) NOT NULL DEFAULT '0',
  `inv_arc` tinyint(1) NOT NULL DEFAULT '0',
  `inv_currency` varchar(4) CHARACTER SET utf8 NOT NULL DEFAULT 'USD',
  `inv_tags` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `inv_prj` int(11) NOT NULL DEFAULT '0',
  `inv_exchange_rate` decimal(10,4) NOT NULL DEFAULT '1.0000',
  `inv_terms` text CHARACTER SET utf8,
  `inv_notes` text CHARACTER SET utf8,
  `inv_view` tinyint(1) NOT NULL DEFAULT '0',
  `inv_lang` varchar(4) NOT NULL DEFAULT 'en',
  `inv_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `invoices_recurring`
--

INSERT INTO `invoices_recurring` (`inv_id`, `inv_user_id`, `inv_client_id`, `inv_collab_id`, `inv_date`, `inv_num`, `inv_name`, `inv_street`, `inv_city`, `inv_state`, `inv_zip`, `inv_country`, `inv_discount_percentage`, `inv_discount_cash`, `inv_subtotal`, `inv_taxes_cash`, `inv_total`, `inv_total_base`, `inv_paid_amount`, `inv_paid_amount_base`, `inv_total_due`, `inv_total_due_base`, `inv_recurring`, `inv_attach`, `inv_status`, `inv_sent_email`, `inv_arc`, `inv_currency`, `inv_tags`, `inv_prj`, `inv_exchange_rate`, `inv_terms`, `inv_notes`, `inv_view`, `inv_lang`, `inv_trash`) VALUES
(1, 1, 12, 0, '2017-10-29', 9, 'нака', '', 'Berlin', '', '', 'DE', '0.00', '0.00', '23.00', '0.00', '23.00', '23.00', '0.00', '0.00', '23.00', '23.00', 0, 0, 1, 0, 0, 'USD', 'нака 9 23.00', 0, '1.0000', '', '', 0, 'en', 0),
(2, 1, 12, 0, '2017-10-29', 1, 'нака', '', 'Berlin', '', '', 'DE', '0.00', '0.00', '123.00', '0.00', '123.00', '123.00', '0.00', '0.00', '123.00', '123.00', 0, 0, 1, 0, 0, 'USD', 'нака 1 123.00', 0, '1.0000', '', '', 0, 'en', 0),
(3, 1, 4, 0, '2017-10-29', 2, 'Jack Ermolenko', 'Малиновского 27/23 КВ 402', 'Киев', '', '10000', 'UA', '10.00', '2.30', '23.00', '0.00', '20.70', '20.70', '0.00', '0.00', '20.70', '20.70', 0, 0, 1, 0, 0, 'USD', 'Jack Ermolenko 2 20.70', 0, '1.0000', '', '', 0, 'en', 0),
(4, 1, 0, 60, '2017-10-29', 3, 'Anna Kramar', '', '', '', '', 'UA', '0.00', '0.00', '23.00', '0.00', '23.00', '23.00', '0.00', '0.00', '23.00', '23.00', 0, 0, 1, 0, 0, 'USD', 'Anna Kramar 3 23.00', 0, '1.0000', '', '', 0, 'en', 0),
(5, 1, 13, 0, '2017-10-29', 5, 'ertretter', '', '', 'AK', '', 'US', '0.00', '0.00', '23.00', '0.00', '23.00', '23.00', '0.00', '0.00', '23.00', '23.00', 0, 0, 1, 0, 0, 'USD', 'ertretter 5 23.00', 0, '1.0000', '', '', 0, 'en', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `invoices_statuses`
--

CREATE TABLE `invoices_statuses` (
  `invstatus_id` int(11) UNSIGNED NOT NULL,
  `invstatus_status` tinyint(4) DEFAULT NULL,
  `invstatus_name` varchar(255) DEFAULT NULL,
  `invstatus_lang` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `invoices_statuses`
--

INSERT INTO `invoices_statuses` (`invstatus_id`, `invstatus_status`, `invstatus_name`, `invstatus_lang`) VALUES
(1, 1, 'Draft', 'en'),
(2, 2, 'Paid', 'en'),
(3, 3, 'Partial', 'en'),
(4, 4, 'Past Due', 'en'),
(5, 5, 'Pending', 'en'),
(6, 6, 'Charge Off', 'en'),
(7, 1, 'Черновик', 'ru'),
(8, 2, 'Оплачен', 'ru'),
(9, 3, 'Оплачен Частично', 'ru'),
(10, 4, 'Платеж Просрочен', 'ru'),
(11, 5, 'Ожидает оплаты', 'ru'),
(12, 6, 'Не будет оплачен', 'ru'),
(13, 1, 'Чернетка', 'ua'),
(14, 2, 'Сплачений', 'ua'),
(15, 3, 'Частково Сплачений', 'ua'),
(16, 4, 'Прострочений', 'ua'),
(17, 5, 'В очікуванні ', 'ua'),
(18, 6, 'Не буде сплачений', 'ua');

-- --------------------------------------------------------

--
-- Структура таблицы `invoice_actions`
--

CREATE TABLE `invoice_actions` (
  `invact_id` int(11) UNSIGNED NOT NULL,
  `invact_action` varchar(100) DEFAULT NULL,
  `invact_desc` varchar(100) DEFAULT NULL,
  `invact_lang` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `invoice_actions`
--

INSERT INTO `invoice_actions` (`invact_id`, `invact_action`, `invact_desc`, `invact_lang`) VALUES
(1, 'create', 'Invoice Created', 'en'),
(2, 'update', 'Invoice Updated', 'en'),
(3, 'pay', 'Payment Added', 'en'),
(4, 'send', 'Sent by Email', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `invoice_history`
--

CREATE TABLE `invoice_history` (
  `invhist_id` int(11) UNSIGNED NOT NULL,
  `invhist_user_id` int(11) NOT NULL DEFAULT '0',
  `invhist_inv_id` int(11) NOT NULL DEFAULT '0',
  `invhist_action` varchar(255) DEFAULT NULL,
  `invhist_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `invhist_email` varchar(255) DEFAULT NULL,
  `invhist_cc` varchar(255) DEFAULT NULL,
  `invhist_desc` varchar(255) DEFAULT NULL,
  `invhist_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invhist_currency` varchar(4) NOT NULL DEFAULT 'USD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `invoice_history`
--

INSERT INTO `invoice_history` (`invhist_id`, `invhist_user_id`, `invhist_inv_id`, `invhist_action`, `invhist_date`, `invhist_email`, `invhist_cc`, `invhist_desc`, `invhist_amount`, `invhist_currency`) VALUES
(1, 1, 1, 'create', '2017-10-29 00:28:52', NULL, NULL, NULL, '0.00', 'USD'),
(2, 1, 2, 'create', '2017-10-29 00:29:28', NULL, NULL, NULL, '0.00', 'USD'),
(3, 1, 3, 'create', '2017-10-29 00:30:00', NULL, NULL, NULL, '0.00', 'USD'),
(4, 1, 4, 'create', '2017-10-29 00:30:32', NULL, NULL, NULL, '0.00', 'USD'),
(5, 1, 5, 'create', '2017-10-29 00:31:33', NULL, NULL, NULL, '0.00', 'USD'),
(6, 16, 6, 'create', '2017-10-31 07:45:37', NULL, NULL, NULL, '0.00', 'USD'),
(7, 16, 6, 'email', '2017-10-31 07:45:37', 'pr.idea.pr@gmail.com', NULL, NULL, '0.00', 'USD');

-- --------------------------------------------------------

--
-- Структура таблицы `issues`
--

CREATE TABLE `issues` (
  `issue_id` int(11) UNSIGNED NOT NULL,
  `issue_user_id` int(11) NOT NULL DEFAULT '0',
  `issue_project_id` int(11) NOT NULL DEFAULT '0',
  `issue_milestone_id` int(11) NOT NULL DEFAULT '0',
  `issue_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issue_cat` int(11) NOT NULL DEFAULT '10',
  `issue_priority` int(11) NOT NULL DEFAULT '10',
  `issue_status` int(11) NOT NULL DEFAULT '10',
  `issue_subj` varchar(255) DEFAULT NULL,
  `issue_text` text,
  `issue_assign` int(11) NOT NULL DEFAULT '0',
  `issue_msg` int(11) NOT NULL DEFAULT '0',
  `issue_deadline` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `issue_trash` tinyint(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `issues`
--

INSERT INTO `issues` (`issue_id`, `issue_user_id`, `issue_project_id`, `issue_milestone_id`, `issue_created`, `issue_cat`, `issue_priority`, `issue_status`, `issue_subj`, `issue_text`, `issue_assign`, `issue_msg`, `issue_deadline`, `issue_trash`) VALUES
(1, 3, 1, 0, '2017-09-26 06:08:14', 10, 30, 10, 'Ошибка в контроллере Инвойсов', '<img src=\"http://secure.floctopus.info/redactor/t_22c727a1cabfc2c26ec39fbaf28a245a.png\" class=\"img-responsive\"><p \"=\"\">914 линия в контроллере инвойсов, $html не установлена</p>', 1, 0, '2017-09-27 17:00:00', 0),
(2, 3, 1, 0, '2017-09-26 06:11:40', 10, 30, 10, 'Ошибка в логин-контроллере', '<img src=\"http://secure.floctopus.info/redactor/t_6f172f0a44b3f1882624f9aca15b25e9.png\" class=\"img-responsive\"><p \"=\"\">154 строка не обьявленная переменная</p>', 1, 0, '2017-09-27 17:00:00', 0),
(3, 3, 1, 0, '2017-09-26 06:42:13', 10, 10, 10, 'model/libs/jetpaypal 252стр. неустановленная переменная', '<img src=\"http://secure.floctopus.info/redactor/t_ba3e4133a452ba80df685b392aa78bb5.png\" class=\"img-responsive\">', 1, 0, '2017-09-27 17:00:00', 0),
(5, 3, 7, 13, '2017-10-02 11:41:23', 30, 30, 10, 'Поставить список этапов по дате', '<img src=\"http://secure.floctopus.info/redactor/t_4d07023c797325c7909e0078d1564869.png\" class=\"img-responsive\"><p \"=\"\">при редактировании этапов более корректно выстраивать этапы по дате, например я добавила этап и дата закрытия 3 число он должен стоять в начале списка перед 8-м</p>', 1, 0, '2017-10-10 17:00:00', 0),
(6, 3, 7, 0, '2017-10-03 16:26:25', 10, 30, 10, 'Не работает загрузка картинок в Документах', '<img src=\"http://secure.floctopus.info/redactor/t_0d5bacecaba16b8855129889bbe645fc.png\" class=\"img-responsive\"><p \"=\"\">не работает загрузка картинок в документах и некрасиво отображается на русском языке<br></p>', 1, 0, '2017-10-04 17:00:00', 0),
(8, 4, 7, 0, '2017-10-03 18:31:42', 10, 30, 10, 'На почту не приходят инвойсы и сметы', '<p \"=\"\">при отправке инвойсов или смет письмо на почту клиента не приходит</p>', 1, 0, '2017-10-04 17:00:00', 0),
(10, 3, 7, 44, '2017-10-03 19:03:16', 30, 30, 10, 'Ошибки в Messegere', '<ol><li>в чат добавились клиенты, но они не зарегистрированы в системе. </li></ol><img src=\"http://secure.floctopus.info/redactor/t_570ba0373614f555c477f87dee81c327.png\" class=\"img-responsive\"><p \"=\"\">2.Клиентам можно отправить сообщения</p><p><img src=\"http://secure.floctopus.info/redactor/t_2d72bd4b61b160afac99f73a698789f6.png\" class=\"img-responsive\"></p><p \"=\"\">НО...</p><p><img src=\"http://secure.floctopus.info/redactor/t_681c3863a179fb49ccd9a1c6d10cf6ab.png\" class=\"img-responsive\"></p><p \"=\"\">одно и то же сообщение приходит и к одному и к другому клиенту<br></p><p \"=\"\"><br></p><p \"=\"\"><br></p>', 1, 0, '2017-10-11 17:00:00', 0),
(11, 3, 7, 0, '2017-10-03 19:09:00', 20, 30, 10, 'перевод инвойсов', '<p>инвойсы в языковые файлы</p>', 0, 0, '2017-10-04 17:00:00', 0),
(12, 3, 7, 0, '2017-10-03 19:11:12', 20, 30, 10, 'перевод смет', '<p>перевести сметы</p>', 0, 0, '2017-10-04 17:00:00', 0),
(13, 3, 7, 7, '2017-10-03 19:13:23', 20, 40, 50, 'Тестирование ядра', '<p \"=\"\">Проверить Инвойсы, Сметы, Расходы, Категории расходов, Транзакции</p>', 1, 0, '2017-10-04 17:00:00', 0),
(14, 1, 6, 0, '2017-10-12 07:38:05', 20, 20, 10, 'Материалы', '<img src=\"http://secure.floctopus.info/redactor/41e6424b5645dad83c4046fc87a0e53f.jpeg\" class=\"img-responsive\"><p>Тут то что они изначально прислали</p>', 3, 0, '2018-04-01 00:00:00', 0),
(15, 3, 7, 0, '2017-10-12 11:20:47', 30, 30, 10, 'Мулитиязычность пагинации', '<p \"=\"\"><strong>Мулитиязычность пагинации</strong>. Сторінка-Страница-Page</p>', 1, 0, '2017-10-13 17:00:00', 0),
(16, 3, 7, 0, '2017-10-12 11:51:03', 10, 40, 50, 'Не работает поиск на русском языке', '', 3, 1, '2017-10-13 17:00:00', 0),
(17, 1, 7, 0, '2017-10-12 12:37:30', 10, 50, 50, 'Проблема с приглашениями', '<p>Подключает к сотрудничеству и показывает во всех списках без потверждения приглашенного<br></p>', 1, 0, '2017-10-14 00:00:00', 0),
(18, 1, 7, 0, '2017-10-12 23:24:51', 10, 60, 50, 'Список проектов в фильтре', '<p>При регистрации нового пользователя выводит неправильный список проектов вфильтре</p>', 1, 0, '2017-10-14 00:00:00', 0),
(19, 1, 7, 0, '2017-10-13 12:28:54', 20, 40, 10, 'Просмотр профилей в приглашениях', '<p>Сделать просмотр профилей пользователей в приглашениях<br></p>', 1, 0, '2017-10-15 00:00:00', 0),
(20, 3, 7, 0, '2017-10-13 12:32:15', 10, 10, 50, 'test', 'asdasd', 3, 0, '2017-10-14 17:00:00', 0),
(21, 3, 7, 11, '2017-10-13 12:36:56', 30, 30, 50, 'Задачи. Сделать ссылку на профиль пользователя в задачах и ссылку на проект', '<p><img src=\"http://secure.floctopus.info/redactor/t_2d5911d95b876d871d74b14a6af6edf9.png\" class=\"img-responsive\"></p>', 1, 1, '2017-10-14 17:00:00', 0),
(22, 3, 7, 0, '2017-10-13 12:39:48', 10, 30, 50, 'Сделать возможность добавления задач сотруднику проекта', '<img src=\"http://secure.floctopus.info/redactor/t_98d2989c8ad461e7a9f96c6221a3cff4.png\" class=\"img-responsive\">', 1, 0, '2017-10-14 17:00:00', 0),
(23, 3, 7, 14, '2017-10-13 12:47:57', 10, 40, 50, 'Клиенты. Клиенты не работает удаление клиента и клиентов группой и редактирование информации о клиенте', '<p><img src=\"http://secure.floctopus.info/redactor/t_a336e7cee885fd644ef8ee02eb44b44f.png\" class=\"img-responsive\"></p><p><img src=\"http://secure.floctopus.info/redactor/t_10d5e9b7d44167692bf5b0e4a61c42eb.png\" class=\"img-responsive\"></p>', 1, 0, '2017-10-16 17:00:00', 0),
(24, 3, 7, 0, '2017-10-13 13:13:11', 10, 60, 50, 'Убрать возможность удаления проекта если ты не создатель', '<img src=\"http://secure.floctopus.info/redactor/t_8c5bc9e45fe081432d33b8215cc9b861.png\" class=\"img-responsive\">', 1, 0, '2017-10-14 17:00:00', 0),
(26, 3, 7, 12, '2017-10-13 14:37:00', 10, 30, 10, 'Календарь. неверное отображение времени', '<ol><li>Когда в календаре ставишь задачу и на определенное время то ставит на весь день<img src=\"http://secure.floctopus.info/redactor/t_87ab0c4a06f823d606b94d409f21d7f7.png\" class=\"img-responsive\"></li><li>при просмотре одного дня показвает только две задачи, хотя на день запланировано больше и с 20-00 что неправильно так как дед-лайн до восьми вечера <br> </li></ol><img src=\"http://secure.floctopus.info/redactor/t_9e19540c68a6938406a4fba2fe836e3a.png\" class=\"img-responsive\"><p><img src=\"http://secure.floctopus.info/redactor/t_ae6d0f519be1f3f860492e0893c8d499.png\" class=\"img-responsive\"></p>', 1, 0, '2017-10-14 17:00:00', 0),
(27, 3, 7, 11, '2017-10-13 14:40:53', 20, 30, 50, 'Задачи. в задачах не подключен поиск', '<p>в задачах не подключен поиск</p>', 1, 0, '2017-10-14 17:00:00', 0),
(28, 3, 7, 11, '2017-10-13 14:58:44', 10, 30, 20, 'Задачи. не работает фильтр-сортировка', '<p><img src=\"http://secure.floctopus.info/redactor/t_3c3bb056729c04bcc113885cf8beb2e4.png\" class=\"img-responsive\"></p>', 1, 1, '2017-10-14 17:00:00', 0),
(29, 3, 3, 0, '2017-10-13 15:17:24', 10, 10, 10, 'Задачи. Добавить фильтр по этапу и название проекта и этапа вынести над номером задачи', '<img src=\"http://secure.floctopus.info/redactor/t_002b1db157abcaf205dc4863bde2dcd2.png\" class=\"img-responsive\"><p>Добавить фильтр по этапу и название проекта и этапа вынести над номером задачи</p>', 3, 0, '2017-10-14 17:00:00', 0),
(30, 3, 7, 32, '2017-10-13 15:39:21', 60, 20, 10, 'Уведомление. кто-то поделился документом', '<p \"=\"\">нужно чтобы когда кто-то поделился документом человеку приходило уведомление</p><p><img src=\"http://secure.floctopus.info/redactor/t_e0e7ef76635cff686a9781f73ae73ee5.png\" class=\"img-responsive\"></p>', 1, 0, '2017-10-14 17:00:00', 0),
(31, 3, 7, 11, '2017-10-13 15:42:03', 60, 10, 10, 'Уведомление. Когда назначена задача присылать уведомление', '<p \"=\"\">Когда назначена задача присылать уведомление не только на почту но и в колокольчик, тот который возле профиля вверху страницы</p>', 1, 1, '2017-10-14 17:00:00', 0),
(32, 1, 12, 21, '2017-10-13 19:31:03', 30, 10, 50, 'Шаринг', '<p>При шаринге в вк ссылка ведет сразу на раздел калькулятора, а наверное нужно на самый верх.</p>', 1, 2, '2017-10-17 00:00:00', 0),
(33, 1, 12, 21, '2017-10-13 19:32:19', 10, 10, 50, 'вывести other spends во внутреннем калькулятове в итоговой таблице', ' вывести other spends во внутреннем калькулятове в итоговой таблице', 1, 0, '2017-10-17 00:00:00', 0),
(34, 1, 12, 21, '2017-10-13 19:33:09', 20, 10, 50, 'добавить тур про черепах без кнопки “пересчитать тур”', '<p> добавить тур про черепах без кнопки “пересчитать тур”</p>', 1, 0, '2017-10-31 00:00:00', 0),
(35, 1, 12, 21, '2017-10-13 19:33:46', 20, 10, 10, 'добавить джип, логика как и в яла-парке. джип удавалаве, цена такая же. и прикрутить его стоимость к туру ЗОЛОТОЙ ТРЕУГОЛЬНИК', '<p>добавить джип, логика как и в яла-парке. джип удавалаве, цена такая же. и прикрутить его стоимость к туру ЗОЛОТОЙ ТРЕУГОЛЬНИК</p>', 1, 1, '2017-10-17 00:00:00', 0),
(36, 1, 12, 21, '2017-10-13 19:34:46', 10, 10, 30, 'выводится сумма на двоих, а должна на одного', '<p>неправильно выводится сумма в форме выбора даты , выводится сумма на двоих, а должна на одного</p><p><img src=\"https://lh5.googleusercontent.com/h3jDHTjeE28n0vYqyidbsP-ax12orsHn9t6y9EGX7udBWKaC0aScgwCcMRYyOVSNBLCI0-7fkGBXUR-tvkKQuHPnMQ9oG2WTr26sKoOZiFVlNAJoLduM5fp72olr1eJ5PhWIDpYB\"><span class=\"redactor-invisible-space\"><br></span></p>', 1, 0, '2017-10-17 00:00:00', 0),
(37, 1, 12, 21, '2017-10-13 19:37:20', 20, 20, 10, 'Хит продаж', '<p> Есть ли у тебя значки, “Хит продаж”, “новинка”, которые мы поставим на пакет на главной? Можно ли как то добавить их добавление в админке?</p><p><br></p>', 1, 1, '2017-10-17 00:00:00', 0),
(38, 3, 7, 0, '2017-10-14 15:31:22', 10, 30, 50, 'Загрузка фото в профиле, обложки в портфолио', '<p>Загрузка фото в профиле, обложки в портфолио</p>', 0, 0, '2017-10-15 17:00:00', 0),
(39, 1, 1, 0, '2017-10-14 23:42:01', 10, 10, 10, 'test general', '<p>ku ku</p>', 8, 0, '2017-10-16 17:00:00', 0),
(40, 3, 7, 43, '2017-10-15 08:31:26', 20, 30, 10, 'Поиск в документах', '<p>не подключен поиск в документах</p>', 1, 0, '2017-10-16 17:00:00', 0),
(44, 11, 25, 0, '2017-10-20 13:48:39', 20, 30, 10, 'Встреча с консультантом', '<p>пн 18:00</p>', 12, 3, '2017-10-23 15:00:00', 0),
(47, 11, 25, 0, '2017-10-20 15:06:22', 20, 40, 10, 'Финальное предложение о Paul', '<p>Во вложении оправляю финальное предложение от Поля с разбивкой бюджетов по маркетингу и СЕО.</p><p>Кратко:</p><p>$ 3000 - единоразовая плата за разработку АНАЛИЗЫ/СТРАТЕГИЯ</p><p>$ 3500 - ежемесячная плата за реализацию маркетингового плана (описано несколько позиций, но у меня тут много вопросов - мне не совсем понятно, что они предлагают за эту сумму и как она у них нарисовалась, надо уточнять при встрече)<br></p><p>СМЕТА ПО СЕО (Поль разбил в табличке что он будет делать/где затраты/где его доход - как собственно, мы и просили)</p><p>1 month - $5950 </p><p>2 month - $5950 </p><p>3 month - $5950 </p><p>Ongoing SEO Month 4 $4000 </p><p>Ongoing SEO month 5 $4000 </p><p>Ongoing SEO month 6 $4000</p><p>Мое резюме:</p><p> 1) последняя уточняющая встреч а в скайпе в 10:00 в вс (все-таки уточнить по маркетинговым услугам, как эта сумма появляется еще до анализа и самого плана)</p><p> 2) в перечне за маркетинговый анализ пропала строка общий СЕО анализ, (а в таблице поля отдельной строкой это не прописано - то есть, или специально выкинули, или пропустили, в таблице осталось только анализ ссылок конкурентов)..... - опять же, хотелось бы уточнить</p><p>3) Оплата Поля все-таки завышенная для нас, даже при условии, что он с 4 месяца снижает стоимость (все равно он в таблице делает приписку, что увеличение бюджета после 6 мес возможно...)</p><p>PS. маркетинговая часть переведена прямо в том же документе <a target=\"_blank\" href=\"http://secure.floctopus.info//userfiles/11/attach/final_marketing_costs.xlsx\">final_marketing_costs</a><span class=\"redactor-invisible-space\"> там небольшая табличка), а разбивка работ СЕо Поля в отдельном доке </span></p>', 12, 1, '2017-10-21 16:00:00', 0),
(48, 1, 7, 44, '2017-10-21 02:40:27', 20, 30, 50, 'Отправка сообщения в Messanger', '<p>Сдклвть отправку сообщения по Enter</p>', 1, 0, '2017-10-22 17:00:00', 0),
(49, 11, 20, 0, '2017-10-23 08:40:28', 20, 30, 10, 'описание проекта', '<p>Анют, во вложении описание всех преимуществ FL. Чтобы понимать нагляднее, посмотри еще конкурентов на досуге</p>', 11, 0, '2017-10-24 17:00:00', 0),
(50, 11, 25, 0, '2017-10-23 08:41:24', 20, 10, 10, 'описание проекта', '', 0, 0, '2017-10-24 17:00:00', 0),
(51, 11, 7, 0, '2017-10-23 09:33:07', 10, 10, 10, 'правки сайта', '<p>1) в мессенджере не хватает функции (\"доставлено\"-просмотрено\") после того, как отправил сообщение</p>', 1, 0, '2017-10-24 17:00:00', 0),
(52, 11, 25, 0, '2017-10-23 09:51:31', 60, 30, 10, 'предложение от консультанта на собеседование', '<p>Александр, во вложении, предложение по вопросам и заданиям от Виктории для специалистов по маркетингу.</p><p>Вопросы хорошие, и если их давать, то в принципе, можно отсеять сразу некачественных кандидатов. Я бы, еще уточнила некоторые ответы (чтобы понять/правильно или нет ответил кандидат). Мне, например, тяжело будет оценить ответ на вопрос в части  (Finance: P&L и точка безубыточности), и в платной задаче я бы, наверное, ограничивала их нашми реальными бюджетами с указанием ожиданий по лидогенерации, чтобы получать готовое ипредложение (или обоснование, почему этих показателей, например, не достичь за предложенную сумму)</p><p>Ознакомьтесь и скажите, как считаете</p>', 12, 0, '2017-10-24 17:00:00', 0),
(53, 13, 7, 0, '2017-10-23 10:54:07', 30, 30, 10, 'маленький экран', '', 1, 0, '2017-10-30 17:00:00', 0),
(54, 13, 7, 0, '2017-10-23 11:02:05', 30, 30, 10, 'вставка картинок/скриншотов', '<p>возможность добавить картинку или скриншот комбинацией клавиш ctrl+v </p>', 1, 0, '2017-10-24 17:00:00', 0),
(55, 13, 27, 0, '2017-10-23 11:06:19', 60, 30, 50, 'предупреждение о к-стве симоволов', '<p>1. При создании Никнэйма - предупреждать о минимальном количестве символов</p><p>2. При загрузке аватаров - предупреждать о минимальном размере файла </p>', 12, 0, '2017-10-24 17:00:00', 0),
(56, 13, 7, 0, '2017-10-23 11:08:58', 60, 30, 10, 'предупреждение о к-стве символов', '<p>1. Предупреждение о минимальном количестве букв Никнэйма</p><p>2. Предупреждение о размере файла при загрузке аватара</p>', 1, 0, '2017-10-24 17:00:00', 0),
(57, 13, 26, 0, '2017-10-24 15:53:48', 60, 30, 10, 'описание модулей', '', 12, 0, '2017-10-24 17:00:00', 0),
(58, 13, 26, 0, '2017-10-24 15:54:40', 60, 10, 10, 'описание блоков', '', 1, 0, '2017-10-25 17:00:00', 0),
(59, 1, 27, 0, '2017-10-24 20:49:32', 30, 30, 30, 'Убрать приставку мини-', '<p>В тексте документов встречается слово мини-бухгалтерия. Не надо употреблять приставку мини- , полу и т.д.</p>', 13, 0, '2017-10-25 17:00:00', 0),
(60, 1, 27, 0, '2017-10-24 20:51:28', 30, 30, 30, 'Разбить документ на части', '<p>Сейчас вся документация в одном документе. Желательно разбить его по модулям ( каждый в отдельном документе ). Так же желательно не использовать MS Word. Google Documents очень хорошее приложение и позволяет создавать документы не хуже Word\'а. Желательно перейти на Google Drive</p>', 13, 0, '2017-10-25 17:00:00', 0),
(61, 1, 27, 0, '2017-10-24 20:53:27', 60, 20, 50, 'Раздел документации для Floctopus', '<p>Сейчас мы создаем ресурс где можно и нужно вести документацию он будет находиться на <a href=\"http://doc.floctopus.com\">http://doc.floctopus.com</a> ( пока еще не доступен ). Я сообщу когда будет готов</p>', 12, 4, '2017-10-25 17:00:00', 0),
(62, 13, 27, 0, '2017-10-25 09:25:56', 30, 30, 50, 'разбить документ на части', '<p \"=\"\">модуль Ресурсы распишу, когда у меня будет понимание, что в себя будут включать  Услуги и Оборудование </p>', 1, 0, '2017-10-25 17:00:00', 0),
(63, 15, 31, 52, '2017-10-26 08:34:25', 20, 10, 10, 'Что то сделать для этапа 1', '<p>Это задача для первого этапа</p>', 15, 2, '2017-10-27 17:00:00', 0),
(64, 14, 12, 21, '2017-10-26 08:43:24', 10, 10, 30, 'Не отображается название тура в форме заказа', '<p>При переходе из калькулятора не отображается название тура в форме заказа. </p>', 14, 3, '2017-10-29 00:00:00', 0),
(65, 14, 12, 21, '2017-10-26 08:47:35', 10, 10, 50, 'фантомное поле стандард', '<p>В форме заказа, над датой</p>', 14, 4, '2017-10-28 00:00:00', 0),
(66, 14, 29, 0, '2017-10-26 08:54:10', 10, 10, 10, 'floctopus не вставляются линки copy pst', 'не вставляются линки copy past<p>курсор опускается на строку, но верхняя остается пустой</p>', 1, 0, '2017-10-28 00:00:00', 0),
(67, 14, 12, 21, '2017-10-26 09:12:13', 10, 10, 50, 'info = ДУБЛЬ', '<p>info из формы заказа нужно переименовать в info1 (сейчас там текст \"Окончательный план поездки составляется с учетом конкретного времени Вашего прилета и отлета утверждается только после согласования с Вами\".)</p><p>info уже использовано в деталях тура. Раньше там стояло слово \"Информация\", а теперь ошибочно выводится текст из формы заказа..</p>', 14, 3, '2017-10-28 00:00:00', 0),
(68, 7, 12, 21, '2017-10-26 09:17:45', 10, 10, 50, 'не загружаются фотки', '<p>Не загружаются фотографии для баяна. Захожу в Пакежес, нажимаю тур, фото, нажимаю на плюс, ничего не происходит</p>', 7, 3, '2017-10-28 00:00:00', 0),
(69, 14, 12, 21, '2017-10-26 09:27:20', 10, 10, 20, 'Ошибки в отображениях сумм и обозначений валют в деталях тура', '<p>Где-то показывает цену в долларах, но пишет рубли. Где-то евро вместо долларов. </p>', 14, 2, '2017-10-28 00:00:00', 0),
(70, 14, 12, 21, '2017-10-26 09:41:21', 10, 10, 30, 'Почта из \"консультации\" не приходит', 'При отправке сообщения из \"консультации\" ничего не приходит на почту ', 14, 1, '2017-10-28 00:00:00', 0),
(71, 14, 12, 21, '2017-10-26 09:44:28', 10, 10, 30, 'Не срабатывает округление в форме заказа', '<p>При переходе из банеров туров или деталей тура через выбор даты не срабатывает округление в форме заказа</p>', 1, 11, '2017-10-29 00:00:00', 0),
(72, 14, 12, 21, '2017-10-26 13:30:48', 10, 10, 30, 'неправильные символы в панели язык/валюта', 'в header  была ситуация, когда сайт показывался на немецком языке, но в переключателе стояло <a href=\"http://hikkaduwa.com/packages#order\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"searchIcon\">РУС / USD</span></a><p>Повторить пока не смог, возникла при новом  открытии сайта.</p>', 1, 2, '2017-10-28 00:00:00', 0),
(73, 14, 12, 0, '2017-10-26 14:10:25', 50, 10, 30, 'Ссылка на детали тура', '<p>Валера, а как выглядит ссылка на деталированный тур (подробнее)?</p><p>Она вообще может быть расположена в любой другой точке сайта, кроме как в банере тура  на кнопке \"подробнее\"? </p>', 1, 2, '2017-10-28 00:00:00', 0),
(74, 1, 27, 0, '2017-10-26 21:23:58', 20, 40, 10, 'Рвздел для управление документацией', '<p>Вот тут : <a href=\"https://doc.floctopus.com\">https://doc.floctopus.com</a> запустили модуль управления документацией. Для входа использовать обычный email и пароль, который используете для входа в основной сайт. Этот модуль будет расширятся и дополнятся, на данный момент того функционала, который там сейчас есть достаточно для добавления статей.</p><p>Доступ туда ограничен,  сейчас туда имеют доступ :</p><p>alexandr ( a.spassky@alliancepack.com.ua )<br></p><p>booper (ivafirst@gmail,.com)</p><p>blackg<span class=\"redactor-invisible-space\"> (kramar.anna@alliancepack.com.ua)</span></p><p>Для создания скриншотов надо использовать програму Monosnap (<a href=\"https://monosnap.com/welcome\" target=\"_blank\">https://monosnap.com/welcome</a><span class=\"redactor-invisible-space\">)</span></p><p><span class=\"redactor-invisible-space\">Я добавил туда один пример</span></p><p><span class=\"redactor-invisible-space\"><em><strong>ВАЖНО: Вся документация должна быть на АНГЛИЙСКОМ языке - никакого русского.</strong> </em></span></p>', 13, 0, '2017-10-28 17:00:00', 0),
(75, 1, 12, 21, '2017-10-27 07:47:50', 50, 30, 30, 'Meta tegs for FB Sharing', '<p><a href=\"https://developers.facebook.com/docs/sharing/webmasters/\">https://developers.facebook.com/docs/sharing/webma...</a></p><p><br></p><p><span class=\"tag\" style=\"font-family: Menlo, Monaco, monospace, sans-serif; font-size: 12.6px;\"></span></p>', 14, 1, '2017-10-28 17:00:00', 0),
(76, 14, 12, 21, '2017-10-27 07:55:31', 10, 30, 50, 'не закрывается окно', '<p><a href=\"http://screenshotlink.ru/b1aa21e99dbc935833773e2343b4d735.jpg\">скрин</a></p><p><img src=\"http://screenshotlink.ru/b1aa21e99dbc935833773e2343b4d735.jpg\"><br>не закрывается окно<span class=\"redactor-invisible-space\"> после нажатия на ok</span><br></p>', 1, 3, '2017-10-29 00:00:00', 0),
(77, 1, 32, 0, '2017-10-27 16:22:08', 30, 40, 10, 'Fixed Discount ( Ticket# 659 of MM )', '<p>Is there a way to include a whole $ discount instead of a % when preparing an estimate? Having the option of both would be great. thank you<span></span><br></p>', 1, 0, '2017-10-30 17:00:00', 0),
(78, 1, 32, 0, '2017-10-27 16:32:11', 30, 10, 10, 'when a user clocks in, can the red 1 show this way they don’t forget to clock out.', '<p>when a user clocks in, can the red 1 show this way they don’t forget to clock out.</p>', 1, 0, '2017-10-28 17:00:00', 0),
(79, 1, 32, 0, '2017-10-27 16:36:42', 20, 10, 10, 'Print Calendar', '<p>I am trying to print my calendar schedule and when I do it wants to not only print the entire page with the left hand side options, but it moves jobs to different days, it\'s like it\'s not in printable format. Is there a way to add a button that changes the calendar schedule to printable format like the route map is?</p>', 1, 0, '2017-10-28 17:00:00', 0),
(80, 1, 32, 0, '2017-10-27 16:44:17', 20, 50, 50, 'remove any numbers from invoice email', '<p>I really need the email format to change. I have customers who see the email and write a check off of the invoice amount they see in the email instead of clicking on the link and looking at the invoice where it shows the past due amount.</p><p>Please change the format so that it either shows nothing (forcing them to click on the link, which I would prefer) or change it so that it is a reflection of account balance and not just current invoice amount.</p><p>I think it would be better to only send a link. I cannot tell for certain if customers have seen the invoice and not clicked on it (some of them have sent payments and the icon is still blue) or if they have not received the invoice email. I do not want to keep on sending repeat emails if they have already seen it.</p><p>This has been an ongoing frustration for a while and I cannot see any other way to solve it. Please let me know if you have another idea.</p>', 1, 0, '2017-10-28 17:00:00', 0),
(81, 1, 32, 0, '2017-10-27 16:47:15', 20, 30, 50, 'settings options', '<section><p>I am submitting these issues as a new support ticket, after posting it in the support forum on September 14, 2017. With the digital changeover office wide, time is of the essence, each issue we encounter puts a hold on the progress of the entire workflow, so please excuse my persistence (again).</p><p>Am I missing a setting to show the asset on the invoice, WITHOUT starting it as a task? We are in the process of change over to a digital office. Incoming service calls are put in as a task, and in turn, can be invoiced, etc. However, the invoices I\'m entering that are going out this month, that were already in play, before we began using the software, won\'t seem to allow a spot for the asset to be entered.</p><p>Second, I also need a place on the invoice to reflect the payment due dates. I have attempted to add it in with \"custom field\" option, but it doesn\'t reflect on the invoice. I\'ve tried using the comment/notes boxes, but the information is not placed in a sensible position on the invoice; the due date should reflect at the top of the page, with the rest of the pertinent invoice information, i.e. Invoice date, invoice number, etc.</p></section><p>If either of these are due to a setting or option I have missed, please advise, otherwise I will wait for your suggested solution.</p>', 1, 0, '2017-11-03 17:00:00', 0),
(82, 1, 32, 0, '2017-10-27 16:50:22', 20, 10, 50, 'Maintenance tasks', '<p>Maintenance tasks only show up on the administrators task list. Need to be able to assign maintenance tasks like all other tasks so that they are visible to the appropriate people.</p>', 1, 0, '2017-11-03 17:00:00', 0),
(83, 1, 32, 0, '2017-10-27 16:51:29', 20, 10, 10, 'Is there not a way to print out a customer statement for a time period? I can view a report, and there is an email option but no print button.', 'Is there not a way to print out a customer statement for a time period? I can view a report, and there is an email option but no print button.', 1, 0, '2017-11-03 17:00:00', 0),
(84, 1, 32, 0, '2017-10-27 16:52:18', 20, 50, 10, 'in the client dashboard you can not send messages on mobile (iphone', 'in the client dashboard you can not send messages on mobile (iphone', 1, 0, '2017-10-30 17:00:00', 0),
(85, 1, 32, 0, '2017-10-27 16:53:03', 10, 50, 10, 'SMS issues', '<p>I\'m finding some problems with SMS.</p><p>1. If I\'m texted from a number I don\'t have saved in clients, the phone number don\'t show up.</p><p>2. If I\'m texted from a number I don\'t have saved in clients, once I leave the message, the only way to get back in is to hit the bell and SMS received. <br></p><p>3. We are not able to send text to numbers we don\'t have saved!</p><p>4. The text counter does not reset every month.</p><p>5. I created a ticket about another SMS issue. Was there resolution yet?</p>', 1, 0, '2017-10-30 17:00:00', 0),
(86, 1, 7, 35, '2017-10-27 16:54:41', 20, 10, 10, 'Создвние нового проека , при выбраном фильтре проектов', '<p>Если фильтр установлен на какой либо проект, делать активным этот проект при создании issue</p>', 9, 0, '2017-10-30 17:00:00', 0),
(87, 1, 32, 0, '2017-10-29 11:53:51', 30, 30, 10, 'Add Clover payments', '<p>is there anyway we can add other credit card merchant such as Clover through Bank of America. Square updates so much that it freezes on here</p>', 1, 0, '2017-11-17 18:00:00', 0),
(88, 1, 32, 0, '2017-10-29 11:54:43', 30, 30, 10, 'Integrate braintreepayments.com', '<p>Integrate <a href=\"https://www.braintreepayments.com\">https://www.braintreepayments.com</a></p>', 1, 0, '2017-11-30 18:00:00', 0),
(89, 1, 32, 0, '2017-10-29 12:10:02', 30, 40, 10, 'SMS template ( Signature )', '<p>When sending texts/sms from the client section, is it possible for me to brand a text permanently without us having to put in who the text is from each time?</p><p>Ex: I sent a text to a client advising the technician was enroute, the client\'s wife answered his text and responded \" Who is this \"?. I have to put in company info each text, which is wasting characters.</p><p>Thanks Mike...</p>', 1, 0, '2017-10-30 18:00:00', 0),
(91, 1, 32, 0, '2017-10-29 12:37:14', 30, 10, 10, 'Stop the clock', '<p>when a user clocks in, can the red 1 show this way they don’t forget to clock out.</p>', 1, 0, '2017-11-16 18:00:00', 0),
(92, 11, 33, 60, '2017-10-31 07:15:47', 20, 50, 30, 'написание брифа', '<p>1) поиск шаблона брифа с максимально полным списком требований к подрядчику - бнедбука, гидлайна, логобука</p><p><strong>Логобук</strong> включает в себя правила использования <strong>логотипа</strong>: как выглядит его вертикальная и горизонтальная версии, как работать с логотипом на фоне, что можно и чего нельзя делать с фирменным знаком и надписями. Это базовый документ, первый шаг к закреплению фирменного стиля.</p><p><strong>Гайдлайн</strong> — уже полноценное руководство по <strong>фирменному стилю</strong>. Кроме правил использования логотипа, здесь можно найти много полезной информации: фирменные шрифты, цвета и паттерны. В гайдлайне описываются правила разработки дизайн-макетов комплекта офисной продукции: <strong>визитки, конверта, папки и бланка</strong>.<span class=\"redactor-invisible-space\"><br></span></p><p><span class=\"redactor-invisible-space\"><strong>Брендбук</strong> — это документ, в котором закреплена вся <strong>информация о бренде</strong>: миссия и философия компании, правила работы с логотипом, шаблоны верстки всех необходимых носителей фирменного стиля, которые обговариваются индивидуально перед началом работы. Это незаменимый инструмент не только бренд-менеджера, дизайнера или маркетолога, но и каждого сотрудника компании. Брендбук существенно облегчает производство рекламных материалов и разработку новых дизайн-макетов.<span class=\"redactor-invisible-space\"><br></span></span></p><p>2) дописать п.п., которые будут актуальны для ФЛ<br></p><p>3) найти примеры удачных стилей (на выбор)</p><p>3) Согласовать со мной</p>', 16, 1, '2017-11-03 18:00:00', 0),
(93, 11, 33, 63, '2017-10-31 07:38:24', 20, 50, 10, 'поиск 5 подрядчиков для создания брендбука', '<p>Найти самых рейтинговых подрядчиков</p><p>2) свести все данные в одну таблицу по характеристикам: название, опыт, цена, дедлайн , примечания по каждому п.п. </p><p>лого</p><p>фирменный силь</p><p>слоган</p><p><br></p><p><br></p>', 16, 0, '2017-11-01 18:00:00', 0),
(94, 1, 32, 0, '2017-10-31 09:42:11', 60, 10, 10, 'PPL who willl leave feedback', '<p>Please note: we love this app so far. Better, cleaner, more efficient than merging data, documents and money through Office and Quickbooks. We are still at getting to know Managemart but very excited about the possibilities. We launched another business last week and are strongly considering using Managemart starting with our first customer.</p><p>I have a marketing degree and decades of small business experience. Please feel free to ask my opinion or for feedback relative to the program. Also, I am an avid google reviewer and plan to submit a very positive review soon. This is truly one of the best small business programs available! Something to consider in the future; increasing your price. Its worth it!</p><p>Most sincerely,</p><p>Dave</p><p><br></p><table class=\"table\"><tbody><tr><td>Address</td><td><address>2441 40th Avenue <br>Hudsonville, MI 49426<br>United States</address></td></tr><tr><td>Email</td><td>kelly@proscapelawnandsnow.com</td></tr><tr><td>Login Email</td><td>davidmteachout@gmail.com</td></tr><tr><td>Phone</td><td>616-284-1020</td></tr></tbody></table>', 1, 0, '2018-06-24 18:00:00', 0),
(95, 1, 32, 0, '2017-10-31 17:46:35', 30, 20, 10, 'individually for late fees amount or percentage.​', 'I know you guys have a lot on your plate, but I would like the ability to set each customer individually for late fees amount or percentage.', 1, 0, '2017-12-25 18:00:00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `issue_attachment`
--

CREATE TABLE `issue_attachment` (
  `issueatt_id` int(11) UNSIGNED NOT NULL,
  `issueatt_issue_id` int(11) DEFAULT '0',
  `issueatt_filename` varchar(250) DEFAULT NULL,
  `issueatt_user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `issue_attachment`
--

INSERT INTO `issue_attachment` (`issueatt_id`, `issueatt_issue_id`, `issueatt_filename`, `issueatt_user_id`) VALUES
(1, 92, 'the_Best_Bref_Logo_and_Style.doc', 11);

-- --------------------------------------------------------

--
-- Структура таблицы `issue_category`
--

CREATE TABLE `issue_category` (
  `issuecat_id` int(11) UNSIGNED NOT NULL,
  `issuecat_cat` int(11) DEFAULT NULL,
  `issuecat_name` varchar(100) DEFAULT NULL,
  `issuecat_color` varchar(7) DEFAULT NULL,
  `issuecat_lang` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `issue_category`
--

INSERT INTO `issue_category` (`issuecat_id`, `issuecat_cat`, `issuecat_name`, `issuecat_color`, `issuecat_lang`) VALUES
(10, 10, 'bug', 'danger', 'en'),
(20, 20, 'task', 'primary', 'en'),
(30, 30, 'enhancement', 'info', 'en'),
(50, 50, 'question', 'success', 'en'),
(60, 60, 'proposal', 'warning', 'en'),
(61, 10, 'ошибка', 'danger', 'ru'),
(62, 20, 'задача', 'primary', 'ru'),
(63, 30, 'улучшение', 'info', 'ru'),
(64, 50, 'вопрос', 'success', 'ru'),
(65, 60, 'предложение', 'warning', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `issue_history`
--

CREATE TABLE `issue_history` (
  `ihist_id` int(11) UNSIGNED NOT NULL,
  `ihist_issue_id` int(11) DEFAULT NULL,
  `ihist_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ihist_user_id` int(11) DEFAULT NULL,
  `ihist_msg` tinyint(11) NOT NULL DEFAULT '0',
  `ihist_message` text,
  `ihist_status` int(11) NOT NULL DEFAULT '0',
  `ihist_assign` int(11) NOT NULL DEFAULT '0',
  `ihist_priority` int(11) NOT NULL DEFAULT '0',
  `ihist_cat` int(11) NOT NULL DEFAULT '0',
  `ihist_prj` int(11) NOT NULL DEFAULT '0',
  `ihist_milestone` int(11) NOT NULL DEFAULT '0',
  `ihist_milestone_change` tinyint(4) NOT NULL DEFAULT '0',
  `ihist_deadline` timestamp NULL DEFAULT NULL,
  `ihist_deadline_change` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `issue_history`
--

INSERT INTO `issue_history` (`ihist_id`, `ihist_issue_id`, `ihist_date`, `ihist_user_id`, `ihist_msg`, `ihist_message`, `ihist_status`, `ihist_assign`, `ihist_priority`, `ihist_cat`, `ihist_prj`, `ihist_milestone`, `ihist_milestone_change`, `ihist_deadline`, `ihist_deadline_change`) VALUES
(1, 10, '2017-10-03 19:03:41', 3, 0, NULL, 0, 1, 0, 0, 0, 0, 0, NULL, 0),
(2, 10, '2017-10-12 11:23:54', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(3, 5, '2017-10-12 11:24:25', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(6, 6, '2017-10-12 11:28:38', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(7, 8, '2017-10-12 11:28:59', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(9, 11, '2017-10-12 11:30:16', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(10, 12, '2017-10-12 11:30:30', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(11, 13, '2017-10-12 11:30:51', 3, 0, NULL, 0, 0, 40, 0, 0, 0, 0, NULL, 0),
(12, 13, '2017-10-12 11:30:58', 3, 0, NULL, 0, 1, 0, 0, 0, 0, 0, NULL, 0),
(13, 13, '2017-10-12 11:31:06', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(14, 16, '2017-10-12 12:17:00', 3, 0, NULL, 0, 3, 0, 0, 0, 0, 0, NULL, 0),
(15, 16, '2017-10-12 12:17:38', 3, 1, '<p> сделать поиск везде кроме клиентов</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(16, 18, '2017-10-12 23:25:09', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(17, 18, '2017-10-12 23:25:11', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(18, 16, '2017-10-13 11:47:59', 3, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(19, 13, '2017-10-13 11:55:13', 3, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(21, 20, '2017-10-13 12:38:10', 3, 0, NULL, 0, 0, 0, 0, 3, 0, 0, NULL, 0),
(22, 20, '2017-10-13 12:38:16', 3, 0, NULL, 0, 0, 0, 0, 4, 0, 0, NULL, 0),
(23, 20, '2017-10-13 12:38:27', 3, 0, NULL, 0, 0, 0, 0, 0, 8, 1, NULL, 0),
(24, 20, '2017-10-13 12:38:44', 3, 0, NULL, 0, 0, 0, 0, 3, 0, 0, NULL, 0),
(25, 20, '2017-10-13 12:39:28', 3, 0, NULL, 0, 0, 0, 0, 4, 0, 0, NULL, 0),
(26, 20, '2017-10-13 12:39:58', 3, 0, NULL, 0, 0, 0, 0, 3, 0, 0, NULL, 0),
(27, 20, '2017-10-13 12:40:11', 3, 0, NULL, 0, 0, 0, 0, 4, 0, 0, NULL, 0),
(28, 20, '2017-10-13 12:40:15', 3, 0, NULL, 0, 0, 0, 0, 0, 8, 1, NULL, 0),
(29, 20, '2017-10-13 12:40:22', 3, 0, NULL, 0, 0, 0, 0, 3, 0, 0, NULL, 0),
(31, 20, '2017-10-13 12:40:52', 3, 0, NULL, 0, 0, 0, 0, 4, 0, 0, NULL, 0),
(32, 20, '2017-10-13 12:41:36', 3, 0, NULL, 0, 0, 0, 0, 6, 0, 0, NULL, 0),
(33, 20, '2017-10-13 12:41:39', 3, 0, NULL, 0, 0, 0, 0, 3, 0, 0, NULL, 0),
(34, 20, '2017-10-13 12:41:45', 3, 0, NULL, 0, 0, 0, 0, 4, 0, 0, NULL, 0),
(35, 20, '2017-10-13 12:41:49', 3, 0, NULL, 0, 0, 0, 0, 0, 8, 1, NULL, 0),
(36, 20, '2017-10-13 12:46:31', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(37, 20, '2017-10-13 12:52:19', 3, 0, NULL, 0, 0, 0, 0, 4, 0, 0, NULL, 0),
(38, 20, '2017-10-13 12:52:44', 3, 0, NULL, 0, 0, 0, 0, 0, 8, 1, NULL, 0),
(39, 20, '2017-10-13 12:53:09', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(40, 20, '2017-10-13 12:53:26', 3, 0, NULL, 0, 0, 0, 0, 3, 0, 0, NULL, 0),
(41, 20, '2017-10-13 13:00:11', 3, 0, NULL, 0, 0, 0, 0, 4, 0, 0, NULL, 0),
(42, 20, '2017-10-13 13:00:19', 3, 0, NULL, 0, 0, 0, 0, 3, 0, 0, NULL, 0),
(43, 20, '2017-10-13 13:00:25', 3, 0, NULL, 0, 0, 0, 0, 6, 0, 0, NULL, 0),
(44, 20, '2017-10-13 13:00:29', 3, 0, NULL, 0, 0, 0, 0, 4, 0, 0, NULL, 0),
(45, 20, '2017-10-13 13:00:32', 3, 0, NULL, 0, 0, 0, 0, 0, 8, 1, NULL, 0),
(46, 20, '2017-10-13 13:00:38', 3, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(47, 20, '2017-10-13 13:39:33', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(48, 22, '2017-10-13 13:39:54', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(49, 24, '2017-10-13 13:40:16', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(52, 20, '2017-10-13 15:00:04', 3, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(53, 22, '2017-10-13 15:02:41', 3, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(54, 24, '2017-10-13 15:03:21', 3, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(55, 19, '2017-10-13 15:11:43', 3, 0, NULL, 0, 0, 0, 0, 0, 15, 1, NULL, 0),
(56, 28, '2017-10-13 15:12:22', 3, 0, NULL, 0, 0, 0, 0, 0, 11, 1, NULL, 0),
(57, 27, '2017-10-13 15:12:43', 3, 0, NULL, 0, 0, 0, 0, 0, 11, 1, NULL, 0),
(58, 11, '2017-10-13 15:14:23', 3, 0, NULL, 0, 0, 0, 0, 0, 11, 1, NULL, 0),
(59, 23, '2017-10-13 15:18:39', 3, 0, NULL, 0, 0, 0, 0, 0, 14, 1, NULL, 0),
(60, 26, '2017-10-13 15:19:01', 3, 0, NULL, 0, 0, 0, 0, 0, 14, 1, NULL, 0),
(61, 26, '2017-10-13 15:19:10', 3, 0, NULL, 0, 0, 0, 0, 0, 12, 1, NULL, 0),
(62, 21, '2017-10-13 15:19:46', 3, 0, NULL, 0, 0, 0, 0, 0, 11, 1, NULL, 0),
(63, 23, '2017-10-13 15:20:13', 3, 0, NULL, 0, 0, 0, 0, 0, 0, 0, '2017-10-16 17:00:00', 1),
(64, 17, '2017-10-13 15:20:59', 3, 0, NULL, 0, 0, 0, 0, 0, 15, 1, NULL, 0),
(65, 11, '2017-10-13 15:21:52', 3, 0, NULL, 0, 0, 0, 0, 0, 0, 1, NULL, 0),
(66, 5, '2017-10-13 15:24:57', 3, 0, NULL, 0, 0, 0, 0, 0, 13, 1, NULL, 0),
(67, 10, '2017-10-13 15:27:22', 3, 0, NULL, 0, 0, 0, 0, 0, 19, 1, NULL, 0),
(68, 10, '2017-10-13 15:27:27', 3, 0, NULL, 0, 0, 0, 0, 0, 0, 1, NULL, 0),
(69, 6, '2017-10-13 15:27:46', 3, 0, NULL, 0, 0, 0, 0, 0, 12, 1, NULL, 0),
(70, 6, '2017-10-13 15:27:52', 3, 0, NULL, 0, 0, 0, 0, 0, 0, 1, NULL, 0),
(71, 8, '2017-10-13 15:28:17', 3, 0, NULL, 0, 0, 0, 0, 0, 17, 1, NULL, 0),
(72, 8, '2017-10-13 15:28:26', 3, 0, NULL, 0, 0, 0, 0, 0, 0, 1, NULL, 0),
(73, 12, '2017-10-13 15:28:56', 3, 0, NULL, 0, 0, 0, 0, 0, 18, 1, NULL, 0),
(74, 12, '2017-10-13 15:29:04', 3, 0, NULL, 0, 0, 0, 0, 0, 0, 1, NULL, 0),
(75, 17, '2017-10-13 15:30:30', 3, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(76, 23, '2017-10-13 15:42:48', 3, 0, NULL, 0, 0, 30, 0, 0, 0, 0, NULL, 0),
(77, 23, '2017-10-13 15:42:57', 3, 0, NULL, 0, 0, 40, 0, 0, 0, 0, NULL, 0),
(78, 36, '2017-10-13 21:56:21', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(79, 38, '2017-10-14 15:31:32', 3, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(80, 38, '2017-10-14 15:31:36', 3, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(81, 31, '2017-10-14 19:42:50', 1, 1, '<p>Называется внутренние уведомления</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(82, 27, '2017-10-14 19:44:35', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(83, 28, '2017-10-14 22:18:53', 1, 1, '<p>пока отключил</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(84, 28, '2017-10-14 22:20:04', 1, 0, NULL, 20, 0, 0, 0, 0, 0, 0, NULL, 0),
(85, 23, '2017-10-14 23:05:10', 1, 0, NULL, 0, 0, 0, 0, 16, 0, 0, NULL, 0),
(86, 23, '2017-10-14 23:05:23', 1, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(87, 23, '2017-10-14 23:05:36', 1, 0, NULL, 0, 0, 0, 0, 0, 14, 1, NULL, 0),
(88, 23, '2017-10-14 23:10:00', 1, 0, NULL, 0, 8, 0, 0, 0, 0, 0, NULL, 0),
(89, 21, '2017-10-15 00:59:37', 1, 1, '<p>Гтово!</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(90, 21, '2017-10-15 00:59:45', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(91, 21, '2017-10-15 00:59:46', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(92, 31, '2017-10-15 01:00:18', 1, 0, NULL, 0, 0, 0, 0, 0, 11, 1, NULL, 0),
(93, 30, '2017-10-15 01:01:30', 1, 0, NULL, 0, 0, 0, 0, 0, 32, 1, NULL, 0),
(94, 19, '2017-10-15 01:01:53', 1, 0, NULL, 0, 0, 0, 0, 0, 26, 1, NULL, 0),
(95, 10, '2017-10-15 01:02:24', 1, 0, NULL, 0, 8, 0, 0, 0, 0, 0, NULL, 0),
(96, 38, '2017-10-15 01:54:20', 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(97, 38, '2017-10-15 01:54:46', 1, 0, NULL, 0, 3, 0, 0, 0, 0, 0, NULL, 0),
(98, 38, '2017-10-15 01:54:54', 1, 0, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(99, 27, '2017-10-15 08:13:48', 3, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(100, 30, '2017-10-18 11:31:59', 1, 0, NULL, 0, 9, 0, 0, 0, 0, 0, NULL, 0),
(101, 15, '2017-10-18 11:32:22', 1, 0, NULL, 0, 9, 0, 0, 0, 0, 0, NULL, 0),
(102, 40, '2017-10-18 13:28:57', 1, 0, NULL, 0, 8, 0, 0, 0, 0, 0, NULL, 0),
(103, 44, '2017-10-20 14:00:41', 11, 1, '<p>Встречаемся?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(104, 44, '2017-10-20 14:03:51', 12, 1, '<p>определенность будет до 22,10 -я смогу сообщить-получится у меня или нет?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(107, 44, '2017-10-20 15:29:52', 11, 1, '<p>На данный момент консультант Валерий  (США, Калифорния), отписался,  что у него свободно время встречи в скайпе  пн, 18:00 - Он ждет нашего подтверждения на пн или другой удобный день. Жду вашего ответа 22.10</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(108, 47, '2017-10-20 19:03:41', 11, 1, '<p><a target=\"_blank\" href=\"http://secure.floctopus.info/userfiles/11/attach/final_seo_contract__.xlsx\">final_seo_contract__.xlsx</a></p><p><a target=\"_blank\" href=\"http://secure.floctopus.info/userfiles/11/attach/final_seo_contract___1.xlsx\">final_seo_contract___1.xlsx</a></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(109, 23, '2017-10-21 00:37:17', 12, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(110, 23, '2017-10-21 00:39:05', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(111, 17, '2017-10-21 00:39:41', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(112, 10, '2017-10-21 00:40:19', 1, 0, NULL, 0, 0, 0, 0, 0, 44, 1, NULL, 0),
(113, 48, '2017-10-21 03:03:31', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(114, 48, '2017-10-21 03:03:32', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(115, 55, '2017-10-23 11:06:40', 13, 0, NULL, 0, 12, 0, 0, 0, 0, 0, NULL, 0),
(116, 55, '2017-10-23 11:06:52', 13, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(117, 50, '2017-10-23 12:19:32', 11, 0, NULL, 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(118, 56, '2017-10-24 16:18:27', 1, 0, NULL, 0, 1, 0, 0, 0, 0, 0, NULL, 0),
(119, 54, '2017-10-24 16:18:39', 1, 0, NULL, 0, 1, 0, 0, 0, 0, 0, NULL, 0),
(120, 56, '2017-10-24 16:19:11', 1, 0, NULL, 0, 0, 0, 0, 0, 45, 1, NULL, 0),
(121, 56, '2017-10-24 16:19:17', 1, 0, NULL, 0, 0, 0, 0, 0, 0, 1, NULL, 0),
(122, 60, '2017-10-24 20:51:44', 1, 0, NULL, 0, 0, 30, 0, 0, 0, 0, NULL, 0),
(123, 53, '2017-10-24 20:54:49', 1, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(124, 61, '2017-10-24 21:25:56', 12, 1, '<p>ок. и до когда по времени сможешь сообщить?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(125, 61, '2017-10-24 21:27:21', 12, 1, '<p>кстати, сегодня вечером у меня стерлись все старые задачи. Куда-то делись они. Порядок это по-моему</p><p><br></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(126, 59, '2017-10-25 08:45:22', 13, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(127, 60, '2017-10-25 09:23:35', 13, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(128, 61, '2017-10-25 10:56:53', 1, 1, '<p>Я только что зашел и посмотрел в твоем экакаунте я вижу много задач, Какие старые ты имеешь ввиду? Из какого проекта? Возможно неправильно установлен фильтр справой стороны </p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(129, 61, '2017-10-25 11:46:09', 12, 1, '<p>ну да. Понял. </p><p>Спасибо</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(130, 63, '2017-10-26 08:34:43', 15, 0, NULL, 0, 0, 0, 20, 0, 0, 0, NULL, 0),
(131, 63, '2017-10-26 08:34:53', 15, 1, '<p>Зачем поменял?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(132, 63, '2017-10-26 08:35:02', 15, 1, '<p>Захотелось и поменял</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(133, 63, '2017-10-26 08:35:15', 15, 0, NULL, 0, 0, 0, 0, 0, 52, 1, NULL, 0),
(134, 34, '2017-10-26 08:53:45', 7, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(135, 34, '2017-10-26 08:53:49', 7, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(136, 33, '2017-10-26 08:56:23', 7, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(137, 33, '2017-10-26 08:56:25', 7, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(138, 37, '2017-10-26 08:57:25', 14, 1, '<p>Напоминаю идею про gif в окне с фоткой</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(139, 64, '2017-10-26 17:25:24', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(140, 64, '2017-10-26 17:25:33', 1, 0, NULL, 0, 14, 0, 0, 0, 0, 0, NULL, 0),
(141, 67, '2017-10-26 17:31:22', 1, 1, '<p>Добавил info, вкрнее переименовал info в info1</p><p>Добавь info1=\"xxx\" в языковой файл, И все будет работать</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(142, 67, '2017-10-26 17:31:25', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(143, 65, '2017-10-26 17:31:58', 1, 1, '<p>что значит фантомное?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(144, 68, '2017-10-26 17:33:17', 1, 1, '<p>Что такое Пакежес ?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(145, 68, '2017-10-26 17:40:40', 1, 1, '<p>Исправил</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(146, 68, '2017-10-26 17:40:44', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(147, 68, '2017-10-26 17:40:48', 1, 0, NULL, 0, 7, 0, 0, 0, 0, 0, NULL, 0),
(148, 69, '2017-10-26 17:41:47', 1, 1, '<p>Нужно точнее, я попробовал и все вроде нормально, конкретно какой тур, и при каких условиях, какой язык и какая валюта выбрана</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(149, 69, '2017-10-26 17:41:54', 1, 0, NULL, 20, 0, 0, 0, 0, 0, 0, NULL, 0),
(150, 69, '2017-10-26 17:42:03', 1, 0, NULL, 0, 14, 0, 0, 0, 0, 0, NULL, 0),
(151, 70, '2017-10-26 17:42:31', 1, 1, '<p>Уже приходит, проверте</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(152, 70, '2017-10-26 17:42:34', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(153, 70, '2017-10-26 17:42:40', 1, 0, NULL, 0, 14, 0, 0, 0, 0, 0, NULL, 0),
(154, 71, '2017-10-26 17:43:30', 1, 1, '<p>Какой конкретно тур ?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(155, 72, '2017-10-26 17:44:30', 1, 1, '<p>При открытии сайта всегда устанавливается русский язык</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(156, 73, '2017-10-26 17:45:47', 1, 1, '<p>В приницепе да, может, почему вопрос?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(157, 35, '2017-10-26 17:46:18', 1, 1, '<p>Дык вроде сделал, что разве не работает?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(158, 65, '2017-10-26 17:48:35', 14, 1, '<p>Линк <img src=\"http://screenshotlink.ru/e035a189958c95584122fe41114d8b26.jpg\">не вставляется, отправил в фб, но думаю, ты его тоже увидишь. Поле которого не может быть, в виде кнопки standard <br></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(159, 71, '2017-10-26 17:49:08', 14, 1, '<p>любой</p><p><br></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(160, 72, '2017-10-26 17:50:31', 14, 1, '<p>В таком случае, у меня уткрылся сайт, у которого был выставлен русский язык, но он показывал всё на намецком</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(161, 32, '2017-10-26 17:52:51', 1, 1, 'ТОлько что попробовал шарить на VK, все вроде нормально расшарилось, я не понимаю:, чьл значит лезет к калькулятору<p><img src=\"http://secure.floctopus.info/redactor/t_b40035090926eaf838a102fcb561add1.png\" class=\"img-responsive\" style=\"display: block; margin: auto;\" alt=\"\"></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(162, 69, '2017-10-26 17:59:30', 14, 1, '<p>например Горная Ланка</p><p>586 долларов</p><p>при пересчете в евро (498), показывает в деталях тура 586, и на двоих пишет 1172 ЕВРО!</p><p><br></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(163, 71, '2017-10-26 18:00:51', 14, 1, '<p>та же горная ланка</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(164, 71, '2017-10-26 18:04:03', 14, 1, '<p>там где написано \"на человека\"</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(165, 71, '2017-10-26 18:05:34', 14, 1, '<p>почему floctopus пишет, что всё я пишу сейчас, сделано 6 часов назад?)</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(166, 73, '2017-10-26 18:24:45', 14, 1, '<p>Думаю о том, как расставлять линки на туры в описаниях точек. Например: Точка \"Хортон Плэйн\", а в подробном описании ещё добавлено \"Хортон Плайн мы посещаем в следующих турах: </p><p><a href=\"http://hikkaduwa.com/packages#\" data-id=\"24\" class=\"tour-box\" target=\"_blank\">ЗНАКОМСТВО С ЛАНКОЙ + ПЛАТО ХОРТОНА</a></p><p>И при линке открывается детальное описание тура.:\"</p><p>Но вот где юзер будет оказываться, когда закроет детальное описание тура? Опять в отрытой точке \"Хортон Плэйн\" возможно? </p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(167, 61, '2017-10-26 21:28:07', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(168, 62, '2017-10-26 21:28:44', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(169, 56, '2017-10-26 21:29:10', 1, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(170, 54, '2017-10-26 21:29:28', 1, 0, NULL, 0, 0, 0, 0, 7, 0, 0, NULL, 0),
(171, 67, '2017-10-27 06:21:07', 14, 0, NULL, 10, 0, 0, 0, 0, 0, 0, NULL, 0),
(172, 67, '2017-10-27 06:30:35', 14, 1, '<p>Вставил в документ</p><p>info1 = \"Окончательный план поездки составляется с учетом конкретного времени Вашего прилета и отлета утверждается только после согласования с Вами.\"</p><p>info2 = \"Бронирование утвержденной экспедиции возможно только после внесения предоплаты в размере 12,5% от суммы заказа.\"</p><p>\"Информация\"  в деталях тура не появилась - строки поехали.<br></p><p>В моем документе текстов для \"деталей тура\" и комманды info - нет. </p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(173, 64, '2017-10-27 06:38:11', 14, 0, NULL, 10, 0, 0, 0, 0, 0, 0, NULL, 0),
(174, 64, '2017-10-27 06:45:58', 14, 1, '<p>При при первом расчете дефолтной Горной Ланки из калькулятора, в форме заказа вместо \"горной ланки\" написано  \"итогоовя информация.</p><p>Если перейти в форму заказ из туров, то название тура появляется, но при любом перерасчете из калькулятора больше не меняется.</p><p><br></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(175, 65, '2017-10-27 07:07:00', 1, 1, '<p>Тут можно и нужно загружать картинки</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(176, 65, '2017-10-27 07:07:43', 1, 1, '<p>ни че не понял</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(177, 65, '2017-10-27 07:07:49', 1, 0, NULL, 0, 14, 0, 0, 0, 0, 0, NULL, 0),
(178, 67, '2017-10-27 07:15:36', 1, 1, '<p>О чем мы разговариваем ???? У меня все хорошо выглядит.  КТо куда поехал?  Какя инфа? Загружайте подробные скриншоты</p><p><br></p><p><img src=\"http://secure.floctopus.info/redactor/t_eb02fe50c9cfd6be6a029e12bff73476.png\" class=\"img-responsive\"></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(179, 67, '2017-10-27 07:16:14', 1, 0, NULL, 0, 14, 0, 0, 0, 0, 0, NULL, 0),
(180, 71, '2017-10-27 07:17:56', 1, 1, '<p>Не пойму, что за округление и где не работает??? Можно скриншот????</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(181, 71, '2017-10-27 07:18:03', 1, 0, NULL, 0, 14, 0, 0, 0, 0, 0, NULL, 0),
(182, 64, '2017-10-27 07:24:00', 1, 1, '<a href=\"https://monosnap.com/welcome\">https://monosnap.com/welcome</a>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(183, 65, '2017-10-27 07:30:36', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(184, 71, '2017-10-27 07:33:06', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(185, 72, '2017-10-27 07:35:39', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(186, 73, '2017-10-27 07:37:16', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(187, 32, '2017-10-27 07:40:04', 1, 0, NULL, 20, 0, 0, 0, 0, 0, 0, NULL, 0),
(188, 67, '2017-10-27 07:48:20', 14, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(189, 71, '2017-10-27 08:04:29', 14, 0, NULL, 10, 0, 0, 0, 0, 0, 0, NULL, 0),
(190, 71, '2017-10-27 08:05:27', 14, 1, '<p><img src=\"http://secure.floctopus.info/redactor/t_24114d25e9675c400ecf9c25e9b189fa.jpeg\" class=\"img-responsive\"></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(191, 64, '2017-10-27 08:07:46', 14, 0, NULL, 0, 0, 0, 0, 0, 0, 0, '2017-10-29 00:00:00', 1),
(192, 71, '2017-10-27 08:09:10', 14, 0, NULL, 0, 0, 0, 0, 0, 0, 0, '2017-10-29 00:00:00', 1),
(193, 71, '2017-10-27 08:23:33', 14, 0, NULL, 0, 1, 0, 0, 0, 0, 0, NULL, 0),
(194, 71, '2017-10-27 08:23:43', 14, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(195, 71, '2017-10-27 08:23:45', 14, 0, NULL, 10, 0, 0, 0, 0, 0, 0, NULL, 0),
(196, 71, '2017-10-27 14:11:19', 1, 1, '<p>Какой это тур и при каких условиях? Я не могу повторить</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(197, 76, '2017-10-27 14:11:56', 1, 1, '<p>Что значит не закрывается?</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(198, 76, '2017-10-27 14:21:01', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(199, 75, '2017-10-27 14:29:51', 1, 1, '<p>ok, добавил meta tegs for FB, для vk.com, как и ожидалось все очень убого :</p><p><a href=\"https://valerykoretsky.com/blog/metatags-vk-facebook-google-twitter/\">https://valerykoretsky.com/blog/metatags-vk-facebo...</a><br></p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(200, 75, '2017-10-27 14:29:57', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(201, 64, '2017-10-27 14:39:50', 1, 1, '<p>готово!</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(202, 64, '2017-10-27 14:39:55', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(203, 68, '2017-10-27 16:26:14', 7, 1, '<p>работает, спасибо</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(204, 68, '2017-10-27 16:26:17', 7, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(205, 32, '2017-10-27 16:27:03', 7, 1, '<p>да, сейчас тоже проверила, все ок. </p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(206, 32, '2017-10-27 16:27:07', 7, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(207, 71, '2017-10-27 17:23:17', 14, 1, '<p>всё как на скрине </p><p>тур Горная Ланка</p><p>евро</p><p>любой переход  из любого тура (не из калькулятора)</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(208, 71, '2017-10-27 17:31:04', 1, 1, '<p>ААА, Euro, не доллар, ща</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(209, 71, '2017-10-27 17:40:05', 1, 1, '<p>Исправлено</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(210, 71, '2017-10-27 17:40:11', 1, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(211, 76, '2017-10-27 17:40:44', 14, 1, '<p>\"Чи не закривається\" означає вікно \"Спасибі\" залишалося відкритим після натискання на ОК. Я тикав тикав, але нічого не відбулося. Екран залишався таким, як на скріншоті. Зараз працює. </p><p>Оставляем открытым. Буду ловить.</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(212, 76, '2017-10-27 19:02:52', 1, 1, '<p>Це я цю багу випрвив, вiд тепер все повинно працювати як слiд</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(213, 80, '2017-10-27 19:21:02', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(214, 82, '2017-10-27 19:40:41', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(215, 81, '2017-10-27 19:44:37', 1, 0, NULL, 50, 0, 0, 0, 0, 0, 0, NULL, 0),
(216, 87, '2017-10-29 11:54:56', 1, 0, NULL, 0, 0, 30, 0, 0, 0, 0, NULL, 0),
(217, 92, '2017-10-31 07:19:22', 16, 1, '<p>ок, в работу принято</p>', 0, 0, 0, 0, 0, 0, 0, NULL, 0),
(218, 92, '2017-10-31 07:21:20', 16, 0, NULL, 20, 0, 0, 0, 0, 0, 0, NULL, 0),
(219, 92, '2017-10-31 07:21:43', 16, 0, NULL, 30, 0, 0, 0, 0, 0, 0, NULL, 0),
(220, 93, '2017-10-31 07:40:59', 11, 0, NULL, 0, 16, 0, 0, 0, 0, 0, NULL, 0),
(221, 74, '2017-11-09 10:33:44', 1, 0, NULL, 0, 13, 0, 0, 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `issue_priority`
--

CREATE TABLE `issue_priority` (
  `issueprior_id` int(11) UNSIGNED NOT NULL,
  `issueprior_prior` int(11) DEFAULT NULL,
  `issueprior_name` varchar(100) DEFAULT NULL,
  `issueprior_color` varchar(20) DEFAULT NULL,
  `issueprior_lang` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `issue_priority`
--

INSERT INTO `issue_priority` (`issueprior_id`, `issueprior_prior`, `issueprior_name`, `issueprior_color`, `issueprior_lang`) VALUES
(2, 10, 'none', 'default', 'en'),
(3, 20, 'low', 'info', 'en'),
(4, 30, 'normal', 'primary', 'en'),
(5, 40, 'high', 'warning', 'en'),
(6, 50, 'urgent', 'danger', 'en'),
(7, 60, 'immediate', 'danger', 'en'),
(61, 10, 'нет ', 'default', 'ru'),
(62, 20, 'низкий', 'info', 'ru'),
(63, 30, 'обычный', 'primary', 'ru'),
(64, 40, 'высокий', 'warning', 'ru'),
(65, 50, 'срочно', 'danger', 'ru'),
(66, 60, 'критично', 'danger', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `issue_status`
--

CREATE TABLE `issue_status` (
  `issuestatus_id` int(11) UNSIGNED NOT NULL,
  `issuestatus_status` int(11) NOT NULL DEFAULT '0',
  `issuestatus_name` varchar(100) DEFAULT NULL,
  `issuestatus_color` varchar(20) DEFAULT NULL,
  `issuestatus_lang` varchar(4) DEFAULT 'en',
  `issuestatus_color_hex` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `issue_status`
--

INSERT INTO `issue_status` (`issuestatus_id`, `issuestatus_status`, `issuestatus_name`, `issuestatus_color`, `issuestatus_lang`, `issuestatus_color_hex`) VALUES
(1, 10, 'open', 'danger', 'en', '#ef6262'),
(2, 20, 'in process', 'warning', 'en', '#ffc870'),
(3, 30, 'resolved', 'success', 'en', '#302f2f'),
(4, 40, 'wontfix', 'default', 'en', '#40bbe4'),
(5, 50, 'closed', 'success', 'en', '#68c5b5'),
(6, 10, 'открыт', 'danger', 'ru', '#ef6262'),
(7, 20, 'в процессе', 'warning', 'ru', '#ffc870'),
(8, 30, 'решено', 'success', 'ru', '#302f2f'),
(9, 40, 'не решаемо', 'default', 'ru', '#40bbe4'),
(10, 50, 'закрыт', 'success', 'ru', '#68c5b5');

-- --------------------------------------------------------

--
-- Структура таблицы `languages`
--

CREATE TABLE `languages` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `lang_iso` varchar(4) DEFAULT NULL,
  `lang_name` varchar(200) DEFAULT NULL,
  `lang_order` smallint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `languages`
--

INSERT INTO `languages` (`lang_id`, `lang_iso`, `lang_name`, `lang_order`) VALUES
(1, 'en', 'English', 1),
(2, 'ru', 'Русский', 7),
(3, 'ua', 'Українська Мова', 6),
(4, 'de', 'Deutsche', 3),
(5, 'fr', 'Française', 4),
(6, 'it', 'Ittaliana', 5),
(7, 'es', 'Español', 2),
(8, 'pt', 'Português', 999),
(9, 'tr', 'Türk', 9999);

-- --------------------------------------------------------

--
-- Структура таблицы `log`
--

CREATE TABLE `log` (
  `log_id` bigint(20) NOT NULL,
  `log_user_id` bigint(20) DEFAULT NULL,
  `log_module` varchar(255) DEFAULT NULL,
  `log_object_id` int(11) DEFAULT NULL,
  `log_action` varchar(255) DEFAULT NULL,
  `log_ref_id` int(11) DEFAULT '0',
  `log_mail_to` varchar(255) DEFAULT NULL,
  `log_sent_email` varchar(255) DEFAULT NULL,
  `log_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `log_customer_id` int(11) DEFAULT '0',
  `log_amount` decimal(10,2) DEFAULT '0.00',
  `log_details` text,
  `log_ip` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `log`
--

INSERT INTO `log` (`log_id`, `log_user_id`, `log_module`, `log_object_id`, `log_action`, `log_ref_id`, `log_mail_to`, `log_sent_email`, `log_date`, `log_customer_id`, `log_amount`, `log_details`, `log_ip`) VALUES
(1, 2, 'logout', 0, 'logout', 0, NULL, NULL, '2017-09-23 21:25:38', 0, '0.00', 'User logout', '45.56.148.227'),
(2, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-09-25 10:55:23', 0, '0.00', 'User logout', '172.110.128.70'),
(3, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-09-25 10:58:32', 0, '0.00', 'User logged in', '172.110.128.70'),
(4, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-09-27 19:07:13', 0, '0.00', 'User logged in', '188.163.34.17'),
(5, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-09-29 10:03:44', 0, '0.00', 'User logged in', '188.163.34.17'),
(6, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-09-29 10:24:04', 0, '0.00', 'User logged in', '188.163.34.17'),
(7, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-09-29 10:29:36', 0, '0.00', 'User logged in', '188.163.34.17'),
(8, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-02 16:37:35', 0, '0.00', 'User logged in', '93.79.28.21'),
(9, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 16:01:38', 0, '0.00', 'User logout', '178.162.199.116'),
(10, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 16:05:12', 0, '0.00', 'User logged in', '178.162.199.116'),
(11, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 16:33:39', 0, '0.00', 'User logout', '178.162.199.116'),
(12, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 17:10:48', 0, '0.00', 'User logged in', '178.162.199.116'),
(13, 4, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 17:14:34', 0, '0.00', 'User logout', '178.162.199.116'),
(14, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 17:14:40', 0, '0.00', 'User logged in', '178.162.199.116'),
(15, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 17:15:36', 0, '0.00', 'User logout', '178.162.199.116'),
(16, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 17:15:51', 0, '0.00', 'User logged in', '178.162.199.116'),
(17, 4, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 17:19:53', 0, '0.00', 'User logout', '178.162.199.116'),
(18, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 17:20:02', 0, '0.00', 'User logged in', '178.162.199.116'),
(19, 3, 'users', 13, 'accept', 0, NULL, NULL, '2017-10-03 17:27:45', 0, '0.00', 'User Invitation Accepted', '178.162.199.116'),
(20, 3, 'users', 13, 'del', 0, NULL, NULL, '2017-10-03 17:28:15', 0, '0.00', 'User deleted', '178.162.199.116'),
(21, 3, 'users', 15, 'accept', 0, NULL, NULL, '2017-10-03 17:33:22', 0, '0.00', 'User Invitation Accepted', '178.162.199.116'),
(22, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 17:33:53', 0, '0.00', 'User logout', '178.162.199.116'),
(23, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 17:36:52', 0, '0.00', 'User logged in', '178.162.199.116'),
(24, 4, 'users', 16, 'del', 0, NULL, NULL, '2017-10-03 17:37:37', 0, '0.00', 'User deleted', '178.162.199.116'),
(25, 4, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 17:37:48', 0, '0.00', 'User logout', '178.162.199.116'),
(26, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 17:37:55', 0, '0.00', 'User logged in', '178.162.199.116'),
(27, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 17:39:56', 0, '0.00', 'User logout', '178.162.199.116'),
(28, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 17:40:03', 0, '0.00', 'User logged in', '178.162.199.116'),
(29, 4, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 17:40:45', 0, '0.00', 'User logout', '178.162.199.116'),
(30, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 17:40:53', 0, '0.00', 'User logged in', '178.162.199.116'),
(31, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 18:01:59', 0, '0.00', 'User logout', '178.162.199.116'),
(32, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 18:02:06', 0, '0.00', 'User logged in', '178.162.199.116'),
(33, 4, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-03 18:31:48', 0, '0.00', 'User logout', '178.162.199.116'),
(34, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-03 18:31:53', 0, '0.00', 'User logged in', '178.162.199.116'),
(35, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-12 07:47:40', 0, '0.00', 'User logged in', '93.170.65.221'),
(36, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-12 07:51:57', 0, '0.00', 'User logged in', '93.170.65.221'),
(37, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-12 09:59:13', 0, '0.00', 'User logged in', '93.170.65.221'),
(38, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-12 12:34:43', 0, '0.00', 'User logout', '64.64.117.126'),
(39, 2, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-12 12:36:00', 0, '0.00', 'User logout', '64.64.117.126'),
(40, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-12 12:36:09', 0, '0.00', 'User logged in', '64.64.117.126'),
(41, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-12 15:38:13', 0, '0.00', 'User logout', '107.181.69.199'),
(42, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-12 16:43:33', 0, '0.00', 'User logout', '188.163.34.17'),
(43, 5, 'login', 0, 'login', 0, NULL, NULL, '2017-10-12 23:04:47', 0, '0.00', 'User logged in', '45.56.148.15'),
(44, 5, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-12 23:22:39', 0, '0.00', 'User logout', '45.56.148.15'),
(45, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-12 23:22:47', 0, '0.00', 'User logged in', '45.56.148.15'),
(46, 5, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-12 23:49:05', 0, '0.00', 'User logout', '45.56.148.15'),
(47, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-12 23:49:15', 0, '0.00', 'User logged in', '45.56.148.15'),
(48, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 03:40:06', 0, '0.00', 'User logout', '45.56.149.95'),
(49, 5, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 03:40:17', 0, '0.00', 'User logged in', '45.56.149.95'),
(50, 3, 'users', 19, 'accept', 0, NULL, NULL, '2017-10-13 10:43:57', 0, '0.00', 'User Invitation Accepted', '93.170.69.225'),
(51, 3, 'users', 19, 'del', 0, NULL, NULL, '2017-10-13 10:44:23', 0, '0.00', 'User deleted', '93.170.69.225'),
(52, 3, 'users', 27, 'accept', 0, NULL, NULL, '2017-10-13 10:45:33', 0, '0.00', 'User Invitation Accepted', '93.170.69.225'),
(53, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 10:49:32', 0, '0.00', 'User logged in', '93.170.69.225'),
(54, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 11:07:04', 0, '0.00', 'User logout', '188.163.34.17'),
(55, 5, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 11:18:17', 0, '0.00', 'User logout', '45.56.148.169'),
(56, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 11:18:27', 0, '0.00', 'User logged in', '45.56.148.169'),
(57, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 11:56:31', 0, '0.00', 'User logout', '188.163.34.17'),
(58, 5, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 11:56:44', 0, '0.00', 'User logged in', '188.163.34.17'),
(59, 5, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 11:58:54', 0, '0.00', 'User logout', '188.163.34.17'),
(60, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 11:59:08', 0, '0.00', 'User logged in', '188.163.34.17'),
(61, 3, 'users', 27, 'del', 0, NULL, NULL, '2017-10-13 12:09:49', 0, '0.00', 'User deleted', '188.163.34.17'),
(62, 3, 'users', 29, 'accept', 0, NULL, NULL, '2017-10-13 12:11:49', 0, '0.00', 'User Invitation Accepted', '188.163.34.17'),
(63, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 12:18:49', 0, '0.00', 'User logout', '188.163.34.17'),
(64, 5, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 12:20:29', 0, '0.00', 'User logged in', '188.163.34.17'),
(65, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 12:30:59', 0, '0.00', 'User logout', '188.163.34.17'),
(66, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 13:00:51', 0, '0.00', 'User logout', '188.163.34.17'),
(67, 5, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 13:37:17', 0, '0.00', 'User logout', '188.163.34.17'),
(68, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 13:37:26', 0, '0.00', 'User logged in', '188.163.34.17'),
(69, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 13:41:26', 0, '0.00', 'User logout', '188.163.34.17'),
(70, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 13:41:38', 0, '0.00', 'User logged in', '188.163.34.17'),
(71, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 14:50:54', 0, '0.00', 'User logout', '188.163.34.17'),
(72, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 15:35:24', 0, '0.00', 'User logged in', '188.163.34.17'),
(73, 4, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 18:45:58', 0, '0.00', 'User logout', '93.170.69.225'),
(74, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 18:46:14', 0, '0.00', 'User logged in', '93.170.69.225'),
(75, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 18:46:25', 0, '0.00', 'User logout', '93.170.69.225'),
(76, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 18:46:53', 0, '0.00', 'User logged in', '93.170.69.225'),
(77, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-13 23:58:50', 0, '0.00', 'User logout', '188.163.34.17'),
(78, 5, 'login', 0, 'login', 0, NULL, NULL, '2017-10-13 23:59:00', 0, '0.00', 'User logged in', '188.163.34.17'),
(79, 5, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-14 22:22:37', 0, '0.00', 'User logout', '188.163.34.17'),
(80, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-15 07:17:55', 0, '0.00', 'User logged in', '93.170.69.225'),
(81, 8, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-18 11:29:15', 0, '0.00', 'User logout', '91.235.71.94'),
(82, 9, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-18 11:33:34', 0, '0.00', 'User logout', '91.235.71.94'),
(83, 10, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-18 11:35:09', 0, '0.00', 'User logout', '91.235.71.94'),
(84, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-18 11:35:18', 0, '0.00', 'User logged in', '91.235.71.94'),
(85, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-19 10:14:34', 0, '0.00', 'User logout', '37.58.59.183'),
(86, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-10-19 10:14:40', 0, '0.00', 'User logged in', '37.58.59.183'),
(87, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-19 23:57:15', 0, '0.00', 'User logged in', '46.219.209.209'),
(88, 3, 'users', 49, 'del', 0, NULL, NULL, '2017-10-20 10:17:44', 0, '0.00', 'User deleted', '178.162.200.48'),
(89, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-10-20 10:35:07', 0, '0.00', 'User logged in', '178.150.225.132'),
(90, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-20 12:42:56', 0, '0.00', 'User logout', '91.235.71.94'),
(91, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-20 13:51:16', 0, '0.00', 'User logout', '91.235.71.94'),
(92, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-20 13:56:49', 0, '0.00', 'User logout', '91.235.71.94'),
(93, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-20 13:57:09', 0, '0.00', 'User logged in', '91.235.71.94'),
(94, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-20 13:57:57', 0, '0.00', 'User logout', '91.235.71.94'),
(95, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-20 13:58:13', 0, '0.00', 'User logged in', '91.235.71.94'),
(96, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-20 13:58:14', 0, '0.00', 'User logout', '178.150.225.132'),
(97, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-10-20 13:58:18', 0, '0.00', 'User logged in', '178.150.225.132'),
(98, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-20 14:01:37', 0, '0.00', 'User logout', '91.235.71.94'),
(99, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-20 14:02:20', 0, '0.00', 'User logged in', '91.235.71.94'),
(100, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-20 14:02:56', 0, '0.00', 'User logout', '91.235.71.94'),
(101, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-20 14:03:52', 0, '0.00', 'User logout', '91.235.71.94'),
(102, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-20 14:10:03', 0, '0.00', 'User logged in', '91.235.71.94'),
(103, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-21 00:35:07', 0, '0.00', 'User logout', '46.219.212.65'),
(104, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-10-23 08:06:26', 0, '0.00', 'User logged in', '46.211.104.180'),
(105, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-25 10:54:23', 0, '0.00', 'User logout', '37.0.123.65'),
(106, 12, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-25 10:55:32', 0, '0.00', 'User logout', '37.0.123.65'),
(107, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-25 10:55:45', 0, '0.00', 'User logged in', '37.0.123.65'),
(108, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-25 16:36:07', 0, '0.00', 'User logged in', '188.163.34.17'),
(109, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-25 17:31:46', 0, '0.00', 'User logged in', '188.163.34.17'),
(110, 7, 'login', 0, 'login', 0, NULL, NULL, '2017-10-26 08:51:25', 0, '0.00', 'User logged in', '78.35.172.127'),
(111, 14, 'login', 0, 'login', 0, NULL, NULL, '2017-10-26 13:23:56', 0, '0.00', 'User logged in', '80.147.67.173'),
(112, 1, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-26 17:04:10', 0, '0.00', 'User logout', '188.163.34.17'),
(113, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-26 17:04:26', 0, '0.00', 'User logged in', '188.163.34.17'),
(114, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-26 19:47:45', 0, '0.00', 'User logged in', '188.163.34.17'),
(115, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-10-26 20:36:21', 0, '0.00', 'User logged in', '37.0.123.66'),
(116, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:01:33', 0, '0.00', 'User logout', '178.150.225.132'),
(117, 16, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:05:01', 0, '0.00', 'User logout', '178.150.225.132'),
(118, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-10-31 07:05:06', 0, '0.00', 'User logged in', '178.150.225.132'),
(119, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:16:24', 0, '0.00', 'User logout', '178.150.225.132'),
(120, 16, 'login', 0, 'login', 0, NULL, NULL, '2017-10-31 07:17:21', 0, '0.00', 'User logged in', '178.150.225.132'),
(121, 16, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:22:42', 0, '0.00', 'User logout', '178.150.225.132'),
(122, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-10-31 07:22:46', 0, '0.00', 'User logged in', '178.150.225.132'),
(123, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:27:47', 0, '0.00', 'User logout', '178.150.225.132'),
(124, 16, 'login', 0, 'login', 0, NULL, NULL, '2017-10-31 07:28:12', 0, '0.00', 'User logged in', '178.150.225.132'),
(125, 16, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:29:35', 0, '0.00', 'User logout', '178.150.225.132'),
(126, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-10-31 07:29:41', 0, '0.00', 'User logged in', '178.150.225.132'),
(127, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:32:00', 0, '0.00', 'User logout', '178.150.225.132'),
(128, 17, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:33:00', 0, '0.00', 'User logout', '178.150.225.132'),
(129, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-10-31 07:33:03', 0, '0.00', 'User logged in', '178.150.225.132'),
(130, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:41:59', 0, '0.00', 'User logout', '178.150.225.132'),
(131, 16, 'login', 0, 'login', 0, NULL, NULL, '2017-10-31 07:42:23', 0, '0.00', 'User logged in', '178.150.225.132'),
(132, 16, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 07:47:38', 0, '0.00', 'User logout', '178.150.225.132'),
(133, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-10-31 07:47:41', 0, '0.00', 'User logged in', '178.150.225.132'),
(134, 3, 'logout', 0, 'logout', 0, NULL, NULL, '2017-10-31 09:28:34', 0, '0.00', 'User logout', '93.170.69.225'),
(135, 4, 'login', 0, 'login', 0, NULL, NULL, '2017-10-31 09:28:40', 0, '0.00', 'User logged in', '93.170.69.225'),
(136, 4, 'logout', 0, 'logout', 0, NULL, NULL, '2017-11-01 09:27:05', 0, '0.00', 'User logout', '93.170.69.225'),
(137, 3, 'login', 0, 'login', 0, NULL, NULL, '2017-11-01 09:27:09', 0, '0.00', 'User logged in', '93.170.69.225'),
(138, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-11-04 13:37:02', 0, '0.00', 'User logged in', '37.110.39.29'),
(139, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-11-07 08:37:39', 0, '0.00', 'User logged in', '62.80.186.50'),
(140, 11, 'logout', 0, 'logout', 0, NULL, NULL, '2017-11-08 13:53:25', 0, '0.00', 'User logout', '62.80.186.50'),
(141, 11, 'login', 0, 'login', 0, NULL, NULL, '2017-11-08 13:53:30', 0, '0.00', 'User logged in', '62.80.186.50'),
(142, 1, 'login', 0, 'login', 0, NULL, NULL, '2017-11-09 10:18:38', 0, '0.00', 'User logged in', '91.235.71.94');

-- --------------------------------------------------------

--
-- Структура таблицы `month`
--

CREATE TABLE `month` (
  `mon_id` int(11) NOT NULL,
  `mon_name` varchar(200) DEFAULT NULL,
  `mon_num` tinyint(4) DEFAULT NULL,
  `mon_full_name` varchar(100) DEFAULT NULL,
  `mon_lang` varchar(3) NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `month`
--

INSERT INTO `month` (`mon_id`, `mon_name`, `mon_num`, `mon_full_name`, `mon_lang`) VALUES
(1, 'Jan', 1, 'January', 'en'),
(2, 'Feb', 2, 'February', 'en'),
(3, 'Mar', 3, 'March', 'en'),
(4, 'Apr', 4, 'April', 'en'),
(5, 'May', 5, 'May', 'en'),
(6, 'Jun', 6, 'June', 'en'),
(7, 'Jul', 7, 'July', 'en'),
(8, 'Aug', 8, 'August', 'en'),
(9, 'Sep', 9, 'September', 'en'),
(10, 'Oct', 10, 'October', 'en'),
(11, 'Nov', 11, 'November', 'en'),
(12, 'Dec', 12, 'December', 'en'),
(13, 'Янв', 1, 'Январь', 'ru'),
(14, 'Фев', 2, 'Февраль', 'ru'),
(15, 'Март', 3, 'Март', 'ru'),
(16, 'Апр', 4, 'Апрель', 'ru'),
(17, 'Май', 5, 'Май', 'ru'),
(18, 'Июнь', 6, 'Июнь', 'ru'),
(19, 'Июль', 7, 'Июль', 'ru'),
(20, 'Авг', 8, 'Август', 'ru'),
(21, 'Сен', 9, 'Сентябрь', 'ru'),
(22, 'Окт', 10, 'Октябрь', 'ru'),
(23, 'Ноя', 11, 'Ноябрь', 'ru'),
(24, 'Дек', 12, 'Декабрь', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `notes`
--

CREATE TABLE `notes` (
  `note_id` int(11) UNSIGNED NOT NULL,
  `note_user_id` int(11) NOT NULL DEFAULT '0',
  `note_subj` varchar(255) DEFAULT NULL,
  `note_text` text NOT NULL,
  `note_trash` int(11) NOT NULL DEFAULT '0',
  `note_tags` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notes`
--

INSERT INTO `notes` (`note_id`, `note_user_id`, `note_subj`, `note_text`, `note_trash`, `note_tags`) VALUES
(1, 1, '', '<p>Dns Zone check is enabled.</p><p>+===================================+</p><p>| New Account Info                  |</p><p>+===================================+</p><p>| Domain: allamericannotes.com</p><p>| Ip: 67.227.189.166 (n)</p><p>| HasCgi: y</p><p>| UserName: allamericannotes</p><p>| PassWord: Zanzibar2017</p><p>| CpanelMod: paper_lantern</p><p>| HomeRoot: /home</p><p>| Quota: unlimited</p><p>| NameServer1: ns1.managemart.com</p><p>| NameServer2: ns2.managemart.com</p><p>| NameServer3: </p><p>| NameServer4: </p><p>| Contact Email: ivafirst@gmail.com</p><p>| Package: default</p><p>| Feature List: default</p><p>| Language: en</p><p>+===================================+</p>', 0, ' <p>Dns Zone check is enabled.</p><p>+===================================+</p><p>| New Account Info                  |</p><p>+===================================+</p><p>| Domain: allamericannotes.com</p><p>| Ip: 67.227.189.166 (n)</p><p>| HasCgi: y</p><p>| UserName: allamericannotes</p><p>| PassWord: Zanzibar2017</p><p>| CpanelMod: paper_lantern</p><p>| HomeRoot: /home</p><p>| Quota: unlimited</p><p>| NameServer1: ns1.managemart.com</p><p>| NameServer2: ns2.managemart.com</p><p>| NameServer3: </p><p>| NameServer4: </p><p>| Contact Email: ivafirst@gmail.com</p><p>| Package: default</p><p>| Feature List: default</p><p>| Language: en</p><p>+===================================+</p>'),
(2, 1, 'advokatura.lg.ua', '<p>Dns Zone check is enabled.</p><pre>+===================================+\r\n| New Account Info                  |\r\n+===================================+\r\n| Domain: advokatura.lg.ua\r\n| Ip: 67.227.189.166 (n)\r\n| HasCgi: y\r\n| UserName: advokaturalg\r\n| PassWord: uspeh33\r\n| CpanelMod: paper_lantern\r\n| HomeRoot: /home\r\n| Quota: unlimited\r\n| NameServer1: ns1.managemart.com\r\n| NameServer2: ns2.managemart.com\r\n| NameServer3: \r\n| NameServer4: \r\n| Contact Email: ivafirst@gmail.com\r\n| Package: default\r\n| Feature List: default\r\n| Language: en\r\n+===================================+\r\n...Done</pre>', 0, 'advokatura.lg.ua <p>Dns Zone check is enabled.</p><pre>+===================================+\r\n| New Account Info                  |\r\n+===================================+\r\n| Domain: advokatura.lg.ua\r\n| Ip: 67.227.189.166 (n)\r\n| HasCgi: y\r\n| UserName: advokaturalg\r\n| PassWord: uspeh33\r\n| CpanelMod: paper_lantern\r\n| HomeRoot: /home\r\n| Quota: unlimited\r\n| NameServer1: ns1.managemart.com\r\n| NameServer2: ns2.managemart.com\r\n| NameServer3: \r\n| NameServer4: \r\n| Contact Email: ivafirst@gmail.com\r\n| Package: default\r\n| Feature List: default\r\n| Language: en\r\n+===================================+\r\n...Done</pre>'),
(3, 1, 'apple ID', '<p style=\"margin-left: 20px;\">booperx@live.com</p><p style=\"margin-left: 20px;\">Kasablanka2017$</p>', 0, 'apple ID <p style=\"margin-left: 20px;\">booperx@live.com</p><p style=\"margin-left: 20px;\">Kasablanka2017$</p>'),
(4, 3, 'Незнайка', '<img src=\"http://secure.floctopus.info/redactor/t_024beb302690918d7a4a76ee17169acc.jpeg\" class=\"img-responsive\">', 0, 'Незнайка <img src=\"http://secure.floctopus.info/redactor/t_024beb302690918d7a4a76ee17169acc.jpeg\" class=\"img-responsive\">'),
(5, 3, 'Водород', '<img src=\"http://secure.floctopus.info/redactor/c4b31e8558437b69a2f191da06049eca.jpeg\" class=\"img-responsive\">', 0, 'Водород <img src=\"http://secure.floctopus.info/redactor/c4b31e8558437b69a2f191da06049eca.jpeg\" class=\"img-responsive\">'),
(6, 3, '', '<img src=\"http://secure.floctopus.info/redactor/ca3d07a8ddc4ebba6f0dae6b8d7f19d0.jpeg\" class=\"img-responsive\">', 1, ' <img src=\"http://secure.floctopus.info/redactor/ca3d07a8ddc4ebba6f0dae6b8d7f19d0.jpeg\" class=\"img-responsive\">'),
(7, 11, 'пропроп', '<p>пнгаган</p>', 1, 'пропроп <p>пнгаган</p>'),
(8, 1, 'ios developer', '<p>junior iOS Developer</p><p>Необходимые навыки:</p><ul><li>Знание iOS SDK/Xcode, основных фреймворков</li><li>Знание Swift / Objective C </li><li>Понимание RESTful API, JSON and XML,</li><li>Умение анализировать и работать над улучшением кода</li><li>Опыт разработки под iOS (iPhone/iPad)</li><li>GIT,SVN</li></ul><p>Опыт работы от 1 года</p>', 0, 'ios developer <p>junior iOS Developer</p><p>Необходимые навыки:</p><ul><li>Знание iOS SDK/Xcode, основных фреймворков</li><li>Знание Swift / Objective C </li><li>Понимание RESTful API, JSON and XML,</li><li>Умение анализировать и работать над улучшением кода</li><li>Опыт разработки под iOS (iPhone/iPad)</li><li>GIT,SVN</li></ul><p>Опыт работы от 1 года</p>'),
(9, 1, 'Bitcoin Wallet', '<p>3HFf9yzNLTCyfhzE2n5UFjgng1hcvXV51V</p>', 0, 'Bitcoin Wallet <p>3HFf9yzNLTCyfhzE2n5UFjgng1hcvXV51V</p>'),
(10, 15, 'Заметил', '<p>Это заметка просто один</p><p><a href=\"http://www.google.com\">http://www.google.com</a></p>', 0, 'Заметил <p>Это заметка просто один</p><p><a href=\"http://www.google.com\">http://www.google.com</a></p>'),
(11, 1, 'odeneto.com', '<p>odeneto.com</p><p>U-$h)A357RVL^Peq</p>', 0, 'odeneto.com <p>odeneto.com</p><p>U-$h)A357RVL^Peq</p>'),
(12, 1, 'Wave wallet backup', '<p><br></p><ol><li>Cry</li><li>Cause</li><li>check</li><li>wolf</li><li>novel</li><li>strategy</li><li>apple</li><li>young</li><li>warfare</li><li>veteran</li><li>orient</li><li>segment</li><li>bleak</li><li>forward</li><li>north<span></span></li></ol>', 0, 'Wave wallet backup <p><br></p><ol><li>Cry</li><li>Cause</li><li>check</li><li>wolf</li><li>novel</li><li>strategy</li><li>apple</li><li>young</li><li>warfare</li><li>veteran</li><li>orient</li><li>segment</li><li>bleak</li><li>forward</li><li>north<span></span></li></ol>'),
(13, 1, 'btc-trade.com.ua', '<p><img src=\"https://btc-trade.com.ua/pin_image/e01d863dc536be5bef5364f8a30623ea367f63373c5e8d36d65df5de4bb203a9\">PIN: NFXIG</p><p><img src=\"http://secure.floctopus.info/redactor/791c19f3d9a13265456b5a68daf18377.png\" class=\"img-responsive\"></p>', 0, 'btc-trade.com.ua <p><img src=\"https://btc-trade.com.ua/pin_image/e01d863dc536be5bef5364f8a30623ea367f63373c5e8d36d65df5de4bb203a9\">PIN: NFXIG</p><p><img src=\"http://secure.floctopus.info/redactor/791c19f3d9a13265456b5a68daf18377.png\" class=\"img-responsive\"></p>'),
(14, 1, 'blockchain.com', '<p><label>Wallet ID</label></p><p>Wallet ID is your unique identifier. It is completely individual to you, and it is what you will use to log in and access your wallet. It is <strong>not</strong> an address for sending or receiving. <span class=\"security-red\">Do not share your Wallet ID with others.</span></p><p><label>ded40eac-70e4-4acf-a4fc-6dc013232abf</label></p><p>Ether:</p><p><label>Address</label>: 0x04d096A076237d01Bb890Ace877624c621E6eAE8</p><p><label>Balance</label>: 0 ETH</p><p><label style=\"background-color: initial;\">Private Key</label>: 14d9e295fdfb538d66a94789e29474f4be76dcd5f7dc278b6413bb884dcee6d9</p><p><br></p><p><strong>Write It Down</strong></p><p>Use a pen to legibly write down the following 12 words onto your printed Recovery Sheet. It is important that you write down the words exactly as they appear here and in this order.</p><p><strong>1.deal</strong></p><p><strong>2.relief</strong></p><p><strong>3.abstract</strong></p><p><strong>4.tomorrow</strong></p><p><strong>Write It Down</strong></p><p>Use a pen to legibly write down the following 12 words onto your printed Recovery Sheet. It is important that you write down the words exactly as they appear here and in this order.</p><p><strong>5.gold</strong></p><p><strong>6.blossom</strong></p><p><strong>7.stadium</strong></p><p><strong></strong></p><p><strong>8.caution</strong></p><p><strong>9.direct</strong></p><p><strong>10.acoustic</strong></p><p><strong>11.favorite</strong></p><p><strong></strong></p><p><strong>12.tired</strong></p><p><strong>BITCOIN:</strong></p><p><strong><a href=\"https://www.blockchain.com/\"></a>12afGnd92z8fE55n9RUFDBtGPqVLxQfiWP<br></strong></p><p><strong><br></strong></p>', 0, 'blockchain.com <p><label>Wallet ID</label></p><p>Wallet ID is your unique identifier. It is completely individual to you, and it is what you will use to log in and access your wallet. It is <strong>not</strong> an address for sending or receiving. <span class=\"security-red\">Do not share your Wallet ID with others.</span></p><p><label>ded40eac-70e4-4acf-a4fc-6dc013232abf</label></p><p>Ether:</p><p><label>Address</label>: 0x04d096A076237d01Bb890Ace877624c621E6eAE8</p><p><label>Balance</label>: 0 ETH</p><p><label style=\"background-color: initial;\">Private Key</label>: 14d9e295fdfb538d66a94789e29474f4be76dcd5f7dc278b6413bb884dcee6d9</p><p><br></p><p><strong>Write It Down</strong></p><p>Use a pen to legibly write down the following 12 words onto your printed Recovery Sheet. It is important that you write down the words exactly as they appear here and in this order.</p><p><strong>1.deal</strong></p><p><strong>2.relief</strong></p><p><strong>3.abstract</strong></p><p><strong>4.tomorrow</strong></p><p><strong>Write It Down</strong></p><p>Use a pen to legibly write down the following 12 words onto your printed Recovery Sheet. It is important that you write down the words exactly as they appear here and in this order.</p><p><strong>5.gold</strong></p><p><strong>6.blossom</strong></p><p><strong>7.stadium</strong></p><p><strong></strong></p><p><strong>8.caution</strong></p><p><strong>9.direct</strong></p><p><strong>10.acoustic</strong></p><p><strong>11.favorite</strong></p><p><strong></strong></p><p><strong>12.tired</strong></p><p><strong>BITCOIN:</strong></p><p><strong><a href=\"https://www.blockchain.com/\"></a>12afGnd92z8fE55n9RUFDBtGPqVLxQfiWP<br></strong></p><p><strong><br></strong></p>'),
(15, 1, 'cv', '<p>My name is Valerii, i am from Ukraine. </p><p>I am experienced (more then 12 years) in  web development with LAMP technologies using PHP, MySQL, Javascript, HTML/CSS and AWS web services. I have a working experience with Model View Controller (MVC). I have also designed and developed web user controls, master pages, validation controls, CSS files using technologies like AJAX Toolkit, JQuery, JavaScript,Bootstrap framework, XML, HTML and DHTML.</p><p>When working on a new project, I like to speak with my clients so that I can have a clear understanding of their needs and vision of the project. Thank you in advance for your time and consideration. I look forward to working with you soon.</p><p>Here some of my work:</p><p><a href=\"https://www.managemart.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=https://www.managemart.com&source=gmail&ust=1510088825377000&usg=AFQjCNGJNBpcD086RnUw1iqGe4IJcI6pSQ\">https://www.managemart.com</a></p><p><a href=\"https://www.lawnprosoftware.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=https://www.lawnprosoftware.com&source=gmail&ust=1510088825377000&usg=AFQjCNFirirwBd34QaAStHVtoZ4k2vI0HQ\">https://www.lawnprosoftware.<wbr>com</a> </p><p><a href=\"https://www.scapersoft.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=https://www.scapersoft.com&source=gmail&ust=1510088825377000&usg=AFQjCNFuHehYO-HnV-N-oCIH80KaDrag7g\">https://www.scapersoft.com</a> </p><p><a href=\"http://www/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=http://www&source=gmail&ust=1510088825377000&usg=AFQjCNH4QLlCgxi-bmo_vsC8NZUgx0tSvg\">http://www</a>.<a href=\"http://floctopus.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=http://floctopus.com&source=gmail&ust=1510088825377000&usg=AFQjCNEf3M33S-YjiU6SMM35qZIfN5BqFQ\">floctopus.com</a></p><p><a href=\"https://www.wodtees.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=https://www.wodtees.com&source=gmail&ust=1510088825377000&usg=AFQjCNFQVjKMpu4Cfw_anMBE9XzT29QiUA\">https://www.wodtees.com</a></p><p>.. </p><p>Contact me if you are interesting in my service. Thank you </p>', 0, 'cv <p>My name is Valerii, i am from Ukraine. </p><p>I am experienced (more then 12 years) in  web development with LAMP technologies using PHP, MySQL, Javascript, HTML/CSS and AWS web services. I have a working experience with Model View Controller (MVC). I have also designed and developed web user controls, master pages, validation controls, CSS files using technologies like AJAX Toolkit, JQuery, JavaScript,Bootstrap framework, XML, HTML and DHTML.</p><p>When working on a new project, I like to speak with my clients so that I can have a clear understanding of their needs and vision of the project. Thank you in advance for your time and consideration. I look forward to working with you soon.</p><p>Here some of my work:</p><p><a href=\"https://www.managemart.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=https://www.managemart.com&source=gmail&ust=1510088825377000&usg=AFQjCNGJNBpcD086RnUw1iqGe4IJcI6pSQ\">https://www.managemart.com</a></p><p><a href=\"https://www.lawnprosoftware.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=https://www.lawnprosoftware.com&source=gmail&ust=1510088825377000&usg=AFQjCNFirirwBd34QaAStHVtoZ4k2vI0HQ\">https://www.lawnprosoftware.<wbr>com</a> </p><p><a href=\"https://www.scapersoft.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=https://www.scapersoft.com&source=gmail&ust=1510088825377000&usg=AFQjCNFuHehYO-HnV-N-oCIH80KaDrag7g\">https://www.scapersoft.com</a> </p><p><a href=\"http://www/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=http://www&source=gmail&ust=1510088825377000&usg=AFQjCNH4QLlCgxi-bmo_vsC8NZUgx0tSvg\">http://www</a>.<a href=\"http://floctopus.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=http://floctopus.com&source=gmail&ust=1510088825377000&usg=AFQjCNEf3M33S-YjiU6SMM35qZIfN5BqFQ\">floctopus.com</a></p><p><a href=\"https://www.wodtees.com/\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=ru&q=https://www.wodtees.com&source=gmail&ust=1510088825377000&usg=AFQjCNFQVjKMpu4Cfw_anMBE9XzT29QiUA\">https://www.wodtees.com</a></p><p>.. </p><p>Contact me if you are interesting in my service. Thank you </p>'),
(16, 19, 'Floctopus', '<p><img src=\"http://secure.floctopus.info/redactor/t_9ad36b2d35521c079d1bd34079998320.jpeg\" class=\"img-responsive\"></p>', 0, 'Floctopus <p><img src=\"http://secure.floctopus.info/redactor/t_9ad36b2d35521c079d1bd34079998320.jpeg\" class=\"img-responsive\"></p>'),
(17, 1, 'Всленские законы процветания', '<p><br></p><p>1. Закон пустоты. Что бы получить что то надо избавиться от старого. Природа не терпит пустоты. </p><p>2. Закон циркуляции. Скупость и постоянное накопление ведут к застою. Чувство обладания не должно быть - все на время! Все должно меняться и заменяться. Каждые пол - года или год новая техника и одежда. Все надо менять. Первый и второй закон связаны. Трата приводит к новому доходу. Циркуляция денег приводит к возникновении энергии, которая привлекает еще больше денег.</p><p>3. Закон воображения. Процветание создается в голове!!! Мысли программируют реальность! Эмоции должны учавствовать в процессе привлечения!!!</p><p><img src=\"http://secure.floctopus.info/redactor/t_7ed6a03c1f14ee2abd1ec24f6d4710c2.jpeg\" class=\"img-responsive\" style=\"display: block; margin: auto;\" alt=\"\"></p><p>4. Закон Идей- закон творчества.  Любая идея реализуется!!! Идеи основа сознания!!! ИЗМЕНИТЬ ОТНОШЕНИЕ К РАСХОДАМ! МОИ КРЕДИТОРЫ - МОИ ПАРТНЕРЫ! Завладеть сознанием вселенной<br></p><p>5. Закон воздаяния и получения!  Закон - законов Все что подчиняется принципу обмена одной ценности на другую! Все отданное возвращается преумноженным ! Чем больше я даю, тем больше получаю в замен!!!!! Нужно приветствовать свои врожденные таланты! Нет случайностей и совпадений - все имеет причину. Все обстоятельства моей жизни - результат причин, которые я создал - отдавать надо добро и буду получать добро!</p><p>6. Закон десятины! Надо отдавать 10% от всех доходов источнику моего духовного роста. Уплата десятины - возвращение 10% того, что получаю. Если не отдовать осознанно эти 10% вселенная заберет их сама, но в крайне неудобной ситуации</p><p>7. Закон прощения. Необходимо простить и отпустить всех и вся, кто сделал или намеревался делать мне зло. Необходимо простить себя за все что сам с собой сделал!!</p><p><br></p><p><br></p>', 0, 'Всленские законы процветания <p><br></p><p>1. Закон пустоты. Что бы получить что то надо избавиться от старого. Природа не терпит пустоты. </p><p>2. Закон циркуляции. Скупость и постоянное накопление ведут к застою. Чувство обладания не должно быть - все на время! Все должно меняться и заменяться. Каждые пол - года или год новая техника и одежда. Все надо менять. Первый и второй закон связаны. Трата приводит к новому доходу. Циркуляция денег приводит к возникновении энергии, которая привлекает еще больше денег.</p><p>3. Закон воображения. Процветание создается в голове!!! Мысли программируют реальность! Эмоции должны учавствовать в процессе привлечения!!!</p><p><img src=\"http://secure.floctopus.info/redactor/t_7ed6a03c1f14ee2abd1ec24f6d4710c2.jpeg\" class=\"img-responsive\" style=\"display: block; margin: auto;\" alt=\"\"></p><p>4. Закон Идей- закон творчества.  Любая идея реализуется!!! Идеи основа сознания!!! ИЗМЕНИТЬ ОТНОШЕНИЕ К РАСХОДАМ! МОИ КРЕДИТОРЫ - МОИ ПАРТНЕРЫ! Завладеть сознанием вселенной<br></p><p>5. Закон воздаяния и получения!  Закон - законов Все что подчиняется принципу обмена одной ценности на другую! Все отданное возвращается преумноженным ! Чем больше я даю, тем больше получаю в замен!!!!! Нужно приветствовать свои врожденные таланты! Нет случайностей и совпадений - все имеет причину. Все обстоятельства моей жизни - результат причин, которые я создал - отдавать надо добро и буду получать добро!</p><p>6. Закон десятины! Надо отдавать 10% от всех доходов источнику моего духовного роста. Уплата десятины - возвращение 10% того, что получаю. Если не отдовать осознанно эти 10% вселенная заберет их сама, но в крайне неудобной ситуации</p><p>7. Закон прощения. Необходимо простить и отпустить всех и вся, кто сделал или намеревался делать мне зло. Необходимо простить себя за все что сам с собой сделал!!</p><p><br></p><p><br></p>'),
(18, 1, 'Офис Киев', '<p>Ул. Петра Калнышевского 7</p>', 0, 'Офис Киев <p>Ул. Петра Калнышевского 7</p>'),
(19, 1, 'Ежедневный отчет', '<p>  1) How many hours you worked <br>   2) What you accomplished (not what you did, but what you accomplished) <br>   3) What problems you encountered, and <br>   4) What questions you have for me. I will review this immediately every night and answer your questions.</p>', 0, 'Ежедневный отчет <p>  1) How many hours you worked <br>   2) What you accomplished (not what you did, but what you accomplished) <br>   3) What problems you encountered, and <br>   4) What questions you have for me. I will review this immediately every night and answer your questions.</p>'),
(20, 1, 'Телегруп Украина', '<p>Телегрупп Украина Договор  0101004604. Тел 044 303 91 00, 044 303 64 50. Тех. поддержка 050 591 91 00</p><p><a href=\"http://my.telegroup.ua/my_net_account.php\">http://my.telegroup.ua/my_net_account.php</a></p><p>договор 0101004604</p><p>пароль 0952596451</p>', 0, 'Телегруп Украина <p>Телегрупп Украина Договор  0101004604. Тел 044 303 91 00, 044 303 64 50. Тех. поддержка 050 591 91 00</p><p><a href=\"http://my.telegroup.ua/my_net_account.php\">http://my.telegroup.ua/my_net_account.php</a></p><p>договор 0101004604</p><p>пароль 0952596451</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE `notifications` (
  `n_id` int(11) NOT NULL,
  `n_user_id` int(11) NOT NULL DEFAULT '0',
  `n_src` int(11) DEFAULT '0',
  `n_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `n_subj` varchar(255) DEFAULT NULL,
  `n_text` text,
  `n_status` int(11) DEFAULT '0',
  `n_status_preview` int(11) DEFAULT '0',
  `n_type` varchar(255) DEFAULT NULL,
  `n_object_id` int(11) NOT NULL DEFAULT '0',
  `n_customer` int(11) NOT NULL DEFAULT '1',
  `n_link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notifications`
--

INSERT INTO `notifications` (`n_id`, `n_user_id`, `n_src`, `n_date`, `n_subj`, `n_text`, `n_status`, `n_status_preview`, `n_type`, `n_object_id`, `n_customer`, `n_link`) VALUES
(1, 1, 0, '2017-09-21 11:09:40', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(2, 1, 0, '2017-09-21 11:34:18', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(3, 2, 0, '2017-09-21 15:11:19', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(4, 1, 1, '2017-09-21 16:01:34', NULL, NULL, 0, 1, 'invite_received', 2, 1, '/users/invites'),
(5, 1, 1, '2017-09-21 16:03:33', NULL, NULL, 0, 1, 'invite_received', 2, 1, '/users/invites'),
(6, 1, 1, '2017-09-21 16:06:47', NULL, NULL, 0, 1, 'invite_received', 2, 1, '/users/invites'),
(7, 1, 1, '2017-09-21 19:42:24', NULL, NULL, 0, 1, 'colab_del', 2, 1, '/users'),
(8, 1, 1, '2017-09-21 19:42:53', NULL, NULL, 0, 1, 'invite_received', 2, 1, '/users/invites'),
(9, 1, 1, '2017-09-21 19:43:20', NULL, NULL, 0, 1, 'colab_del', 2, 1, '/users'),
(10, 1, 1, '2017-09-21 21:37:41', NULL, NULL, 0, 1, 'invite_received', 2, 1, '/users/invites'),
(11, 1, 1, '2017-09-21 21:37:57', NULL, NULL, 0, 1, 'colab_del', 2, 1, '/users'),
(12, 1, 1, '2017-09-21 21:38:32', NULL, NULL, 0, 1, 'invite_received', 2, 1, '/users/invites'),
(13, 2, 1, '2017-09-21 21:38:44', NULL, NULL, 0, 1, 'invite_accept', 1, 1, '/users'),
(14, 3, 0, '2017-09-26 06:01:24', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(15, 1, 1, '2017-09-26 06:04:04', NULL, NULL, 0, 1, 'invite_received', 3, 1, '/users/invites'),
(16, 3, 1, '2017-09-27 19:07:44', NULL, NULL, 0, 1, 'invite_accept', 1, 1, '/users'),
(17, 1, 1, '2017-10-02 11:24:36', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/4'),
(18, 4, 0, '2017-10-03 16:02:20', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(19, 4, 1, '2017-10-03 16:33:23', NULL, NULL, 0, 1, 'invite_received', 3, 1, '/users/invites'),
(20, 4, 1, '2017-10-03 16:33:32', NULL, NULL, 0, 1, 'colab_del', 3, 1, '/users'),
(21, 4, 1, '2017-10-03 17:15:03', NULL, NULL, 0, 1, 'invite_received', 3, 1, '/users/invites'),
(22, 4, 1, '2017-10-03 17:27:45', NULL, NULL, 0, 1, 'invite_accept', 3, 1, '/users'),
(23, 4, 1, '2017-10-03 17:28:15', NULL, NULL, 0, 1, 'colab_del', 3, 1, '/users'),
(24, 4, 1, '2017-10-03 17:33:11', NULL, NULL, 0, 1, 'invite_received', 3, 1, '/users/invites'),
(25, 4, 1, '2017-10-03 17:33:22', NULL, NULL, 0, 1, 'invite_accept', 3, 1, '/users'),
(26, 3, 1, '2017-10-03 17:37:37', NULL, NULL, 0, 1, 'colab_del', 4, 1, '/users'),
(27, 4, 1, '2017-10-03 17:38:56', NULL, NULL, 0, 1, 'invite_received', 3, 1, '/users/invites'),
(28, 3, 1, '2017-10-03 17:40:34', NULL, NULL, 0, 1, 'invite_accept', 4, 1, '/users'),
(29, 3, 1, '2017-10-12 07:35:07', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/6'),
(30, 3, 1, '2017-10-12 07:40:50', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/7'),
(31, 2, 1, '2017-10-12 12:15:39', NULL, NULL, 0, 1, 'invite_received', 3, 1, '/users/invites'),
(32, 5, 0, '2017-10-12 15:40:17', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(33, 5, 1, '2017-10-13 03:37:26', NULL, NULL, 0, 1, 'invite_received', 1, 1, '/users/invites'),
(34, 1, 1, '2017-10-13 03:40:29', NULL, NULL, 0, 1, 'invite_accept', 5, 1, '/users'),
(35, 5, 1, '2017-10-13 03:55:56', NULL, NULL, 0, 1, 'colab_del', 1, 1, '/users'),
(36, 2, 1, '2017-10-13 03:56:51', NULL, NULL, 0, 0, 'colab_del', 1, 1, '/users'),
(37, 5, 1, '2017-10-13 03:58:43', NULL, NULL, 0, 1, 'invite_received', 1, 1, '/users/invites'),
(38, 1, 1, '2017-10-13 03:59:32', NULL, NULL, 0, 1, 'invite_accept', 5, 1, '/users'),
(39, 5, 1, '2017-10-13 03:59:49', NULL, NULL, 0, 1, 'colab_del', 1, 1, '/users'),
(40, 5, 1, '2017-10-13 04:03:03', NULL, NULL, 0, 1, 'invite_received', 1, 1, '/users/invites'),
(41, 1, 1, '2017-10-13 04:05:00', NULL, NULL, 0, 1, 'colab_del', 5, 1, '/users'),
(42, 2, 1, '2017-10-13 10:43:57', NULL, NULL, 0, 0, 'invite_accept', 3, 1, '/users'),
(43, 2, 1, '2017-10-13 10:44:23', NULL, NULL, 0, 0, 'colab_del', 3, 1, '/users'),
(44, 2, 1, '2017-10-13 10:44:38', NULL, NULL, 0, 0, 'invite_received', 3, 1, '/users/invites'),
(45, 2, 1, '2017-10-13 10:45:33', NULL, NULL, 0, 0, 'invite_accept', 3, 1, '/users'),
(46, 2, 1, '2017-10-13 12:09:49', NULL, NULL, 0, 0, 'colab_del', 3, 1, '/users'),
(47, 2, 1, '2017-10-13 12:10:32', NULL, NULL, 0, 0, 'invite_received', 3, 1, '/users/invites'),
(48, 2, 1, '2017-10-13 12:11:49', NULL, NULL, 0, 0, 'invite_accept', 3, 1, '/users'),
(49, 1, 1, '2017-10-13 12:20:41', NULL, NULL, 0, 1, 'invite_received', 5, 1, '/users/invites'),
(50, 5, 1, '2017-10-13 12:21:26', NULL, NULL, 0, 1, 'invite_received', 3, 1, '/users/invites'),
(51, 2, 1, '2017-10-13 12:48:41', NULL, NULL, 0, 0, 'colab_del', 3, 1, '/users'),
(52, 5, 1, '2017-10-13 12:49:14', NULL, NULL, 0, 1, 'colab_del', 3, 1, '/users'),
(53, 5, 1, '2017-10-13 13:58:38', NULL, NULL, 0, 1, 'invite_received', 3, 1, '/users/invites'),
(54, 6, 0, '2017-10-13 14:51:16', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(55, 6, 1, '2017-10-13 14:52:39', NULL, NULL, 0, 1, 'invite_received', 1, 1, '/users/invites'),
(56, 5, 1, '2017-10-13 23:59:22', NULL, NULL, 0, 1, 'invite_accept', 1, 1, '/users'),
(57, 1, 1, '2017-10-14 00:00:53', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/13'),
(58, 7, 0, '2017-10-14 11:06:10', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(59, 1, 1, '2017-10-14 11:08:43', NULL, NULL, 0, 1, 'invite_received', 7, 1, '/users/invites'),
(60, 7, 1, '2017-10-14 11:09:24', NULL, NULL, 0, 1, 'invite_accept', 1, 1, '/users'),
(61, 3, 1, '2017-10-14 16:12:00', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/16'),
(62, 5, 1, '2017-10-14 16:12:00', NULL, NULL, 0, 0, 'prj_add', 0, 1, '/projects/view/16'),
(63, 5, 1, '2017-10-14 18:56:27', NULL, NULL, 0, 0, 'prj_add', 0, 1, '/projects/view/7'),
(64, 3, 1, '2017-10-14 18:59:58', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/7'),
(65, 3, 1, '2017-10-14 19:02:27', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/7'),
(66, 3, 1, '2017-10-14 19:20:51', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/7'),
(67, 7, 1, '2017-10-14 19:25:50', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/12'),
(68, 5, 1, '2017-10-14 21:42:36', NULL, NULL, 0, 0, 'prj_add', 0, 1, '/projects/view/7'),
(69, 8, 0, '2017-10-14 22:23:03', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(70, 1, 1, '2017-10-14 22:24:23', NULL, NULL, 0, 1, 'invite_received', 8, 1, '/users/invites'),
(71, 8, 1, '2017-10-14 22:59:59', NULL, NULL, 0, 1, 'invite_accept', 1, 1, '/users'),
(72, 8, 1, '2017-10-14 23:00:51', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/7'),
(73, 5, 1, '2017-10-18 09:23:53', NULL, NULL, 0, 0, 'colab_del', 3, 1, '/users'),
(74, 9, 0, '2017-10-18 11:29:56', NULL, NULL, 0, 0, 'acc_created', 0, 1, '/'),
(75, 1, 1, '2017-10-18 11:31:02', NULL, NULL, 0, 1, 'invite_received', 9, 1, '/users/invites'),
(76, 9, 1, '2017-10-18 11:31:16', NULL, NULL, 0, 0, 'invite_accept', 1, 1, '/users'),
(77, 9, 1, '2017-10-18 11:31:40', NULL, NULL, 0, 0, 'prj_add', 0, 1, '/projects/view/7'),
(78, 10, 0, '2017-10-18 11:33:51', NULL, NULL, 0, 0, 'acc_created', 0, 1, '/'),
(79, 1, 1, '2017-10-18 11:34:37', NULL, NULL, 0, 1, 'invite_received', 10, 1, '/users/invites'),
(80, 10, 1, '2017-10-18 11:34:48', NULL, NULL, 0, 0, 'invite_accept', 1, 1, '/users'),
(81, 11, 0, '2017-10-18 12:34:37', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(82, 1, 1, '2017-10-18 12:37:32', NULL, NULL, 0, 1, 'invite_received', 11, 1, '/users/invites'),
(83, 11, 1, '2017-10-18 12:38:20', NULL, NULL, 0, 1, 'invite_accept', 1, 1, '/users'),
(84, 11, 1, '2017-10-18 12:38:49', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/7'),
(85, 2, 1, '2017-10-19 10:18:05', NULL, NULL, 0, 0, 'invite_received', 3, 1, '/users/invites'),
(86, 4, 1, '2017-10-19 10:20:05', NULL, NULL, 0, 1, 'colab_del', 3, 1, '/users'),
(87, 3, 1, '2017-10-19 10:21:48', NULL, NULL, 0, 1, 'invite_received', 4, 1, '/users/invites'),
(88, 4, 1, '2017-10-19 10:23:27', NULL, NULL, 0, 1, 'invite_accept', 3, 1, '/users'),
(89, 10, 1, '2017-10-20 08:40:02', NULL, NULL, 0, 0, 'prj_add', 0, 1, '/projects/view/7'),
(90, 9, 1, '2017-10-20 08:40:02', NULL, NULL, 0, 0, 'prj_add', 0, 1, '/projects/view/7'),
(91, 11, 1, '2017-10-20 08:40:02', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/7'),
(92, 8, 1, '2017-10-20 08:40:02', NULL, NULL, 0, 0, 'prj_add', 0, 1, '/projects/view/7'),
(93, 2, 1, '2017-10-20 10:17:43', NULL, NULL, 0, 0, 'colab_del', 3, 1, '/users'),
(94, 12, 0, '2017-10-20 12:44:05', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(95, 12, 1, '2017-10-20 12:44:05', NULL, NULL, 0, 1, 'invite_received', 1, 1, '/users/invites'),
(96, 11, 1, '2017-10-20 12:56:09', NULL, NULL, 0, 1, 'invite_received', 12, 1, '/users/invites'),
(97, 12, 1, '2017-10-20 13:01:17', NULL, NULL, 0, 1, 'invite_accept', 11, 1, '/users'),
(98, 1, 1, '2017-10-20 13:08:32', NULL, NULL, 0, 1, 'invite_accept', 12, 1, '/users'),
(99, 10, 1, '2017-10-20 13:14:10', NULL, NULL, 0, 0, 'prj_add', 0, 1, '/projects/view/24'),
(100, 11, 1, '2017-10-20 13:18:03', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/25'),
(101, 1, 1, '2017-10-20 13:18:03', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/25'),
(102, 12, 1, '2017-10-20 14:16:57', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/7'),
(103, 2, 1, '2017-10-23 06:40:27', NULL, NULL, 0, 0, 'invite_received', 1, 1, '/users/invites'),
(104, 13, 0, '2017-10-23 07:15:03', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(105, 1, 1, '2017-10-23 07:34:30', NULL, NULL, 0, 1, 'invite_received', 13, 1, '/users/invites'),
(106, 13, 1, '2017-10-23 07:40:13', NULL, NULL, 0, 1, 'invite_accept', 1, 1, '/users'),
(107, 12, 1, '2017-10-23 07:41:16', NULL, NULL, 0, 1, 'invite_received', 13, 1, '/users/invites'),
(108, 13, 1, '2017-10-23 07:42:04', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/27'),
(109, 12, 1, '2017-10-23 07:42:04', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/27'),
(110, 11, 1, '2017-10-23 07:43:48', NULL, NULL, 0, 1, 'invite_received', 13, 1, '/users/invites'),
(111, 13, 1, '2017-10-23 08:38:20', NULL, NULL, 0, 1, 'invite_accept', 11, 1, '/users'),
(112, 13, 1, '2017-10-24 13:15:21', NULL, NULL, 0, 1, 'invite_accept', 12, 1, '/users'),
(113, 14, 0, '2017-10-25 21:43:16', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(114, 1, 1, '2017-10-25 22:09:34', NULL, NULL, 0, 1, 'invite_received', 14, 1, '/users/invites'),
(115, 7, 1, '2017-10-25 22:10:18', NULL, NULL, 0, 1, 'invite_received', 14, 1, '/users/invites'),
(116, 14, 1, '2017-10-26 08:25:18', NULL, NULL, 0, 1, 'invite_accept', 1, 1, '/users'),
(117, 14, 1, '2017-10-26 08:26:07', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/12'),
(118, 15, 0, '2017-10-26 08:27:18', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(119, 14, 1, '2017-10-26 08:51:35', NULL, NULL, 0, 1, 'invite_accept', 7, 1, '/users'),
(120, 16, 0, '2017-10-31 07:02:38', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(121, 16, 1, '2017-10-31 07:02:38', NULL, NULL, 0, 1, 'invite_received', 11, 1, '/users/invites'),
(122, 11, 1, '2017-10-31 07:04:48', NULL, NULL, 0, 1, 'invite_accept', 16, 1, '/users'),
(123, 16, 1, '2017-10-31 07:09:06', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/33'),
(124, 17, 0, '2017-10-31 07:32:11', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(125, 17, 1, '2017-10-31 07:32:11', NULL, NULL, 0, 1, 'invite_received', 11, 1, '/users/invites'),
(126, 11, 1, '2017-10-31 07:32:54', NULL, NULL, 0, 1, 'invite_accept', 17, 1, '/users'),
(127, 18, 0, '2017-11-01 15:06:02', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(128, 18, 1, '2017-11-01 15:06:02', NULL, NULL, 0, 1, 'invite_received', 1, 1, '/users/invites'),
(129, 1, 1, '2017-11-01 15:07:24', NULL, NULL, 0, 1, 'invite_accept', 18, 1, '/users'),
(130, 18, 1, '2017-11-01 16:53:56', NULL, NULL, 0, 1, 'prj_add', 0, 1, '/projects/view/7'),
(131, 1, 1, '2017-11-02 01:25:06', NULL, NULL, 0, 1, 'invite_accept', 6, 1, '/users'),
(132, 19, 0, '2017-11-07 11:36:21', NULL, NULL, 0, 1, 'acc_created', 0, 1, '/'),
(133, 16, 1, '2017-11-07 12:10:14', NULL, NULL, 0, 0, 'invite_received', 19, 1, '/users/invites');

-- --------------------------------------------------------

--
-- Структура таблицы `notifications_sources`
--

CREATE TABLE `notifications_sources` (
  `notifysrc_id` int(11) UNSIGNED NOT NULL,
  `notifysrc_src_id` int(11) DEFAULT '0',
  `notifysrc_lang` varchar(4) DEFAULT 'en',
  `notifysrc_title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notifications_sources`
--

INSERT INTO `notifications_sources` (`notifysrc_id`, `notifysrc_src_id`, `notifysrc_lang`, `notifysrc_title`) VALUES
(2, 0, 'en', 'System'),
(3, 0, 'ru', 'Система'),
(4, 1, 'en', 'User'),
(5, 1, 'ru', 'Пользователь');

-- --------------------------------------------------------

--
-- Структура таблицы `notifications_types`
--

CREATE TABLE `notifications_types` (
  `ntype_id` int(11) UNSIGNED NOT NULL,
  `ntype_action` varchar(100) DEFAULT NULL,
  `ntype_desc` varchar(255) DEFAULT NULL,
  `ntype_lang` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notifications_types`
--

INSERT INTO `notifications_types` (`ntype_id`, `ntype_action`, `ntype_desc`, `ntype_lang`) VALUES
(1, 'invite_accept', 'Invitation accepted', 'en'),
(2, 'invite_received', 'Invitation to collaboration', 'en'),
(3, 'acc_created', 'Account Has Been Created', 'en'),
(4, 'colab_del', 'User removed you from your colloborates', 'en'),
(5, 'invite_accept', 'Приглашение принято', 'ru'),
(6, 'invite_received', 'Приглашение к сотрудничеству', 'ru'),
(7, 'acc_created', 'Учетная запись создана', 'ru'),
(8, 'colab_del', 'Пользователь прекратил сотрудничество с Вами', 'ru'),
(9, 'prj_add', 'You have been added to project', 'en'),
(10, 'prj_add', 'Вас добавили в проект', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `payments_method`
--

CREATE TABLE `payments_method` (
  `paymethod_id` int(11) UNSIGNED NOT NULL,
  `paymethod_method` int(11) NOT NULL DEFAULT '0',
  `paymethod_name` varchar(255) DEFAULT NULL,
  `paymethod_lang` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `payments_method`
--

INSERT INTO `payments_method` (`paymethod_id`, `paymethod_method`, `paymethod_name`, `paymethod_lang`) VALUES
(1, 1, 'Cash', 'en'),
(2, 2, 'Check', 'en'),
(3, 3, 'Credit Card', 'en'),
(5, 5, 'Bank Transfer', 'en'),
(6, 6, 'Paypal', 'en'),
(7, 7, 'Western Union', 'en'),
(8, 8, 'Other', 'en'),
(9, 1, 'Наличные', 'ru'),
(10, 2, 'Чек', 'ru'),
(11, 3, 'Кредитная Карта', 'ru'),
(13, 5, 'Банковский Перевод', 'ru'),
(14, 6, 'PayPal', 'ru'),
(15, 7, 'Western Union', 'ru'),
(16, 8, 'Другой', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `portfolio`
--

CREATE TABLE `portfolio` (
  `portfolio_id` int(11) UNSIGNED NOT NULL,
  `portfolio_user_id` int(11) NOT NULL DEFAULT '0',
  `portfolio_title` varchar(255) DEFAULT NULL,
  `portfolio_desc` text,
  `portfolio_thumbnail` varchar(255) NOT NULL DEFAULT 'nopic.jpg',
  `portfolio_status` tinyint(4) NOT NULL DEFAULT '0',
  `portfolio_customer` varchar(255) DEFAULT NULL,
  `portfolio_link` varchar(255) DEFAULT NULL,
  `portfolio_trash` tinyint(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `portfolio`
--

INSERT INTO `portfolio` (`portfolio_id`, `portfolio_user_id`, `portfolio_title`, `portfolio_desc`, `portfolio_thumbnail`, `portfolio_status`, `portfolio_customer`, `portfolio_link`, `portfolio_trash`) VALUES
(1, 3, 'New Portfolio', '', '59d22757dfd27.jpg', 0, '', 'http://', 1),
(2, 3, 'аарпо', '', '59e0b86e13feb.jpg', 0, '', 'http://', 1),
(3, 3, 'Заголовок', '', '59e0c891e6ea6.jpg', 0, '', 'http://', 1),
(4, 3, 'ааааааааааа', '', 'nopic.jpg', 0, '', 'http://', 1),
(5, 3, 'ммммммммммммм', '', '59e0e0e6d3434.jpg', 0, '', 'http://', 0),
(6, 3, 'ап', '', 'nopic.jpg', 0, '', 'http://', 1),
(7, 3, 'апппппа', '', 'nopic.jpg', 0, '', 'http://', 1),
(8, 1, 'ывывы', '<p>ывыв</p>', 'nopic.jpg', 0, 'ывыв', 'http://ывыв', 0),
(9, 1, 'ываыа ываываыва ываываыва ываываыа ываываыв аыаываы ываыва ываываыва', '', 'nopic.jpg', 0, 'ываываыв', 'http://ываыв', 0),
(10, 15, 'Портфолио', '', 'nopic.jpg', 0, 'Порт', 'http://google.com', 0),
(11, 19, 'Iceland', '', '5a019d689fa74.jpg', 0, '', 'http://', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `portfolio_pic`
--

CREATE TABLE `portfolio_pic` (
  `portfoliopic_id` int(11) NOT NULL,
  `portfoliopic_user_id` int(11) NOT NULL DEFAULT '0',
  `portfoliopic_portfolio_id` int(11) NOT NULL DEFAULT '0',
  `portfoliopic_pic` varchar(255) NOT NULL DEFAULT 'nopic.jpg',
  `portfoliopic_trash` tinyint(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `portfolio_pic`
--

INSERT INTO `portfolio_pic` (`portfoliopic_id`, `portfoliopic_user_id`, `portfoliopic_portfolio_id`, `portfoliopic_pic`, `portfoliopic_trash`) VALUES
(1, 3, 1, 'o_1breekjbjnef1ekk12eug1i1ss4i.jpg', 0),
(2, 3, 1, 'o_1breel80ukoh1kd5ouusto19rs19.jpeg', 0),
(3, 3, 1, 'o_1breel80v1d291nj2k16ec10ml1a.jpeg', 0),
(4, 3, 1, 'o_1breel810q181n341qlug818b11b.jpeg', 0),
(5, 3, 1, 'o_1breel811rsqjjbrh9d611t051c.jpeg', 0),
(6, 3, 1, 'o_1breel81271q1bqphoe199e1g1k1d.jpeg', 0),
(7, 3, 1, 'o_1breel8121j7it6rtos1l1gb301e.jpeg', 0),
(8, 3, 1, 'o_1breel8131lml1i2at19tea186e1f.jpeg', 0),
(9, 3, 1, 'o_1breel8131ik318alvln1sk61t4n1g.jpeg', 0),
(10, 3, 1, 'o_1breel8131m8rkg7kpdtem16ib1h.jpeg', 0),
(11, 3, 1, 'o_1breel8136va1dd51eg210111hsj1i.jpg', 0),
(12, 3, 1, 'o_1breel8141n0c631qcd41k15ih1j.png', 0),
(13, 3, 1, 'o_1breel815569138mt141h4r1ff81k.png', 0),
(14, 3, 1, 'o_1breel8151q9e1h471ikgqrj176o1l.png', 0),
(15, 3, 1, 'o_1breel816e2315o31tlc1j361ior1m.png', 0),
(16, 3, 1, 'o_1breel8166hv5iufor7rqkvs1n.png', 0),
(17, 3, 1, 'o_1breel8161a7e1boks2qn11ibt1o.png', 0),
(18, 3, 1, 'o_1breel81632ensj1ja5t4dpd11p.png', 0),
(19, 3, 1, 'o_1breel8172984m7io21ee313bq1q.png', 0),
(20, 3, 1, 'o_1breel818sqovtk1ph81qnoqm01r.png', 0),
(21, 3, 3, 'o_1bsb1075e13fcrknva17urbu3t.jpg', 0),
(22, 3, 3, 'o_1bsb1075hpapbu21ndu1drf18ugu.jpeg', 0),
(23, 3, 3, 'o_1bsb1075i1c1o1ntb19afqtj1lh010.jpeg', 0),
(24, 3, 3, 'o_1bsb1075j1jbuo3scea12k8uep11.jpeg', 0),
(25, 3, 3, 'o_1bsb1075k1cai1djg39a1d5r2bl13.jpeg', 0),
(26, 15, 10, 'o_1btbssa0mvig1qnvgqt5e7crmd.jpg', 0),
(28, 19, 11, 'o_1bub4q4ft80ausos7lf55hn4i.jpg', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `professions`
--

CREATE TABLE `professions` (
  `pro_id` int(11) NOT NULL,
  `pro_index` int(11) NOT NULL DEFAULT '0',
  `pro_name` varchar(255) DEFAULT NULL,
  `pro_desc` varchar(255) DEFAULT NULL,
  `pro_sort` int(11) NOT NULL DEFAULT '99999'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `professions`
--

INSERT INTO `professions` (`pro_id`, `pro_index`, `pro_name`, `pro_desc`, `pro_sort`) VALUES
(1, 1, 'Marketing', 'Marketing', 99999),
(2, 2, 'Web Development', 'Web Development', 99999),
(3, 3, 'App Development', 'App Development', 99999),
(4, 4, 'Accounting', 'Accounting', 99999),
(5, 5, 'Teaching and Tutoring', 'Teaching and Tutoring', 99999),
(6, 6, 'Graphic Design', 'Graphic Design', 99999),
(7, 7, 'Photographer', 'Photographer', 99999),
(8, 8, 'Blogger', 'Blogger', 99999),
(9, 9, 'SEO', 'SEO', 99999),
(10, 10, 'Virtual Assistant', 'Virtual Assistant', 99999),
(11, 11, 'Writing & Copywriting', 'Writing & Copywriting', 99999),
(12, 12, 'Software Development', 'Software Development', 99999),
(13, 13, 'CAD', 'CAD', 99999),
(14, 14, 'Project Manager', 'Project Manager', 99999);

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) UNSIGNED NOT NULL,
  `project_user_id` int(11) DEFAULT NULL,
  `project_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `project_name` varchar(255) DEFAULT NULL,
  `project_desc` text,
  `project_status` int(11) NOT NULL DEFAULT '1',
  `project_trash` tinyint(11) NOT NULL DEFAULT '0',
  `project_default` tinyint(11) NOT NULL DEFAULT '0',
  `project_client_id` int(11) NOT NULL DEFAULT '0',
  `project_collab_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`project_id`, `project_user_id`, `project_date`, `project_name`, `project_desc`, `project_status`, `project_trash`, `project_default`, `project_client_id`, `project_collab_id`) VALUES
(1, 1, '2017-09-21 11:09:40', 'Default', NULL, 1, 0, 1, 0, 0),
(2, 2, '2017-09-21 15:11:19', 'Default', NULL, 1, 0, 1, 0, 0),
(3, 3, '2017-09-26 06:01:24', 'Default', NULL, 1, 0, 1, 0, 0),
(5, 4, '2017-10-03 16:02:20', 'Default', NULL, 1, 0, 1, 0, 0),
(6, 1, '2017-10-12 07:35:07', 'Whatsuap.eu', 'Сайт для немцев', 1, 0, 0, 0, 0),
(7, 1, '2017-10-12 07:40:50', 'Floctopus.com разработка', 'Мега проджект', 1, 0, 0, 0, 0),
(8, 5, '2017-10-12 15:40:17', 'Default', NULL, 1, 0, 1, 0, 0),
(10, 6, '2017-10-13 14:51:16', 'Default', NULL, 1, 0, 1, 0, 0),
(11, 1, '2017-10-13 17:07:11', 'ghfghf', '', 1, 1, 0, 0, 0),
(12, 1, '2017-10-13 19:30:07', 'hikkaduwa.com', 'hikaduwa.com сайт', 1, 0, 0, 0, 0),
(14, 7, '2017-10-14 11:06:10', 'Default', NULL, 1, 0, 1, 0, 0),
(16, 1, '2017-10-14 16:12:00', 'Test2', '', 1, 1, 0, 0, 0),
(17, 8, '2017-10-14 22:23:03', 'Default', NULL, 1, 0, 1, 0, 0),
(18, 9, '2017-10-18 11:29:56', 'Default', NULL, 1, 0, 1, 0, 0),
(19, 10, '2017-10-18 11:33:51', 'Default', NULL, 1, 0, 1, 0, 0),
(20, 11, '2017-10-18 12:34:37', 'Default', NULL, 1, 0, 1, 0, 0),
(21, 1, '2017-10-19 21:13:38', 'gdhd', '', 1, 1, 0, 0, 0),
(22, 1, '2017-10-19 21:55:02', 'adad', '', 1, 1, 0, 4, 0),
(23, 12, '2017-10-20 12:44:05', 'Default', NULL, 1, 0, 1, 0, 0),
(24, 1, '2017-10-20 13:14:10', 'sSs', '', 1, 1, 0, 0, 0),
(25, 12, '2017-10-20 13:18:03', 'продвижение Флоктопус', '', 1, 0, 0, 0, 0),
(26, 13, '2017-10-23 07:15:03', 'Default', NULL, 1, 0, 1, 0, 0),
(27, 1, '2017-10-23 07:42:04', 'Floctopus.com Документация', 'Документация к Floctopus.com', 1, 0, 0, 0, 0),
(28, 13, '2017-10-23 09:14:38', 'ппп', '', 1, 1, 0, 0, 0),
(29, 14, '2017-10-25 21:43:16', 'Default', NULL, 1, 0, 1, 0, 0),
(30, 15, '2017-10-26 08:27:18', 'Default', NULL, 1, 0, 1, 0, 0),
(31, 15, '2017-10-26 08:33:56', 'Проект Проектирования', 'Проектируется проект для проекта', 1, 0, 0, 18, 0),
(32, 1, '2017-10-27 16:21:23', 'Managemart.com', 'Managemart.com', 1, 0, 0, 0, 0),
(33, 11, '2017-10-31 06:58:40', 'Создание бренд-бука', 'Создание руководства по фирменному стилю бренда. история, ценности и принципы работы бренда на рынке\r\nхарактеристика целевой аудитории товара\r\nтребования по соблюдению фирменного стиля торговой марки\r\nпримеры готовых материалов и шаблоны дизайна', 1, 0, 0, 19, 0),
(34, 16, '2017-10-31 07:02:38', 'Default', NULL, 1, 0, 1, 0, 0),
(35, 17, '2017-10-31 07:32:11', 'Default', NULL, 1, 0, 1, 0, 0),
(36, 18, '2017-11-01 15:06:02', 'Default', NULL, 1, 0, 1, 0, 0),
(37, 19, '2017-11-07 11:36:21', 'Default', NULL, 1, 0, 1, 0, 0),
(38, 15, '2017-11-07 13:15:12', '1', '123', 1, 0, 0, 18, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `project_collaborators`
--

CREATE TABLE `project_collaborators` (
  `prju_id` int(11) UNSIGNED NOT NULL,
  `prju_user_id` int(11) DEFAULT NULL,
  `prju_collaborator_id` int(11) DEFAULT NULL,
  `prju_project_id` int(11) DEFAULT NULL,
  `prju_role` smallint(3) NOT NULL DEFAULT '10' COMMENT '10 - collaborator, 20-Manager',
  `prju_trash` tinyint(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `project_collaborators`
--

INSERT INTO `project_collaborators` (`prju_id`, `prju_user_id`, `prju_collaborator_id`, `prju_project_id`, `prju_role`, `prju_trash`) VALUES
(65, 1, 1, 6, 1, 0),
(67, 1, 1, 7, 1, 0),
(69, 1, 1, 12, 1, 0),
(71, 1, 1, 16, 1, 1),
(72, 1, 3, 16, 20, 1),
(103, 1, 1, 21, 1, 1),
(104, 1, 1, 22, 1, 1),
(111, 1, 1, 24, 1, 1),
(112, 1, 10, 24, 20, 1),
(113, 12, 12, 25, 1, 0),
(114, 12, 11, 25, 20, 0),
(115, 12, 1, 25, 10, 0),
(122, 1, 1, 27, 1, 0),
(123, 1, 13, 27, 20, 0),
(124, 1, 12, 27, 10, 0),
(125, 13, 13, 28, 1, 1),
(126, 1, 7, 12, 10, 0),
(127, 1, 14, 12, 10, 0),
(128, 15, 15, 31, 1, 0),
(129, 1, 1, 32, 1, 0),
(130, 11, 11, 33, 1, 0),
(133, 11, 16, 33, 10, 0),
(134, 1, 3, 7, 10, 0),
(135, 1, 10, 7, 20, 0),
(136, 1, 9, 7, 20, 0),
(137, 1, 11, 7, 20, 0),
(138, 1, 8, 7, 20, 0),
(139, 1, 12, 7, 20, 0),
(140, 1, 18, 7, 20, 0),
(141, 15, 15, 38, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `project_milestones`
--

CREATE TABLE `project_milestones` (
  `prjm_id` int(11) UNSIGNED NOT NULL,
  `prjm_user_id` int(11) NOT NULL DEFAULT '0',
  `prjm_project_id` int(11) NOT NULL DEFAULT '0',
  `prjm_name` varchar(255) NOT NULL DEFAULT '0',
  `prjm_deadline` date DEFAULT NULL,
  `prjm_status` int(11) NOT NULL DEFAULT '0',
  `prjm_trash` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `project_milestones`
--

INSERT INTO `project_milestones` (`prjm_id`, `prjm_user_id`, `prjm_project_id`, `prjm_name`, `prjm_deadline`, `prjm_status`, `prjm_trash`) VALUES
(1, 3, 4, 'Тестирование', '2017-10-08', 0, 1),
(2, 3, 4, 'Устранение ошибок', '2017-10-13', 0, 1),
(3, 3, 4, 'Тестирование', '2017-10-08', 0, 1),
(4, 3, 4, 'Устранение ошибок', '2017-10-13', 0, 1),
(5, 3, 4, 'Сливание веток', '2017-10-03', 0, 1),
(6, 3, 4, 'Создание плана тестирования', '2017-10-04', 0, 1),
(7, 3, 4, 'Тестирование', '2017-10-08', 0, 1),
(8, 3, 4, 'Устранение ошибок', '2017-10-13', 0, 1),
(9, 3, 4, 'Сливание веток', '2017-10-03', 0, 1),
(10, 3, 4, 'Создание плана тестирования', '2017-10-04', 0, 1),
(11, 1, 7, 'Задачи', '2017-11-16', 0, 1),
(12, 1, 7, 'Календарь', '2017-12-29', 0, 1),
(13, 1, 7, 'Проекты', '2017-10-13', 0, 1),
(14, 1, 7, 'Клиенты', '2017-10-13', 0, 1),
(15, 1, 7, 'Сотрудники', '2017-10-13', 0, 1),
(16, 1, 7, 'Услуги', '2017-10-13', 0, 1),
(17, 1, 7, 'Инвойсы', '2017-10-13', 0, 1),
(18, 1, 7, 'Сметы', '2017-10-13', 0, 1),
(19, 1, 7, 'Расходы', '2017-10-13', 0, 1),
(20, 1, 7, 'Транзакции', '2017-10-13', 0, 1),
(21, 1, 12, 'Packages', '2017-10-16', 0, 0),
(22, 1, 7, 'Задачи', '2017-11-16', 0, 1),
(23, 1, 7, 'Календарь', '2017-12-29', 0, 1),
(24, 1, 7, 'Проекты', '2017-10-13', 0, 1),
(25, 1, 7, 'Клиенты', '2017-10-13', 0, 1),
(26, 1, 7, 'Сотрудники', '2017-10-13', 0, 1),
(27, 1, 7, 'Услуги', '2017-10-13', 0, 1),
(28, 1, 7, 'Инвойсы', '2017-10-13', 0, 1),
(29, 1, 7, 'Сметы', '2017-10-13', 0, 1),
(30, 1, 7, 'Расходы', '2017-10-13', 0, 1),
(31, 1, 7, 'Транзакции', '2017-10-13', 0, 1),
(32, 1, 7, 'Документ', '2017-10-15', 0, 1),
(33, 1, 7, 'Задачи', '2017-11-16', 0, 0),
(34, 1, 7, 'Календарь', '2017-12-29', 0, 0),
(35, 1, 7, 'Проекты', '2017-10-13', 0, 0),
(36, 1, 7, 'Клиенты', '2017-10-13', 0, 0),
(37, 1, 7, 'Сотрудники', '2017-10-13', 0, 0),
(38, 1, 7, 'Услуги', '2017-10-13', 0, 0),
(39, 1, 7, 'Инвойсы', '2017-10-13', 0, 0),
(40, 1, 7, 'Сметы', '2017-10-13', 0, 0),
(41, 1, 7, 'Расходы', '2017-10-13', 0, 0),
(42, 1, 7, 'Транзакции', '2017-10-13', 0, 0),
(43, 1, 7, 'Документ', '2017-10-15', 0, 0),
(44, 1, 7, 'Messenger', '2017-10-15', 0, 0),
(45, 1, 27, 'People', '2017-11-30', 0, 0),
(46, 1, 27, 'Money', '2017-11-30', 0, 0),
(47, 1, 27, 'Calendar', '2017-11-30', 0, 0),
(48, 1, 27, 'Projects', '2017-11-30', 0, 0),
(49, 13, 28, 'п1', '2017-10-26', 0, 1),
(50, 13, 28, 'п2', '2017-10-30', 0, 1),
(51, 13, 28, 'т3', '2017-11-02', 0, 1),
(52, 15, 31, 'первый', '2017-10-26', 0, 0),
(53, 15, 31, 'Второй', '2017-10-27', 0, 0),
(54, 11, 33, 'написание брифа для подрядчика', '2017-10-31', 0, 1),
(55, 11, 33, 'Согласование  брифа с клиентом', '2017-11-02', 0, 1),
(56, 11, 33, 'утверждение брифа', '2017-11-04', 0, 1),
(57, 11, 33, 'поиск и выбор 5 подрядчиков , анализ всех характеристик цена-качество', '2017-11-09', 0, 1),
(58, 11, 33, 'выбор 1 подрядчика', '2017-11-15', 0, 1),
(59, 11, 33, 'утверждение лого, фирменного стиля, слогана, цветовой палитры, шрифтов', '2017-11-21', 0, 1),
(60, 11, 33, 'написание брифа для подрядчика', '2017-10-31', 0, 0),
(61, 11, 33, 'Согласование  брифа с клиентом', '2017-11-02', 0, 0),
(62, 11, 33, 'утверждение брифа', '2017-11-04', 0, 0),
(63, 11, 33, 'поиск и выбор 5 подрядчиков , анализ всех характеристик цена-качество', '2017-11-09', 0, 0),
(64, 11, 33, 'выбор 1 подрядчика', '2017-11-15', 0, 0),
(65, 11, 33, 'утверждение лого, фирменного стиля, слогана, цветовой палитры, шрифтов', '2017-11-21', 0, 0),
(66, 11, 33, 'прием и оплата работы', '2017-11-23', 0, 0),
(67, 15, 38, '12', '2017-11-07', 0, 0),
(68, 15, 38, '13', '2017-11-07', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `project_status`
--

CREATE TABLE `project_status` (
  `prjstatus_id` int(11) UNSIGNED NOT NULL,
  `prjstatus_status` int(11) DEFAULT NULL,
  `prjstatus_name` varchar(100) DEFAULT NULL,
  `prjstatus_lang` varchar(4) DEFAULT NULL,
  `prjstatus_color` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `project_status`
--

INSERT INTO `project_status` (`prjstatus_id`, `prjstatus_status`, `prjstatus_name`, `prjstatus_lang`, `prjstatus_color`) VALUES
(1, 1, 'Open', 'en', 'danger'),
(2, 2, 'Suspended', 'en', 'warning'),
(3, 3, 'Close', 'en', 'success'),
(4, 1, 'Открыт', 'ru', 'danger'),
(5, 2, 'Приостановлен', 'ru', 'warning'),
(6, 3, 'Закрыт', 'ru', 'success');

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `srv_id` int(11) NOT NULL,
  `srv_user_id` int(11) NOT NULL DEFAULT '0',
  `srv_name` varchar(255) DEFAULT NULL,
  `srv_desc` text,
  `srv_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `srv_tax1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `srv_tax2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `srv_tax_cash1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `srv_tax_cash2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `srv_trash` tinyint(4) NOT NULL DEFAULT '0',
  `srv_currency` varchar(5) NOT NULL DEFAULT 'usd',
  `srv_qty` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`srv_id`, `srv_user_id`, `srv_name`, `srv_desc`, `srv_cost`, `srv_tax1`, `srv_tax2`, `srv_tax_cash1`, `srv_tax_cash2`, `srv_trash`, `srv_currency`, `srv_qty`) VALUES
(1, 3, 'Floctopus', '', '100.00', '0.00', '0.00', '0.00', '0.00', 0, 'USD', 1),
(2, 3, 'ggggggggggggg', '', '100.00', '0.00', '0.00', '0.00', '0.00', 0, 'USD', 1),
(3, 3, 'олол', 'бььол', '10000.00', '10.00', '3.00', '0.00', '0.00', 0, 'USD', 1),
(4, 3, 'Название', 'ваваав', '100000.00', '0.00', '0.00', '0.00', '0.00', 0, 'usd', 1),
(5, 1, 'a', '', '100.00', '0.00', '0.00', '0.00', '0.00', 0, 'USD', 1),
(6, 1, 'as', '', '123.00', '0.00', '0.00', '0.00', '0.00', 0, 'USD', 1),
(7, 13, 'Invoice', '', '100.00', '7.50', '0.00', '7.50', '0.00', 0, 'USD', 1),
(8, 1, 'dddd', '', '123.00', '0.00', '0.00', '0.00', '0.00', 0, 'USD', 1),
(9, 1, 'dd', '', '23.00', '0.00', '0.00', '0.00', '0.00', 0, 'USD', 1),
(10, 1, 'ff', '', '123.00', '0.00', '0.00', '0.00', '0.00', 0, 'USD', 1),
(11, 1, 'ggg', 'f', '23.00', '0.00', '0.00', '0.00', '0.00', 0, 'USD', 1),
(12, 16, 'создание брифа', '', '100.00', '5.00', '0.00', '5.00', '0.00', 0, 'USD', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `set_id` int(11) NOT NULL,
  `set_user_id` int(11) NOT NULL DEFAULT '0',
  `set_inv_number_start` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `set_inv_terms` text CHARACTER SET utf8,
  `set_inv_notes` text CHARACTER SET utf8,
  `set_est_number_start` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `set_est_terms` text CHARACTER SET utf8,
  `set_est_notes` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `set_past_due_terms` int(11) NOT NULL DEFAULT '30',
  `set_show_outstanding` tinyint(1) NOT NULL DEFAULT '0',
  `set_show_payment_stub` tinyint(1) NOT NULL DEFAULT '0',
  `set_billing_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `set_billing_street` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `set_billing_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `set_billing_state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `set_billing_zip` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `set_billing_country` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `set_logo` varchar(255) NOT NULL DEFAULT 'nologo.png',
  `set_task_color` varchar(10) NOT NULL DEFAULT '#094eac',
  `set_issue_color` varchar(10) NOT NULL DEFAULT '#e9552c',
  `set_meeting_color` varchar(10) NOT NULL DEFAULT '#094eac',
  `set_milestone_color` varchar(10) NOT NULL DEFAULT '#0e2f44',
  `set_phone_color` varchar(10) NOT NULL DEFAULT '#094eac',
  `set_event_color` varchar(10) NOT NULL DEFAULT '#094bde'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`set_id`, `set_user_id`, `set_inv_number_start`, `set_inv_terms`, `set_inv_notes`, `set_est_number_start`, `set_est_terms`, `set_est_notes`, `set_past_due_terms`, `set_show_outstanding`, `set_show_payment_stub`, `set_billing_name`, `set_billing_street`, `set_billing_city`, `set_billing_state`, `set_billing_zip`, `set_billing_country`, `set_logo`, `set_task_color`, `set_issue_color`, `set_meeting_color`, `set_milestone_color`, `set_phone_color`, `set_event_color`) VALUES
(1, 1, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Valerii Igumentsoff', '', '', '', '', 'US', 'o_1bt4a4tmvk3n15g1ac61seg1o8d8.png', '#ea2e95', '#e9552c', '#008b8b', '#0e2f44', '#094eac', '#59b8e6'),
(2, 2, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Вася Пупкинг', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(3, 3, '1', NULL, NULL, '1', NULL, NULL, 30, 1, 1, 'Juliya', 'Харьковская 58Г кв 92', 'Сумы', 'Сумская область', '40007', 'US', 'nologo.png', '#0e2f44', '#cc0000', '#094eac', '#dfe134', '#ea2e95', '#094bde'),
(4, 4, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Юлия ', '', '', '', '', 'US', 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(5, 5, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Alan Harper', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(6, 6, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Bob Martin', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(7, 7, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Юля', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(8, 8, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Vlad Skotsenko', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(9, 9, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Pavel Polyakov', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(10, 10, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Sergey Goloto', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(11, 11, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Mila Tretyak', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(12, 12, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Спасский Александр', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(13, 13, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Anna Kramar', '', '', '', '', 'UA', 'nologo.png', '#dfe134', '#e9552c', '#094eac', '#0e2f44', '#3fac54', '#094bde'),
(14, 14, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Sascha', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(15, 15, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Миша', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(16, 16, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Мила Моисеева', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(17, 17, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Нина Ярославская', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(18, 18, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Sergei Shamshin', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde'),
(19, 19, '1', NULL, NULL, '1', NULL, NULL, 30, 0, 0, 'Ant', NULL, NULL, NULL, NULL, NULL, 'nologo.png', '#094eac', '#e9552c', '#094eac', '#0e2f44', '#094eac', '#094bde');

-- --------------------------------------------------------

--
-- Структура таблицы `states`
--

CREATE TABLE `states` (
  `state_id` int(11) NOT NULL,
  `state_name` char(40) NOT NULL,
  `state_code` char(4) NOT NULL,
  `state_country_id` int(11) NOT NULL,
  `state_country_iso` varchar(3) NOT NULL DEFAULT 'US'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Satets Lists';

--
-- Дамп данных таблицы `states`
--

INSERT INTO `states` (`state_id`, `state_name`, `state_code`, `state_country_id`, `state_country_iso`) VALUES
(1, 'Alaska', 'AK', 840, 'US'),
(2, 'Alabama', 'AL', 840, 'US'),
(3, 'American Samoa', 'AS', 840, 'US'),
(4, 'Arizona', 'AZ', 840, 'US'),
(5, 'Arkansas', 'AR', 840, 'US'),
(6, 'California', 'CA', 840, 'US'),
(7, 'Colorado', 'CO', 840, 'US'),
(8, 'Connecticut', 'CT', 840, 'US'),
(9, 'Delaware', 'DE', 840, 'US'),
(10, 'District of Columbia', 'DC', 840, 'US'),
(11, 'Federated tbl_states of Micronesia', 'FM', 840, 'US'),
(12, 'Florida', 'FL', 840, 'US'),
(13, 'Georgia', 'GA', 840, 'US'),
(14, 'Guam', 'GU', 840, 'US'),
(15, 'Hawaii', 'HI', 840, 'US'),
(16, 'Idaho', 'ID', 840, 'US'),
(17, 'Illinois', 'IL', 840, 'US'),
(18, 'Indiana', 'IN', 840, 'US'),
(19, 'Iowa', 'IA', 840, 'US'),
(20, 'Kansas', 'KS', 840, 'US'),
(21, 'Kentucky', 'KY', 840, 'US'),
(22, 'Louisiana', 'LA', 840, 'US'),
(23, 'Maine', 'ME', 840, 'US'),
(24, 'Marshall Islands', 'MH', 840, 'US'),
(25, 'Maryland', 'MD', 840, 'US'),
(26, 'Massachusetts', 'MA', 840, 'US'),
(27, 'Michigan', 'MI', 840, 'US'),
(28, 'Minnesota', 'MN', 840, 'US'),
(29, 'Mississippi', 'MS', 840, 'US'),
(30, 'Missouri', 'MO', 840, 'US'),
(31, 'Montana', 'MT', 840, 'US'),
(32, 'Nebraska', 'NE', 840, 'US'),
(33, 'Nevada', 'NV', 840, 'US'),
(34, 'New Hampshire', 'NH', 840, 'US'),
(35, 'New Jersey', 'NJ', 840, 'US'),
(36, 'New Mexico', 'NM', 840, 'US'),
(37, 'New York', 'NY', 840, 'US'),
(38, 'North Carolina', 'NC', 840, 'US'),
(39, 'North Dakota', 'ND', 840, 'US'),
(40, 'Northern Mariana Islands', 'MP', 840, 'US'),
(41, 'Ohio', 'OH', 840, 'US'),
(42, 'Oklahoma', 'OK', 840, 'US'),
(43, 'Oregon', 'OR', 840, 'US'),
(44, 'Palau', 'PW', 840, 'US'),
(45, 'Pennsylvania', 'PA', 840, 'US'),
(46, 'Puerto Rico', 'PR', 840, 'US'),
(47, 'Rhode Island', 'RI', 840, 'US'),
(48, 'South Carolina', 'SC', 840, 'US'),
(49, 'South Dakota', 'SD', 840, 'US'),
(50, 'Tennessee', 'TN', 840, 'US'),
(51, 'Texas', 'TX', 840, 'US'),
(52, 'Utah', 'UT', 840, 'US'),
(53, 'Vermont', 'VT', 840, 'US'),
(54, 'Virgin Islands', 'VI', 840, 'US'),
(55, 'Virginia', 'VA', 840, 'US'),
(56, 'Washington', 'WA', 840, 'US'),
(57, 'West Virginia', 'WV', 840, 'US'),
(58, 'Wisconsin', 'WI', 840, 'US'),
(59, 'Wyoming', 'WY', 840, 'US'),
(60, 'New South Wales', 'NSW', 36, 'AU'),
(61, 'Queensland', 'QLD', 36, 'AU'),
(62, 'South Australia', 'SA', 36, 'AU'),
(63, 'Tasmania', 'TAS', 36, 'AU'),
(64, 'Victoria', 'VIC', 36, 'AU'),
(65, 'Western Australia', 'WA', 36, 'AU'),
(66, 'Australian Capital Territory', 'ACT', 36, 'AU'),
(67, 'Northern Territory', 'NT', 36, 'AU'),
(68, 'Alberta', 'AB', 124, 'CA'),
(69, 'British Columbia', 'BC', 124, 'CA'),
(70, 'Manitoba', 'MB', 124, 'CA'),
(71, 'New Brunswick', 'NB', 124, 'CA'),
(72, 'Newfoundland and Labrador', 'NL', 124, 'CA'),
(73, 'Northwest Territories', 'NT', 124, 'CA'),
(74, 'Nova Scotia', 'NS', 124, 'CA'),
(75, 'Nunavut', 'NU', 124, 'CA'),
(76, 'Ontario', 'ON', 124, 'CA'),
(77, 'Prince Edward Island', 'PE', 124, 'CA'),
(78, 'Quebec', 'QC', 124, 'CA'),
(79, 'Saskatchewan', 'SK', 124, 'CA'),
(80, 'Yukon', 'YT', 124, 'CA'),
(81, 'Northland', 'NT', 554, 'NZ'),
(82, 'Auckland', 'AU', 554, 'NZ'),
(83, 'Waikato', 'WK', 554, 'NZ'),
(84, 'Bay of Plenty', 'BO', 554, 'NZ'),
(85, 'Gisborne', 'GI', 554, 'NZ'),
(86, 'Hawke\'s Bay', 'HK', 554, 'NZ'),
(87, 'Taranaki', 'TK', 554, 'NZ'),
(88, 'Manawatu-Wanganui', 'MW', 554, 'NZ'),
(89, 'Wellington', 'WG', 554, 'NZ'),
(90, 'Tasman', 'TA', 554, 'NZ'),
(91, 'Nelson', 'NS', 554, 'NZ'),
(92, 'Marlborough', 'MB', 554, 'NZ'),
(93, 'West Coast', 'WT', 554, 'NZ'),
(94, 'Canterbury', 'CA', 554, 'NZ'),
(95, 'Otago', 'OT', 554, 'NZ'),
(96, 'Southland', 'ST', 554, 'NZ');

-- --------------------------------------------------------

--
-- Структура таблицы `timezones`
--

CREATE TABLE `timezones` (
  `tz_id` int(11) NOT NULL,
  `tz_olson` varchar(100) NOT NULL,
  `tz_name` varchar(255) NOT NULL,
  `tz_offset` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Time Zones LIst';

--
-- Дамп данных таблицы `timezones`
--

INSERT INTO `timezones` (`tz_id`, `tz_olson`, `tz_name`, `tz_offset`) VALUES
(3, 'Pacific/Apia', '(GMT-11:00) Midway Island, Samoa', -11),
(4, 'Pacific/Honolulu', '(GMT-10:00) Hawaii', -10),
(5, 'America/Anchorage', '(GMT-09:00) Alaska', -9),
(6, 'America/Los_Angeles', '(GMT-08:00) Pacific Time (US & Canada); Tijuana', -8),
(7, 'America/Phoenix', '(GMT-07:00) Arizona', -7),
(8, 'America/Denver', '(GMT-07:00) Mountain Time (US & Canada)', -7),
(9, 'America/Chihuahua', '(GMT-07:00) Chihuahua, La Paz, Mazatlan', -7),
(10, 'America/Managua', '(GMT-06:00) Central America', -6),
(11, 'America/Regina', 'GMT-06:00) Saskatchewan', -6),
(12, 'America/Mexico_City', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', -6),
(13, 'America/Chicago', '(GMT-06:00) Central Time (US & Canada)', -6),
(14, 'America/Indianapolis', '(GMT-05:00) Indiana (East)', -5),
(15, 'America/Bogota', '(GMT-05:00) Bogota, Lima, Quito', -5),
(16, 'America/New_York', '(GMT-05:00) Eastern Time (US & Canada)', -5),
(17, 'America/Caracas', '(GMT-04:00) Caracas, La Paz', -4),
(18, 'America/Santiago', '(GMT-04:00) Santiago', -4),
(19, 'America/Halifax', '(GMT-04:00) Atlantic Time (Canada)', -4),
(20, 'America/St_Johns', '(GMT-03:30) Newfoundland', -3),
(21, 'America/Buenos_Aires', '(GMT-03:00) Buenos Aires, Georgetown', -3),
(22, 'America/Godthab', '(GMT-03:00) Greenland', -3),
(23, 'America/Sao_Paulo', '(GMT-03:00) Brasilia', -3),
(24, 'America/Noronha', '(GMT-02:00) Mid-Atlantic', -2),
(25, 'Atlantic/Cape_Verde', '(GMT-01:00) Cape Verde Is.', -1),
(26, 'Atlantic/Azores', '(GMT-01:00) Azores', -1),
(27, 'Africa/Casablanca', '(GMT) Casablanca, Monrovia', 0),
(28, 'Europe/London', '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London', 0),
(29, 'Africa/Lagos', '(GMT+01:00) West Central Africa', 1),
(30, 'Europe/Berlin', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 1),
(31, 'Europe/Paris', '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris', 1),
(32, 'Europe/Sarajevo', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 1),
(33, 'Europe/Belgrade', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', 1),
(34, 'Africa/Johannesburg', '(GMT+02:00) Harare, Pretoria', 2),
(35, 'Asia/Jerusalem', '(GMT+02:00) Jerusalem', 2),
(36, 'Europe/Istanbul', '(GMT+02:00) Athens, Istanbul, Minsk', 2),
(37, 'Europe/Helsinki', '(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius', 2),
(38, 'Africa/Cairo', '(GMT+02:00) Cairo', 0),
(39, 'Europe/Bucharest', '(GMT+02:00) Bucharest', 2),
(40, 'Africa/Nairobi', '(GMT+03:00) Nairobi', 3),
(41, 'Asia/Riyadh', '(GMT+03:00) Kuwait, Riyadh', 3),
(42, 'Europe/Moscow', '(GMT+03:00) Moscow, St. Petersburg, Volgograd', 3),
(43, 'Asia/Baghdad', '(GMT+03:00) Baghdad', 3),
(44, 'Asia/Tehran', '(GMT+03:30) Tehran', 3),
(45, 'Asia/Muscat', '(GMT+04:00) Abu Dhabi, Muscat', 4),
(46, 'Asia/Tbilisi', '(GMT+04:00) Baku, Tbilisi, Yerevan', 4),
(47, 'Asia/Kabul', '(GMT+04:30) Kabul', 4),
(48, 'Asia/Karachi', '(GMT+05:00) Islamabad, Karachi, Tashkent', 5),
(49, 'Asia/Yekaterinburg', '(GMT+05:00) Ekaterinburg', 5),
(50, 'Asia/Calcutta', '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi', 5),
(51, 'Asia/Katmandu', '(GMT+05:45) Kathmandu', 5),
(52, 'Asia/Colombo', '(GMT+06:00) Sri Jayawardenepura', 6),
(53, 'Asia/Dhaka', '(GMT+06:00) Astana, Dhaka', 6),
(54, 'Asia/Novosibirsk', '(GMT+06:00) Almaty, Novosibirsk', 6),
(55, 'Asia/Rangoon', '(GMT+06:30) Rangoon', 6),
(56, 'Asia/Bangkok', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 7),
(57, 'Asia/Krasnoyarsk', '(GMT+07:00) Krasnoyarsk', 7),
(58, 'Australia/Perth', '(GMT+08:00) Perth', 8),
(59, 'Asia/Taipei', '(GMT+08:00) Taipei', 8),
(60, 'Asia/Singapore', '(GMT+08:00) Kuala Lumpur, Singapore', 8),
(61, 'Asia/Hong_Kong', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi', 8),
(62, 'Asia/Irkutsk', '(GMT+08:00) Irkutsk, Ulaan Bataar', 8),
(63, 'Asia/Tokyo', '(GMT+09:00) Osaka, Sapporo, Tokyo', 9),
(64, 'Asia/Seoul', '(GMT+09:00) Seoul', 9),
(65, 'Asia/Yakutsk', '(GMT+09:00) Yakutsk', 9),
(66, 'Australia/Darwin', '(GMT+09:30) Darwin', 9),
(67, 'Australia/Adelaide', '(GMT+09:30) Adelaide', 9),
(68, 'Pacific/Guam', '(GMT+10:00) Guam, Port Moresby', 10),
(69, 'Australia/Brisbane', '(GMT+10:00) Brisbane', 0),
(70, 'Asia/Vladivostok', '(GMT+10:00) Vladivostok', 10),
(71, 'Australia/Hobart', '(GMT+10:00) Hobart', 10),
(72, 'Australia/Sydney', '(GMT+10:00) Canberra, Melbourne, Sydney', 10),
(73, 'Asia/Magadan', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 11),
(74, 'Pacific/Fiji', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 12),
(75, 'Pacific/Auckland', '(GMT+12:00) Auckland, Wellington', 12),
(76, 'Pacific/Tongatapu', '(GMT+13:00) Nuku\'alofa', 13);

-- --------------------------------------------------------

--
-- Структура таблицы `transactions`
--

CREATE TABLE `transactions` (
  `t_id` int(11) UNSIGNED NOT NULL,
  `t_collab_id` int(20) DEFAULT '0',
  `t_client_id` int(20) DEFAULT '0',
  `t_user_id` int(11) DEFAULT '0',
  `t_method` int(11) DEFAULT '0',
  `t_date` date DEFAULT NULL,
  `t_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `t_amount_base` decimal(10,2) NOT NULL DEFAULT '0.00',
  `t_exchange_rate` decimal(10,4) NOT NULL DEFAULT '1.0000',
  `t_inv_id` int(11) NOT NULL DEFAULT '0',
  `t_exp_id` int(11) NOT NULL DEFAULT '0',
  `t_notes` text,
  `t_tags` varchar(255) DEFAULT NULL,
  `t_gw` varchar(255) NOT NULL DEFAULT 'manual',
  `t_currency` varchar(4) NOT NULL DEFAULT 'USD',
  `t_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transactions`
--

INSERT INTO `transactions` (`t_id`, `t_collab_id`, `t_client_id`, `t_user_id`, `t_method`, `t_date`, `t_amount`, `t_amount_base`, `t_exchange_rate`, `t_inv_id`, `t_exp_id`, `t_notes`, `t_tags`, `t_gw`, `t_currency`, `t_trash`) VALUES
(1, 0, 0, 3, 1, '2017-10-18', '0.00', '0.00', '1.0000', 0, 1, 'dfdddf', '0 dfdddf', 'manual', 'USD', 1),
(2, 0, 0, 3, 1, '2017-10-18', '0.00', '0.00', '1.0000', 0, 2, 'dffdfgfg', '0 dffdfgfg', 'manual', 'USD', 1),
(3, 0, 0, 1, 1, '2017-11-09', '25.34', '25.34', '1.0000', 0, 3, 'Набор гелевых ручек', '25.34 Набор гелевых ручек', 'manual', 'UAH', 0),
(4, 0, 0, 1, 1, '2017-11-09', '119.99', '119.99', '1.0000', 0, 4, 'Офисный канцелярский набор', '119.99 Офисный канцелярский набор', 'manual', 'UAH', 0),
(5, 0, 0, 1, 1, '2017-11-09', '59.99', '59.99', '1.0000', 0, 5, 'Бумага для заметок в коробке', '59.99 Бумага для заметок в коробке', 'manual', 'UAH', 0),
(6, 0, 0, 1, 1, '2017-11-09', '57.99', '57.99', '1.0000', 0, 6, 'Тетрадь для записей', '57.99 Тетрадь для записей', 'manual', 'UAH', 0),
(7, 0, 0, 1, 1, '2017-11-09', '89.91', '89.91', '1.0000', 0, 7, 'Мышка проводная Piko', '89.91 Мышка проводная Piko', 'manual', 'UAH', 0),
(8, 0, 0, 1, 1, '2017-11-09', '400.00', '400.00', '1.0000', 0, 8, 'Интернет', '400.00 Интернет', 'manual', 'UAH', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_username` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_facebook` varchar(255) DEFAULT NULL,
  `user_google` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_since` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_country` varchar(4) DEFAULT 'US',
  `user_state` varchar(255) DEFAULT NULL,
  `user_city` varchar(255) DEFAULT NULL,
  `user_street` varchar(255) DEFAULT NULL,
  `user_zip` varchar(100) DEFAULT NULL,
  `user_phone` varchar(255) DEFAULT NULL,
  `user_tz` varchar(255) DEFAULT 'America/New_York',
  `user_dformat` varchar(255) DEFAULT 'M d,Y',
  `user_tformat` varchar(255) DEFAULT 'g:i a',
  `user_mformat` int(11) DEFAULT '1',
  `user_dsep` varchar(2) DEFAULT '.',
  `user_tsep` varchar(2) DEFAULT ',',
  `user_currency` varchar(4) DEFAULT 'USD',
  `user_pic` varchar(255) DEFAULT 'nopic.png',
  `user_logo` varchar(255) DEFAULT 'nologo.png',
  `user_rop` int(11) DEFAULT '30',
  `user_wstart` int(11) DEFAULT '1',
  `user_proffession` int(11) DEFAULT '1',
  `user_first` int(11) DEFAULT '1',
  `user_status` int(11) DEFAULT '1',
  `user_lang` varchar(4) DEFAULT 'en',
  `user_about` text,
  `user_twitter` varchar(255) DEFAULT NULL,
  `user_trash` int(11) DEFAULT '0',
  `user_www` varchar(255) DEFAULT NULL,
  `user_skype` varchar(255) DEFAULT NULL,
  `user_bday` date NOT NULL DEFAULT '1977-03-08',
  `user_proffession_pub` tinyint(4) NOT NULL DEFAULT '1',
  `user_bday_pub` tinyint(4) NOT NULL DEFAULT '1',
  `user_addr_pub` tinyint(4) NOT NULL DEFAULT '1',
  `user_www_pub` tinyint(4) NOT NULL DEFAULT '1',
  `user_skype_pub` tinyint(4) NOT NULL DEFAULT '1',
  `user_phone_pub` tinyint(4) NOT NULL DEFAULT '1',
  `user_twitter_pub` tinyint(4) NOT NULL DEFAULT '1',
  `user_country_pub` tinyint(4) NOT NULL DEFAULT '1',
  `user_google_token` varchar(200) NOT NULL DEFAULT '0',
  `user_pusher_channel` varchar(255) DEFAULT NULL,
  `user_chat_sound` tinyint(1) NOT NULL DEFAULT '1',
  `user_reset_key` varchar(255) DEFAULT NULL,
  `user_doc` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_username`, `user_email`, `user_password`, `user_facebook`, `user_google`, `user_name`, `user_since`, `user_country`, `user_state`, `user_city`, `user_street`, `user_zip`, `user_phone`, `user_tz`, `user_dformat`, `user_tformat`, `user_mformat`, `user_dsep`, `user_tsep`, `user_currency`, `user_pic`, `user_logo`, `user_rop`, `user_wstart`, `user_proffession`, `user_first`, `user_status`, `user_lang`, `user_about`, `user_twitter`, `user_trash`, `user_www`, `user_skype`, `user_bday`, `user_proffession_pub`, `user_bday_pub`, `user_addr_pub`, `user_www_pub`, `user_skype_pub`, `user_phone_pub`, `user_twitter_pub`, `user_country_pub`, `user_google_token`, `user_pusher_channel`, `user_chat_sound`, `user_reset_key`, `user_doc`) VALUES
(1, 'booper', 'ivafirst@gmail.com', '0ec6d71cb7a1ee1ce7e190a32f333fae', NULL, NULL, 'Valerii Igumentsev', '2017-09-21 11:34:18', 'US', '', '', '', '', '', 'Europe/Helsinki', 'M d, Y', 'H:i', 1, '.', ',', 'UAH', '59c7808db9fbf.jpg', 'nologo.png', 30, 0, 3, 0, 1, 'en', 'i am cool!!', '', 0, 'floctopus.com', 'valeron.igumentsoff', '1977-03-08', 1, 1, 1, 1, 1, 0, 0, 1, '0', 'private-59c3a3ba46526', 1, NULL, 1),
(2, 'pupkingaaa', 'booper@ymail.com', '0ec6d71cb7a1ee1ce7e190a32f333fae', NULL, NULL, 'Вася Пупкингaaa', '2017-09-21 15:11:18', 'US', '', '', '', '', '', 'Europe/Helsinki', 'M d, Y', 'g:i a', 1, '.', ',', 'UAH', 'nopic.png', 'nologo.png', 30, 1, 2, 0, 1, 'ru', '', '', 0, '', '', '1977-03-08', 1, 1, 1, 0, 0, 0, 0, 1, '0', 'private-59c3d696e94b7', 1, NULL, 0),
(3, 'juliya', 'ugoloto@gmail.com', '4a7d1ed414474e4033ac29ccb8653d9b', NULL, NULL, 'Juliya', '2017-09-26 06:01:23', 'US', '', '', '', '', '', 'Europe/Helsinki', 'd.m.Y', 'H:i', 1, '.', ' ', 'USD', '59e9f42386f3f.jpg', 'nologo.png', 30, 1, 2, 0, 1, 'ru', '', '', 0, '', '', '1990-09-24', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59c9ed334c09f', 1, NULL, 0),
(4, 'Юлия Валерьевна', 'zavhozmain@gmail.com', '4a7d1ed414474e4033ac29ccb8653d9b', NULL, NULL, 'Юлия Валерьевна', '2017-10-03 16:02:20', 'US', '', '', '', '', '', 'Europe/Helsinki', 'd-M-Y', 'H:i', 1, '.', ' ', 'USD', 'nopic.png', 'nologo.png', 30, 1, 2, 0, 1, 'ru', '', '', 0, '', '', '1990-08-24', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59d3b48c167ec', 1, '59d3bc1120bbf', 0),
(5, 'booperx', 'booperx@live.com', '0ec6d71cb7a1ee1ce7e190a32f333fae', NULL, NULL, 'Alan Harper', '2017-10-12 15:40:16', 'US', NULL, NULL, NULL, NULL, NULL, 'Europe/Helsinki', 'd.m.Y', 'H:i', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 2, 0, 1, 'en', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59df8ce0a7d0c', 1, '59dfdce4da3bf', 0),
(6, 'ivamanagemart', 'iva@managemart.com', '0ec6d71cb7a1ee1ce7e190a32f333fae', NULL, NULL, 'Bob Martin', '2017-10-13 14:51:16', 'US', NULL, NULL, NULL, NULL, NULL, 'America/New_York', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 6, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59e0d2e486dd9', 1, NULL, 0),
(7, 'ychernaya', 'fda86@list.ru', 'c03d33b27510974ab2f0d9d22801ebb1', NULL, NULL, 'Юля', '2017-10-14 11:06:09', 'US', NULL, NULL, NULL, NULL, NULL, 'America/New_York', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 4, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59e1efa1f3b76', 1, NULL, 0),
(8, 'crapez', 'vlad@floctopus.com', '0ec6d71cb7a1ee1ce7e190a32f333fae', NULL, NULL, 'Vlad Skotsenko', '2017-10-14 22:23:02', 'US', NULL, NULL, NULL, NULL, NULL, 'Europe/Helsinki', 'd.m.Y', 'H:i', 1, '.', ',', 'UAH', 'nopic.png', 'nologo.png', 30, 1, 2, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59e28e46e6714', 1, NULL, 0),
(9, 'palpol', 'palpol@floctopus.com', '0ec6d71cb7a1ee1ce7e190a32f333fae', NULL, NULL, 'Pavel Polyakov', '2017-10-18 11:29:56', 'US', NULL, NULL, NULL, NULL, NULL, 'Europe/Helsinki', 'd.m.Y', 'H:i', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 2, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59e73b340bf58', 1, NULL, 0),
(10, 'sgoloto', 'sgoloto@gmail.com', '0ec6d71cb7a1ee1ce7e190a32f333fae', NULL, NULL, 'Sergey Goloto', '2017-10-18 11:33:50', 'US', NULL, NULL, NULL, NULL, NULL, 'America/New_York', 'd.m.Y', 'H:i', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 2, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59e73c1ee2f27', 1, NULL, 0),
(11, 'milatretyak', 'pr.idea.pr@gmail.com', '5922e76b86f3c9b863e68aac3f238d75', NULL, NULL, 'Mila Tretyak', '2017-10-18 12:34:36', 'US', NULL, NULL, NULL, NULL, NULL, 'Europe/Helsinki', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 11, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 0, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59e74a5cce2b8', 1, NULL, 0),
(12, 'alexandr', 'a.spassky@alliancepack.com.ua', 'c057c7acda1bbb4a458d9de2542c8575', NULL, NULL, 'Спасский Александр', '2017-10-20 12:44:05', 'US', NULL, NULL, NULL, NULL, NULL, 'Europe/Helsinki', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 14, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59e9ef9538352', 1, NULL, 1),
(13, 'blackg', 'kramar.anna@alliancepack.com.ua', '6630596fd687288189534efa5b774fd1', NULL, NULL, 'Anna Kramar', '2017-10-23 07:15:02', 'UA', '', '', '', '', '+380932851296', 'Europe/Helsinki', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 14, 0, 1, 'ru', '', '', 0, '', 'kramar.anna', '1991-11-05', 1, 1, 1, 1, 0, 1, 1, 1, '0', 'private-59ed96f6865cf', 1, NULL, 1),
(14, 'sascha', 'info@hikkaduwa.com', '2cc18a588712f39668e82e0dcdaf2f4b', NULL, NULL, 'Sascha', '2017-10-25 21:43:15', 'US', NULL, NULL, NULL, NULL, NULL, 'America/New_York', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 4, 0, 1, 'en', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59f10573a3585', 1, NULL, 0),
(15, 'mikhail', 'alperovitch@mac.com', '52ab3173f47b90c4f7d376b6cdac028c', NULL, NULL, 'Миша', '2017-10-26 08:27:18', 'US', NULL, NULL, NULL, NULL, NULL, 'Europe/Helsinki', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 14, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59f19c663eb8d', 1, NULL, 0),
(16, 'floctproject@gmail.com', 'floct.project@gmail.com', '5922e76b86f3c9b863e68aac3f238d75', NULL, NULL, 'Мила Моисеева', '2017-10-31 07:02:37', 'US', NULL, NULL, NULL, NULL, NULL, 'America/New_York', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 6, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59f8200dea23d', 1, NULL, 0),
(17, 'pr.idea.pr@gmail.com', 'prmuseforyouth@gmail.com', '5922e76b86f3c9b863e68aac3f238d75', NULL, NULL, 'Нина Ярославская', '2017-10-31 07:32:10', 'US', NULL, NULL, NULL, NULL, NULL, 'America/New_York', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 1, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59f826fa86c33', 1, NULL, 0),
(18, 'sergeishamshin', 'sergeyshamshin94@gmail.com', '062af015f65d3d7b3e9f845154d1de47', NULL, NULL, 'Sergei Shamshin', '2017-11-01 15:06:01', 'US', NULL, NULL, NULL, NULL, NULL, 'Europe/Helsinki', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', 'nopic.png', 'nologo.png', 30, 1, 2, 0, 1, 'en', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-59f9e2d9cf7e2', 1, NULL, 0),
(19, 'lader88', 'wopoch@yandex.ru', '814cfb9a51a3cfe66ae3a38cc75da576', NULL, NULL, 'Ant', '2017-11-07 11:36:20', 'US', NULL, NULL, NULL, NULL, NULL, 'America/New_York', 'M d, Y', 'g:i a', 1, '.', ',', 'USD', '5a019dff75bfe.jpg', 'nologo.png', 30, 1, 14, 0, 1, 'ru', NULL, NULL, 0, NULL, NULL, '1977-03-08', 1, 1, 1, 1, 1, 1, 1, 1, '0', 'private-5a019ab4e043f', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user_education`
--

CREATE TABLE `user_education` (
  `edu_id` int(11) UNSIGNED NOT NULL,
  `edu_user_id` int(11) NOT NULL DEFAULT '0',
  `edu_school` varchar(255) DEFAULT NULL,
  `edu_year_from` int(4) NOT NULL DEFAULT '0',
  `edu_year_to` int(4) NOT NULL DEFAULT '0',
  `edu_month_from` int(11) DEFAULT '0',
  `edu_month_to` int(11) DEFAULT NULL,
  `edu_degree` varchar(255) DEFAULT NULL,
  `edu_area` varchar(255) DEFAULT NULL,
  `edu_trash` tinyint(4) NOT NULL DEFAULT '0',
  `edu_desc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_education`
--

INSERT INTO `user_education` (`edu_id`, `edu_user_id`, `edu_school`, `edu_year_from`, `edu_year_to`, `edu_month_from`, `edu_month_to`, `edu_degree`, `edu_area`, `edu_trash`, `edu_desc`) VALUES
(1, 1, 'Станично Луганская СШ №1', 1984, 1994, 9, 5, 'Среднее Образование', '', 0, 'Средняя Школа'),
(2, 1, 'Станично Луганская СШ №2', 1984, 1994, 9, 5, 'Среднее Образование', '', 0, 'Средняя Школа');

-- --------------------------------------------------------

--
-- Структура таблицы `user_work`
--

CREATE TABLE `user_work` (
  `work_id` int(11) NOT NULL,
  `work_user_id` int(11) NOT NULL DEFAULT '0',
  `work_company` varchar(255) DEFAULT NULL,
  `work_position` varchar(255) DEFAULT NULL,
  `work_location` varchar(255) DEFAULT NULL,
  `work_desc` text,
  `work_year_from` smallint(6) NOT NULL DEFAULT '0',
  `work_year_to` smallint(6) NOT NULL DEFAULT '0',
  `work_month_from` smallint(6) NOT NULL DEFAULT '0',
  `work_month_to` smallint(6) NOT NULL DEFAULT '0',
  `work_trash` tinyint(4) NOT NULL DEFAULT '0',
  `work_current` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_work`
--

INSERT INTO `user_work` (`work_id`, `work_user_id`, `work_company`, `work_position`, `work_location`, `work_desc`, `work_year_from`, `work_year_to`, `work_month_from`, `work_month_to`, `work_trash`, `work_current`) VALUES
(27, 1, 'Таможня', 'Сис админ', 'Луганск', 'Рулил', 1998, 2003, 12, 8, 0, 0),
(28, 1, 'Polyplast', 'Сисадмин', 'Луганск', 'Рулил компами', 2003, 2007, 9, 10, 0, 0),
(29, 1, '', 'Фрилансер', '', '', 2007, 0, 10, 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `vendors`
--

CREATE TABLE `vendors` (
  `vendor_id` int(11) UNSIGNED NOT NULL,
  `vendor_user_id` int(11) NOT NULL DEFAULT '0',
  `vendor_name` varchar(255) DEFAULT NULL,
  `vendor_addr` varchar(255) DEFAULT NULL,
  `vendor_phone` varchar(50) DEFAULT NULL,
  `vendor_trash` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `vendors`
--

INSERT INTO `vendors` (`vendor_id`, `vendor_user_id`, `vendor_name`, `vendor_addr`, `vendor_phone`, `vendor_trash`) VALUES
(1, 1, '', NULL, NULL, 0),
(2, 1, '', NULL, NULL, 0),
(3, 1, '', NULL, NULL, 0),
(4, 1, '', NULL, NULL, 0),
(5, 1, 'asdasdasd', NULL, NULL, 0),
(6, 1, 'sss', NULL, NULL, 0),
(7, 1, 'Best Buy', NULL, NULL, 0),
(8, 1, 'sss', NULL, NULL, 0),
(9, 3, '', NULL, NULL, 0),
(10, 3, 'fddffgd', NULL, NULL, 0),
(11, 1, 'Foxtrot', NULL, NULL, 0),
(12, 1, 'Foxtrot', NULL, NULL, 0),
(13, 1, 'Сельпо', NULL, NULL, 0),
(14, 1, 'Фокстрот', NULL, NULL, 0),
(15, 1, 'Фокстрот', NULL, NULL, 0),
(16, 1, 'Сельпо', NULL, NULL, 0),
(17, 1, 'Телегруп Украина', NULL, NULL, 0),
(18, 1, 'Телегруп Украина', NULL, NULL, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `antispam`
--
ALTER TABLE `antispam`
  ADD PRIMARY KEY (`as_id`);

--
-- Индексы таблицы `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`chat_id`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`),
  ADD KEY `client_user_id` (`client_user_id`);

--
-- Индексы таблицы `clients_types`
--
ALTER TABLE `clients_types`
  ADD PRIMARY KEY (`clienttype_id`);

--
-- Индексы таблицы `collaborator`
--
ALTER TABLE `collaborator`
  ADD PRIMARY KEY (`cb_id`),
  ADD KEY `cb_user_id` (`cb_user_id`),
  ADD KEY `cb_colloborator_id` (`cb_collobarator_id`);

--
-- Индексы таблицы `collaborator_statuses`
--
ALTER TABLE `collaborator_statuses`
  ADD PRIMARY KEY (`cbs_id`),
  ADD KEY `cbs_status_id` (`cbs_status_id`);

--
-- Индексы таблицы `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`color_id`);

--
-- Индексы таблицы `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Индексы таблицы `cronemail`
--
ALTER TABLE `cronemail`
  ADD PRIMARY KEY (`cronemail_id`);

--
-- Индексы таблицы `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`currency_id`);

--
-- Индексы таблицы `docs`
--
ALTER TABLE `docs`
  ADD PRIMARY KEY (`doc_id`),
  ADD KEY `doc_user_id` (`doc_user_id`);

--
-- Индексы таблицы `doc_collaborators`
--
ALTER TABLE `doc_collaborators`
  ADD PRIMARY KEY (`dc_id`),
  ADD KEY `dc_user_id` (`dc_user_id`),
  ADD KEY `dc_doc_id` (`dc_collab_id`);

--
-- Индексы таблицы `estimates`
--
ALTER TABLE `estimates`
  ADD PRIMARY KEY (`est_id`),
  ADD KEY `est_user_id` (`est_user_id`);

--
-- Индексы таблицы `estimates_attach`
--
ALTER TABLE `estimates_attach`
  ADD PRIMARY KEY (`estatt_id`);

--
-- Индексы таблицы `estimates_lines`
--
ALTER TABLE `estimates_lines`
  ADD PRIMARY KEY (`estline_id`);

--
-- Индексы таблицы `estimates_statuses`
--
ALTER TABLE `estimates_statuses`
  ADD PRIMARY KEY (`eststatus_id`);

--
-- Индексы таблицы `estimate_actions`
--
ALTER TABLE `estimate_actions`
  ADD PRIMARY KEY (`estact_id`);

--
-- Индексы таблицы `estimate_history`
--
ALTER TABLE `estimate_history`
  ADD PRIMARY KEY (`esthist_id`);

--
-- Индексы таблицы `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`e_id`);

--
-- Индексы таблицы `events_recurring`
--
ALTER TABLE `events_recurring`
  ADD PRIMARY KEY (`er_id`);

--
-- Индексы таблицы `events_types`
--
ALTER TABLE `events_types`
  ADD PRIMARY KEY (`etype_id`);

--
-- Индексы таблицы `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`exp_id`),
  ADD KEY `USER` (`exp_user_id`),
  ADD KEY `CATEGORY` (`exp_category`);

--
-- Индексы таблицы `expenses_category`
--
ALTER TABLE `expenses_category`
  ADD PRIMARY KEY (`expcat_id`),
  ADD KEY `expcat_user_id` (`expcat_user_id`);

--
-- Индексы таблицы `expenses_vendors`
--
ALTER TABLE `expenses_vendors`
  ADD PRIMARY KEY (`vendor_id`);

--
-- Индексы таблицы `exrates`
--
ALTER TABLE `exrates`
  ADD PRIMARY KEY (`exr_id`);

--
-- Индексы таблицы `help_docs`
--
ALTER TABLE `help_docs`
  ADD PRIMARY KEY (`hdoc_id`);

--
-- Индексы таблицы `help_docs_category`
--
ALTER TABLE `help_docs_category`
  ADD PRIMARY KEY (`hdoccat_id`);

--
-- Индексы таблицы `invites`
--
ALTER TABLE `invites`
  ADD PRIMARY KEY (`invite_id`);

--
-- Индексы таблицы `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`inv_id`),
  ADD KEY `inv_user_id` (`inv_user_id`);

--
-- Индексы таблицы `invoices_attach`
--
ALTER TABLE `invoices_attach`
  ADD PRIMARY KEY (`invatt_id`);

--
-- Индексы таблицы `invoices_attach_recurring`
--
ALTER TABLE `invoices_attach_recurring`
  ADD PRIMARY KEY (`invatt_id`);

--
-- Индексы таблицы `invoices_lines`
--
ALTER TABLE `invoices_lines`
  ADD PRIMARY KEY (`invline_id`);

--
-- Индексы таблицы `invoices_lines_recurring`
--
ALTER TABLE `invoices_lines_recurring`
  ADD PRIMARY KEY (`invline_id`);

--
-- Индексы таблицы `invoices_recurring`
--
ALTER TABLE `invoices_recurring`
  ADD PRIMARY KEY (`inv_id`),
  ADD KEY `inv_user_id` (`inv_user_id`);

--
-- Индексы таблицы `invoices_statuses`
--
ALTER TABLE `invoices_statuses`
  ADD PRIMARY KEY (`invstatus_id`);

--
-- Индексы таблицы `invoice_actions`
--
ALTER TABLE `invoice_actions`
  ADD PRIMARY KEY (`invact_id`);

--
-- Индексы таблицы `invoice_history`
--
ALTER TABLE `invoice_history`
  ADD PRIMARY KEY (`invhist_id`);

--
-- Индексы таблицы `issues`
--
ALTER TABLE `issues`
  ADD PRIMARY KEY (`issue_id`);

--
-- Индексы таблицы `issue_attachment`
--
ALTER TABLE `issue_attachment`
  ADD PRIMARY KEY (`issueatt_id`);

--
-- Индексы таблицы `issue_category`
--
ALTER TABLE `issue_category`
  ADD PRIMARY KEY (`issuecat_id`);

--
-- Индексы таблицы `issue_history`
--
ALTER TABLE `issue_history`
  ADD PRIMARY KEY (`ihist_id`);

--
-- Индексы таблицы `issue_priority`
--
ALTER TABLE `issue_priority`
  ADD PRIMARY KEY (`issueprior_id`);

--
-- Индексы таблицы `issue_status`
--
ALTER TABLE `issue_status`
  ADD PRIMARY KEY (`issuestatus_id`);

--
-- Индексы таблицы `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`lang_id`);

--
-- Индексы таблицы `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`);

--
-- Индексы таблицы `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`mon_id`);

--
-- Индексы таблицы `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`n_id`);

--
-- Индексы таблицы `notifications_sources`
--
ALTER TABLE `notifications_sources`
  ADD PRIMARY KEY (`notifysrc_id`);

--
-- Индексы таблицы `notifications_types`
--
ALTER TABLE `notifications_types`
  ADD PRIMARY KEY (`ntype_id`);

--
-- Индексы таблицы `payments_method`
--
ALTER TABLE `payments_method`
  ADD PRIMARY KEY (`paymethod_id`);

--
-- Индексы таблицы `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`portfolio_id`);

--
-- Индексы таблицы `portfolio_pic`
--
ALTER TABLE `portfolio_pic`
  ADD PRIMARY KEY (`portfoliopic_id`);

--
-- Индексы таблицы `professions`
--
ALTER TABLE `professions`
  ADD PRIMARY KEY (`pro_id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Индексы таблицы `project_collaborators`
--
ALTER TABLE `project_collaborators`
  ADD PRIMARY KEY (`prju_id`),
  ADD KEY `prju_user_id` (`prju_user_id`),
  ADD KEY `collab` (`prju_collaborator_id`),
  ADD KEY `prj_collab` (`prju_project_id`);

--
-- Индексы таблицы `project_milestones`
--
ALTER TABLE `project_milestones`
  ADD PRIMARY KEY (`prjm_id`);

--
-- Индексы таблицы `project_status`
--
ALTER TABLE `project_status`
  ADD PRIMARY KEY (`prjstatus_id`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`srv_id`),
  ADD KEY `srv_user_id` (`srv_user_id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`set_id`),
  ADD KEY `set_user_id` (`set_user_id`);

--
-- Индексы таблицы `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Индексы таблицы `timezones`
--
ALTER TABLE `timezones`
  ADD PRIMARY KEY (`tz_id`);

--
-- Индексы таблицы `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`t_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `user_education`
--
ALTER TABLE `user_education`
  ADD PRIMARY KEY (`edu_id`);

--
-- Индексы таблицы `user_work`
--
ALTER TABLE `user_work`
  ADD PRIMARY KEY (`work_id`);

--
-- Индексы таблицы `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`vendor_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `antispam`
--
ALTER TABLE `antispam`
  MODIFY `as_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `chats`
--
ALTER TABLE `chats`
  MODIFY `chat_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `clients_types`
--
ALTER TABLE `clients_types`
  MODIFY `clienttype_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `collaborator`
--
ALTER TABLE `collaborator`
  MODIFY `cb_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT для таблицы `collaborator_statuses`
--
ALTER TABLE `collaborator_statuses`
  MODIFY `cbs_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `colors`
--
ALTER TABLE `colors`
  MODIFY `color_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;
--
-- AUTO_INCREMENT для таблицы `cronemail`
--
ALTER TABLE `cronemail`
  MODIFY `cronemail_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `currencies`
--
ALTER TABLE `currencies`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `docs`
--
ALTER TABLE `docs`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `doc_collaborators`
--
ALTER TABLE `doc_collaborators`
  MODIFY `dc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `estimates`
--
ALTER TABLE `estimates`
  MODIFY `est_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `estimates_attach`
--
ALTER TABLE `estimates_attach`
  MODIFY `estatt_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `estimates_lines`
--
ALTER TABLE `estimates_lines`
  MODIFY `estline_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `estimates_statuses`
--
ALTER TABLE `estimates_statuses`
  MODIFY `eststatus_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `estimate_actions`
--
ALTER TABLE `estimate_actions`
  MODIFY `estact_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `estimate_history`
--
ALTER TABLE `estimate_history`
  MODIFY `esthist_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `events`
--
ALTER TABLE `events`
  MODIFY `e_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `events_recurring`
--
ALTER TABLE `events_recurring`
  MODIFY `er_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `events_types`
--
ALTER TABLE `events_types`
  MODIFY `etype_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `expenses`
--
ALTER TABLE `expenses`
  MODIFY `exp_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `expenses_category`
--
ALTER TABLE `expenses_category`
  MODIFY `expcat_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `expenses_vendors`
--
ALTER TABLE `expenses_vendors`
  MODIFY `vendor_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `exrates`
--
ALTER TABLE `exrates`
  MODIFY `exr_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1251;
--
-- AUTO_INCREMENT для таблицы `help_docs`
--
ALTER TABLE `help_docs`
  MODIFY `hdoc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `help_docs_category`
--
ALTER TABLE `help_docs_category`
  MODIFY `hdoccat_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `invites`
--
ALTER TABLE `invites`
  MODIFY `invite_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `invoices`
--
ALTER TABLE `invoices`
  MODIFY `inv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `invoices_attach`
--
ALTER TABLE `invoices_attach`
  MODIFY `invatt_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `invoices_attach_recurring`
--
ALTER TABLE `invoices_attach_recurring`
  MODIFY `invatt_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `invoices_lines`
--
ALTER TABLE `invoices_lines`
  MODIFY `invline_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `invoices_lines_recurring`
--
ALTER TABLE `invoices_lines_recurring`
  MODIFY `invline_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `invoices_recurring`
--
ALTER TABLE `invoices_recurring`
  MODIFY `inv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `invoices_statuses`
--
ALTER TABLE `invoices_statuses`
  MODIFY `invstatus_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `invoice_actions`
--
ALTER TABLE `invoice_actions`
  MODIFY `invact_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `invoice_history`
--
ALTER TABLE `invoice_history`
  MODIFY `invhist_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `issues`
--
ALTER TABLE `issues`
  MODIFY `issue_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT для таблицы `issue_attachment`
--
ALTER TABLE `issue_attachment`
  MODIFY `issueatt_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `issue_category`
--
ALTER TABLE `issue_category`
  MODIFY `issuecat_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT для таблицы `issue_history`
--
ALTER TABLE `issue_history`
  MODIFY `ihist_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;
--
-- AUTO_INCREMENT для таблицы `issue_priority`
--
ALTER TABLE `issue_priority`
  MODIFY `issueprior_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT для таблицы `issue_status`
--
ALTER TABLE `issue_status`
  MODIFY `issuestatus_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `languages`
--
ALTER TABLE `languages`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `log`
--
ALTER TABLE `log`
  MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT для таблицы `month`
--
ALTER TABLE `month`
  MODIFY `mon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `notes`
--
ALTER TABLE `notes`
  MODIFY `note_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `notifications`
--
ALTER TABLE `notifications`
  MODIFY `n_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT для таблицы `notifications_sources`
--
ALTER TABLE `notifications_sources`
  MODIFY `notifysrc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `notifications_types`
--
ALTER TABLE `notifications_types`
  MODIFY `ntype_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `payments_method`
--
ALTER TABLE `payments_method`
  MODIFY `paymethod_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `portfolio_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `portfolio_pic`
--
ALTER TABLE `portfolio_pic`
  MODIFY `portfoliopic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT для таблицы `professions`
--
ALTER TABLE `professions`
  MODIFY `pro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблицы `project_collaborators`
--
ALTER TABLE `project_collaborators`
  MODIFY `prju_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT для таблицы `project_milestones`
--
ALTER TABLE `project_milestones`
  MODIFY `prjm_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT для таблицы `project_status`
--
ALTER TABLE `project_status`
  MODIFY `prjstatus_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `srv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `set_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT для таблицы `timezones`
--
ALTER TABLE `timezones`
  MODIFY `tz_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT для таблицы `transactions`
--
ALTER TABLE `transactions`
  MODIFY `t_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `user_education`
--
ALTER TABLE `user_education`
  MODIFY `edu_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `user_work`
--
ALTER TABLE `user_work`
  MODIFY `work_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `vendors`
--
ALTER TABLE `vendors`
  MODIFY `vendor_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
