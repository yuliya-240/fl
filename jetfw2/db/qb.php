<?php
namespace jet\db;
use \floctopus\application as app;
/* Класс построения запросов */
/*     qb - Query Builder    */

class qb {
    protected $query;
    protected $db = NULL;      
    protected $sType = NULL;   
    protected $auto_quoting = false;
    
    public static function __callStatic($name, $arguments) {        
        if (substr($name,0,1) == "_") {
            $qb = new qb();
            return @call_user_func_array(array($qb, substr($name,1)), $arguments);
        }
    }
    
    public static function q() {  
        return new qb();   
    }
    
    function __construct($data = NULL, $sType = NULL) {
        if ($data instanceof jet_db_database) {
            $this->db = $data;
           // app::trace($this->db);
            
            $this->sType = $sType;
        }
        else if ($data !== NULL) $this->query['table']=array($data);
        return $this;
    }
    
    function __get($get) {
        switch(strtolower($get)){
            case 'o':
                return clone $this;
                break;
            case 'q':
                $this->auto_quoting = !$this->auto_quoting;
                return $this;
            case 'new':
                return new qb($this->db, $this->sType);
                break;
            case 'select':
                return $this->select('*', true);
                break;
            case 'delete':
                return $this->delete();
                break;
            case 'nowhere':
                $this->query['where'] = array();    
                return $this;
            default:
        }
        return false;
    }
    
    function Table () {
        $arg_list = func_get_args();
        if (is_array($arg_list[0])) $data = $arg_list[0];  
        else $data = $arg_list;          
        if (empty($this->query['table'])) $this->query['table'] = array();
        $this->query['table'] = array_merge($this->query['table'], $data);    
        return $this;
    }
    
    function join () {        
        $arg_list = func_get_args();      
        
        $good = true;
        if (!is_array($arg_list)) {
            throw new \jet\exception("QB.WrongArguments.JOIN", E_USER_ERROR, array('method'=>'join','structure'=>$this->query,'arguments'=>$arg_list));
            return $this;    
        }
        
        $last = count($arg_list) - 1; 
        $type = 'inner';
         
        switch($arg_list[$last]) {
            case 'inner':
            case 'left':
            case 'right':
                $type = array_pop($arg_list);
                break;
            default:
                break;
        }  
        
        $type = strtoupper($type);       
        
        if(count($arg_list)<2) {
            throw new \jet\exception("QB.WrongArguments.JOIN", E_USER_ERROR, array('method'=>'join','type'=>$type,'arguments'=>$arg_list,'structure'=>$this->query));
            return $this;       
        }
        
        $table = array_shift($arg_list);
          
        if (count($arg_list)>=2) {
            $field1 = array_shift($arg_list);   
            $field2 = array_shift($arg_list); 
            if (!$field2 && $field1) {
                //$field1 = array_shift($arg_list);          
                $data = "USING(".$field1.")";    
            }
            else $data = "ON ".$field1."=".$field2;            
        }
        else {
            $field1 = array_shift($arg_list);          
            $data = "USING(".$field1.")";
        }
        
        if (count($arg_list)>0) {
            $data .= " ".array_shift($arg_list);              
        }     
          
        if (empty($this->query['join'])) $this->query['join'] = array();
        array_push($this->query['join'], array($table, $data, $type));        
        return $this;
    }  
    
    function customJoin($table,$data,$type){
    	if (empty($this->query['join'])) $this->query['join'] = array();
        array_push($this->query['join'], array($table, $data, $type));        
        return $this;
	    
	    
    }
             
    function innerjoin ($table, $fields) {     
        $arg_list = func_get_args();     
        $arg_list[] = 'inner';
        return  call_user_func_array(array($this,'join'),$arg_list);     
    }
    
    function leftjoin ($table, $fields) {  
        $arg_list = func_get_args();     
        $arg_list[] = 'left';
        return  call_user_func_array(array($this,'join'),$arg_list);     
    }
    
    function rightjoin ($table, $fields) { 
        $arg_list = func_get_args();     
        $arg_list[] = 'right';
        return  call_user_func_array(array($this,'join'),$arg_list);        
    }  
    
    function Values () {
        $arg_list = func_get_args();
        if (is_array($arg_list[0])) $data = $arg_list[0];
        else $data = $arg_list;
        $values = $data;

        if (empty($this->query['values'])) $this->query['values'] = array();
        $this->query['values'] = array_merge($this->query['values'], $values);       
        return $this;
    }
    
    function Limit() {
        $limit = func_get_args();
        if (func_num_args() == 0) $limit = array(0,1);
        if (func_num_args() == 1) $limit = array(0,$limit[0]);      
        if (empty($this->query['limit'])) $this->query['limit'] = array();
        $this->query['limit'] = $limit;
        return $this;
    }
    
    function Order () {
        $arg_list = func_get_args();
        if ($this->is_assoc($arg_list[0])) $data = $this->assocToOrder($arg_list[0]);
        else if (is_array($arg_list[0])) $data = $arg_list[0];
        else $data = $arg_list;  
        if (empty($this->query['order'])) $this->query['order'] = array();
        $this->query['order'] = array_merge($this->query['order'], $data); 
        return  $this;
    }
    
    function OrderBy () {
        $arg_list = func_get_args();
        if ($this->is_assoc($arg_list[0])) $data = $this->assocToOrder($arg_list[0]);
        else if (is_array($arg_list[0])) $data = $arg_list[0];
        else $data = $arg_list;  
        if (empty($this->query['order'])) $this->query['order'] = array();
        $this->query['order'] = array_merge($this->query['order'], $data); 
        return  $this;
    }   
                            
    function GroupBy () {
        $arg_list = func_get_args();
        if ($this->is_assoc($arg_list[0])) $data = $this->assocToOrder($arg_list[0]);
        else if (is_array($arg_list[0])) $data = $arg_list[0];
        else $data = $arg_list;
        if (empty($this->query['group'])) $this->query['group'] = array();
        $this->query['group'] = array_merge($this->query['group'], $data);
        return $this;
    }
    
    function Group () {
        $arg_list = func_get_args();
        if ($this->is_assoc($arg_list[0])) $data = $this->assocToOrder($arg_list[0]);
        else if (is_array($arg_list[0])) $data = $arg_list[0];
        else $data = $arg_list;
        if (empty($this->query['group'])) $this->query['group'] = array();
        $this->query['group'] = array_merge($this->query['group'], $data);
        return $this;
    }
    
    function onDuplicate($array, $strict = false, $slice_key = false) {
        $data = array();      
        if ($this->is_assoc($array)) {
            if ($strict) {
                foreach($array as $k => $v) {
                    if ($slice_key == $k) continue; 
                    $v = "VALUES($k)";  
                    $data[$k] = (object)$v;
                }    
            }
            else {
                foreach($array as $k => $v) {
                    if ($slice_key == $k) continue; 
                    $data[$k] = $v;
                }      
            }
        }
        else if (is_array($array)) {
            foreach($array as $k) {
                if ($slice_key == $k) continue;
                $v = "VALUES($k)";
                $data[$k] = (object)$v;
            }     
        }
        else $data = $array;
        
        if (empty($this->query['onduplicate'])) $this->query['onduplicate'] = array();
        $this->query['onduplicate'] = array_merge($this->query['onduplicate'], $data); 
        return $this;
    }
   
    function Where() {
        $disunction = false;
        $arg_list = func_get_args();
        if (is_bool(end($arg_list))) $disunction = array_pop($arg_list);
        if ($this->is_assoc($arg_list[0])) {
            
            foreach ($arg_list[0] as $key => $val) {
                if (is_array($val) && count($val) == 0) {
                    unset($arg_list[0][$key]);
                }
            }
            if (count($arg_list[0]) == 0)  return $this;     
            $data = $this->assocToWhere($arg_list[0]);   
        }
        else if (is_array($arg_list[0])) {
            if (count($arg_list[0]) == 0)  return $this;       
            $data = $arg_list[0];    
        }
        else if (count($arg_list)>1) {          
            $string = array_shift($arg_list);
            $data = array(vsprintf($string, $arg_list));   
        }
        else $data = $arg_list;                 
        if (empty($this->query['where'])) $this->query['where'] = array();
        array_push($this->query['where'], array($data, $disunction));     

        return $this;
    }
   
    function nWhere() {
        $disunction = false;
        $arg_list = func_get_args();
        if (is_bool(end($arg_list))) $disunction = array_pop($arg_list);
        if ($this->is_assoc($arg_list[0])) {
            
            foreach ($arg_list[0] as $key => $val) {
                if (is_array($val) && count($val) == 0) {
                    unset($arg_list[0][$key]);
                }
            }
            if (count($arg_list[0]) == 0)  return $this;     
            $data = $this->assocToNWhere($arg_list[0]);   
        }
        else if (is_array($arg_list[0])) {
            if (count($arg_list[0]) == 0)  return $this;       
            $data = $arg_list[0];    
        }
        else if (count($arg_list)>1) {          
            $string = array_shift($arg_list);
            $data = array(vsprintf($string, $arg_list));   
        }
        else $data = $arg_list;                 
        if (empty($this->query['where'])) $this->query['where'] = array();
        array_push($this->query['where'], array($data, $disunction));     

        return $this;
    }

    function dWhere() {
        $disunction = true;
        $arg_list = func_get_args();
        if (is_bool(end($arg_list))) $disunction = array_pop($arg_list);
        if ($this->is_assoc($arg_list[0])) $data = $this->assocToWhere($arg_list[0]);
        else if (is_array($arg_list[0])) $data = $arg_list[0];
        else if (count($arg_list)>1) {      
            $string = array_shift($arg_list);
            $data = vsprintf($string,$arg_list);
        }
        else $data = $arg_list;                 
        if (empty($this->query['where'])) $this->query['where'] = array();
        array_push($this->query['where'], array($data, $disunction));       
        return $this;
    }
    
    function Search() {
        $strict = false;
        $arg_list = func_get_args();
        if (is_bool(end($arg_list))) $strict = array_pop($arg_list);
        if ($this->is_assoc($arg_list[0])) $data = $this->assocToSearch($arg_list[0], $strict);
        else $data = array();                               
        if (empty($this->query['search'])) $this->query['search'] = array();
        $this->query['search'] = array_merge($this->query['search'], $data); 
        return $this;
    }
    
    function setType($type) {
        $this->sType = $type;
        return $this;   
    }

    function Count() {
        return $this->Select('COUNT(*)');
    }    

    function Select($fields = '*', $debug = false) {
        $arg_list = func_get_args();
      if (func_num_args() == 0) $fields = array('*');
          else if ($this->is_assoc($arg_list[0])) $fields = $this->assocToSelect($arg_list[0]);
          else if (is_array($arg_list[0])) $fields = $arg_list[0];
          else $fields = $arg_list;

        $q = 'SELECT ';
        $q .= $this->parse_enum($fields);
        if (isset($this->query['table'])) {
            $q .= ' FROM ';
            $q .= $this->parse_enum($this->query['table']);      
        }
            if (isset($this->query['join'])) {   
                if (count($this->query['join'])>0) {
                    foreach ($this->query['join'] as $join)  {
                        $q .= ' '.$join[2].' JOIN '.$join[0].' '.$join[1].' ';
                    }
                }
            }
        
           if (!empty($this->query['where'])) {
               $q .= ' WHERE ';
               $q .= $this->parse_where($this->query['where']);
               if (!empty($this->query['search'])) {
                   $q .= ' AND (';
                   $q .= $this->parse_search($this->query['search']);
                   $q .= ')';
               }
           }
           else {
               if (!empty($this->query['search'])) {
                   $q .= ' WHERE ';
                   $q .= $this->parse_search($this->query['search']);
               }
           }
           
           if (!empty($this->query['group'])) {
            $q .= ' GROUP BY ';
               $q .= $this->parse_enum($this->query['group']);
           }
           if (!empty($this->query['order'])) {
            $q .= ' ORDER BY ';
               $q .= $this->parse_enum($this->query['order']);
           }
           if (!empty($this->query['limit'])) {
            $q .= ' LIMIT '.$this->query['limit'][0];
            if ($this->query['limit'][1])
                   $q .= ', '.$this->query['limit'][1];
           }
        elseif ($this->sType == 'row' || $this->sType == 'one') {
          $q .= ' LIMIT 1 ';
        }
        if ($this->db !== NULL && !$debug) {
            if ($this->sType == 'one' ) return $this->db->queryOne($q);
            else if ($this->sType == 'row' ) return $this->db->queryRow($q);
            else return $this->db->query($q);
        }
        else return $q;
    }
    
    function Update() {
      $arg_list = func_get_args();
      if ($this->is_assoc($arg_list[0])) $set = $this->assocToSet($arg_list[0]);
      else if (is_array($arg_list[0])) $set = $arg_list[0];
      else $set = $arg_list;
       $q = 'UPDATE ';
       $q .= $this->parse_enum($this->query['table']);
       if ($this->query['table']) $q .= ' SET ';
       $q .= $this->parse_enum($set);
       if (!empty($this->query['where'])) {
           $q .= ' WHERE ';
           $q .= $this->parse_where($this->query['where']);
           if (!empty($this->query['search'])) {
               $q .= ' AND (';
               $q .= $this->parse_search($this->query['search']);
               $q .= ')';
           }
       }
       else {
           if (!empty($this->query['search'])) {
               $q .= ' WHERE ';
               $q .= $this->parse_search($this->query['search']);
           }
       }
        if ($this->db !== NULL) return $this->db->query($q);
        else return $q;
    }

    function Insert() {
        $arg_list = func_get_args();

        $multiple_insert = false;
        if ($this->is_assoc($arg_list[0])) {
            $r = $this->assocToInsert($arg_list[0]);

            $set = $r[0];
            $this->query['values'] = $r[1];
            
        }           
        else if (is_array($arg_list[0]))
        {
            if (isset($arg_list[0][0]) && $this->is_assoc($arg_list[0][0])) {
                $multiple_insert = array();
                foreach ($arg_list[0] as $ar) {
                    $r = $this->assocToInsert($ar);
                    $set = $r[0];  
                    array_push($multiple_insert,$r[1]);
                    $this->query['values'] = $r[1];
                }
            } 
            else if (isset($arg_list[0][0]) && str_replace('=','',$arg_list[0][0])!=$arg_list[0][0]) {
                        $c=0;
                        $count = count($arg_list[0]);
                        $set = array();
                        $this->query['values'] = array();
                        while ($c<$count) {
                            $n = explode('=',$arg_list[0][$c]);
                            array_push($set,$n[0]);
                            array_push($this->query['values'],$n[1]);
                            $c++;
                        }
            }
            else $set = $arg_list[0];    
        }
        else $set = $arg_list;
        
       if (!isset($this->query['values'])) return null;
       $q = 'INSERT INTO ';
       $q .= $this->parse_enum($this->query['table']);
       if (!empty($set)) {
           $q .= ' (';
           $q .= $this->parse_enum($set);
           $q .= ')';
       }
       $q .= ' VALUES ';
        if ($multiple_insert) for($i = 0; $i < count($multiple_insert)-1; $i++) {
           $q .= ' (';
           $q .= $this->parse_values($multiple_insert[$i]);
           $q .= '), ';     
        }
        $q .= ' (';
        $q .= $this->parse_values($this->query['values']);
        $q .= ') ';
        
        if (!empty($this->query['onduplicate'])) {
           $q .= 'ON DUPLICATE KEY UPDATE ';
           $set =  $this->assocToSet($this->query['onduplicate']); 
           $q .= $this->parse_enum($set);    
        }   
        if ($this->db !== NULL) return $this->db->query($q);
        else return $q;
    }

    function Insertignore() {
        $arg_list = func_get_args();

        $multiple_insert = false;
        if ($this->is_assoc($arg_list[0])) {
            $r = $this->assocToInsert($arg_list[0]);

            $set = $r[0];
            $this->query['values'] = $r[1];
            
        }           
        else if (is_array($arg_list[0]))
        {
            if (isset($arg_list[0][0]) && $this->is_assoc($arg_list[0][0])) {
                $multiple_insert = array();
                foreach ($arg_list[0] as $ar) {
                    $r = $this->assocToInsert($ar);
                    $set = $r[0];  
                    array_push($multiple_insert,$r[1]);
                    $this->query['values'] = $r[1];
                }
            } 
            else if (isset($arg_list[0][0]) && str_replace('=','',$arg_list[0][0])!=$arg_list[0][0]) {
                        $c=0;
                        $count = count($arg_list[0]);
                        $set = array();
                        $this->query['values'] = array();
                        while ($c<$count) {
                            $n = explode('=',$arg_list[0][$c]);
                            array_push($set,$n[0]);
                            array_push($this->query['values'],$n[1]);
                            $c++;
                        }
            }
            else $set = $arg_list[0];    
        }
        else $set = $arg_list;
        
       if (!isset($this->query['values'])) return null;
       $q = 'INSERT IGNORE INTO ';
       $q .= $this->parse_enum($this->query['table']);
       if (!empty($set)) {
           $q .= ' (';
           $q .= $this->parse_enum($set);
           $q .= ')';
       }
       $q .= ' VALUES ';
        if ($multiple_insert) for($i = 0; $i < count($multiple_insert)-1; $i++) {
           $q .= ' (';
           $q .= $this->parse_values($multiple_insert[$i]);
           $q .= '), ';     
        }
        $q .= ' (';
        $q .= $this->parse_values($this->query['values']);
        $q .= ') ';
        
        if (!empty($this->query['onduplicate'])) {
           $q .= 'ON DUPLICATE KEY UPDATE ';
           $set =  $this->assocToSet($this->query['onduplicate']); 
           $q .= $this->parse_enum($set);    
        }   
        if ($this->db !== NULL) return $this->db->query($q);
        else return $q;
    }

    function Delete() {
        if (func_num_args()>0) {
            $arg_list = func_get_args();
            if ($this->is_assoc($arg_list[0])) $where = array(array($this->assocToWhere($arg_list[0]),false));
            else if (is_array($arg_list[0])) $where = array(array($arg_list[0],false));
            else $where = array(array($arg_list,false));
        }
        else if (!empty($this->query['where'])) $where = $this->query['where'];
        else $where = array();

        $q = 'DELETE ';
        $q .= ' FROM ';
        $q .= $this->parse_enum($this->query['table']);
           if (!empty($where)) {
               $q .= ' WHERE ';
               $q .= $this->parse_where($this->query['where']);
               if (!empty($this->query['search'])) {
                   $q .= ' AND (';
                   $q .= $this->parse_search($this->query['search']);
                   $q .= ')';
               }
           }
           else {
               if (!empty($this->query['search'])) {
                   $q .= ' WHERE ';
                   $q .= $this->parse_search($this->query['search']);
               }
           }
        if ($this->db !== NULL) return $this->db->query($q);
        return $q;
    }

    private function parse_enum($enum=array()) {
        $sql='';
        $c=0;
        $count = count($enum);
        while ($c<$count) {
            if ($c>0) $sql.=', ';
            $sql.= $enum[$c];
            $c++;
        }
        return $sql;
    }

    private function parse_values($enum=array()) {
        $sql='';
        $c=0;
        $count = count($enum);
        while ($c<$count) {
            if ($c>0) $sql.=', ';
            $sql.= $this->quote($enum[$c]);
            $c++;
        }
        return $sql;
    }

    private function parse_where($where=array()) {
        $sql = '';
        $i = 0;
        $icount = count($where);
		
		//app::trace($where);
		
        while ($i < $icount)
        {
            $c = 0;
            $count = count($where[$i][0]);
            if ($where[$i][1]) $sql.= '(';
            while ($c < $count) {
                if ($c>0&&!$where[$i][1]) $sql.=' AND ';
                else if ($c>0&&$where[$i][1]) $sql.=' OR ';
                $sql.= $where[$i][0][$c];
                $c++;
            }
            $c--;
            if ($where[$i][1]) $sql.= ')';
            $i++;
            if ($i<count($where)&&!$where[$i][1])  $sql.= ' AND ';    
            else if ($i<count($where)&&$where[$i][1]) $sql.= ' OR ';             
                        
        }
        return $sql;
    }
    
    private function parse_search($where=array(),$d = false) {
        $sql='';
        $i=0;
        $icount = count($where);        

        while ($i<$icount)
        {
            $c=0;
            $count = count($where[$i][0]);
            if ($where[$i][1]) $sql.= '(';
            while ($c<$count) {
                if ($c>0&&$where[$i][1]) $sql.=' AND ';
                else if ($c>0&&!$where[$i][1]) $sql.=' OR ';
                $sql.= $where[$i][0][$c];
                $c++;
            }
            if ($where[$i][1]) $sql.= ')';
            $i++;
            if ($i<count($where)) $sql.= ' OR ';
        }
        return $sql;
    }

     public function assocToWhere($data) {
        $new = array();
        foreach ($data as $key=>$value) { 
            if ($this->is_assoc($value)) {                
                foreach ($value as $o=>$v) {    
                    if (strtolower($o) == 'between') array_push($new,$key.' BETWEEN '.$this->quote($v[0]).' AND '.$this->quote($v[1]));     
                    else array_push($new,$key.' '.$o.' '.$this->quote($v));     
                }
            }            
            else if (is_array($value) && count($value)>1) {
                if (count($value) > 0) {
                $r = "(";
                for ($i = 0; $i<count($value);$i++) {
                    if ($r!="(") $r.=",";
                    $r.=$this->quote($value[$i]);
                }
                $r.=")";                
                array_push($new,$key.' IN '.$r);
                }
                else array_push($new,'1>1');
            }
            else if(is_array($value) && count($value)==1) {
                array_push($new,$key.'='.$this->quote($value[0]));                  
            } 
            else if(is_array($value) && count($value)==0) {
               
            }     
            else array_push($new,$key.'='.$this->quote($value)); 
        }
        return $new;
    }
     
     public function assocToNWhere($data) {
        $new = array();
        foreach ($data as $key=>$value) { 
            if ($this->is_assoc($value)) {                
                foreach ($value as $o=>$v) {    
                    if (strtolower($o) == 'between') array_push($new,$key.' BETWEEN '.$this->quote($v[0]).' AND '.$this->quote($v[1]));     
                    else array_push($new,$key.' '.$o.' '.$this->quote($v));     
                }
            }            
            else if (is_array($value) && count($value)>1) {
                if (count($value) > 0) {
                $r = "(";
                for ($i = 0; $i<count($value);$i++) {
                    if ($r!="(") $r.=",";
                    $r.=$this->quote($value[$i]);
                }
                $r.=")";                
                array_push($new,$key.' NOT IN '.$r);
                }
                else array_push($new,'1>1');
            }
            else if(is_array($value) && count($value)==1) {
                array_push($new,$key.'='.$this->quote($value[0]));                  
            } 
            else if(is_array($value) && count($value)==0) {
               
            }     
            else array_push($new,$key.'='.$this->quote($value)); 
        }
        return $new;
    }     
    
    public function assocToSearch($data, $strict) {
        $new = array();
        foreach ($data as $key=>$value) {    
            
            if (!is_array($value)) {
                $value = preg_replace('/[\s,.\%_]+/', ',', $value);
                $value = explode(',',$value);
            }        
            $r = array();
            foreach ($value as $v) {                 
                array_push($r,$key.' LIKE '.$this->quote('%'.$v.'%'));
            }
            array_push($new,array($r,$strict));
        }
        return $new;
    }        
    
    public function assocToInsert($data) {
        $res[0] = array_keys($data);
        $res[1] = array_values($data);
        return $res;
    }

     public function assocToSet($data) {
        $new = array();
        foreach ($data as $key=>$value) {
            array_push($new,$key.'='.$this->quote($value));
        }
        return $new;
    }
     
    public function assocToSelect($data) {
        $new = array();
        foreach ($data as $key=>$value) {
            array_push($new,$key.' as '.$this->quote($value));
        }
        return $new;
    }

    public function assocToOrder($data) {
        $new = array();
        foreach ($data as $key=>$value) {
            if (!$value) $value = 'ASC';
            array_push($new,$key.' '.$value);
        }
        return $new;
    }

    public function is_assoc($array) {
        return (is_array($array) && 0 !== count(array_diff_key($array, array_keys(array_keys($array)))));
    }

    private function quote($value){    
        /*if (!get_magic_quotes_gpc() && $this->db->getDBMS() == 'mysql') {
            $value = mysql_real_escape_string($value);            
        }*/     


        $db_ini = parse_ini_file(app::$settings->config_path._.'db.cfg', true); 
    
        $connect = array(
            'host'        => $db_ini['db']['db_host'],
            'database'    => $db_ini['db']['db_name'],
            'user'        => $db_ini['db']['db_user'],
            'password'    => $db_ini['db']['db_password'],
        );      
    
		$link = \mysqli_connect($connect['host'], $connect['user'], $connect['password'],$connect['database']); 

        if (is_object($value) && isset($value->scalar)) {
            return $value->scalar;    
        }  
        else if ($value === null) {
            return 'NULL';
        }
        else if ($this->auto_quoting && is_numeric($value)) {
            return $value;            
        }
        else if (gettype($value) == 'string') {
          // print_r(mysqli_real_escape_string($link,$value));
          // exit;
            return '\''.mysqli_real_escape_string($link,$value).'\''; 
        }
        else if (gettype($value) != 'string') {
            return $value;
        }
        else if (!$this->auto_quoting && !is_string($value)) {
            return $value;            
        }
        else {               
            return '\''.mysqli_real_escape_string($link,$value).'\'';
        }
    }

    function __destruct(){
        $this->query = NULL;
        $this->db = NULL;
        $this->sType = NULL;
    }   
    
    public static function f($func) {
        return new qbfunc($func);
    }
}

