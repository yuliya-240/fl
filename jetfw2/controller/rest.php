<?php    
namespace jet\controller; 
use \jet\application as app;
use \jet\view\twig as View; 
/* Класс HTTP контроллера */
class rest extends \jet\controller {    
   
    public $headers = array();
    
    
    public function __construct() {          
        parent::__construct();

        if (app::$settings->content_type)  \header(app::$settings->content_type);    
        
        if (app::$settings->use_session) {
            
            if (app::$settings->session_name) 
                \session_name(app::$settings->session_name);  
                          
            if (app::$settings->session_id) {
                \session_id(app::$settings->session_id);                           
            }          
            
            \session_start();  
        } 
        
        foreach($_SERVER as $key => $value) {
        	if (substr($key, 0, 5) <> 'HTTP_') {
            	continue;
            }
            $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $this->headers[$header] = $value;
        }
        //$str = var_export($this->headers,true);
    
    


    }

          
             
    
    
    public function __before() {
    }
    
    
    public function __default() {
        //$this->view->message = "Hello world!";
    }  

    
    public function __error($e) {        
        if (isset($e->is_jet)) {
            $e->setFormat($this->__getFormat());            
        } else {
            $e = new \jet\exception($e);
            $e->setFormat($this->__getFormat());              
        }
        die($e);
    }
    
    public function __http404() {
        
    }
    
    public function __after() {
       
    }    

}
