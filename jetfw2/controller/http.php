<?php    
namespace jet\controller; 
use \jet\application as app;
use \jet\view\twig as View;   

/* Класс HTTP контроллера */
class http extends \jet\controller {    
    protected $view;
    
    public function __construct() {          
        parent::__construct();

        if (app::$settings->content_type)  \header(app::$settings->content_type);    
        
        if (app::$settings->use_session) {
            
            if (app::$settings->session_name) 
                \session_name(app::$settings->session_name);  
                          
            if (app::$settings->session_id) {
                \session_id(app::$settings->session_id);                           
            }          
            
            \session_set_cookie_params(time()+60*60*24*30);
            
            \session_start();  
        }   
             
    }
    
    public function __before() {
	    if(isset($_SERVER['HTTPS']))$_SESSION['http']='https://'; else $_SESSION['http']='http://';
    
 
        
    }
    
    public function __default() {
        //$this->view->message = "Hello world!";
    }  


	public function handleClientUrl(){
		if(isset($_SERVER['HTTPS']))$_SESSION['http']='https://'; else $_SESSION['http']='http://';
    	if(!isset($_SESSION['INVOICE_RET_URL']))$_SESSION['INVOICE_RET_URL'] = $_SESSION['http'].$_SERVER['HTTP_HOST'].'/client/invoices';
    	if(!isset($_SESSION['ESTIMATE_RET_URL']))$_SESSION['ESTIMATE_RET_URL'] = $_SESSION['http'].$_SERVER['HTTP_HOST'].'/client/estimates';
		
	}
	
	 public function handleURL(){
    	
    	
    	$url = $_SESSION['http'].$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    	
    	if(!isset($_SESSION['CURR_URL'])  ) {
    		$_SESSION['CURR_URL'] = $url;
    		$_SESSION['RET_URL'] = $url;
    	}
    	
    	if($_SESSION['CURR_URL']!=$url){
    		$_SESSION['RET_URL'] = $_SESSION['CURR_URL'];
    	}
    	
    	
    	$_SESSION['CURR_URL'] = $_SESSION['http'].$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    	$_SESSION['backlogin'] = $url; 
    	return  true;
	}

    
    public function __error($e) {        
        if (isset($e->is_jet)) {
            $e->setFormat($this->__getFormat());            
        } else {
            $e = new \jet\exception($e);
            $e->setFormat($this->__getFormat());              
        }
        die($e);
    }
    
    public function __http404() {
        
    }
    
    public function __after() {
       
    }    

}
