<?php
namespace jet;

class exception extends \exception {
    
    protected $details = array();
    
    protected $format = 'HTML';   
    
    protected $HTTPCode = 200;    
     
    public $is_jet = true;      
    
    public function __construct($message = "Unknown", $code = E_USER_ERROR, $details = array() , \exception $previous = null) {    
        
        if ($message instanceof \exception) { 
            $this->line = $message->getLine(); 
            $this->file = $message->getFile();         
            $this->trace = $message->getTrace();
            parent::__construct($message->getMessage(), $message->getCode(), $message->getPrevious());
        }
        else {
            
            if (is_array($message)) {     
                
                $code = $message['type']; 
                $this->line = $message['line']; 
                $this->file = $message['file'];
                $message = $message['message'];  
            }                                         
            if (!empty($details)) {
                
                if (isset($details['line']))  {
                    $this->line = $details['line'];    
                    unset($details['line']);
                }
                if (isset($details['file'])){
                    $this->file = $details['file'];    
                    unset($details['file']);    
                }  
                if (isset($details['httpcode']))    $this->HTTPCode = $details['httpcode']; 
                $this->details = $details;                                      
            }

            parent::__construct($message, $code, $previous); 
            
        }
        $this->checkForHTTPCodeError();
    }
    

                 
    public function setFormat($format) {   
        switch (strtolower($format)) {
            case 'json':
                $this->format = 'JSON';
                break;         
            case 'html':
                $this->format = 'HTML';   
                break;     
            case 'string':
                $this->format = 'String';   
                break;         
            default:                
                break;  
        }        
        return $this;   
    }
    
    public function getFormat($format) {
        return strtolower($this->format);
    }               

    public function __toString() {
        $view_function = "getAs{$this->format}";
        
        return $this->$view_function(); 
    }
    
    public function getDetails() {
        return $this->details; 
    }
    
    public function __get($key) {
        if (isset($this->details[$key])) {
            return $this->details[$key]; 
        }
        else {
            return null;
        }
    }
    
    public function getDetailsAsString() {
        $string = "";
        if (is_array($this->details)) {
            foreach ($this->details as $key => $value) {    
                $string .= "[".$key."]: ".$value."\n";    
            }
        }
        return $string; 
    } 
    
    public function getDetailsAsHTML() {
        $string = "";
        if (is_array($this->details)) {
            foreach ($this->details as $key => $value) {
                $string .= "<p><b><i>$key:</i></b> $value</p>";      
            }
        }
        return $string; 
    } 
    
    public function getAsString() {
        return "Error | ".$this->getMessage()." \nIn ".$this->getFile()." on line ".$this->getLine().". Level-type-code: ".$this->getCode().".\n\nDetails:\n".$this->getDetailsAsString()."\nBacktrace:\n".$this->getTraceAsString();                         
    }

    public function getAsHTML() {
        return "<div style='font-family: verdana;'><h3 style='color: #DB2325'>Error</h3><p><b>".$this->getMessage()."</b></p><p>in <b>".$this->getFile()."</b> on <b>line ".$this->getLine()."</b>. Level-type-code: ".$this->getCode()."</p>".$this->getDetailsAsHTML()."<p><b>Backtrace:</b><pre style='font-family: verdana;'>".$this->getTraceAsString()."</pre></p></div>";     
    }
    
    public function getAsJSON() {
        return json::encode(array('error' => $this->getAsArray())); 
    }
    
    public function getAsArray() {
        return array(
            "message" => $this->getMessage(),
            "code"    => $this->getCode(),
            "details" => $this->getDetails(),
            "line" => $this->getLine(),
            "file" => $this->getFile(),
            'trace' => $this->getTrace()
        );
    }         
    
    private function checkForHTTPCodeError() {
        preg_match("/HTTP\.([\d]{3})/i", $this->getMessage(), $m);     
         
        if (isset($m[1])) {
            $this->HTTPCode = intval($m[1]);  
        }      

        if ($this->code == E_ERROR  || 
        $this->code == E_CORE_ERROR ||
        $this->code == E_PARSE ||
        $this->code == E_COMPILE_ERROR) {
            $this->HTTPCode = 500;    
        }                  
        
        if ($this->HTTPCode < 500 && $this->HTTPCode > 200) {
            header("HTTP/1.0 ".$this->HTTPCode." ".self::HTTPErrors($this->code));         
        }
    }
    
    private static function HTTPErrors($code) {
        switch($code) {
            case 404:
                return "Not Found";
            case 500:
                return "Internal Server Error";
        }    
    }          
}
