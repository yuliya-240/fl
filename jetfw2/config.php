<?php
namespace jet;

class config {
  
  public $file;
  
  public $autosave;
  
  private $conf_orig;
    
  private $conf;     
    
  /**
   * Конструктор
   */
  public function __construct($file = '', $autosave = false){
    $this->file = $file;
    if (!$this->file) throw new exception('Config.WrongArgument', E_USER_WARNING, array('info' => 'Не передан путь до конфигурационного файла') );
    if (!file_exists($this->file))  
      throw new exception('Config.FileNotFound', E_USER_WARNING,  array('file' => $this->file));
    $this->autosave = $autosave;
    $this->conf = parse_ini_file($this->file,true);
    if (count($this->conf) == 0) {
      throw new exception('Config.IsEmpty', E_USER_WARNING, array('file' => $this->file,'info'=> 'Конфигурационный файл пуст'));
    }
    $this->conf_orig = $this->conf;
    if ($this->autosave) {
        $this->checkRights();  
    }
    
  }
  // ---------------------------------------------------------------------------
  
  /**
   * Деструктор
   */
  public function __destruct() {
    if ($this->autosave) $this->__save($this->conf);
  }
  // ---------------------------------------------------------------------------
  
  /**
   * Гет
   */
  public function __get($name){
    if ($name == 'all') return $this->conf;
    if (isset($this->conf[$name])) return $this->conf[$name]; 
    return '';
  }
  // ---------------------------------------------------------------------------
  
  /**
   * Сет
   */
  public function __set($name, $value){
    $this->conf[$name] = $value;
  }
  // ---------------------------------------------------------------------------
  
  /**
   * Проверяем права на запись
   */
  public function checkRights() {
    if (is_writable($this->file) === false) {
        throw new exception('Config.NoWriteAcces', E_USER_WARNING, array('file' => $this->file,'info'=> 'Не достаточно прав на запись'));      
    }    
  }
  // ---------------------------------------------------------------------------
  
  /**
   * Сохраняем конфиги
   */
  public function save($conf = false) {
    // проверим права на запись
    $this->checkRights();
    $this->__save($conf);    
  }
  // ---------------------------------------------------------------------------
  
  /**
   * Сохраняем конфиги
   */
  private function __save($conf = false){
    // если ничего не передано, то сохраняем текущие настройки
    if (!$conf) $conf = $this->conf; 
    
    // само сохранение
    $tmp = '';
    foreach($conf as $section => $values){
        if (is_array($values)) {
            $tmp .= "[$section]\n";
            foreach($values as $key => $val){
                if(is_array($val)){
                    foreach($val as $k =>$v){
                        $tmp .= "{$key}[$k] = \"$v\"\n";
                    }
                }
                else
                    $tmp .= "$key = \"$val\"\n";
            }
        }
        else {
            if(is_array($values)){
                foreach($values as $k =>$v){
                    $tmp .= "{$section}[$k] = \"$v\"\n";
                }
            }
            else
                $tmp .= "$section = \"$values\"\n";    
        }
        $tmp .= "\n";
        
    }
    if (trim($tmp) != '') {
      file_put_contents($this->file, $tmp);
      unset($tmp);
    }
  }
  // ---------------------------------------------------------------------------
  
  /**
   * Отдадим все конфиги
   */
  public function configs(){
    return $this->conf;
  }
  // ---------------------------------------------------------------------------
  
  /**
   * Отменяем изменения, сохранив начальные значения
   */
  public function discard(){
    $this->save($this->conf_orig);
  }
  // ---------------------------------------------------------------------------
}
