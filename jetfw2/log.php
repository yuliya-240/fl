<?php
namespace jet;

class log {
  
  /**
   * Режим записи лога
   */
  static public $recording_mode = false;
  
  /**
   * Количество строк лога 0 - бесконечный 
   */
  static public $log_line_count = 10;
  
  /**
   * Записываем в лог
   */
  public static function write($text, $tag = 'default'){
    if (self::$recording_mode) {
      if(!$fh = @fopen(self::file_path(),"r+")){
        if (!file_exists(self::file_path())) {
          $fh = @fopen(self::file_path(),"wr+");
          if (!file_exists(self::file_path())) 
            throw new \jet\exception('LOG.FileNotFound', E_USER_NOTICE, array('info'=>'Не найден файл debug.log'));          
        } else {                                                                                                              
          throw new \jet\exception('LOG.NoWritingRights', E_USER_NOTICE, array('info'=>"Установите права на запись " . self::file_path()));            
        }     
      }                                 
      $content = explode("\r", file_get_contents(self::file_path()));
      $fh = fopen(self::file_path(),"w");  
      array_unshift($content, date('Y-m-d H:i:s') . "\t" . $tag . "\t" . $text);
      if(self::$log_line_count) array_splice($content, self::$log_line_count);
      fwrite($fh, implode("\r", $content));
      fclose($fh);
    } 
  }
  //----------------------------------------------------------------------------
  
  /**
   * Чистим лог
   */
  public static function clear(){
    if (file_exists(self::file_path())) {
      $fh = fopen(self::file_path(),"w");
      fclose($fh);      
    }
  }
  //----------------------------------------------------------------------------
  
  /**
   * Достаем значения из лога
   */
  public static function get($tag = ''){
    if (file_exists(self::file_path())) {
      if ($tag) {         
        $pattern = '/\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}[\s]+?('.$tag.')[\s]+?(.*?)(\r|$)/';
        preg_match_all($pattern, file_get_contents(self::file_path()), $matches, PREG_PATTERN_ORDER);
        if (count($matches[0]) > 0) 
            return implode("\r\n", $matches[0]);
        return "Не найдено совпаденией по тегу '{$tag}'";
      } else {
        return file_get_contents(self::file_path());
      }
    }
    return  '';
  }
  //----------------------------------------------------------------------------
  

  /**
   * Возвращает путь до файла лога
   */
  public static function file_path(){
    if (isset(application::$settings->path)) {
      return application::$settings->path ._. 'debug.log';
    } else {
      $_SERVER['DOCUMENT_ROOT'] ._. 'debug.log';
    }
  }
  //----------------------------------------------------------------------------
}  