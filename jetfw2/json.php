<?php
namespace jet;

class json {

  protected $json_obj;
    
  public function __construct($obj) {
        $this->json_obj = $obj;
  }
  
  public function __toString() {
        return self::encode($this->json_obj);    
  }
   
  /**
   * Json_encode
   * Функция не преобразует ключи в utf-8
   * {"\u0410":"Б","\u0412":"Г","\u0414":"Е","\u0401":"Ж","\u0417":"И"}
   */
  public static function encode($arr){
    //convmap since 0x80 char codes so it takes all multibyte codes (above ASCII 127). So such characters are being "hidden" from normal json_encoding
    \array_walk_recursive($arr, 
    
        function (&$item, $key) {
            if (\is_string($item)) 
                $item = \mb_encode_numericentity($item, array (0x80, 0xffff, 0, 0xffff), 'UTF-8');
        }
    
    );
    
    $json = \mb_decode_numericentity(\json_encode($arr), array (0x80, 0xffff, 0, 0xffff), 'UTF-8');
    return $json;    
  }
  //----------------------------------------------------------------------------
  
  public function __invoke($arr) {
      return self::encode($arr);
  }
  /**
   * Json_dencode
   */
  public static function decode($json, $assoc = true){
    return \json_decode($json, $assoc);
  }
  //----------------------------------------------------------------------------
  
}  