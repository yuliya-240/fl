<?php
namespace jet\rpc\json;

use \jet\json as json;

class server {
    
    private static $session_id = null;
    private static $result = null;
    private static $request = null;
    private static $error = null;    
    private static $_error = null;       
    
    public static function checkRequest() {
        if (self::$request === null) {
            $input = \file_get_contents('php://input');
            self::$request = json::decode($input,true);  
        }
        if (self::$request) { 
            return true;
        }
        else {
            return false;
        }

    }
    
    public static function getRequest() {
        if (self::$request === null) {
            if (!self::checkRequest()) self::$request = false;           
        }      
        return self::$request;         
    }
    
    public static function getRequestMethod() {
        $request = self::getRequest(); 
        if (!empty($request['method'])) return $request['method'];                 
        else return null;
    }
    
    public static function getRequestParams() {
        $request = self::getRequest();      
        if (!empty($request['params'])) return $request['params'];                 
        else return array();            
    }
    
    public static function setSessionId($id) {
        self::$request['id'] = $id;  
    }
    
    public static function getSessionId() {
        $request = self::getRequest();
        if (!empty($request['id'])) return $request['id'];                 
        return null;                 
    }
    
    public static function setError($error, $e = null) {
        if ($e && method_exists($e,'getLine')) {
            self::$_error = array(
                'line' => $e->getLine(),
                'file' => $e->getFile(),
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            );
        }
        self::$error = $error;
    }
    
    public static function setResult($result) {
        self::$result = $result;    
    }
    
    public static function getResponse() {
        return json::encode(array(
             'id'       => self::getSessionId(),
             'result'   => self::$result,
             'error'    => self::$error,
             '_error'   => self::$_error
         ));
    }
    
    public static function throwResponse() {
        header('content-type: application/json'); 
        if (self::getSessionId() || self::$error) {
            die(self::getResponse());  
        }  
        else die();
    }
    
    public static function renderResponse() {
        header('content-type: application/json'); 
        if (self::getSessionId() || self::$error) {
            echo self::getResponse();  
        }  
    }
    
    public static function handleError($errno, $errstr, $errfile, $errline, array $errcontext)
    {
        if (0 === error_reporting()) {
            return false;
        }
        throw new fault($errstr, 0, $errno, $errfile, $errline);
    }

    public static function handle($object, $render = true) {
        set_error_handler('\jet\rpc\json\server::handleError');

        $request = self::getRequest();
        
        if (!$request) return false;   
        
        if (!method_exists($object, self::getRequestMethod())) {
            self::setError('There is no such remote method like ['.self::getRequestMethod().'] in class ['.$Controller.']');                                  
        } 

        try {
            $result = call_user_func_array(array($object, self::getRequestMethod()), self::getRequestParams());
            self::setResult($result);
        } 
        catch (\exception $e) {
            self::setError($e->getMessage(),$e);   
        }
        
        restore_error_handler();
        
        if ($render) {
            self::renderResponse();
        }

        return true;
    }
  
}
