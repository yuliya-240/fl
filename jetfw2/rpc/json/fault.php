<?php
namespace jet\rpc\json;   

class fault extends \exception {
    public function __construct($errstr, $errno = 0, $errfile = null, $errline = 0) {
        $this->line = $errline;
        $this->file = $errfile;
        if (is_array($errstr)) $errstr = serialize($errstr);
        parent::__construct($errstr, $errno);
    }     
}