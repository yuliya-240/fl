<?php
namespace jet\utils; 

class jetIO {  
	public static function redirect($url, $exit = true) {
		header("Location: $url");
		if ($exit) {
			exit();
		}
	}

	/**
	 * Client-side GET: This function builds the full HTTP URL with parameters and redirects via Location header.
	 * 
	 * @param string $url Destination URL
	 * @param array $data Data
	 * @param boolean $exit Whether to call exit() right after redirection
	 */
	public static function clientGet($url, $data = array(), $exit = true) {
		if($data)$url = $url.'?'.http_build_query($data, '', '&');
		self::redirect($url, $exit);
	}

	/**
	 * Generates a simple HTML form with $data initialized and post results via JavaScript
	 * 
	 * @param string $url URL to be POSTed
	 * @param array $data Data to be POSTed
	 */
	public static function clientPost($url, $data = array()) {
		$html = '<html><body onload="postit();"><form name="auth" method="post" action="'.$url.'">';
		
		if (!empty($data) && is_array($data)) {
			$flat = self::flattenArray($data);
			foreach ($flat as $key => $value) {
				$html .= '<input type="hidden" name="'.$key.'" value="'.$value.'">';
			}
		}
		
		$html .= '</form>';
		$html .= '<script type="text/javascript">function postit(){ document.auth.submit(); }</script>';
		$html .= '</body></html>';
		echo $html;
	}
	
	/**
	 * Basic server-side HTTP GET request via self::httpRequest(), wrapper of file_get_contents
	 * 
	 * @param string $url Destination URL
	 * @param array $data Data to be submitted via GET
	 * @param array $options Additional stream context options, if any
	 * @param string $responseHeaders Response headers after HTTP call. Useful for error debugging.
	 * @return string Content resulted from request, without headers
	 */
	public static function serverGet($url, $data, $options = null, &$responseHeaders = null) {
		return self::httpRequest($url.'?'.\http_build_query($data, '', '&'), $options, $responseHeaders);
	}

	/**
	 * Basic server-side HTTP POST request via self::httpRequest(), wrapper of file_get_contents
	 * 
	 * @param string $url Destination URL
	 * @param array $data Data to be POSTed
	 * @param array $options Additional stream context options, if any
	 * @param string $responseHeaders Response headers after HTTP call. Useful for error debugging.
	 * @return string Content resulted from request, without headers
	 */
	public static function serverPost($url, $data, $options = array(), &$responseHeaders = null) {
		if (!is_array($options)) {
			$options = array();
		}

		$query = \http_build_query($data, '', '&');

		$stream = array('http' => array(
			'method' => 'POST',
			'header' => "Content-type: application/x-www-form-urlencoded",
			'content' => $query
		));

		$stream = array_merge_recursive($options, $stream);

		return self::httpRequest($url, $stream, $responseHeaders);	
	}
	
	/**
	 * Simple server-side HTTP request with file_get_contents
	 * Provides basic HTTP calls.
	 * See serverGet() and serverPost() for wrapper functions of httpRequest()
	 * 
	 * Notes:
	 * Reluctant to use any more advanced transport like cURL for the time being to not 
	 *     having to set cURL as being a requirement.
	 * Strategy is to provide own HTTP transport handler if requiring more advanced support.
	 * 
	 * @param string $url Full URL to load
	 * @param array $options Stream context options (http://php.net/stream-context-create)
	 * @param string $responseHeaders Response headers after HTTP call. Useful for error debugging.
	 * @return string Content resulted from request, without headers
	 */
	public static function httpRequest($url, $options = null, &$responseHeaders = null) {
		$context = null;
		if (!empty($options) && is_array($options)) {
			$context = stream_context_create($options);
		}

		$content = file_get_contents($url, false, $context);
		$responseHeaders = implode("\r\n", $http_response_header);

		return $content;
	}
	


}