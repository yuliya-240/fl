<?php  

require_once "../jetfw2/jet.php";   

require_once "../application/application.php"; 

use \floctopus\application as iiApplication;

$myApp = iiApplication::create(array('debug'=>true));  

$myApp->run($_SERVER['REQUEST_URI']);  
?>     