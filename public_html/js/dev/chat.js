function  strip_tags(str, allowed_tags) { 
  var key = '', allowed = false; 
  var matches = []; 
  var allowed_array = []; 
  var allowed_tag = ''; 
  var i = 0; 
  var k = ''; 
  var html = ''; 

  var replacer = function(search, replace, str) { 
      return str.split(search).join(replace); 
  }; 

  // Build allowes tags associative array 
  if (allowed_tags) { 
      allowed_array = allowed_tags.match(/([a-zA-Z]+)/gi); 
  } 

  str += ''; 

  // Match tags 
  matches = str.match(/(<\/?[\S][^>]*>)/gi); 

  // Go through all HTML tags 
  for (key in matches) { 
      if (isNaN(key)) { 
          // IE7 Hack 
          continue; 
      } 

      // Save HTML tag 
      html = matches[key].toString(); 

      // Is tag not in allowed list? Remove from str! 
      allowed = false; 

      // Go through all allowed tags 
      for (k in allowed_array) { 
          // Init 
          allowed_tag = allowed_array[k]; 
          i = -1; 

          if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+'>');} 
          if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+' ');} 
          if (i != 0) { i = html.toLowerCase().indexOf('</'+allowed_tag)   ;} 

          // Determine 
          if (i == 0) { 
              allowed = true; 
              break; 
          } 
      } 

      if (!allowed) { 
          str = replacer(html, "", str); // Custom replace. No regexing 
      } 
  } 

  return str; 
}

function sendMsg(inmsg, h =0){

	var ch = $("#_pu-ch").val();
    var msg = '<li class="self"><div class="msg">'+inmsg+'</div></li>';
    $("#chat-flow").append(msg);
	
	$.post("/messenger/send",{msg:inmsg,channel:ch},function(r){},"json")
	$("#"+ch).prependTo("#chat-contact-lis");
	
	scrollToBottom(h);
	return true;
	
}

function scrollToBottom(h1 = 0) {
	
	$('#chat-main-box').imagesLoaded( function() {
			
		var h = document.getElementById("chat-main-box").scrollHeight;
		$('#chat-main-box').scrollTop(h);
		
	});
	
}

function setUserActive(){
	
	var ch = $("#_pu-ch").val();
	

	$("#"+ch).addClass("active")
	
	if(ch=="floctoid"){
		$("#ch-in-b").removeClass("col-sm-10").addClass("col-sm-11");
		$("#btn-cha-attach").hide();
	}else{
		$("#ch-in-b").removeClass("col-sm-11").addClass("col-sm-10");
		$("#btn-cha-attach").show();
		
	}
	
	var pic = $("#"+ch+"lnk").data("pic");
	var name = $("#"+ch+"lnk").data("name");
	var title = $("#"+ch+"lnk").data("title");

	$("#chat-head-user-image").prop("src",pic);
	$("#chat-head-usr-name").text(name);
	$("#chat-head-usr-title").text(title);
	
	$("#unread"+ch).text("");
	$("#unreadval"+ch).val(0);
	$.get("/messenger/getChat",{ch:ch},function(r){
		
		$("#chat-flow").html(r.html);
		scrollToBottom();
		
	},"json");
	
}

$(document).on("click","#showMessanger",function(){
	showProcess();
		$("#msgUnreadCount").hide();
		$('#msg-box').modal('show');
		$('.modal-content').css('height',$( window ).height()*0.9);
	
	$.get("/messenger/getContent",function(r){
		
		$("#chatopen").val(1);
		$("#ch-c").html(r);
		
		var ch = $("#_pu-ch").val();
		$("#"+ch).prependTo("#chat-contact-lis");

		$.get("/messenger/getUnread",function(r){
			
			if(r.status){
				$(".user").each(function(){
					
					var id = $(this).prop("id");
					
					var k = arraySearch(r.unrids,id);
					
					console.log(k)
					if(k!==false){
						$("#unread"+id).text(r.unrcnt[k]);
						$("#unreadval"+id).val(r.unrcnt[k]);
						$( "#"+id ).insertAfter( ".user.active" );
						
					}
				})
				setUserActive();	
			}
			
			
		},"json")
		endProcess();
		
		chatuploader = new plupload.Uploader({
				runtimes : 'html5,flash,html4',
				browse_button : 'chAttach', // you can pass an id...
				container: document.getElementById('btn-cha-attach'), // ... or DOM Element itself
				url : '/img/uploadchat',
				unique_names:true,
				multi_selection:false,
				max_file_count: 1,
			
				
				filters : {
					max_file_size : '64mb',
					mime_types: [
						{title : "Image files", extensions : "jpg,gif,png,jpeg"},
						
					]
				},
			
				init: {
					PostInit: function() {
						
					},
			
					FilesAdded: function(up, files) {
						
						swal({
							title:"Loading...",
							text:'<div class="progress  progress-striped active" id="progress1" ><div id="progressSlide" style="width: 0%;" class="progress-bar progress-bar-success">Loading...</div></div>',
							showCancelButton:false,
							showConfirmButton:false,
							html: true
						});
						
						up.refresh(); // Reposition Flash/Silverlight
						chatuploader.start();
					},
			
					UploadProgress: function(up, file) {
						$("#progressSlide").css('width',file.percent+"%");
						$("#progress1").data('percent',file.percent+"%");
					},
			
					Error: function(up, err) {
						
						swal({
							title:err.message,
							text:err.code,
							type:"error"
						})
				
						up.refresh(); // Reposition Flash/Silverlight
				
					},
					
					FileUploaded: function(up, file, info) {
						//swal.close();
						var obj = JSON.parse(info.response);
						if(!obj.status){
							
							
						}else{
						swal({
							title:"<small>Ready to send</small>",
							//text:'<img class="img-responsive" src="/chat/'+obj.cleanFileName+'">',
							imageUrl:"/chat/"+obj.cleanFileName,
							imageSize:obj.fsize,
							showCancelButton:true,
							showConfirmButton:true,
							confirmButtonText:"Send",
							html: true
						},function(isConfirm){
							
							if (isConfirm) {
								
								var h = obj.height;
								var m = '<img class="img-responsive" src="/chat/'+obj.prefix+obj.cleanFileName+'">';
								sendMsg(m,h);
							}
							
						});
							
											
						}
					
						
					}
				}
		});	

		chatuploader.init();
		setUserActive();
		var isSound = $("#_chsound").val();
		if(isSound=="1"){
			$(".btn-sound-off").show();
			$(".btn-sound-on").hide();
		}else{
			$(".btn-sound-off").hide();
			$(".btn-sound-on").show();
		}

	
	});
	
	return false;
});

$('#msg-box').on('hidden.bs.modal', function () {
   $("#chatopen").val(0);
   $("#ch-c").html("");
})


function onTestChange() {
    var key = window.event.keyCode;

    // If the user has pressed enter
    if (key === 13) {
        console.log("Enter");
        $("#btn-chat-s").trigger("click");

        return false;
    }
    else {
        return true;
    }
}


$(document).on("click","#btn-chat-s",function(){
	
	var s = $("#chat-m").val();
	
	s = $.trim(s);
	
	s = strip_tags(s);
	if(s.length){
		var html = urlify(s);
		
		sendMsg(html);
		
	}	
	$("#chat-m").val("");
	return false;
});

$(document).on("click",".ch-usr",function(){
	
	$(".ch-usr-item").removeClass("active");
	var ch = $(this).data("ch");
	$("#_pu-ch").val(ch);
	setUserActive();
	return false;
});

$(document).on("click",".btn-sound-off",function(){
	
	$(this).hide();
	$("#_chsound").val(0);
	$(".btn-sound-on").show();
	
	$.post("/messenger/changeSound",{val:0});
})

$(document).on("click",".btn-sound-on",function(){
	
	$(this).hide();
	$("#_chsound").val(1);
	$(".btn-sound-off").show();
	$.post("/messenger/changeSound",{val:1});
})


Pusher.logToConsole = true;

var pusher = new Pusher('927e2dc012613094456b', {
  encrypted: true,
  authEndpoint: '/messenger/auth' 
  
});

var msg = $("#_mch").val();

var brd = $("#_broadcast").val();

var chmessages = pusher.subscribe(msg);

var broadcast = pusher.subscribe(brd);

chmessages.bind('msg', function(data) {
	var ch = $("#_pu-ch").val();
	var isopen = $("#chatopen").val();
	var isSound = $("#_chsound").val();
	if(isopen=="1"){
		if(ch==data.from){
			
			var id = data.id;	
			var html = urlify(data.msg);
			$.post("/messenger/markUnread",{id:id,ch:ch});
			var m = '<li class="friend"><div class="msg">'+html+'</div></li>';
			$("#chat-flow").append(m);
			scrollToBottom();
			
			if(isSound=="1"){
				var audio = new Audio('/sounds/out.mp3');
				audio.play();
					
			}

			
		}else{
			
			var ch = data.from;
			var count = $("#unreadval"+ch).val();
			
			if(!count.length)count=0;
			var c = parseInt(count);
			c++;
			$("#unread"+ch).text(c);
			$("#unreadval"+ch).val(c);
			$( "#"+ch ).insertAfter( ".user.active" );
			
			if(isSound=="1"){

				var audio = new Audio('/sounds/in_active.mp3');
				audio.play();
			}

		}
		
	}else{
		
		$("#msgUnreadCount").show();
		if(isSound=="1"){
			var audio = new Audio('/sounds/in_active.mp3');
			audio.play();
			}
		
	}
});

function arraySearch(arr,val) {
    for (var i=0; i<arr.length; i++)
        if (arr[i] === val)                    
            return i;
    return false;
}

function urlify(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a target="_blank" href="' + url + '">' + url + '</a>';
    })
    // or alternatively
    // return text.replace(urlRegex, '<a href="$1">$1</a>')
}



