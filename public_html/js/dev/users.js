function getContent(){
	
	$.get('/users/getContent',function(r){
		
		$("#d-content").html(r.html);
		
		var c = parseInt(r.count);
		if(c>0){
			
			$("#megafooter").html(r.footer);
		}
		endProcess();
		
	},"json");
	
}

$(document).ready(function() {
	
	getContent();
});

$(document).on("click",".del",function(){
	
	 var id= $("#vcollab_id").val();
	 var title = $("#_udt").val();
	 var msg = $("#_udm").val();
	 var yb = $("#_yb").val();
	 var cb = $("#_cb").val();
	 
	 swal({
		 title:title,
		 text:msg,
		 type: "warning",
		 showCancelButton: true,
		 confirmButtonColor: "#DD6B55",
		 confirmButtonText: yb,
		 cancelButtonText: cb,
	 },function(result){
	 	if(result){
			 showProcess();
			 $.post('/users/delete',{id:id},function(r){
				$("#profile-view-box-content").html("");
				$("#profile-list-box").show();
				$("#btn-add-collab").show();
				$("#megafooter").show();
				
				$("#btn-back-list").hide();
				$("#profile-view-box").hide();
				$("#btn-del").hide();
				getContent()
				endProcess();

			 },"json");
		 	
	 	}
	 })
	
	
	return false;
});
 
$(document).on("click","#bundleDelete",function(){


	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_bdt").val();
	var msg = $("#_bdm").val();
	var yes = $("#_yes").val();

	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true,
			showCancelButton: true,},
			function(){
					
				showProcess();	
				$.ajax({
						type:'POST', 
						url: '/users/deleteBundle', 
						data:$("#flist").serialize(), 
						dataType: 'json',
						success: function(r) {
						
								getContent();

						}
				});					
			});	
	}
	return false;
});     	

$(document).on("click",".prevp, .nextp",function(){
	
	showProcess();
	var p = $(this).data("page");
	$.post('/users/changePage',{p:p},function(r){
		
		//	var s = $("#s").val();
			getContent();
		
	},"json");
	
	return false;
});

$(document).on("click",".view",function(){
	showProcess();
	var id = $(this).data("id");
	
	//$.get("/ppl/getProfile",{id:id},function(r){
        window.location.href = "ppl/" + id;
        
		endProcess();
		
	//},"json");
	//
	//return false;
});


$(document).on("click","#btn-back-list",function(){
	
	$("#profile-view-box-content").html("");

	$("#btn-back-list").hide();
	$("#btn-del").hide();
	$("#profile-view-box").hide();


	$("#main-content-box").show();
	$("#btn-add-collab").show();
	$("#megafooter").show();
	
	return false;
});