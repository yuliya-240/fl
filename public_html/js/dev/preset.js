function showProcess(){
	$("#process-loader").addClass("in");
};


function endProcess(){
	$("#process-loader").removeClass("in");
	
}

$(document).ready(function(){
      	//initialize the javascript
    App.init();
      	


	$("#freset").validate({

		  errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
		submitHandler: function() { 	
			showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/login/doSetNewPass', 
				data:$('#freset').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						$("#new-box").hide();
						$("#done-box").show();
						endProcess();
					}else{
						$("#pass").val('');
						
						endProcess();
						swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
						
						
					}
				}
			
			});
			
		}
			
	});
      	
});