 var mf = $("#mf").val();
 var ws = $("#weekstart").val();
 
 
 var SUCCESS_STATUS = 30;
 
$('#edit-recurring').hide()
 
 function doFormValidation(){
	var validator = $( "#fdata" ).validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		

		
	});
	return validator.form();
}

$(document).ready(function(){
	 
    $('#external-events .external-event').each(function() {

      // store data so the calendar knows to render an event upon drop
      $(this).data('event', {
        title: $.trim($(this).text()), // use the element's text as the event title
        stick: true // maintain when user navigates (see docs on the renderEvent method)
      });

      // make the event draggable using jQuery UI
      $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
      });

    });

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    
    var y = $("btnYes").val();
	
	var n = $("btnNo").val();


		$(".pub").bootstrapSwitch({"size":"medium", "onText":y, "offText":n});

		$(".pub").on('switchChange.bootstrapSwitch', function(event, state) {
			
			

		
			var name = $(this).prop("name");
			
			if(state){
				var val = 1;
				
				$("#e-date").hide();
				$("#rec-off").hide();
				$("#rec-on").show();
				
			}else{
				var val = 0;
				$("#e-date").show();
				$("#rec-on").hide();
				$("#rec-off").show();
			}
			$('#new-event-content').css('height', document.getElementById('new-event-inner-content').offsetHeight+200)
			
				  
		});
		
	var wstart =  $("#weekstart").val();
		
	var tf = $("#tformat").val();
	
	if(tf=="H:i"){
			var meridian = false;
		}else{
			var meridian = true;
		}
		
	$(".date-picker").datepicker({
			autoclose: true,
			todayHighlight: true,
			weekStart: wstart,
			readOnly: true
		});
		
	$('.time-picker').timepicker({
		    template: false,
		    showSeconds: false,
		    showMeridian: meridian,    
		    showInputs: false,
		    minuteStep: 1
		});
		
    /* initialize the calendar
    -----------------------------------------------------------------*/

    $('#calendar').fullCalendar({
      header: {
        left: 'title',
        center: '',
        right: 'today, prev,next',
      },
	  eventSources: [ // your event source
		{
			url: '/calendar/getEvents',
			type: 'GET',
			
			error: function() {
				alert('There was an error while fetching events!');
			}
		} 
		],

		eventRender: function(event, element) {
			
			if(!event.close){

				 if(event.type=='4'){          
				    element.find(".fc-title").prepend("<i class='fa fa-handshake-o'></i> ");
				    element.css("background-color",$("#ev-meet-color").val());
				    element.css("border-color",$("#ev-meet-color").val());
				 }
				 if(event.type=='5'){          
				    element.find(".fc-title").prepend("<i class='fa fa-phone'></i> ");
				    element.css("background-color",$("#ev-phone-color").val());
				    element.css("border-color",$("#ev-phone-color").val());
				 }



				 if(event.type=='3'){          
				    element.find(".fc-title").prepend("<i class='fa fa-bug'></i> ");
				    element.css("background-color",$("#ev-issue-color").val());
				    element.css("border-color",$("#ev-issue-color").val());
				 }

				 if(event.type=='2'){          
				    element.find(".fc-title").prepend("<i class='fa fa-fire'></i> ");
				    element.css("background-color",$("#ev-mil-color").val());
				    element.css("border-color",$("#ev-mil-color").val());
				 }


				 if(event.type=='1'){          
				    element.find(".fc-title").prepend("<i class='fa fa-tasks'></i> ");
				    element.css("background-color",$("#ev-task-color").val());
				    element.css("border-color",$("#ev-task-color").val());
				 }

				
			}else{
				
				element.find(".fc-title").prepend("<i class='fa fa-check'></i>");
				element.css("background-color","#f2f2f2");
				element.css("border-color","#b4b4b4");
				element.css("color","green");
				 
			}
		},   
	  eventAfterRender: function(event, element, view){
		  
		  
	  },
	  eventClick: function(calendarEvent, jsEvent, view){
		  console.log(calendarEvent)
		  if(["3", "4", "5", "7", "9999"].indexOf(calendarEvent.type) === -1)
		  	return void 0;
		  $.ajax({
			  method: 'GET',
			  url: '/calendar/getEvent/',
			  data: {
				  id: calendarEvent.id
			  }
		  }).done(function(response){
			  
			  showEventModal((JSON.parse(response)).info, (JSON.parse(response)).issue)
		  })
	  },
	  eventDrop: function(event, delta, revertFunc){
		  var newDate = new Date(event.start)
		  var time = newDate.getHours()+' '+newDate.getMinutes()+' '+newDate.getSeconds()
		  console.log(event)
		  		  $.post('/calendar/dropEvent', 'e_id='+event.id+
		  										'&new_date='+encodeURI(event.start.format()))
		  	.done(function(response){
			  	$('#calendar').fullCalendar( 'refetchEvents' )
		  	})
		  	.fail(revertFunc)	  
	  },
	  views: {
	
        week:{
	        
	        titleFormat: mf
        },
        
        day:{
	        
	        titleFormat: mf
        }
	  },
	 // defaultView: window.mobilecheck() ? "basicDay" : "agendaWeek",
	  displayEventTime:false,
	  firstDay:ws,
      defaultDate: date,
      editable: true,
      //eventLimit: true,
      droppable: true, // this allows things to be dropped onto the calendar
    });
	 
 });
var showNewEventModal = function(){
	//DIRTY CSS HACKS
	$('html').css('overflow', 'hidden')
	$('div.am-wrapper.am-white-header.am-fixed-sidebar').css('overflow', 'hidden')
	$('#new-event-modal-back').css('height', window.outerHeight)
	$('#new-event-modal').css('height', window.screen.height)
	$('#new-event-modal').toggleClass('hidden')
	$('#new-event-content').css('height', document.getElementById('new-event-inner-content').offsetHeight+100+'px')
	$('#new-event-modal-back').toggleClass('hidden')
	$('new-event-inner-content').css('height', 'auto')
	
	$('#new-event-modal').animate({
		left: '10%'
	}, 300, "easeOutExpo", function(){
		$('#new-event-modal-close').toggleClass('hidden')
	})
 }
 
var hideNewEventModal = function(){
	$('div.am-wrapper.am-white-header.am-fixed-sidebar').css('overflow', 'auto')
	$(".pub").bootstrapSwitch('state', false)
	$('html').css('overflow', 'auto')
	$('#new-event-modal-close').toggleClass('hidden')
	$('#new-event-modal').animate({
			left: '100%'
		}, 300, "easeOutExpo", function() {
			$('#new-event-modal-back').toggleClass('hidden')
			$('#new-event-modal').toggleClass('hidden')
		}) 
}

var showEventModal = function(eventObject, issueObject){
	$('html').css('overflow', 'hidden')
	$('#show-event-modal-back').toggleClass('hidden')
	$('div.am-wrapper.am-white-header.am-fixed-sidebar').css('overflow', 'hidden')
	$('#show-event-modal').toggleClass('hidden')

	$('#show-event-modal').css('height', window.outerHeight)
	$('#show-event-modal-back').css('height', window.outerHeight)
	$('#show-event-modal').css('top', document.documentElement.scrollTop-100)
	$('#show-event-modal-close').css('top', document.documentElement.scrollTop-50)

	//$('#new-event-modal-content').css('height', 'auto')
	$('#show-event-inner-content').css('height', document.getElementById('show-event-modal').offsetHeight)
	$('#show-event-inner-content').css('height', 'auto')
	$('#show-event-modal-close').toggleClass('hidden')
	$('#show-event-modal').animate({
		left: '10%'
	}, {
		duration: 300,
		easing: "easeOutExpo",
		start: function(){
			if (eventObject.e_type == "4" || eventObject.e_type == "5" || eventObject.e_type == "7" || eventObject.e_type == "9999") {
				$('.show-event-table, .show-event-buttons').show()
				$('.show-issue-table, .show-issue-buttons').hide()
				calendarEvent(eventObject)
			} else {
				$('.show-event-table, .show-event-buttons').hide()
				if (eventObject.e_type == "3"){
					$(".show-issue-table, .show-issue-buttons").show()
					issueEvent(eventObject, issueObject)
				}
			}
		}
/*
		done: function(){	
			switch(eventObject.e_type){
				case ("4"): 
				case ("5"):
				case ("7"):
				case ("9999"):
					calendarEvent(eventObject)
				case ("2"):
					void 0;
				case ("3"): issueEvent(eventObject, issueObject)
			}		
*/	
	})
 }
 
var removeListeners = function(selectors){
	selectors.forEach(function(s){
		$(s).off()
	})
}

var issueEvent = function(event, issue){
	
	$('#show-issue-title').html(issue.issue_subj)
	$('#show-issue-cr_date').html(moment(issue.moment_created).format(issue.moment))
	$('#show-issue-dl_date').html(moment(issue.moment_deadline).format(issue.moment))
	$('#show-issue-desc').html(issue.issue_text)
	console.log(issue)
	$('#show-issue-details').click(showIssueDetails.bind(this, issue))
	$('#show-issue-resolve').click(showIssueResolve.bind(this, issue))
}

var showIssueDetails = function(issue){
	
	window.location.href="/issues/view/"+issue.issue_id;
	
}

var showIssueResolve = function(issue){
	$.post('/issues/changeStatus', 'id='+issue.issue_id+'&status='+SUCCESS_STATUS)
	hideEventModal()
}

var calendarEvent = function(eventObject){
			
	
			var notSet = $('#show-event-modal').attr('data-not-set')
			//FILL FIELDS WITH EVENT DATA
			for (var key in eventObject){
				if(key == 'e_date'){
					var _date = moment(eventObject.e_date).format('LLL')	
					$('#show-event-'+key).html(_date)
				}
				else { 
					document.getElementById('show-event-'+key) !== null ? 
					(!!eventObject[key]) && (eventObject[key] !== '0') ? 
						$('#show-event-'+key).removeClass("empty-p").html(eventObject[key]) : $('#show-event-'+key).addClass("empty-p").html(notSet)
					: void 0
				}
			}
				
			//FILL INPUT FIELDS WITH EVENT DATA
			//ON THE EDIT FORM
			$('#edit_deadline_date').datepicker("update", new Date(eventObject.e_date))
			
			$('#edit-deadline_time').timepicker("update", new Date(eventObject.e_date))
			
			$('#edit_title').val(eventObject.e_title)
			
			Array.prototype.forEach.call(document.getElementById('edit_type').childNodes, function(node, i, l){
				
				if (node.nodeName == 'OPTION') {
					if(node.value == eventObject.e_type) {
						node.setAttribute('selected', 'selected')
					} else {
						node.removeAttribute('selected' )
					}
				} else return node
				})
			
			$('#edit_desc').val(eventObject.e_desc)
			//IF EVENT COMPLITE HIDE 'complit' BUTTON 
			//AND MAKE RESULT DIELD READONLY
			if (eventObject.e_close === '1'){
				$('#show-event-complete-button, #show-event-complete-e_result, #show-event-edit-button, #show-event-edit-rec-button').hide()
				$('#show-event-complete-e_result_p').html(eventObject.e_result.trim().length ? eventObject.e_result : '<p class="empty-p">'+notSet+'</p>').show()
				$('#show-event-reopen').show()
				 
			} else {
				$('#show-event-complete-button, #show-event-complete-e_result, #show-event-edit-button, #show-event-edit-rec-button').show()
				$('#show-event-complete-e_result_p, #show-event-reopen').hide()
			}
			//ADD LISTENERS
			//LISTENERS SHOULD BE REMOVED WHEN MODAL HIDING
			//(removeListeners in hideEventModal func)
			$('#show-event-delete-button').click(deleteEvent.bind(this, eventObject))
			$('#show-event-complete-button').click(completeEvent.bind(this, eventObject, $('#show-event-complete-e_result')))
			$('#show-event-edit-button').click(editEvent.bind(eventObject))
			if (eventObject.e_close == 1) {
				$('#show-event-reopen').click(reopenEvent.bind(null, eventObject)) 
				} 
			$('#show-event-cancel-edit').click(cancelEditing)
			$('#show-event-save-edited-event-button').click(saveEditedEvent.bind(this,
				eventObject.e_id, 
				$('#edit_deadline_date'), 
				$('#edit-deadline_time'),
				$('#edit_type'),
				$('#edit_title'),
				$('#edit_location'),
				$('#edit_desc'),
				eventObject.e_recurring_id))
			$('#show-event-edit-rec-button').click(editRecurringEvent.bind(this, eventObject))
			$('#cancel-rec-event').click(function(e){
				e.preventDefault()	
				cancelEditing()
			})

}

var reopenEvent = function(eventObj, e){
	$.post('/calendar/reopenEvent', 'e_id='+eventObj.e_id, function(result){
		
		$('#calendar').fullCalendar( 'refetchEvents' )
		hideEventModal()

	})
}

var editRecurringEvent = function(event, e){
	$('.show-event-table, .show-event-buttons').hide('slide', {direction: 'right'}, 150, function(){
		
			$('#edit-recurring').show('slide', {direction: 'left'}, 150)
		})
}

var editEvent = function(eventObj, e){
	$('.show-event-table, .show-event-buttons').hide('slide', {direction: 'right'}, 150, function(){
		
			$('#show-event-edit-event, #show-event-save-edited-event').show('slide', {direction: 'left'}, 150)
		})
}

var cancelEditing = function(){
	
	$('#show-event-edit-event, #show-event-save-edited-event, #edit-recurring').hide('slide', {direction: 'right'}, 150, function(){
			$('.show-event-table, .show-event-buttons').show('slide', {direction: 'left'}, 150)
		})
}
 
var completeEvent = function(eventObj, resultElement, e){
	$.post('/calendar/completeEvent', 'e_id='+eventObj.e_id+'&e_result='+resultElement.val(), function(response){
			hideEventModal()
			e.preventDefault();
			$('#calendar').fullCalendar( 'refetchEvents' )
	})
}
var saveEditedEvent = function(id, dateEl, timeEl, typeEl, titleEl, locEl, descriptionEl, rec_id){
	var obj = {
		e_id: id,
		deadline_date: dateEl.val(),
		deadline_time: timeEl.val(),
		e_type: typeEl.val(),
		title: titleEl.val(),
		location: locEl.val(),
		e_desc: descriptionEl.val(),
		rec_id: rec_id
		}
	
	var serialized = Object.keys(obj).reduce(function(acc, curr, i){
		var keyvalue = curr+'='+encodeURI(obj[curr])
		return (i !== 0 ) ? acc=acc+'&'+keyvalue : acc=keyvalue 
	}, '')
	
	$.post('/calendar/editEvent', serialized, function(response){
		
		$('#calendar').fullCalendar( 'refetchEvents' )
		hideEventModal()
	})	
}
 
var deleteEvent = function(eventObj, e){
	
			swal({
				title: $('#show-event-modal').attr('data-delete-msg'),
				text: $('#show-event-modal').attr('data-delete-description'),
				type: "warning",
				showCancelButton: true,
				dangerMode: true,
				closeOnConfirm: false
			}, function(result){
				if (result){
					e.preventDefault()
					$.post('/calendar/deleteEvent/', 'e_id='+eventObj.e_id, function(data){ 
			    				hideEventModal()
			    				swal('Deleted!', '', 'success')
			    				
			    				$('#calendar').fullCalendar( 'refetchEvents' )
					})
				} else {
					
				}
			})
			
		}
 
var hideEventModal = function(){
	
	$('html').css('overflow', 'auto')
	$('div.am-wrapper.am-white-header.am-fixed-sidebar').css('overflow', 'auto')
	cancelEditing()
	removeListeners(['#show-event-save-edited-event-button', '#show-event-delete-button', '#show-event-complete-button', '#show-event-edit-button, #show-event-reopen, #show-issue-details, #show-issue-resolve'])
	$('#show-event-modal-close').toggleClass('hidden')
	$('#show-event-modal').animate({
			left: '100%'
		}, 300, "easeOutExpo", function() {
			$('#show-event-modal-back').toggleClass('hidden')
			$('#show-event-modal').toggleClass('hidden')
			
		}) 
}


$(document).on("click",".newEvent",function(){
	
	showNewEventModal()
})
$('#new-event-modal-close').click(function(){
	
	hideNewEventModal()	
})
$('#show-event-modal-close').click(hideEventModal)
  
$(document).on("click",".doSave",function(){
	
	var isvalid = doFormValidation();
	
	if(isvalid){
		showProcess();
		var pd = $("#fdata").serialize();
		$.post('/calendar/doAdd',pd,function(r){
			$("#cal-title").text($("#_cal-title").val());
			$("#new-event-box").hide();
			$(".newEvent").show();
			$("#calendar-box").show();
			$('#calendar').fullCalendar( 'refetchEvents' );
			hideNewEventModal();
			endProcess();
		},"json");
	}
	
	return false;
}); 

/*
$(document).on("click",".newEvent",function(){
	showProcess();
	$("#cal-title").text($("#_cal-n-e").val());
	$(".newEvent").hide();
	$("#calendar-box").hide();
	debugger;
//	$("#new-event-box").show();
	
	$.get("/calendar/add",function(r){
		
		$("#new-event-box").html(r.html).show();
		$(".select2").select2({
	      width: '100%'
		});
	
	    
		var wstart =  $("#weekstart").val();
		
		var tf = $("#tformat").val();
		
		if(tf=="H:i"){
			
			var meridian = false;
		}else{
			var meridian = true;
		}
		
		$(".date-picker").datepicker({
			autoclose: true,
			todayHighlight: true,
			weekStart: wstart,
			readOnly: true
		});
		
		$('.time-picker').timepicker({
		    template: false,
		    showSeconds: false,
		    showMeridian: meridian,    
		    showInputs: false,
		    minuteStep: 1
		});

	
		
		
	    
	    endProcess();
		
	},"json")
	
	
	return false;
});
*/

$(document).on("click",".cancelAddEvent",function(){
/*
	$("#cal-title").text($("#_cal-title").val());
	$("#new-event-box").hide();
	$(".newEvent").show();
	$("#calendar-box").show();
*/
	hideNewEventModal()
	
	
	return false;
})

$(document).on("change","#recOptions",function(){
		var recoption = $(this).val();
		
		$(".optBox").hide(); 
		
		if(recoption == 'd')$("#dayOptionBox").show();	
		
		if(recoption == 'w')$("#weekOptionBox").show();
		
		if(recoption == 'm')$("#monthOptionBox").show();
		
		if(recoption == 'y')$("#yearOptionBox").show();
		

		
		return false;
});

$(document).on("change","#ir",function(){
	
	var v = $(this).val();
	
	$("#rsd").hide();
	$("#rsc").hide();
	
	if(v==2){
		
		$("#rsd").show();
		
	}
	
	if(v==3){
		
		$("#rsc").show();
		
	}

	
})


