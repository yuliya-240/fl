function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function doFormValidation(){
	var validator = $( "#fsend" ).validate();
	return validator.form();
}

 $(document).ready(function(){

	$(".sendInvite").click(function(){
		
		var email = $("#s").val();
		if(validateEmail(email))$("#email").val(email);
		$('#invite-box').modal('show');
		return false;
	});

	$("#fsend").validate({
		  rules: {
		    
		    email: {
		      required: true,
		      email: true
		    },
		  },
		  errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
		submitHandler: function() { 	
			showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/users/doSendInvite', 
				data:$('#fsend').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						
						$('#invite-box').modal('hide');
						endProcess();
						swal({   title: r.title,   text: r.msg,   type: "success" });
					}else{
					  $("#email").val("");
					  $("#error-box").html("");
						endProcess();
						
						if(r.err=="exist"){
							//$("#invite_username").html(r.user_username);
							//$("#invite_email").html(r.user_email);
							$("#ex-u-b").html(r.html);
							
							$("#user-own-email").hide();
							$("#user-exist-box").show();
						}

						if(r.err=="own"){
							$("#user-exist-box").hide();
							$("#user-own-email").show();
						}

						if(r.err="friend"){
						 swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
							
						}

						
						if(r.err="sent"){
						 swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
							
						}
						
						if(r.err="not_send"){
						 swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
							
						}
						
						
					}
				}
			
			});
			
		}
			
	});

	$('#invite-box').on('hidden.bs.modal', function (e) {
		$("#email").val("");
		$("#error-box").html("");
			  
		$("#user-own-email").hide();
		$("#user-exist-box").hide();		  
	});
	 
 });
 
 
 $(document).on('keyup','#s',function(){
	 var s = $(this).val();
	
		$.get('/users/filterz',{s:s},function(r){
			$("#ulist-res-box").show();
			if(r.status){
				$("#unet-box").hide();
				$("#ulist-box").show();
				
				var tbl = '<table class="table table-hover" >';
				
				for(var i=0; i<r.id.length; i++){
					var hidebrdr="";
					if(!i) hidebrdr = 'style="border-top:1px solid #fff;"';  
					
					tbl=tbl+'<tr style="cursor:pointer;" class="add-user" data-id="'+r.id[i]+'">'+
					'<td '+hidebrdr+' >'+r.username[i]+'</td>'+
					'<td '+hidebrdr+' >'+r.name[i]+'</td>'+
					'<td '+hidebrdr+'>'+r.email[i]+'</td>'+
					'<td width="1%" '+hidebrdr+'><i class="material-icons text-success">add_circle_outline</i></td>'+
					'</tr>';
					
				}
				
				tbl=tbl+'</table>';
				
				$("#ulist-box").html(tbl);
				
			}else{
				$("#ulist-box").html("").hide();
				$("#unet-box").show();
			}
			
		},"json");
	 
 });
 
 $(document).on("click",".add-user",function(){
	 var title = $("#_cet").val();
	 var msg = $("#_uau").val();
	 var yb = $("#_yb").val();
	 var cb = $("#_cb").val();
	 var id= $(this).data("id");
	 
	 swal({
		 title:title,
		 text:msg,
		 type: "warning",
		 showCancelButton: true,
		 confirmButtonColor: "#7accbe",
		 confirmButtonText: yb,
		 cancelButtonText: cb,
	 },function(result){
	 	if(result){
			 showProcess();
			 $.post('/users/doInvite',{id:id},function(r){
				 if(r.status){
					 window.location.href="/users/invites";
				 }else{
					 endProcess();
					 swal(r.title,r.msg,"error");
				 }
				 
			 },"json");
		 	
	 	}
	 })
	 
	 return false;
 });
 
 
