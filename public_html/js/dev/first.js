function showProcess(){
	
	$("#process-loader").addClass("in");
	
};

function endProcess(){
	$("#process-loader").removeClass("in");
	
}

$(document).ready(function(){
	

	/*$("input#username").on({
	  keydown: function(e) {
	    
	   	if (e.which === 32)return false;
 
	  },
	  change: function() {
	    this.value = this.value.replace(/\s/g, "");
	  }
	});*/
	
	var offset = new Date().getTimezoneOffset();
	var offsetHr = (offset/60)*(-1);
	
	$.get("/first/getTZ",{offset:offsetHr},function(r){
		if(r.status){
			$("#tz").val(r.tz.tz_olson).change();
		}
		
	},"json");
	
	$("#fdata").validate({
	  	rules: {
		    user_username: {
			  minlength: 6,
		      required: true,
		      remote: {
		        url: "/first/isusrnm",
		        type: "post",
		
		        dataType: "json",
		      }
		    }
	  	},

		/*showErrors: function(errorMap, errorList) {
			this.defaultShowErrors();
			//window.location.href="#top";
			
		},*/
		submitHandler: function() {
			var pd = $('#fdata').serialize();
			showProcess();
			$.post("/first/setupacc",pd,function(r){
				
				if(!r.status){
					
					endProcess();
					swal({
						title:"Operation Failed",
						text: r.errmsg,
						type:"error"
					});
					
				}else{
					
					window.location.href='/';
				}
				
			},"json");
			
			
		}
	});
	
});

$(document).on("change","#dformat, #tformat, #tz",function(){
	     
	var df=$('#dformat').val();
	var tf=$('#tformat').val();
	var tz=$('#tz').val();
	
	var strf ="df="+df+"&tf="+tf+"&tz="+tz;
	$.post('/misc/firstChangeDateTimeFormat',strf,function(data){ 
    	$('#dateExample').html(data.html);
				
 	},"json");    
 	return false;
});

$("input#username").on("keypress", function(event) {

    // Disallow anything not matching the regex pattern (A to Z uppercase, a to z lowercase and white space)
    // For more on JavaScript Regular Expressions, look here: https://developer.mozilla.org/en-US/docs/JavaScript/Guide/Regular_Expressions
    var englishAlphabetAndWhiteSpace = /[A-Za-z0-9]/g;
   
    // Retrieving the key from the char code passed in event.which
    // For more info on even.which, look here: http://stackoverflow.com/q/3050984/114029
    var key = String.fromCharCode(event.which);
    
    //alert(event.keyCode);
    
    // For the keyCodes, look here: http://stackoverflow.com/a/3781360/114029
    // keyCode == 8  is backspace
    // keyCode == 37 is left arrow
    // keyCode == 39 is right arrow
    // englishAlphabetAndWhiteSpace.test(key) does the matching, that is, test the key just typed against the regex pattern
    if (event.keyCode == 8  || englishAlphabetAndWhiteSpace.test(key)) {
        return true;
    }

    // If we got this far, just return false because a disallowed key was typed.
    return false;
});


$('input#username').on("paste",function(e)
{
    e.preventDefault();
});



