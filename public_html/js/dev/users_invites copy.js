function getContent(p){
	
	$.get('/users/getContentInvites',{p:p},function(r){
		
		$("#d-content").html(r.html);
		$("#megafooter").html(r.footer);
		endProcess();
					var c = parseInt(r.count);
		if(c){
			
			$("#fl-footer").addClass('in');
		}else{
			
			$("#fl-footer").removeClass('in');
		}
	},"json");
	
}

$(document).ready(function() {
	
	getContent(1);
});

$(document).on("click",".del",function(){
	
	 var id= $(this).data("id");
	 var title = $("#_udt").val();
	 var msg = $("#_udm").val();
	 var btndel = $("#_bd").val();
	 var cb = $("#_bc").val();
	 
	 swal({
		 title:title,
		 text:msg,
		 type: "warning",
		 showCancelButton: true,
		 confirmButtonColor: "#DD6B55",
		 confirmButtonText: btndel,
		 cancelButtonText: cb,
	 },function(result){
	 	if(result){
			 showProcess();
			 $.post('/users/delete',{id:id},function(r){
				 window.location.href="/users/invites";
			 },"json");
		 	
	 	}
	 })
	
	
	return false;
});

$(document).on("click",".accept",function(){
	
	var id=$(this).data("id");
	showProcess();
	$.post("/users/doAccept",{id:id},function(r){
		
		window.location.href="/users";
		
		
	},"json");
	
	return false;
});
 
$(document).on("click","#bundleDelete",function(){

	var curl = $("#curl").val();
	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_dt").val();
	var msg = $("#_dm").val();
	var yes = $("#_yes").val();

	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true,
			showCancelButton: true,},
			function(){
					
					showProcess();	
					$.ajax({
							type:'POST', 
							url: '/users/deleteBundle', 
							data:$("#flist").serialize(), 
							dataType: 'json',
							success: function(r) {
							
									getContent(1);
	
							}
					});					
			});	
	}
	return false;
});     	


$(document).on("click","#bundleAccept",function(){

	var curl = $("#curl").val();
	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;
	
	var title = $("#_at").val();
	var msg = $("#_am").val();
	var yes = $("#_yes").val();
	
	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#7accbe",
			closeOnConfirm: true,
			showCancelButton: true,},
			function(){
					
					showProcess();	
					$.ajax({
							type:'POST', 
							url: '/users/acceptBundle', 
							data:$("#flist").serialize(), 
							dataType: 'json',
							success: function(r) {
							
									getContent(1);
	
							}
					});					
			});	
	}
	return false;
});     	
