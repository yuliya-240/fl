var $container = $('.gallery-container');
function aplyMassonry() {

    $container.masonry({
        columnWidth: 0,
        itemSelector: '.item'
    });
}

$(document).ready(function(){
    $container.imagesLoaded(function(){
        aplyMassonry();
    });
});