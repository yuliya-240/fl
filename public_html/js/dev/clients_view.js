function doFormValidationOnFly(formstr){
	var validator = $(formstr).validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		

		
	});
	return validator.form();
}

function getDetailsList(){
	
	var id = $("#client_id").val();
	
	$.get("/clients/getDetails",{id:id},function(r){
		
		$("#details-edit").html("").hide();
		$("#details-view").html(r.html).show();
		$("#btn-edit-details").show();
		
	},"json");
	
}

function getAddrView(){
	
	var id = $("#client_id").val();
	
	$.get("/clients/getAddr",{id:id},function(r){
		
		$("#addr-edit").html("").hide();
		$("#addr-view").html(r.html).show();
		$("#btn-edit-addr").show();
		
	},"json");
	
}


function getContactView(){
	
	var id = $("#client_id").val();
	
	$.get("/clients/getContact",{id:id},function(r){
		
		$("#contact-edit").html("").hide();
		$("#contact-view").html(r.html).show();
		$("#btn-edit-contact").show();
		
	},"json");
	
}



$(document).ready(function(){
	$("#dis").mask('0999999999.09');
	
	getDetailsList();
	getAddrView();
	getContactView();
	
});


// Details

$(document).on("click","#btn-edit-details",function(){
	
	showProcess();
	var id = $("#client_id").val();

	$.get("/clients/getEditDetails",{id:id},function(r){
		
		$("#btn-edit-details").hide();
		$("#details-view").hide();
		$("#details-edit").html(r.html).show();
		$("#dis").mask('0999999999.09');

		endProcess();
		
	},"json");
	
	
	return false;
});

$(document).on("click","#btn-cancel-details",function(){
	
	
	getDetailsList();
	return false;
});

$(document).on("click","#btn-save-details",function(){
	
	var isValid = doFormValidationOnFly("#fdata");
	
	if(isValid){

		showProcess();
		var pd = $("#fdata").serialize();
		
	$.post("/clients/doUpdateDetails",pd,function(r){

		getDetailsList();
		endProcess();
		
	},"json");
		
		
	}
	return false;
});

// Address

$(document).on("click","#btn-edit-addr",function(){
	
	showProcess();
	var id = $("#client_id").val();

	$.get("/clients/getEditAddr",{id:id},function(r){
		
		$("#btn-edit-addr").hide();
		$("#addr-view").hide();
		$("#addr-edit").html(r.html).show();
		endProcess();
		
	},"json");
	
	
	return false;
});

$(document).on("change","#country",function(){
	var strf ="countryID="+$(this).val();
	$.post('/misc/changeCountry',strf,function(data){ 
	 
	  if(!data.count){
	  	$('#region').hide();
	  	$('#region').attr('name','');
	  	$('#inputstate').attr('name','client_state');
	  	$('#inputstate').show();
	  	
	  }else{
	  	$('#inputstate').hide();
	  	$('#inputstate').attr('name','');
	  	$('#region').show();
	  	$('#region').attr('name','client_state');
	  	
	  	$('#region').html(data.list);
	
	  }
				
		},"json");    
		return false;
        	
});


$(document).on("click","#btn-cancel-addr",function(){
	
	getAddrView();
	return false;
});

$(document).on("click","#btn-save-addr",function(){
	
	var isValid = doFormValidationOnFly("#fdata");
	
	if(isValid){

		showProcess();
		var pd = $("#fdata").serialize();
		
	$.post("/clients/doUpdateAddr",pd,function(r){

		getAddrView();
		endProcess();
		
	},"json");
		
		
	}
	return false;
});

//Contacts

$(document).on("click","#btn-edit-contact",function(){
	
	showProcess();
	var id = $("#client_id").val();

	$.get("/clients/getEditContact",{id:id},function(r){
		
		$("#btn-edit-contact").hide();
		$("#contact-view").hide();
		$("#contact-edit").html(r.html).show();
	

		endProcess();
		
	},"json");
	
	
	return false;
});

$(document).on("click","#btn-cancel-contact",function(){
	
	getContactView();
	return false;
});

$(document).on("click","#btn-save-contact",function(){
	
	var isValid = doFormValidationOnFly("#fdata");
	
	if(isValid){

		showProcess();
		var pd = $("#fdata").serialize();
		
	$.post("/clients/doUpdateContact",pd,function(r){

		getContactView();
		endProcess();
		
	},"json");
		
		
	}
	return false;
});


$(document).on("click",".del",function(){

	var id = $("#client_id").val();

	var title = $("#_pdt").val();
	var msg = $("#_pdm").val();
	var yes = $("#_yes").val();
	
	swal({
	
	text: msg,
	title:title,
	type: "warning",
	confirmButtonText: yes,
	confirmButtonColor: "#DD6B55",
	closeOnConfirm: true,
	showCancelButton: true,
	},
	function(){
			
		showProcess();	
		$.post('/clients/delete',{id:id},function(r){
			
			window.location.href="/clients";
			
		},"json")
		
	});	
	
	return false;
})