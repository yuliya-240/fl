function doFormValidation() {
    var validator = $("#fdata").validate({
        errorPlacement: function (label, elem) {
            var errbox = elem.data("errorbox");
            $(errbox).html(label);
        },


    });
    return validator.form();
}


$(document).ready(function () {

    $('#notes').redactor({

        buttonSource: true,
        imageUpload: '/img/redactorUp',
        minHeight: 200

    });


});

$(document).on("click", "#doSave", function () {

    var isvalid = doFormValidation();

    if (isvalid) {
        showProcess();
        var pd = $("#fdata").serialize();
        $.post('/notes/doAddNote', pd, function (r) {

            window.location.href = "/notes";

        }, "json");
    }

    return false;
});
