var wstart =  $("#weekstart").val();

function doFormValidationOnFly(formstr){
	var validator = $(formstr).validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		

		
	});
	return validator.form();
}
 
function doMilestoneFormValidation(){
	
	var validator = $( "#fmiles").validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
	});
	return validator.form();
}

function issuechart(){
	    
	    var prj = $("#prj_id").val();
	    
	    $.get("/projects/getIssuesChart",{prj:prj},function(r){
		    
		    var data = r.issues;
		    var legendContainer = $("#issue-chart").parent().next().find(".legend");

			$.plot('#issue-chart', data, {
			    series: {
		        pie: {
		          innerRadius: 0.4,
		          show: true,
		          highlight: {
								opacity: 0.1
							}
			      }
			    },
			    grid:{
			    	hoverable: true
			    },
			    legend:{
			    	container: legendContainer
			    },
			    colors: r.colors
			});
    

		    
	    },"json");
	    
    }

function getViewDetails(){
	
	var id = $("#prj_id").val();
	
	$.get("/projects/getDetailsView",{id:id},function(r){
		
		$("#details-edit").html("").hide();
		$("#details-view").html(r.html).show();
		$("#btn-edit-details").show();
		
	},"json");
	
}

function getViewCollabList(){
	
	var id = $("#prj_id").val();
	
	$.get("/projects/getCollabListView",{id:id},function(r){
		
		$("#collab-edit").html("").hide();
		$("#collab-view").html(r.html).show();
		$("#btn-collab-edit").show();
		
	},"json");
	
}
    
$(document).ready(function(){
	
	issuechart();
	getViewDetails();
	getViewCollabList();
});

$(document).on("click","#miles-edit-btn",function(){
	showProcess();
	$(this).tooltip( 'hide' );
	$(this).hide();
	$(".view-box").hide();
	var id=$("#prj_id").val();
	$.get("/projects/getMilestones",{id:id},function(r){
		
		$("#miles-edit-box").html(r.html).show();
		$(".date-picker").datepicker({
			autoclose: true,
			todayHighlight: true,
										/*format : "M dd, yyyy",*/
			weekStart: wstart,
			readOnly: true
		});
	
		
	    endProcess();
	    window.location.href="#m-block";	
		
		},"json")
	
	
	return false;
});

$(document).on("click","#addMilestone",function(){
    $("#addMilestone").prop('disabled',true);
    showProcess();
    
    $.get('/projects/getMilestoneBox',{src:1},function(r){
	   
	    $("#milestones-box").append(r.html);
		$("#addMilestone").prop('disabled',false);
		//var wstart =  $("#weekstart").val();
		
		$(".date-picker").datepicker({
			autoclose: true,
			todayHighlight: true,
			weekStart: wstart,
			readOnly: true
		});
		
		endProcess();
    },"json");
    
    return false;
});

$(document).on("click",".del-mil",function(){
	
	var id = $(this).data("id");
	$("#"+id).remove();
	
	return false;
});

$(document).on("click","#doSaveMilestone",function(){
	
	var isValid = doMilestoneFormValidation();
	
	if(isValid){
		
		showProcess();
		var pd = $("#fmiles").serialize();
		var id = $("#prj_id").val();
		var ps = pd+"&id="+id;
		$.post("/projects/doUpdateMilestones",ps,function(r){
			
			$("#miles-edit-box").html("").hide();
			$("#miles-view-box").html(r.html).show();
			$("#miles-edit-btn").show();
			
			endProcess();
			notify(r.msg,'inverse');
		},"json")		
		
	}
	
	
	return false;
});

$(document).on("click","#cancelEditMilestone",function(){
	
	$("#miles-edit-box").html("").hide();
	$("#miles-edit-btn").show();
	$("#miles-view-box").show();
	
	return false;
});

$(document).on("click",".delprj",function(){
	var id= $(this).data("id");
	var title = $("#_pdt").val();
	var msg = $("#_pdm").val();
	var yes = $("#_yes").val();


	swal({
	
	text: msg,
	title:title,
	type: "warning",
	confirmButtonText: yes,
	confirmButtonColor: "#DD6B55",
	closeOnConfirm: true,
	showCancelButton: true,},
	function(){
			
		showProcess();	
		$.post("/projects/delete",{id:id},function(r){
			
			window.location.href="/projects";
			
		},"json");

	});	
	
	return false;
});


// Details

$(document).on("click","#btn-edit-details",function(){
	
	$("#btn-edit-details").hide();
	var id = $("#prj_id").val();
	
	$.get("/projects/getDetailsEdit",{id:id},function(r){
		
		$("#details-view").hide();
		$("#details-edit").html(r.html).show();
		$(".select2").select2({
	      width: '100%'
	    });
		
		
	},"json");
	
	return false;
});

$(document).on("click","#btn-cancel-details",function(){
	showProcess();
	getViewDetails();
	endProcess();
	return false;
});

$(document).on("click","#btn-save-details",function(){
	
	var isValid = doFormValidationOnFly("#fdata");
	
	if(isValid){

		showProcess();
		var pd = $("#fdata").serialize();
		
	$.post("/projects/doUpdateDetails",pd,function(r){

		getViewDetails();
		endProcess();
		
	},"json");
		
		
	}
	return false;
});


$(document).on("click","#btn-collab-edit",function(){
	
	$("#tn-collab-edit").hide();
	var id = $("#prj_id").val();
	
	$.get("/projects/getCollabListEdit",{id:id},function(r){
		
		$("#collab-view").hide();
		$("#collab-edit").html(r.html).show();
		
	},"json");
	
	
	return false;
});

$(document).on("click",".btn-add-line",function(){
	$(this).hide();
	$(".del-collab-line").hide();
	var pd = $("#fdata").serialize();
	
	$.get("/projects/addNewCollabLine",pd,function(r){
		
		
		if(r.status){
			$("#collab-list").append(r.html);
			$('#collab-list-item'+r.id).css('backgroundColor', '#dedede');
			$("#collab-list-item"+r.id).animate({
			  backgroundColor: "transparent"
			}, 'slow');

		}else{
			
			$(".del-collab-line").show();
		}

	},"json")
	
	return false;
});

$(document).on("change",".collab-item-sel",function(){
	
	var id = $(this).val();
	
	
	var did = $(this).data("id");
	var txt = $("#prju_collaborator_id"+did+" option:selected").text();	
	console.log(did);
	$(this).hide();
	$("#collab-name-text"+did).text(txt).show();
	//$("#prju_collaborator_id"+did).val(id)
	$(".btn-add-line").show();
	$(".del-collab-line").show();
	return false;
});

$(document).on("click",".del-collab-line",function(){
		var did = $(this).data("id");

		$("#collab-list-item"+did).remove();
		$(".btn-add-line").show();
	
	return false;
});

$(document).on("click","#btn-cancel-collab",function(){
	
	getViewCollabList();
	return false;
});

$(document).on("click","#btn-save-collab",function(){

	showProcess();
	var pd = $("#fdata").serialize();
		
	$.post("/projects/doUpdateCollab",pd,function(r){

		getViewCollabList();
		endProcess();
		
	},"json");
		
		

	return false;
});





