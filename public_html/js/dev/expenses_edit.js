var uploader;
uploader = new plupload.Uploader({
    runtimes : 'html5,flash,html4',
    browse_button : 'pickfiles2', // you can pass an id...
    container: document.getElementById('counteinerpic'), // ... or DOM Element itself
    url : '/img/uploadexppic',
    unique_names:true,
    multi_selection:false,
    max_file_count: 1,
    filters : {
        max_file_size : '2mb',
        mime_types: [
            {title : "Image files", extensions : "jpg,gif,png,jpeg"},

        ]
    },

    init: {
        PostInit: function() {},

        FilesAdded: function(up, files) {
            $("#progress2").show();
            $("#progressSlide2").css('width',"0%");
            up.refresh(); // Reposition Flash/Silverlight
            uploader.start();

            //showProcess();
        },

        UploadProgress: function(up, file) {
            $("#progressSlide2").css('width',file.percent+"%");
            $("#progress2").data('percent',file.percent+"%");

        },

        Error: function(up, err) {

            swal({
                title:err.message,
                text:err.code,
                type:"error"
            })

            up.refresh(); // Reposition Flash/Silverlight

            $("#progress2").hide();



        },

        FileUploaded: function(up, file, info) {

            var obj = JSON.parse(info.response);
            //var $htm = $(obj.html);
            $("#receipt").prop('src',obj.imghost+obj.cid+'/pic/'+obj.cleanFileName);
            $("#pic").val(obj.cleanFileName);


        },
        UploadComplete: function(up,files){
            $("#progress2").hide();


        }
    }
});

function calculateRate(){

    var total = parseFloat($("#amount").val());
    if(!total) total =0;
    total = total.toFixed(2);

    var rate = parseFloat($("#exchange_rate").val());
    var total_base = total*rate;

    $("#base").val(total_base.toFixed(2));

}

function doExpenseFormValidation(){

    var validator = $( "#fexpense").validate({
        errorPlacement: function(label, elem) {
            var errbox = elem.data("errorbox");
            $(errbox).html(label);
        },
    });
    return validator.form();
}

$(document).ready(function(){

    $(".select2").select2({
        width: '100%',
        templateResult: function (result) {

            if(!result.loading){

                var s = result.id.split('@');
                var parent = parseInt(s[1]);
                if(parent!=0){

                    str = $('<span><img src="/img/subcat.png"> '+result.text+'</span>');
                }else{

                    return str = result.text;
                }
            }else{

                return str = result.text;
            }

            return str;
        },
    });

    $('.venobox').venobox();

    var wstart =  $("#weekstart").val();

    $(".date-picker").datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart: wstart,
        readOnly: true
    });

    $(".dec").livequery(function(){


        $(this).mask('0999999999.09', {
            maxlength: false,
            onChange: function(cep){

                calculateRate();
            },
            onEmpty : function(){
                calculateRate();
            }
        });
    });

    $(".tax").livequery(function(){


        $(this).mask('0999999999.09', {
            maxlength: false,

        });
    });

    $("#currency").trigger("change");

    uploader.init();
});

$(document).on("change","#currency",function(){
    var base = $("#basecurrency").val();
    var c = $(this).val();
    var rate = $("#rate"+c).val();
    $("#currency-label").text(c+"/"+base)
    $("#selcurr").text(c);
    $("#exchange_rate").val(rate);
    calculateRate();


    if(base==c){

        $("#rate-box").hide();

    }else{

        $("#rate-box").show();

    }
});



$("#vendor").livequery(function(){


    var $input = $(this);
    $input.typeahead({
        source:function ( query,process) {

            return $.post('/vendors/vfilter', { query: query }, function (data) {
                return process(data);
            },"json");
        },
        autoSelect: true,
        fitToElement: true,
        items:21,

    });

    $input.change(function() {
        var obj = $input.typeahead("getActive");
        if (obj) {

            $("#vendor").val(obj.name);
            $("#vendor_id").val(obj.id);

        }
    });


});


$(document).on("click","#doSave",function(){

    var isValid = doExpenseFormValidation();

    if(isValid){
        showProcess();

        var pd = $("#fexpense").serialize();

        $.post("/expenses/doUpdate",pd,function(r){

            window.location.href="/expenses";

        },"json")

    }


    return false;
});

$(document).on('click', '#doSaveCat', function(){
    var pd = $('#fdata').serialize();
    $.post("/expenses/doAddCat",pd, function(r){
        //$("#cat").html(r.html);
        //$('#cat').val(r.id);
        //$("#cat").trigger("change");
         window.location.href = "/expenses/edit/"+ r.idedit;
        $('#new-cat-box').modal('hide');
    },"json");
});

$(document).on("click","#addLine",function(){
	
	$(this).prop("disabled",true);
	
	$.get("/expenses/getNewLine",function(r){
		
		$(r.html).appendTo("#exp-lines");
		$("#addLine").prop("disabled",false);
		
	},"json");
	
	return false;
});

$(document).on("click",".delline",function(){
	
	var id = $(this).data("id");
	
	$("#line"+id).remove();
	return false;
});

