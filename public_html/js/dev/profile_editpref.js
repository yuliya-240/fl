$(document).on("change","#dformat, #tformat, #tz",function(){
	     
	var df=$('#dformat').val();
	var tf=$('#tformat').val();
	var tz=$('#tz').val();
	
	var strf ="df="+df+"&tf="+tf+"&tz="+tz;
	$.post('/misc/firstChangeDateTimeFormat',strf,function(data){ 
    	$('#dateExample').html(data.html);
				
 	},"json");    
 	return false;
});

$(document).ready(function(){
	
	var offset = new Date().getTimezoneOffset();
	var offsetHr = (offset/60)*(-1);
	
	$.get("/first/getTZ",{offset:offsetHr},function(r){
		if(r.status){
			$("#tz").val(r.tz.tz_olson).change();
		}
		
	},"json");

	$("#fdata").validate({


		submitHandler: function() {
			var pd = $('#fdata').serialize();
			showProcess();
			$.post("/profile/updateProfile",pd,function(r){
				
				if(!r.status){
					
					endProcess();
					swal({
						title:"Operation Failed",
						text: r.errmsg,
						type:"error"
					});
					
				}else{
					
					window.location.href='/profile';
				}
				
			},"json");
			
			
		}
	});
	
	
});



