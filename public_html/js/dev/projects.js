function getContent(p){
	
	$.get('/projects/getContent',{p:p},function(r){
		
		$("#d-content").html(r.html);
		
		endProcess();
		var c = parseInt(r.count);
		if(c>0){
			
			$("#megafooter").html(r.footer);
		}
		
	},"json");
}

$(document).ready(function() {
	showProcess();
	getContent(1);
});

$(document).on("click",".tbl-pointer",function(){
	showProcess();
	var id = $(this).data("id");
	
	window.location.href="/projects/view/"+id;
	
	return false;
});

 $(document).on("click","#bundleDelete",function(){


	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_pdt").val();
	var msg = $("#_pdm").val();
	var yes = $("#_yes").val();

	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true,
			showCancelButton: true,
            },
			function(){
					
				showProcess();	
				$.ajax({
						type:'POST', 
						url: '/projects/deleteBundle', 
						data:$("#flist").serialize(), 
						dataType: 'json',
						success: function(r) {
						
								getContent(1);

						}
				});					
			});	
	}
	return false;
});     	
