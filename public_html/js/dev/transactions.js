var mf = $("#moment-format").val();


function getContent(searchstr){

		$.get('/transactions/getContent',{query:searchstr},function(r){
			
			$("#d-content").html(r.html);
				
			var c = parseInt(r.count);
			if(c>0){
				
				$("#megafooter").html(r.footer);
			}
			
			endProcess();
		},"json");
	
	
}
/*
function getContent(p){

    $.get('/transactions/getContent',function(r){

        $("#d-content").html(r.html);

        var c = parseInt(r.count);
        if(c>0){

            $("#megafooter").html(r.footer);
        }

        endProcess();
    },"json");


}*/

$(document).ready(function() {

    var ss = $('#s').val();
    getContent(ss);

    $('#s').keyup(function(e){

        if ( e.which == 13 ) {
            e.preventDefault();
        }

        var searchstr = $(this).val();

        if(searchstr.length){

            $("#delsearch").show();
        }else{
            $("#delsearch").hide();
        }


        getContent(searchstr);
    });


    $('#tdrp').daterangepicker({
        locale: {
            cancelLabel: 'Clear',
            format: mf
        },
        opens: "left"
    });

    var startdate = $("#startDate").val();
    var enddate =  $("#endDate").val();

    if(startdate!="none"){

        $('#tdrp').data('daterangepicker').setStartDate(moment(startdate).format(mf));
        $('#tdrp').data('daterangepicker').setEndDate(moment(enddate).format(mf));
        var str = moment(startdate).format(mf)+' - '+moment(enddate).format(mf);
        $("#tdrp").html(str);
    }

    $('#tdrp').on('apply.daterangepicker', function(ev, picker) {
        showProcess();
        var dstart = picker.startDate.format('YYYY-MM-DD');
        var dend = picker.endDate.format('YYYY-MM-DD');
        $("#startDate").val(dstart);
        $("#endDate").val(dend);
        var str = ''+picker.startDate.format(mf)+' - '+picker.endDate.format(mf);
        $("#tdrp").html(str);
        $.post("/transactions/changeDateRange",{start:dstart,end:dend},function(r){

            getContent(r.s)

        },"json");
    });

    $('#tdrp').on('cancel.daterangepicker', function(ev, picker) {
        var dstart = "none";
        var dend = "none";
        $("#startDate").val(dstart);
        $("#endDate").val(dend);

        $("#tdrp").html('<span class="s7-date" style="font-size: 24px;"></span>');
        $.post("/transactions/changeDateRange",{start:dstart,end:dend},function(r){

            getContent(r.s)

        },"json");
    });


});



$(document).on("click",".tma",function(){
    $(this).addClass("active");
    $(".tm").removeClass("active");
    $(".tmethod-val").remove();
    $("#foptions").append('<input type="hidden" class="tmethod-val" id="tma" name="tmethod[]" value="0"> ');
    return false;
});


$(document).on("click",".tm",function(){
    $(".tma").removeClass("active");
    var el = $(this);
    var id = el.data("val");
    if(el.hasClass("active")){

        $("input#tm"+id).remove();
        el.removeClass("active");
        var c =  $(".tm.active");
        if(!c.length){
            $(".tma").addClass("active");
            $(".tmethod-val").remove();
            $("#foptions").append('<input class="tmethod-val" type="hidden" id="tma" name="tmethod[]" value="0"> ');
        }

    }else{
        $("#tma").remove();

        $("#foptions").append('<input type="hidden" class="tmethod-val" id="tm'+id+'" name="tmethod[]" value="'+id+'"> ');

        el.addClass("active");
    }
});

$(document).on("click",".type",function(){

    var v = $(this).data("val");

    $("#type").val(v);

    $(".type").removeClass("active");
    $(this).addClass("active");

    return false;
});

$(document).on("click",".doApplyFilter",function(){
    showProcess();
    var pd = $("#foptions").serialize();

    $.post("/transactions/doApplyFilter",pd,function(r){

        getContent(r.s);

    },"json")

    return false;
});

$(document).on('click', '.view', function(){
    showProcess();

    var id = $(this).data("id");

    $.get('/transactions/getViewBox', {id: id}, function (r) {
        $("#view-details-body").html(r.html);
        $('#view-details').modal('show');
       endProcess();
    }, "json");

    return false;

});




