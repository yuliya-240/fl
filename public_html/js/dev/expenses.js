var mf = $("#moment-format").val();

function getContent(searchstr){

	$.get('/expenses/getContent',{query:searchstr},function(r){
		
		$("#d-content").html(r.html);
			
		var c = parseInt(r.count);
		if(c>0){
			
			$("#megafooter").html(r.footer);
		}
		
		endProcess();
	},"json");
}

$(document).ready(function() {
	
	
	var ss = $('#s').val();
	getContent(ss);
	
	$('#s').keyup(function(e){
		
		  if ( e.which == 13 ) {
		  	e.preventDefault();
		  }
		
		var searchstr = $(this).val();
		
		if(searchstr.length){
			
			$("#delsearch").show();
		}else{
			$("#delsearch").hide();
		}
		
		
		getContent(searchstr);
	});

    $(".select2").select2({
        width: '100%',
        templateResult: function (result) {

            if(!result.loading){

                var s = result.id.split('@');
                var parent = parseInt(s[1]);
                if(parent!=0){

                    str = $('<span><img src="/img/subcat.png"> '+result.text+'</span>');
                }else{

                    return str = result.text;
                }
            }else{

                return str = result.text;
            }

            return str;
        },
    });

	$('#idrp').daterangepicker({
		locale: {
          cancelLabel: 'Clear',
          format: mf
		},
		opens: "left"
	});
	
	var startdate = $("#startDate").val();
	var enddate =  $("#endDate").val();
	
	if(startdate!="none"){
		
		$('#idrp').data('daterangepicker').setStartDate(moment(startdate).format(mf));
		$('#idrp').data('daterangepicker').setEndDate(moment(enddate).format(mf));
	    var str = moment(startdate).format(mf)+' - '+moment(enddate).format(mf);
		$("#idrp").html(str);
	}
	
	$('#idrp').on('apply.daterangepicker', function(ev, picker) {
		showProcess();
		var dstart = picker.startDate.format('YYYY-MM-DD');
		var dend = picker.endDate.format('YYYY-MM-DD');
	    $("#startDate").val(dstart);
	    $("#endDate").val(dend);
	    var str = ''+picker.startDate.format(mf)+' - '+picker.endDate.format(mf);
		$("#idrp").html(str);
		$.post("/expenses/changeDateRange",{start:dstart,end:dend},function(r){
			
			getContent(r.s)
			
		},"json");
  	});

	$('#idrp').on('cancel.daterangepicker', function(ev, picker) {
		var dstart = "none";
		var dend = "none";
	    $("#startDate").val(dstart);
	    $("#endDate").val(dend);
		  
	    $("#idrp").html('<span class="s7-date" style="font-size: 24px;"></span>');
		$.post("/expenses/changeDateRange",{start:dstart,end:dend},function(r){
			
			getContent(r.s)
			
		},"json");
	});
	
});

$(document).on("change","#cat", function(){

    var cat = $(this).val();

    $.post("/expenses/changeCat",{cat:cat},function(r){

        getContent("");

    },"json")

    return false;

});

$(document).on('click', '.view', function(){
   	showProcess();
    var id = $(this).data("id");
    $.get('/expenses/getViewBox', {id: id}, function (r) {
        $("#view-exp-details-body").html(r.html);
		$('#expenses-view').modal('show');
	
		endProcess();
    }, "json");
    
    return false;

});

$(document).on('click', '#editExpeFromView', function(){
	showProcess();
    var id = $('#exp_id').val();
    window.location.href = "/expenses/edit/"+id;

});

$(document).on("click","#bundleDelete",function(){

    var $b = $('input.check[type=checkbox]');
    var countChecked = $b.filter(':checked').length;

    var title = $("#_pdt").val();
    var msg = $("#_pdm").val();
    var yes = $("#_yes").val();

    if(countChecked>0){

        swal({

                text: msg,
                title:title,
                type: "warning",
                confirmButtonText: yes,
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true,
                showCancelButton: true,},
            function(){

                showProcess();
                $.ajax({
                    type:'POST',
                    url: '/expenses/deleteBundle',
                    data:$("#flist").serialize(),
                    dataType: 'json',
                    success: function(r) {

                        var s = $("#s").val();
                        getContent(s);


                    }
                });
            });
    }
    return false;
});

$(document).on("click",".del",function(){

    var id= $("#exp_id").val();
    var title = $("#_pdt").val();
    var msg = $("#_pdm").val();
    var yes = $("#_yes").val();



        swal({

                text: msg,
                title:title,
                type: "warning",
                confirmButtonText: yes,
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true,
                showCancelButton: true,
        },
            function(result){
                if(result){
                    showProcess();
                    $.post('/expenses/delete',{id:id},function(r){
                        endProcess();
                        $('#expenses-view').modal('hide');
                        var s = $("#s").val();
                        getContent(s);
                    },"json");

                }
            });


    return false;
});

$(document).on("click",".prevp, .nextp",function(){

    showProcess();
    var p = $(this).data("page");
    $.post('/expenses/changePage',{p:p},function(r){

        var s = $("#s").val();
        getContent(s);

    },"json");

    return false;
});




