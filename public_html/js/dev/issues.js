var mf = $("#moment-format").val();

function getContent(searchstr){

	$.get('/issues/getContent',{query:searchstr},function(r){
		
		$("#d-content").html(r.html);
			
		var c = parseInt(r.count);
		if(c>0){
			
			$("#megafooter").html(r.footer);
		}
		
		endProcess();
	},"json");
}


$(document).ready(function() {
	
	var ss = $('#s').val();
	getContent(ss);
	
	$('#s').keyup(function(e){
		
	  if ( e.which == 13 ) {
	  	e.preventDefault();
	  }
		
		var searchstr = $(this).val();
		
		getContent(searchstr);
	});
	
	
	$('#idrp').daterangepicker({
		locale: {
          cancelLabel: 'Clear',
          format: mf
		},
		ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Next 7 Days': [moment().add(1, 'days'),moment().add(7, 'days'), moment()],
		},		
		opens: "left"
	});
	
	var startdate = $("#startDate").val();
	var enddate =  $("#endDate").val();
	
	if(startdate!="none"){
		
		$('#idrp').data('daterangepicker').setStartDate(moment(startdate).format(mf));
		$('#idrp').data('daterangepicker').setEndDate(moment(enddate).format(mf));
	    var str = moment(startdate).format(mf)+' - '+moment(enddate).format(mf);
		$("#idrp").html(str);
	}

	$('#idrp').on('apply.daterangepicker', function(ev, picker) {
		showProcess();
		var dstart = picker.startDate.format('YYYY-MM-DD');
		var dend = picker.endDate.format('YYYY-MM-DD');
	    $("#startDate").val(dstart);
	    $("#endDate").val(dend);
	    var str = ''+picker.startDate.format(mf)+' - '+picker.endDate.format(mf);
		$("#idrp").html(str);
		$.post("/issues/changeDateRange",{start:dstart,end:dend},function(r){
			
			getContent(r.s)
			
		},"json");
  	});

	$('#idrp').on('cancel.daterangepicker', function(ev, picker) {
		var dstart = "none";
		var dend = "none";
	    $("#startDate").val(dstart);
	    $("#endDate").val(dend);
		  
	    $("#idrp").html('<span class="s7-date" style="font-size: 24px;"></span>');
		$.post("/issues/changeDateRange",{start:dstart,end:dend},function(r){
			
			getContent(r.s)
			
		},"json");
	});

	$(".ranges").prepend('<h4 class="text-center">Deadline</h4>');
	
	
	
	
});


$(document).on("click",".btn-my-tasks",function(){
	
	var id = $("#c-u").val();
	$("#participant").val(id).trigger("change");
	
	return false;
});

$(document).on("change",".opt-filter",function(){
	showProcess();
	var pd = $("#options").serialize();
	
	$.post('/issues/changeOptions',pd,function(r){
	
		getContent("");
		
		
	},"json");
	
});

$(document).on("change",".opt-filter-prj",function(){
	showProcess();
	var pd = $("#options").serialize();
	
	$.post('/issues/changeOptionsPrj',pd,function(r){
		
		$("#miles-box").html(r.htmlmiles);
		if(r.prj==0){
			$("#miles-box").hide();
		}else{
			
			$("#miles-box").show();
		}
		$("#usr-box").html(r.html);
		getContent("");
		
		
	},"json");
	
});
$(document).on("click",".prevp, .nextp",function(){
	
	showProcess();
	var p = $(this).data("page");
	$.post('/issues/changePage',{p:p},function(r){
		
			var s = $("#s").val();
			getContent(s);
		
	},"json");
	
	return false;
});




