function doFormValidation(){
	var validator = $( "#fdata" ).validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		

		
	});
	return validator.form();
}

$(document).ready(function(){
	$(".select2").select2({
      width: '100%'
	});

    
	var wstart =  $("#weekstart").val();
	
	var tf = $("#tformat").val();
	
	if(tf=="H:i"){
		
		var meridian = false;
	}else{
		var meridian = true;
	}
	
	$(".date-picker").datepicker({
		autoclose: true,
		todayHighlight: true,
		weekStart: wstart,
		readOnly: true
	});
	
	$('.time-picker').timepicker({
	    template: false,
	    showSeconds: false,
	    showMeridian: meridian,    
	    showInputs: false,
	    minuteStep: 1
	});
    
    
});

    
$(document).on("click",".doSave",function(){
	
	var isvalid = doFormValidation();
	
	if(isvalid){
		showProcess();
		var pd = $("#fdata").serialize();
		$.post('/calendar/doAdd',pd,function(r){
			
			window.location.href="/calendar";
			
		},"json");
	}
	
	return false;
}); 
