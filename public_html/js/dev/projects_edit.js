var dformat = $("#dpformat").val();
$(document).ready(function() {

	$('#emplist').multiSelect({
		selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Type user name'>",
		selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Type user name'>",
		selectableFooter: "<div class='ms-footer'>Available Users</div>",
		selectionFooter: "<div class='ms-footer'>Selected Users</div>",
		  afterInit: function(ms){
		    var that = this,
		        $selectableSearch = that.$selectableUl.prev(),
		        $selectionSearch = that.$selectionUl.prev(),
		        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
		        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
		
		    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
		    .on('keydown', function(e){
		      if (e.which === 40){
		        that.$selectableUl.focus();
		        return false;
		      }
		    });
		
		    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
		    .on('keydown', function(e){
		      if (e.which == 40){
		        that.$selectionUl.focus();
		        return false;
		      }
		    });
		  },
		  afterSelect: function(values){
		    this.qs1.cache();
		    this.qs2.cache();
		    
		    
		  },
		  afterDeselect: function(values){
		    this.qs1.cache();
		    this.qs2.cache();
		    
		  }	  
	});
    
    $("#addMilestone").click(function(){
	    $("#addMilestone").prop('disabled',true);
	    showProcess();
	    
	    $.get('/projects/getMilestoneBox',function(r){
		    endProcess();
		    	$("#milestones-box").append(r.html);
				$("#addMilestone").prop('disabled',false);
	    },"json");
	    
	    return false;
    });

	$("#fdata").validate({

		  errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
		submitHandler: function() { 	
			showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/projects/doUpdate', 
				data:$('#fdata').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						window.location.href='/projects';
					}
				}
			
			});
			
		}
			
	});


});

$(document).on('focus',".datetimepicker", function(){
	$(this).datetimepicker({
		autoclose: true,
		format:dformat,
		navIcons:{
			rightIcon: 's7-angle-right',
			leftIcon: 's7-angle-left'
		}
	})
});

$(document).on("click",".del-mil",function(){
	
	var id = $(this).data("id");
	$("#"+id).remove();
	
	return false;
});

