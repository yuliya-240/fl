function showProcess(){
	$("#process-loader").addClass("in");
};


function endProcess(){
	$("#process-loader").removeClass("in");
	
}

	    
 $(document).ready(function(){
      	//initialize the javascript
    App.init();
      	
	$("#flogin").validate({
		  rules: {
		    
		    email: {
		      required: true,
		      email: true
		    },
		  },
		  errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
		submitHandler: function() { 	
			showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/login/doLogin', 
				data:$('#flogin').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						window.location.href=r.url;
					}else{
						$("#email").val('');
						$("#pass").val('');
						endProcess();
						swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
						
						
					}
				}
			
			});
			
		}
			
	});

	$("#freset").validate({
		  rules: {
		    
		    remail: {
		      required: true,
		      email: true
		    },
		  },
		  errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
		submitHandler: function() { 	
			showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/login/doSendReset', 
				data:$('#freset').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						$("#login-box").hide();
						$("#reset-box").hide();
						$("#done-box").show();
						endProcess();	
					}else{
						$("#remail").val('');
						
						endProcess();
						swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
						
						
					}
				}
			
			});
			
		}
			
	});
      	
});


$("#act-forget").click(function(){
	
	$("#login-box").hide();
	$("#done-box").hide();
	$("#reset-box").show();
	return false;
});

$(".back-login").click(function(){
	$("#reset-box").hide();
	$("#done-box").hide();
	$("#login-box").show();
	
	return false;
});
