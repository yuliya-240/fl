var uploader = new plupload.Uploader({
	runtimes : 'html5,flash,html4',
	browse_button : 'pickfiles', // you can pass an id...
	container: document.getElementById('containerpic'), // ... or DOM Element itself
	url : '/img/uploadlogo',
	unique_names:true,
	multi_selection:false,
	//max_file_count: 1,

	flash_swf_url : '/js/lib/plupload-2.1.8/js/Moxie.swf',
	silverlight_xap_url : '/js/lib/plupload-2.1.8/js/Moxie.xap',
	
	filters : {
		max_file_size : '100mb',
		mime_types: [
			{title : "Image files", extensions : "jpg,gif,png,jpeg"},
			
		]
	},

	init: {
		PostInit: function() {},

		FilesAdded: function(up, files) {
			$("#progressSlide").css('width',"0%");
			$("#progress1").show();
			$("#pickfiles").hide();
			$("#delpic").hide();
			while (up.files.length > 1) {
	        	up.removeFile(up.files[0]);
	        }		
	
			up.refresh(); // Reposition Flash/Silverlight
			uploader.start();
		},

		UploadProgress: function(up, file) {
			$("#progressSlide").css('width',file.percent+"%");
			$("#progress1").data('percent',file.percent+"%");
		},

		Error: function(up, err) {
			var errmsg="<h4>Upload failed</h4><p>Error Code:"+err.code+"</p><p>"+err.message+"</p>";

			up.refresh(); // Reposition Flash/Silverlight
			
			$("#progress1").hide();
			$('#delpic').show();
			$("#pickfiles").show();
		},
		
		FileUploaded: function(up, file, info) {
			notify("Image Uploaded",'inverse');
			$("#progress1").hide();
			var obj = JSON.parse(info.response);
			$('#logo-box').html('<img class="img-responsive center-block"  src="'+obj.imghost+'userfiles/'+obj.uid+'/pic/'+obj.cleanFileName+'">');
			$('#delpic').show();
			$("#pickfiles").show();
			
	
		}
	}
});


function doFormValidation(formstr){
	var validator = $(formstr).validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		

		
	});
	return validator.form();
}

$(document).ready(function() {
	uploader.init();
	
	$("#edit-inv-pref").click(function(){
		showProcess();
		$("#edit-inv-pref").hide();
		$("#pref-view").hide();
		$.get("/settings/getInvPrefs",function(r){
			
			$("#pref-edit").html(r.html).show();
			$(".pub").bootstrapSwitch({"size":"medium", "onText":'<i class="fa fa-eye" aria-hidden="true"></i>', "offText":'<i class="fa fa-eye-slash" aria-hidden="true"></i>'});
		    $(".inum").mask('0#', {
			   maxlength: false,
			});
		    endProcess();
			
		},"json")
		
		
		return false;
	});
	
	$("#edit-inv-terms").click(function(){
		
		showProcess();
		$("#edit-inv-terms").hide();
		$("#def-terms-inv-view").hide();
		$.get("/settings/getInvTerms",function(r){
			
			$("#def-terms-inv-edit").html(r.html).show();
			window.location.href="#interms-panel";
		    endProcess();
			
		},"json")
		
		return false;
	});

	$("#delpic").click(function(){
		
		$.post('/settings/removeLogo');
        $('#logo-box').html('<img class="img-responsive center-block" src="/img/nologo.png">'); 
        $(this).hide();
		return false;
	});


	$(".color").simplecolorpicker({picker: true, theme: 'glyphicons'});

  /*$inputs.each(function(idx, item) {
    var $input = $(item);
    var $target = $input.parents('.control-group:first').find('label:first');
    $target.css('color', $input.val());

    $input.on('colorPicker:preview colorPicker:change', function(e, value) {
      $target.css('color', value);
    });

    $input.on('colorPicker:addSwatch', function(e, value) {
      console.log('added custom swatch with value:', value);
    });
  });*/



});

$(document).on("click","#cancelSaveInvPref",function(){
	
	$("#edit-inv-pref").show();
	$("#pref-edit").hide();
	$("#pref-view").show();
	
	
	return false;
});

$(document).on("click","#doSaveInvPref",function(){
	
	var isValid = doFormValidation("#fpref");
	
	if(isValid){
		showProcess();
		var pd = $("#fpref").serialize();
		
		$.post("/settings/doUpdateInvPref",pd,function(r){
			
			$("#edit-inv-pref").show();
			$("#pref-edit").hide();
			$("#pref-view").html(r.html).show();
			
			endProcess();
			
		},"json")
	}
	
	
	return false;
});

$(document).on("click","#cancelSaveInvTerms",function(){
	
	$("#edit-inv-terms").show();
	$("#def-terms-inv-edit").hide();
	$("#def-terms-inv-view").show();
	
	
	return false;
});

$(document).on("click","#doSaveInvTerms",function(){

	showProcess();
	var pd = $("#fterms").serialize();
	
	$.post("/settings/doUpdateInvTerms",pd,function(r){

		$("#edit-inv-terms").show();
		$("#def-terms-inv-edit").hide();
		$("#def-terms-inv-view").html(r.html).show();
		
		endProcess();
		
	},"json")
	return false;
});


// Estimates Terms & Notes

$(document).on("click","#edit-est-terms",function(){
	
	showProcess();
	$("#edit-est-terms").hide();
	$("#def-terms-est-view").hide();
	$.get("/settings/getEstTerms",function(r){
		
		$("#def-terms-est-edit").html(r.html).show();
		window.location.href="#estterms-panel";
	    endProcess();
		
	},"json")
	
	return false;
});

$(document).on("click","#cancelSaveEstTerms",function(){
	
	$("#edit-est-terms").show();
	$("#def-terms-est-edit").hide();
	$("#def-terms-est-view").show();
	
	
	return false;
});

$(document).on("click","#doSaveEstTerms",function(){

	showProcess();
	var pd = $("#fterms").serialize();
	
	$.post("/settings/doUpdateEstTerms",pd,function(r){

		$("#edit-est-terms").show();
		$("#def-terms-est-edit").hide();
		$("#def-terms-est-view").html(r.html).show();
		window.location.href="#estterms-panel";
		endProcess();
		
	},"json")
	return false;
});

$(document).on("change",".color",function(){
	
	var color= $(this).val();
	var id = $(this).prop("id");
	$.post("/settings/changeColor",{field:id,color:color},function(r){
		
		notify(r.msg,'inverse');
		
	},"json");
	
});

