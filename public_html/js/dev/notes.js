var $container = $('.gallery-container');
var mas;



function aplyMassonry() {

    $container.masonry('destroy');


    // initialize
    $container.masonry({
        columnWidth: 0,
        itemSelector: '.item'
    });
    
    
    
}

function getContent(p) {
    $.get('/notes/getContent', {p: p}, function (r) {
        $("#d-content").html(r.html);
        $(".wells").hover(function () {
            var id = $(this).data("id");
            $("#action" + id).show();
            $(this).css("box-shadow", "0 0 5px 5px rgba(122,122,122,0.3)");

        }, function () {
            var id = $(this).data("id");
            $("#action" + id).hide();
            $(this).css("box-shadow", "0 0 5px rgba(122,122,122,0.5)");
        });
        if (r.pag.next > 0) {
            $("#load-more").data("next", r.pag.next).show();
        }
		
		$('#d-content').imagesLoaded( function() {
			aplyMassonry();
		});

        $("#megafooter").html(r.footer);
        endProcess();

    }, "json");
}
// function appendContent(){
// 	$(aplyMassonry).append(getContent());
// }
$(document).ready(function () {
    showProcess();
    var ss = $('#s').val();
    getContent(ss);


    $('#s').keyup(function (e) {

        if (e.which == 13) {
            e.preventDefault();
        }

        var searchstr = $(this).val();

        if(searchstr.length){

            $("#delsearch").show();
        }else{
            $("#delsearch").hide();
        }


        getContent(searchstr);

        $.get('/notes/getContent', {query: searchstr}, function (r) {

            $("#d-content").html(r.html);
            aplyMassonry()

        }, "json");


    });


});

$(document).on("click", ".item1", function (e) {
    e.preventDefault();
    showProcess();
    var id = $(this).data("id");
    $.get('/notes/view', {id: id}, function (r) {
        endProcess();


        bootbox.dialog({
            title: r.title,

            message: r.html,
            buttons: {
                edit: {
                    label: r.btnedit,
                    className: 'btn-primary pull-right',
                    callback: function () {
                        showProcess();
                        window.location.href = '/notes/edit/' + id;
                    }

                },
                delete: {
                    label: r.btndel,
                    className: 'btn-danger pull-right m-r-5',

                    callback: function () {

                        var title = $("#_idt").val();
                        var msg = $("#_idm").val();
                        var yb = $("#_yb").val();
                        var cb = $("#_cb").val();

                        swal({
                            title: title,
                            text: msg,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: yb,
                            cancelButtonText: cb,
                        }, function (result) {
                            if (result) {
                                showProcess();
                                $.post("/notes/del", {id: id}, function () {
                                    endProcess();
                                    $(".div" + id).remove();
                                    bootbox.hideAll();
                                    $container.masonry('layout');
                                }, "json");

                            }
                        });


                        return false;

                    }

                },
                close: {
                    label: r.btnclose,
                    className: 'btn-default pull-left',
                }
            }
        });

    }, "json");

    return false;
});

function appendContent(p) {
    $.get('/notes/getContent', {p: p}, function (rez) {
        var $boxHtm = $(rez.html);
        $container.append($boxHtm);
		$('#d-content').imagesLoaded( function() {
	       //$container.masonry('reloadItems')
            $container.masonry('appended', $boxHtm).masonry('layout');
        
        });
        if (rez.pag.next > 0) {
            $("#load-more").data("next", rez.pag.next).show();
        } else {

            $("#load-more").hide();
        }
    }, "json")
}
$(document).on("click", "#load-more", function () {
    var p = $(this).data("next");
    console.log(p);
    appendContent(p);
    //aplyMassonry();
    return false;
});

$(document).on("click","#bundleDelete",function(){

    var $b = $('input.check[type=checkbox]');
    var countChecked = $b.filter(':checked').length;

    var title = $("#_ndt").val();
    var msg = $("#_ndm").val();
    var yes = $("#_yes").val();



    if(countChecked>0){

        swal({

                text: msg,
                title:title,
                type: "warning",
                confirmButtonText: yes,
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true,
                showCancelButton: true,
            },
            function(){

                showProcess();
                $.ajax({
                    type:'POST',
                    url: '/notes/deleteBundle',
                    data:$("#flist").serialize(),
                    dataType: 'json',
                    success: function(r) {

                        var s = $("#s").val();
                        getContent(s);


                    }
                });
            });
    }
    return false;
});