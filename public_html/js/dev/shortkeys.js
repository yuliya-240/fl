// define a handler
function doc_keyUp(e) {

    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.keyCode == 40) {
        // call your function to do the thing
        pauseSound();
    }
}
// register the handler 
document.addEventListener('keyup', doc_keyUp, false);

/*
MAC COMMAND button:
 	
Firefox: 224
Opera: 17
WebKit (Safari/Chrome): 91 (Left Apple) or 93 (Right Apple)

	
*/	