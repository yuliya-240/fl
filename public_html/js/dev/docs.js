var tcb = $("#_tcb").val();
var avc = $("#_avc").val();
var selc = $("#_selc").val();

function doFormValidation(){
	var validator = $( "#ffolder" ).validate({
				  errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		

		
	});
	return validator.form();
}

function getContent(){
	
		$.get('/doc/getContent',function(r){
			
			$("#d-content").html(r.html);
			$("#megafooter").html(r.footer);
			
			if(r.folder>0){
				$(".nfldr").hide();
				$(".back").show();
				$("#c-f-name").text(r.foldername);
				$("#f-t").show();
				$("#docid").val(r.folder);
				
				if(r.sharemode==2){
					$(".shareldr").hide();
					
				}else{
				
					$(".shareldr").show();
				}
				
				$("#sh-colab-box").html(r.sharebox);

				$('#emplist').multiSelect({
					selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='"+tcb+"'>",
					selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='"+tcb+"'>",
					selectableFooter: "<div class='ms-footer'>"+avc+"</div>",
					selectionFooter: "<div class='ms-footer'>"+selc+"</div>",
					  afterInit: function(ms){
					    var that = this,
					        $selectableSearch = that.$selectableUl.prev(),
					        $selectionSearch = that.$selectionUl.prev(),
					        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
					        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
					
					    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
					    .on('keydown', function(e){
					      if (e.which === 40){
					        that.$selectableUl.focus();
					        return false;
					      }
					    });
					
					    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
					    .on('keydown', function(e){
					      if (e.which == 40){
					        that.$selectionUl.focus();
					        return false;
					      }
					    });
					  },
					  afterSelect: function(values){
					    this.qs1.cache();
					    this.qs2.cache();
					    
					    
					  },
					  afterDeselect: function(values){
					    this.qs1.cache();
					    this.qs2.cache();
					    
					  }	  
				});
		
				
			}else{
				
				if(r.sharemode==2){
				
					$(".nfldr").hide();
				}else{
				
					$(".nfldr").show();
				}
				$(".shareldr").hide();				
				$(".back").hide();
				$("#f-t").hide();
			}
			
		
			$("#cshmode").val(r.sharemode);
			
			$("#cfolder").val(r.folder);
			
			endProcess();
			var c = parseInt(r.count);
			if(c){
				
				$("#megafooter").show();
			}else{
				
				$("#megafooter").hide();
			}
		},"json");
	
}

$(document).ready(function() {
	
	getContent(1);
	



	
	
});

$(document).on("click",".nfldr",function(){
	
	var t = $("#_tnf").val();
	var txt = $("#_txtnf").val();

	var c = $("#_fc").val();
	var ca = $("#_cancel").val();

	
	bootbox.dialog({
		
		title:t,
		className:"modal-colored-header",
		message:'<form id="ffolder" class="form-horizontal">'+
				'<div class="form-group">'+
					'<label class="col-sm-3 control-label no-padding-right">'+txt+' </label>'+
				    '<div class="col-sm-9">'+
					    '<input type="text" required name="folder_name" id="folder_name"  class="form-control" >'+
					'</div>'+
				'</div>'+
				'</form>',
            buttons: {
                edit: {
                    label: c,
                    className: 'btn-primary pull-right',
                    callback: function () {
	                    var isg = doFormValidation();
	                    
	                    if(!isg){
		                    
		                    return false;
	                    }else{
		                    
                        showProcess();
                        var name = $("#folder_name").val()
                        $.post('/doc/createFolder',{name:name},function(r){
	                        
	                        getContent();
                        },"json")
	                    }
	                    
                    }

                },

                close: {
                    label: ca,
                    className: 'btn-default pull-left',
                }
            }		
	});
	
	return false;
});

$(document).on("click",".newdoc",function(){
	
	showProcess();
	var f = $("#cfolder").val();
	$.get('/doc/add',{f:f},function(r){
		
		window.location.href="/doc/edit/"+r.id;
	},"json");
	
	return false;
});

$(document).on("click",".prevp, .nextp",function(){
	
	showProcess();
	var p = $(this).data("page");
	$.post('/doc/changePage',{p:p},function(r){
		
		getContent();
		
	},"json");
	
	return false;
});

$(document).on("click","#bundleDelete",function(){


	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_pdt").val();
	var msg = $("#_pdm").val();
	var yes = $("#_yes").val();

	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true,
			showCancelButton: true,},
			function(){
					
				showProcess();	
				$.ajax({
					type:'POST', 
					url: '/doc/deleteBundle', 
					data:$("#flist").serialize(), 
					dataType: 'json',
					success: function(r) {
					
							getContent(1);

					}
				});					
			});	
	}
	return false;
});     	

$(document).on('keypress', function(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
    }
});

$(document).on("click",".folder",function(){
	
	showProcess();
	
	var id = $(this).data("id");
	
	$.post("/doc/changeFolder",{id:id},function(r){
		
		getContent();
	},"json")
	
	return false;
})

$(document).on("change","#share-mode",function(){
	
	showProcess();
	var mode = $(this).val();
	
	$.post("/doc/changeShareMode",{mode:mode},function(r){
		
		getContent();
		
	},"json")
	
});

$(document).on("click",".shareldr",function(){
	
	//$(".dropdowndoc").dropdown('toggle');
	$('#share-box').modal('show');
	
	return false;
});

$(document).on("click",".doShare",function(){
	
	showProcess();
	
	var pd = $("#fcollab").serialize();
	
	$.post("/doc/doShareFolder",pd,function(r){
		
		$('#share-box').modal('hide') ;
		getContent();
		
	},"json");
	
	
	return false;
});


