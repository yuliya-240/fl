var $container = $('.gallery-container');
var mas;


function aplyMassonry() {

    $container.masonry('destroy');
    // initialize
    $container.masonry({
        columnWidth: 0,
        itemSelector: '.item'
    });



}

function getContent(p) {
    $.get('/portfolio/getContent', {p: p}, function (r) {

        $("#d-content").html(r.html);

        if (r.pag.next > 0) {
             $("#load-more").data("next", r.pag.next).show();
        }

         $('#d-content').imagesLoaded( function() {
             //$container.masonry();
            aplyMassonry();
         });

        $("#megafooter").html(r.footer);
        endProcess();

    }, "json");
}

$(document).ready(function () {
    showProcess();
    var ss = $('#s').val();
    getContent(ss);


    $('#s').keyup(function (e) {

        if (e.which == 13) {
            e.preventDefault();
        }

        var searchstr = $(this).val();

        if(searchstr.length){

            $("#delsearch").show();
        }else{
            $("#delsearch").hide();
        }


        getContent(searchstr);

        $.get('/portfolio/getContent', {query: searchstr}, function (r) {

            $("#d-content").html(r.html);
            aplyMassonry()

        }, "json");


    });


});

//$(document).on("click", ".item", function (e) {
//    e.preventDefault();
//    showProcess();
//    var id = $(this).data("id");
//    $.get('/portfolio/view', {id: id}, function (r) {
//        endProcess();
//
//    }, "json");
//
//    return false;
//});

function appendContent(p) {
    $.get('/portfolio/getContent', {p: p}, function (rez) {
        var $boxHtm = $(rez.html);
        $container.append($boxHtm);
        $('#d-content').imagesLoaded( function() {
            //$container.masonry('reloadItems')
            $container.masonry('appended', $boxHtm).masonry('layout');

        });
        if (rez.pag.next > 0) {
            $("#load-more").data("next", rez.pag.next).show();
        } else {

            $("#load-more").hide();
        }
    }, "json")
}
$(document).on("click", "#load-more", function () {
    var p = $(this).data("next");
    console.log(p);
    appendContent(p);
    //aplyMassonry();
    return false;
});
$(document).on("click",".s7-trash", function(){
    var id = $(this).data("id");
    var title = $("#_idt").val();
    var msg = $("#_idm").val();
    var yb = $("#_yb").val();
    var cb = $("#_cb").val();

    swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: yb,
            cancelButtonText: cb,
        }, function (result) {
        if (result) {
            showProcess();
            $.post("/portfolio/del", {id: id}, function () {
                endProcess();
                $(".div" + id).remove();
                $container.masonry('reloadItems');
                $container.masonry("layout");
            }, "json");
        }
    })
    return false;

});
$(document).on("click",".s7-edit", function(){
    var id = $(this).data("id");
    window.location.href = '/portfolio/edit/' + id;
});

$(document).on("click","#bundleDelete",function(){

    var $b = $('input.check[type=checkbox]');
    var countChecked = $b.filter(':checked').length;

    var title = $("#_pdt").val();
    var msg = $("#_pdm").val();
    var yes = $("#_yes").val();



    if(countChecked>0){

        swal({

                text: msg,
                title:title,
                type: "warning",
                confirmButtonText: yes,
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true,
                showCancelButton: true,
            },
            function(){

                showProcess();
                $.ajax({
                    type:'POST',
                    url: '/portfolio/deleteBundle',
                    data:$("#flist").serialize(),
                    dataType: 'json',
                    success: function(r) {

                        var s = $("#s").val();
                        getContent(s);


                    }
                });
            });
    }
    return false;
});