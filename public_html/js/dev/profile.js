var uploader;

var jCrop={
	initialized:false,
}

function doFormValidationOnFly(formstr){
	var validator = $(formstr).validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		

		
	});
	return validator.form();
}

function doFormValidationUname(){
	var validator = $( "#fdata" ).validate({
				errorPlacement: function(label, elem) {
				  	var errbox = elem.data("errorbox");
				  	$(errbox).html(label);
		  		},	
		  		  
			  	rules: {
				    user_username: {
					  minlength: 6,
				      required: true,
				    }
			  	},		  		  	
	});
	
	return validator.form();
}

function getAccVieInfo(){
	
		$.get("/profile/getAccView",function(rr){
				
				$("#infoacc-edit-box").html("").hide();		
				$("#infoacc-view-box").html(rr.html).show();
				$(".btn-edit-acc").show();
				$(".pub").bootstrapSwitch({"size":"medium", "onText":'<i class="fa fa-eye" aria-hidden="true"></i>', "offText":'<i class="fa fa-eye-slash" aria-hidden="true"></i>'});
				
				$(".pub").on('switchChange.bootstrapSwitch', function(event, state) {
				
					var name = $(this).prop("name");
					
					if(state){
						var val = 1;
					}else{
						var val = 0;
					}
					
					$.post('/profile/updPub',{name:name,val:val},function(r){
						
						notify(r.msg,'inverse');
						
					},"json");
			  
				});

				endProcess();
				
			},"json");	
}

function getEduList(){
	
	
	$.get("/profile/getEduList",function(r){
		
		$("#edu-edit-box").hide();
		$("#edu-view-box").html(r.html).show();
		
	},"json");
	
}

function getWorkList(){


	$.get("/profile/getWorkList",function(r){

		$("#work-edit-box").hide();
		$("#work-view-box").html(r.html).show();

	},"json");

}

function getPrefs(){
	
	$.get("/profile/getPref",function(rr){
		
		$("#acc-pref-edit").html("").hide();		
		$("#acc-pref-view").html(rr.html).show();
		$(".btn-pref-edit").show();
		
	},"json")
	
}

$(document).ready(function(){
	function updateCoords(c){

    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };
  
	uploader = new plupload.Uploader({
		runtimes : 'html5,flash,html4',
		browse_button : 'pickfiles', // you can pass an id...
		container: document.getElementById('counteinerpic'), // ... or DOM Element itself
		url : '/img/uploaduserpic',
		unique_names:true,
		multi_selection:false,
		max_file_count: 1,
	
		
		filters : {
			max_file_size : '10mb',
			mime_types: [
				{title : "Image files", extensions : "jpg,gif,png,jpeg"},
				
			]
		},
	
		init: {
			PostInit: function() {
                var pic = $("#u-ava").prop("src");
                $("#target").prop("src",pic);
                var ava = $("#fname").val();
                if(ava=="nopic.png"){
                    $("#del-pic").hide();
                }else{
                    $("#del-pic").show();
                }
            },
	
			FilesAdded: function(up, files) {
				$("#progressSlide").css('width',"0%");
				$("#progress1").show();
				$("#btns").hide();
		
				up.refresh(); // Reposition Flash/Silverlight
				uploader.start();
			},
	
			UploadProgress: function(up, file) {
				$("#progressSlide").css('width',file.percent+"%");
				$("#progress1").data('percent',file.percent+"%");
			},
	
			Error: function(up, err) {
				
				swal({
					title:err.message,
					text:err.code,
					type:"error"
				})
		
				up.refresh(); // Reposition Flash/Silverlight
				
				$("#progress1").hide();
				$("#btns").show();
		
			},
			
			FileUploaded: function(up, file, info) {
				$("#progress1").hide();
				var obj = JSON.parse(info.response);
				if(!obj.status){
					swal({
						title:obj.title,
						text:obj.msg,
						type:"error"
					})
					
				}else{
					$('#target').prop('src',obj.imghost+obj.cid+'/pic/'+obj.cleanFileName);
					$("#fname").val(obj.cleanFileName);
                    $("#ftype").val(obj.type);
					$("#md-custom").find('.modal-content').css({'max-width':obj.width });

					$('#target').Jcrop({
						onChange: updateCoords,
					   onSelect: updateCoords,	
					  setSelect: [ 0,0,200,200 ],
					  aspectRatio: 1,
					  bgColor: 'blue',
					  bgOpacity:0.7,
					  minSize: [200,200]
					},function(){
					    jCrop.initialized=true;
					    jCrop.jcrop_api = this;
					});
					
					$("#picSave").show();
                    $("#del-pic").show();
				}
				$("#btns").show();
				
			}
		}
});	
		
	uploader.init();

	//$('#md-custom').on('shown.bs.modal', function () {
    //
     //   var ava = $("#fname").val();
     //   if(ava=="nopic.png"){
     //       $("#del-pic").hide();
     //   }
    //
     //   uploader.refresh();
    //
	//});
	
	getAccVieInfo();	
	getEduList();
    getWorkList();
	getPrefs();
});

$(document).on("click","#btnCancel",function(){
	
	
	$("#md-custom").modal('hide');
	
	if (jCrop.initialized){
		$('#target').Jcrop('destroy');
		jCrop.initialized = false;
	}

	var pic = $("#u-ava").prop("src");
	$("#target").prop("src",pic);
	$("#picSave").hide();
	return false;	
});

$(document).on("click","#picSave",function(){
	
	showProcess();
	
	var  pd = $("#fcoord").serialize();
	
	$.post('/img/createavatar',pd,function(r){

        endProcess();
        $("#md-custom").modal('hide');
        $("#picSave").hide();
        $("#u-ava").prop("src","/userfiles/"+ r.cid +"/pic/"+ r.fname );
        $("#u-ava-min").prop("src","/userfiles/"+ r.cid +"/pic/"+ r.fname );
        $("#target").prop("src","/userfiles/"+ r.cid +"/pic/"+ r.fname );
        $("#name-ava").val(r.fname);
        if (jCrop.initialized){
            $('#target').Jcrop('destroy');
            jCrop.initialized = false;
        }

    },"json");

	return false;
});

$(document).on("click", "#del-pic", function(){

    $.get('/img/delava',function(r){
        $("#fname").val('nopic.png');
        $("#name-ava").val("nopic.png");
        $("#u-ava").prop("src","/img/avatar/avatar2x200.jpg");
        $("#u-ava-min").prop("src","/img/avatar/avatar2x200.jpg");
        $("#target").prop("src","/img/avatar/avatar2x200.jpg");
        if (jCrop.initialized){
            $('#target').Jcrop('destroy');
            jCrop.initialized = false;
        }
        $("#picSave").hide();
        $("#del-pic").hide();
    },"json");

   return false;
});


$(document).on("click","#chUsername",function(){
	showProcess();
	$.get('/profile/getUnameBox',function(r){
	endProcess();
	bootbox.dialog({
			
		title:r.title,
		message:r.html,
		className: "modal-colored-header",
		closeButton: false,
		buttons: {
		    add: {
		      label: 'Save',
		      className: "btn-primary",
		      callback: function() {
			   
			   var isValid = doFormValidationUname(); 
			   
			   if(isValid){
				var pd = $("#fdata").serialize();
				$.get('/profile/isusrnm',pd,function(rez){
					
					if(rez.status){
				    
					    showProcess();
						
				        $.post('/profile/changeUname',pd,function(r){
					         endProcess();
							 bootbox.hideAll();
						       
							notify(r.msg,'inverse');
							$("#l-u").html(r.uname+r.html);
					        
				        },"json");
					    
				    }else{
					    
					    $("#error-box").html('Username already Taken');
					    return false;

					}
					
				},"json");
				return false;
		      }else{
			      
			      return false;
		      }
		    }
		    },
		    cancel: {
		      label: 'Cancel',
		      className: "btn btn-default pull-left",
		     
		    }
		  }
		})
		
		
	},"json");
	
	return false;
});

$(document).on("click","#chEmail",function(){
 
	var uemail = $("#login-email").val();
	var email_req = $("#_er").val();
	var not_email = $("#_ne").val();
	var loginemail = $("#_le").val();
	var rtitle = $("#_tit").val();
	
	bootbox.dialog({
			
		title:rtitle,
		message:'<form id="femaillogin" class="form-horizontal" role="form"><br><div class="form-group">'+
				'<label class="col-sm-3 control-label no-padding-right" >'+loginemail+'</label>'+
				'<div class="col-sm-7">'+
				'<input type="email"  class="form-control no-margin-bottom" value="'+uemail+'" name="loginemail" required=""  data-errorbox="#error-box"  placeholder="E-mail" autocomplete="off" data-msg-required="'+email_req+'" data-msg-email="'+not_email+'">'+		
				'<div id="error-box" style="margin-top:5px;"></div>'+
				'</div>'+
				'</div>'+
				'</form>',
		 closeButton: false,
		 className: "modal-colored-header",
		 buttons: {
		    add: {
		      label: 'Save',
		      className: "btn-primary",
		      callback: function() {
			    
			    var isValid =  doFormValidationOnFly("#femaillogin");
			    
			    if(isValid){
				    showProcess();
					
					var pd = $("#femaillogin").serialize();
					
			        $.post('/profile/changeEmail',pd,function(r){
				         endProcess();
				        if(!r.status){
					       
					        
					        swal({
						       title:r.title,
						       text: r.msg,
						       type:"error"
					        });
					        
				        }else{
					       
							notify(r.msg,'inverse');
							$("#l-e").html(r.email+r.html);
					        
				        }
				        
			        },"json");
				    
			    }else{
				    return false;
			    }
		        
		        
		      }
		    },
		    main: {
		      label: 'Cancel',
		      className: "btn btn-default pull-left",
		      callback: function() {
		        
		      }
		    }
		  }
		})
	
	return false;
});

$(document).on("click","#chPass",function(){
			showProcess();
		 
			
			$.post('/profile/getBoxPass/',function(r){
				endProcess();
				bootbox.dialog({
					title: r.title,
					message:r.html,
					className: "modal-colored-header",
					closeButton: false,
					 buttons: {
					    add: {
					      label: 'Save',
					      className: "btn-primary",
					      callback: function() {
						    
							var isvld = doFormValidationOnFly("#floginpass");
							
							if(isvld){

								showProcess();
								
								var pd = $("#floginpass").serialize();
							
						        $.post('/profile/changePassword',pd,function(r){
							        endProcess();
							        if(!r.status){
								       
								        swal({
									       title:r.title,
									       text: r.msg,
									       type:"error"
								        });
							        }else{
								        
								        notify(r.msg,'inverse');
								        
							        }
							        
						        },"json")
								
							}else{
								
								
								return false;
							}
							

					      }
					    },
					    main: {
					      label: 'Cancel',
					      className: "btn btn-default pull-left",
					      callback: function() {
					        
					      }
					    }
					  }
				})
				
			},"json");
	    	    
	    return false;
	    
    });

$(document).on('keypress', function(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
    }
});

// Billing Address

$(document).on("click","#billing-addr-edit",function(){
	
	showProcess();
	$("#billing-addr-edit").hide();
	$("#billing-addr-view-box").hide();
	$.get("/profile/getBillAddr",function(r){
		
		$("#billing-addr-edit-box").html(r.html).show();
		window.location.href="#prof-bill-box";
	    endProcess();
		
	},"json")
	
	return false;
});

$(document).on("click","#cancelSaveBillAddr",function(){
	
	$("#billing-addr-edit").show();
	$("#billing-addr-edit-box").hide();
	$("#billing-addr-view-box").show();
	
	
	return false;
});

$(document).on("click","#doSaveBillAddr",function(){
	var isValid = doFormValidationOnFly("#fbill");
	
	if(isValid){

	showProcess();
	var pd = $("#fbill").serialize();
	
	$.post("/profile/doUpdateBillAddr",pd,function(r){

		$("#billing-addr-edit").show();
		$("#billing-addr-edit-box").hide();
		$("#billing-addr-view-box").html(r.html).show();
		window.location.href="#bill-panel";
		endProcess();
		
	},"json");
	}
	return false;
});

// account edit

$(document).on("click",".btn-edit-acc",function(){
	$(".btn-edit-acc").hide();
	showProcess();
	$.get("/profile/editinfo",function(r){
		
		$("#infoacc-view-box").hide();
		$("#infoacc-edit-box").html(r.html).show();
		//window.location.href="#acc-win";
		endProcess();
		
	},"json")
	
	return false;
});

$(document).on("click","#btn-cancel-acc-edit",function(){
	//showProcess();

	$("#infoacc-edit-box").html("").hide();		
	$("#infoacc-view-box").show();
	$(".btn-edit-acc").show();
	
	return false;
});

$(document).on("click","#btn-save-acc-edit",function(){
	
	var isvalid = doFormValidationOnFly("#facc");
	
	if(!isvalid){
		
		return false;
	}else{
		showProcess();
		var pd = $('#facc').serialize();
		
		$.post("/profile/updateAcc",pd,function(r){
			
			getAccVieInfo();
			
			
		},"json");
		
		
	}
	
	return false;
});


// preferences edit

$(document).on("click",".btn-pref-edit",function(){
	$(".btn-pref-edit").hide();
	showProcess();
	$.get("/profile/getEditPref",function(r){
		
		$("#acc-pref-view").hide();
		$("#acc-pref-edit").html(r.html).show();

		var offset = new Date().getTimezoneOffset();
		var offsetHr = (offset/60)*(-1);
		
		$.get("/first/getTZ",{offset:offsetHr},function(r){
			if(r.status){
				$("#tz").val(r.tz.tz_olson).change();
			}
			
		},"json");

		window.location.href="#prof-pref-box";
		endProcess();
		
	},"json")
	
	return false;
});

$(document).on("change","#dformat, #tformat, #tz",function(){
	     
	var df=$('#dformat').val();
	var tf=$('#tformat').val();
	var tz=$('#tz').val();
	
	var strf ="df="+df+"&tf="+tf+"&tz="+tz;
	$.post('/misc/firstChangeDateTimeFormat',strf,function(data){ 
    	$('#dateExample').html(data.html);
				
 	},"json");    
 	return false;
});

$(document).on("click","#btn-cancel-acc-pref",function(){

	$("#acc-pref-edit").html("").hide();		
	$("#acc-pref-view").show();
	$(".btn-pref-edit").show();
	
	return false;
});

$(document).on("click","#btn-save-acc-pref",function(){
	
	var isvld = doFormValidationOnFly("#fpref");
	
	if(!isvld){
		return false;
	}else{
		showProcess();
		var pd = $('#fpref').serialize();
		
		$.post("/profile/updateProfile",pd,function(r){
			
			if(!r.status){
				
				endProcess();
				swal({
					title:"Operation Failed",
					text: r.errmsg,
					type:"error"
				});
				
			}else{
				
				$.get("/profile/getPref",function(rr){
					
					window.location.href="/profile";
					
				},"json")
			}
			
		},"json");
		
	}
	
	return false;
});



// Education

$(document).on("click",".btn-edu-edit",function(){
	
	$(".btn-edu-edit").hide();
	showProcess();
	$.get("/profile/getEduEdit",function(r){
	
		
		$("#edu-view-box").hide();
		$("#edu-edit-box").html(r.html).show();
		window.location.href="#prof-edu-box";
		endProcess();
	
	},"json")
	
	
	return false;
});

$(document).on("click",".btn-edu-add",function(){
	
	$(".btn-edu-edit").hide();
	showProcess();
	$.get("/profile/getEduEdit",function(r){
	
		
		$("#edu-view-box").hide();
		$("#edu-edit-box").html(r.html).show();
		$(".btn-edu-add-record").trigger("click");
		
		window.location.href="#prof-edu-box";
		endProcess();
	
	},"json")
	
	
	return false;
});

$(document).on("click",".btn-edu-add-record",function(){
	
	$(".btn-edu-add-record").prop("disabled",true);
	$.get("/profile/getNewEduRecord",function(r){
		
		$("#edu-edit-list").append(r.html);
		$(".btn-edu-add-record").prop("disabled",false);
	},"json")
	
	return false;
});

$(document).on("click",".del-edu-record",function(){
	
	var id = $(this).data("id");

    $.post("/profile/deleteEdu", {id:id}, function(){
        $("#"+id).remove();
    },"json");
	
	return false;
});

$(document).on("click","#btn-cancel-edu",function(){
	showProcess();
	$("#edu-edit-box").html("").hide();
	getEduList();
	$("#edu-view-box").show();
	$(".btn-edu-edit").show();
	endProcess();
	return false;
});

$(document).on("click","#btn-save-edu",function(){
	
	
	var isvld = doFormValidationOnFly("#fedu");
	
	if(isvld){
		showProcess();
		var pd = $("#fedu").serialize();
	
		$.post("/profile/updEdu",pd,function(r){
			
			if(r.status){
				
				$("#edu-edit-box").html("").hide();
				getEduList();
				$("#edu-view-box").show();
				$(".btn-edu-edit").show();
				endProcess();

			}else{
				
				endProcess();
			}		
		},"json")
		

	}
	
	return false;
});

// Work

$(document).on("click",".btn-work-edit",function(){

    $(".btn-work-edit").hide();
    showProcess();
    $.get("/profile/getWorkEdit",function(r){


        $("#work-view-box").hide();
        $("#work-edit-box").html(r.html).show();

        $(".check-work-now").bootstrapSwitch({"size":"medium", "onText":'<i aria-hidden="true">' + r.btnyes + '</i>', "offText":'<i aria-hidden="true">' + r.btnno + '</i>'});

        $(".check-work-now").on('switchChange.bootstrapSwitch', function(event, state) {


            var id = $(this).data("id");
             
					if(state){
						$("#work-to-box"+id).hide();
						$("#wcv"+id).val(1);
					}else{
						$("#work-to-box"+id).show();
						$("#wcv"+id).val(0);
					}
            
            
            
        });



        window.location.href="#prof-work-box";
        endProcess();

    },"json")


    return false;
});

$(document).on("click",".btn-work-add",function(){

    $(".btn-work-edit").hide();
    showProcess();
    $.get("/profile/getWorkEdit",function(r){


        $("#work-view-box").hide();
        $("#work-edit-box").html(r.html).show();
        $(".btn-work-add-record").trigger("click");

        window.location.href="#prof-work-box";
        endProcess();

    },"json");


    return false;
});

$(document).on("click",".btn-work-add-record",function(){

    $(".btn-work-add-record").prop("disabled",true);
    $.get("/profile/getNewWorkRecord",function(r){

        $("#work-edit-list").append(r.html);
        $(".btn-work-add-record").prop("disabled",false);
        $(".check-work-now").bootstrapSwitch({"size":"medium", "onText":'<i aria-hidden="true">' + r.btnyes + '</i>', "offText":'<i aria-hidden="true">' + r.btnno + '</i>'});

        $(".check-work-now").on('switchChange.bootstrapSwitch', function(event, state) {


            var id = $(this).data("id");
			if(state){
				$("#work-to-box"+id).hide();
				$("#wcv"+id).val(1);
			}else{
				$("#work-to-box"+id).show();
				$("#wcv"+id).val(0);
			}
        });
    },"json");

    return false;
});

$(document).on("click",".del-work-record",function(){

    var id = $(this).data("id");
    $.post("/profile/deleteWork", {id:id}, function(){
        $("#"+id).remove();
    },"json");
    return false;
});

$(document).on("click","#btn-cancel-work",function(){
    showProcess();
    $("#work-edit-box").html("").hide();
    getWorkList();
    $("#work-view-box").show();
    $(".btn-work-edit").show();
    endProcess();
    return false;
});

$(document).on("click","#btn-save-work",function(){


    var isvld = doFormValidationOnFly("#fwork");

    if(isvld){
        showProcess();
        var pd = $("#fwork").serialize();

        $.post("/profile/updWork",pd,function(r){

            if(r.status){

                $("#work-edit-box").html("").hide();
                getWorkList();
                $("#work-view-box").show();
                $(".btn-work-edit").show();
                endProcess();

            }else{
                swal({

                        text: r.msg,
                        title: r.title,
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        closeOnConfirm: true,
                        showCancelButton: true,
                })
                endProcess();
            }
        },"json")


    }

    return false;
});





