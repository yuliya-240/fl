uploader = new plupload.Uploader({
		runtimes : 'html5,flash,html4',
		browse_button : 'pickfiles', // you can pass an id...
		container: document.getElementById('counteinerpic'), // ... or DOM Element itself
		url : '/img/uploadissueattach',
		unique_names:false,
		multi_selection:true,
		max_file_count: 10,
		filters : {
			max_file_size : '100mb',
			/*mime_types: [
				{title : "Image files", extensions : "jpg,gif,png,jpeg"},
				
			]*/
		},
	
		init: {
			PostInit: function() {
				
			},
	
			FilesAdded: function(up, files) {
				$("#progressSlide").css('width',"0%");
				$("#progress1").show();
				$("#pickfiles").hide();
		
				up.refresh(); // Reposition Flash/Silverlight
				uploader.start();
			},
	
			UploadProgress: function(up, file) {
				$("#progressSlide").css('width',file.percent+"%");
				$("#progress1").data('percent',file.percent+"%");
			},
	
			Error: function(up, err) {
				
				swal({
					title:err.message,
					text:err.code,
					type:"error"
				})
		
				up.refresh(); // Reposition Flash/Silverlight
				
				$("#progress1").hide();
				$("#pickfiles").show();
		
			},
			
			FileUploaded: function(up, file, info) {
				$("#progress1").hide();
				var obj = JSON.parse(info.response);
				if(!obj.status){
					swal({
						title:obj.title,
						text:obj.msg,
						type:"error"
					})
					
				}else{
					$('#attach-files tr:last').after('<tr id="'+obj.id+'"><td style="border-top:1px solid #fff;border-bottom:1px solid #ccc;" width="2%"><i class="material-icons">insert_drive_file</i></td><td style="border-top:1px solid #fff;border-bottom:1px solid #ccc;"><a target="_blank" href="'+obj.fullpath+'">'+obj.cleanFileName+'</a></td><td style="border-top:1px solid #fff;border-bottom:1px solid #ccc;" width="2%"><a href="#" data-id="'+obj.id+'" class="delFile"><i style="color:red;" class="material-icons">close</i></a></td></tr><input type="hidden" name="attachments[]" id="input'+obj.id+'" value="'+obj.cleanFileName+'">');
					$("#pickfiles").show();
					
				}
				
				
			}
		}
});	

function doFormValidation(){
	var validator = $( "#fdata" ).validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		

		
	});
	return validator.form();
}

$(document).ready(function() {

uploader.init();


$('#notes').redactor({
	 
        buttonSource: true,		 
		imageUpload: '/img/redactorUp',
		minHeight: 200
		 
	 });


$("#project-sel").change(function(){
		
		var id = $(this).val();
		if(id=="-1"){
			showProcess();
			window.location.href="/projects/add";
		}
		var sid = parseInt(id);
		var def = parseInt($("#defprj").val());
		
		if(sid==def){
			showProcess();
			$.get('/projects/getDetails',{id:id},function(r){
				
				$("#sel-miles-box").html("");
				$("#miles-box").hide();
				$("#sel-collab-box").html(r.collabs);
				endProcess();
			},"json");
		}else{
			showProcess();
			$.get('/projects/getDetails',{id:id},function(r){
				
				$("#sel-miles-box").html(r.miles);
				$("#miles-box").show();
				$("#sel-collab-box").html(r.collabs);
				endProcess();
			},"json");
		}
	});

$("#project-sel").trigger("change");

});

$(document).on("click",".delFile",function(){
	
	var id=$(this).data("id");
	
	$("#"+id).remove();
	$("#input"+id).remove();
	
	return false;
});

$(document).on("click","#doSave",function(){
	
	var isvalid = doFormValidation();
	
	if(isvalid){
		showProcess();
		var pd = $("#fdata").serialize();
		$.post('/issues/doAdd',pd,function(r){
			
			window.location.href="/issues";
			
		},"json");
	}
	
	return false;
});

$(document).on("click",".tbl-pointer",function(){
	
	showProcess();
	var id=$(this).data("id");
	window.location.href="/issues/view/"+id;
	
	return false;
});

var wstart =  $("#weekstart").val();

var tf = $("#tformat").val();

if(tf=="H:i"){
	
	var meridian = false;
}else{
	var meridian = true;
}

$(".date-picker").datepicker({
	autoclose: true,
	todayHighlight: true,
	weekStart: wstart,
	readOnly: true
});

$('.time-picker').timepicker({
    template: false,
    showSeconds: false,
    showMeridian: meridian,    
    showInputs: false,
    minuteStep: 1
});
