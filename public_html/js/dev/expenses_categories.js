
function getContent(p) {
    $.get('/expenses/categories/getContent', {p: p}, function (r) {
        $("#d-content").html(r.html);
        $(".list-group-item").hover(function(){
        var id=$(this).data("id");
            $("#li"+id).css("background", "1px solid #ddd");
        });
        endProcess();
    }, "json");

}

$(document).ready(function () {
    showProcess();
    getContent(1);


});

$(document).on("click",".btn-n-c",function(){

	$('#new-cat-box').modal('show');
    $("#doSave").show();
    $("#doEdit").hide();
	return false;
});

$(document).on("click",".n-s-c",function(){
    var id = $(this).data("id");
    $.get("/expenses/categories/viewAddSubCat", {id: id}, function(r){
        $("#issub").val(1);
        $("#catlist-sel").html(r.html);
        $("#catlist-box").show();
        $('#new-cat-box').modal('show');

    },"json");


	return false;
});

//$(document).on("click", "#editSubCat",function(){
//    var id = $(this).data("id");
//    $.get("/expenses/categories/viewAddSubCat", {id: id}, function(r){
//        $("#issub").val(1);
//        $("#catlist-sel").html(r.html);
//        $("#catlist-box").show();
//        $('#new-cat-box').modal('show');
//
//    },"json");
//
//
//    return false;
//});
$(document).on("click", "#editCat",function(){
    var id = $(this).data("id");
    $.get("/expenses/categories/editCat", {id: id}, function(r){
        $(".edit-cat").html(r.html);
        $('#new-cat-box').modal('show');
        $("#doSave").hide();
        $("#doEdit").show();

    },"json");
    return false;
});

$(document).on('click', '#doEdit', function(){
    var pd = $('#fdata').serialize();
    $.post("/expenses/categories/doUpdate",pd, function(r){
        $('#new-cat-box').modal('hide');
        var sub = r.issub;
        if(sub){
            $("#li"+r.id).html(
            '<div class="col-xs-1" style="padding-right: 0; padding-top: 10px;">'+
            '<span class="s7-angle-right" style="font-size: 25px;"></span></div>'+
            '<div class="col-xs-11" style="padding-left: 0; padding-top: 3px;">'+
            '<a class="list-group-item " data-id="'+ r.id+'" >'+ r.name+
            '<span class="pull-right s7-trash" id="delSubCat" data-id="'+r.id+'" style="font-size: 20px; color: red;"></span>'+
            '<span class="pull-right s7-note" id="editCat" data-id="'+ r.id+'" style="font-size: 20px; color: #0000ff;"></span>'+
            '</a></div>');}
        else{
            getContent();
        }
    },"json");
});


$(document).on('click', '#doSave', function(){
    var pd = $('#fdata').serialize();
    $.post("/expenses/categories/doAdd",pd, function(r){
        $('#new-cat-box').modal('hide');
        var sub = r.issub;
        if(sub){
        $("#subcatlist"+r.id).append(' <div id="li'+ r.id+'">'+
       '<div class="col-xs-1" style="padding-right: 0; padding-top: 10px;">'+
        '<span class="s7-angle-right" style="font-size: 25px;"></span></div>'+
        '<div class="col-xs-11" style="padding-left: 0; padding-top: 3px;">'+
        '<a class="list-group-item " data-id="'+ r.id+'" >'+ r.name+
    '<span class="pull-right s7-trash" id="delSubCat" data-id="'+r.id+'" style="font-size: 20px; color: red;"></span>'+
    '<span class="pull-right s7-note" id="editCat" data-id="'+ r.id+'" style="font-size: 20px; color: #0000ff;"></span>'+
    '</a></div></div>');}
        else{
            getContent();
        }
    },"json");
});

$('#new-cat-box').on('hidden.bs.modal', function () {
    $("#issub").val(0);
    $("#catlist-box").hide();
    //$("#cat_name").empty();
    $('#cat_name').val('');
});

$(document).on("click", "#delSubCat", function(){
    var id=$(this).data('id');
    $.post("/expenses/categories/delSubCat", {id: id}, function () {
        $("#li"+id).remove();

    }, "json");
});

$(document).on("click", "#delCat", function(){
    var id=$(this).data('id');
    $.post("/expenses/categories/delCat", {id: id}, function () {
        $("#ac"+id).remove();
        //getContent();
    }, "json");
});
