function showProcess(){
	$("#process-loader").addClass("in");
};


function endProcess(){
	$("#process-loader").removeClass("in");
	
}

	    
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
      	
	$("#regForm").validate({
		  rules: {
		    
		    email: {
		      required: true,
		      email: true
		    },
		  },
		  errorLabelContainer: "#error-box",
		
		submitHandler: function() { 	
			showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/signup/doSignup', 
				data:$('#regForm').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						window.location.href=r.url;
					}else{
						$("#email").val('');
	
						endProcess();
						swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
						
						
					}
				}
			
			});
			
		}
			
	});

      	
      });
