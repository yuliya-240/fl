function doTransFormValidation(){
	
	var validator = $( "#ftrans").validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
	});
	return validator.form();
}

function doBillingAddrFormValidation(){
	
	var validator = $( "#fbilladdr").validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
	});
	return validator.form();
}

function doEmailFormValidation(){
	
	var validator = $( "#femail").validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
	});
	return validator.form();
}

function doMyBillingAddrFormValidation(){
	
	var validator = $( "#fmybilladdr").validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
	});
	return validator.form();
}


$(document).ready(function(){
	var wstart =  $("#weekstart").val();
	$(".date-picker").datepicker({
		autoclose: true,
		todayHighlight: true,
									/*format : "M dd, yyyy",*/
		weekStart: wstart,
		readOnly: true
	});	
	$("time.timeago").timeago();
	$('.venobox').venobox(); 
	
	$(".dec").livequery(function(){
	
	   
	    $(this).mask('0999999999.09', {
		   	maxlength: false,
		});
    }); 	
    
    $(".xr").livequery(function(){
	
	   
	    $(this).mask('0999999999.0999', {
		   	maxlength: false,
		   	
		   	onChange: function(cep){
		   		var xrate = parseFloat($("#pay_rate").val());
		   		var am = parseFloat($("#pay_amount").val());
		   		var baseam = am * xrate;
		   		$("#pay_due_base").val(baseam.toFixed(2));
			},
			onEmpty : function(){
				$("#pay_due_base").val(0);
			}
		   	
		});
    }); 	

	
});

$(document).on("click",".del",function(){
	$("#act-box").dropdown('toggle');
	 var id= $("#inv_id").val();
	 var title = $("#_idt").val();
	 var msg = $("#_idm").val();
	 var yb = $("#_yb").val();
	 var cb = $("#_cb").val();
	 
	 swal({
		 title:title,
		 text:msg,
		 type: "warning",
		 showCancelButton: true,
		 confirmButtonColor: "#DD6B55",
		 confirmButtonText: yb,
		 cancelButtonText: cb,
	 },function(result){
	 	if(result){
			 showProcess();
			 $.post('/invoices/delete',{id:id},function(r){
				 window.location.href="/invoices";
			 },"json");
		 	
	 	}
	 })
	
	
	return false;
});

$(document).on("click",".copy",function(){
	$("#act-box").dropdown('toggle');
	var id = $("#inv_id").val();
	var cptit = $("#_copy_tit").val();
	var cptxt = $("#_copy_msg").val();
	 var yb = $("#_yb").val();
	 var cb = $("#_cb").val();
	
	
	swal({
		
		text: cptxt,
		title:cptit,
		type: "warning",
		confirmButtonText: yb,
		cancelButtonText: cb,
		confirmButtonColor: "#4e91ff",
		closeOnConfirm: true,
		showCancelButton: true,},
		function(){
			showProcess();	
			window.location.href="/invoices/copy/"+id;
	});	
	
	return false;
});

$(document).on("click",".arc",function(){
	$("#act-box").dropdown('toggle');
	var id = $("#inv_id").val();
	var arctit = $("#_arc_tit").val();
	var arctxt = $("#_arc_msg").val();
	var yb = $("#_yb").val();
	var cb = $("#_cb").val();

	swal({
		
		text: arctxt,
		title:arctit,
		type: "warning",
		confirmButtonText: yb,
		cancelButtonText: cb,
		confirmButtonColor: "#4e91ff",
		closeOnConfirm: true,
		showCancelButton: true,},
		function(){
				
			showProcess();	
			$.post('/invoices/doarc',{id:id,act:1},function(){
					
				window.location.href="/invoices/view/"+id;
					
			},"json")
				
	});	

	return false;
});

$(document).on("click",".unarc",function(){
	$("#act-box").dropdown('toggle');
	var id = $("#inv_id").val();
	var arctit = $("#_uarc_tit").val();
	var arctxt = $("#_uarc_msg").val();
	var yb = $("#_yb").val();
	var cb = $("#_cb").val();

	swal({
		
		text: arctxt,
		title:arctit,
		type: "warning",
		confirmButtonText: yb,
		cancelButtonText: cb,
		confirmButtonColor: "#6fb3e0",
		closeOnConfirm: true,
		showCancelButton: true,},
		function(){
				
			showProcess();	
			$.post('/invoices/doarc',{id:id,act:0},function(){
					
				window.location.href="/invoices/view/"+id;
					
			},"json")
				
	});	

	return false;
});

$(document).on("click",".pay",function(){
	
	$('#pay-box').modal('show');
	
	return false;
});

$(document).on("click","#doAddTrans",function(){
	
	var isValid = doTransFormValidation();
	
	if(isValid){
		
		
		var due = parseFloat($("#pay_due").val());
		var amount = parseFloat($("#pay_amount").val());

		if(amount>due){
				
			$(".pay-err").hide();
			$("#more-due").show();
			
			return false;
		}
		
		var pd = $("#ftrans").serialize();
		

		$.post("/transactions/inc/doAddInvoice",pd,function(r){

			showProcess();
			$('#pay-box').modal('hide');
			window.location.href="/invoices/view/"+r.invid;
		},"json")
		

		
	}
	
	return false;
});

$('#pay-box').on('hidden.bs.modal', function () {
    
	$(".pay-err").hide();
	$("#errpayamnt").hide();

});

$(document).on('click','#payinfull',function(){
		
		var a = $(this).prop("checked");
		
		if ( a ){
	       var amount = $("#pay_due").val();
	    
	       $('#pay_amount').val(amount);
	       $('#pay_amount').prop('readonly',true);
	    } else {
	       
	       $('#pay_amount').val('');
	       $('#pay_amount').prop('readonly',false);
	
	    }
		
	});

$(document).on("click",".sendemail",function(){
	
	$('#send-box').modal('show');
	
	return false;
});

$(document).on("click","#chAddr",function(){
	
	showProcess();
	
	
	var id=$("#client").val();
	$.get("/invoices/getBillingAddr",{id:id},function(r){
		
		$("#bill-adr-box").html(r.html);
		
		endProcess();
		
	},"json");
	
	$('#addr-box').modal('show');
	
	return false;
	
});

$(document).on("click",".doChAddr",function(){
	
	
	var isValid = doBillingAddrFormValidation();
	
	if(isValid){
		
		showProcess();
		var pd = $("#fbilladdr").serialize();
		var inv = $("#inv_id").val();
		var ps = pd+"&invid="+inv;
		$.post("/invoices/doChAddrView",ps,function(r){
			
			$("#recipient-addr").html(r.addr);

			$('#addr-box').modal('hide') ;
			endProcess();
			
		},"json");
	}
	
	return false;
	
	
});

$(document).on("click","#chMyAddr",function(){
	
	showProcess();
	
	$.get("/invoices/getMyBillingAddr",function(r){
		
		$("#my-bill-adr-box").html(r.html);
		
		endProcess();
		
	},"json");
	
	$('#my-addr-box').modal('show');
	
	return false;
	
});

$(document).on("click",".doChMyAddr",function(){
	var isValid = doMyBillingAddrFormValidation();
	
	if(isValid){
		
		showProcess();
		var pd = $("#fmybilladdr").serialize();
		var inv = $("#inv_id").val();
		var ps = pd+"&invid="+inv;
		$.post("/invoices/doChMyAddrView",ps,function(r){
			
			$("#inv-my-addr").html(r.addr);
			$('#my-addr-box').modal('hide') ;
			endProcess();
			
		},"json");
	}
	
	return false;
	
	
});

$(document).on("change","#country2",function(){
	var strf ="countryID="+$(this).val();
	$.post('/misc/changeCountry',strf,function(data){ 
	 
	  if(!data.count){
	  	$('#region2').hide();
	  	$('#region2').attr('name','');
	  	$('#inputstate2').attr('name','billing_state');
	  	$('#inputstate2').show();
	  	
	  }else{
	  	$('#inputstate2').hide();
	  	$('#inputstate2').attr('name','');
	  	$('#region2').show();
	  	$('#region2').attr('name','billing_state');
	  	$('#region2').html(data.list);
	
	  }
				
	},"json");    
	return false;
        	
});

$(document).on("click","#doSend",function(){
	
	var isValid = doEmailFormValidation();
	
	if(isValid){
		showProcess();
		var pd = $("#femail").serialize();
		
		$.post("/invoices/doEmail",pd,function(r){
			
			if(!r.status){
				
				swal({
					
					title:r.title,
					text:r.msg,
					type:"error"
				});
				
				endProcess();
			}else{
				$('#send-box').modal('hide');
				swal({
					
					title:r.title,
					text:r.msg,
					type:"success"
				},function(){
					
					window.location.href="/invoices/view/"+r.id;
				});
				
			}
			
		},"json")
	}
	
	return false;
	
});


