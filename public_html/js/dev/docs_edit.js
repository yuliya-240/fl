var ps = $("#_ps").val();
var achs = $("#_achs").val();
var chns = $("#_chns").val();
var lng = $("#lang").val();
var tcb = $("#_tcb").val();
var avc = $("#_avc").val();
var selc = $("#_selc").val();

window.setInterval(function(){
	var text = CKEDITOR.instances.txt.getData();
	var id = $("#doc_id").val();
	$("#save-status").removeClass("red").removeClass("text-muted").addClass("green").text(ps);
	$.post("/doc/doUpdateTxt",{txt:text,id:id},function(r){
		
		$("#save-status").removeClass("red").removeClass("green").addClass("text-muted").text(achs);
		
	},"json")
	
}, 10000);

$.fn.editable.defaults.mode = 'inline';

$(document).ready(function() {
	var editorElem = document.getElementById("txt");
	
	$('#doc_name').editable();
	
	CKEDITOR.replace( 'txt', {
		// Define the toolbar: http://docs.ckeditor.com/#!/guide/dev_toolbar
		// The full preset from CDN which we used as a base provides more features than we need.
		// Also by default it comes with a 3-line toolbar. Here we put all buttons in a single row.
		/* on : {
		         // maximize the editor on startup
		         'instanceReady' : function( evt ) {
		            evt.editor.execCommand( 'maximize' );
		         }
		},*/
		
		on: {
			'instanceReady' :function() { 
	        var textEditHeight      = $("#area").height();
	        var ckTopHeight         = $("#cke_1_top").height();
	        var ckContentsHeight    = $("#cke_1_contents").height();
	
	        for (var i = 1; i < 10; i++) {
	            $("#cke_"+i+"_contents").height( (textEditHeight - ckTopHeight - 10) + "px");
	
	        }
	    	}
		},
	
		toolbar: [
		
			{ name: 'clipboard', items: [ 'Undo', 'Redo' ] },
			{ name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
			{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat' ] },
			{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
			{ name: 'links', items: [ 'Link', 'Unlink' ] },
			{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
			{ name: 'insert', items: [ 'Image', 'Table' ] },

		],
		// Since we define all configuration options here, let's instruct CKEditor to not load config.js which it does by default.
		// One HTTP request less will result in a faster startup time.
		// For more information check http://docs.ckeditor.com/#!/api/CKEDITOR.config-cfg-customConfig
		customConfig: '',
		language: lng,
		// Sometimes applications that convert HTML to PDF prefer setting image width through attributes instead of CSS styles.
		// For more information check:
		//  - About Advanced Content Filter: http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
		//  - About Disallowed Content: http://docs.ckeditor.com/#!/guide/dev_disallowed_content
		//  - About Allowed Content: http://docs.ckeditor.com/#!/guide/dev_allowed_content_rules
		disallowedContent: 'img{width,height,float}',
		extraAllowedContent: 'img[width,height,align]',
		// Enabling extra plugins, available in the full-all preset: http://ckeditor.com/presets-all
		extraPlugins: 'tableresize,uploadimage,uploadfile,image2',
			// Upload images to a CKFinder connector (note that the response type is set to JSON).
		uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

		// Configure your file manager integration. This example uses CKFinder 3 for PHP.
	//	filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
	//	filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
		filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

					// The following options are not necessary and are used here for presentation purposes only.
					// They configure the Styles drop-down list and widgets to use classes.

			
		/*********************** File management support ***********************/
		// In order to turn on support for file uploads, CKEditor has to be configured to use some server side
		// solution with file upload/management capabilities, like for example CKFinder.
		// For more information see http://docs.ckeditor.com/#!/guide/dev_ckfinder_integration
		// Uncomment and correct these lines after you setup your local CKFinder instance.
		// filebrowserBrowseUrl: 'http://example.com/ckfinder/ckfinder.html',
		// filebrowserUploadUrl: 'http://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		/*********************** File management support ***********************/
		// Make the editing area bigger than default.
		// An array of stylesheets to style the WYSIWYG area.
		// Note: it is recommended to keep your own styles in a separate file in order to make future updates painless.
		contentsCss: [ 'https://cdn.ckeditor.com/4.6.1/full-all/contents.css', '/css/mystyles.css?v=3' ],
		// This is optional, but will let us define multiple different styles for multiple editors using the same CSS file.
		bodyClass: 'document-editor',
		// Reduce the list of block elements listed in the Format dropdown to the most commonly used.
		format_tags: 'p;h1;h2;h3;pre',
		// Simplify the Image and Link dialog windows. The "Advanced" tab is not needed in most cases.
		removeDialogTabs: 'image:advanced;link:advanced',
		// Define the list of styles which should be available in the Styles dropdown list.
		// If the "class" attribute is used to style an element, make sure to define the style for the class in "mystyles.css"
		// (and on your website so that it rendered in the same way).
		// Note: by default CKEditor looks for styles.js file. Defining stylesSet inline (as below) stops CKEditor from loading
		// that file, which means one HTTP request less (and a faster startup).
		// For more information see http://docs.ckeditor.com/#!/guide/dev_styles
		stylesSet: [
			/* Inline Styles */
			{ name: 'Marker', element: 'span', attributes: { 'class': 'marker' } },
			{ name: 'Cited Work', element: 'cite' },
			{ name: 'Inline Quotation', element: 'q' },
			/* Object Styles */
			{
				name: 'Special Container',
				element: 'div',
				styles: {
					padding: '5px 10px',
					background: '#eee',
					border: '1px solid #ccc'
				}
			},
			{
				name: 'Compact table',
				element: 'table',
				attributes: {
					cellpadding: '5',
					cellspacing: '0',
					border: '1',
					bordercolor: '#ccc'
				},
				styles: {
					'border-collapse': 'collapse'
				}
			},
			{ name: 'Borderless Table', element: 'table', styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
			{ name: 'Square Bulleted List', element: 'ul', styles: { 'list-style-type': 'square' } }
		]
	} );

	CKEDITOR.instances.txt.on('change', function() { 
    $("#save-status").removeClass("text-muted").addClass("red").text(chns);
});	

	$('#emplist').multiSelect({
		selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='"+tcb+"'>",
		selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='"+tcb+"'>",
		selectableFooter: "<div class='ms-footer'>"+avc+"</div>",
		selectionFooter: "<div class='ms-footer'>"+selc+"</div>",
		  afterInit: function(ms){
		    var that = this,
		        $selectableSearch = that.$selectableUl.prev(),
		        $selectionSearch = that.$selectionUl.prev(),
		        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
		        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
		
		    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
		    .on('keydown', function(e){
		      if (e.which === 40){
		        that.$selectableUl.focus();
		        return false;
		      }
		    });
		
		    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
		    .on('keydown', function(e){
		      if (e.which == 40){
		        that.$selectionUl.focus();
		        return false;
		      }
		    });
		  },
		  afterSelect: function(values){
		    this.qs1.cache();
		    this.qs2.cache();
		    
		    
		  },
		  afterDeselect: function(values){
		    this.qs1.cache();
		    this.qs2.cache();
		    
		  }	  
	});
		


});


$(document).on("click",".close-doc",function(){
	
	showProcess();
	
	window.location.href="/doc";
	
	return false;
});

$(document).on("click",".del",function(){
	$(".dropdowndoc").dropdown('toggle');
	 var id= $("#doc_id").val();
	 var title = $("#_idt").val();
	 var msg = $("#_idm").val();
	 var yb = $("#_yb").val();
	 var cb = $("#_cb").val();
	 
	 swal({
		 title:title,
		 text:msg,
		 type: "warning",
		 showCancelButton: true,
		 confirmButtonColor: "#DD6B55",
		 confirmButtonText: yb,
		 cancelButtonText: cb,
	 },function(result){
	 	if(result){
			 showProcess();
			 $.post('/doc/delete',{id:id},function(r){
				 window.location.href="/doc";
			 },"json");
		 	
	 	}
	 })
	
	
	return false;
});

$(document).on("click",".savedoc",function(){
	var text = CKEDITOR.instances.txt.getData();
	var id = $("#doc_id").val();
	$("#save-status").removeClass("red").removeClass("text-muted").addClass("green").text(ps);
	$.post("/doc/doUpdateTxt",{txt:text,id:id},function(r){
		
		$("#save-status").removeClass("red").removeClass("green").addClass("text-muted").text(achs);
		notify(r.msg,'inverse');
	},"json")
	
	
	return false;
});

$(document).on("click",".share",function(){
	
	$(".dropdowndoc").dropdown('toggle');
	$('#share-box').modal('show');
	
	return false;
});

$(document).on("click",".doShare",function(){
	
	showProcess();
	
	
	var pd = $("#fcollab").serialize();
	
	$.post("/doc/doShare",pd,function(r){
		
		$('#share-box').modal('hide') ;
		endProcess();
		
	},"json");
	
	
	return false;
});




