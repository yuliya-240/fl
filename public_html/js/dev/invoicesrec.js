var mf = $("#moment-format").val();

function getContent(searchstr){

	$.get('/invoices/recurring/getContent',{query:searchstr},function(r){
		
		$("#d-content").html(r.html);
			
		var c = parseInt(r.count);
		if(c>0){
			
			$("#megafooter").html(r.footer);
		}
		
		endProcess();
	},"json");
}

$(document).ready(function() {
	
	var ss = $('#s').val();
	getContent(ss);
	
	$('#s').keyup(function(e){
		
		  if ( e.which == 13 ) {
		  	e.preventDefault();
		  }
		
		var searchstr = $(this).val();
		
		/*if(searchstr.length){
			
			$("#delsearch").show();
		}else{
			$("#delsearch").hide();
		}
		*/
		
		getContent(searchstr);
	});
	

	
	
});



$(document).on("click","#bundleDelete",function(){

	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_pdt").val();
	var msg = $("#_pdm").val();
	var yes = $("#_yes").val();

	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true,
			showCancelButton: true,},
			function(){
					
				showProcess();	
				$.ajax({
						type:'POST', 
						url: '/invoices/deleteBundle', 
						data:$("#flist").serialize(), 
						dataType: 'json',
						success: function(r) {
						
							var s = $("#s").val();
							getContent(s);


						}
				});					
			});	
	}
	return false;
});     	




$(document).on("click",".prevp, .nextp",function(){
	
	showProcess();
	var p = $(this).data("page");
	$.post('/invoices/recurring/changePage',{p:p},function(r){
		
			var s = $("#s").val();
			getContent(s);
		
	},"json");
	
	return false;
});
