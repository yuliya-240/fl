function getContent(p){
	
		$.get('/services/getContent',{p:p},function(r){
			
			$("#d-content").html(r.html);
			$("#megafooter").html(r.footer);
			endProcess();
			var c = parseInt(r.count);
			if(c){
				
				$("#megafooter").show();
			}else{
				
				$("#megafooter").hide();
			}
		},"json");
	
}

$(document).ready(function() {
	
	getContent(1);
});

 $(document).on("click","#bundleDelete",function(){


	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_bdt").val();
	var msg = $("#_bdm").val();
	var yes = $("#_yes").val();

	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true,
			showCancelButton: true,},
			function(){
					
				showProcess();	
				$.ajax({
						type:'POST', 
						url: '/services/deleteBundle', 
						data:$("#flist").serialize(), 
						dataType: 'json',
						success: function(r) {
						
								getContent(1);

						}
				});					
			});	
	}
	return false;
});     	
    	


