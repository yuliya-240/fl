function doTransFormValidation(){
	
	var validator = $( "#ftrans").validate({
		ignore: "input[type='text']:hidden",
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
	});
	return validator.form();
}


$(document).ready(function() {
	
	var wstart =  $("#weekstart").val();
	$(".date-picker").datepicker({
		autoclose: true,
		todayHighlight: true,
									/*format : "M dd, yyyy",*/
		weekStart: wstart,
		readOnly: true
	});	
	   
    $(".dec").mask('0999999999.09', {
	   	maxlength: false,
	});
 	
   
	
	$(".xr").livequery(function(){  
		var id = $(this).data("id");
	    $(this).mask('0999999999.0999', {
		   	maxlength: false,
		   	
		   	onChange: function(cep){

		   		var xrate = parseFloat($("#pay_rate"+id).val());
		   		var am = parseFloat($("#pay_amount"+id).val());
		   		var baseam = am * xrate;
		   		$("#pay_due_base"+id).val(baseam.toFixed(2));
			},
			onEmpty : function(){
					var id = $(this).data("id");
				$("#pay_due_base"+id).val(0);
			}
		   	
		});
    })
	
	
});

$(document).on("click",".btn-del-pay",function(){
	
	var id = $(this).data("id");
	
	$("#pay-box"+id).remove();
	
	return false;
	
});

$(document).on('click','.payinfull',function(){
		
	var a = $(this).prop("checked");
	var id = $(this).data("id");
	
	if ( a ){
       var amount = $("#pay_due"+id).val();
    
       $('#pay_amount'+id).val(amount);
       $('#pay_amount'+id).prop('readonly',true);
    } else {
       
       $('#pay_amount'+id).val('');
       $('#pay_amount'+id).prop('readonly',false);

    }
		
});

$(document).on("click","#doPayAll",function(r){
	var isValid = doTransFormValidation();
	
	if(isValid){
		showProcess();
		var pd= $("#ftrans").serialize();
		
		$.post("/transactions/inc/doBulkPay",pd,function(r){
			
			window.location.href = "/invoices";
			
		},"json")
			
	}
	
	return false;
})