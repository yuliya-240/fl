function getView(){
	var id = $("#issue_id").val();
	$.get("/issues/getView",{id:id},function(r){
		
		$("#i-content").html(r.html);
		
		$("time.timeago").timeago();

$('#comment').redactor({
	 
        buttonSource: true,		 
		imageUpload: '/img/redactorUp',
		minHeight: 200
		 
	 });

		
		endProcess();
		
		
		
	},"json");
}

function doFormValidation(){
	var validator = $( "#feditissue" ).validate({
				  errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		

		
	});
	return validator.form();
}

$(document).ready(function() {

	getView();

});

$(document).on("click","#doSave",function(){
	showProcess();
	var pd = $("#fcomment").serialize();
	
	$.post('/issues/doAddComment',pd,function(r){
		
		if(!r.status){
			endProcess();
			swal({
				title:r.title,
				text:r.msg,
				type:"error"
			})	
		}else{
			
			getView();
			endProcess();
		}
		
		//window.location.href="#last";
	},"json");
	
	return false;
});

$(document).on("change","#i-status",function(){
	
	showProcess();
	var status = $(this).val();
	var id =$("#issue_id").val();
	
	$.post("/issues/changeStatus",{status:status,id:id},function(r){
		
		getView();
		endProcess();
		//window.location.href="#last";
	},"json");
	
});

$(document).on("click","#i-close",function(){
	showProcess();
	var status = 50;
	var id =$("#issue_id").val();
	
	$.post("/issues/changeStatus",{status:status,id:id},function(r){
		
		getView();
		$("#i-close").hide();
		$("#foption").hide();
		$("#i-open").show();
		endProcess();
		//window.location.href="#last";
	},"json");
	
	return false;
});

$(document).on("click","#i-open",function(){
	showProcess();
	var status = 10;
	var id =$("#issue_id").val();
	
	$.post("/issues/changeStatus",{status:status,id:id},function(r){
		
		getView();
		$("#i-open").hide();
		$("#i-close").show();
		$("#foption").show();
		endProcess();
		//window.location.href="#last";
	},"json");
	
	return false;
});

//Assignee


$(document).on("click","#i-ch-assign",function(){
	
	$("#edit-assign-link").hide();
	$("#cancel-assign-select").show();
	$("#i-assign-user").hide();
	
	$("#i-ch-priority-sel").trigger("click");
	$("#i-ch-category-sel").trigger("click");
	$("#i-ch-project-sel").trigger("click");
	$("#btn-deadline-cancel").trigger("click");
	var prj = $("#prj-id").val();
	var iid =$("#issue_id").val();
	$.get("/issues/getAssignSel",{prj:prj,iid:iid},function(r){
		$("#i-assign-user-sel").html(r.html);
		$("#i-assign-user-sel").show();
	},"json");
	
	return false;
});

$(document).on("click","#i-ch-assign-sel",function(){
	$("#cancel-assign-select").hide();
	$("#edit-assign-link").show();
	$("#i-assign-user").show();
	$("#i-assign-user-sel").hide();
	
	return false;
});

$(document).on("change","#i-assign-user-sel",function(){
	
	showProcess();
	var assign = $(this).val();
	var id =$("#issue_id").val();
	
	$.post("/issues/changeAssign",{assign:assign,id:id},function(r){
		
		$("#i-assign-user").html(r.img);
		$("#cancel-assign-select").hide();
		$("#edit-assign-link").show();
		$("#i-assign-user").show();
		$("#i-assign-user-sel").hide();
	
		
		getView();
		endProcess();
		//window.location.href="#last";
	},"json");
	
	
	
});

// Deadline


$(document).on("click","#i-ch-deadline",function(){
	
	$("#edit-deadline-link").hide();
	$("#cancel-deadline-select").show();
	$("#i-deadline-date").hide();
	
	$("#i-ch-priority-sel").trigger("click");
	$("#i-ch-category-sel").trigger("click");
	$("#i-ch-project-sel").trigger("click");
	$("#i-ch-assign-sel").trigger("click");

	$("#i-deadline-edit-box").show();
	return false;
});


$(document).on("click","#btn-deadline-cancel",function(){

		
	$("#i-deadline-edit-box").hide();
	$("#edit-deadline-link").show();
	$("#i-deadline-date").show();
	return false;
});


$(document).on("click","#btn-deadline-save",function(){
	showProcess();
	var date = $("#deadline_date").val();
	var time = $("#deadline_time").val();
	var id=$("#issue_id").val();
	var status = $("#issue_status").val();
	
	$.post("/issues/updateDeadline",{id:id,date:date,time:time,status:status},function(r){
		
		$("#i-deadline-edit-box").hide();
		$("#edit-deadline-link").show();
		$("#i-deadline-date").html(r.html).show();
		getView();
		endProcess();		
		
	},"json");
	
	
	return false;
	
	
})

//priority

$(document).on("click","#i-ch-priority",function(){
	
	$("#edit-priority-link").hide();
	$("#cancel-priority-select").show();
	$("#i-priority").hide();
	$("#i-sel-priority-box").show();
	
	$("#i-ch-assign-sel").trigger("click");
	$("#i-ch-category-sel").trigger("click");
	$("#i-ch-project-sel").trigger("click");
	$("#btn-deadline-cancel").trigger("click");
	
	return false;
});

$(document).on("click","#i-ch-priority-sel",function(){
	$("#cancel-priority-select").hide();
	$("#edit-priority-link").show();
	$("#i-priority").show();
	$("#i-sel-priority-box").hide();
	
	return false;
});

$(document).on("change","#i-sel-priority-box",function(){
	
	showProcess();
	var p = $(this).val();
	var id =$("#issue_id").val();
	
	$.post("/issues/changePriority",{p:p,id:id},function(r){
		
		$("#cancel-priority-select").hide();
		$("#edit-priority-link").show();
		$("#i-priority").show();
		$("#i-sel-priority-box").hide();
		$("#i-priority").html(r.html);
		
		getView();
		endProcess();
		//window.location.href="#last";
	},"json");
	
	
	
});

//caregory
$(document).on("click","#i-ch-category",function(){
	
	$("#edit-category-link").hide();
	$("#cancel-category-select").show();
	$("#i-category").hide();
	$("#i-sel-category-box").show();

	$("#i-ch-priority-sel").trigger("click");
	$("#i-ch-assign-sel").trigger("click");
	$("#i-ch-project-sel").trigger("click");
	$("#btn-deadline-cancel").trigger("click");

	return false;
});

$(document).on("click","#i-ch-category-sel",function(){
	$("#cancel-category-select").hide();
	$("#edit-category-link").show();
	$("#i-category").show();
	$("#i-sel-category-box").hide();
	
	return false;
});

$(document).on("change","#i-sel-category-box",function(){
	
	showProcess();
	var c = $(this).val();
	var id =$("#issue_id").val();
	
	$.post("/issues/changeCategory",{c:c,id:id},function(r){
		
		$("#cancel-category-select").hide();
		$("#edit-category-link").show();
		$("#i-category").show();
		$("#i-sel-category-box").hide();
		$("#i-category").html(r.html);
		
		getView();
		endProcess();
		//window.location.href="#last";
	},"json");
	
});

//projects
$(document).on("click","#i-ch-project",function(){
	
	$("#edit-project-link").hide();
	$("#cancel-project-select").show();
	$("#i-project").hide();
	$("#i-sel-project-box").show();

	$("#i-ch-priority-sel").trigger("click");
	$("#i-ch-category-sel").trigger("click");
	$("#i-ch-assign-sel").trigger("click");
	$("#btn-deadline-cancel").trigger("click");

	return false;
});

$(document).on("click","#i-ch-project-sel",function(){
	$("#cancel-project-select").hide();
	$("#edit-project-link").show();
	$("#i-project").show();
	$("#i-sel-project-box").hide();
	
	return false;
});

$(document).on("change","#i-sel-project-box",function(){
	
	showProcess();
	var p = $(this).val();
	var id =$("#issue_id").val();
	
	$.post("/issues/changePrj",{p:p,id:id},function(r){
		
		$("#cancel-project-select").hide();
		$("#edit-project-link").show();
		$("#i-project").show();
		$("#i-sel-project-box").hide();
		$("#i-project").html(r.html);
		$("#i-miles").html(r.miles_show);
		$("#i-sel-miles-box").html(r.miles);
		getView();
		endProcess();
		//window.location.href="#last";
	},"json");
});


// Milestones

$(document).on("click","#i-ch-miles",function(){
	
	$("#edit-miles-link").hide();
	$("#cancel-miles-select").show();
	$("#i-miles").hide();
	$("#i-sel-miles-box").show();

	$("#i-ch-priority-sel").trigger("click");
	$("#i-ch-category-sel").trigger("click");
	$("#i-ch-assign-sel").trigger("click");
	$("#i-ch-project-sel").trigger("click");
	$("#btn-deadline-cancel").trigger("click");
	return false;
});

$(document).on("click","#i-ch-miles-sel",function(){
	$("#cancel-miles-select").hide();
	$("#edit-miles-link").show();
	$("#i-miles").show();
	$("#i-sel-miles-box").hide();
	
	return false;
});

$(document).on("change","#i-sel-miles-box",function(){
	
	showProcess();
	var m = $(this).val();
	var id =$("#issue_id").val();
	
	$.post("/issues/changeMiles",{m:m,id:id},function(r){
		
		$("#cancel-miles-select").hide();
		$("#edit-miles-link").show();
		$("#i-miles").html(r.html);
		$("#i-miles").show();
		$("#i-sel-miles-box").hide();

		
		getView();
		endProcess();
		//window.location.href="#last";
	},"json");
});

// DELETE

$(document).on("click",".del",function(){
	
	 var id= $("#issue_id").val();
	 var title = $("#_idt").val();
	 var msg = $("#_idm").val();
	 var yb = $("#_yb").val();
	 var cb = $("#_cb").val();
	 
	 swal({
		 title:title,
		 text:msg,
		 type: "warning",
		 showCancelButton: true,
		 confirmButtonColor: "#DD6B55",
		 confirmButtonText: yb,
		 cancelButtonText: cb,
	 },function(result){
	 	if(result){
			 showProcess();
			 $.post('/issues/delete',{id:id},function(r){
				 window.location.href="/issues";
			 },"json");
		 	
	 	}
	 })
	
	
	return false;
});

$(document).on("click","#edit-issue",function(){
	
	var id = $("#issue_id").val();
	
	$.get("/issues/getTextIssue",{id:id},function(r){
		
		$("#issue-edit-subj").val(r.subj);
		$("#issue-edit-text").val(r.text);
		
		$('#issue-edit-text').redactor({
	 
	        buttonSource: true,		 
			imageUpload: '/img/redactorUp',
			minHeight: 200
		 
		});
		
		
	},"json");
	
	$("#issue-text").hide();
	$("#edit-issue-box").show();
	
	return false;
});

$(document).on("click","#cancel-edit-issue",function(){
	
	$("#edit-issue-box").hide();
	$("#issue-text").show();
	
	$('#issue-edit-text').redactor('core.destroy');
	return false;
});

$(document).on("click","#doSaveEditIssue",function(){
	
	var isvalid = doFormValidation();
	
	if(isvalid){
		showProcess();
		var pd = $("#feditissue").serialize();
		var id = $("#issue_id").val();
		var ps = pd+"&id="+id;
		
		
		$.post('/issues/doUpdate',ps,function(r){
			
			getView();
			
		},"json");
	}
	
	return false;
});



var wstart =  $("#weekstart").val();

var tf = $("#tformat").val();

if(tf=="H:i"){
	
	var meridian = false;
}else{
	var meridian = true;
}

$(".date-picker").datepicker({
	autoclose: true,
	todayHighlight: true,
	weekStart: wstart,
	readOnly: true
});

$('.time-picker').timepicker({
    template: false,
    showSeconds: false,
    showMeridian: meridian,    
    showInputs: false,
    minuteStep: 1
});


$(document).on("click",".view-u-profile",function(){
	showProcess();
	var id = $(this).data("id");
	
	$.get("/users/getUserProfile",{id:id},function(r){
		
		$("#profile-view-box-content").html(r.html);
		$("#main-content-box").hide();
		$(".del").hide();
		$("#bk").hide();
		$("#megafooter").hide();
		
		$("#btn-profile-close").show();
		$("#profile-view-box").show();

		endProcess();
		
	},"json");
	
	return false;
});


$(document).on("click","#btn-profile-close",function(){

	$("#btn-profile-close").hide();
	$("#profile-view-box").hide();
	
	$("#profile-view-box-content").html("");
	$("#main-content-box").show();
		$(".del").show();
		$("#bk").show();
	$("#megafooter").show();
	

	
	
	return false;
});


