$(document).ready(function(){

	$("#dis").mask('0999999999.09');


    $("#fdata").validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
		},		

		submitHandler: function() {
		showProcess();
			$.ajax({
				type:'POST', 
				url: '/clients/doUpdate', 
				data:$('#fdata').serialize(),  
				dataType: 'json',
				success: function(r) {
				
					window.location.href="/clients";
	
				}
		
		});
		
		
		 }
		
	});


})

$(document).on("change","#country",function(){
        	var strf ="countryID="+$(this).val();
        	$.post('/misc/changeCountry',strf,function(data){ 
        	 
              if(!data.count){
              	$('#region').hide();
              	$('#region').attr('name','');
              	$('#inputstate').attr('name','client_state');
              	$('#inputstate').show();
              	
              }else{
              	$('#inputstate').hide();
              	$('#inputstate').attr('name','');
              	$('#region').show();
              	$('#region').attr('name','client_state');
              	
              	$('#region').html(data.list);
			
              }
 					
         	},"json");    
         	return false;
        	
});
