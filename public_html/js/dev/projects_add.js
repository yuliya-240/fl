var dformat = $("#dpformat").val();

function doClientPopValidation(){
	
	var validator = $( "#fnewcustomerpopup").validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
	});
	return validator.form();
}

$(document).ready(function() {
	$(".select2").select2({
      width: '100%'
    });

    $("#addMilestone").click(function(){
	    $("#addMilestone").prop('disabled',true);
	    showProcess();
	    
	    $.get('/projects/getMilestoneBox',function(r){
		    endProcess();
		    	$("#milestones-box").append(r.html);
				$("#addMilestone").prop('disabled',false);
	    },"json");
	    
	    return false;
    });

	$("#fdata").validate({

		  errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
		submitHandler: function() { 	
			showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/projects/doAdd', 
				data:$('#fdata').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						window.location.href='/projects';
					}
				}
			
			});
			
		}
			
	});


});

$(document).on('focus',".datetimepicker", function(){
	$(this).datetimepicker({
		autoclose: true,
		format:dformat,
		navIcons:{
			rightIcon: 's7-angle-right',
			leftIcon: 's7-angle-left'
		}
	})
});

$(document).on("click",".del-mil",function(){
	
	var id = $(this).data("id");
	$("#"+id).remove();
	
	return false;
});

$(document).on("click",".btn-add-line",function(){
	$(this).hide();
	$(".del-collab-line").hide();
	var pd = $("#fdata").serialize();
	
	$.get("/projects/addNewCollabLine",pd,function(r){
		
		
		if(r.status){
			$("#collab-list").append(r.html);
			$('#collab-list-item'+r.id).css('backgroundColor', '#dedede');
			$("#collab-list-item"+r.id).animate({
			  backgroundColor: "transparent"
			}, 'slow');

		}else{
			
			$(".del-collab-line").show();
		}

	},"json")
	
	return false;
});

$(document).on("change",".collab-item-sel",function(){
	
	var id = $(this).val();
	
	
	var did = $(this).data("id");
	var txt = $("#prju_collaborator_id"+did+" option:selected").text();	
	console.log(did);
	$(this).hide();
	$("#collab-name-text"+did).text(txt).show();
	//$("#prju_collaborator_id"+did).val(id)
	$(".btn-add-line").show();
	$(".del-collab-line").show();
	return false;
});

$(document).on("click",".del-collab-line",function(){
		var did = $(this).data("id");

		$("#collab-list-item"+did).remove();
		$(".btn-add-line").show();
	
	return false;
})


$(document).on("click","#a-c",function(){
	
	/*$(".dropdowndoc").dropdown('toggle');*/
	$('#share-box').modal('show');
	
	return false;
});

$(document).on("click",".doAddClient",function(){
	
	var isValid = doClientPopValidation();
	
	if(isValid){
		
		showProcess();
		var pd = $("#fnewcustomerpopup").serialize();
		
		$.post("/clients/doAddPopUp",pd,function(r){
			
			$("#client").html(r.selBox);
			$('#client').val(r.id);
			$("#client").trigger("change");
			$('#share-box').modal('hide') ;
			endProcess();
			
		},"json");
	}
	
	return false;
});

$(document).on("change","#country",function(){
	var strf ="countryID="+$(this).val();
	$.post('/misc/changeCountry',strf,function(data){ 
	 
      if(!data.count){
      	$('#region').hide();
      	$('#region').attr('name','');
      	$('#inputstate').attr('name','client_state');
      	$('#inputstate').show();
      	
      }else{
      	$('#inputstate').hide();
      	$('#inputstate').attr('name','');
      	$('#region').show();
      	$('#region').attr('name','client_state');
      	
      	$('#region').html(data.list);
	
      }
				
 	},"json");    
 	return false;
	
});

