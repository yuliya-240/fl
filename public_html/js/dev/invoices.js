var mf = $("#moment-format").val();

function getContent(searchstr){

	$.get('/invoices/getContent',{query:searchstr},function(r){
		
		$("#d-content").html(r.html);
			
		var c = parseInt(r.count);
		if(c>0){
			
			$("#megafooter").html(r.footer);
		}
		
		endProcess();
	},"json");
}

$(document).ready(function() {
	
	var ss = $('#s').val();
	getContent(ss);
	
	$('#s').keyup(function(e){
		
		  if ( e.which == 13 ) {
		  	e.preventDefault();
		  }
		
		var searchstr = $(this).val();
		
		/*if(searchstr.length){
			
			$("#delsearch").show();
		}else{
			$("#delsearch").hide();
		}
		*/
		
		getContent(searchstr);
	});
	

	$('#idrp').daterangepicker({
		locale: {
          cancelLabel: 'Clear',
          format: mf
		},
		opens: "left"
	});
	
	var startdate = $("#startDate").val();
	var enddate =  $("#endDate").val();
	
	if(startdate!="none"){
		
		$('#idrp').data('daterangepicker').setStartDate(moment(startdate).format(mf));
		$('#idrp').data('daterangepicker').setEndDate(moment(enddate).format(mf));
	    var str = moment(startdate).format(mf)+' - '+moment(enddate).format(mf);
		$("#idrp").html(str);
	}
	
	$('#idrp').on('apply.daterangepicker', function(ev, picker) {
		showProcess();
		var dstart = picker.startDate.format('YYYY-MM-DD');
		var dend = picker.endDate.format('YYYY-MM-DD');
	    $("#startDate").val(dstart);
	    $("#endDate").val(dend);
	    var str = ''+picker.startDate.format(mf)+' - '+picker.endDate.format(mf);
		$("#idrp").html(str);
		$.post("/invoices/changeDateRange",{start:dstart,end:dend},function(r){
			
			getContent(r.s)
			
		},"json");
  	});

	$('#idrp').on('cancel.daterangepicker', function(ev, picker) {
		var dstart = "none";
		var dend = "none";
	    $("#startDate").val(dstart);
	    $("#endDate").val(dend);
		  
	    $("#idrp").html('<span class="s7-date" style="font-size: 24px;"></span>');
		$.post("/invoices/changeDateRange",{start:dstart,end:dend},function(r){
			
			getContent(r.s)
			
		},"json");
	});
	
	
	
});

$(document).on("click",".ita",function(){
	$(this).addClass("active");
	$(".it").removeClass("active");
	$(".invtypes-val").remove();
	$("#foptions").append('<input type="hidden" class="invtypes-val" id="ita" name="invtypes[]" value="0"> ');
	return false;
});

$(document).on("click",".it",function(){
	$(".ita").removeClass("active");
	var el = $(this);
	var id = el.data("val"); 
	if(el.hasClass("active")){
		
		$("input#it"+id).remove();
		el.removeClass("active");
		var c =  $(".it.active");
		if(!c.length){
			$(".ita").addClass("active");
			$(".invtypes-val").remove();
			$("#foptions").append('<input class="invtypes-val" type="hidden" id="ita" name="invtypes[]" value="0"> ');
		}	
		
	}else{
		$("#ita").remove();
		
		$("#foptions").append('<input type="hidden" class="invtypes-val" id="it'+id+'" name="invtypes[]" value="'+id+'"> ');
		
		el.addClass("active");
	}
});

$(document).on("click",".iarc",function(){
	
	var v = $(this).data("val");
	
	$("#invarc").val(v);
	
	$(".iarc").removeClass("active");
	$(this).addClass("active");
	
	return false;
});

$(document).on("click",".doApplyFilter",function(){
	showProcess();
	var pd = $("#foptions").serialize();
	
	$.post("/invoices/doApplyFilter",pd,function(r){
		
		getContent(r.s);
		
	},"json")
	
	return false;
});

$(document).on("click","#bundleDelete",function(){

	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_pdt").val();
	var msg = $("#_pdm").val();
	var yes = $("#_yes").val();

	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true,
			showCancelButton: true,},
			function(){
					
				showProcess();	
				$.ajax({
						type:'POST', 
						url: '/invoices/deleteBundle', 
						data:$("#flist").serialize(), 
						dataType: 'json',
						success: function(r) {
						
							var s = $("#s").val();
							getContent(s);


						}
				});					
			});	
	}
	return false;
});     	

$(document).on("click","#bundlePay",function(){

	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_bpt").val();
	var msg = $("#_bpm").val();
	var yes = $("#_yes").val();
	var no = $("#_no").val();


	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#7ACCBE",
			closeOnConfirm: true,
				showCancelButton:true,
				showConfirmButton:true,
				cancelButtonText:no,
			
			},
			function(){
					
				showProcess();	
				$.ajax({
					type:'POST', 
					url: '/invoices/payBundle', 
					data:$("#flist").serialize(), 
					dataType: 'json',
					success: function(r) {
					
							window.location.href=r.url;

					}
				});					
			});	
	}
	return false;
}); 

$(document).on("click","#bundleSend",function(){

	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_bst").val();
	var msg = $("#_bsm").val();
	var yes = $("#_yes").val();
	var no = $("#_no").val();


	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#7ACCBE",
			closeOnConfirm: true,
				showCancelButton:true,
				showConfirmButton:true,
				cancelButtonText:no,
			
			},
			function(){
					
				showProcess();	
				$.ajax({
					type:'POST', 
					url: '/invoices/sendBundle', 
					data:$("#flist").serialize(), 
					dataType: 'json',
					success: function(r) {
					
							if(!r.status){
								endProcess();
								$("#err-list-box").html(r.html);
								$('#send-err-box').modal('show');
							}else{
								
								swal({
									
									title:r.title,
									text:r.msg,
									type:"success"
								},function(){
									
									var s = $("#s").val();
									getContent(s);
								});
								
							}

					}
				});					
			});	
	}
	return false;
}); 

$(document).on("click","#bundleArc",function(){

	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_bat").val();
	var msg = $("#_bam").val();
	var yes = $("#_yes").val();
	var no = $("#_no").val();


	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#7ACCBE",
			closeOnConfirm: true,
				showCancelButton:true,
				showConfirmButton:true,
				cancelButtonText:no,
			
			},
			function(){
					
				showProcess();	
				var data = $("#flist").serialize();
				var pd = data+"&act=1"; 
				$.post("/invoices/arcBundle",pd,function(r){
					var s = $("#s").val();
					getContent(s);
					
				},"json")
				
			});	
	}
	return false;
}); 

$(document).on("click","#bundleUnarc",function(){

	var $b = $('input.check[type=checkbox]');
	var countChecked = $b.filter(':checked').length;

	var title = $("#_buat").val();
	var msg = $("#_buam").val();
	var yes = $("#_yes").val();
	var no = $("#_no").val();


	if(countChecked>0){

		swal({
			
			text: msg,
			title:title,
			type: "warning",
			confirmButtonText: yes,
			confirmButtonColor: "#7ACCBE",
			closeOnConfirm: true,
				showCancelButton:true,
				showConfirmButton:true,
				cancelButtonText:no,
			
			},
			function(){
					
				showProcess();	
				var data = $("#flist").serialize();
				var pd = data+"&act=0"; 
				$.post("/invoices/arcBundle",pd,function(r){
					var s = $("#s").val();
					getContent(s);
					
				},"json")
				
			});	
	}
	return false;
}); 

$('#send-err-box').on('hidden.bs.modal', function () {
	showProcess();
	var s = $("#s").val();
	getContent(s);
});

$(document).on("click",".prevp, .nextp",function(){
	
	showProcess();
	var p = $(this).data("page");
	$.post('/invoices/changePage',{p:p},function(r){
		
			var s = $("#s").val();
			getContent(s);
		
	},"json");
	
	return false;
});
