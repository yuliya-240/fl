var uploader = new plupload.Uploader({
			runtimes : 'html5,flash,html4',
			browse_button : 'pickfiles', // you can pass an id...
			container: document.getElementById('containerpic'), // ... or DOM Element itself
			url : '/img/uploadinvpic/',
			unique_names:false,
			multi_selection:true,
			max_file_count: 10,
			flash_swf_url : '/js/lib/plupload-2.1.8/js/Moxie.swf',
			silverlight_xap_url : '/js/lib/plupload-2.1.8/js/Moxie.xap',
			
			filters : {
				max_file_size : '10mb',
				/*mime_types: [
					{title : "Image files", extensions : "jpg,gif,png"},
					
				]*/
			},
		
			init: {
				PostInit: function() {},
		
				FilesAdded: function(up, files) {
					$("#progressSlide").css('width',"0%");
					$("#progress1").show();
					$("#pickfiles").hide();
			
					up.refresh(); // Reposition Flash/Silverlight
					uploader.start();
				},
		
				UploadProgress: function(up, file) {
					$("#progressSlide").css('width',file.percent+"%");
					$("#progress1").data('percent',file.percent+"%");
				},
		
				Error: function(up, err) {
					var errmsg="<h4>Upload failed</h4><p>Error Code:"+err.code+"</p><p>"+err.message+"</p>";
			
					up.refresh(); // Reposition Flash/Silverlight
					
					$("#progress1").hide();
					$("#pickfiles").show();
					$("#delpic").show();
				},
				
				FileUploaded: function(up, file, info) {
					
					var obj = JSON.parse(info.response);
					var $htm = $(obj.html);
					$('#plist').append($htm);
					$('.venobox').venobox(); 
					
				},
				UploadComplete: function(up,files){
					$("#progress1").hide();
			
					$("#pickfiles").show();
					
					
				}
			}
		});

function recalculateBaseAmounts(){
	
	var total = parseFloat($("#total").val());
	total = total.toFixed(2);
	var due = parseFloat($("#due").val());
	due = due.toFixed(2);

	var rate = parseFloat($("#inv_exchange_rate").val());
	var total_base = total*rate;
	var due_base = due*rate;
	
	$("#total_base").val(total_base.toFixed(2));
	$("#due_base").val(due_base.toFixed(2));
	$("#total-txt-base").text(total_base.toFixed(2));
	$("#due-txt-base").text(due_base.toFixed(2));			

	var base = $("#basecurrency").val();
	var c = $("#inv_currency").val();

	if(base!=c){

		$(".base-currency-totals").show();
	}

}

function doClientPopValidation(){
	
	var validator = $( "#fnewcustomerpopup").validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
	});
	return validator.form();
}

function doBillingAddrFormValidation(){
	
	var validator = $( "#fbilladdr").validate({
		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
  		  },		
	});
	return validator.form();
}

function doInvoiceFormValidation(){
	var validator = $( "#finvoice" ).validate({
		
    	rules: {
	        'invrow_srv[]': {
	            required: true,
	            minlength: 1
	        },

	        'inv_num': {
	            required: true,
	            minlength: 1
	        },
	        
	        

        },	

		errorPlacement: function(label, elem) {
		  	var errbox = elem.data("errorbox");
		  	$(errbox).html(label);
		  	
  		},		


		
	});
	return validator.form();
}

function recountLineTotal(id){
	var rateval = $("#irrate"+id).val();
	if(rateval.length==0)rateval=0;
	var rate = parseFloat(rateval);
	
	var qtyval = $("#irqty"+id).val();
	if(qtyval.length==0)qtyval=0;
	var qty = parseFloat(qtyval);

	var lt = rate*qty	
	var linet = lt.toFixed(2);
	
	$("#irtotal"+id).val(linet);

   	calcTaxCashLine(id);
   	recountTaxes();

	
}

function recountSubtotal(){
	
	var subtotal = 0;
	
	$('.linetotal').each(function() {
		var val = $(this).val();
		var linetotal = 0;
		 
		if(val)linetotal = parseFloat(val);
		subtotal=subtotal+parseFloat(linetotal); 
		
	});
	
	subtotal = subtotal.toFixed(2);
	
	$("#subtotal-txt").text(subtotal);
	$("#subtotal").val(subtotal);
}

function calcTaxCashLine(id){
	
	var tax=0;
	var tax1 = 0;
	var tax2 = 0;
	var linetotal = parseFloat($("#irtotal"+id).val());
	
	var taxval1 = $("#irtax1"+id).val();
	var taxval2 = $("#irtax2"+id).val();
	
	if(taxval1.length==0){
		tax1=0;
	}else{
		tax1 = parseFloat(taxval1);
		tax1 = tax1.toFixed(2);
		
	}
	
	if(taxval2.length==0){
		tax2=0;
	}else{
		tax2 = parseFloat(taxval2);
		tax2 = tax2.toFixed(2);
		
	}
	
	var cashText1 = (linetotal*tax1)/100;
	cashText1 = cashText1.toFixed(2);

	var cashText2 = (linetotal*tax2)/100	;
	cashText2 = cashText2.toFixed(2);
	
	$("#taxcash1"+id).val(cashText1);
	$("#taxcash2"+id).val(cashText2);
}

function recountTaxes(){

	var tax = 0;
	$('.tax1').each(function() {
		var id = $(this).data("id");
		var tax1 = parseFloat($(this).val());
		var tax2 = parseFloat($("#taxcash2"+id).val());
		var totallinetax = tax1+tax2;
		tax=tax+totallinetax;
	});
	tax = tax.toFixed(2);
	$("#totaltax").val(tax);
	$("#totaltax-txt").text(tax);
	
}

function calcAllTotal(){
	var paid = parseFloat($("#paid").val());
	var subtotal = parseFloat($("#subtotal").val());
	var taxes = parseFloat($("#totaltax").val());	
	
	var discountval = $("#discount").val();//parseFloat($("#discount").val());
	
	if(discountval.length==0)discountval=0;
	
	var discount = parseFloat(discountval);	
	var totaltax = subtotal+taxes;
	var disccash = (totaltax*discount)/100;
	
	var total = totaltax-disccash;
	total = total.toFixed(2);
	var due = total - paid;
	due = due.toFixed(2);

	var rate = parseFloat($("#inv_exchange_rate").val());
	var total_base = total*rate;
	var due_base = due*rate;
	var disccash_base = disccash*rate;
	
	$("#total_base").val(total_base.toFixed(2));
	$("#due_base").val(due_base.toFixed(2));

	$("#total-txt-base").text(total_base.toFixed(2));
	$("#due-txt-base").text(due_base.toFixed(2));


	$("#total").val(total);
	$("#total-txt").text(total);

	$("#due").val(due);
	$("#due-txt").text(due);
	$("#disc-cash").val(disccash);
}


$(document).ready(function(){
	$(".select2").select2({
      width: '100%'
    });

	var y = $("btnYes").val();
	
	var n = $("btnNo").val();
	
	$('.venobox').venobox(); 
	

	$(".asend").bootstrapSwitch({"size":"medium", "onText":y, "offText":n});
	
	$(".asend").on('switchChange.bootstrapSwitch', function(event, state) {
	
		var name = $(this).prop("name");
		
		if(state){
			$("#asend-email-box").show();
			
			//$("#rec-off").hide();
			//$("#rec-on").show();
			
		}else{
			$("#asend-email-box").hide();
			//$("#rec-on").hide();
			//$("#rec-off").show();
		}
  
	});

	
	

	uploader.init();
	
	$(".dec").livequery(function(){
	
	    var id = $(this).data("id");
	    $(this).mask('0999999999.09', {
		   	maxlength: false,
		   	onChange: function(cep){
		    	recountLineTotal(id);
		    	recountSubtotal();
		    	calcAllTotal()
			},
			onEmpty : function(){
				conaole.log(id);
				recountLineTotal(id);
				recountSubtotal();
				calcAllTotal()
			}
		});
    }); 
    
    $(".xr").livequery(function(){
	
	   
	    $(this).mask('0999999999.0999', {
		   	maxlength: false,
		   	
		   	onChange: function(cep){

				
				recalculateBaseAmounts();
				/*var total = parseFloat($("#total").val());
				total = total.toFixed(2);
				var due = parseFloat($("#due").val());
				due = due.toFixed(2);
			
				var rate = parseFloat($("#inv_exchange_rate").val());
				var total_base = total*rate;
				var due_base = due*rate;
				
				$("#total_base").val(total_base.toFixed(2));
				$("#due_base").val(due_base.toFixed(2));
				$("#total-txt-base").text(total_base.toFixed(2));
				$("#due-txt-base").text(due_base.toFixed(2));	*/		
			},
			/*onEmpty : function(){
				$("#pay_due_base").val(0);
			}*/
		   	
		});
    }); 	

	$(".inum").livequery(function(){
	
    $(this).mask('0#', {
	   maxlength: false,

	    
	   });

	
    }); 

	$(".tax").livequery(function(){
	
	    var id = $(this).data("id");
	    $(this).mask('0999999999.09', {
		   	maxlength: false,
		   	onChange: function(cep){
			   	calcTaxCashLine(id);
			   	recountTaxes();
			   	calcAllTotal();
		  
			},
			onEmpty : function(){
		
				calcTaxCashLine(id);
				recountTaxes();
				calcAllTotal()
			}
		});
    }); 
    
	$(".dis").livequery(function(){
	
	    var id = $(this).data("id");
	    $(this).mask('0999999999.09', {
		   	maxlength: false,
		   	onChange: function(cep){

			   	calcAllTotal();
	
			},
			onEmpty : function(){

				calcAllTotal()
			}
		});
    }); 
	
	var wstart =  $("#weekstart").val();
	
	$(".date-picker").datepicker({
		autoclose: true,
		todayHighlight: true,
									/*format : "M dd, yyyy",*/
		weekStart: wstart,
		readOnly: true
	});

	$(".linefilter").livequery(function(){
	 	var lineid = $(this).data('id');
	 	
	 	var $input = $(this);
	 	$input.typeahead({
			source:function ( query,process) {
	    	
		       return $.post('/services/invfilter', { query: query }, function (data) {
		             return process(data);
		        },"json");
	        },
			autoSelect: true,
			fitToElement: true,
			items:21,
		
		}); 
	
		$input.change(function() {
		    var obj = $input.typeahead("getActive");
		    if (obj) {
			    
			    $("#name"+lineid).val(obj.fullname);
				$("#irrate"+lineid).val(obj.cost);
				$("#irqty"+lineid).val(obj.qty);
				$("#irtax1"+lineid).val(obj.tax1);
				$("#irtax2"+lineid).val(obj.tax2);
				$("#irdesc"+lineid).val(obj.desc);
				$("#irtotal"+lineid).val(obj.total);
				recountLineTotal(lineid);
				recountSubtotal();
			   	calcTaxCashLine(lineid);
			   	recountTaxes();
			   	calcAllTotal();
				
		    }
		});
	 
	
	});

	$("#recOptions").change(function(){
		var recoption = $(this).val();
		
		$(".optBox").hide(); 
		
		if (recoption == 'd')$("#dayOptionBox").show();	
		
		if(recoption == 'w')$("#weekOptionBox").show();
		
		if(recoption == 'm')$("#monthOptionBox").show();
		
		if(recoption == 'y')$("#yearOptionBox").show();
		
		return false;
	});

	//$("#inv_currency").trigger("change");
	recalculateBaseAmounts();
})

$(document).on("click","#chAddr",function(){
	
	showProcess();
	
	
	var id=$("#client").val();
	$.get("/invoices/getBillingAddr",{id:id},function(r){
		
		$("#bill-adr-box").html(r.html);
		
		endProcess();
		
	},"json");
	
	$('#addr-box').modal('show');
	
	return false;
	
});

$(document).on("click",".addline",function(){
	
	$(this).prop("disabled",true);
	
	$.get("/invoices/getNewLine",function(r){
		
		$(r.html).appendTo("#inv-lines");
		
		$(".addline").prop("disabled",false);
		
	},"json")
	
	return false;
});

$(document).on("change","#client",function(){
	showProcess();
		var id = $(this).val();
		
	
		
		if(id=="0"){
		
			$('#address').html("").hide();
			$('#inv_street').val("");
			$('#inv_name').val("");
			$('#inv_city').val("");
			$('#inv_state').val("");
			$('#inv_zip').val("");
			$('#inv_country').val("");
		
			
		}else{
		
			$.get('/invoices/getAddresstoInvoice',{ id: id},function(r){
				$('#address').html(r.html).show();
				
				$('#discount').val(r.discount);	
				$('#inv_street').val(r.street);
				$('#inv_name').val(r.name);
				$('#inv_city').val(r.city);
				$('#inv_state').val(r.state);
				$('#inv_zip').val(r.zip);
				$('#inv_country').val(r.country);
				
				calcAllTotal()	;	
				endProcess();
				
	    	},"json");    
				
		}
		
	return false;
});

$(document).on("click",".delatt",function(){
	
	var id = $(this).data("id");
	
	$("#attitem"+id).remove();
	$("#attfile"+id).remove();
	
	return false;
})

$(document).on("click",".delline",function(){
	
	var id = $(this).data("id");
	
	$("#line"+id).remove();
	recountTaxes();
	recountSubtotal();
	calcAllTotal()
	
	return false;
});

$(document).on("change","#ir",function(){
	
	var v = $(this).val();
	
	$("#rsd").hide();
	$("#rsc").hide();
	
	if(v==2){
		$("#rsd").show();
	}
	
	if(v==3){
		$("#rsc").show();
	}

	
})

$('#doSave, #doSave1').click(function(){
	
	var isValid = doInvoiceFormValidation();
	
	if(isValid){
		showProcess();
		var postdata = $('#finvoice').serialize();
		var postimg = $('#fpic').serialize();
		
		$.post('/invoices/recurring/doUpdate',postdata+'&'+postimg,function(r){ 
			
			if(r.status){
				
				window.location.href = '/invoices/recurring/view/'+r.id+'/?act=upd' ;//returl;
				
			}else{
				endProcess();
				swal({
					title:r.title,
					text: r.msg,
					type:"error"
					
				});
			};
			
		},"json");
	}	
	
	return false;
});

$(document).on("click","#a-c",function(){
	
	/*$(".dropdowndoc").dropdown('toggle');*/
	$('#share-box').modal('show');
	
	return false;
});

$(document).on("change","#country",function(){
	var strf ="countryID="+$(this).val();
	$.post('/misc/changeCountry',strf,function(data){ 
	 
      if(!data.count){
      	$('#region').hide();
      	$('#region').attr('name','');
      	$('#inputstate').attr('name','client_state');
      	$('#inputstate').show();
      	
      }else{
      	$('#inputstate').hide();
      	$('#inputstate').attr('name','');
      	$('#region').show();
      	$('#region').attr('name','client_state');
      	
      	$('#region').html(data.list);
	
      }
				
 	},"json");    
 	return false;
	
});

$(document).on("change","#country2",function(){
	var strf ="countryID="+$(this).val();
	$.post('/misc/changeCountry',strf,function(data){ 
	 
      if(!data.count){
      	$('#region2').hide();
      	$('#region2').attr('name','');
      	$('#inputstate2').attr('name','billing_state');
      	$('#inputstate2').show();
      	
      }else{
      	$('#inputstate2').hide();
      	$('#inputstate2').attr('name','');
      	$('#region2').show();
      	$('#region2').attr('name','billing_state');
      	$('#region2').html(data.list);
	
      }
				
 	},"json");    
 	return false;
	
});

$(document).on("click",".doAddClient",function(){
	
	var isValid = doClientPopValidation();
	
	if(isValid){
		
		showProcess();
		var pd = $("#fnewcustomerpopup").serialize();
		
		$.post("/clients/doAddPopUp",pd,function(r){
			
			$("#client").html(r.selBox);
			$('#client').val(r.id);
			$("#client").trigger("change");
			$('#share-box').modal('hide') ;
			endProcess();
			
		},"json");
	}
	
	return false;
});

$(document).on("click","#doSaveDraft",function(){
	
	var client = parseInt($("#client").val());
	
	var tit = $("#_tit").val();
	var txt = $("#_txt").val();
	var ctit = $("#_ctit").val();
	var ctxt = $("#_ctxt").val();
	var ltit = $("#_ltit").val();
	var ltxt = $("#_ltxt").val();
	
	if(client > 0){
		
		var lineCount = 0;
		
		$(".linefilter").each(function(){
			
			lineCount++;
		})
		
		if(lineCount>0){

			var isValid = doInvoiceFormValidation();
			
			if(isValid){
				showProcess();
				var pd = $("#finvoice").serialize();
				
				$.post("/invoices/doUpdate",pd,function(r){
					
					if(r.status){
						
						window.location.href="/invoices";
					}else{
						
						endProcess();
					}
					
				},"json")
				
			}else{
				swal({
					title:tit,
					text:txt,
					type:"error"
				})	
			}
		
		}else{
			
			swal({
				title:ltit,
				text:ltxt,
				type:"error"
			})	
			
			
		}
	}else{
		
		swal({
			title:ctit,
			text:ctxt,
			type:"error"
		})	
	}
	
	
	return false;
	
});

$(document).on("click",".doChAddr",function(){
	
	
	var isValid = doBillingAddrFormValidation();
	
	if(isValid){
		
		showProcess();
		var pd = $("#fbilladdr").serialize();
		
		$.post("/invoices/doChAddr",pd,function(r){
			
			$("#client").html(r.selBox);
			$('#client').val(r.id);
			$("#client").trigger("change");
			$('#addr-box').modal('hide') ;
			endProcess();
			
		},"json");
	}
	
	return false;
	
	
})

$(document).on("change","#inv_currency",function(){
	var base = $("#basecurrency").val();
	var c = $(this).val();
	var rate = $("#rate"+c).val();
	$("#currency-label").text(c+"/"+base);
	$("#inv_exchange_rate").val(rate);

	var total = parseFloat($("#total").val());
	total = total.toFixed(2);
	var due = parseFloat($("#due").val());
	due = due.toFixed(2);

	var rate = parseFloat($("#inv_exchange_rate").val());
	var total_base = total*rate;
	var due_base = due*rate;
	
	$("#total_base").val(total_base.toFixed(2));
	$("#due_base").val(due_base.toFixed(2));
	$("#total-txt-base").text(total_base.toFixed(2));
	$("#due-txt-base").text(due_base.toFixed(2));


	if(base==c){
		
		$("#rate-box").hide();
		$(".base-currency-totals").hide();
	}else{
		$(".sel-curr").text(c);
		$("#rate-box").show();
		$(".base-currency-totals").show();
	}
});

$(document).on("change","#inv_lang",function(){
	showProcess();
	var lng = $(this).val();
	
	$.get("/invoices/changeLang",{lang:lng},function(r){
		
		$.each(r.lang, function(key, reader){
		
			$(".invlang_"+key).text(reader);
		});
		
		endProcess();
	},"json")
	
	
});

