<?php

/* desktop/projects/details_view.tpl */
class __TwigTemplate_75a85a6135f083c2bad9d234452d5c4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table id=\"prj-details-tbl\" class=\"table\">
    <tr>
        <td width=\"20%\" ><small class=\"text-muted\">";
        // line 3
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "tbl_name");
        echo "</small></td>
        <td id=\"tbl-prj-name\">";
        // line 4
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_name");
        echo "</td>
    </tr>

    <tr>
        <td><small class=\"text-muted\">";
        // line 8
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "client");
        echo "</small></td>
        <td >

\t    \t";
        // line 11
        if ((($this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_client_id") == 0) && ($this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_collab_id") == 0))) {
            // line 12
            echo "\t\t\t<span>";
            echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "noassign");
            echo "</span>
\t\t\t";
        } else {
            // line 14
            echo "\t        \t";
            if ($this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_client_id")) {
                // line 15
                echo "\t        \t\t";
                echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "client_name");
                echo "
\t        \t";
            } else {
                // line 17
                echo "\t        \t\t";
                echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "user_name");
                echo "
\t        \t";
            }
            // line 19
            echo "\t        ";
        }
        // line 20
        echo "\t    </td>
    </tr>


    <tr>
        <td><small class=\"text-muted\">";
        // line 25
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "tbl_status");
        echo "</small></td>
        <td class=\"text-";
        // line 26
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "prjstatus_color");
        echo "\" id=\"tbl-prj-status\">";
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "prjstatus_name");
        echo "</td>
    </tr>


\t<tr>
\t\t<td colspan=\"2\">
        ";
        // line 32
        if ($this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_desc")) {
            // line 33
            echo "\t\t";
            echo nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_desc"), "html", null, true));
            echo "
\t\t";
        } else {
            // line 35
            echo "\t\t\t<div class=\"text-center text-danger\">";
            echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "no_desc");
            echo "</div>
\t\t";
        }
        // line 37
        echo "\t\t</td>
\t\t
\t</tr>\t
</table>   ";
    }

    public function getTemplateName()
    {
        return "desktop/projects/details_view.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 37,  94 => 35,  88 => 33,  86 => 32,  75 => 26,  71 => 25,  64 => 20,  61 => 19,  55 => 17,  49 => 15,  46 => 14,  40 => 12,  38 => 11,  32 => 8,  25 => 4,  21 => 3,  17 => 1,);
    }
}
