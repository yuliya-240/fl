<?php

/* desktop/common/root.tpl */
class __TwigTemplate_9eeebe931623a2d20a67c3317f1ed7c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'preheadcss' => array($this, 'block_preheadcss'),
            'headcss' => array($this, 'block_headcss'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'js' => array($this, 'block_js'),
            'modals' => array($this, 'block_modals'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">
    <title>Floctopus</title>
    <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"/assets/lib/font-awesome/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/assets/lib/stroke-7/style.css\"/>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/assets/lib/jquery.nanoscroller/css/nanoscroller.css\"/>
    <!--[if lt IE 9]>
\t    
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    ";
        // line 18
        $this->displayBlock('preheadcss', $context, $blocks);
        // line 19
        echo "    
    ";
        // line 21
        echo "    <link rel=\"stylesheet\" href=\"/assets/css/themes/theme-google.css\" type=\"text/css\"/>
    <link rel=\"stylesheet\" href=\"/assets/lib/sweetalert/sweetalert.css\" type=\"text/css\"/>
    <link rel=\"stylesheet\" href=\"/css/app.css\" type=\"text/css\"/>
\t<link rel=\"stylesheet\" href=\"/assets/css/chat.css\" type=\"text/css\"/>
\t
\t";
        // line 26
        $this->displayBlock('headcss', $context, $blocks);
        // line 27
        echo "\t
\t
\t<script src=\"https://js.pusher.com/4.1/pusher.min.js\"></script>
  </head>
  <body>
    <div class=\"am-wrapper am-white-header am-fixed-sidebar ";
        // line 32
        echo (isset($context["fixed_footer"]) ? $context["fixed_footer"] : null);
        echo "\">
\t
\t<div id=\"process-loader\" class=\"fade\">
\t    <div class=\"material-loader\">
\t        <svg class=\"circular\" viewBox=\"25 25 50 50\">
\t            <circle class=\"path\" cx=\"50\" cy=\"50\" r=\"20\" fill=\"none\" stroke-width=\"2\" stroke-miterlimit=\"10\"/>
\t        </svg>
\t        <div class=\"message\">";
        // line 39
        echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "loader");
        echo "</div>
\t    </div>
\t</div>
\t
\t<input type=\"hidden\" id=\"curl\" value=\"";
        // line 43
        echo (isset($context["curl"]) ? $context["curl"] : null);
        echo "\">
\t<input type=\"hidden\" id=\"lang\" value=\"";
        // line 44
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_lang");
        echo "\">
\t<input type=\"hidden\" id=\"_mch\" value=\"";
        // line 45
        echo (isset($context["channel"]) ? $context["channel"] : null);
        echo "\">\t   
\t<input type=\"hidden\" id=\"_broadcast\" value=\"broadcast\">\t
\t<input type=\"hidden\" id=\"_chsound\" value=\"";
        // line 47
        echo (isset($context["chsound"]) ? $context["chsound"] : null);
        echo "\">
\t<input type=\"hidden\" id=\"chatopen\" value=\"0\">\t
\t<input type=\"hidden\" id=\"c-u\" value=\"";
        // line 49
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id");
        echo "\">
\t<input type=\"hidden\" id=\"basecurrency\" value=\"";
        // line 50
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_currency");
        echo "\">
\t<input type=\"hidden\" id=\"weekstart\" value=\"";
        // line 51
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_wstart");
        echo "\">
\t <input type=\"hidden\" id=\"tformat\" value=\"";
        // line 52
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_tformat");
        echo "\">
    \t";
        // line 53
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/nav.tpl"));
        $template->display($context);
        echo " 
    \t";
        // line 54
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/sidebar_left.tpl"));
        $template->display($context);
        echo " 
    \t<div class=\"am-content\">
\t    \t
\t    \t";
        // line 57
        $this->displayBlock('content', $context, $blocks);
        // line 58
        echo "
\t\t\t <div id=\"msg-box\" tabindex=\"-1\" role=\"dialog\" class=\"modal modal-colored-header fade\" >
\t\t\t  <div style=\"width: 100%;\" class=\"modal-dialog\">
\t\t\t    <div class=\"modal-content\" id=\"ch-c\" style=\"max-width: 90%; background-image: url(/img/chatbg4.jpg); background-color: #e7e7e7; position: relative;\">
\t\t\t\t
\t\t\t    </div>
\t\t\t  </div>
\t\t\t </div>   

\t    \t";
        // line 67
        $this->displayBlock('footer', $context, $blocks);
        echo "\t
\t    \t
\t    \t<div class=\"main-content\" id=\"profile-view-box\"  style=\"display: none;\">
\t    \t\t<div class=\"row\" >
\t\t\t\t\t<div class=\"col-xs-12\" id=\"profile-view-box-content\">
\t\t\t\t\t  \t
\t\t\t\t\t</div>\t
\t\t\t\t</div>\t
\t    \t</div>
    \t</div>
\t\t<!-- right sidebar -->
    </div>

    ";
        // line 81
        echo "    <script  src=\"http://code.jquery.com/jquery-2.2.4.min.js\" integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\" crossorigin=\"anonymous\"></script>
    <script src=\"/assets/lib/jquery-ui/ui/minified/jquery-ui.min.js\"></script>
    <script type=\"text/javascript\" src=\"/assets/lib/touch/jquery.ui.touch.js\"></script>
    <script src=\"/assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js\" type=\"text/javascript\"></script>
    <script src=\"/assets/js/main.js\" type=\"text/javascript\"></script>
    <script src=\"/assets/lib/bootstrap/dist/js/bootstrap.min.js\" type=\"text/javascript\"></script>
    <script type=\"text/javascript\">
    \$(document).ready(function(){
\t    App.init({
\t      openLeftSidebarOnClick: true
\t    });      
    });
      
    </script>
    \t
\t<script src=\"/assets/lib/jquery-validate/jquery.validate.min.js\"></script>
\t<script src=\"/assets/lib/sweetalert/sweetalert.min.js\"></script>
\t";
        // line 99
        echo "    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js\"></script>
\t<script src=\"/assets/lib/timeago/jquery.timeago.js\"></script>
\t<script src=\"/assets/lib/timeago/locales/jquery.timeago.";
        // line 101
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_lang");
        echo ".js\"></script>
\t<script src=\"/js/dev/common.js?v=";
        // line 102
        echo (isset($context["hashver"]) ? $context["hashver"] : null);
        echo "\"></script>
\t<script src=\"/assets/lib/plupload-2.3.1/js/plupload.full.min.js\"></script>
\t<script src=\"https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js\"></script>

\t<script src=\"/js/dev/chat.js?v=";
        // line 106
        echo (isset($context["hashver"]) ? $context["hashver"] : null);
        echo "\"></script>\t

    ";
        // line 108
        $this->displayBlock('js', $context, $blocks);
        // line 109
        echo "    
    ";
        // line 110
        $this->displayBlock('modals', $context, $blocks);
        // line 111
        echo "  </body>

</html>
";
    }

    // line 18
    public function block_preheadcss($context, array $blocks = array())
    {
    }

    // line 26
    public function block_headcss($context, array $blocks = array())
    {
    }

    // line 57
    public function block_content($context, array $blocks = array())
    {
    }

    // line 67
    public function block_footer($context, array $blocks = array())
    {
    }

    // line 108
    public function block_js($context, array $blocks = array())
    {
    }

    // line 110
    public function block_modals($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "desktop/common/root.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 110,  228 => 108,  223 => 67,  218 => 57,  213 => 26,  208 => 18,  201 => 111,  199 => 110,  196 => 109,  194 => 108,  189 => 106,  182 => 102,  178 => 101,  174 => 99,  155 => 81,  139 => 67,  128 => 58,  126 => 57,  119 => 54,  114 => 53,  110 => 52,  106 => 51,  102 => 50,  98 => 49,  93 => 47,  88 => 45,  84 => 44,  80 => 43,  73 => 39,  63 => 32,  56 => 27,  54 => 26,  47 => 21,  44 => 19,  42 => 18,  23 => 1,  27 => 4,  24 => 3,);
    }
}
