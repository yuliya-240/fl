<?php

/* desktop/projects/index.tpl */
class __TwigTemplate_5bc34592f19568de5e863cab5d18c56f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'headcss' => array($this, 'block_headcss'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_headcss($context, array $blocks = array())
    {
        // line 3
        echo "    
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "<input type=\"hidden\" id=\"_pdt\" value=\"";
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "del_bprj_title");
        echo "\">
<input type=\"hidden\" id=\"_pdm\" value=\"";
        // line 8
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "del_bprj_message");
        echo "\">
<input type=\"hidden\" id=\"_yes\" value=\"";
        // line 9
        echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "btn_yes");
        echo "\">

<div class=\"page-head\">
\t<h3 style=\"margin-bottom: 2px; margin-top: 2px;\">";
        // line 12
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "title");
        echo "
\t\t<a href=\"/projects/add\" class=\"btn btn-head btn-success pull-right\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-original-title=\"";
        // line 13
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "btn_add");
        echo "\" >";
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "btn_add");
        echo "</a>

\t\t
\t</h3>
</div>\t


<div class=\"main-content\">
<div class=\"row\">
\t<div class=\"col-xs-12\">
\t\t<form id=\"flist\" style=\"border-radius: 0px;\" class=\"form-horizontal group-border-dashed\">
\t\t<div class=\"panel panel-default panel-borders\">
\t       ";
        // line 31
        echo "\t        <div class=\"panel-body\">
\t\t\t
\t\t\t\t\t<div id=\"d-content\">
\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t   
\t
\t        </div>
\t\t</div>
\t\t</form>
\t</div>
</div>\t\t
</div>

";
    }

    // line 47
    public function block_footer($context, array $blocks = array())
    {
        // line 48
        echo "<div id=\"megafooter\">
\t
</div>\t
";
    }

    // line 53
    public function block_js($context, array $blocks = array())
    {
        // line 54
        echo "
   <script src=\"/js/dev/projects.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "desktop/projects/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 54,  102 => 53,  95 => 48,  92 => 47,  74 => 31,  57 => 13,  53 => 12,  47 => 9,  43 => 8,  38 => 7,  35 => 6,  30 => 3,  27 => 2,);
    }
}
