<?php

/* desktop/projects/view.tpl */
class __TwigTemplate_73f1791b1fee5bbdf3135a7fb1003689 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'preheadcss' => array($this, 'block_preheadcss'),
            'headcss' => array($this, 'block_headcss'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_preheadcss($context, array $blocks = array())
    {
        // line 4
        echo "     <link rel=\"stylesheet\" type=\"text/css\" href=\"/assets/lib/select2/css/select2.min.css\"/>
";
    }

    // line 8
    public function block_headcss($context, array $blocks = array())
    {
        // line 9
        echo "<link rel=\"stylesheet\" href=\"/assets/lib/date-time/datepicker.css\" />
<link rel=\"stylesheet\" href=\"/css/growl.css\">
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "<input type=\"hidden\" id=\"_pdt\" value=\"";
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "del_prj_title");
        echo "\">
<input type=\"hidden\" id=\"_pdm\" value=\"";
        // line 15
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "del_prj_message");
        echo "\">
<input type=\"hidden\" id=\"_yes\" value=\"";
        // line 16
        echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "btn_yes");
        echo "\">
<input type=\"hidden\" id=\"prj_id\" value=\"";
        // line 17
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
        echo "\">

<div class=\"page-head\">
  <h3 style=\"margin-top:5px;\">";
        // line 20
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_name");
        echo "  
\t  ";
        // line 21
        if (($this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_user_id") == $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id"))) {
            // line 22
            echo "      <a href=\"#\" data-id=\"";
            echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
            echo "\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\"  data-original-title=\"";
            echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "btn_del");
            echo "\" class=\"btn btn-head btn-space btn-danger pull-right delprj\">";
            echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "btn_del");
            echo "</a>
  ";
        }
        // line 24
        echo "      <a href=\"/issues/add/?prj=";
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
        echo "\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\"  data-original-title=\"";
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "cr_issue");
        echo "\" class=\"btn btn-head btn-space btn-success pull-right\">";
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "cr_issue");
        echo "</a>

     
      <a href=\"/projects\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\"  data-original-title=\"";
        // line 27
        echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "back_list");
        echo "\"  class=\"btn btn-dark btn-head btn-space pull-right\"><i class=\"material-icons btn-head-s\">arrow_back</i></a>
      
      
  </h3>
</div>

 <div class=\"main-content\" style=\"margin-bottom: 50px;\">
\t <input type=\"hidden\" id=\"prj_id\" value=\"";
        // line 34
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
        echo "\">
\t <div class=\"row\">
\t\t <div class=\"col-md-5 col-sm-12\">
\t\t\t<div class=\"panel panel-default panel-borders\" style=\"margin-bottom:5px;\">
\t        <div class=\"panel-heading\">
\t\t         ";
        // line 39
        if (((isset($context["role"]) ? $context["role"] : null) < 20)) {
            // line 40
            echo "\t\t        <div class=\"tools\" ><a  href=\"#\" id=\"btn-edit-details\" data-id=\"";
            echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
            echo "\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\"  data-original-title=\"";
            echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "btn_edit");
            echo "\"><span class=\"icon s7-note\" ></span></a></div>
\t\t        ";
        }
        // line 42
        echo "\t\t        <span class=\"title\">";
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "title_details");
        echo "</span>
\t\t    </div>
\t        <div class=\"panel-body\" >
\t\t       
\t\t        <div id=\"details-view\"></div> 
\t\t\t\t<div id=\"details-edit\" style=\"display: none;\"></div>
\t\t        
\t        </div>
\t\t\t</div>    

\t\t\t<div class=\"panel panel-default panel-borders\" id=\"m-block\" style=\"margin-bottom:5px;\">
\t        <div class=\"panel-heading\">
\t\t        ";
        // line 54
        if (((isset($context["role"]) ? $context["role"] : null) < 20)) {
            // line 55
            echo "\t\t        <div class=\"tools\" >
\t\t\t        <a href=\"#\" data-toggle=\"tooltip\" id=\"miles-edit-btn\"  data-placement=\"bottom\" data-original-title=\"";
            // line 56
            echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "btn_edit");
            echo "\">
\t\t\t\t        <span class=\"icon s7-note\" ></span>
\t\t\t\t    </a>
\t\t\t\t</div>
\t\t        ";
        }
        // line 61
        echo "\t\t        <span class=\"title\">";
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "tbl_miles");
        echo "</span>
\t\t    </div>
\t        <div class=\"panel-body\">
\t\t\t\t<div id=\"miles-view-box\" class=\"table-responsive view-box\">
\t\t\t\t\t";
        // line 65
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/projects/viewmiles.tpl"));
        $template->display($context);
        // line 66
        echo "\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"miles-edit-box\" class=\"edit-box\">
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t        </div>
\t\t\t</div>    
\t\t\t 
\t\t </div>
\t\t <div class=\"col-md-7 col-sm-12\">
\t\t\t<div class=\"row\"> 
              <div class=\"col-md-7\">
              \t<div class=\"widget widget-pie \" style=\"padding-bottom: 60px;\">
                <div class=\"widget-head\"><span class=\"title\">";
        // line 80
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "issue_status");
        echo "</span></div>
                 <div class=\"row chart-container\">
                  <div class=\"col-xs-6\">
                    <div id=\"issue-chart\" class=\"chart\"></div>
                  </div>
                  <div class=\"col-xs-6\">
                    <div class=\"legend\"></div>
                  </div>
                </div>
               
              </div>\t
              </div>
              <div class=\"col-md-5\">
               <div class=\"widget widget-tile g-l box-go\" data-go=\"/issues/?prj=";
        // line 93
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
        echo "&status=10\" style=\"margin-bottom: 4px; padding-top: 7px; padding-bottom: 7px;\">
\t              <div class=\"data-info\">
\t                <div data-toggle=\"counter\" data-end=\"156\" class=\"value\">";
        // line 95
        echo (isset($context["issue_opens"]) ? $context["issue_opens"] : null);
        echo " <span class=\"text-muted\" style=\"font-size: smaller;\">(";
        echo (isset($context["issue_opens_percent"]) ? $context["issue_opens_percent"] : null);
        echo "%)</span></div>
\t                <div class=\"desc\" style=\"color: red;\">";
        // line 96
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "open_tasks");
        echo "</div>
\t              </div>
\t              <div class=\"icon\" style=\"color: red;\"><span class=\"s7-config\"></span></div>
\t            </div>
\t            
               <div class=\"widget widget-tile g-l box-go\" data-go=\"/issues/?prj=";
        // line 101
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
        echo "&status=50\"  style=\"margin-bottom: 4px; padding-top: 7px; padding-bottom: 7px;\">
\t              <div class=\"data-info\">
\t                <div data-toggle=\"counter\" data-end=\"156\" class=\"value\">";
        // line 103
        echo (isset($context["issue_closed"]) ? $context["issue_closed"] : null);
        echo " <span class=\"text-muted\" style=\"font-size: smaller;\">(";
        echo (isset($context["issue_closed_percent"]) ? $context["issue_closed_percent"] : null);
        echo "%)</span> </div>
\t                <div class=\"desc text-success\">";
        // line 104
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "closed_tasks");
        echo "</div>
\t              </div>
\t              <div class=\"icon \"  ><span class=\"s7-check text-success\"></span></div>
\t            </div>
               
               
               <div class=\"widget widget-tile g-l box-go\" data-go=\"/issues/?prj=";
        // line 110
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
        echo "\" style=\"margin-bottom: 4px; padding-top: 7px; padding-bottom: 7px;\">
\t              <div class=\"data-info\">
\t                <div data-toggle=\"counter\" data-end=\"156\" class=\"value\">";
        // line 112
        echo (isset($context["issue_other"]) ? $context["issue_other"] : null);
        echo " <span class=\"text-muted\" style=\"font-size: smaller;\">(";
        echo (isset($context["issue_other_percent"]) ? $context["issue_other_percent"] : null);
        echo "%)</span></div>
\t                <div class=\"desc \">";
        // line 113
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "other_tasks");
        echo "</div>
\t              </div>
\t              <div class=\"icon \"><span class=\"s7-plugin \"></span></div>
\t            </div>
               <div class=\"text-center\">
\t               <a class=\"g-l\" data-go=\"/issues/?prj=";
        // line 118
        echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
        echo "\" href=\"#\">";
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "vt");
        echo "</a>
               </div>    
              </div>    

\t\t\t</div>  
\t\t\t
\t\t\t<div class=\"panel panel-default panel-borders\" style=\"margin-bottom:5px;\">
\t        <div class=\"panel-heading\">
\t\t        ";
        // line 126
        if (((isset($context["role"]) ? $context["role"] : null) < 20)) {
            // line 127
            echo "\t\t        <div class=\"tools\"><a href=\"#\" data-id=\"";
            echo $this->getAttribute((isset($context["prj"]) ? $context["prj"] : null), "project_id");
            echo "\" id=\"btn-collab-edit\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\"  data-original-title=\"";
            echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "btn_edit");
            echo "\"><span class=\"icon s7-note\" ></span></a></div>
\t\t        ";
        }
        // line 129
        echo "\t\t        <span class=\"title\">";
        echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "tbl_collabs");
        echo "</span>
\t\t    </div>
\t        <div class=\"panel-body\">
\t\t\t\t<div id=\"collab-view\"></div>
\t\t\t\t<div id=\"collab-edit\" style=\"display: none;\"></div>
\t        </div>
\t\t\t</div> 
\t\t\t   \t\t 
\t\t </div>\t
\t </div>\t 
 </div>\t 
 
 ";
    }

    // line 142
    public function block_js($context, array $blocks = array())
    {
        // line 143
        echo "<script src=\"/assets/lib/date-time/bootstrap-datepicker.min.js\"></script>\t
    <script src=\"/assets/lib/bootstrap-growl/bootstrap-growl.min.js\"></script>
    <script src=\"/assets/lib/jquery-flot/jquery.flot.js\" type=\"text/javascript\"></script>
    <script src=\"/assets/lib/jquery-flot/jquery.flot.pie.js\" type=\"text/javascript\"></script>
    <script src=\"/assets/lib/jquery-flot/jquery.flot.resize.js\" type=\"text/javascript\"></script>    
        <script src=\"/assets/lib/select2/js/select2.min.js\" type=\"text/javascript\"></script>
   <script src=\"/js/dev/projects_view.js?v=";
        // line 149
        echo (isset($context["hashver"]) ? $context["hashver"] : null);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "desktop/projects/view.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  303 => 149,  295 => 143,  292 => 142,  274 => 129,  266 => 127,  264 => 126,  251 => 118,  243 => 113,  237 => 112,  232 => 110,  223 => 104,  217 => 103,  212 => 101,  204 => 96,  198 => 95,  193 => 93,  177 => 80,  161 => 66,  158 => 65,  150 => 61,  142 => 56,  139 => 55,  137 => 54,  121 => 42,  113 => 40,  111 => 39,  103 => 34,  93 => 27,  82 => 24,  72 => 22,  70 => 21,  66 => 20,  60 => 17,  56 => 16,  52 => 15,  47 => 14,  44 => 13,  38 => 9,  35 => 8,  30 => 4,  27 => 3,);
    }
}
