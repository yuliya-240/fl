<?php

/* desktop/projects/footer.tpl */
class __TwigTemplate_2187d9d8201c118ce33a8f4f0bb44638 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer class=\"hidden-xs\">
    <div class=\"am-checkbox m-l-18 m-b-5 pull-left\" style=\"margin-top:-2px; padding: 0; margin-right: 5px;\">
      <input type=\"checkbox\" id=\"checkall\">
      <label for=\"checkall\"></label>
    </div>
\t<span >            \t\t
\t<a href=\"#\" id=\"bundleDelete\" class=\"text-danger hidden-xs\" style=\"margin-right: 5px;\" >";
        // line 7
        echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "btn_del");
        echo "</a>
\t
\t</span>\t
\t";
        // line 10
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous")) {
            echo " 
\t<a href=\"#\" data-page=\"";
            // line 11
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous");
            echo "\" class=\" prevp\" ><i class=\"fa fa-arrow-left\"></i></a>
\t";
        }
        // line 13
        echo "\t<span >";
        echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "info");
        echo "</span>
\t";
        // line 14
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next")) {
            // line 15
            echo "\t<a href=\"#\" data-page=\"";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next");
            echo "\" class=\"nextp\" ><i class=\"fa fa-arrow-right\"></i></a>
\t";
        }
        // line 17
        echo "
</footer>


";
    }

    public function getTemplateName()
    {
        return "desktop/projects/footer.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 15,  45 => 14,  40 => 13,  35 => 11,  31 => 10,  25 => 7,  131 => 44,  124 => 41,  121 => 40,  114 => 35,  103 => 32,  97 => 31,  91 => 30,  85 => 29,  77 => 28,  74 => 27,  68 => 24,  62 => 23,  59 => 22,  57 => 21,  53 => 17,  49 => 18,  41 => 13,  37 => 12,  33 => 11,  29 => 10,  19 => 2,  17 => 1,);
    }
}
