<?php

/* desktop/projects/page.tpl */
class __TwigTemplate_102057fa340f48babc26100bf8941581 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["records"]) ? $context["records"] : null)) {
            // line 2
            echo "<div class=\"table-responsive\">
<form id=\"flist\">
<table class=\"table  table-hover\">
\t<thead>
\t\t<tr>
\t\t\t<th width=\"1%\" class=\"text-center hidden-xs\">

\t\t\t</th>
\t\t\t<th>";
            // line 10
            echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "tbl_status");
            echo "</th>
\t\t\t<th>";
            // line 11
            echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "tbl_name");
            echo "</th>
\t\t\t<th>";
            // line 12
            echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "tbl_desc");
            echo "</th>
\t\t\t<th>";
            // line 13
            echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "tbl_creator");
            echo "</th>
\t\t\t<th width=\"1%\"></th>
\t\t</tr>
\t</thead>
\t<tbody>
\t\t";
            // line 18
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 19
                echo "\t\t<tr>
\t\t\t<td class=\"text-center hidden-xs\" style=\"vertical-align:middle;\">
\t           ";
                // line 21
                if (($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_user_id") == $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id"))) {
                    // line 22
                    echo "\t            <div class=\"am-checkbox\">
\t              <input type=\"checkbox\" id=\"chk";
                    // line 23
                    echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_id");
                    echo "\" class=\"check\" name=\"row[]\" value=\"";
                    echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_id");
                    echo "\">
\t              <label for=\"chk";
                    // line 24
                    echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_id");
                    echo "\"></label>
\t            </div>
\t            ";
                }
                // line 27
                echo "\t\t\t</td>
\t\t\t<td class=\"tbl-pointer text-";
                // line 28
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "prjstatus_color");
                echo "\" data-id=\"";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_id");
                echo "\" >";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "prjstatus_name");
                echo "</td>
\t\t\t<td class=\"tbl-pointer\" data-id=\"";
                // line 29
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_id");
                echo "\">";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_name");
                echo "</td>
\t\t\t<td class=\"tbl-pointer\" data-id=\"";
                // line 30
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_id");
                echo "\">";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_desc");
                echo "</td>
\t\t\t<td class=\"tbl-pointer\" data-id=\"";
                // line 31
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_id");
                echo "\">";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_username");
                echo "</td>
\t\t\t<td class=\"tbl-pointer\" data-id=\"";
                // line 32
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_id");
                echo "\"><a href=\"#\" class=\"del\" data-id=\"";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "project_id");
                echo "\"><i class=\"material-icons\">visibility</i></a></td>
\t\t</tr>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 35
            echo "\t</tbody>
</table>\t\t\t\t
</form>
</div>
";
        } else {
            // line 40
            echo "\t<div class=\"text-center\">
\t\t";
            // line 41
            echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "norecords");
            echo "
\t</div>\t

";
        }
        // line 44
        echo "\t\t\t
";
    }

    public function getTemplateName()
    {
        return "desktop/projects/page.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 44,  124 => 41,  121 => 40,  114 => 35,  103 => 32,  97 => 31,  91 => 30,  85 => 29,  77 => 28,  74 => 27,  68 => 24,  62 => 23,  59 => 22,  57 => 21,  53 => 19,  49 => 18,  41 => 13,  37 => 12,  33 => 11,  29 => 10,  19 => 2,  17 => 1,);
    }
}
