<?php

/* desktop/projects/viewmiles.tpl */
class __TwigTemplate_cf756ba1f8384e71f0096a858d7ab903 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["milestones"]) ? $context["milestones"] : null)) {
            // line 2
            echo "<table class=\"table\">
\t";
            // line 3
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["milestones"]) ? $context["milestones"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
                // line 4
                echo "\t<tr>
\t\t<td width=\"60%\">";
                // line 5
                echo $this->getAttribute((isset($context["m"]) ? $context["m"] : null), "prjm_name");
                echo "</td>
\t\t<td align=\"right\">";
                // line 6
                echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["m"]) ? $context["m"] : null), "prjm_deadline"), $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_dformat"));
                echo "</td>
\t</tr>\t\t
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 9
            echo "</table>\t
";
        } else {
            // line 11
            echo "<div class=\"text-center text-danger\">";
            echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "miles_not_def");
            echo "</div>
";
        }
    }

    public function getTemplateName()
    {
        return "desktop/projects/viewmiles.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 11,  42 => 9,  33 => 6,  29 => 5,  26 => 4,  22 => 3,  19 => 2,  17 => 1,  303 => 149,  295 => 143,  292 => 142,  274 => 129,  266 => 127,  264 => 126,  251 => 118,  243 => 113,  237 => 112,  232 => 110,  223 => 104,  217 => 103,  212 => 101,  204 => 96,  198 => 95,  193 => 93,  177 => 80,  161 => 66,  158 => 65,  150 => 61,  142 => 56,  139 => 55,  137 => 54,  121 => 42,  113 => 40,  111 => 39,  103 => 34,  93 => 27,  82 => 24,  72 => 22,  70 => 21,  66 => 20,  60 => 17,  56 => 16,  52 => 15,  47 => 14,  44 => 13,  38 => 9,  35 => 8,  30 => 4,  27 => 3,);
    }
}
