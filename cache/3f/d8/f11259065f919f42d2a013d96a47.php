<?php

/* desktop/login/index.tpl */
class __TwigTemplate_3fd8f11259065f919f42d2a013d96a47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">
    <title>";
        // line 8
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "title");
        echo "</title>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/assets/lib/stroke-7/style.css\"/>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/assets/lib/jquery.nanoscroller/css/nanoscroller.css\"/><!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
    
    <link rel=\"stylesheet\" href=\"/assets/css/style.css\" type=\"text/css\"/>
    <link rel=\"stylesheet\" href=\"/assets/css/themes/theme-google.min.css\" type=\"text/css\"/>
    <link rel=\"stylesheet\" href=\"/assets/lib/sweetalert/sweetalert.min.css\" type=\"text/css\"/>
    <link rel=\"stylesheet\" href=\"/css/app.css\" type=\"text/css\"/>
\t<link rel=\"stylesheet\" href=\"/css/login.css\" type=\"text/css\"/>
  </head>
  <body class=\"am-splash-screen\">
\t<!-- begin #page-loader -->

\t<div id=\"process-loader\" class=\"fade\">
\t    <div class=\"material-loader\">
\t        <svg class=\"circular\" viewBox=\"25 25 50 50\">
\t            <circle class=\"path\" cx=\"50\" cy=\"50\" r=\"20\" fill=\"none\" stroke-width=\"2\" stroke-miterlimit=\"10\"/>
\t        </svg>
\t        <div class=\"message\">";
        // line 29
        echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "loader");
        echo "</div>
\t    </div>
\t</div>
\t<!-- end #page-loader -->
\t<div id=\"login-box\" class=\"login  animated fadeInDown\">  
    <div class=\"am-wrapper am-login\">
      <div class=\"am-content\">
        <div class=\"main-content\">
          <div class=\"login-container\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\"><img src=\"/logo/login_signup_black.png\" alt=\"logo\"  class=\"logo-img\"><span>";
        // line 39
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "desc");
        echo "</span></div>
              <div class=\"panel-body\">
                <form  id=\"flogin\" class=\"form-horizontal\">
                  <div class=\"login-form\">
                    <div class=\"form-group\">
\t                    
                      <div id=\"email-handler\" class=\"input-group\"><span class=\"input-group-addon\"><i class=\"icon s7-mail\"></i></span>
                        <input type=\"email\" id=\"email\" name=\"email\" required=\"\" data-errorbox=\"#error-box\"  placeholder=\"E-mail\" autocomplete=\"off\" data-msg-required=\"";
        // line 46
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "email_req");
        echo "\" data-msg-email=\"";
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "not_email");
        echo "\" autofocus=\"\" class=\"form-control\">
                      </div>
                      <div id=\"error-box\" style=\"margin-top:5px;\"></div>
                    </div>
                    <div class=\"form-group\">
\t                    
                        <div id=\"password-handler\" class=\"input-group\"><span class=\"input-group-addon\"><i class=\"icon s7-lock\"></i></span>
                          <input name=\"password\" id=\"pass\" type=\"password\" data-errorbox=\"#error-box-pass\"  placeholder=\"";
        // line 53
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "placeholder_pass");
        echo "\" data-msg-required=\"";
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "pass_req");
        echo "\"  required=\"\" class=\"form-control\">
                        </div>
                        <div id=\"error-box-pass\" style=\"margin-top:5px;\"></div>
                    </div>
                    <div class=\"form-group login-submit\">
                      <button data-dismiss=\"modal\" type=\"submit\" class=\"btn btn-primary btn-lg\">";
        // line 58
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "btn_login");
        echo "</button>
                    </div>
                    <div class=\"form-group footer row\">
                      <div class=\"col-xs-6\"><a href=\"#\" id=\"act-forget\">";
        // line 61
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "btn_forget");
        echo "</a></div>
                      <div class=\"col-xs-6 remember\">
                        <label for=\"remember\">";
        // line 63
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "btn_remember");
        echo "</label>
                        <div class=\"am-checkbox\">
                          <input type=\"checkbox\" id=\"remember\">
                          <label for=\"remember\"></label>
                        </div>
                      </div>
                    </div>

                    <div class=\"form-group footer row\">
                      <div class=\"col-xs-12 text-center\"><a href=\"/signup\">";
        // line 72
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "btn_createaccount");
        echo "</a></div>
                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
\t</div>
\t
\t<div id=\"reset-box\" class=\"login  animated fadeInDown\" style=\"display:none;\">
    
    <div class=\"am-wrapper am-login\">
      <div class=\"am-content\">
        <div class=\"main-content\">
          <div class=\"login-container\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\"><img src=\"/logo/login_signup_black.png\" alt=\"logo\"  class=\"logo-img\"><span>";
        // line 92
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "res_pass");
        echo "</span></div>
              <div class=\"panel-body\">
                <form  id=\"freset\" class=\"form-horizontal\">
                  <div class=\"login-form\">
\t                <div role=\"alert\" class=\"alert alert-warning alert-icon alert-dismissible\">
                    <div class=\"icon\"><span class=\"s7-attention\"></span></div>
                    <div class=\"message\">
                      <strong>";
        // line 99
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "fear_not");
        echo "</strong>";
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "instr");
        echo "
                    </div>
                  </div>
                    <div class=\"form-group\">
\t                    
                      <div id=\"reset-email-handler\" class=\"input-group\"><span class=\"input-group-addon\"><i class=\"icon s7-mail\"></i></span>
                        <input type=\"email\" id=\"remail\" name=\"remail\" required=\"\" data-errorbox=\"#reset-error-box\"  placeholder=\"E-mail\" autocomplete=\"off\" data-msg-required=\"";
        // line 105
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "email_req");
        echo "\" data-msg-email=\"";
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "not_email");
        echo "\"  class=\"form-control\">
                      </div>
                      <div id=\"reset-error-box\" style=\"margin-top:5px;\"></div>
                    </div>
                  
                    <div class=\"form-group login-submit\">
                      <button data-dismiss=\"modal\" type=\"submit\" class=\"btn btn-primary btn-lg\">";
        // line 111
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "res_pass");
        echo "</button>
                    </div>
              

                    <div class=\"form-group footer row\">
                      <div class=\"col-xs-12 text-center\"><a href=\"#\" class=\"back-login\">";
        // line 116
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "let_log");
        echo "</a></div>
                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
\t</div>

\t<div id=\"done-box\" class=\"login  animated fadeInDown\" style=\"display: none;\">  
    <div class=\"am-wrapper am-login\">
      <div class=\"am-content\">
        <div class=\"main-content\">
          <div class=\"login-container\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\"><img src=\"/logo/login_signup_black.png\" alt=\"logo\"  class=\"logo-img\"></div>
              <div class=\"panel-body\">
            
\t            
                  <div class=\"login-form\">
            
\t\t\t\t  <div role=\"alert\" class=\"alert alert-success alert-icon alert-border-color alert-dismissible\">
                    <div class=\"icon\"><span class=\"s7-check\"></span></div>
                    <div class=\"message\">
                     <strong>";
        // line 145
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "good");
        echo "</strong>";
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "sent_inst");
        echo "
                    </div>
                  </div>                  

                    <div class=\"form-group footer row\">
                      <div class=\"col-xs-12 text-center\"><a class=\"back-login\" href=\"#\">";
        // line 150
        echo $this->getAttribute((isset($context["lng_login"]) ? $context["lng_login"] : null), "log_now");
        echo "</a></div>
                    </div>

                  </div>
    
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
\t</div>
\t
    <script src=\"/assets/lib/jquery/jquery.min.js\" type=\"text/javascript\"></script>
    <script src=\"/assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js\" type=\"text/javascript\"></script>
    <script src=\"/assets/js/main.js\" type=\"text/javascript\"></script>
    <script src=\"/assets/lib/bootstrap/dist/js/bootstrap.min.js\" type=\"text/javascript\"></script>
\t<script src=\"/assets/lib/jquery-validate/jquery.validate.min.js\" type=\"text/javascript\"></script>
\t<script src=\"/assets/lib/sweetalert/sweetalert.min.js\" type=\"text/javascript\"></script>
\t<script src=\"/js/dev/login.js\" type=\"text/javascript\"></script>
  </body>
</html>";
    }

    public function getTemplateName()
    {
        return "desktop/login/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 150,  213 => 145,  181 => 116,  173 => 111,  162 => 105,  151 => 99,  141 => 92,  118 => 72,  106 => 63,  101 => 61,  95 => 58,  85 => 53,  73 => 46,  63 => 39,  50 => 29,  26 => 8,  17 => 1,);
    }
}
