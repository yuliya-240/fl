<?php

/* desktop/projects/collabs_view.tpl */
class __TwigTemplate_7ebddb055dcbd91adb2e0f4671872adf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"table\">


";
        // line 4
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["collabs"]) ? $context["collabs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 5
            echo "<tr>
\t<td>";
            // line 6
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "user_username");
            echo " (";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "user_name");
            echo ")</td>
\t<td><span class=\"text-danger\">";
            // line 7
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "open");
            echo "</span> / <span class=\"text-success\">";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "close");
            echo "</span></td>
\t<td align=\"right\">
\t\t";
            // line 9
            if (($this->getAttribute((isset($context["c"]) ? $context["c"] : null), "prju_role") == 1)) {
                echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "role_owner");
            }
            // line 10
            echo "\t\t";
            if (($this->getAttribute((isset($context["c"]) ? $context["c"] : null), "prju_role") == 10)) {
                echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "role_manager");
            }
            // line 11
            echo "\t\t";
            if (($this->getAttribute((isset($context["c"]) ? $context["c"] : null), "prju_role") == 20)) {
                echo $this->getAttribute((isset($context["lng_projects"]) ? $context["lng_projects"] : null), "role_collab");
            }
            // line 12
            echo "\t\t
\t</td>
</tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 16
        echo "</table>
\t
";
    }

    public function getTemplateName()
    {
        return "desktop/projects/collabs_view.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 16,  56 => 12,  51 => 11,  46 => 10,  42 => 9,  35 => 7,  29 => 6,  26 => 5,  22 => 4,  17 => 1,);
    }
}
