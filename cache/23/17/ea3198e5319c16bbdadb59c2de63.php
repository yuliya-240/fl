<?php

/* desktop/common/nav.tpl */
class __TwigTemplate_2317ea3198e5319c16bbdadb59c2de63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "      <nav class=\"navbar navbar-default navbar-fixed-top am-top-header\">
        <div class=\"container-fluid\">
          <div class=\"navbar-header\">
            <div class=\"page-title\"><span>";
        // line 4
        echo (isset($context["page_title"]) ? $context["page_title"] : null);
        echo "</span></div>
            <a href=\"#\" class=\"am-toggle-left-sidebar navbar-toggle collapsed\">
\t            <span class=\"icon-bar\">
\t            <span></span>
\t            <span></span>
\t            <span></span>
\t            </span></a>
\t\t\t\t<a href=\"/\" class=\"navbar-brand\"></a>
          </div>
          ";
        // line 14
        echo "          <a href=\"#\" data-toggle=\"collapse\" data-target=\"#am-navbar-collapse\" class=\"am-toggle-top-header-menu collapsed\">
\t          <span class=\"icon s7-angle-down\"></span>
\t      </a>
          <div id=\"am-navbar-collapse\" class=\"collapse navbar-collapse\">
            <ul class=\"nav navbar-nav navbar-right am-user-nav\">
              <li class=\"dropdown\">
              <a href=\"#\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\" class=\"dropdown-toggle\">
\t              <img id=\"u-ava-min\" class=\"img-responsible\" ";
        // line 21
        if (($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_pic") == "nopic.png")) {
            echo "src=\"/img/avatar/avatar2x200.jpg\"";
        } else {
            echo "src=\"/userfiles/";
            echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id");
            echo "/pic/";
            echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_pic");
            echo "\"";
        }
        echo ">
\t              <span class=\"user-name\">";
        // line 22
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_name");
        echo "</span><span class=\"angle-down s7-angle-down\"></span>
\t          </a>
                <ul role=\"menu\" class=\"dropdown-menu\">
                  <li><a href=\"/profile\"> <span class=\"icon s7-user\"></span>";
        // line 25
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "profile");
        echo "</a></li>
                  <li><a href=\"/settings\"> <span class=\"icon s7-config\"></span>";
        // line 26
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "settings");
        echo "</a></li>
                  <li><a href=\"/logout\"> <span class=\"icon s7-power\"></span>";
        // line 27
        echo $this->getAttribute((isset($context["lng_common"]) ? $context["lng_common"] : null), "exit");
        echo "</a></li>
                </ul>
              </li>
            </ul>
            
            <ul class=\"nav navbar-nav am-nav-right\">
              <li><a href=\"/\">";
        // line 33
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "dash");
        echo "</a></li>
              <li><a href=\"/notes\">";
        // line 34
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "notes");
        echo "</a></li>
              <li><a href=\"/portfolio\">";
        // line 35
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "portfolio");
        echo "</a></li>
       
             ";
        // line 46
        echo "            </ul>
            
            <ul class=\"nav navbar-nav navbar-right am-icons-nav\">
\t           ";
        // line 74
        echo "              <li class=\"dropdown\"><a href=\"#\"  role=\"button\" id=\"showMessanger\" ><span class=\"icon s7-comment\"></span><span id=\"msgUnreadCount\" ";
        if ((!(isset($context["isUnread"]) ? $context["isUnread"] : null))) {
            echo "style=\"display: none;\"";
        }
        echo " class=\"indicator\"></span></a>
               </li>
              <li class=\"dropdown\"><a href=\"#\" id=\"notifyBox\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\" class=\"dropdown-toggle\"><span class=\"icon s7-bell\"></span><span id=\"notifyCount\" class=\"indicator\" ";
        // line 76
        if ((!(isset($context["previewNotifyCount"]) ? $context["previewNotifyCount"] : null))) {
            echo "style=\"display:none;\"";
        }
        echo "></span></a>
                <ul class=\"dropdown-menu am-notifications\">
                  <li>
                    <div class=\"title\">";
        // line 79
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "n_title");
        echo "<span class=\"badge\">";
        echo (isset($context["previewNotifyCount"]) ? $context["previewNotifyCount"] : null);
        echo "</span></div>
                    <div class=\"list\">
                      <div class=\"am-scroller nano\">
                        <div class=\"content nano-content\">
                          <ul>
\t                        ";
        // line 84
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["noteficationsList"]) ? $context["noteficationsList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["n"]) {
            echo "  
                            <li ";
            // line 85
            if (($this->getAttribute((isset($context["n"]) ? $context["n"] : null), "n_status_preview") == 0)) {
                echo "class=\"active n-list\"";
            }
            echo ">
                            \t<a href=\"";
            // line 86
            echo $this->getAttribute((isset($context["n"]) ? $context["n"] : null), "n_link");
            echo "\">
\t                                <div class=\"logo\">
\t\t                            ";
            // line 88
            if (($this->getAttribute((isset($context["n"]) ? $context["n"] : null), "n_src") == 0)) {
                // line 89
                echo "\t\t                                <span class=\"icon s7-gleam\"></span>
\t\t                            ";
            }
            // line 91
            echo "\t\t                            
\t\t                            ";
            // line 92
            if (($this->getAttribute((isset($context["n"]) ? $context["n"] : null), "n_src") == 1)) {
                // line 93
                echo "\t\t                                <span class=\"icon s7-id\"></span>
\t\t                            ";
            }
            // line 95
            echo "\t\t                            
\t\t                            </div>
\t                                <div class=\"user-content\">
\t\t                                ";
            // line 98
            if (($this->getAttribute((isset($context["n"]) ? $context["n"] : null), "n_status_preview") == 0)) {
                echo "<span class=\"circle\"></span>";
            }
            // line 99
            echo "\t\t                         
\t\t                                <span class=\"text-content\"> ";
            // line 100
            echo $this->getAttribute((isset($context["n"]) ? $context["n"] : null), "ntype_desc");
            echo "</span>
\t\t                                <span class=\"date\"><time datetime=\"";
            // line 101
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["n"]) ? $context["n"] : null), "n_date"), "Y-m-d");
            echo "T";
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["n"]) ? $context["n"] : null), "n_date"), "H:i:s");
            echo "\" class=\"timeago\"></time></span>
\t\t                            </div>
\t                            </a>
\t                        </li>
\t                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['n'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 106
        echo "                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class=\"footer\"> <a href=\"#\">";
        // line 110
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "n_view_all");
        echo "</a></div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
";
    }

    public function getTemplateName()
    {
        return "desktop/common/nav.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 106,  170 => 101,  166 => 100,  163 => 99,  159 => 98,  154 => 95,  150 => 93,  148 => 92,  145 => 91,  141 => 89,  134 => 86,  122 => 84,  112 => 79,  104 => 76,  96 => 74,  91 => 46,  86 => 35,  82 => 34,  78 => 33,  69 => 27,  65 => 26,  61 => 25,  55 => 22,  43 => 21,  34 => 14,  22 => 4,  17 => 1,  233 => 110,  228 => 108,  223 => 67,  218 => 57,  213 => 26,  208 => 18,  201 => 111,  199 => 110,  196 => 109,  194 => 108,  189 => 110,  182 => 102,  178 => 101,  174 => 99,  155 => 81,  139 => 88,  128 => 85,  126 => 57,  119 => 54,  114 => 53,  110 => 52,  106 => 51,  102 => 50,  98 => 49,  93 => 47,  88 => 45,  84 => 44,  80 => 43,  73 => 39,  63 => 32,  56 => 27,  54 => 26,  47 => 21,  44 => 19,  42 => 18,  23 => 1,  27 => 4,  24 => 3,);
    }
}
