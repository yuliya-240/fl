<?php

/* desktop/common/sidebar_left.tpl */
class __TwigTemplate_2615fd7c88c789d749eb2252be84cc20 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "      <div class=\"am-left-sidebar\">
        <div class=\"content\">
          <div class=\"am-logo\"></div>
          <ul class=\"sidebar-elements\">
     
\t        <li class=\"";
        // line 6
        echo (isset($context["sbPeople"]) ? $context["sbPeople"] : null);
        echo "\">
\t          <a href=\"#\"><i class=\"icon s7-users\"></i><span>";
        // line 7
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "ppl");
        echo "</span></a>
\t          <ul class=\"sub-menu\">
\t           <li class=\"";
        // line 9
        echo (isset($context["subClients"]) ? $context["subClients"] : null);
        echo "\"><a href=\"#\" class=\"g-l\" data-go=\"/clients\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "clients");
        echo "</a></li>
\t           <li class=\"";
        // line 10
        echo (isset($context["subCollabs"]) ? $context["subCollabs"] : null);
        echo "\"><a href=\"#\" class=\"g-l\" data-go=\"/users\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "colleague");
        echo "</a></li>
\t           <li class=\"";
        // line 11
        echo (isset($context["subInvites"]) ? $context["subInvites"] : null);
        echo "\"><a href=\"#\" class=\"g-l\" data-go=\"/users/invites\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "invites");
        echo " ";
        if ((isset($context["countInvites"]) ? $context["countInvites"] : null)) {
            echo "&nbsp;<span class=\"text-danger\">";
            echo (isset($context["countInvites"]) ? $context["countInvites"] : null);
            echo "</span>";
        }
        echo "</a></li>
\t          </ul>
\t        </li>
\t       
\t        <li class=\"";
        // line 15
        echo (isset($context["sbMoney"]) ? $context["sbMoney"] : null);
        echo "\"><a href=\"#\"><i class=\"icon s7-cash\"></i><span>";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "money");
        echo "</span></a>
\t          <ul class=\"sub-menu\">
\t           <li class=\"";
        // line 17
        echo (isset($context["subInvoices"]) ? $context["subInvoices"] : null);
        echo "\"><a href=\"#\" class=\"g-l\" data-go=\"/invoices\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "inv");
        echo "</a></li>
\t           <li class=\"";
        // line 18
        echo (isset($context["subInvoicesRec"]) ? $context["subInvoicesRec"] : null);
        echo "\"><a href=\"#\" class=\"g-l\" data-go=\"/invoices/recurring\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "invrec");
        echo "</a></li>
\t           <li class=\"";
        // line 19
        echo (isset($context["subEstimates"]) ? $context["subEstimates"] : null);
        echo "\"><a href=\"/estimates\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "est");
        echo "</a></li>
\t           <li class=\"";
        // line 20
        echo (isset($context["subTransactions"]) ? $context["subTransactions"] : null);
        echo "\"><a href=\"/transactions\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "trans");
        echo "</a></li>
\t           <li class=\"";
        // line 21
        echo (isset($context["subExpenses"]) ? $context["subExpenses"] : null);
        echo "\"><a href=\"/expenses\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "exp");
        echo "</a></li>
               <li class=\"";
        // line 22
        echo (isset($context["subExpensesCat"]) ? $context["subExpensesCat"] : null);
        echo "\"><a href=\"/expenses/categories\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "expcat");
        echo "</a></li>
\t           
\t          </ul>
\t        </li>
            
            <li class=\"";
        // line 27
        echo (isset($context["sbRes"]) ? $context["sbRes"] : null);
        echo "\"><a href=\"#\"><i class=\"icon s7-plugin\"></i><span>";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "res");
        echo "</span></a>
              <ul class=\"sub-menu\">
\t            <li class=\"";
        // line 29
        echo (isset($context["subPortfolio"]) ? $context["subPortfolio"] : null);
        echo "\"><a href=\"/portfolio\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "portfolio");
        echo "</a></li>
                <li class=\"";
        // line 30
        echo (isset($context["subSrv"]) ? $context["subSrv"] : null);
        echo "\"><a href=\"/services\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "srv");
        echo "</a></li>
                <li class=\"";
        // line 31
        echo (isset($context["subEquip"]) ? $context["subEquip"] : null);
        echo "\"><a href=\"/equip\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "equip");
        echo "</a></li>
              </ul>
            </li>

            <li class=\"";
        // line 35
        echo (isset($context["sbProjects"]) ? $context["sbProjects"] : null);
        echo "\"><a href=\"#\"><i class=\"icon s7-portfolio\"></i><span>";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "projects");
        echo "</span></a>
              <ul class=\"sub-menu\">
                <li class=\"";
        // line 37
        echo (isset($context["subPrj"]) ? $context["subPrj"] : null);
        echo "\"><a href=\"/projects\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "projects");
        echo "</a></li>
                <li class=\"";
        // line 38
        echo (isset($context["subIssues"]) ? $context["subIssues"] : null);
        echo "\"><a href=\"/issues\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "bugtracker");
        echo "</a></li>
              </ul>
            </li>
            
            <li class=\"";
        // line 42
        echo (isset($context["sbCalendar"]) ? $context["sbCalendar"] : null);
        echo "\"><a href=\"#\"><i class=\"icon icon s7-date\"></i><span>";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "cal");
        echo "</span></a>
              <ul class=\"sub-menu\">
\t            <li class=\"";
        // line 44
        echo (isset($context["subDoc"]) ? $context["subDoc"] : null);
        echo "\"><a href=\"/calendar\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "cal_shedule");
        echo "</a></li>
                <li class=\"";
        // line 45
        echo (isset($context["subNotes"]) ? $context["subNotes"] : null);
        echo "\"><a href=\"/calendar/recurring\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "cal_rec");
        echo "</a></li>
              </ul>
            </li>

            <li class=\"";
        // line 49
        echo (isset($context["sbDoc"]) ? $context["sbDoc"] : null);
        echo "\"><a href=\"#\"><i class=\"icon icon s7-note\"></i><span>";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "doc");
        echo "</span></a>
              <ul class=\"sub-menu\">
\t            <li class=\"";
        // line 51
        echo (isset($context["subDoc"]) ? $context["subDoc"] : null);
        echo "\"><a href=\"/doc\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "doc");
        echo "</a></li>
                <li class=\"";
        // line 52
        echo (isset($context["subNotes"]) ? $context["subNotes"] : null);
        echo "\"><a href=\"/notes\">";
        echo $this->getAttribute((isset($context["lng_menu"]) ? $context["lng_menu"] : null), "notes");
        echo "</a></li>
              </ul>
            </li>
            
          </ul>
          <!--Sidebar bottom content-->
        </div>
      </div>";
    }

    public function getTemplateName()
    {
        return "desktop/common/sidebar_left.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 52,  186 => 51,  179 => 49,  164 => 44,  157 => 42,  142 => 37,  135 => 35,  120 => 30,  107 => 27,  97 => 22,  85 => 20,  79 => 19,  67 => 17,  60 => 15,  45 => 11,  39 => 10,  33 => 9,  28 => 7,  183 => 106,  170 => 45,  166 => 100,  163 => 99,  159 => 98,  154 => 95,  150 => 93,  148 => 38,  145 => 91,  141 => 89,  134 => 86,  122 => 84,  112 => 79,  104 => 76,  96 => 74,  91 => 21,  86 => 35,  82 => 34,  78 => 33,  69 => 27,  65 => 26,  61 => 25,  55 => 22,  43 => 21,  34 => 14,  22 => 4,  17 => 1,  233 => 110,  228 => 108,  223 => 67,  218 => 57,  213 => 26,  208 => 18,  201 => 111,  199 => 110,  196 => 109,  194 => 108,  189 => 110,  182 => 102,  178 => 101,  174 => 99,  155 => 81,  139 => 88,  128 => 85,  126 => 31,  119 => 54,  114 => 29,  110 => 52,  106 => 51,  102 => 50,  98 => 49,  93 => 47,  88 => 45,  84 => 44,  80 => 43,  73 => 18,  63 => 32,  56 => 27,  54 => 26,  47 => 21,  44 => 19,  42 => 18,  23 => 1,  27 => 4,  24 => 6,);
    }
}
