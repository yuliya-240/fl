<?php
namespace floctopus\models\logic;

use \floctopus\application as app;
use \floctopus\models\orm\paypal as OrmPaypal; 
use \jet\libs\vpaypal as PAYPALVERIFYLIB;


class paypal extends \floctopus\models\common\model {   
	
	function verifyPaypal($email){
		
		PAYPALVERIFYLIB::load();
		
		$ppv=new \paypal_verify($email,'booper@ymail.com','Uspeh1977');
	
		if ($ppv->isLogin()){
			if ($ppv->isVerified()) {

				return true;
			
			}else{
				return false;
			}
			
		
		}else{
			return false;

		} 

		
	}
	
	function addTransaction($post,$senderID=0,$receiverID=0){
		
		 $paypal = new OrmPaypal();
		
         $pt['pt_txn_id']   =       	$post['txn_id'];
         $pt['pt_co_id']   =       		$receiverID;
         $pt['pt_payment_status'] = 	$post['payment_status'];
         $pt['pt_txn_type']       = 	$post['txn_type'];
         $pt['pt_payment_type']=		$post['payment_type'];
         $pt['pt_payer_lastname']=		$post['last_name'];
         $pt['pt_payer_firstname']=		$post['first_name'];
         $pt['pt_item_name']=			$post['item_name'];
         $pt['pt_item_id']=			    $post['item_number'];
         $pt['pt_qty']=					$post['quantity'];
         $pt['pt_paypal_payer_id']=		$post['payer_id'];
         $pt['pt_paypal_receiver_id']=	$post['receiver_id'];
         $pt['pt_inv_id']=				$post['item_number'];
         $pt['pt_business_email']=		$post['business'];//receiver_email
         $pt['pt_receiver_email']=		$post['receiver_email'];
         $pt['pt_payer_email']=			$post['payer_email']; //payer email
         $pt['pt_payer_user_id']=		$senderID;//payer_user_id
         $pt['pt_receiver_user_id']=	$receiverID;//$post['custom'];
         $pt['pt_payment_fee']=			$post['payment_fee'];
         $pt['pt_payment_gross']=		$post['payment_gross'];
         $pt['pt_mc_currency']=			$post['mc_currency'];
         $pt['pt_mc_fee']=				$post['mc_fee'];
         $pt['pt_mc_gross']=			$post['mc_gross'];
         $pt['pt_payment_amount']=		$post['payment_gross'];
         $pt['pt_verify_sign']=			$post['verify_sign'];
         
         if(isset($post['address_status'])) $pt['pt_address_status']=		$post['address_status'];
         if(isset($post['pending_reason']))$pt['pt_pending_reason']=		$post['pending_reason'];
         if(isset($post['settle_amount']))$pt['pt_settle_amount']=		$post['settle_amount'];
         if(isset($post['settle_currency']))$pt['pt_settle_currency']=		$post['settle_currency'];
         if(isset($post['exchange_rate']))$pt['pt_exchange_rate']=		$post['exchange_rate'];
         if(isset($post['invoice']))$pt['pt_invoice']=				$post['invoice'];
	
		  $paypal->addTransaction($pt);
		  
		  return true;
	}
	
}

?>