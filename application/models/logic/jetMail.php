<?php      
namespace floctopus\models\logic;

use \floctopus\application as app; 
use \floctopus\models\aws\mail as Mail;
use \floctopus\models\orm\accounts as OrmAccounts; 
use \floctopus\models\orm\invoices as OrmInvoices;
use \floctopus\models\orm\clients as OrmClients;
use \floctopus\models\orm\log as OrmLog;
use \floctopus\models\logic\validator as Validator;

use \jet\libs\Amazon as AWAMZON;
use Aws\Common\Credentials\Credentials;
use Aws\Ses\SesClient;

//use \jet\libs\MPDF as PDFLIB; 

class jetMail extends Mail {
	protected $view;
	private $company;
	private $inv;
	private $http_proto;
	
	function __construct() {
		parent::__construct();	
		
		
		$this->view = new \jet\twig(); 
		$this->view->setPath('email');
		if(isset($_SERVER['HTTPS'])); else $this->http_proto='http://';
		$this->log = new OrmLog();
		$this->awskey = app::$config->amazon['KEY_ID'];
		$this->awssecret = app::$config->amazon['KEY_SECRET'];
		$this->client = app::$config->site['client'];
	}

	function sendWelcome($email){
		
	
		AWAMZON::load();
	
		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	
		
	    $this->view->setTemplate('welcome_en.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		//app::trace(app::$config->mail['EMAIL_FROM']);
		
		$emailarr = array(
			'Source' => '"Floctopus"'.app::$config->mail['EMAIL_FROM'],
			//'Source' => app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				// Data is required
			        'Subject' => array(
			            // Data is required
			            'Data' => "Welcome ro Floctops",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                // Data is required
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                // Data is required
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array("robot@floctopus.com"),	
			
			);
		
	
		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
	}

	function sendIssueNotify($email,$id,$obj){
		
		
		AWAMZON::load();
	
		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	
	
	
		$this->view->obj = $obj;
		$this->view->id = $id;
		$this->view->host = app::$proto.app::$config->site['host'];
		$this->view->uname = $_SESSION['account']['user_username'];
	    $this->view->setTemplate('issuenotify_en.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		$emailarr = array(
			'Source' => '"Floctopus"'.app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				
			        'Subject' => array(
			            
			            'Data' => "Issue #".$id." has changed",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array("robot@floctopus.com"),	
			
			);
		
	
		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
	}

	function sendIssueCommentNotify($email,$msg,$id){
		
	
		AWAMZON::load();
	
		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	
		
		$this->view->id = $id;
		$this->view->msg = $msg;
		$this->view->host = app::$proto.app::$config->site['host'];
		$this->view->uname = $_SESSION['account']['user_username'];
	    $this->view->setTemplate('issuenotifycomment_en.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		$emailarr = array(
			'Source' => '"Floctopus"'.app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				
			        'Subject' => array(
			            
			            'Data' => "Issue #".$id." has changed",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array("robot@floctopus.com"),	
			
			);
		
	
		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
	}

	function sendAddIssueNotify($email,$subj,$msg,$id){
		

		AWAMZON::load();
	
		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	

	
	
		
		$this->view->subj = $subj;
		$this->view->id = $id;
		$this->view->msg = $msg;
		$this->view->uname = $_SESSION['account']['user_username'];
		$this->view->host = app::$proto.app::$config->site['host'];
		
	    $this->view->setTemplate('issueaddnotify_en.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		$emailarr = array(
			'Source' => '"Floctopus"'.app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				
			        'Subject' => array(
			            
			            'Data' => "New Issue #".$id." has been created",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array("robot@floctopus.com"),	
			
			);
		
	
		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
	}
	
	function sendAddIssueUpdNotify($email,$subj,$msg,$id){

		AWAMZON::load();
	
		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	

	
	
		
		$this->view->subj = $subj;
		$this->view->id = $id;
		$this->view->msg = $msg;
		$this->view->uname = $_SESSION['account']['user_username'];
		$this->view->host = app::$proto.app::$config->site['host'];
		
	    $this->view->setTemplate('issueupdnotify_en.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		$emailarr = array(
			'Source' => '"Floctopus"'.app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				
			        'Subject' => array(
			            
			            'Data' => "Issue #".$id." has been updated",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array("robot@floctopus.com"),	
			
			);
		
	
		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
	}

	function sendIssueAssigntNotify($email,$id){
		
	
		AWAMZON::load();
	
		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	
	
		$this->view->id = $id;
		$this->view->host = app::$proto.app::$config->site['host'];
		$this->view->uname = $_SESSION['account']['user_username'];
	    $this->view->setTemplate('issuenotifyassign_en.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		$emailarr = array(
			'Source' => '"Floctopus"'.app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				
			        'Subject' => array(
			            
			            'Data' => "Issue #".$id." has been assigned to you",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array("robot@floctopus.com"),	
			
			);
		
	
		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
		
	}	

	function sendInvite($email,$key){
		

		AWAMZON::load();
	//	$sm = new OrmSM();
		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	
			 
		$coEmail = 'robot@floctopus.com';
		
	
		$this->view->link=app::$proto.app::$config->site['host']."/signup/?key=".$key;
		$this->view->acs = $_SESSION['account'];
	    $this->view->setTemplate('invite_en.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		
		//$result = $client->listVerifiedEmailAddresses();
		
		//app::trace($result);
		
		$emailarr = array(
			'Source' => '"Floctopus.com"'.app::$config->mail['EMAIL_FROM'],
			//'Source' => app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				// Data is required
			        'Subject' => array(
			            // Data is required
			            'Data' => "Invitation to Floctopus.com",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                // Data is required
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                // Data is required
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array($coEmail),	
			
			);
		

		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
		if(isset($messageID)){
			

			$res['status']=true;
			return $res;
			
		}else{
			
			$res['status']=false;
			return $res;
		}
		
	}

	function notifyInvite($email){
		
		
		AWAMZON::load();

		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	
			 
		
		
		$coEmail = 'robot@floctopus.com';
	
		$this->view->link=app::$proto.app::$config->site['host']."/users/invites";
		$this->view->acs = $_SESSION['account'];
	    $this->view->setTemplate('invitenotify_en.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		$emailarr = array(
			'Source' => '"Floctopus.com"'.app::$config->mail['EMAIL_FROM'],
			//'Source' => app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				// Data is required
			        'Subject' => array(
			            // Data is required
			            'Data' => "Invitation to Floctopus.com",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                // Data is required
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                // Data is required
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array($coEmail),	
			
			);
		

		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
		if(isset($messageID)){
			

			$res['status']=true;
			return $res;
			
		}else{
			
			$res['status']=false;
			return $res;
		}
		
	}

	function sendShareDoc($email,$id,$obj){

		AWAMZON::load();
	
		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	
	
		$this->view->obj = $obj;
		$this->view->id = $id;
		$this->view->host = app::$proto.app::$config->site['host'];
		$this->view->uname = $_SESSION['account']['user_username'];
	    $this->view->setTemplate('issuenotify_en.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		$emailarr = array(
			'Source' => '"Floctopus"'.app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				
			        'Subject' => array(
			            
			            'Data' => "Issue #".$id." has changed",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array("robot@floctopus.com"),	
			
			);
		
	
		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
	}
	
	function sendResetPassword($email,$uid,$key){
		
		
	
		AWAMZON::load();

		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$client = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	
		
		$fullstr = $uid."@".$key;
		$en64 = \base64_encode($fullstr);
		$this->view->key = \bin2hex($en64);
		$this->view->host = app::$config->site['host'];
		$this->view->proto = app::$proto;
		
	    $this->view->setTemplate('resetpass.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		//app::trace(app::$config->mail['EMAIL_FROM']);
		
		$emailarr = array(
			'Source' => '"Floctopus"'.app::$config->mail['EMAIL_FROM'],
			//'Source' => app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				// Data is required
			        'Subject' => array(
			            // Data is required
			            'Data' => "Reset Password",
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                // Data is required
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                // Data is required
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			        
			        		
			),
			'ReplyToAddresses' => array("robot@floctopus.com"),	
			
			);
		
	
		
		$r = $client->sendEmail($emailarr);
		
		$messageID = $r->getPath('MessageId');
		
		
		
	}
	
	function sendInvoice($email,$invoiceid,$senderid,$msg=""){
		
		$Invoice = new OrmInvoices();
		$Acc = new OrmAccounts();
		$Cli = new OrmClients();

		AWAMZON::load();
	
		$credentials = new Credentials(app::$config->amazon['KEY_ID'], app::$config->amazon['KEY_SECRET']);
		$awsclient = SesClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-west-2'	
		));	
		
		$inv = $Invoice->getCronByID($invoiceid,$senderid);
		$account = $Acc->getAccount($senderid);
		if($inv['inv_client_id']){
			
			$fullstr = $inv['inv_client_id']."@".$senderid;
			$en64 = \base64_encode($fullstr);
			$enryptedstr = \bin2hex($en64);
			
			$client = $Cli->getCronByID($inv['inv_client_id'],$senderid);
			$returnemail = $account['user_email']; 
			
		}
		
		$this->view->inv = $inv ;
		$this->view->link=$this->http_proto.'client.floctopus.com/invoices/view/?key='.$enryptedstr."&inv=".$invoiceid;
		$this->view->acs = $account;
		$this->view->msg = $msg;
	    $this->view->setTemplate('invoice.tpl');
	    $html=$this->view->render();	
		
		$destArr['ToAddresses'] =array($email);
		
		$emailarr = array(
			'Source' => '"'.$account['user_name'].'"'.app::$config->mail['EMAIL_FROM'],
			'Destination' => $destArr,
			'Message' => array(
				// Data is required
			        'Subject' => array(
			            // Data is required
			            'Data' => "New Invoice #".$inv['inv_num'],
			            'Charset' => 'UTF-8',
			        ),	
			        
		        'Body' => array(
		            'Text' => array(
		                // Data is required
		                'Data' => '',
		                'Charset' => 'UTF-8',
		            ),
		            'Html' => array(
		                // Data is required
		                'Data' => $html,
		                'Charset' => 'UTF-8',
		            ),
		        ),		        
			),
			'ReplyToAddresses' => array($returnemail),	
			
			);
		$r = $awsclient->sendEmail($emailarr);
		$messageID = $r->getPath('MessageId');
	}
	
	

}


?>