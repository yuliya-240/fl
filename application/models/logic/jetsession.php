<?php
namespace floctopus\models\logic;

use \floctopus\application as app;
use \floctopus\models\orm\accounts as OrmAccounts;
use \floctopus\models\orm\misc as OrmMisc;
use \floctopus\models\orm\users as OrmUsers;
use \floctopus\models\orm\projects as OrmPrj;
use \jet\libs\MDETECT as MOB; 
use \jet\db\qb as qb;

class jetsession extends \floctopus\models\common\model {   
  
    function login($email, $password, $hashed = false) {
        $Accounts = new OrmAccounts();
        
        $account = $Accounts->access($email, $password, $hashed); 
        
        if (!$account) {
            throw new \jet\exception('account.wrong');
            return false;   
        }
        
        if ($account['user_status']!=1 ) {
            throw new \jet\exception('account.blocked');
            return false;   
        }
        $_SESSION['account'] = $account;

		$prj = new OrmPrj();
		$dp = $prj->getDefaultProject();
		if(!$dp){
			
			$ad['project_user_id'] = $_SESSION['account']['user_id'];
			$ad['project_name'] = 'Default';
			$ad['project_default'] = 1;
			$dp = $prj->addDef($ad);
		}
		$_SESSION['DEFPRJ'] = $dp;
        
        
        return $account;
    }

    function alogin($email) {
        $Accounts = new OrmAccounts();
        
        $account = $Accounts->aaccess($email); 
        
        if (!$account) {
            throw new \jet\exception('account.wrong');
            return false;   
        }
        
        if ($account['user_status']!=1 ) {
            throw new \jet\exception('account.blocked');
            return false;   
        }
        $_SESSION['account'] = $account;

		$prj = new OrmPrj();
		$dp = $prj->getDefaultProject();
		if(!$dp){
			
			$ad['project_user_id'] = $_SESSION['account']['user_id'];
			$ad['project_name'] = 'Default';
			$ad['project_default'] = 1;
			$dp = $prj->addDef($ad);
		}
		$_SESSION['DEFPRJ'] = $dp;
        
        
        return $account;
    }
	
	function loginSOCIAL($id,$provider="fb"){
        $Accounts = new OrmAccounts();
       
        if(isset($_SESSION['jet_redirect_history']))$redirect_history = $_SESSION['jet_redirect_history'];
        if(isset($redirect_history))$_SESSION['jet_redirect_history']= $redirect_history ;
        
        $account = $Accounts->accessSOCIAL($id,$provider); 
        
        if (!$account) {
            throw new \jet\exception('account.wrong');
            return false;   
        }
        
        if ($account['user_status']!=1) {
            throw new \jet\exception('account.blocked');
            return false;   
        }
         
        if ($account) 	{
	        
	        $account = $Accounts->getAccount($account['user_id']); 
	        
            $_SESSION['account'] = $account;
            
        }
        
        return $account;		
		
	}

    function findByEmail($email){
	    $acc = new OrmAccounts();
	    
	    return $acc->findByEmail($email);
    }
        
    function mobDetect(){
	    MOB::load();
	    $detect = new \Mobile_Detect();
	    if ($detect->isMobile()) return "desktop"; else return "desktop";
	    
    }
 
     function isMobPhone(){
	    MOB::load();
	    $detect = new \Mobile_Detect();
	    if( $detect->isMobile() && !$detect->isTablet() ){
			return true;
		}else{
			
			return false;
		}
	    
    }
    
    function refresh() {
        if (isset($_SESSION['account']) && isset($_SESSION['account']['user_id']))  {
             $Accounts = new OrmAccounts();
             $account = $Accounts->getAccount($_SESSION['account']['user_id']);     
             
             if (!$account) {
                 $this->logout();
                 return false;
             }
             
             
             if ($account['user_status'] != 1) {
                $this->logout();    
                return false;
            } 
             
             $_SESSION['account'] = $account;
             return true;
             
        }
        else {
            $this->logout();  
            return false;     
        }
    }

    function checkAccountID($userID = null) {
        if ($userID === null) return false;
        if (isset($_SESSION['account']['user_id']) && $_SESSION['account']['user_id'] == $userID) return true;
        return false;     
    }

    function getAccountID() {
        if (isset($_SESSION['account']['user_id'])) return $_SESSION['account']['user_id'];
        return false;     
    }

    function logout() {
       
       foreach ($_SESSION as $k=>$v){
	       
	       unset($_SESSION[$k]);
       }
         
       $_SESSION['account'] = null; 
        $this->disableAutoLogin() ;
    }
   
    function isEmailExist($email=''){
	     $Accounts = new OrmAccounts();
	     
	     $a = $Accounts->findByEmail($email);
	     
	     if(!$a) return true;
	     
	     return false;
	    
    }

    function isLogged() {
        if (isset($_SESSION['account']) && isset($_SESSION['account']['user_id'])) return true;
    }

    function getData() {
        if (isset($_SESSION['account']))
            return $_SESSION['account'];
        else return null;
    }

    /************************************************
    
    	Autologin ( Remeber me)
    	
    *************************************************/
    
    function enableAutoLogin($email = null, $password = null) {

		\setcookie('fl_email',$email,time()+60*60*24*30,'/');
		\setcookie('fl_password', $password,time()+60*60*24*30,'/');
		
	}
	
	function tryAutoLogin() {
		if (!isset($_COOKIE['fl_password'])) {
			//throw new \jet\exception('no.autologin');
			return false;
		}
		else return true; //return $this->login($_COOKIE['ii_email'],$_COOKIE['ii_password'],true);
	}
	
	function disableAutoLogin() {
		setcookie('fl_email',null,time(),'/');
		setcookie('fl_password',null,time(),'/');
	}

	/**** 
	
	CLIENT SESSION
	
	**/
	
	function setTZ(){
		
		//app::trace($_SESSION['account']['co_tz']);
		
		 \date_default_timezone_set($_SESSION['account']['user_tz']);  
		 $time_offset = date('P');
		 if($time_offset=="+14:00")$time_offset="+13:00";
		 $this->db->q("SET `time_zone`='".$time_offset."'");
		 
	}

	function setTZUTC(){
		
		 \date_default_timezone_set('UTC');  
		  $this->db->q("SET `time_zone`='".date('P')."'");
		 
	}
	
	function updateAcc($data=array()){
		$acc = new OrmAccounts();
		
		$acc->update($_SESSION['account']['user_id'],$data);
		
		return  false;
	}
    
	
}
