<?php
namespace floctopus\models\aws;

use \floctopus\application as app;
use \jet\libs\AWS as AWSLIB;

class mail extends \floctopus\models\common\model {   

	private $message;
	private $subj;
	private $to;//
	private $fromEmail;
	private $fromName;
	private $answerEmail;

	function __construct() {
		
		$this->fromEmail = app::$config->mail['EMAIL_FROM'];
		$this->to = array(app::$config->mail['EMAIL_TO']);
		$this->fromName = app::$config->mail['EMAIL_FROM_NAME'];	
		$this->fromStr="\"".$this->fromName."\" ".$this->fromEmail;
		$this->message = "";
		$this->subj = "";
		$this->answerEmail=array(app::$config->mail['EMAIL_ANSWER_TO']);
  
	}
	
	function setRecepient($emailTo = null  ){
		
		if (!is_array($emailTo))$this->to = array($emailTo); else $this->to= $emailTo;
		
		
	}
	
	function setAnswerEmail($email){
		
		$this->answerEmail=array($email);
	}

	function setFromName($name){
		
		$this->fromName=$name;
		$this->fromStr="\"".$this->fromName."\" ".$this->fromEmail;
	}

	
	function setMessage($message) {
		$this->message = $message;
	}
	
	function setSubj($subj) {
		$this->subj = $subj;
	}

	function send() {
		 AWSLIB::load();
		$email = new \AmazonSES();
    
		$response = $email->send_email(
		$this->fromStr, // Source (aka From)
			array('ToAddresses' => // Destination (aka To)
				$this->to
			),
			array( // Message (short form)
				'Subject.Data' => $this->subj,
				// 'Body.Text.Data' => 'This is a simple test message ' . time(),
				'Body.Html.Data' => $this->message 
			),
			array( // Optional parameters
				'ReplyToAddresses' => $this->answerEmail
        
        //'ReplyToAddresses' => array('noreply@managemart.com')
       		)
       	);

    
       	return $response->isOK();
	}

}
