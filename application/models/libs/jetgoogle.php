<?php
namespace floctopus\models\libs;

use \floctopus\application as app;
use \floctopus\models\logic\jetsession as Session;
use \jet\libs\google as GOOGLELIB; 

class jetgoogle extends \floctopus\models\common\model {
	
	private $token;
	private $client;
	private $people_service;
	
	function __construct() {
	    GOOGLELIB::load();
	    $this->client = new \Google_Client();
	    $this->client->setClientId(app::$config->google['CLIENT_ID']);
		$this->client->setClientSecret(app::$config->google['CLIENT_SECRET']);
		$this->client->setRedirectUri('http://secure.floctopus.com/auth/google/oauth2callback');
		$this->client->addScope('profile');
		$this->client->addScope('https://www.googleapis.com/auth/contacts.readonly');
		$this->client->addScope('https://www.googleapis.com/auth/admin.directory.resource.calendar.readonly');
		$this->token = $_SESSION['account']['user_google_token'];
		if($this->token!=="0")$this->client->setAccessToken($this->token);
	}	
	
	public function isAuth(){
		
		if($this->token==="0")return false; else return true;
	}
	
	public function getAuthUrl(){
		
		return $this->client->createAuthUrl();
	}
	
	
	public function getAccessToken(){
		
		return $this->client->getAccessToken();
	}
	
	public function doAuthenticate($code){
		
		return $this->client->authenticate($code);
	}
	
	public function pplSrv(){
		
		$this->people_service = new \Google_Service_People($this->client);
	}
	
	public function fetchAccessTokenWithAuthCode($code){
			
		return  $this->client->fetchAccessTokenWithAuthCode($code);
	}
	
	
	
}