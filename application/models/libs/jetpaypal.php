<?php
namespace floctopus\models\libs;

use \floctopus\application as app;
use \floctopus\models\logic\jetsession as Session;

use \floctopus\models\orm\log as OrmLog;
use \floctopus\models\orm\company as Company; 
use \floctopus\models\orm\misc as OrmMisc; 
use \floctopus\models\logic\orders as LogicOrders;
use \jet\libs\paypalsdk as paypalsdk;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;

use PayPal\Api\ChargeModel; 
use PayPal\Api\Currency; 
use PayPal\Api\MerchantPreferences; 
use PayPal\Api\PaymentDefinition; 
use PayPal\Api\Plan;
use PayPal\Api\Agreement;

use PayPal\Api\Patch; 
use PayPal\Api\PatchRequest; 
 use PayPal\Common\PayPalModel; 

use \jet\db\qb as qb;

class jetpaypal extends \floctopus\models\common\model {
	
	function __construct() {
		paypalsdk::load();
		$this->apiContext = new \PayPal\Rest\ApiContext(
			new \PayPal\Auth\OAuthTokenCredential(
				app::$config->paypal['CLIENT_ID'],    
				app::$config->paypal['SECRET']     
			)
		);	
				
	//	$this->apiContext->setConfig(array('mode' => 'live'));	
		$this->apiContext->setConfig(array('mode' => 'sandbox'));	
		$this->co = new Company();
		$this->session = new Session();
		$this->misc = new OrmMisc();
		

	}	
	
	function getUrl($name,$sku,$price,$coid,$desc,$returl_sucess,$returl_cancel){
		
		$res['status'] = true;
		$payer = new Payer();
		$payer->setPaymentMethod("paypal");
		
		$item1 = new Item();
		$item1->setName($name)
		    ->setCurrency('EUR')
		    ->setQuantity(1)
		    ->setSku($sku) // Similar to `item_number` in Classic API
		    ->setPrice($price);
		
		$itemList = new ItemList();
		$itemList->setItems(array($item1));		

		$amount = new Amount();
		$amount->setCurrency("EUR")->setTotal($price);		

		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setCustom($coid )
		    ->setItemList($itemList)
		    ->setDescription($desc)
		    ->setInvoiceNumber(uniqid());

		//$baseUrl = getBaseUrl();
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl($returl_sucess)
		    ->setCancelUrl($returl_cancel);

		$payment = new Payment();
		$payment->setIntent("sale")
		    ->setPayer($payer)
		    ->setRedirectUrls($redirectUrls)
		    ->setTransactions(array($transaction));
		
		$request = clone $payment;
		
		try {
			$payment->create($this->apiContext);
		} catch (Exception $ex) {
			//ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
			$res['status'] = false;
			$res['msg'] = "Created Payment Using PayPal. Please visit the URL to Approve.";
			$res['err'] = $ex;
		}
		
		$res['url'] =  $payment->getApprovalLink();
		
		return $res;
		
	}
	
	function doPay($paymentId,$payerID,$sku){
		$this->orders = new LogicOrders();
		
	    $payment = Payment::get($paymentId, $this->apiContext);	
		$execution = new PaymentExecution();
		$execution->setPayerId($payerID);

		try {
			$result = $payment->execute($execution, $this->apiContext);
			try {
            	$payment = Payment::get($paymentId, $this->apiContext);
            	
			} catch (Exception $ex) {
	       
            	exit(1);
			}	
        } catch (Exception $ex) {
	       
		   exit(1);
		} 
		$p = $payment->getPayer();
		$tz = $payment->getTransactions();
		$t = $tz[0];
				
		$amount = $t->getAmount();				

		$tr['amount']   = $amount->getTotal();
		$tr['date']  = \date('Y-m-d H:i:s',strtotime($payment->getCreateTime()));
		$tr['transaction_id']=$payment->getId();
		$this->orders->addOrderPaypal( $t->getCustom(),$sku,$tr);
		return true;	
		
	}
	
	function createPlan2(){
		
		// Create a new instance of Plan object
		$plan = new Plan();
		
		// # Basic Information
		// Fill up the basic information that is required for the plan
		$plan->setName('ManageMart Yearly Plan')
		    ->setDescription('Yearly Plan of ManageMart')
		    ->setType('infinite');
		
		// # Payment definitions for this billing plan.
		$paymentDefinition = new PaymentDefinition();
		
		// The possible values for such setters are mentioned in the setter method documentation.
		// Just open the class file. e.g. lib/PayPal/Api/PaymentDefinition.php and look for setFrequency method.
		// You should be able to see the acceptable values in the comments.
		$paymentDefinition->setName('MM1M2')
		    ->setType('REGULAR')
		    ->setFrequency('Month')
		    ->setFrequencyInterval("1")
		    ->setCycles("0")
		    ->setAmount(new Currency(array('value' => 19.99, 'currency' => 'USD')));
		
		// Charge Models
		$chargeModel = new ChargeModel();
		$chargeModel->setType('TAX')
		    ->setAmount(new Currency(array('value' => 0, 'currency' => 'USD')));
		
		$paymentDefinition->setChargeModels(array($chargeModel));
		
		$merchantPreferences = new MerchantPreferences();
		//$baseUrl = getBaseUrl();
		// ReturnURL and CancelURL are not required and used when creating billing agreement with payment_method as "credit_card".
		// However, it is generally a good idea to set these values, in case you plan to create billing agreements which accepts "paypal" as payment_method.
		// This will keep your plan compatible with both the possible scenarios on how it is being used in agreement.
		$merchantPreferences->setReturnUrl("https://".app::$config->site['host']."/webhook/paypal/subscription")
		    ->setCancelUrl("https://".app::$config->site['host']."/webhook/paypal/subscription")
		    ->setAutoBillAmount("yes")
		    ->setInitialFailAmountAction("CONTINUE")
		    ->setMaxFailAttempts("0")
		    ->setSetupFee(new Currency(array('value' => 0, 'currency' => 'USD')));
		
		
		$plan->setPaymentDefinitions(array($paymentDefinition));
		$plan->setMerchantPreferences($merchantPreferences);
		
		// For Sample Purposes Only.
		$request = clone $plan;
		
		// ### Create Plan
		try {
		    $output = $plan->create($this->apiContext);
		} catch (Exception $ex) {
		    // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
		 	//ResultPrinter::printError("Created Plan", "Plan", null, $request, $ex);
		    exit(1);
		}
		
		// NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
		// ResultPrinter::printResult("Created Plan", "Plan", $output->getId(), $request, $output);
		
		app::trace($output);
				
		
	}

	function activatePlan($planid){
		try {
		    $patch = new Patch();
		
		    $value = new PayPalModel('{
			       "state":"ACTIVE"
			     }');
		
		    $patch->setOp('replace')
		        ->setPath('/')
		        ->setValue($value);
		    $patchRequest = new PatchRequest();
		    $patchRequest->addPatch($patch);
			$createdPlan = Plan::get($planid, $this->apiContext);
			
		    $createdPlan->update($patchRequest, $this->apiContext);
		
		   // $plan = Plan::get($planid, $this->apiContext);
		
		} catch (Exception $ex) {
		
		 	//ResultPrinter::printError("Updated the Plan to Active State", "Plan", null, $patchRequest, $ex);
		    exit(1);
		}
		
		 //ResultPrinter::printResult("Updated the Plan to Active State", "Plan", $plan->getId(), $patchRequest, $plan);
		
		return $createdPlan;		
	}

	function listPlans(){

		$plan = new Plan();

		try {
		
		    $params = array('page_size' => '10');
		    $planList = Plan::all($params, $this->apiContext);
		} catch (Exception $ex) {

			$res['status'] = false;
			$res['request'] = $request;
			$res['ex'] = $ex;
		 	//ResultPrinter::printError("Created Plan", "Plan", null, $request, $ex);
		    return $res;
		}

			$res['status'] = true;
			$res['plans'] = $planList;
			//$res['ex'] = $ex;
		    return $res;

	}
	
	function crateAgreement($paypalid,$coid,$sku,$iprice){
		$agreement = new Agreement();

		$cdate = \date("Y-m-d",strtotime("+30 days"))."T".\date("H:i:s")."Z";

		if($sku=="MM1M")$desc ="ManageMart Monthly Premium Plan Agreement";//T-Shirt of the Month Club Agreement
		if($sku=="MM6M")$desc ="ManageMart 6 Month Premium Plan Agreement";
		if($sku=="MM12M")$desc ="ManageMart 12 Month Premium Plan Agreement";
		
		$agreement->setName('Base Agreement')
		    ->setDescription($desc)
		    ->setStartDate($cdate);
		
		$plan = new Plan();
		$plan->setId($paypalid);
		$agreement->setPlan($plan);
		
		$payer = new Payer();
		$payer->setPaymentMethod('paypal');
		$agreement->setPayer($payer);
		
		$merchantPreferences = new MerchantPreferences();
		$merchantPreferences->setReturnUrl("https://".app::$config->site['host']."/webhook/paypal/subscription/?res=success&co=".$coid."&sku=".$sku)
		    ->setCancelUrl("https://".app::$config->site['host']."/webhook/paypal/subscription/?res=fail&co=".$coid."&sku=".$sku)
		    ->setAutoBillAmount("yes")
		    ->setInitialFailAmountAction("CONTINUE")
		    ->setMaxFailAttempts("0")
		    ->setSetupFee(new Currency(array('value' => $iprice, 'currency' => 'USD')));

		
		$agreement->setOverrideMerchantPreferences($merchantPreferences);
		
		$request = clone $agreement;
		
		//app::trace($agreement);
		
		try {
		
		    $agreement = $agreement->create($this->apiContext);
		    $approvalUrl = $agreement->getApprovalLink();
		
		} catch (Exception $ex) {
		
			$res['status'] = false;
			$res['msg'] = "Something went wrong  Please report this issue to support.";
			$res['err'] = $ex;
		}
			
		$res['status'] = true;
		$res['url'] = 	$approvalUrl;
		return 	$res;	
		
	}
	
	function execAgreement($token){

			$agreement = new \PayPal\Api\Agreement();
			//app::trace($agreement);
			try {
				
				$agreement->execute($token, $this->apiContext);
			}catch (Exception $ex) {
				
				$agreementarr['status'] = false;
			}
		
			$agreement = \PayPal\Api\Agreement::get($agreement->getId(), $this->apiContext);
			
			$payer = new Payer();
			$payer =  $agreement->getPayer();
			$payerinfo= $payer->getPayerInfo();
			
			$agreementarr=array(
				"status" => true,
				"id" => $agreement->getId(),
				"state" => $agreement->getState(),
				"name" => $agreement->getName(),
				"desc" => $agreement->getDescription(),
				"start_date" => $agreement->getStartDate(),
				"pay_method" => $payer->getPaymentMethod(),
				"payer_account_status" =>  $payer->getStatus(),
				"payer_email" => $payerinfo->getEmail(),
				"payer_fname" => $payerinfo->getFirstName(),
				"payer_lname" => $payerinfo->getLastName(),
				"payer_paypal_id" => $payerinfo->getPayerId(),
				"sku" => $_GET['sku'],
				"company" => $_GET['co'],
			);

			
			return $agreementarr;
	}
	
}
