<?php
namespace floctopus\models\libs;

use \floctopus\application as app;
use \floctopus\models\logic\jetsession as Session;
use \jet\libs\stripe as STRIPELIB; 
use \floctopus\models\orm\log as OrmLog;
use \floctopus\models\orm\company as Company; 
use \floctopus\models\orm\misc as OrmMisc; 
use \floctopus\models\orm\store as OrmStore;
use \floctopus\models\orm\stripeerr as OrmStripeErr;
use \jet\db\qb as qb;

class jetstripelib extends \floctopus\models\common\model {
	
	function __construct() {
		STRIPELIB::Load();
	    \Stripe
	    \Stripe::setApiKey(app::$config->stripe['testSecretKey']);		
		$this->co = new Company();
		$this->session = new Session();
		$this->misc = new OrmMisc();

	}	
	
	function GetListCustomers(){
		
		
		$list = \Stripe\Customer::all();
		
		return $list['data'];
	}
	
	function GetCustomer($id=''){
		
		$customer = \Stripe\Customer::retrieve($id);
		
		return $customer;
	}
	
	function delteCustomer($id=''){
		
		$cu = \Stripe\Customer::retrieve($id);
		$cu->delete();
		
		return true;
	}
	
	function CreateCustomer($tok,$email,$desc){
			
			
			try {  
				$cu =  \Stripe\Customer::create(array(
						"description" => $desc,
						"source" => $tok, // obtained with Stripe.js
						"email" => $email,
						"metadata" => array("custom_id" => $desc)
				));
					
				$upd['co_stripe_id'] = $cu->id;
				$this->co->updateCompany($_SESSION['account']['co_id'], $upd);
				$this->session->refresh();
				$res['status'] = true;
				
			} catch(\Stripe\Error\Card $e) {
				// Since it's a decline, Stripe_CardError will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
			
					
  			} catch (\Stripe\Error\RateLimit $e) {
	  			// Authentication with Stripe's API failed
	  			// (maybe you changed API keys recently)
	  			
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
				

	  			
	  		} catch (\Stripe\Error\InvalidRequest $e) {
		  		// Network communication with Stripe failed
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
			
		  		
		  		
		  	} catch (\Stripe\Error\Authentication $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
				

		  	} catch (\Stripe\Error\ApiConnection $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
	

		  	} catch (\Stripe\Error\Base $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;

			  	
			  
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;

				
  			}    
	    
		    if(!$res['status']){
		    
		    	$this->addErrLog($err);
				$res['msg'] = $err['message'];
				return $res;

		    }		
		
		 return $res;
		 
	}
	
	function createChargeApplication($amount=0,$currency,$token,$accId,$desc){
		
		try {  
			\Stripe\Charge::create(array(
			  'amount' => $amount,
			  'currency' => $currency,
			  'source' => $token,
			  'description' => $desc
			), array('stripe_account' => $accId));
				$res['charge']=$r;
				$res['status']=true;
				
			} catch(\Stripe\Error\Card $e) {
				// Since it's a decline, Stripe_CardError will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
					
  			} catch (\Stripe\Error\RateLimit $e) {
	  			// Authentication with Stripe's API failed
	  			// (maybe you changed API keys recently)
	  			
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
			
	  		} catch (\Stripe\Error\InvalidRequest $e) {
		  		// Network communication with Stripe failed
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
		  		
		  	} catch (\Stripe\Error\Authentication $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;

		  	} catch (\Stripe\Error\ApiConnection $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;

		  	} catch (\Stripe\Error\Base $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
			  
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
  			}    
	    
		    if(!$res['status']){
		    
		    	$this->addErrLog($err);
				$res['msg'] = $err['message'];
				return $res;
		    }
			
			$res['status']=true;
			return $res;
		
		
		
	}
	
	function createCharge($sku){
		     $store = new OrmStore();
		     $item = $store->getStoreInfoBySKU($sku);
			
			 $arr['sku']=$sku;
			 $arr['store']=1;
			 $arr['co']=$_SESSION['account']['co_id'];
			 
			  try {  
				$r =\Stripe\Charge::create(array(
				  "amount" => $item['sitem_price_cents'],
				  "currency" => "usd",
				  "customer" => $_SESSION['account']['co_stripe_id'], // obtained with Stripe.js
				  "description" => $item['sitem_name'],
				  "metadata" =>$arr
				));
				
				$res['charge']=$r;
				$res['status']=true;
				
			} catch(\Stripe\Error\Card $e) {
				// Since it's a decline, Stripe_CardError will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
					
  			} catch (\Stripe\Error\RateLimit $e) {
	  			// Authentication with Stripe's API failed
	  			// (maybe you changed API keys recently)
	  			
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
			
	  		} catch (\Stripe\Error\InvalidRequest $e) {
		  		// Network communication with Stripe failed
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
		  		
		  	} catch (\Stripe\Error\Authentication $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;

		  	} catch (\Stripe\Error\ApiConnection $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;

		  	} catch (\Stripe\Error\Base $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
			  
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
  			}    
	    
		    if(!$res['status']){
		    
		    	$this->addErrLog($err);
				$res['msg'] = $err['message'];
				return $res;
		    }
			
			$res['status']=true;
			return $res;

	}
	

	function addCard($tok){
		try { 
		   		$cu = \Stripe\Customer::retrieve($_SESSION['account']['co_stripe_id']);
		   		if(isset($cu->object) && $cu->object=="customer"){
					if(isset($cu->sources->data[0]['id']))$cu->sources->retrieve($cu->sources->data[0]['id'])->delete();
					$cu->sources->create(array("source" => $tok['id']));
					$res['status'] = true;
			   		
		   		}else{
			   		
					$res['status'] = false;
					$res['msg'] = "Customer doesn't exist in Stripe DB. Please report this error to support";
			   		
		   		}
			} catch(\Stripe\Error\Card $e) {
				// Since it's a decline, Stripe_CardError will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
				$res['msg'] = $err['message'];
				$this->addErrLog($err);
					
  			} catch (\Stripe\Error\RateLimit $e) {
	  			// Authentication with Stripe's API failed
	  			// (maybe you changed API keys recently)
	  			
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
				$res['msg'] = $err['message'];
				$this->addErrLog($err);

	  			
	  		} catch (\Stripe\Error\InvalidRequest $e) {
		  		// Network communication with Stripe failed
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
				$res['msg'] = $err['message'];
				$this->addErrLog($err);
		  		
		  		
		  	} catch (\Stripe\Error\Authentication $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
				$res['msg'] = $err['message'];
				$this->addErrLog($err);

		  	} catch (\Stripe\Error\ApiConnection $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['msg'] = $err['message'];
				$res['status'] = false;
				$this->addErrLog($err);

		  	} catch (\Stripe\Error\Base $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
				$res['msg'] = $err['message'];
				$this->addErrLog($err);
			  	
			  
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
				$res['msg'] = $err['message'];
				$this->addErrLog($err);
				
  			}    

		   
		   return $res;
	}

	function isCard(){
		
		$cu = \Stripe\Customer::retrieve($_SESSION['account']['co_stripe_id']);
		if(isset($cu->sources->data[0]['id'])) return true;
		
		return false;
	}

	function getInvoice($inv){
		return  \Stripe\Invoice::retrieve($inv);
		
	}

	function subscribe($sku){
		
		$arr['sku']=$sku;
		$arr['store']=0;
		$arr['co']=$_SESSION['account']['co_id'];
	
		 try { 
		    	$cus = \Stripe\Customer::retrieve($_SESSION['account']['co_stripe_id']);
				$r = $cus->subscriptions->create(array("plan" => $sku, "metadata" =>$arr));
				
				$res['sub'] = $r;
				$res['status'] = true;
				
			} catch(\Stripe\Error\Card $e) {
				// Since it's a decline, Stripe_CardError will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
					
  			} catch (\Stripe\Error\RateLimit $e) {
	  			// Authentication with Stripe's API failed
	  			// (maybe you changed API keys recently)
	  			
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
	  			
	  		} catch (\Stripe\Error\InvalidRequest $e) {
		  		// Network communication with Stripe failed
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
		  		
		  		
		  	} catch (\Stripe\Error\Authentication $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;

		  	} catch (\Stripe\Error\ApiConnection $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;

		  	} catch (\Stripe\Error\Base $e) {
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
			  
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				$body = $e->getJsonBody();
				$err  = $body['error'];
				$res['status'] = false;
				
  			}    

		    if(!$res['status']){
		    
		    	$this->addErrLog($err);
				$res['msg'] = $err['message'];
				return $res;
		    }

			
			return $res;
		
	}
	
	function unsubscribe($sku){
	
		$cus = \Stripe\Customer::retrieve($_SESSION['account']['co_stripe_id']);
		
		foreach($cus['subscriptions']['data'] as $k=>$v){
			
			if($v['plan']['id']==$sku){
				
				$cus->subscriptions->retrieve($v['id'])->cancel();	
			}
			
		}

	}


	function getSubscription($inv,$customer){
		
		$invoice = $this->getInvoice($inv);
		$cu = $this->GetCustomer($customer);
		$subscription = $cu->subscriptions->retrieve($invoice['subscription']);
					
		return $subscription;

	}	
	

    function addErrLog($err=array()){
    			
    			//$log = new OrmLog();
    			
    			$serr = new OrmStripeErr();
				
				$addErr['stripeerr_stripe_id'] = $_SESSION['account']['co_stripe_id'];
				$addErr['stripeerr_co_id'] = $_SESSION['account']['co_id'];
				if(isset($err['type']) && $err['type']!=null)$addErr['stripeerr_type'] = $err['type'];
				if(isset($err['code']) && $err['code']!=null)$addErr['stripeerr_code'] = $err['code'];
				if(isset($err['param']) && $err['param']!=null)$addErr['stripeerr_param'] = $err['param'];
				if(isset($err['message']) && $err['message']!=null)$addErr['stripeerr_msg'] = $err['message'];
				$serr->add($addErr);
				
				return false;
    
    }
	
	
}