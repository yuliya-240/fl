<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class misc extends \floctopus\models\common\model {
	
	function getMonthsList($where=array(),$order='mon_num ASC') {
        $select = '*';
        $where['mon_lang'] = $_SESSION['account']['user_lang'];
        $collection = qb::_table('month');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

	function getCurrencies(){
		$where=array();
        $select = '*';
        
        $collection = qb::_table('currencies');
        return $this->db->q($collection->where($where)->select($select));    
		
	}
	
	function getProfessionName(){
		
        $select = 'pro_name';
        $where['pro_id'] = $_SESSION['account']['user_proffession'];
        $collection = qb::_table('professions');
        
        return $this->db->q1($collection->where($where)->select($select));    
		
	}
	
	function getLanguageISO($iso){
		
        $select = 'lang_name';
        $where['lang_iso'] = $iso;
        $collection = qb::_table('languages');
        return $this->db->q1($collection->where($where)->select($select));    
	}

	function getProfessionNameByID($id=0){
		
        $select = 'pro_name';
        $where['pro_id'] = $id;
        $collection = qb::_table('professions');
        
        return $this->db->q1($collection->where($where)->select($select));    
		
	}
	
	function getAllRates($currency="USD"){
		
     	$where['exr_base']=$currency;
        $collection = qb::_table('exrates');
              
        return $this->db->q($collection->where($where)->select("*"));    
	}
	
	function getAllLangs(){
     
        $collection = qb::_table('languages');
        return $this->db->q($collection->orderby("lang_order")->select("*"));    
	}
	
	function getRateByCurrency($currency="USD",$base="UAH"){
		
		$where['exr_base']=$currency;
		$where['exr_currency']=$base;
        $collection = qb::_table('exrates');
              
        return $this->db->q_($collection->where($where)->select("*"));    
		
		
	}
	
	function getColors(){
		$where=array();
        $select = '*';
        
        $collection = qb::_table('colors');
        return $this->db->q($collection->where($where)->select($select));    
		
		
	}
	
}
?>