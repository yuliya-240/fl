<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class invoicesrecurring extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('invoices_recurring')->insert($add));
        return $this->db->getLastID();      
    }

    function update($id=0, $data = array()) {
        $where = array(
            'inv_id' => $id
        );
        
        $this->db->q(qb::_table('invoices_recurring')->where($where)->update($data));    
        return true;
    }

    function updateMass($where=array(), $data = array()) {

        
        $this->db->q(qb::_table('invoices_recurring')->where($where)->update($data));    
        return true;
    }

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='inv_date ASC') {
	    $where['inv_user_id']=$_SESSION['account']['user_id'];
	    $where['inv_trash']=0;
	  //  $where['invoices_statuses.invstatus_lang'] = $_SESSION['account']['user_lang'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('invoices_recurring');
        $collection->leftjoin('events_recurring', 'events_recurring.er_invoice_id', 'invoices_recurring.inv_id');    
        $collection->leftjoin('month', 'month.mon_num', 'events_recurring.er_year_month'," and month.mon_lang='".$_SESSION['account']['user_lang']."'");     
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getAllList($where=array(),$order='inv_date ASC') {
	    $where['inv_user_id']=$_SESSION['account']['user_id'];
	    $where['inv_trash']=0;
	  
        $select = '*';
        $collection = qb::_table('invoices_recurring');
         $collection->leftjoin('events_recurring', 'events_recurring.er_invoice_id', 'invoices_recurring.inv_id');       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

    function getListCount($where=array(),$search = array()) {
	    $where['inv_user_id']=$_SESSION['account']['user_id'];
	    $where['inv_trash']=0;
        $select = '*';
        $collection = qb::_table('invoices_recurring');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="inv_date ASC") {
		
    	$where['inv_trash']=0;
    	$where['inv_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(inv_tags LIKE '".$tag."')";
			$searchnew['inv_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('invoices_recurring');
         $collection->leftjoin('events_recurring', 'events_recurring.er_invoice_id', 'invoices_recurring.inv_id'); 
       // $collection->leftjoin('countries', 'countries.country_iso', 'clients.inv_country');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('inv_id')->Limit($offset,$count)->select($select));    
    }

    function getListCountSearch($where=array(),$search = array()) {
    	$where['inv_trash']=0;
    	$where['inv_user_id']=$_SESSION['account']['user_id'];
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(inv_tags LIKE '".$tag."')";
			$searchnew['inv_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('invoices_recurring');

       $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('inv_id')->count('*'));  
       return count($count);  
    }
	
	function getByID($id=0){
		
		$where['inv_id'] = $id;
	    $where['inv_user_id'] = $_SESSION['account']['user_id'];
	    $where['inv_trash']=0;
        $select = '*';
        $collection = qb::_table('invoices_recurring');
         $collection->leftjoin('events_recurring', 'events_recurring.er_invoice_id', 'invoices_recurring.inv_id'); 
         $collection->leftjoin('month', 'month.mon_num', 'events_recurring.er_year_month'," and month.mon_lang='".$_SESSION['account']['user_lang']."'");   
        //$collection->leftjoin('countries', 'countries.country_iso', 'clients.inv_country');
        return $this->db->q_($collection->where($where)->select($select));    
	}

    function getInvAmount($where=array()){
	    $where['inv_trash']=0;
        $select = 'sum(inv_total_due_base)';
        $collection = qb::_table('invoices_recurring');
        $r =  $this->db->q1($collection->where($where)->select($select));    
		if(!$r)$r=0;
		
		return $r;
    }
	
	// Lines
	
	function addLine($add) {
	
        $this->db->q(qb::_table('invoices_lines_recurring')->insert($add));
        return $this->db->getLastID();      
    }

    function getLinesList($where=array(),$order='invline_id ASC') {

        $select = '*';
        $collection = qb::_table('invoices_lines_recurring');
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

	function delLines($invid=0){
		 $where['invline_user_id'] = $_SESSION['account']['user_id'];
		 $where['invline_inv_id'] = $invid;
		 $collection = qb::_table('invoices_lines_recurring');
		 return $this->db->q($collection->where($where)->delete());   
	}
	
	
	// HISTORY
	
	function addHistory($add=array()){
		
        $this->db->q(qb::_table('invoice_history')->insert($add));
        return $this->db->getLastID();      
		
		
	}

    function getHistoryList($where=array(), $order='invhist_id DESC') {
	    $where['invhist_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('invoice_history');
       $collection->leftjoin('invoice_actions', 'invoice_history.invhist_action', 'invoice_actions.invact_action', " and invact_lang='".$_SESSION['account']['user_lang']."'");          
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }


	// ATTACH
	
	function addAttach($add=array()){
		
        $this->db->q(qb::_table('invoices_attach_recurring')->insert($add));
        return $this->db->getLastID();      
	}

    function getAttachList($invid = 0, $order='invatt_id ASC') {
	    $where['invatt_user_id']=$_SESSION['account']['user_id'];
	    $where['invatt_trash']=0;
	    $where['invatt_inv_id'] = $invid;
        $select = '*';
        $collection = qb::_table('invoices_attach_recurring');
               
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

	function delAttach($invid=0){
		 $where['invatt_user_id'] = $_SESSION['account']['user_id'];
		 $where['invatt_inv_id'] = $invid;
		 $collection = qb::_table('invoices_attach_recurring');
		 return $this->db->q($collection->where($where)->delete());   
	}

	
}