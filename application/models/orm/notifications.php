<?php
namespace floctopus\models\orm;
use \floctopus\application as app;
use \jet\db\qb as qb;

class notifications extends \floctopus\models\common\model {

	function add($arr=array()){
		$this->db->q(qb::_table('notifications')->insert($arr));
        return $this->db->getLastID();
	}
	
	function update($id=0,$arr=array()){
		$w['n_user_id']=$_SESSION['account']['user_id'];
		$w['n_id']=$id;
		$this->db->q(qb::_table('notifications')->where($w)->update($arr));
        return true;
	}

	function update2($w=array(),$arr=array()){
		$w['n_user_id']=$_SESSION['account']['user_id'];
		$this->db->q(qb::_table('notifications')->where($w)->update($arr));
        return true;
	}

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='n_date DESC') {
	    $where['n_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('notifications');
		$collection->leftjoin('notifications_types', 'notifications_types.ntype_action', 'notifications.n_type', " AND notifications_types.ntype_lang='".$_SESSION['account']['user_lang']."'");
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }
    
    function getListCount($where=array(),$search = array()) {

    	$where['n_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('notifications');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getCount($where=array(),$search = array()) {
    	$where['n_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('notifications');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getAllList($where=array(),$order='n_date DESC'){
        $where['n_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('notifications');
       // $collection->leftjoin('customs', 'customs.custom_id', 'expenses.exp_object_id', 'AND exp_object=1');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
	    
    }

    function getInfo($id=0){
	    $where['n_user_id']=$_SESSION['account']['user_id'];
	    $where['n_id'] = $id;
        $select = '*';
        $collection = qb::_table('notifications');
       // $collection->leftjoin('expenses_cat', 'exp_cat', 'expenses_cat.expcat_id');
        return $this->db->q_($collection->where($where)->select($select));    
    }
	


    function getEmailByID($cid=0) {
    	$where['sm_email_id'] = $cid;
        $select = '*';
        $collection = qb::_table('notifications');

        return $this->db->q_($collection->where($where)->select($select));    
    }
    
    function  markAsRead(){
	    
	   	$where['n_user_id'] = $_SESSION['account']['user_id'];
	    $where['n_status'] = 0;
	    $upd['n_status'] = 1;
		
		$this->db->q(qb::_table('notifications')->where($where)->update($upd));
        return true;
	    
	    
    }
	
}