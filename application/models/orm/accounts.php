<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class accounts extends \floctopus\models\common\model {   
    
   
    
    /* Crate new account */
    function createAccount($email, $password) {
	
	
        if ($this->findByEmail($email)) { 
            throw new \exception('email.exist');
        }
        // Create User
        $user = array(
            'user_email'        =>  $email,
            'user_username'		=>  "u".uniqid(),
            'user_password'     => $this->hashPassword($password),
            'user_pusher_channel'   => "private-".uniqid(),
        );
        
        $this->db->q(qb::_table('users')->insert($user));
		$userid = $this->db->getLastID();
       
        $user = array(
            'set_user_id'        =>  $userid,
        );
        
        $this->db->q(qb::_table('settings')->insert($user));
		

       
        $res['uid'] = $userid;
        //app::trace($res);
        return $res;
    }

	function findByUsername($userID){
        $where = array(
            'users.user_username' => $userID
        );
        return $this->returnAccount($where, $collection);    
		
		
	}

    function createAccountSOCIAL($id,$email, $fname, $lname, $srv="fb") {
	
	
        if ($this->findByEmail($email)) { 
            throw new \exception("Well darn, it looks like that email is already being used. Please try another email");
        }
 
        if ($this->findByFB($id)) { 
            throw new \exception("Well darn, it looks like that Facebook account is already being used. Please try another Facebook Account");
        }
       
        if ($this->findByGOOGLE($id)) { 
            throw new \exception("Well darn, it looks like that Google account is already being used. Please try another Google Account");
        }

        
       $date = \date("Y-m-d",\strtotime("+7 days"));
       
	   $pass= \uniqid();
       
        $ins = array(
            'customer_name'		  =>  "New Company",
            'customer_date_exp'     => $date,
            
        );
        $this->db->q(qb::_table('customers')->insert($ins));
        
        $companyID = $this->db->getLastID();
       
        // Create company
        $user = array(
            'user_email'        =>  $email,
   
            'user_customer_id'	=>  $companyID,
            'user_username'		=>  $email,
            'user_fname'        =>  $fname, 
            'user_lname'        =>  $lname, 
            'user_password'     => $this->hashPassword($pass),
            'user_role'         => 30,
        );
        
        
        if($srv=="fb"){
	        
	        $user['user_facebook_id'] = $id;
        }
        
        if($srv=="google"){
	        
	        $user['user_google_id'] = $id;
        }
        
        
        
        $userid = $this->db->q(qb::_table('users')->insert($user));
        
        $res['pass'] =  $pass;
        $res['coid'] = $companyID;
        $res['uid'] = $userid;
        return $res;
    }
    
    function uuid() {
        return uniqid(rand(0,1000));
    }
    
	function getAllAccounts(){
        $where=array();
        $collection = qb::_table('users');
        $account = $this->db->q($collection->where($where)->select("*"));    
		
        return $account;
	}
    
    /* Get account by ID */
    function getAccount($userID, $collection = array()) {
        $where = array(
            'users.user_id' => $userID
        );
        return $this->returnAccount($where, $collection);    
    }
    
 	function getByChannel($channel){
        $where = array(
            'users.user_pusher_channel' => $channel
        );
        return $this->returnAccount($where);    
 	}   
    
    function getAccountInfo($where=array()){
	  
	    return $this->returnAccount($where); 
    }
  
    function getAccountByFacebookID($fbID, $collection = array()) {
        $where = array(
            'users.user_facebook_id' => $fbID
        );
        $acc = $this->returnAccount($where, $collection);  
        
        if($acc) return true; else return false;  
    }  
    
    /* Get account by email and password */
    function access($email, $password, $passwordHashed = false, $collection = array()) {
        
        $where = array(
            'user_email'     => $email,
             'user_password'  => ($passwordHashed)?$password:$this->hashPassword($password)
        );
        
        $account = $this->returnAccount($where, $collection);  
        
        return $account;
    }
    
    function aaccess($email,  $collection = array()) {
        
        $where = array(
            'user_email'     => $email,
            
        );
        
        $account = $this->returnAccount($where, $collection);  
        
        return $account;
    }  
    
    function accessSOCIAL($id,$provider="fb",$collection = array()) {
        if($provider=="fb"){
			$where['user_facebook_id'] = $id;	        
        }
        
        if($provider=="google"){
			$where['user_google_id'] = $id;	        
        }
        
		$where['user_trash'] = 0;	
        
        $account = $this->returnAccount($where, $collection);  
        
        return $account;
    }
    
    /* Check if email exists */
    function findByEmail($email) {
        $where = array(
            'user_email'     => $email
        );
        return $this->returnAccount($where);
    }
    
    function isUnameExist($un){
        $where['user_username']=$un;
        $where['user_id!'] = $_SESSION['account']['user_id'];
        return $this->returnAccount($where);
    }
    
    function findByFB($id){
        $where = array(
            'user_facebook_id'     => $id
        );
        return $this->returnAccount($where);
	    
	    
    }

    function findByGOOGLE($id){
        $where = array(
            'user_google_id'     => $id
        );
        return $this->returnAccount($where);
	    
	    
    }

    function isEmailOtherAccount($where) {

        return $this->returnAccount($where);
    }
    
    /*** Change Password ****/
    
    function changePassword($userID,$password){
        $update = array(
            'user_password'   => $this->hashPassword($password)
        );
	    
	$this->updateUser($userID,$update) ;
	return true;
    }
    
    function isPassword($password){
	    
	    $newhashpass = $this->hashPassword($password);
	    
	    if($newhashpass==$_SESSION['account']['user_password'])return true; else return false;
    }
    
    /* Update Account */


    function getList($where=array(),$page=1,$count=1, $search = array(),$order='user_name asc') {
        
        if($page>0)$page--;
        $offset = \intval($page*$count);
        
        $where['user_trash']=0;
        $select = '*';
        $collection = qb::_table('users');
        
       // app::trace($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));
        
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }
 
    function update($id=0, $data = array()) {
        $where = array(
            'user_id' => $id
        );
        
        $this->db->q(qb::_table('users')->where($where)->update($data));    
        
        return true;
    }
    
    function updateUser($userID=0, $data = array()) {
        $where = array(
            'user_id' => $userID
        );
        
        $this->db->q(qb::_table('users')->where($where)->update($data));    
        
        return true;
    }
    
    function getUserID($where=array()){
        $select = 'users.user_id';
        $collection = qb::_table('users');
	    return $this->db->q1($collection->where($where)->select($select));  
	    
    }
    
    function getUserInfo($userID=0){
    	$where = array(
            'user_id' => $userID
        );
        $select = '*';
        $collection = qb::_table('users');
	    return $this->db->q_($collection->where($where)->select($select));  
	    
    }
    
    function confirmCode($userID,$code){
	    
	    $where = array(
            'users.user_id' => $userID,
            'users.user_confirm_code' => $code
        );
        return $this->returnAccount($where);    

	    
    }
    
    function updateSettings($userID=0,$data=array()){
	    
        $where = array(
            'set_user_id' => $userID
        );
        
        $this->db->q(qb::_table('settings')->where($where)->update($data));    
        
        return true;
	    
	    
    }
    
    /* Wrap Password with md5 and salt */
   function hashPassword($password) {
        return md5($password);
    }
 
     /* Get user data by criteria */
    private function returnAccount($where, $components = array()) {
        
        $collection = qb::_table('users');
		$collection->leftjoin('settings', 'settings.set_user_id', 'users.user_id'); 
		
        $account = $this->db->q_($collection->where($where)->select("*"));
        return $account;
    }
    
    function getAllList($where=array()) {
	
        $select = '*';
        $collection = qb::_table('users');
		
        return $this->db->q($collection->where($where)->select($select));    
    }
    
    // Education
    
    function getEduList($user=0){
	    
	    $where['edu_user_id'] = $user;
	    $where['edu_trash'] = 0;
        $order = " edu_year_from ASC";
        $select = 'user_education.*, monthfrom.mon_name as mfrom, monthto.mon_name as mto';
        $collection = qb::_table('user_education');
		$collection->leftjoin('month as monthfrom', 'monthfrom.mon_num', 'user_education.edu_month_from'," and monthfrom.mon_lang='".$_SESSION['account']['user_lang']."'");
		$collection->leftjoin('month as monthto', 'monthto.mon_num', 'user_education.edu_month_to'," and monthto.mon_lang='".$_SESSION['account']['user_lang']."'");
        return $this->db->q($collection->where($where)->orderBy($order)->select($select));    
    }
    
    
    function delEdu($id=0){
        $where = array(
            'edu_user_id' => $_SESSION['account']['user_id'],
          //  'edu_id' => $id
        );
        
        $data['edu_trash'] = 1;
        $this->db->q(qb::_table('user_education')->where($where)->update($data));    
        return true;
    }

    function addEdu($data){
        
        $this->db->q(qb::_table('user_education')->insert($data));  
       
        return $this->db->getLastID(); 
    }
    
    // Work

    function getWorkList($user=0){

        $where['work_user_id'] = $user;
        $where['work_trash'] = 0;
        $order = " work_year_from ASC";
        $select = 'user_work.*, monthfrom.mon_name as mfrom, monthto.mon_name as mto';
        $collection = qb::_table('user_work');
        $collection->leftjoin('month as monthfrom', 'monthfrom.mon_num', 'user_work.work_month_from'," and monthfrom.mon_lang='".$_SESSION['account']['user_lang']."'");
        $collection->leftjoin('month as monthto', 'monthto.mon_num', 'user_work.work_month_to'," and monthto.mon_lang='".$_SESSION['account']['user_lang']."'");
        return $this->db->q($collection->where($where)->orderBy($order)->select($select));
    }


    function delWork($id=0){
        $where = array(
            'work_user_id' => $_SESSION['account']['user_id'],
            //'work_id' => $id
        );

        $data['work_trash'] = 1;
        $this->db->q(qb::_table('user_work')->where($where)->delete());
        return true;
    }

    function addWork($data){

        $this->db->q(qb::_table('user_work')->insert($data));

        return $this->db->getLastID();
    }
}
