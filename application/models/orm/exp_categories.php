<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;


class exp_categories extends \floctopus\models\common\model
{

    function add($add){
        $this->db->q(qb::_table('expenses_category')->insert($add));
        return $this->db->getLastID();
    }

    function update($id = 0, $data = array()){
        $w['expcat_id'] = $id;
        $this->db->q(qb::_table('expenses_category')->where($w)->update($data));
        return true;
    }

    function delSubCat($id = 0){
        $where['expcat_user_id'] = $_SESSION['account']['user_id'];
        $where['expcat_id']=$id;
        $collection = qb::_table('expenses_category');

        $parentcat =  $this->db->q1($collection->where($where)->select("expcat_parent_id"));

        $whereupd['exp_user_id'] = $_SESSION['account']['user_id'];
        $whereupd['exp_category']=$id;
        $updparent['exp_category'] = $parentcat;
        $updparent['exp_category_parent'] = 0;
        $collection = qb::_table('expenses');
        $this->db->q($collection->where($whereupd)->update($updparent));

        $where['expcat_id']=$id;
        $where['expcat_user_id'] = $_SESSION['account']['user_id'];
        $upd['expcat_trash'] = 1;
        $collection = qb::_table('expenses_category');
        $this->db->q($collection->where($where)->update($upd));

        return false;

    }

    function delCat($id = 0){
        $where['expcat_id']=$id;
        $where['expcat_user_id'] = $_SESSION['account']['user_id'];
        $upd['expcat_trash'] = 1;
        $collection = qb::_table('expenses_category');
        $this->db->q($collection->where($where)->update($upd));


        $wheres['expcat_parent_id']=$id;
        $where['expcat_user_id'] = $_SESSION['account']['user_id'];
        $upds['expcat_trash'] = 1;
        $collection = qb::_table('expenses_category');
        $this->db->q($collection->where($wheres)->update($upds));


        $wherec['expcat_user_id'] = $_SESSION['account']['user_id'];
        $wherec['expcat_default'] = 1;
        $collection = qb::_table('expenses_category');

        $defcat =  $this->db->q1($collection->where($wherec)->select("expcat_id"));

        $whereupd= array();

        $whereupd['exp_category']=$id;
        $whereupd['exp_user_id'] = $_SESSION['account']['user_id'];
        $upddef['exp_category'] = $defcat;
        //$upddef['exp_category_parent'] = 0;
        $collection = qb::_table('expenses');
        $this->db->q($collection->where($whereupd)->update($upddef));

        $whereupd = array();
        $whereupd['exp_category_parent']=$id;
        $whereupd['exp_user_id'] = $_SESSION['account']['user_id'];
        $upddef['exp_category'] = $defcat;
        $upddef['exp_category_parent'] = 0;
        $collection = qb::_table('expenses');
        $this->db->q($collection->where($whereupd)->update($upddef));
        return false;


    }

    function getList($where = array(), $page = 1, $count = 1, $search = array(), $order = 'expcat_name ASC'){
		$where['expcat_user_id'] = $_SESSION['account']['user_id'];
        if ($page > 0) $page--;
        $offset = \intval($page * $count);
        $select = '*';
        $collection = qb::_table('expenses_category');

        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset, $count)->select($select));
    }


    function getAllList($where = array(),  $order = 'expcat_name ASC'){
		$where['expcat_user_id'] = $_SESSION['account']['user_id'];
        $where['expcat_trash'] = 0;
        $select = '*';
        $collection = qb::_table('expenses_category');

        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));
    }


    function getListCount($where = array(), $search = array()){
		$where['expcat_user_id'] = $_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('expenses_category');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));
    }


    function getByID($id = 0){
	    $where['expcat_user_id'] = $_SESSION['account']['user_id'];
        $where['expcat_id'] = $id;
        $select = '*';
        $collection = qb::_table('expenses_category');
        return $this->db->q_($collection->where($where)->select($select));

    }


}
