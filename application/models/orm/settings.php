<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class settings extends \floctopus\models\common\model {
	
	
    function add($add) {
	
        $this->db->q(qb::_table('settings')->insert($add));
        return $this->db->getLastID();      
    }

    function update($id=0, $data = array()) {
        $where = array(
            'set_id' => $id
        );
        
        $this->db->q(qb::_table('settings')->where($where)->update($data));    
        return true;
    }

	
}
