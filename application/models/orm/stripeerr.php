<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class stripeerr extends \floctopus\models\common\model {   
	
    function add($arr=array()){
	    $arr['stripeerr_co_id'] = $_SESSION['account']['co_id'];
		$this->db->q(qb::_table('stripeerror')->insert($arr));
        return $this->db->getLastID();
	}

    function getListCount($where=array(),$search = array(),$order='inv_id ASC') {
    	
    	$where['stripeerr_co_id']=$_SESSION['account']['co_id'];
        $select = '*';
        $collection = qb::_table('stripeerror');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));      
    }

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='log_date DESC') {
        
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $where['stripeerr_co_id']=$_SESSION['account']['co_id'];
    
        $select = '*';
        $collection = qb::_table('stripeerror');
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }
    
    function getByID($id=0) {
    	$where['log_id'] = $id;
    	$where['stripeerr_co_id']=$_SESSION['account']['co_id'];
        $select = '*';
        $collection = qb::_table('stripeerror');
        return $this->db->q_($collection->where($where)->select($select));    
    }    
    
    function getAllCount($w=array()){
        $select = '*';
        $collection = qb::_table('stripeerror');
      
        $w['stripeerr_co_id']=$_SESSION['account']['co_id'];
        return $this->db->q1($collection->where($w)->count($select));    
    }
	
	
	
}
