<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;


class docs extends \floctopus\models\common\model{

    function add($add){
        $this->db->q(qb::_table('docs')->insert($add));
        return $this->db->getLastID();
    }

    function update($id = 0, $data = array()){
        $w['doc_id'] = $id;
        $w['doc_user_id'] = $_SESSION['account']['user_id'];
        $this->db->q(qb::_table('docs')->where($w)->update($data));
        return true;
    }

    function delete($id = 0){
        $cArr['doc_trash'] = 1;
        $this->update($id, $cArr);
    }

    function getList($where = array(), $page = 1, $count = 1, $search = array(), $order = 'doc_type DESC, doc_name asc'){
        $where['doc_trash'] = 0;

        if ($page > 0) $page--;
        $offset = \intval($page * $count);
        $select = '*';
        $collection = qb::_table('docs');
        $collection->leftjoin('users', 'users.user_id', 'docs.doc_user_id');

        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset, $count)->select($select));
    }

    function getListCount($where = array(), $search = array()){
        $where['doc_trash'] = 0;

        //$where['project_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('docs');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));
    }

    function getListSearch($where = array(), $page = 1, $count = 1, $search = array(), $order = "doc_text ASC"){

        $where['doc_trash'] = 0;

        if ($page > 0) $page--;
        $offset = \intval($page * $count);

        $add = "";
        foreach ($search as $k => $tag) {
            if ($k > 0) $add .= " + ";
            $add .= "(doc_tags LIKE '" . $tag . "')";
            $searchnew['doc_tags'][] = $tag;
        }
        $ss = "(" . $add . ") as hits";

        $select = '*,' . $ss;
        $collection = qb::_table('docs');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('doc_id')->Limit($offset, $count)->select($select));
    }

    function getListCountSearch($where = array(), $search = array()){
        $where['doc_trash'] = 0;

        $add = "";
        foreach ($search as $k => $tag) {
            if ($k > 0) $add .= " + ";
            $add .= "(doc_tags LIKE '" . $tag . "')";
            $searchnew['doc_tags'][] = $tag;
        }
        $ss = "(" . $add . ") as hits";

        $collection = qb::_table('docs');

        $count = $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('doc_id')->count('*'));
        return count($count);
    }

    function getByID($id = 0){
        $where['doc_trash'] = 0;
		//$where['doc_user_id'] = $_SESSION['account']['user_id'];
        $where['doc_id'] = $id;
        $select = '*';
        $collection = qb::_table('docs');
        return $this->db->q_($collection->where($where)->select($select));
    }
    
    function getFolderDocIDS($folder=0,$order="doc_name ASC"){
		$arr = array();
		$where['doc_folder'] = $folder;

        $select = 'doc_id as id';
        $collection = qb::_table('docs');
		$records =  $this->db->q($collection->where($where)->OrderBy($order)->select($select));
	    
	    foreach($records as $k=>$v){
		    
		    $arr[] = $v['id'];
	    }
	    
	    $arr[] = $folder;
	    
	    return $arr;
	    
    }
    
	//Collaborators
	
	function getCollabs($id=0){
    
    	$where['dc_doc_id']=$id;
    	$where['dc_user_id'] = $_SESSION['account']['user_id'];
    	
        $select = '*';
        $collection = qb::_table('doc_collaborators');
        $collection->leftjoin('users', 'users.user_id', 'doc_collaborators.dc_collab_id');
        $r = $this->db->q($collection->where($where)->select('*'));    
        
        $o = $this->getByID($id);
        $r[]=$o;
        return $r;
	}

	function addCollab($cid=0,$id=0){
		
		$addm['dc_user_id'] = $_SESSION['account']['user_id'];
		$addm['dc_doc_id'] = $id;
		$addm['dc_collab_id'] = $cid;
        $this->db->q(qb::_table('doc_collaborators')->insert($addm));
        return $this->db->getLastID();      
	}

    function getCollabSelIDS($id){
	    $collab = array();
	    $where['dc_doc_id']=$id;
        $select = 'dc_collab_id';
        $collection = qb::_table('doc_collaborators');
        $rec = $this->db->qa($collection->where($where)->select($select));    
        
        if(is_array($rec)){
	        
	        foreach($rec as $v){
		        $collab[] = array_shift($v);
		        
	        }
        }
        
        return $collab;        
	    
    }
    
    function delShare($id=0){
	    
	    $where['dc_doc_id'] = $id;
	    $where['dc_user_id'] = $_SESSION['account']['user_id'];
	    $collection = qb::_table('doc_collaborators');
	    $this->db->q($collection->where($where)->delete()); 
		
		$upd['doc_shared'] = 0;
		$this->update($id,$upd);
		
		return false;
    }
    
    function addShare($add){
        $this->db->q(qb::_table('doc_collaborators')->insert($add));
        return $this->db->getLastID();
    }
		
    function getSharedDocsID(){
	    
	    $prjids = array();
	    
	    
	    $where['dc_collab_id']=$_SESSION['account']['user_id'];
	    //$dwhere['prju_user_id'] = $_SESSION['account']['user_id'];
        $select = 'dc_doc_id';
        $collection = qb::_table('doc_collaborators');
       // app::trace($collection->dwhere($dwhere)->groupby('prju_project_id')->select($select));
        $rec = $this->db->qa($collection->where($where)->groupby('dc_doc_id')->select($select));    
        
        if(is_array($rec)){
	        
	        foreach($rec as $v){
		        $prjids[] = array_shift($v);
	        }
        }
        
        return $prjids;        
    }

    
}
