<?php
namespace instantinvoices\models\orm;

use \instantinvoices\application as app;
use \jet\db\qb as qb;

class jobs extends \instantinvoices\models\common\model {
	
	
	function add($arr=array() ){

		 \date_default_timezone_set("UTC");  
		 $time_offset = date('P');
		
		 $this->db->q("SET `time_zone`='".$time_offset."'");

	
		$this->db->q(qb::_table('jobs')->insert($arr));
        $equipID = $this->db->getLastID();
        
        return $equipID;
	}
	
	function update($id=0,$arr=array()){
		$w['job_id']=$id;
		$this->db->q(qb::_table('jobs')->where($w)->update($arr));
        return true;
	}

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='jobs.job_date DESC') {
        if($page>0)$page--;
        $offset = \intval($page*$count);

        $select = '*';
        $collection = qb::_table('jobs');
	    //$collection->leftjoin('jobs_provider', 'jobs_provider.jp_code', 'job_provider');
	    $collection->leftjoin('job_fav', 'job_fav.jobf_job_id', 'job_id',' AND job_fav.jobf_co_id = '.$_SESSION['account']['co_id']);
        $collection->leftjoin('jobs_bid', 'jobs_bid.jbid_job_id', 'job_id',' AND jobs_bid.jbid_co_id = '.$_SESSION['account']['co_id']);
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getListCount($where=array(),$search = array(),$order='jobs.job_date DESC') {
        $select = '*';
        $collection = qb::_table('jobs');
	   // $collection->leftjoin('jobs_provider', 'jobs_provider.jp_code', 'job_provider');
	   
        $collection = qb::_table('jobs');
         $collection->leftjoin('job_fav', 'job_fav.jobf_job_id', 'job_id',' AND job_fav.jobf_co_id = '.$_SESSION['account']['co_id']);
        return $this->db->q1($collection->where($where)->Search($search)->OrderBy($order)->count('*'));    
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order='tag_hits DESC') {

        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(job_tags LIKE '".$tag."')";
			$searchnew['job_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('jobs');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('job_id')->Limit($offset,$count)->select($select));    
    }

    function getListCountSearch($where=array(),$search = array()) {

		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(job_tags LIKE '".$tag."')";
			$searchnew['job_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('jobs');
        $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('job_id')->count('*'));  
       return count($count);  
    }

    function getByProviderID($id="") {
    
    	$where['job_provider_id'] = $id;
        $select = '*';
        $collection = qb::_table('jobs');
        //$collection->leftjoin('jobs', 'equip_suplier', 'customs.custom_id');
        
        return $this->db->q_($collection->where($where)->select($select));    
    }	

    function getByID($id=0) {
    
    	$where['job_id'] = $id;
        $select = '*';
        $collection = qb::_table('jobs');
        //$collection->leftjoin('jobs', 'equip_suplier', 'customs.custom_id');
        $collection->leftjoin('job_fav', 'job_fav.jobf_job_id', 'job_id',' AND job_fav.jobf_co_id = '.$_SESSION['account']['co_id']);
         $collection->leftjoin('jobs_bid', 'jobs_bid.jbid_job_id', 'job_id',' AND jobs_bid.jbid_co_id = '.$_SESSION['account']['co_id']);
        return $this->db->q_($collection->where($where)->select($select));    
    }	
    
    function getJobInfo($where=array()){
    
        $select = '*';
        $collection = qb::_table('jobs');
        //$collection->leftjoin('jobs', 'equip_suplier', 'customs.custom_id');
        $collection->leftjoin('job_fav', 'job_fav.jobf_job_id', 'job_id',' AND job_fav.jobf_co_id = '.$_SESSION['account']['co_id']);
        $collection->leftjoin('jobs_bid', 'jobs_bid.jbid_job_id', 'job_id',' AND jobs_bid.jbid_co_id = '.$_SESSION['account']['co_id']);
        return $this->db->q_($collection->where($where)->select($select));    
    }


    function getJobInfoWithoutBid($where=array()){
    
        $select = '*';
        $collection = qb::_table('jobs');
        return $this->db->q_($collection->where($where)->select($select));    
    }

    
    function addFav($id=0){
	    
	    $arr = array(
		    "jobf_co_id" => $_SESSION['account']['co_id'],
		    "jobf_job_id" => $id
		    
	    );
		$this->db->q(qb::_table('job_fav')->insert($arr));
        $ids = $this->db->getLastID();
        
        return $ids;
	    
    }


    function delFav($id=0){
	    
	    $arr = array(
		    "jobf_co_id" => $_SESSION['account']['co_id'],
		    "jobf_job_id" => $id
		    
	    );
		$this->db->q(qb::_table('job_fav')->where($arr)->delete());
        
        
        return true;
	    
    }

    function addTag($tag){
	    
	    $whereTag['jtag_tag'] = $tag;
        $istag = $this->db->q_(qb::_table('jobs_tags')->where($whereTag)->select('*'));    
	    
	    if(!$istag){
		    $arr = array(
			    
			    "jtag_tag" => $tag
			    
		    );
			$this->db->q(qb::_table('jobs_tags')->insert($arr));
	        $ids = $this->db->getLastID();
	    }
	    
        
        return true;
	    
    }

    function addBid($add){
	    
			$this->db->q(qb::_table('jobs_bid')->insert($add));
	        $ids = $this->db->getLastID();
	
        
        return true;
	    
    }


    function getAllTagsList($search=array(),$order='jtag_tag') {
	    
	    
        $select = '*';
        $collection = qb::_table('jobs_tags');
        return $this->db->q($collection->Search($search)->OrderBy($order)->select($select));    
    }

    function getAllBidsList($where=array(),$order='jbid_date') {
	    
	    
        $select = 'jobs_bid.*,company.user_nick as unick, company.user_pic as upic';
        $collection = qb::_table('jobs_bid');
        $collection->leftjoin('company', 'company.co_id', 'jbid_co_id');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }


	
}
