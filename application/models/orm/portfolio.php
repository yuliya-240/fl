<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;


class portfolio extends \floctopus\models\common\model
{

    function add($add)
    {
        $this->db->q(qb::_table('portfolio')->insert($add));
        return $this->db->getLastID();
    }

    function update($id = 0, $data = array())
    {
        $w['portfolio_id'] = $id;
        $this->db->q(qb::_table('portfolio')->where($w)->update($data));
        return true;
    }

    function updateMass($id=0, $data = array()) {
        $where = array(
            'portfolio_id' => $id
        );

        $this->db->q(qb::_table('portfolio')->where($where)->update($data));
        return true;
    }


    function delete($id = 0)
    {
        $cArr['portfolio_trash'] = 1;
        $this->update($id, $cArr);
    }

    function getList($where = array(), $page = 1, $count = 1, $search = array(), $order = 'portfolio_id DESC')
    {

        if ($page > 0) $page--;
        $offset = \intval($page * $count);
        $select = '*';
        $collection = qb::_table('portfolio');
       // $collection->leftjoin('users', 'users.user_id', 'portfolio.portfolio_user_id');

        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset, $count)->select($select));
    }

    function getAllList($id=0)
    {
        $where['portfolio_trash'] = 0;
        $where['portfolio_user_id'] = $id;
        $select = '*';
        $collection = qb::_table('portfolio');

        return $this->db->q($collection->where($where)->select($select));
    }

    function getListCount($where = array(), $search = array())
    {

        $select = '*';
        $collection = qb::_table('portfolio');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));
    }

    function getListSearch($where = array(), $page = 1, $count = 1, $search = array(), $order = "portfolio_id ASC")
    {

        $where['portfolio_trash'] = 0;
        $where['portfolio_user_id'] = $_SESSION['account']['user_id'];
        if ($page > 0) $page--;
        $offset = \intval($page * $count);

        $add = "";
        foreach ($search as $k => $tag) {
            if ($k > 0) $add .= " + ";
            $add .= "(portfolio_title LIKE '" . $tag . "')";
            $searchnew['portfolio_title'][] = $tag;
        }
        $ss = "(" . $add . ") as hits";

        $select = '*,' . $ss;
        $collection = qb::_table('portfolio');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('portfolio_id')->Limit($offset, $count)->select($select));
    }

    function getListCountSearch($where = array(), $search = array())
    {
        $where['portfolio_trash'] = 0;
        $where['portfolio_user_id'] = $_SESSION['account']['user_id'];
        $add = "";
        foreach ($search as $k => $tag) {
            if ($k > 0) $add .= " + ";
            $add .= "(portfolio_title LIKE '" . $tag . "')";
            $searchnew['portfolio_title'][] = $tag;
        }
        $ss = "(" . $add . ") as hits";

        $collection = qb::_table('portfolio');

        $count = $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('portfolio_id')->count('*'));
        return count($count);
    }


    function getByID($id = 0)
    {
        $where['portfolio_id'] = $id;
        $select = '*';
        $collection = qb::_table('portfolio');
       // $collection->leftjoin('portfoliopic', 'portfoliopic.portfoliopic_pid', 'portfolio.portfolio_id');
        return $this->db->q_($collection->where($where)->select($select));

    }

// portfolio pic

    function addPortfoliopic($addp=array(),$id){

        $addp['portfoliopic_user_id'] = $_SESSION['account']['user_id'];
        $addp['portfoliopic_portfolio_id'] = $id;

        $this->db->q(qb::_table('portfolio_pic')->insert($addp));
        return $this->db->getLastID();
    }

    function getPortfoliopic($id=0){

        $where['portfoliopic_portfolio_id']=$id;
        $select = '*';
        $collection = qb::_table('portfolio_pic');
        return $this->db->q($collection->where($where)->select('*'));
    }

    function delPortfoliopic($id=0){

        $where['portfoliopic_portfolio_id'] = $id;
        $collection = qb::_table('portfolio_pic');
        $this->db->q($collection->where($where)->delete());
        return false;

    }
    



}
