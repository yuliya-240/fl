<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class users extends \floctopus\models\common\model {
	
    function add($user) {
	
        
        $userid = $this->db->q(qb::_table('users')->insert($user));
        
        $res['uid'] = $userid;
        return $res;
    }
    

    function getAllList($where=array()) {
	
        $select = '*';
        $collection = qb::_table('users');
		
        return $this->db->q($collection->where($where)->select($select));    
    }

    function update($id=0, $data = array()) {
        $where = array(
            'user_id' => $id
        );
        
        $this->db->q(qb::_table('users')->where($where)->update($data));    
        
        return true;
    }

	
}
