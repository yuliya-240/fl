<?php
namespace floctopus\models\orm;
use \jet\db\qb as qb;

class pictures extends \floctopus\models\common\model {

	function add($arr=array()){
		$this->db->q(qb::_table('pictures')->insert($arr));
        return $this->db->getLastID();
	}

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='pic_id ASC') {
        
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $where['pic_co_id']=$_SESSION['account']['co_id'];
        $select = '*';
        $collection = qb::_table('pictures');
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }	

    function getListCount($where=array(),$search = array()) {
    
    	$where['pic_co_id']=$_SESSION['account']['co_id'];
        $select = '*';
        $collection = qb::_table('pictures');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getAllCount($w=array()){
        $where['pic_co_id']=$_SESSION['account']['co_id']; 
        $select = '*';
        $collection = qb::_table('pictures');
		       
		return $this->db->q1($collection->where($w)->count($select));    
    }
    
    function getAllList($where=array(),$search = array(),$order='pic_id ASC'){
        $where['pic_co_id']=$_SESSION['account']['co_id'];
        $select = '*';
        $collection = qb::_table('pictures');
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->select($select));    
    }
    
    function delete($where){
	    
        $collection = qb::_table('pictures');
        return $this->db->q($collection->where($where)->delete());    
	    
    }

	
}