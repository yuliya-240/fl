<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class services extends \floctopus\models\common\model {
	
	function add($srvArr=array()){
		$this->db->q(qb::_table('services')->insert($srvArr));
        return $this->db->getLastID();
	}
	
	function update($itemID=0,$itemArr=array()){
		$w['srv_user_id'] = $_SESSION['account']['user_id'];
		$w['srv_id']=$itemID;
		$this->db->q(qb::_table('services')->where($w)->update($itemArr));
        return true;
	}

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='') {
        if($page>0)$page--;
        $offset = \intval($page*$count);
		$where['srv_trash']=0;
        $where['srv_user_id'] = $_SESSION['account']['user_id'];

        $select = '*';
        $collection = qb::_table('services');
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }


    function getListCount($where=array(),$search = array()) {
        $select = '*';
		$where['srv_trash']=0;
        $where['srv_user_id'] = $_SESSION['account']['user_id'];

        $collection = qb::_table('services');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }


    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order='hits DESC') {
		$where['srv_trash']=0;
        $where['srv_user_id'] = $_SESSION['account']['user_id'];

        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(srv_name LIKE '".$tag."')";
			$searchnew['srv_name'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('services');
        
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('srv_id')->Limit($offset,$count)->select($select));    
    }
    

    function getListCountSearch($where=array(),$search = array()) {
		$where['srv_trash']=0;
        $where['srv_user_id'] = $_SESSION['account']['user_id'];

		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(srv_name LIKE '".$tag."')";
			$searchnew['srv_name'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('services');

        $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('srv_id')->count('*'));  
        return count($count);  
    }


    function getByID($id=0) {
        $select = '*';
        $where['srv_id'] = $id;
        $where['srv_user_id'] = $_SESSION['account']['user_id'];
        $collection = qb::_table('services');
        return $this->db->q_($collection->where($where)->select($select));    
    }



    function getInfo($where=array()) {
        $select = '*';
		$where['srv_trash']=0;
        $where['srv_user_id'] = $_SESSION['account']['user_id'];
        
        $collection = qb::_table('services');
        return $this->db->q_($collection->where($where)->select($select));    
    }
    
    function delete($id =0 ){
	  	 $where['srv_trash']=1;
	    
	    $this->update($id,$where);    
	    return true;
    }
    
    function getAllList($where=array(),$order='item_name ASC'){
        $select = '*';
		$where['srv_trash']=0;
        $where['srv_user_id'] = $_SESSION['account']['user_id'];
        
        $collection = qb::_table('services');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
	    
    }
    
    function getAllCount($where=array()){
        $select = '*';
		$where['srv_trash']=0;
        $where['srv_user_id'] = $_SESSION['account']['user_id'];
        $collection = qb::_table('services');
        return $this->db->q1($collection->where($where)->count($select));    
    }
    
	
	
	
}
