<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;


class portfolio_pic extends \floctopus\models\common\model
{

    function add($add)
    {
        $this->db->q(qb::_table('portfolio_pic')->insert($add));
        return $this->db->getLastID();
    }

    function update($id = 0, $data = array())
    {
        $w['portfoliopic_id'] = $id;
        $this->db->q(qb::_table('portfolio_pic')->where($w)->update($data));
        return true;
    }

    function delete($id = 0)
    {
        $cArr['portfoliopic_trash'] = 1;
        $this->update($id, $cArr);
    }

    function getList($where = array(), $page = 1, $count = 1, $search = array(), $order = 'portfoliopic_id DESC')
    {

        if ($page > 0) $page--;
        $offset = \intval($page * $count);
        $select = '*';
        $collection = qb::_table('portfolio_pic');
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset, $count)->select($select));
    }

    function getListCount($where = array(), $search = array())
    {

        $select = '*';
        $collection = qb::_table('portfolio_pic');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));
    }


    function getByID($id = 0)
    {
        $where['portfolio_id'] = $id;
        $select = '*';
        $collection = qb::_table('portfolio');
        //$collection->leftjoin('portfolio_pic', 'portfolio_pic.portfoliopic_pid', 'portfolio.portfolio_id');
        return $this->db->q_($collection->where($where)->select($select));
    }
}
