<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class recevents extends \floctopus\models\common\model {
	
	function getAllRecurringEventCount($where=array()){
        $select = '*';
        $collection = qb::_table('events_recurring');
        return $this->db->q1($collection->where($where)->count($select));    
    }
   
    function getAllRecurringEvents($where=array(),$exception=array()){
     	
     	if(count($exception)>1){
			foreach($exception as $v){
	     		$where['er_id NOT '][]=$v;
		 	}
	     	
     	}elseif(count($exception)==1){
	     	$where['er_id!']=$exception[0];
     	}
     	
        $select = '*';
        $collection = qb::_table('events_recurring');
        
         return $this->db->q($collection->where($where)->select($select));    
    }
    
	function getDailyRecurringIDs($selDate ,$where=array()){

		$where['e_date'] = $selDate;
		$where['e_user_id'] = $_SESSION['account']['user_id'];
		$where['e_recurring_id!']=0;
		$order = "e_id ASC";

        $select = 'e_recurring_id as rec';
        $collection = qb::_table('events');
       
        $re = $this->db->q($collection->where($where)->OrderBy($order)->select($select));  
        $ids = array();
        	if($re){
	        	
				foreach($re as $v){
					$ids[]=$v['rec'];
				}

        	}
	        return $ids;	
	}
	
}