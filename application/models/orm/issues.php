<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;
use \floctopus\models\orm\calendar as OrmCalendar;

class issues extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('issues')->insert($add));
        return $this->db->getLastID();      
    }

    function update($id=0, $data = array()) {
        $where = array(
            'issue_id' => $id
        );
        
        $this->db->q(qb::_table('issues')->where($where)->update($data));    
        return true;
    }

    function updateMass($where=array(), $data = array()) {

        
        $this->db->q(qb::_table('issues')->where($where)->update($data));    
        return true;
    }

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='issues.issue_deadline DESC') {
	  
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = 'issues.*,issue_category.*,issue_priority.*,issue_status.*,users.*,projects.*,project_milestones.*,assign.user_username as assigned';
        $collection = qb::_table('issues');
        $collection->leftjoin('issue_category', 'issue_category.issuecat_cat', 'issues.issue_cat'," and issue_category.issuecat_lang='".$_SESSION['account']['user_lang']."'");
        $collection->leftjoin('issue_priority', 'issue_priority.issueprior_prior', 'issues.issue_priority'," and issue_priority.issueprior_lang='".$_SESSION['account']['user_lang']."'");
        $collection->leftjoin('issue_status', 'issue_status.issuestatus_status', 'issues.issue_status'," and issue_status.issuestatus_lang='".$_SESSION['account']['user_lang']."'");
		$collection->leftjoin('users', 'users.user_id', 'issues.issue_user_id');
		$collection->leftjoin('users as assign', 'assign.user_id', 'issues.issue_assign');
 		$collection->leftjoin('projects', 'projects.project_id', 'issues.issue_project_id');
 		$collection->leftjoin('project_milestones', 'project_milestones.prjm_id', 'issues.issue_milestone_id');
 		
 		//app::trace($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="issue_deadline DESC") {
		
    	//$where['issue_trash']=0;
 
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(issue_subj LIKE '".$tag."')";
			$searchnew['issue_subj'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = 'issues.*,issue_category.*,issue_priority.*,issue_status.*,users.*,projects.*,project_milestones.*,assign.user_username as assigned,'.$ss;
        $collection = qb::_table('issues');
        $collection->leftjoin('issue_category', 'issue_category.issuecat_cat', 'issues.issue_cat'," and issue_category.issuecat_lang='".$_SESSION['account']['user_lang']."'");
        $collection->leftjoin('issue_priority', 'issue_priority.issueprior_prior', 'issues.issue_priority'," and issue_priority.issueprior_lang='".$_SESSION['account']['user_lang']."'");
        $collection->leftjoin('issue_status', 'issue_status.issuestatus_status', 'issues.issue_status'," and issue_status.issuestatus_lang='".$_SESSION['account']['user_lang']."'");
		$collection->leftjoin('users', 'users.user_id', 'issues.issue_user_id');
		$collection->leftjoin('users as assign', 'assign.user_id', 'issues.issue_assign');
 		$collection->leftjoin('projects', 'projects.project_id', 'issues.issue_project_id');
 		$collection->leftjoin('project_milestones', 'project_milestones.prjm_id', 'issues.issue_milestone_id');
       
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('issue_id')->Limit($offset,$count)->select($select));    
    }

    function getListCount($where=array(),$search = array()) {
        $select = '*';
        $collection = qb::_table('issues');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getListCountSearch($where=array(),$search = array()) {

		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(issue_subj LIKE '".$tag."')";
			$searchnew['issue_subj'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('issues');

       $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('issue_id')->count('*'));  
       return count($count);  
    }
	
	function getByID($id=0){
		
		$where['issue_id'] = $id;
	  
        $select = 'issues.*,issue_category.*,issue_priority.*,issue_status.*,issue_status.*,users.*,projects.*,assign.user_username as assignuser,assign.user_pic as assignpic,assign.user_id as assigid,project_milestones.*';
        $collection = qb::_table('issues');
        $collection->leftjoin('issue_category', 'issue_category.issuecat_cat', 'issues.issue_cat'," and issue_category.issuecat_lang='".$_SESSION['account']['user_lang']."'");
        $collection->leftjoin('issue_priority', 'issue_priority.issueprior_prior', 'issues.issue_priority'," and issue_priority.issueprior_lang='".$_SESSION['account']['user_lang']."'");
        $collection->leftjoin('issue_status', 'issue_status.issuestatus_status', 'issues.issue_status'," and issue_status.issuestatus_lang='".$_SESSION['account']['user_lang']."'");
        $collection->leftjoin('project_milestones', 'project_milestones.prjm_id', 'issues.issue_milestone_id');
        
		$collection->leftjoin('users', 'users.user_id', 'issues.issue_user_id');
 		$collection->leftjoin('projects', 'projects.project_id', 'issues.issue_project_id');
 		$collection->leftjoin('users as assign', 'assign.user_id', 'issues.issue_assign');
       
        return $this->db->q_($collection->where($where)->select($select));    
	}
    
    function getListIssueFromAllProjects($projects=array(),$page=1,$count=1, $search = array(),$order='issue_id DESC') {

		$dwhere['issue_project_id'] = $projects;
		$where="issue_user_id = '".$_SESSION['account']['user_id']."' AND issue_project_id='0'";

        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('issues');
        
        return $this->db->q($collection->dwhere($where)->dwhere($dwhere)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }
    
    function getAllCatList($where=array()){
    	$where['issuecat_lang'] = $_SESSION['account']['user_lang'];
        $select = '*';
        $collection = qb::_table('issue_category');
        return $this->db->q($collection->where($where)->select('*'));    
	    
	    
    }
    
    function getAllPriorList($where=array()){
    	$where['issueprior_lang'] = $_SESSION['account']['user_lang'];
        $select = '*';
        $collection = qb::_table('issue_priority');
        return $this->db->q($collection->where($where)->select('*'));    
	    
	    
    }

    function getAllStatuses($where=array()){
    	$where['issuestatus_lang'] = $_SESSION['account']['user_lang'];
        $select = '*';
        $collection = qb::_table('issue_status');
        return $this->db->q($collection->where($where)->select('*'));    
    }

	function addattach($addf=array()){
		
        $this->db->q(qb::_table('issue_attachment')->insert($addf));
        return $this->db->getLastID();      
		
		
	}  
	
    function getListAttach($where=array(),$order='issueatt_filename ASC') {
	  
		$where['issueatt_user_id'] = $_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('issue_attachment');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }
	
	function addMessage($id,$msg){
        
        $add['ihist_issue_id'] = $id;
        $add['ihist_message'] = $msg;
        $add['ihist_user_id'] = $_SESSION['account']['user_id'];
        $add['ihist_msg'] = 1;
        $this->db->q(qb::_table('issue_history')->insert($add));
        return $this->db->getLastID();      
	}
	
	function addHistory($add = array()){
		
		
        $this->db->q(qb::_table('issue_history')->insert($add));
        return $this->db->getLastID();      
		
	}
	
	function getHistory($id=0){
		
		$where['ihist_issue_id'] = $id;
        $collection = qb::_table('issue_history');
		$collection->leftjoin('users', 'users.user_id', 'issue_history.ihist_user_id');
		$collection->leftjoin('issue_status', 'issue_status.issuestatus_status', 'issue_history.ihist_status', " and issue_status.issuestatus_lang='".$_SESSION['account']['user_lang']."'");
		$collection->leftjoin('issue_priority', 'issue_priority.issueprior_prior', 'issue_history.ihist_priority'," and issue_priority.issueprior_lang='".$_SESSION['account']['user_lang']."'");
		$collection->leftjoin('issue_category', 'issue_category.issuecat_cat', 'issue_history.ihist_cat'," and issue_category.issuecat_lang='".$_SESSION['account']['user_lang']."'");
		$collection->leftjoin('project_milestones', 'project_milestones.prjm_id', 'issue_history.ihist_milestone');
		$collection->leftjoin('projects', 'projects.project_id', 'issue_history.ihist_prj');
        return $this->db->q($collection->where($where)->orderby('ihist_id asc')->select("*"));    
		
		
	}
	
	function delete($id=0){
		
		$where['issue_id'] = $id;
        $this->db->q(qb::_table('issues')->where($where)->delete());
       
		$hist['ihist_issue_id'] = $id;
        $this->db->q(qb::_table('issue_history')->where($hist)->delete());
        
        $cal = new OrmCalendar();
		$upd['e_trash'] = 1;
		$whu['e_type'] = 3;
		$whu['e_object_id'] = $id;
        $cal->updateMass($whu,$upd);

		return false;
		
	}
}
