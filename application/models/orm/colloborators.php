<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class colloborators extends \floctopus\models\common\model {
	
    function add($user) {
	
        $this->db->q(qb::_table('collaborator')->insert($user));
        return $this->db->getLastID();      
    }

    function update($id=0, $data = array()) {
        $where = array(
            'cb_id' => $id
        );
        
        $this->db->q(qb::_table('collaborator')->where($where)->update($data));    
        return true;
    }

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='cb_id DESC') {
	    $where['cb_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('collaborator');
		$collection->leftjoin('users', 'users.user_id', 'collaborator.cb_collobarator_id');
		$collection->leftjoin('collaborator_statuses', 'collaborator_statuses.cbs_status_id', 'collaborator.cb_status'," and collaborator_statuses.cbs_lang='".$_SESSION['account']['user_lang']."'");
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getAllList($where=array(),$nwhere=array(),$order='cb_id DESC') {
	    $where['cb_user_id']=$_SESSION['account']['user_id'];
	    
        $select = '*';
        $collection = qb::_table('collaborator');
		$collection->leftjoin('users', 'users.user_id', 'collaborator.cb_collobarator_id');
		//app::trace($collection->where($where)->OrderBy($order)->select($select));
        return $this->db->q($collection->where($where)->nwhere($nwhere)->OrderBy($order)->select($select));    
    }
    
    function getListCount($where=array(),$search = array()) {
	
    	$where['cb_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('collaborator');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }
    
    function del($id=0){
	    
	
    	$where['cb_id']=$id;

        $collection = qb::_table('collaborator');
        return $this->db->q($collection->where($where)->delete());    
    }
    
    function delFromPrj($user){
    	$where['prju_collaborator_id']=$user;

        $collection = qb::_table('project_collaborators');
        return $this->db->q($collection->where($where)->delete());    
	    
    }

	function addInvite($addinv){
		
        $this->db->q(qb::_table('invites')->insert($addinv));
        return $this->db->getLastID();      
	}

	function getByID($id=0){

    	$where['cb_id']=$id;
        $collection = qb::_table('collaborator');
        $collection->leftjoin('users', 'users.user_id', 'collaborator.cb_collobarator_id');
        $collection->leftjoin('countries', 'countries.country_iso', 'cb_billing_country');
        return $this->db->q_($collection->where($where)->select('*'));    
	}
	
	function getInfo($where){
		

        $collection = qb::_table('collaborator');
        return $this->db->q_($collection->where($where)->select('*'));    
		
		
	}

	function isInviteSent($email){

    	$where['invite_from']=$_SESSION['account']['user_id'];
    	$where['invite_email'] = $email;
        $select = '*';
        $collection = qb::_table('invites');
        return $this->db->q1($collection->where($where)->count('*'));    
	}
	
	function isCollab($user,$collab){
		
    	$where['cb_user_id']=$user;
    	$where['cb_collobarator_id']=$collab;
        $collection = qb::_table('collaborator');
        return $this->db->q_($collection->where($where)->select('*'));    
	}


	function countIncomingInvites(){
		
    	$where['cb_user_id']=$_SESSION['account']['user_id'];
    	$where['cb_status']=2;
        $collection = qb::_table('collaborator');
        return $this->db->q1($collection->where($where)->count('*'));    
		
	}
    
    function updateInvite($id,$upd){
	    
        $where = array(
            'invite_id' => $id
        );
        
        $this->db->q(qb::_table('invites')->where($where)->update($upd));    
        return true;
	    
	    
    }
    
	function getSentInvites($email,$id){
    	
    	$where['invite_email'] = $email;
        $select = '*';
        $collection = qb::_table('invites');
        $inv =  $this->db->q($collection->where($where)->select('*'));
        
        foreach($inv as $i){
	        
	        $add['cb_user_id'] = $i['invite_from'];
	        $add['cb_collobarator_id'] = $id;
	        $add['cb_status'] = 1;
	        $this->add($add);

	        $add['cb_user_id'] =  $_SESSION['account']['user_id'];
	        $add['cb_collobarator_id'] = $i['invite_from'];
	        $add['cb_status'] = 2;
	        $this->add($add);


	        $addn['n_user_id'] = $_SESSION['account']['user_id'];
	        $addn['n_src'] = 1;
	        $addn['n_object_id'] = $i['invite_from'];
	        $addn['n_type'] = "invite_received";
	        $addn['n_link'] ="/users/invites";
	        $this->db->q(qb::_table('notifications')->insert($addn));
	        
	        $upd['invite_status']=2;
	        $this->updateInvite($i['invite_id'],$upd);
        }    
		
		return false;
	}
	
	function getEmailInviteByKey($key){
    	$where['invite_key'] = $key;
        $select = 'invite_email';
        $collection = qb::_table('invites');
        return  $this->db->q1($collection->where($where)->select($select));
		
		
	}

	function getNameByID($id=0){

    	$where['cb_id']=$id;
        $collection = qb::_table('collaborator');
        return $this->db->q1($collection->where($where)->select('cb_billing_name'));    
	}
	
}
