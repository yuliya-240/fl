<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class stripe extends \floctopus\models\common\model {
	
	function add($srvArr=array()){
		$this->db->q(qb::_table('stripe')->insert($srvArr));
        return $this->db->getLastID();
	}

    function get($where=array()) {
        $select = '*';

        //$where['stripe_co_id'] = $_SESSION['account']['co_id'];
        $collection = qb::_table('stripe');
        return $this->db->q_($collection->where($where)->select($select));    
    }
    
    function deleteByUserID($id){
	    $where['stripe_user_id'] = $id;
	    $where['stripe_co_id'] = $_SESSION['account']['co_id'];
	    $collection = qb::_table('stripe');
	    $this->db->q($collection->where($where)->delete()); 

    }
	
	
}
