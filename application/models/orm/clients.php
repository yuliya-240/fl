<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class clients extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('clients')->insert($add));
        return $this->db->getLastID();      
    }

    function update($id=0, $data = array()) {
        $where = array(
            'client_id' => $id
        );
        
        $this->db->q(qb::_table('clients')->where($where)->update($data));    
        return true;
    }

    function updateMass($where=array(), $data = array()) {

        
        $this->db->q(qb::_table('clients')->where($where)->update($data));    
        return true;
    }

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='client_name ASC') {
	    $where['client_user_id']=$_SESSION['account']['user_id'];
	    $where['client_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('clients');
        $collection->leftjoin('countries', 'countries.country_iso', 'clients.client_country');       
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getAllList($where=array(),$order='client_name ASC') {
	    $where['client_user_id']=$_SESSION['account']['user_id'];
	    $where['client_trash']=0;
        $select = '*';
        $collection = qb::_table('clients');
        $collection->leftjoin('countries', 'countries.country_iso', 'clients.client_country');       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

    function getListCount($where=array(),$search = array()) {
	    $where['client_user_id']=$_SESSION['account']['user_id'];
	    $where['client_trash']=0;
        $select = '*';
        $collection = qb::_table('clients');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="client_name ASC") {
		
    	$where['client_trash']=0;
    	$where['client_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(client_tags LIKE '".$tag."')";
			$searchnew['client_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('clients');
        $collection->leftjoin('countries', 'countries.country_iso', 'clients.client_country');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('client_id')->Limit($offset,$count)->select($select));    
    }

    function getListCountSearch($where=array(),$search = array()) {
    	$where['client_trash']=0;
    	$where['client_user_id']=$_SESSION['account']['user_id'];
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(client_tags LIKE '".$tag."')";
			$searchnew['client_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('clients');

       $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('client_id')->count('*'));  
       return count($count);  
    }
	
	function getByID($id=0){
		
		$where['client_id'] = $id;
	    $where['client_user_id'] = $_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('clients');
        $collection->leftjoin('countries', 'countries.country_iso', 'clients.client_country');
        $collection->leftjoin('currencies', 'currencies.currency_iso', 'clients.client_currency');
        return $this->db->q_($collection->where($where)->select($select));    
	}

	function getCronByID($id=0,$userid=0){
		
		$where['client_id'] = $id;
	    $where['client_user_id'] = $userid;
        $select = '*';
        $collection = qb::_table('clients');
        return $this->db->q_($collection->where($where)->select($select));    
	}
	
	function getAllForChat($where=array(), $order="client_name"){
	    $where['client_collab'] =0;
	    $where['client_user_id']=$_SESSION['account']['user_id'];
	    $where['client_trash']=0;
        $select = '*';
        $collection = qb::_table('clients');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
	}


    function getAllAllList($where=array(),$order='client_name ASC') {
        $select = '*';
        $collection = qb::_table('clients');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

	function getNameByID($id=0){
		
		$where['client_id'] = $id;
	    $where['client_user_id'] = $_SESSION['account']['user_id'];
        $select = 'client_name';
        $collection = qb::_table('clients');
        return $this->db->q1($collection->where($where)->select($select));    
	}	
}
