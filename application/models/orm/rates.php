<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class rates extends \floctopus\models\common\model {
	

    function add($add) {
	
        $this->db->q(qb::_table('exrates')->insert($add));
        return $this->db->getLastID();      
    }

	function getAll($currency="USD"){
		
     	$where['exr_base']=$currency;
        $collection = qb::_table('exrates');
              
        return $this->db->q($collection->where($where)->select("*"));    
	}
	
	function delete($currency="USD"){
		
     	$where['exr_base']=$currency;
        $collection = qb::_table('exrates');
       
        return $this->db->q($collection->where($where)->delete());    
		
		
	}	
	
}
