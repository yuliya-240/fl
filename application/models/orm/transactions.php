<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class transactions extends \floctopus\models\common\model {

	function add($arr=array()){
		$this->db->q(qb::_table('transactions')->insert($arr));
        return $this->db->getLastID();
	}

	function update($id=0,$arr=array()){
        //функция обновляет расходы(expenses)
		$w['t_exp_id']=$id;
		$w['t_user_id'] = $_SESSION['account']['user_id']; 
		$this->db->q(qb::_table('transactions')->where($w)->update($arr));
        return true;
	}	

	function updateMass($w=array(),$arr=array()){
		
		$w['t_user_id'] = $_SESSION['account']['user_id']; 
		$this->db->q(qb::_table('transactions')->where($w)->update($arr));
        return true;
	}

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='t_date ASC') {
        $where['t_user_id']=$_SESSION['account']['user_id'];
        $where['t_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('transactions');
        $collection->leftjoin('invoices', 'invoices.inv_id', 'transactions.t_inv_id');
        $collection->leftjoin('expenses', 'expenses.exp_id', 'transactions.t_exp_id');
        $collection->leftjoin('payments_method', 'payments_method.paymethod_method', 'transactions.t_method', " and paymethod_lang='".$_SESSION['account']['user_lang']."'");
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));
    }

    function getAllList($where=array(),$order='t_date ASC') {
        $where['t_user_id']=$_SESSION['account']['user_id'];
        $where['t_trash']=0;

        $select = '*';
        $collection = qb::_table('transactions');

        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));
    }


    function getListCount($where=array(),$search = array()) {
        $where['t_user_id']=$_SESSION['account']['user_id'];
        $where['t_trash']=0;
        $select = '*';
        $collection = qb::_table('transactions');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="t_date ASC") {

        $where['t_trash']=0;
        $where['t_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);

        $add="";
        foreach($search as $k=>$tag){
            if($k>0)$add.=" + ";
            $add.= "(t_tags LIKE '".$tag."')";
            $searchnew['t_tags'][]=$tag;
        }
        $ss = "(".$add.") as hits";

        $select = '*,'.$ss;
        $collection = qb::_table('transactions');
        // $collection->leftjoin('countries', 'countries.country_iso', 'clients.inv_country');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('t_id')->Limit($offset,$count)->select($select));
    }

    function getListCountSearch($where=array(),$search = array()) {
        $where['t_trash']=0;
        $where['t_user_id']=$_SESSION['account']['user_id'];
        $add="";
        foreach($search as $k=>$tag){
            if($k>0)$add.=" + ";
            $add.= "(t_tags LIKE '".$tag."')";
            $searchnew['t_tags'][]=$tag;
        }
        $ss = "(".$add.") as hits";

        $collection = qb::_table('transactions');

        $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('t_id')->count('*'));
        return count($count);
    }

    function getByID($id=0){

        $where['t_id'] = $id;
        $where['t_user_id'] = $_SESSION['account']['user_id'];
        $where['t_trash']=0;
        $select = '*';
        $collection = qb::_table('transactions');
        return $this->db->q_($collection->where($where)->select($select));
    }

    function getByIDForView($id=0){

        $where['t_id'] = $id;
        $where['t_user_id'] = $_SESSION['account']['user_id'];
        $where['t_trash']=0;
        $select = '*';
        $collection = qb::_table('transactions');
        $collection->leftjoin('invoices', 'invoices.inv_id', 'transactions.t_inv_id');
        $collection->leftjoin('expenses', 'expenses.exp_id', 'transactions.t_exp_id');
        return $this->db->q_($collection->where($where)->select($select));
    }

    function getTotalPage($where=array(),$page=1,$count=1){
        $where['t_trash']=0;
        //$where['t_exp_id']=0;

        if($page>0)$page--;
        $offset = \intval($page*$count);

        $select = 't_amount_base';
        $collection = qb::_table('transactions');

        $query = 'select SUM(t_amount_base) as total  from ( '.$collection->where($where)->groupby('t_id')->Limit($offset,$count)->select($select).') AS total ';

        return $this->db->q1($query);
    }

    function getTotalPageSearch($where=array(),$page=1,$count=1, $search = array(),$order='tag_hits DESC'){
        $where['t_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);

        $add="";
        foreach($search as $k=>$tag){
            if($k>0)$add.=" + ";
            $add.= "(t_tags LIKE '".$tag."')";
            $searchnew['t_tags'][]=$tag;
        }
        $ss = "(".$add.") as hits";

        $select = '*,'.$ss;
        $collection = qb::_table('transactions');
        $query = 'select SUM(t_amount_base) as total from ( '.$collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('t_id')->Limit($offset,$count)->select($select).') AS total';
        return $this->db->q1($query);
    }

    function getExp($where=array(),$order='t_date ASC'){
        $where['t_user_id']=$_SESSION['account']['user_id'];
        $where['t_inv_id']=0;

        $select = '*';
        $collection = qb::_table('transactions');

        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));
    }
    /***

Payments Methods

*/


    function getPaymentMethodsList($order='paymethod_id ASC') {
		$where['paymethod_lang'] = $_SESSION['account']['user_lang'];
        $select = '*';
        $collection = qb::_table('payments_method');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }
    
    function getPaymentMethod($id=0) {
	    $where['paymethod_id'] = $id;
        $select = '*';
        $collection = qb::_table('payments_method');
        return $this->db->q_($collection->where($where)->select($select));    
    }
    
    
}

?>