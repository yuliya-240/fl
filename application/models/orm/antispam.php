<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class antispam extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('antispam')->insert($add));
        return $this->db->getLastID();      
    }
    
    
    function getLates($email,$obj,$objid){
	    
	    $query = "SELECT * FROM antispam WHERE (as_date BETWEEN DATE_SUB(NOW() , INTERVAL 10 MINUTE) AND NOW()) AND as_user_id='".$_SESSION['account']['user_id']."' AND as_object='".$obj."' AND as_object_id='".$objid."' AND as_email='".$email."' ";
	    
	    return  $this->db->q($query);
    }
}