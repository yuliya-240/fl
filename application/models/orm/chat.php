<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class chat extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('chats')->insert($add));
        return $this->db->getLastID();      
    }

    function update($id=0, $data = array()) {
        $where = array(
            'chat_id' => $id
        );
        
        $this->db->q(qb::_table('chats')->where($where)->update($data));    
        return true;
    }

    function updateMass($where=array(), $data = array()) {

        
        $this->db->q(qb::_table('chats')->where($where)->update($data));    
        return true;
    }
 
	function getList($channel1,$channel2) {

 
        $select = '*';
        $collection = qb::_table('chats');

       //   app::trace($collection->dwhere($dwhere)->dwhere($dwhere2)->OrderBy($order)->Limit($offset,$count)->select($select));
        return $this->db->q("(SELECT * FROM chats  WHERE (chat_from='".$channel1."' AND chat_to='".$channel2."') OR (chat_to='".$channel1."' AND chat_from='".$channel2."') ORDER BY chat_id DESC LIMIT 0, 30) order by chat_id asc");    
    }
 
	function getByID($id=0){
		
		$where['chat_id'] = $id;
        $select = '*';
        $collection = qb::_table('chats');
       
        return  $this->db->q_($collection->where($where)->select($select));    
	}
	
	function getCountUnread(){

        $where = array(
            'chat_to' => $_SESSION['account']['user_pusher_channel'],
            'chat_status' => 0
        );
        
        $r = $this->db->q1(qb::_table('chats')->where($where)->count('*'));    
        if(!$r)$r=0;
        
        return $r;
		
		
	} 
	
	function getAllUnread(){
		
		$where['chat_status']=0;
		$where['chat_to'] = $_SESSION['account']['user_pusher_channel'];
		
        $select = 'chat_from,count(chat_from) as cnt';
        $collection = qb::_table('chats');
       
        return $this->db->q($collection->where($where)->groupby("chat_from")->select($select));    
		
		
	}
    
}    