<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class calendar extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('events')->insert($add));
        return $this->db->getLastID();      
    }
 
    function update($id=0, $data = array()) {
        $where['e_id'] = $id;
        $this->db->q(qb::_table('events')->where($where)->update($data));    
        return true;
    }
    
    function updateMass($where=array(), $data = array()) {
        
        $this->db->q(qb::_table('events')->where($where)->update($data));    
        return true;
    }
    
    function getEvents($where){
	    
	    
	    $where['e_user_id']=$_SESSION['account']['user_id'];
	    $where['e_trash']=0;
        $select = 'e_id as id, DATE_FORMAT(e_date,"%Y-%m-%dT%TZ") as start,  e_title as title,  CAST(e_allday AS UNSIGNED)  as allDay , e_type as type, e_close as close';
        $collection = qb::_table('events');
       
       
       
      // app::trace($this->db->q($collection->where($where)->select($select)));
       
        return $this->db->q($collection->where($where)->select($select));    
	    
	    
    }
    
    function getEventInfo($where=array()){
	    $where['e_trash']=0;
		$select = '*';
        $collection = qb::_table('events');
		$collection->leftjoin('events_recurring', 'events_recurring.er_id', 'events.e_recurring_id');
		$collection->leftjoin('events_types', 'events_types.etype_type', 'events.e_type', 'AND events_types.etype_lang="'.$_SESSION['account']['user_lang'].'"' );
              
        return $this->db->q_($collection->where($where)->select($select));  	    
    }

    function getEvent4Recurring($tsdate,$recid,$userID){
	    
	    $where['DATE(e_date)']=\date("Y-m-d",$tsdate);
	    $where['e_recurring_id']=$recid;
	    $where['e_user_id'] = $userID;
	    
		$select = 'e_id';
        $collection = qb::_table('events');
              
        return $this->db->q1($collection->where($where)->select($select));  	    
    }
	
	function getRecEventsCount($recid,$userid){
		
	    
	    $where['e_user_id']=$userid;
	    $where['e_recurring_id']=$recid;
        $select = '*';
        $collection = qb::_table('events');
              
        return $this->db->q1($collection->where($where)->count($select));    
	    
		
	}

	// Event Types
	
	function getEventsTypes(){
		$where['etype_lang'] = $_SESSION['account']['user_lang'];
		$where['etype_cal'] = 1;

        $select = '*';
        $collection = qb::_table('events_types');
              
        return $this->db->q($collection->where($where)->order("etype_order ASC")->select($select));    
		
		
	}

	// Recurring Calendar
	
	function addRecurring($add){
		
        $this->db->q(qb::_table('events_recurring')->insert($add));
        return $this->db->getLastID();      
		
	}
 
    function updRecurring($id=0, $data = array()) {
        $where['er_id'] = $id;
        $this->db->q(qb::_table('events_recurring')->where($where)->update($data));    
        return true;
    }   

	function fillRecurring($start,$end,$userid){
		
		$where['er_user_id'] = $userid;
		$where['er_trash'] = 0;
		$where['er_date_start<']=$end;
		//$where['er_date_end>']=$start;
        $collection = qb::_table('events_recurring');
        $resList = $this->db->q($collection->where($where)->select('*'));    
	
		$tsend = strtotime($end);
		//$tsday = strtotime($start);
		
		foreach($resList as $k=>$v){
			$go = true;
			$tsday = strtotime($start);
			$startTS = strtotime($start);
			if($v['er_end']==2 && strtotime($v['er_end_date'])>=$tsday) $go=false;
			if($v['er_end']==3 && ($this->getRecEventsCount($v['er_id'],$_SESSION['account']['user_id'])==$v['er_end_count'] ))$go = false;
			
			if($go){
				while($tsday<=$tsend){
					$er_start = strtotime($v['er_date_start']);
					$isRecurring = false;
					if( $tsday>=$er_start){
					if(!$this->getEvent4Recurring($tsday,$v['er_id'],$_SESSION['account']['user_id'])){
						switch($v['er_freq']){
							
							case "d":
							    
								if($v['er_daily_option']){
									
									$dayOfWeek = \date('N',$tsday);
									if($dayOfWeek<6)$isRecurring = true;
									
								}else{
									
									$startTS = \strtotime($v['er_date_start']);
									$days = app::diffDays($startTS,$tsday);
									if($v['er_daily_days_count']>1){
										if(($days % $v['er_daily_days_count'])==0)$isRecurring = true;
									}else{
										
										$isRecurring = true;
									}
									
								}
							break;
							
							case "w":
								
								$w1 = \date("W",\strtotime($v['er_date_start']));
								$w2 = \date("W",$tsday);
			
								$w1Year = \date("Y",\strtotime($v['er_date_start']));
								$w2Year = \date("Y",$tsday);
								
								
								if($w1Year!=$w2Year){
									$dateOld = \strtotime("31 December $w1Year");
									$lastWeek = \date("W", $dateOld);
									if ($lastWeek==53 || $lastWeek==1){
										
										$w2=$w2+1;
									}
									$dW = $w2-$w1;
									
									
									if($dW==0 || ($dW % $v['er_weekly_count'] )==0){
											
										$dayOfWeek = \date('N',$tsday);
										if($v['er_w'.$dayOfWeek])$isRecurring = true;
									}
			
									
								}else{
									$dW = $w2-$w1;
									if($dW==0 || ($dW % $v['er_weekly_count'] )==0){
											
										$dayOfWeek = \date('N',$tsday);
										if($v['er_w'.$dayOfWeek])$isRecurring = true;
									}
									
								}
								
			
			
			
								$dW = $w2-$w1;
									
								if($dW==0 || ($dW % $v['er_weekly_count'] )==0){
										
									$dayOfWeek = \date('N',$tsday);
									if($v['er_w'.$dayOfWeek])$isRecurring = true;
								}
								
							break;
			
							case "m":
								
								$m1 = \date("n",\strtotime($v['er_date_start']));
								$m2 = \date("n",$tsday);
								$diffMonth = $m2-$m1;
								
								$monthDay = \date("j",$tsday);
								
								if($v['er_monthly_option']==1){
									
									if(($diffMonth % $v['er_month_count1'])==0){
										if($v['er_month_day']==$monthDay)$isRecurring = true;
									}
									
								}else{
									
									$month = \date('F',$tsday);
									$year = \date('Y',$tsday);
									if(($diffMonth % $v['er_month_count2'])==0){    	
										$tmpD = \strtotime($v['er_month_day_number'].' '.$v['er_month_week_day'].' of '.$month.' '.$year);
										$tmpDate = \date("Y-m-d",$tmpD);
										$date = date("Y-m-d",$tsday);
										if($tmpDate==$date)$isRecurring = true;
									}    	
									
								}
								
							
							break;
			
							case "y":
								
								if($v['er_yarly_option']==1){
									
									$monthName = \strtolower(\date("M",$tsday));
									if($monthName == $v['er_year_month1']){
										$dayOfMonth = \date("j",$tsday);
										if($dayOfMonth == $v['er_year_day'])$isRecurring = true;
									}
									
								}else{
									
									$year = \date('Y',$tsday);
									$tmpD = \strtotime($v['er_year_day_number'].' '.$v['er_year_week_day'].' of '.$v['er_year_month2'].' '.$year);
									$tmpDate = \date("Y-m-d",$tmpD);
									$date = date("Y-m-d",$tsday);
									if($tmpDate==$date)$isRecurring = true;
									
								}
								
							break;
					
		
						}	
						if($isRecurring)$this->createRecurringEvent($tsday,$v);				
					}
						
					}

					
					$tsday = strtotime("+1 day",$tsday);
					
				}
					
			}
			
		}
		
		return false;
		
	}
	
	function createRecurringEvent($tsdate,$recevent=array()){
		
		$add['e_user_id'] = $recevent['er_user_id'];
		$add['e_client_id'] = $recevent['er_client_id'];
		$add['e_collab_id'] = $recevent['er_collab_id'];
		$add['e_title'] = $recevent['er_title'];
		$add['e_type'] = $recevent['er_type'];
		$add['e_date'] = \date("Y-m-d H:i:s",$tsdate);
		$add['e_location'] = $recevent['er_location'];
		$add['e_recurring_id'] = $recevent['er_id'];
		$add['e_invoice_id'] = $recevent['er_invoice_id'];
		$add['e_send_auto'] = $recevent['er_send_auto'];
		$add['e_send_email'] = $recevent['er_send_email'];
		return $this->add($add);
		
	}
	
	function deleteEvent($where){
		$data['e_trash'] = 1;
		
		return $this->db->q(qb::_table('events')->where($where)->update($data));
	}
	
	function completeEvent($where, $data){
		return $this->db->q(qb::_table('events')->where($where)->update($data));
	}
	
	function editEvent($where, $data){
		
			return $this->db->q(qb::_table('events')->where($where)->update($data));
		
	}

}