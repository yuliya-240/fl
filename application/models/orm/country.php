<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class country extends \floctopus\models\common\model {   
	

	function getTZList(){
		$collection = qb::_table('timezones');
		return $this->db->q($collection->select('*'));	
	}

	function addTZList($arr){
        $this->db->q(qb::_table('timezones')->insert($arr));
        
        $id = $this->db->getLastID();
        
        return $id;
	}

	function getTZ($where=array()){
		
		$collection = qb::_table('timezones');
		return $this->db->q_($collection->where($where)->select("*"));	
	}
	
	function getCurrencyList(){
		$collection = qb::_table('currencies');
		return $this->db->q($collection->select('*'));	
	}

	function getStateList($countryID = 0){
		$select="*";
		
		$where = array(
            'state_country_iso' => $countryID
        );
		$collection = qb::_table('states');
		return $this->db->q($collection->where($where)->select($select));  
	}

	function detectState($state){
		$select="state_code";
		$collection = qb::_table('states');
		//app::trace($collection->where("state_code='".$state."' or state_name='".$state."'")->select($select));
		$r = $this->db->q1($collection->where("state_code='".$state."' or state_name='".$state."'")->select($select));  
		if (!$r) $r=""; 
		
		return $r;
	}
	
	function getCountriesList(){
		$collection = qb::_table('countries');
		return $this->db->q($collection->select('*'));
	}

    function getStateInfo($where = array()){
	    $select = '*';
	    $collection = qb::_table('states');
	    return $this->db->q_($collection->where($where)->select($select)); 
    }
	
	function getProfList(){
		$select="*";
		
		$collection = qb::_table('professions');
		return $this->db->q($collection->orderby('pro_sort asc, pro_name asc')->select($select));  
		
	}
	
	function returnCountry($where=array(), $components = array()) {
        
        $select = '*';
        
        $collection = qb::_table('countries');
        //$collection->leftjoin('iih_contacts', 'ii_accounts.accountID', 'iih_contacts.accountID', 'AND cAccMain = 1');
        
        foreach ($components as $component) {
            switch ($component) {

                default:
                    break;
            }
        }
        
        $account = $this->db->q_($collection->where($where)->select($select));    

        return $account;
    }
	
	function getLangList($where=array()){
		
        $select = '*';
        $collection = qb::_table('languages');
        return $this->db->q($collection->where($where)->select($select));    
		
		
	}
	
	function getLanguageID($lng){
        
        $where['language_culture'] = $lng;
        $select = 'language_id';
        $collection = qb::_table('languages');
        return $this->db->q1($collection->where($where)->select($select));    
		
		
	}

	function getCountryName($isoname=""){
		if(!strlen($isoname))$isoname = $_SESSION['account']['user_country'];
		
		$w['country_iso'] = $isoname;
		$collection = qb::_table('countries');
		return $this->db->q1($collection->where($w)->select('country_printable_name'));
	}

	
	
	
}

?>