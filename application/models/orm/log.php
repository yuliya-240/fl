<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class log extends \floctopus\models\common\model {   
	
    function add($arr=array()){
		$this->db->q(qb::_table('log')->insert($arr));
        return $this->db->getLastID();
	}

    function getListCount($where=array(),$search = array(),$order='log_date DESC') {
    	
    	$where['log_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('log');
        $collection->leftjoin('users', 'users.user_id', 'actions.log_user_id');
        
        
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));      
    }

    function getListCountSearch($where=array(),$search = array()) {
    	
    	$where['log_user_id']=$_SESSION['account']['user_id'];
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(log_details LIKE '".$tag."')";
			$searchnew['log_details'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('log');
       $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('log_id')->count('*'));  
       return count($count);  
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="log_date DESC") {
		
		
    	
    	$where['log_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(log_details LIKE '".$tag."')";
			$searchnew['log_details'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('log');
        $collection->leftjoin('users', 'users.user_id', 'actions.log_user_id');

        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('log_id')->Limit($offset,$count)->select($select));    
    }


    function getList($where=array(),$page=1,$count=1, $search = array(),$order='log_id DESC') {
        
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $where['log_user_id']=$_SESSION['account']['user_id'];
    
        $select = '*';
        $collection = qb::_table('log');
        $collection->leftjoin('users', 'users.user_id', 'log_user_id');

        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }
    
    function getByID($id=0) {
    	$where['log_id'] = $id;
    	$where['log_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('log');
		$collection->leftjoin('users', 'users.user_id', 'log_user_id');

        $log = $this->db->q_($collection->where($where)->select($select)); 
        
         
        return $log;
    }    
    
    function getAllCount($w=array()){
        $select = '*';
        $collection = qb::_table('log');
      
        $w['log_user_id']=$_SESSION['account']['user_id'];
        return $this->db->q1($collection->where($w)->count($select));    
    }

	
}
