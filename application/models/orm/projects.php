<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \floctopus\models\orm\calendar as OrmCalendar;
use \jet\db\qb as qb;

class projects extends \floctopus\models\common\model {
	
    function add($add=array()) {
		
		$add['project_user_id'] = $_SESSION['account']['user_id'];
		
        $this->db->q(qb::_table('projects')->insert($add));
        return $this->db->getLastID();      
    }

    function addDef($add=array()) {
		
        $this->db->q(qb::_table('projects')->insert($add));
        return $this->db->getLastID();      
    }

    function update($id=0, $prjname="",$desc="",$status=1) {
        $where = array(
            'project_id' => $id
        );
        
		$data['project_name'] = $prjname;
		$data['project_desc'] = $desc;
		$data['project_status'] = $status;
        $this->db->q(qb::_table('projects')->where($where)->update($data));    
        return true;
    }

    function updateArr($id=0, $upd=array()) {
        $where = array(
            'project_id' => $id
        );

        $this->db->q(qb::_table('projects')->where($where)->update($upd));    
        return true;
    }
    
    function delete($id=0){
	    
	    $where['project_id']=$id;
        $upd['project_trash'] = 1;
        $collection = qb::_table('projects');
        $this->db->q($collection->where($where)->update($upd));    

	    $wherem['prjm_project_id']=$id;
	    $wherem['prjm_user_id']=$_SESSION['account']['user_id'];
	    $updm['prjm_trash'] = 1;
        $collection = qb::_table('project_milestones');
        $this->db->q($collection->where($wherem)->update($updm));   

	    $wherec['prju_project_id']=$id;
	    $wherec['prju_user_id']=$_SESSION['account']['user_id'];
	    $updc['prju_trash'] = 1;
        $collection = qb::_table('project_collaborators');
        $this->db->q($collection->where($wherec)->update($updc));  
	    
	    return false;
    }

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='project_id DESC') {
	   // $where['project_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('projects');
        $collection->leftjoin('users', 'users.user_id', 'projects.project_user_id');
        $collection->leftjoin('project_status', 'project_status.prjstatus_status', 'projects.project_status'," and project_status.prjstatus_lang='".$_SESSION['account']['user_lang']."'");
        
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getListCount($where=array(),$search = array()) {
	
    	//$where['project_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('projects');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }
    
    function getDefaultProject(){
	    
    	$where['project_default']=1;
    	$where['project_user_id']=$_SESSION['account']['user_id'];
    	
        $collection = qb::_table('projects');
        return $this->db->q1($collection->where($where)->select('project_id'));    
	    
    }

    function del($id=0){
    	$where['project_id']=$id;

        $collection = qb::_table('projects');
        return $this->db->q($collection->where($where)->delete());    
    }

	function getByID($id=0){

    	$where['project_id']=$id;
        $collection = qb::_table('projects');
        $collection->leftjoin('users', 'users.user_id', 'projects.project_user_id');
        $collection->leftjoin('project_status', 'project_status.prjstatus_status', 'projects.project_status'," and project_status.prjstatus_lang='".$_SESSION['account']['user_lang']."'");
     
    

        return $this->db->q_($collection->where($where)->select('*'));    
	}


	function getByID4View($id=0){

    	$where['project_id']=$id;
        $collection = qb::_table('projects');
        
        $collection->leftjoin('project_status', 'project_status.prjstatus_status', 'projects.project_status'," and project_status.prjstatus_lang='".$_SESSION['account']['user_lang']."'");
        $collection->leftjoin('clients', 'clients.client_id', 'projects.project_client_id');
        $collection->leftjoin('users', 'users.user_id', 'projects.project_collab_id');

        return $this->db->q_($collection->where($where)->select('*'));    
	}

		
    function getPrjByUserID(){
	    
	    $prjids = array();
	    
	    $selPrj['project_user_id']=$_SESSION['account']['user_id'];
	    $collection = qb::_table('projects');
	     $select = 'project_id';
	    $rec0 = $this->db->q($collection->where($selPrj)->select($select)); 
        if(is_array($rec0)){
	        
	        foreach($rec0 as $v){
		        $prjids[] = array_shift($v);
	        }
        }
	    
	    
	    
	    $where['prju_collaborator_id']=$_SESSION['account']['user_id'];
	    //$dwhere['prju_user_id'] = $_SESSION['account']['user_id'];
        $select = 'prju_project_id';
        $collection = qb::_table('project_collaborators');
       // app::trace($collection->dwhere($dwhere)->groupby('prju_project_id')->select($select));
        $rec = $this->db->qa($collection->where($where)->groupby('prju_project_id')->select($select));    
        
        if(is_array($rec)){
	        
	        foreach($rec as $v){
		        $prjids[] = array_shift($v);
	        }
        }
        
        return $prjids;        
    }

    function getAllList($where=array(),$order='project_name ASC') {
        $select = '*';
        $collection = qb::_table('projects');
        
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }	
	
	// Milestones	
	
	function addMilestone($addm=array(),$id){
		
		$addm['prjm_user_id'] = $_SESSION['account']['user_id'];
		$addm['prjm_project_id'] = $id;
		
        $this->db->q(qb::_table('project_milestones')->insert($addm));
        return $this->db->getLastID();      
	}

	function getMilestones($id=0){
    
    	$where['prjm_project_id']=$id;
    	$where['prjm_trash']=0;
    	
        $select = '*';
        $collection = qb::_table('project_milestones');
        return $this->db->q($collection->where($where)->select('*'));    
	}

	function getMilestonesArrayIDS($id=0){
    
    	$where['prjm_project_id']=$id;
    	$where['prjm_trash']=0;
       
        $collection = qb::_table('project_milestones');
        return $this->db->qa($collection->where($where)->select('group_concat(prjm_id)'));    
	}
	
    function delMilestone($id=0){
	    
	    $arrids = $this->getMilestonesArrayIDS($id);
	    
	    if($arrids){
			// dlete all milestones 
		    $a = explode(",",$arrids[0][0]); 
		    $where['prjm_id'] = $a;
		    $where['prjm_user_id'] = $_SESSION['account']['user_id'];
		    $updPrjm['prjm_trash']=1;
	        $this->db->q(qb::_table('project_milestones')->where($where)->update($updPrjm));  
			
			// delete Milestone from  Calendar 
			$cal = new OrmCalendar();
			$selEvents['e_type'] = 2;
			$selEvents['e_object_id'] = $a;
			$selEvents['e_user_id'] = $_SESSION['account']['user_id'];
			$upd['e_trash'] =1;
			$cal->updateMass($selEvents,$upd);
			
			//Remove Milestons from Issues
			
			$selIssue['issue_user_id']= $_SESSION['account']['user_id'];
			$selIssue['issue_milestone_id']= $a;
			$selIssue['issue_project_id'] = $id;
			$updIssue['issue_milestone_id']=0;
			$this->db->q(qb::_table('issues')->where($selIssue)->update($updIssue)); 
	    }
		
        return false;   
	    
    }
	
	//Collaborators
	
	function getCollabs($id=0){
    
    	$where['prju_project_id']=$id;
    	$where['prju_trash'] = 0;
    	//$where['prju_user_id'] = $owner;
    	
        $select = '*';
        $collection = qb::_table('project_collaborators');
        $collection->leftjoin('users', 'users.user_id', 'project_collaborators.prju_collaborator_id');
        $r = $this->db->q($collection->where($where)->select('*'));    
        return $r;
        
	}

	function getCollabsForIssues($id=0,$uid=0){
    
    	$where['prju_project_id']=$id;
    	$where['prju_collaborator_id!']=$uid;
    	$where['prju_trash'] = 0;
    	
        $select = '*';
        $collection = qb::_table('project_collaborators');
        $collection->leftjoin('users', 'users.user_id', 'project_collaborators.prju_collaborator_id');
        $r = $this->db->q($collection->where($where)->select('*'));    
        return $r;
        
	}


	function addCollab($cid=0,$id=0,$role=20){
		
		$addm['prju_user_id'] = $_SESSION['account']['user_id'];
		$addm['prju_project_id'] = $id;
		$addm['prju_collaborator_id'] = $cid;
		$addm['prju_role'] = $role;
        $this->db->q(qb::_table('project_collaborators')->insert($addm));
        return $this->db->getLastID();      
	}

    function getCollabIDS(){
	    $collab = array();
	    $where['prju_collaborator_id']=$_SESSION['account']['user_id'];
        $select = 'prju_user_id';
        $collection = qb::_table('project_collaborators');
        $rec = $this->db->qa($collection->where($where)->select($select));    
        
        if(is_array($rec)){
	        
	        foreach($rec as $v){
		        $collab[] = array_shift($v);
	        }
        }
        
        return $collab;        
    }

    function getCollabsForUpdateIDS($id=0,$owner=0){
	    $collab = array();
	
		$where['prju_project_id'] = $id;
		$where['prju_collaborator_id!'] = $owner;
		$where['prju_trash'] = 0;
        $collection = qb::_table('project_collaborators');
        $rec = $this->db->qa($collection->where($where)->select('prju_collaborator_id'));    
        
        if(is_array($rec)){
	        
	        foreach($rec as $v){
		        $collab[] = array_shift($v);
	        }
        }
        
        return $collab;        
    }

    function getAllAvailProjectsIDS(){
	    $collab = array();
	    $where['prju_collaborator_id']=$_SESSION['account']['user_id'];
		$where['projects.project_trash'] = 0;
		$where['prju_trash'] = 0;
        $select = 'prju_project_id';
        $collection = qb::_table('project_collaborators');
        $collection->rightjoin('projects', 'projects.project_id', 'prju_project_id');
        $rec = $this->db->qa($collection->where($where)->select($select));    
        
        if(is_array($rec)){
	        
	        foreach($rec as $v){
		        $collab[] = array_shift($v);
		        
	        }
        }
        
 	    $where2['project_user_id']=$_SESSION['account']['user_id'];
		$where2['project_trash'] = 0;
		
        $select2 = 'project_id';
        $collection2 = qb::_table('projects');
        $rec2 = $this->db->qa($collection2->where($where2)->select($select2));    
       
        if(is_array($rec2)){
	        
	        foreach($rec2 as $v){
		        $collab[] = array_shift($v);
		        
	        }
        }
        
        $collab[]=$_SESSION['DEFPRJ'];
        
        return $collab;        
    }

    function getAllAvailProjects(){
	    $collab = array();
	    $where['prju_collaborator_id']=$_SESSION['account']['user_id'];
		$where['projects.project_trash'] = 0;
		$where['prju_trash'] = 0;
        $select = 'prju_project_id';
        $collection = qb::_table('project_collaborators');
        $collection->rightjoin('projects', 'projects.project_id', 'prju_project_id');
        $rec = $this->db->qa($collection->where($where)->select($select));    
        
       // app::trace($rec);
        
        if(!empty($rec)){
	        
	        foreach($rec as $v){
		        $collab[] = array_shift($v);
		        
	        }
        }
        
 	    $where2['project_user_id']=$_SESSION['account']['user_id'];
		$where2['project_trash'] = 0;
		$where2['project_default'] = 0;
		
        $select2 = 'project_id';
        $collection2 = qb::_table('projects');
        $rec2 = $this->db->qa($collection2->where($where2)->select($select2));    
	
       
        if(!empty($rec2)){
	        
	        foreach($rec2 as $v){
		        $collab[] = array_shift($v);
		        
	        }
        }
        
        
        if(!empty($collab)){
	        $selPrj['project_id']= $collab;
	        $collection3 = qb::_table('projects');
	       	
	        return $this->db->q($collection3->where($selPrj)->select("*"));
        }
      
         return false;
             
    }

    function getCollabSelIDS($id){
	    $collab = array();
	    $where['prju_project_id']=$id;
        $select = 'prju_collaborator_id';
        $collection = qb::_table('project_collaborators');
        $rec = $this->db->qa($collection->where($where)->select($select));    
        
        if(is_array($rec)){
	        
	        foreach($rec as $v){
		        $collab[] = array_shift($v);
		        
	        }
        }
        
        return $collab;        
	    
    }

    function delCollab($id=0){
	    
	    $where['prju_project_id'] = $id;
	    
        $collection = qb::_table('project_collaborators');
        $this->db->q($collection->where($where)->delete()); 
        return false;   
	    
    }

    function removeAllCollabs($id=0,$owner=0){
	    
	    $where['prju_project_id'] = $id;
	    $where['prju_collaborator_id!']=$owner;
        $collection = qb::_table('project_collaborators');
        $this->db->q($collection->where($where)->delete()); 
        return false;   
	    
    }

	function getAllStatuses(){
		
    	$where['prjstatus_lang']=$_SESSION['account']['user_lang'];
    	
        $select = '*';
        $collection = qb::_table('project_status');
        return $this->db->q($collection->where($where)->select('*'));    
		
		
	}

	function getCollabArr($prjid=0){
    	$c = array();
    	$where['prju_project_id']=$prjid;
        $collection = qb::_table('project_collaborators');
        $collabs = $this->db->q($collection->where($where)->select('*'));    
		
		foreach($collabs as $v){
			$c[]=$v['prju_collaborator_id'];
		}
		
		return $c;
	}

	function getRole($prjid=0,$userid=0){
		
    	$where['prju_project_id']=$prjid;
    	$where['prju_collaborator_id']=$userid;
    	$where['prju_trash'] = 0;
		
        $collection = qb::_table('project_collaborators');
        $r = $this->db->q1($collection->where($where)->select('prju_role'));    
        
        return $r;
	}
	//Status
	
	function getStatusDetails($status=0){
		
		
    	$where['prjstatus_lang']=$_SESSION['account']['user_lang'];
    	$where['prjstatus_status'] = $status;
    	
        $select = '*';
        $collection = qb::_table('project_status');
        return $this->db->q_($collection->where($where)->select('*'));    
		
		
	}
}
