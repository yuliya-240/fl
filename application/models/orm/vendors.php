<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class vendors extends \floctopus\models\common\model {

	function add($srvArr=array()){
		$this->db->q(qb::_table('vendors')->insert($srvArr));
        return $this->db->getLastID();
	}

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order='hits DESC') {
		$where['vendor_trash']=0;
        $where['vendor_user_id'] = $_SESSION['account']['user_id'];

        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(vendor_name LIKE '".$tag."')";
			$searchnew['vendor_name'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('vendors');
        
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('vendor_id')->Limit($offset,$count)->select($select));    
    }


}