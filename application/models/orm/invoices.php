<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class invoices extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('invoices')->insert($add));
        return $this->db->getLastID();      
    }

    function update($id=0, $data = array()) {
        $where = array(
            'inv_id' => $id
        );
        
        $this->db->q(qb::_table('invoices')->where($where)->update($data));    
        return true;
    }

    function updateMass($where=array(), $data = array()) {

        
        $this->db->q(qb::_table('invoices')->where($where)->update($data));    
        return true;
    }

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='inv_date ASC') {
	    $where['inv_user_id']=$_SESSION['account']['user_id'];
	    $where['inv_trash']=0;
	  //  $where['invoices_statuses.invstatus_lang'] = $_SESSION['account']['user_lang'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('invoices');
        $collection->leftjoin('invoices_statuses', 'invoices_statuses.invstatus_status', 'invoices.inv_status', " and invstatus_lang='".$_SESSION['account']['user_lang']."'");       
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getAllList($where=array(),$order='inv_date ASC') {
	    $where['inv_user_id']=$_SESSION['account']['user_id'];
	    $where['inv_trash']=0;
	  
        $select = '*';
        $collection = qb::_table('invoices');
              
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }


    function getListCount($where=array(),$search = array()) {
	    $where['inv_user_id']=$_SESSION['account']['user_id'];
	    $where['inv_trash']=0;
        $select = '*';
        $collection = qb::_table('invoices');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="inv_date ASC") {
		
    	$where['inv_trash']=0;
    	$where['inv_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(inv_tags LIKE '".$tag."')";
			$searchnew['inv_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('invoices');
       // $collection->leftjoin('countries', 'countries.country_iso', 'clients.inv_country');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('inv_id')->Limit($offset,$count)->select($select));    
    }

    function getListCountSearch($where=array(),$search = array()) {
    	$where['inv_trash']=0;
    	$where['inv_user_id']=$_SESSION['account']['user_id'];
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(inv_tags LIKE '".$tag."')";
			$searchnew['inv_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('invoices');

       $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('inv_id')->count('*'));  
       return count($count);  
    }
	
	function getByID($id=0){
		
		$where['inv_id'] = $id;
	    $where['inv_user_id'] = $_SESSION['account']['user_id'];
	    $where['inv_trash']=0;
        $select = '*';
        $collection = qb::_table('invoices');
        //$collection->leftjoin('countries', 'countries.country_iso', 'clients.inv_country');
        return $this->db->q_($collection->where($where)->select($select));    
	}

	function getCronByID($id=0,$userid=0){
		
		$where['inv_id'] = $id;
	    $where['inv_user_id'] = $userid;
	    $where['inv_trash']=0;
        $select = '*';
        $collection = qb::_table('invoices');
        //$collection->leftjoin('countries', 'countries.country_iso', 'clients.inv_country');
        return $this->db->q_($collection->where($where)->select($select));    
	}
	
	function getLastNum(){
		
		$letters="abcdefghijklmopqrstuvwyz";
		
		$where['inv_user_id'] = $_SESSION['account']['user_id'];
		$where['inv_trash'] = 0;
		$collection = qb::_table('invoices');
		$lastnum = $this->db->q1($collection->where($where)->OrderBy("inv_num ASC")->select("MAX(inv_num)"));
		
		//app::trace($lastnum);
		
		if(!$lastnum){
			return $_SESSION['account']['set_inv_number_start'];
		}else{
			
			$newnum = ++$lastnum;			
			return $newnum;
		}
		
		
		
	}

    function getTotalPage($where=array(),$page=1,$count=1){
		$where['inv_trash']=0;	    
        if($page>0)$page--;
        $offset = \intval($page*$count);

        $select = 'inv_total_base';
        $collection = qb::_table('invoices');
        
        $query = 'select SUM(inv_total_base) as total  from ( '.$collection->where($where)->groupby('inv_id')->Limit($offset,$count)->select($select).') AS total ';
        
        return $this->db->q1($query);    
    }

    function getTotalPageSearch($where=array(),$page=1,$count=1, $search = array(),$order='tag_hits DESC'){
		$where['inv_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(inv_tags LIKE '".$tag."')";
			$searchnew['inv_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('invoices');
        //$collection->leftjoin('customs', 'customs.custom_id', 'invoices.inv_customer_id');
        //$collection->leftjoin('invoices_status', 'invoices_status.invstatus_id', 'invoices.inv_status');
 $query = 'select SUM(inv_total_base) as total from ( '.$collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('inv_id')->Limit($offset,$count)->select($select).') AS total';       
         return $this->db->q1($query);
    }

    function getInvAmount($where=array()){
	    $where['inv_trash']=0;
        $select = 'sum(inv_total_due_base)';
        $collection = qb::_table('invoices');
        $r =  $this->db->q1($collection->where($where)->select($select));    
		if(!$r)$r=0;
		
		return $r;
    }
	
	function getInvPaidAmount($where=array()){
		
	    $where['inv_trash']=0;
        $select = 'sum(inv_paid_amount_base)';
        $collection = qb::_table('invoices');
        $r =  $this->db->q1($collection->where($where)->select($select));    
		if(!$r)$r=0;
		
		return $r;
		
	}
	
	// LInes
	
	function addLine($add) {
	
        $this->db->q(qb::_table('invoices_lines')->insert($add));
        return $this->db->getLastID();      
    }

    function getLinesList($where=array(),$order='invline_id ASC') {

        $select = '*';
        $collection = qb::_table('invoices_lines');
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

	function delLines($invid=0){
		 $where['invline_user_id'] = $_SESSION['account']['user_id'];
		 $where['invline_inv_id'] = $invid;
		 $collection = qb::_table('invoices_lines');
		 return $this->db->q($collection->where($where)->delete());   
	}
	
	function getAllStatuses(){
		$where['invstatus_lang']=$_SESSION['account']['user_lang'];
     
        $collection = qb::_table('invoices_statuses');
              
        return $this->db->q($collection->where($where)->OrderBy("invstatus_status")->select("*"));    
		
		
	}
	
	// HISTORY
	
	function addHistory($add=array()){
		
        $this->db->q(qb::_table('invoice_history')->insert($add));
        return $this->db->getLastID();      
		
		
	}

    function getHistoryList($where=array(), $order='invhist_id DESC') {
	    $where['invhist_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('invoice_history');
       $collection->leftjoin('invoice_actions', 'invoice_history.invhist_action', 'invoice_actions.invact_action', " and invact_lang='".$_SESSION['account']['user_lang']."'");          
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }


	// ATTACH
	
	function addAttach($add=array()){
		
        $this->db->q(qb::_table('invoices_attach')->insert($add));
        return $this->db->getLastID();      
	}

    function getAttachList($invid = 0, $order='invatt_id ASC') {
	    $where['invatt_user_id']=$_SESSION['account']['user_id'];
	    $where['invatt_trash']=0;
	    $where['invatt_inv_id'] = $invid;
        $select = '*';
        $collection = qb::_table('invoices_attach');
               
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

	function delAttach($invid=0){
		 $where['invatt_user_id'] = $_SESSION['account']['user_id'];
		 $where['invatt_inv_id'] = $invid;
		 $collection = qb::_table('invoices_attach');
		 return $this->db->q($collection->where($where)->delete());   
	}

	
}