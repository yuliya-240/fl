<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class expenses extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('expenses')->insert($add));
        return $this->db->getLastID();

    }

    function update($id=0, $data = array()) {
        $where = array(
            'exp_id' => $id
        );
        
        $this->db->q(qb::_table('expenses')->where($where)->update($data));    
        return true;
    }

    function updateMass($where=array(), $data = array()) {

        
        $this->db->q(qb::_table('expenses')->where($where)->update($data));    
        return true;
    }

    function getList($where=array(),$dwhere=array(),$page=1,$count=1, $search = array(),$order='exp_date ASC') {
	    $where['exp_user_id']=$_SESSION['account']['user_id'];
	    $where['exp_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = 'e1.expcat_name as catname,e2.expcat_name as parentname,e3.paymethod_name as paymethod_name,expenses.*';
        $collection = qb::_table('expenses');
        $collection->leftjoin('expenses_category as e1', 'e1.expcat_id', 'expenses.exp_category');
        $collection->leftjoin('expenses_category as e2', 'e2.expcat_id', 'expenses.exp_category_parent');
        $collection->leftjoin('payments_method as e3', 'e3.paymethod_method', 'expenses.exp_method', " and paymethod_lang='".$_SESSION['account']['user_lang']."'");

        if(empty($dwhere)){

        	return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));
        }else{
	        
	       return $this->db->q($collection->dwhere($dwhere)->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select)); 
        }
    }

    function getAllList($where=array(),$order='exp_date ASC') {
	    $where['exp_user_id']=$_SESSION['account']['user_id'];
	    $where['exp_trash']=0;
        $select = '*';
        $collection = qb::_table('expenses');
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

    function getListCount($where=array(),$dwhere=array(),$search = array()) {
	    $where['exp_user_id']=$_SESSION['account']['user_id'];
	    $where['exp_trash']=0;
        $select = '*';
        $collection = qb::_table('expenses');
        
      //  app::trace($dwhere);
        if(empty($dwhere)){
	        return $this->db->q1($collection->where($where)->Search($search)->count('*')); 
        }else{
	    	//app::trace($collection->dwhere($dwhere)->where($where)->Search($search)->count('*'));    
	         return $this->db->q1($collection->dwhere($dwhere)->where($where)->Search($search)->count('*')); 
        }
           
    }

    function getListSearch($where=array(),$dwhere=array(),$page=1,$count=1, $search = array(),$order="exp_name ASC") {
		
    	$where['exp_trash']=0;
    	$where['exp_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(exp_desc LIKE '".$tag."')";
			$searchnew['exp_desc'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('expenses');
        if(empty($dwhere)){
		
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('exp_id')->Limit($offset,$count)->select($select));    
        }else{
	        
	        return $this->db->q($collection->dwhere($dwhere)->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('exp_id')->Limit($offset,$count)->select($select));    

        }
    }

    function getListCountSearch($where=array(),$dwhere=array(),$search = array()) {
    	$where['exp_trash']=0;
    	$where['exp_user_id']=$_SESSION['account']['user_id'];
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(exp_desc LIKE '".$tag."')";
			$searchnew['exp_desc'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('expenses');
		if(empty($dwhere)){
		 $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('exp_id')->count('*'));  
       }else{
	       
	        $count =  $this->db->q($collection->dwhere($dwhere)->where($where)->Search($searchnew)->GroupBy('exp_id')->count('*'));
       }
       return count($count);  
    }
	
	function getByID($id=0){
		
		$where['exp_id'] = $id;
	    $where['exp_user_id'] = $_SESSION['account']['user_id'];
        $select = 'e1.expcat_name as catname,e2.expcat_name as parentname,e3.paymethod_name as paymethod_name,expenses.*';
        $collection = qb::_table('expenses');
        $collection->leftjoin('expenses_category as e1', 'e1.expcat_id', 'expenses.exp_category');
        $collection->leftjoin('expenses_category as e2', 'e2.expcat_id', 'expenses.exp_category_parent');
        $collection->leftjoin('payments_method as e3', 'e3.paymethod_method', 'expenses.exp_method', " and paymethod_lang='".$_SESSION['account']['user_lang']."'");

        return $this->db->q_($collection->where($where)->select($select));


    }

	function getCronByID($id=0,$userid=0){
		
		$where['exp_id'] = $id;
	    $where['exp_user_id'] = $userid;
        $select = '*';
        $collection = qb::_table('expenses');
        return $this->db->q_($collection->where($where)->select($select));    
	}
	
    function getTotalPage($where=array(),$dwhere=array(),$page=1,$count=1){
		$where['exp_trash']=0;	    
        if($page>0)$page--;
        $offset = \intval($page*$count);

        $select = 'exp_amount_base';
        $collection = qb::_table('expenses');
         if(empty($dwhere)){
        $query = 'select SUM(exp_amount_base) as total  from ( '.$collection->where($where)->groupby('exp_id')->Limit($offset,$count)->select($select).') AS total ';
        }else{
	         $query = 'select SUM(exp_amount_base) as total  from ( '.$collection->dwhere($dwhere)->where($where)->groupby('exp_id')->Limit($offset,$count)->select($select).') AS total ';
	        
        }
        
        return $this->db->q1($query);    
    }

    function getTotalPageSearch($where=array(),$dwhere=array(),$page=1,$count=1, $search = array(),$order='tag_hits DESC'){
		$where['exp_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(exp_desc LIKE '".$tag."')";
			$searchnew['exp_desc'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('expenses');
        //$collection->leftjoin('customs', 'customs.custom_id', 'invoices.inv_customer_id');
        //$collection->leftjoin('invoices_status', 'invoices_status.invstatus_id', 'invoices.inv_status');
        if(empty($dwhere)){
	        $query = 'select SUM(exp_amount_base) as total from ( '.$collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('exp_id')->Limit($offset,$count)->select($select).') AS total';
	        }else{
		        
	        }$query = 'select SUM(exp_amount_base) as total from ( '.$collection->dwhere($dwhere)->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('exp_id')->Limit($offset,$count)->select($select).') AS total';
		       
         return $this->db->q1($query);
    }


	// Receipt Lines

    function addLine($add) {
	
        $this->db->q(qb::_table('expenses_lines')->insert($add));
        return $this->db->getLastID();

    }


    function getLineList($expid = 0,$order='el_id ASC') {
	    $where['el_user_id']=$_SESSION['account']['user_id'];
	    $where['el_trash']=0;
	    $where['el_expense_id']=$expid;
        $select = '*';
        $collection = qb::_table('expenses_lines');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }
    
    function delOldLines($id = 0){
	    
        $where = array(
            'el_expense_id' => $id,
            'el_user_id' => $_SESSION['account']['user_id']
        );
        
        $data['el_trash'] = 1;
        
        $this->db->q(qb::_table('expenses_lines')->where($where)->update($data));    
        return true;
	    
    }

	
}
