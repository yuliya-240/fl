<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;


class notes extends \floctopus\models\common\model
{

    function add($add)
    {
        $this->db->q(qb::_table('notes')->insert($add));
        return $this->db->getLastID();
    }

    function update($id = 0, $data = array())
    {
        $w['note_id'] = $id;
        $this->db->q(qb::_table('notes')->where($w)->update($data));
        return true;
    }

    function updateMass($id=0, $data = array()) {
        $where = array(
            'note_id' => $id
        );

        $this->db->q(qb::_table('notes')->where($where)->update($data));
        return true;
    }

    function delete($id = 0)
    {
        $cArr['note_trash'] = 1;
        $this->update($id, $cArr);
    }

    function getList($where = array(), $page = 1, $count = 1, $search = array(), $order = 'note_id DESC')
    {

        if ($page > 0) $page--;
        $offset = \intval($page * $count);
        $select = '*';
        $collection = qb::_table('notes');
        $collection->leftjoin('users', 'users.user_id', 'notes.note_user_id');

        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset, $count)->select($select));
    }

    function getListCount($where = array(), $search = array())
    {

        //$where['project_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('notes');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));
    }

    function getListSearch($where = array(), $page = 1, $count = 1, $search = array(), $order = "note_text ASC")
    {

        $where['note_trash'] = 0;
        $where['note_user_id'] = $_SESSION['account']['user_id'];
        if ($page > 0) $page--;
        $offset = \intval($page * $count);

        $add = "";
        foreach ($search as $k => $tag) {
            if ($k > 0) $add .= " + ";
            $add .= "(note_tags LIKE '" . $tag . "')";
            $searchnew['note_tags'][] = $tag;
        }
        $ss = "(" . $add . ") as hits";

        $select = '*,' . $ss;
        $collection = qb::_table('notes');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('note_id')->Limit($offset, $count)->select($select));
    }

    function getListCountSearch($where = array(), $search = array())
    {
        $where['note_trash'] = 0;
        $where['note_user_id'] = $_SESSION['account']['user_id'];
        $add = "";
        foreach ($search as $k => $tag) {
            if ($k > 0) $add .= " + ";
            $add .= "(note_tags LIKE '" . $tag . "')";
            $searchnew['note_tags'][] = $tag;
        }
        $ss = "(" . $add . ") as hits";

        $collection = qb::_table('notes');

        $count = $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('note_id')->count('*'));
        return count($count);
    }


    function getByID($id = 0)
    {
        $where['note_id'] = $id;
        $select = '*';
        $collection = qb::_table('notes');
        return $this->db->q_($collection->where($where)->select($select));
    }
}
