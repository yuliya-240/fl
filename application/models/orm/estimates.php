<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class estimates extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('estimates')->insert($add));
        return $this->db->getLastID();      
    }

    function update($id=0, $data = array()) {
        $where = array(
            'est_id' => $id
        );
        
        $this->db->q(qb::_table('estimates')->where($where)->update($data));    
        return true;
    }

    function updateMass($where=array(), $data = array()) {

        
        $this->db->q(qb::_table('estimates')->where($where)->update($data));    
        return true;
    }

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='est_date ASC') {
	    $where['est_user_id']=$_SESSION['account']['user_id'];
	    $where['est_trash']=0;
	  //  $where['estimates_statuses.invstatus_lang'] = $_SESSION['account']['user_lang'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('estimates');
        $collection->leftjoin('estimates_statuses', 'estimates_statuses.eststatus_status', 'estimates.est_status', " and eststatus_lang='".$_SESSION['account']['user_lang']."'");       
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getAllList($where=array(),$order='est_date ASC') {
	    $where['est_user_id']=$_SESSION['account']['user_id'];
	    $where['est_trash']=0;
	  
        $select = '*';
        $collection = qb::_table('estimates');
              
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }


    function getListCount($where=array(),$search = array()) {
	    $where['est_user_id']=$_SESSION['account']['user_id'];
	    $where['est_trash']=0;
        $select = '*';
        $collection = qb::_table('estimates');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="est_date ASC") {
		
    	$where['est_trash']=0;
    	$where['est_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(est_tags LIKE '".$tag."')";
			$searchnew['est_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('estimates');
       // $collection->leftjoin('countries', 'countries.country_iso', 'clients.est_country');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('est_id')->Limit($offset,$count)->select($select));    
    }

    function getListCountSearch($where=array(),$search = array()) {
    	$where['est_trash']=0;
    	$where['est_user_id']=$_SESSION['account']['user_id'];
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(est_tags LIKE '".$tag."')";
			$searchnew['est_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('estimates');

       $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('est_id')->count('*'));  
       return count($count);  
    }
	
	function getByID($id=0){
		
		$where['est_id'] = $id;
	    $where['est_user_id'] = $_SESSION['account']['user_id'];
	    $where['est_trash']=0;
        $select = '*';
        $collection = qb::_table('estimates');
        //$collection->leftjoin('countries', 'countries.country_iso', 'clients.est_country');
        return $this->db->q_($collection->where($where)->select($select));    
	}

	function getCronByID($id=0,$userid=0){
		
		$where['est_id'] = $id;
	    $where['est_user_id'] = $userid;
	    $where['est_trash']=0;
        $select = '*';
        $collection = qb::_table('estimates');
        //$collection->leftjoin('countries', 'countries.country_iso', 'clients.est_country');
        return $this->db->q_($collection->where($where)->select($select));    
	}
	
	function getLastNum(){
		
		$letters="abcdefghijklmopqrstuvwyz";
		
		$where['est_user_id'] = $_SESSION['account']['user_id'];
		$where['est_trash'] = 0;
		$collection = qb::_table('estimates');
		$lastnum = $this->db->q1($collection->where($where)->OrderBy("est_num ASC")->select("MAX(est_num)"));
		
		//app::trace($lastnum);
		
		if(!$lastnum){
			return $_SESSION['account']['set_est_number_start'];
		}else{
			
			$newnum = ++$lastnum;			
			return $newnum;
		}
		
		
		
	}

    function getTotalPage($where=array(),$page=1,$count=1){
		$where['est_trash']=0;	    
        if($page>0)$page--;
        $offset = \intval($page*$count);

        $select = 'est_total_base';
        $collection = qb::_table('estimates');
        
        $query = 'select SUM(est_total_base) as total  from ( '.$collection->where($where)->groupby('est_id')->Limit($offset,$count)->select($select).') AS total ';
        
        return $this->db->q1($query);    
    }

    function getTotalPageSearch($where=array(),$page=1,$count=1, $search = array(),$order='tag_hits DESC'){
		$where['est_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(est_tags LIKE '".$tag."')";
			$searchnew['est_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('estimates');
        //$collection->leftjoin('customs', 'customs.custom_id', 'invoices.est_customer_id');
        //$collection->leftjoin('estimates_status', 'estimates_status.invstatus_id', 'invoices.est_status');
 $query = 'select SUM(est_total_base) as total from ( '.$collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('est_id')->Limit($offset,$count)->select($select).') AS total';       
         return $this->db->q1($query);
    }

	
	// LInes
	
	function addLine($add) {
	
        $this->db->q(qb::_table('estimates_lines')->insert($add));
        return $this->db->getLastID();      
    }

    function getLinesList($where=array(),$order='estline_id ASC') {

        $select = '*';
        $collection = qb::_table('estimates_lines');
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

	function delLines($estid=0){
		 $where['estline_user_id'] = $_SESSION['account']['user_id'];
		 $where['estline_est_id'] = $estid;
		 $collection = qb::_table('estimates_lines');
		 return $this->db->q($collection->where($where)->delete());   
	}
	
	function getAllStatuses(){
		$where['eststatus_lang']=$_SESSION['account']['user_lang'];
     
        $collection = qb::_table('estimates_statuses');
              
        return $this->db->q($collection->where($where)->OrderBy("eststatus_status")->select("*"));    
		
		
	}
	
	// HISTORY
	
	function addHistory($add=array()){
		
        $this->db->q(qb::_table('estimate_history')->insert($add));
        return $this->db->getLastID();      
		
		
	}

    function getHistoryList($where=array(), $order='esthist_id DESC') {
	    $where['esthist_user_id']=$_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('estimate_history');
       $collection->leftjoin('estimate_actions', 'estimate_history.esthist_action', 'estimate_actions.estact_action', " and estact_lang='".$_SESSION['account']['user_lang']."'");          
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }


	// ATTACH
	
	function addAttach($add=array()){
		
        $this->db->q(qb::_table('estimates_attach')->insert($add));
        return $this->db->getLastID();      
	}

    function getAttachList($estid = 0, $order='estatt_id ASC') {
	    $where['estatt_user_id']=$_SESSION['account']['user_id'];
	    $where['estatt_trash']=0;
	    $where['estatt_est_id'] = $estid;
        $select = '*';
        $collection = qb::_table('estimates_attach');
               
       
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }

	function delAttach($estid=0){
		 $where['estatt_user_id'] = $_SESSION['account']['user_id'];
		 $where['estatt_est_id'] = $estid;
		 $collection = qb::_table('estimates_attach');
		 return $this->db->q($collection->where($where)->delete());   
	}

	
}