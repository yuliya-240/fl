<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class cron extends \floctopus\models\common\model {
	
    function addEmail($add) {
	
        $this->db->q(qb::_table('cronemail')->insert($add));
        return $this->db->getLastID();      
    }

    function updateEmail($id=0, $data = array()) {
        $where = array(
            'cronemail_id' => $id
        );
        
        $this->db->q(qb::_table('cronemail')->where($where)->update($data));    
        return true;
    }    
 
     function getList($where=array(),$page=0,$count=1, $order='cronemail_id ASC') {
	   
	    $where['cronemail_status']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('cronemail');
            
        return $this->db->q($collection->where($where)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    
}