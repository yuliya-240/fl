<?php
namespace floctopus\models\orm;

use \floctopus\application as app;
use \jet\db\qb as qb;

class taxes extends \floctopus\models\common\model {
	
    function add($add) {
	
        $this->db->q(qb::_table('taxes')->insert($add));
        return $this->db->getLastID();      
    }

    function update($id=0, $data = array()) {
        $where = array(
            'tax_id' => $id
        );
        
        $this->db->q(qb::_table('taxes')->where($where)->update($data));    
        return true;
    }

    function updateMass($where=array(), $data = array()) {

        
        $this->db->q(qb::_table('taxes')->where($where)->update($data));    
        return true;
    }

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='tax_id ASC') {
	    $where['tax_user_id']=$_SESSION['account']['user_id'];
	    $where['tax_trash']=0;
        if($page>0)$page--;
        $offset = \intval($page*$count);
        $select = '*';
        $collection = qb::_table('taxes');
      //  $collection->leftjoin('countries', 'countries.country_iso', 'clients.tax_country');       
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getListCount($where=array(),$search = array()) {
	    $where['tax_user_id']=$_SESSION['account']['user_id'];
	    $where['tax_trash']=0;
        $select = '*';
        $collection = qb::_table('taxes');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="tax_id ASC") {
		
    	$where['tax_trash']=0;
    	$where['tax_user_id']=$_SESSION['account']['user_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(tax_tags LIKE '".$tag."')";
			$searchnew['tax_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('taxes');
       // $collection->leftjoin('countries', 'countries.country_iso', 'clients.tax_country');
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('tax_id')->Limit($offset,$count)->select($select));    
    }

    function getListCountSearch($where=array(),$search = array()) {
    	$where['tax_trash']=0;
    	$where['tax_user_id']=$_SESSION['account']['user_id'];
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(tax_tags LIKE '".$tag."')";
			$searchnew['tax_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('taxes');

       $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('tax_id')->count('*'));  
       return count($count);  
    }
	
	function getByID($id=0){
		
		$where['tax_id'] = $id;
	    $where['tax_user_id'] = $_SESSION['account']['user_id'];
        $select = '*';
        $collection = qb::_table('taxes');
        //$collection->leftjoin('countries', 'countries.country_iso', 'clients.tax_country');
        return $this->db->q_($collection->where($where)->select($select));    
	}

    function getAllList($where=array(),$order='tax_id ASC') {
	    $where['tax_user_id']=$_SESSION['account']['user_id'];
	    $where['tax_trash']=0;
        $select = '*';
        $collection = qb::_table('taxes');
      
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
    }
	
	
}