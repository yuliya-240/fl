<?php      
namespace floctopus\models\common;

use \floctopus\application as app;     
use \jet\controller\http as HTTPController;   
use \floctopus\models\logic\jetsession as Session;
use \floctopus\models\orm\notifications as OrmNotifications;
use \floctopus\models\orm\colloborators as OrmCollabs;
use \floctopus\models\orm\chat as OrmChat;
use \floctopus\models\orm\log as OrmLog;

class adminController extends HTTPController {

    function __before() {
        parent::__before();
        $this->session = new Session();
        $this->session->refresh();

        if (!$this->session->isLogged() ) \jet\redirect('/login/',true);
        if($_SESSION['account']['user_first']){
	        
	       \jet\redirect('/first',true); 
        }

        $this->view = new \jet\twig();  
        $this->view->acs = $this->session->getData(); 

        $this->skin=app::$device; 
        $this->view->skin = $this->skin; 

		
		$this->view->today = \date($_SESSION['account']['user_dformat']);
        //$this->view->datetime=$_SESSION['account']['user_dformat'].' '.$_SESSION['account']['user_tformat'];
		
		//$this->view->momentformat = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
		
		$this->view->channel = $_SESSION['account']['user_pusher_channel'];
		
		$this->log = new OrmLog();

		$this->notifications = new OrmNotifications();
		$selPreviewNote['n_status_preview!']=1;
		$selPreviewNote['n_user_id']=$_SESSION['account']['user_id'];
		$this->view->previewNotifyCount = $this->notifications->getCount($selPreviewNote);
		$selNote['n_user_id']=$_SESSION['account']['user_id'];
		$this->view->noteficationsList = $this->notifications->getList($selNote,1,5);
		
        if(!isset($_SESSION['iop']))$_SESSION['iop'] = $_SESSION['account']['user_rop'];
        $this->session->setTZ($_SESSION['account']['user_tz']);	
		
		$this->view->lng_common = app::$lang->common;
		$this->view->lng_menu = app::$lang->menu;

        $this->view->datetime=$_SESSION['account']['user_dformat'].' '.$_SESSION['account']['user_tformat'];
		$this->view->dpformat = app::dateFormatToDatePicker($_SESSION['account']['user_dformat']);
		$this->view->momentformat = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
		if(!isset($_SESSION['iop']))$_SESSION['iop'] = $_SESSION['user']['user_rop'];
		$this->ismobphone = $this->session->isMobPhone();
		$this->view->ismobphone =  $this->ismobphone;
		
		if(!isset($_SESSION['channel']))$_SESSION['channel']="floctoid";
		$this->view->currchannel = $_SESSION['channel'];
		$this->chat = new OrmChat();
		$this->view->isUnread = $this->chat->getCountUnread();
		$this->view->chsound = $_SESSION['account']['user_chat_sound'];
		
		$collabsObj = new OrmCollabs();
		$selInvites['cb_status'] = array(1,2);
	
		$this->view->countInvites = $collabsObj->getListCount($selInvites);
		
		$this->view->hashver = "sfdgseeeefsbdf892";//uniqid();

    }
    
    function __default($args = false){
	     //if($_SESSION['account']['user_role']>1 && !$_SESSION['acl']['cfield']['acl_access']) die();
    }

    function deleteBundle(){
	  //  if($_SESSION['account']['user_role']>1 && $_SESSION['acl']['cfield']['acl_access']<2) die();
	    $failed = array();
	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){
			    $this->delete($val);
		    }
		    
		    $res['status']=true;
		    return $res;
		    
		    
	    }else{
		    $res['title']="Operation Failed";
		    $res['status']=false;
		    $res['msg']="No rows selected. Please select at least one template.";
		    return $res;
	    }	    
	    
    }
   
    function logout(){
	    $this->session->logout();
	   // $this->session->logout();
	    \jet\redirect('/login',true);
    }

    function makeTags($search = null){
    
	    // Приводим строку к нижнему регистру, и убираем пробелы по краям
	    $string = trim(strtolower($search));
	   // app::trace($string);
	    // Заменяем символы на пробелы, т.к. в строке уже встречется символ "-"
	  //  $string = preg_replace("/[^A-Za-z0-9@.\s]/u","",$string);
 
 	    // Удаляем лишние пробелы, оставляем только один пробел между словами
	    $string = trim(preg_replace("/\s+/u", " ", $string));
	    
	    $tags = explode(" ",$string);
	    
	    $srch['str'] = $string;
	    $srch['tags'] = $tags;
	    return $srch;
     }

	function resize($img, $w, $h, $newfilename) {
	    //Check if GD extension is loaded
	    if (!extension_loaded('gd') && !extension_loaded('gd2')) {
	        trigger_error("GD is not loaded", E_USER_WARNING);
	        return false;
	    }
	     
	    //Get Image size info
	    $imgInfo = getimagesize($img);
	    
	    switch ($imgInfo[2]) {
	    
	        case 1: $im = imagecreatefromgif($img); break;
	        case 2: $im = imagecreatefromjpeg($img);  break;
	        case 3: $im = imagecreatefrompng($img); break;
	        default:  trigger_error('Unsupported filetype!', E_USER_WARNING);  break;
	    }
	     
	    //If image dimension is smaller, do not resize
	    if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
	        $nHeight = $imgInfo[1];
	        $nWidth = $imgInfo[0];
	    }
	    else{
	    // yeah, resize it, but keep it proportional
	        if ($w/$imgInfo[0] > $h/$imgInfo[1]) {
	            $nWidth = $imgInfo[0]*($h/$imgInfo[1]);
	            $nHeight = $h;            
	        }
	        else{
	            $nWidth = $w;
	            $nHeight = $imgInfo[1]*($w/$imgInfo[0]);
	        }
	    }
	     
	    $nWidth = round($nWidth);
	     
	    $nHeight = round($nHeight);
	     
	    $newImg = imagecreatetruecolor($nWidth, $nHeight);
	     
	    /* Check if this image is PNG or GIF, then set if Transparent*/  
	    
	    if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
	        imagealphablending($newImg, false);
	        imagesavealpha($newImg,true);
	        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
	        imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
	    }
	     
	    imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);
	     
	    //Generate the file, and rename it to $newfilename
	    switch ($imgInfo[2]) {
	        case 1: imagegif($newImg,$newfilename); break;
	        case 2: imagejpeg($newImg,$newfilename);  break;
	        case 3: imagepng($newImg,$newfilename); break;
	        default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
	    }
	     
	    return $newfilename;
	 }	
    
	public function logit($module,$action,$desc,$obj=0){
		 $loc = app::ip_info();
		 
		 $add = array(

			"log_user_id" => $_SESSION['account']['user_id'],
			"log_action" => $action,
			"log_module" => $module,
			"log_details" => $desc,
			"log_object_id" =>$obj,
			"log_ip" =>$loc['ip'],
		 );
		 
		 $this->log->add($add);
		 
		 return false;
	}
	 

}
