<?php
namespace retail\models\twig;

use jet\libs\Twig\Extension\jetExtension as jetExtension;

class extension extends jetExtension
{
    
  public function getFilters()
  {
    return array_merge(
      parent::getFilters(),
      array(
        'asterify' => new \Twig_Filter_Method($this, 'asterify'),
        'creditCardEscape' => new \Twig_Filter_Method($this, 'creditCardEscape'),
        'clean_num' => new \Twig_Filter_Method($this, 'clean_num')
      )
    );
  }
  
  public function asterify($string = "") {
      return str_repeat('*',strlen($string));
  }
  
  public function creditCardEscape($string = "") {
      return "XXXX-XXXX-XXXX-".substr($string, -4); 
  }
  
  public function clean_num( $num="" ){
  	$pos = strpos($num, '.');
	if($pos === false) { // it is integer number
			return $num;
	}else{ // it is decimal number
			return rtrim(rtrim($num, '0'), '.');
	}
  }

}
