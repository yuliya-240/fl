<?php      
namespace floctopus\controllers\settings;

use \floctopus\application as app;     
use \floctopus\models\orm\accounts as OrmAccounts;
use \floctopus\models\common\adminController as adminController;     
use \floctopus\models\orm\country as Countries; 
use \floctopus\models\orm\misc as OrmMisc; 

class main extends adminController {
    
    function __before() {
    	
    	parent::__before();
    	$this->country = new Countries();
    	$this->view->lng_settings = app::$lang->settings;
        $this->view->setPath(app::$device.'/settings');   
        $this->view->page_title = "Settings";
    	$this->misc = new OrmMisc();

	}   
     
    function __default($args = false) {   
		
		$this->view->colors = $this->misc->getColors();
		// app::trace($_SESSION['account']);
		 $this->view->country = $this->country->getCountryName($_SESSION['account']['set_billing_country']);
        $this->view->setTemplate('index.tpl');  
        $this->view->topInvoices = "active";
        $this->view->d=time();
        return $this->view;
    } 
    
    function getInvPrefs(){

	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    
        $this->view->setTemplate('editinvprefs.tpl');  
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
	    
    }
    
    function doUpdateInvPref(){
	    
	    $acc = new  OrmAccounts();
	    
	    if(!isset($_POST['set_show_outstanding']))$_POST['set_show_outstanding']=0; else $_POST['set_show_outstanding']=1;
	    if(!isset($_POST['set_show_payment_stub']))$_POST['set_show_payment_stub']=0; else $_POST['set_show_payment_stub']=1;
	    
	    $upd['set_inv_number_start']= $_POST['set_inv_number_start'];
	    $upd['set_est_number_start']= $_POST['set_est_number_start'];
	    $upd['set_past_due_terms'] = $_POST['set_past_due_terms'];
	    $upd['set_show_outstanding'] = $_POST['set_show_outstanding'];
	    $upd['set_show_payment_stub'] = $_POST['set_show_payment_stub'];
	    $acc->updateSettings($_SESSION['account']['user_id'],$upd);
	    
	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    
        $this->view->setTemplate('viewinvprefs.tpl');  
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
    }

	function doUpdateInvTerms(){
	    
	    $acc = new  OrmAccounts();
	    

	    $upd['set_inv_terms'] = app::strings_clear($_POST['set_inv_terms']);
	    $upd['set_inv_notes'] = app::strings_clear($_POST['set_inv_notes']);
	    $acc->updateSettings($_SESSION['account']['user_id'],$upd);
	    
	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    
        $this->view->setTemplate('viewinvterms.tpl');   
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
		
		
	}

	function getInvTerms(){

	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    
        $this->view->setTemplate('editinvterms.tpl');  
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
	}

	function getEstTerms(){

	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    
        $this->view->setTemplate('editestterms.tpl');  
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
	}

	function doUpdateEstTerms(){
		
	    $acc = new  OrmAccounts();
	    

	    $upd['set_est_terms'] = app::strings_clear($_POST['set_est_terms']);
	    $upd['set_est_notes'] = app::strings_clear($_POST['set_est_notes']);
	    $acc->updateSettings($_SESSION['account']['user_id'],$upd);
	    
	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    
        $this->view->setTemplate('viewestterms.tpl');   
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
	}

	function getBillAddr(){

	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    $this->view->countryList=$this->country->getCountriesList();

        $this->view->setTemplate('editbilladdr.tpl');  
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
		
		
	}
	
	function doUpdateBillAddr(){
		
	    
	    $acc = new  OrmAccounts();
	    
	    $upd['set_billing_name']= $_POST['set_billing_name'];
	    $upd['set_billing_street']= $_POST['set_billing_street'];
	    $upd['set_billing_city'] = $_POST['set_billing_city'];
	    $upd['set_billing_state'] = $_POST['set_billing_state'];
	    $upd['set_billing_zip'] = $_POST['set_billing_zip'];
	    $upd['set_billing_country'] = $_POST['set_billing_country'];
	    $acc->updateSettings($_SESSION['account']['user_id'],$upd);
	    
	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    $this->view->country = $this->country->getCountryName($upd['set_billing_country']);
        $this->view->setTemplate('viewbillingaddr.tpl');  
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
		
		
	}
    
    function removeLogo(){
	    $acc = new  OrmAccounts();
	    $upd['set_logo'] = "nologo.png";
	    $acc->updateSettings($_SESSION['account']['user_id'],$upd);
	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    $res['status'] = true;
	    return $res;

    }
    
    function changeColor(){
	    $acc = new  OrmAccounts();
	    $color=$_POST['color'];
	    $field = $_POST['field'];
	    
	    $upd[$field]=$color;
	    $acc->updateSettings($_SESSION['account']['user_id'],$upd);
	    $this->session->refresh();
	    
	    $res['status'] = true;
	    $res['msg'] = "Color has been saved";
	    return $res;
	    
    }
}
