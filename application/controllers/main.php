<?php      
namespace floctopus\controllers;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;     
use \floctopus\models\orm\accounts as OrmAccounts; 


class main extends adminController {
    
    function __before() {

    	parent::__before();
    	
        $this->view->setPath(app::$device.'/dashboard');  
        $this->view->page_title = ""; 
	}   
     
    function __default($args = false) {   
	    
	   
	    
	    $this->view->page_title = "Dashboard";
	    $this->view->setTemplate('index.tpl');
        return $this->view;
    } 
    
    
    function logout(){
	
	    $this->logit("logout","logout","User logout");
	    $this->session->logout();
	    \jet\redirect('/login',true);
    }
    
    function __format() {
        return array();
    } 
    
}
