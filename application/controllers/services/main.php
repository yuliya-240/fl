<?php      
namespace floctopus\controllers\services;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;   
use \floctopus\models\orm\services as OrmSrv;
use \floctopus\models\libs\pagination as pagination;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_services = app::$lang->services;
        $this->view->setPath(app::$device.'/services');   
		$this->srv = new OrmSrv();


		if(!isset($_SESSION['SRV_RET_URL']))$_SESSION['SRV_RET_URL']="/services";
		if(!isset($_SESSION['SRV_PAGE']))$_SESSION['SRV_PAGE']=1;
		$this->view->sbRes = "active";
		$this->view->subSrv = "active";
	}   

    function __default($args = false) {   

		$this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
    function add(){
	    
        $this->view->setTemplate('add.tpl');  
        return $this->view;
    }

	function doAdd(){
		
		$name = app::strings_clear($_POST['srv_name']);
		$desc = app::strings_clear($_POST['srv_desc']);
		$cost = $_POST['srv_cost'];
		$tax1 = $_POST['srv_tax1'];
		$tax2 = $_POST['srv_tax2'];
		
		if(!strlen($cost))$cost=0;
		if(!strlen($tax1))$tax1=0;
		if(!strlen($tax2))$tax2=0;
		$taxcash1 = ($cost*$tax1)/100;
		$taxcash1 = round($taxcash1, 2);

		$taxcash2 = ($cost*$tax2)/100;
		$taxcash2 = round($taxcash2, 2);
		
		$add = array(
			
			"srv_name" => $name,
			"srv_desc" => $desc,
			"srv_cost" => $cost,
			"srv_tax1" => $tax1,
			"srv_tax_cash1" => $taxcash1, 
			"srv_tax_cash2" => $taxcash2, 
			"srv_tax2" => $tax2,
			"srv_user_id" => $_SESSION['account']['user_id']
			
		);
		
		$this->srv->add($add);
		
		$res['status'] = true;
		return $res;
		
	}

	function doUpdate(){
		
		$name = app::strings_clear($_POST['srv_name']);
		$desc = app::strings_clear($_POST['srv_desc']);
		$cost = $_POST['srv_cost'];
		$tax1 = $_POST['srv_tax1'];
		$tax2 = $_POST['srv_tax2'];
		
		if(!strlen($cost))$cost=0;
		if(!strlen($tax1))$tax1=0;
		if(!strlen($tax2))$tax2=0;
		
		$add = array(
			
			"srv_name" => $name,
			"srv_desc" => $desc,
			"srv_cost" => $cost,
			"srv_tax1" => $tax1,
			"srv_tax2" => $tax2,
			"srv_user_id" => $_SESSION['account']['user_id']
			
		);
		
		$this->srv->update($_POST['srv_id'],$add);
		
		$res['status'] = true;
		return $res;
		
	}

	function getContent(){
    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['srv_trash']=0;
	    $where['srv_user_id']=$_SESSION['account']['user_id'];

    	if(isset($_GET['query'])){
	    	$r = $this->makeTags($_GET['query']);
		 
		    $srch=$r['str'];
		    if(strlen($srch)){
			    $_SESSION['SRV_SEARCH_TAGS']=array($srch);
			    $_SESSION['SRV_SEARCH_STR']=$srch;
			    
		    }else{
		    	$_SESSION['SRV_SEARCH_TAGS']=NULL;
		    	$_SESSION['SRV_SEARCH_STR']=NULL;
			    
		    }
    	}
    	
     	if(isset($_SESSION['SRV_SEARCH_STR']) ){
	    	
	    	$totalRec = $this->srv->getListCountSearch($where,$_SESSION['SRV_SEARCH_TAGS']);
    	}else{
	    	
	    	$totalRec = $this->srv->getListCount($where,$search);
    	}
	    
     	$page = isset($_GET['p']) && $_GET['p']  ? \abs((int)$_GET['p']) :1;
	    $itemsOnPage = isset($_GET['iop']) && $_GET['iop']  ? \abs((int)$_GET['iop']) : $_SESSION["iop"];

	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		
		$order ='srv_name ASC';
	    
	    $this->view->totalCount=$totalRec;

	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;

    	if(isset($_SESSION['SRV_SEARCH_STR'])){
	    	
	    	$this->view->records = $this->srv->getListSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['SRV_SEARCH_TAGS'],$order);
    	}else{
    		$this->view->records = $this->srv->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
   		}
		
        $this->view->setTemplate('page.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;
        
        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
	}
	
	function deleteBundle(){
		    
	    foreach($_POST['row'] as $val){
		    $z = $this->delete($val);
	    }
	    $res['status']=true;
	    return $res;
		
	}
	
	function delete($id=0){
		
		$upd['srv_trash'] = 1;
		$this->srv->update($id,$upd);
		
		$res['status']=true;
		return $res;
	}

	function edit(){
		
		$id = $this->args[0];
		$this->view->srv = $this->srv->getByID($id);
		
        $this->view->setTemplate('edit.tpl');  
        return $this->view;
		
		
	}

    function invfilter(){
	    $res=array();
	    
	    $where['srv_user_id'] = $_SESSION['account']['user_id'];
	    $where['srv_trash'] = 0;
	    
	    $rz = $this->makeTags($_POST['query']);
		 
		$srch=$rz['str'];
		//$search['item_name'] =$srch;
		//app::trace($_POST['query']);
		//app::trace($search['item_name']);
	    //$srv =  $this->srv->getList($where,1,21,$search,'item_name ASC');
	    
		$srv = $this->srv->getListSearch($where,1,21,array($srch),'srv_name asc');
		
		    $res[0]['id']   = 0;
		    $res[0]['fullname'] = $_POST['query'];
		    $res[0]['name'] = $_POST['query'];
		    $res[0]['cost'] = 0;
		    $res[0]['qty']  = 1;
		    $res[0]['tax1'] = 0;
		    $res[0]['tax2'] = 0;
		    $res[0]['desc'] = '';
	    
	    foreach($srv as $key=>$val){
		   
		    $res[$key+1]['id']   = $val['srv_id'];
		    $res[$key+1]['fullname'] = $val['srv_name'];
		    $res[$key+1]['name'] = $val['srv_name']." ".$val['srv_cost'];
		    $res[$key+1]['cost'] = $val['srv_cost'];
		    $res[$key+1]['qty']  = $val['srv_qty'];
		    $res[$key+1]['tax1'] = $val['srv_tax1'];
		    $res[$key+1]['tax2'] = $val['srv_tax2'];
		    $res[$key+1]['desc'] = $val['srv_desc'];
		    $res[$key+1]['total'] = $val['srv_cost'];//$val['srv_tax_cash1']+$val['srv_tax_cash2']+$val['srv_cost'];
	    }

	    
	    return $res;
    } 	
	
}
