<?php      
namespace floctopus\controllers\messenger;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;  
use \floctopus\models\orm\clients as OrmClients; 
use \floctopus\models\orm\colloborators as OrmCollab;
use \floctopus\models\orm\chat as OrmChat;
use \floctopus\models\orm\accounts as OrmAcc;
use \jet\libs\pusher as PUSHER;
use \floctopus\models\libs\pagination as pagination;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
        $this->view->setPath(app::$device.'/messanger');   
		$this->client = new OrmClients();
    	$this->collab = new OrmCollab();
    	$this->chat = new OrmChat();
		if(!isset($_SESSION['MSG_RET_URL']))$_SESSION['MSG_RET_URL']="/clients";
		$this->view->sbPeople = "active";
		$this->view->subClients = "active";
	}   

    function __default($args = false) {   
		
		
        $this->view->setTemplate('index.tpl');
         
        return $this->view;
    } 
    
    function getContent(){
	    
	    $users = $this->collab->getAllList();
	    $clients = $this->client->getAllForChat();
	    
	    
		//app::Log($t);
	    $this->view->users = $users;
	    $this->view->clients = $clients;
	    
        $this->view->setTemplate('index.tpl');
         
        return $this->view;
	    
    }
	
	function send(){
		
		$channel = $_POST["channel"];
		$msg = trim($_POST["msg"]);
		$_SESSION['channel'] = $channel;
		$addChat['chat_from'] = $_SESSION['account']['user_pusher_channel'];
		$addChat['chat_to'] = $channel;
		$addChat['chat_msg'] = $msg;
		
		$msgid = $this->chat->add($addChat);
		$data['from'] = $_SESSION['account']['user_pusher_channel'];
		if($channel==="floctoid"){
			
			
			$msg = "You said: '".$msg."', i am not so smart enogh to understand this. Valerii has promised to me update my brains. :) ";
			
			if($msg =="hello"){
				
				$msg = "Hello, mr. ".$_SESSION['account']['user_name'];
			}
			
			if($msg =="help"){
				$msg = "I am chat bot. My name is Floctoid. I am here to help you !! Thanks to Valerii :) :)";
				
			}
			$addChat = array();
			$addChat['chat_from'] = $channel;
			$addChat['chat_to'] = $_SESSION['account']['user_pusher_channel'];
			$addChat['chat_msg'] = $msg;
			$msgid = $this->chat->add($addChat);
			
			$channel = $_SESSION['account']['user_pusher_channel'];
			$data['from'] = 'floctoid';

		}
		
			$data['id'] = $msgid;
			$data['msg'] = $msg;
			
			//<time datetime="{{h.log_date|date('Y-m-d')}}T{{h.log_date|date('H:i:s')}}" class="timeago"></time>
			$this->sendPusher($channel,"msg",$data);

		
	}
    
    function sendPusher($channel,$event,$data){
	 PUSHER::load();
	    
	 $options = array(
	    'encrypted' => true
	  );
	  
	  $pusher = new \Pusher\Pusher(
	    '927e2dc012613094456b',
	    'fb43f0687c0f52a0bd25',
	    '379231',
	    $options
	  );
	
	  
	  $pusher->trigger($channel, $event, $data);
	  
	  return false;
    }
    
    function getMsg(){
	    
	    $id = $_GET['id'];
	    
	    $msg = $this->chat->getByID($id);
	    
	    $res['status'] = true;
	    $res['msg'] = $msg;
	    
	    return $msg;
    }
    
    function getChat(){
	    
	    $channel1 = $_GET['ch'];
	
	    $channel2 = $_SESSION['account']['user_pusher_channel'];
	    $ml = $this->chat->getList($channel1,$channel2);
		
		$upd['chat_status']=1;
		$where['chat_to'] = $_SESSION['account']['user_pusher_channel'];
		$where['chat_from'] = $channel1;
		$this->chat->updateMass($where,$upd);
		
	    $this->view->messeges = $ml;
	    $this->view->setTemplate('msghistory.tpl');
		$html = $this->view->render();
	    
	    $res['html'] = $html;
	    $res['status'] = true;
	    
	    return $res;
    }
    
    function getUnread(){

		$res['status'] = false;
	    $unr = $this->chat->getAllUnread();
	    
	    if($unr){
		    foreach($unr as $k=>$v){
			    $unrids[] = $v['chat_from'];
			    $unrcnt[] = $v['cnt'];
		    }
			$res['unrids'] = $unrids;
			$res['unrcnt'] = $unrcnt;
			$res['status'] = true;
		}
		return $res;
    }
    
    function auth(){
		PUSHER::load();
		    
		$options = array(
		    'encrypted' => true
		);
		  
		$pusher = new \Pusher\Pusher(
		    '927e2dc012613094456b',
		    'fb43f0687c0f52a0bd25',
		    '379231',
		    $options
		);
	   
	    $acc = new OrmAcc();
	   
	   
	   
	    $isAuth = $acc->getByChannel($_POST['channel_name']);
	    
	    
	   
	  	if($isAuth){
			echo $pusher->socket_auth($_POST['channel_name'], $_POST['socket_id']);
		}else{
			header('', true, 403);
			echo "Forbidden";
		}
	   
	 
    }

	function changeSound(){
		
		$upd['user_chat_sound'] = $_POST['val'];
		$this->session->updateAcc($upd);
	}
	
	function markUnread(){
		
		$upd['chat_status']=1;
		$where['chat_to'] = $_SESSION['account']['user_pusher_channel'];
		$where['chat_from'] = $_POST['ch'];
		$this->chat->updateMass($where,$upd);
		
	}
    
}