<?php      
namespace floctopus\controllers\equipments;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;   
use \floctopus\models\orm\equipment as OrmEquip;
use \floctopus\models\libs\pagination as pagination;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_equipment = app::$lang->equipment;
        $this->view->setPath(app::$device.'/equipments');   
		$this->equip = new OrmEquip();


		if(!isset($_SESSION['EQUIP_RET_URL']))$_SESSION['EQUIP_RET_URL']="/services";
		if(!isset($_SESSION['EQUIP_PAGE']))$_SESSION['EQUIP_PAGE']=1;
		$this->view->sbRes = "active";
		$this->view->subEquip = "active";
	}   

    function __default($args = false) {   

		$this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
    function add(){
	    
        $this->view->setTemplate('add.tpl');  
        return $this->view;
    }

	function getContent(){
		
    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['EQUIP_trash']=0;
	    $where['EQUIP_user_id']=$_SESSION['account']['user_id'];

    	if(isset($_GET['query'])){
	    	$r = $this->makeTags($_GET['query']);
		 
		    $srch=$r['str'];
		    if(strlen($srch)){
			    $_SESSION['EQUIP_SEARCH_TAGS']=array($srch);
			    $_SESSION['EQUIP_SEARCH_STR']=$srch;
			    
		    }else{
		    	$_SESSION['EQUIP_SEARCH_TAGS']=NULL;
		    	$_SESSION['EQUIP_SEARCH_STR']=NULL;
			    
		    }
    	}
    	
     	if(isset($_SESSION['EQUIP_SEARCH_STR']) ){
	    	
	    	$totalRec = $this->srv->getListCountSearch($where,$_SESSION['EQUIP_SEARCH_TAGS']);
    	}else{
	    	
	    	$totalRec = $this->srv->getListCount($where,$search);
    	}
	    
     	$page = isset($_GET['p']) && $_GET['p']  ? \abs((int)$_GET['p']) :1;
	    $itemsOnPage = isset($_GET['iop']) && $_GET['iop']  ? \abs((int)$_GET['iop']) : $_SESSION["iop"];

	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		
		$order ='EQUIP_name ASC';
	    
	    $this->view->totalCount=$totalRec;

	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;

    	if(isset($_SESSION['EQUIP_SEARCH_STR'])){
	    	
	    	$this->view->records = $this->srv->getListSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['EQUIP_SEARCH_TAGS'],$order);
    	}else{
    		$this->view->records = $this->srv->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
   		}
		
        $this->view->setTemplate('page.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;
        
        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
	}
	
	function deleteBundle(){
		    
	    foreach($_POST['row'] as $val){
		    $z = $this->delete($val);
	    }
	    $res['status']=true;
	    return $res;
		
	}
	
	function delete($id=0){
		
		$upd['equip_trash'] = 1;
		$this->equip->update($id,$upd);
		
		$res['status']=true;
		return $res;
	}

	function edit(){
		
		$id = $this->args[0];
		$this->view->equip = $this->equip->getByID($id);
		
        $this->view->setTemplate('edit.tpl');  
        return $this->view;
		
		
	}
	
}
