<?php      
namespace floctopus\controllers\transactions;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;   
use \floctopus\models\orm\transactions as OrmTrans;
use \floctopus\models\orm\invoices as OrmInv;
use \floctopus\models\orm\expenses as OrmExp;
use \floctopus\models\orm\exp_categories as OrmExpCat;

use \floctopus\models\libs\pagination as pagination;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_transactions = app::$lang->transactions;
    	$this->view->lng_expenses = app::$lang->expenses;
        $this->view->lng_invoices = app::$lang->invoices;
        $this->view->setPath(app::$device.'/transactions');
		$this->trans = new OrmTrans();
		$this->inv = new OrmInv();
		$this->exp = new OrmExp();
		$this->expcat = new OrmExpCat();
		if(!isset($_SESSION['TRANS_RET_URL']))$_SESSION['TRANS_RET_URL']="/transactions";
		if(!isset($_SESSION['TRANS_PAGE']))$_SESSION['TRANS_PAGE']=1;
		$this->view->sbMoney = "active";
		$this->view->subTransactions = "active";
		$this->view->momentformat = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
        if(!isset($_SESSION['INV_LANG']))$_SESSION['INV_LANG'] = $_SESSION['account']['user_lang'];
        if(!isset($_SESSION['TRANS_STARTDATE']))$_SESSION['TRANS_STARTDATE']="none";
        if(!isset($_SESSION['TRANS_ENDDATE']))$_SESSION['TRANS_ENDDATE']="none";
		if(!isset($_SESSION['TRANS_SEARCH_STR']))$_SESSION['TRANS_SEARCH_STR']="";

        if(!isset($_SESSION['TRANS_TYPE']))$_SESSION['TRANS_TYPE']=0;

        if(!isset($_SESSION['TRANS_METHOD'])){$_SESSION['TRANS_METHOD'] = array(0);}

    }
	
	 function __default($args = false) {
      //  unset( $_SESSION['TRANS_TYPE'] );
         $this->view->tmethodlist = $this->trans->getPaymentMethodsList();
         $this->view->searchstr = $_SESSION['TRANS_SEARCH_STR'];
         $this->view->fixed_footer = "am-sticky-footer";
         $this->view->startdate = $_SESSION['TRANS_STARTDATE'];
         $this->view->enddate = $_SESSION['TRANS_ENDDATE'];
         if(!isset($_SESSION['TRANS_PAGE']))$_SESSION['TRANS_PAGE']=1;
         $this->view->ttype = $_SESSION['TRANS_TYPE'];
         $this->view->t_method = $_SESSION['TRANS_METHOD'];
         $this->view->setTemplate('index.tpl');
         return $this->view;
     }
	 
	 function getContent(){
         $pages = new pagination();
         $where=array();
         $search=array();
         $where['t_trash']=0;
         $where['t_user_id']=$_SESSION['account']['user_id'];

         if(isset($_GET['query'])){
             $r = $this->makeTags($_GET['query']);

             $srch=$r['str'];
             if(strlen($srch)){
                 $_SESSION['TRANS_SEARCH_TAGS']=array($srch);
                 $_SESSION['TRANS_SEARCH_STR']=$srch;

             }else{
                 $_SESSION['TRANS_SEARCH_TAGS']=NULL;
                 $_SESSION['TRANS_SEARCH_STR']=NULL;

             }
         }

         foreach($_SESSION['TRANS_METHOD'] as $m){

             if($m!=0)$where['t_method'][]=$m;
         }

         if($_SESSION['TRANS_TYPE']>0){
             if($_SESSION['TRANS_TYPE']==1)
                 $where["t_exp_id"]=0;
             else
                 $where["t_exp_id>"]=1;

         }
        // app::trace($_SESSION['TRANS_TYPE']);

         if($_SESSION['TRANS_STARTDATE']!="none"){

             $where['t_date>']=$_SESSION['TRANS_STARTDATE'];
             $where['t_date<']=$_SESSION['TRANS_ENDDATE'];
         }


         if(isset($_SESSION['TRANS_SEARCH_STR']) ){

             $totalRec = $this->trans->getListCountSearch($where,$_SESSION['TRANS_SEARCH_TAGS']);
         }else{

             $totalRec = $this->trans->getListCount($where,$search);
         }

         $page = $_SESSION['TRANS_PAGE'];
         $itemsOnPage = $_SESSION["account"]['user_rop'];

         $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
         $order ='t_date ASC';
         //$this->view->exp = $this->exp->getList();
         $this->view->records = $this->trans->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
         $this->view->totalCount=$totalRec;

         $this->view->curl = $_SERVER['REQUEST_URI'];
         $this->view->iop = $itemsOnPage;
         $this->view->pagination = $pagination;
         $this->view->totalRec = $totalRec;


         if(isset($_SESSION['TRANS_SEARCH_STR'])){

             $this->view->records = $this->trans->getListSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['TRANS_SEARCH_TAGS'],$order);
             $this->view->totalPage = $this->trans->getTotalPageSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['TRANS_SEARCH_TAGS'],$order);
         }else{
             $this->view->records = $this->trans->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
             $this->view->totalPage = $this->trans->getTotalPage($where,$pagination['current'],$itemsOnPage,$search);
         }


         $this->view->setTemplate('page.tpl');
         $html = $this->view->render();

         $this->view->setTemplate('footer.tpl');
         $footer = $this->view->render();

         $res['footer'] = $footer;
         $res['count'] = $totalRec;
         $res['html'] = $html;
         //app::trace($html);
         return $res;

     }

    function changeDateRange(){

        $_SESSION['TRANS_STARTDATE'] = $_POST['start'];
        $_SESSION['TRANS_ENDDATE'] = $_POST['end'];

        if(isset($_SESSION['TRANS_SEARCH_STR']))$res['s'] = $_SESSION['TRANS_SEARCH_STR']; else $res['s'] = "";
        $res['status'] = true;
        //app::trace($res);
        return $res;
    }

//    function doApplyFilter(){
//
//        if(isset($_GET['cat']))$where['exp_category']=($_GET['cat']);
//        if(isset($_GET['cat']))$this->view->pcat = $_GET['cat'];
//        $res['status'] = true;
//        return $res;
//    }


    function doApplyFilter(){

    //app::trace($_POST);
        if(isset($_POST['tmethod'])){
            $_SESSION['TRANS_METHOD'] = array();
            $arr=array();
            foreach($_POST['tmethod'] as $k=>$v){

                $arr[]=$v;
                $_SESSION['TRANS_METHOD'] = $arr;

            }
        }

        $_SESSION['TRANS_TYPE'] = $_POST['type'];
        $res['s']=$_SESSION['TRANS_SEARCH_STR'];
        $res['status'] = true;
        return $res;
    }

    function getViewBox(){
        //app::trace($_GET);
        $str = $_GET['id'];
        list($id,$at) = explode("@", $str);

        if($at=="exp"){
            $exp = $this->exp->getByID($id);
            $this->view->id = $id;
            $this->view->exp = $exp;
            $this->view->setTemplate('view_exp.tpl');
            $html = $this->view->render();
            $res['id']= $id;
            $res['html'] = $html;
            $res['status'] = true;

            return $res;

        }else{

            $inv = $this->inv->getByID($id);
            $this->view->id = $id;
            $this->view->inv = $inv;

            $this->view->setTemplate('view_inv.tpl');
            $html = $this->view->render();
            $res['id']= $id;
            $res['html'] = $html;
            $res['status'] = true;

            return $res;


        }


    }

}
