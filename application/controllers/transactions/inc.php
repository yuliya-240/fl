<?php      
namespace floctopus\controllers\transactions;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;   
use \floctopus\models\orm\transactions as OrmTrans;
use \floctopus\models\orm\invoices as OrmInv;

class inc extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_transactions = app::$lang->transactions;
        $this->view->setPath(app::$device.'/transactions');   
		$this->trans = new OrmTrans();
		$this->inv = new OrmInv();
		if(!isset($_SESSION['TRANS_RET_URL']))$_SESSION['TRANS_RET_URL']="/transactions";
		if(!isset($_SESSION['TRANS_PAGE']))$_SESSION['TRANS_PAGE']=1;
		$this->view->sbMoney = "active";
		$this->view->subTransactions = "active";
		$this->view->momentformat = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
	}   
	
	
	
/*	function doAdd(){
		
	

		$add['t_user_id']	= $_SESSION['account']['user_id'];
		$add['t_collab_id']	= $_POST['pay_collab_id'];
		$add['t_client_id']	= $_POST['pay_client_id'];
		$add['t_inv_id']	= $_POST['pay_inv_id'];
		$add['t_method']	= $_POST['method'];
		$add['t_date']		= \date("Y-m-d",strtotime($_POST['pay_date']));
		$add['t_amount']	= $_POST['amount'];
		$add['t_notes']		= $_POST['pay_notes'];
		$this->trans->add($add);

		if($_POST['pay_inv_id']){

			$inv = $this->inv->getByID($_POST['pay_inv_id']);

			$upd['inv_paid_amount_base'] = $_POST['amount']/$inv['inv_exchange_rate'];
			$upd['inv_paid_amount'] = $_POST['amount'];
			$upd['inv_total_due'] = $inv['inv_total_due']-$_POST['amount'];
			$upd['inv_total_due_base'] = $inv['inv_total_due_base']-$upd['inv_paid_amount_base'];
			if($_POST['amount']==$inv['inv_total_due'])$upd['inv_status'] = 2;
			if($_POST['amount']<$inv['inv_total_due'])$upd['inv_status'] = 3;
			$this->inv->update($_POST['pay_inv_id'],$upd);

			$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
			$addhistory['invhist_inv_id']  = $_POST['pay_inv_id'];
			$addhistory['invhist_action']  = "pay";
			$addhistory['invhist_amount']  = $_POST['amount'];
			$addhistory['invhist_currency']  = $inv['inv_currency'];
			$this->inv->addHistory($addhistory);
		}

		$res['invid'] = $_POST['pay_inv_id'];
		$res['status'] = true;
		return $res;
	}*/
	
	
	function doAddInvoice(){

		$add['user_id']			= $_SESSION['account']['user_id'];
		$add['pay_collab_id']	= $_POST['pay_collab_id'];
		$add['pay_client_id']	= $_POST['pay_client_id'];
		$add['pay_inv_id']		= $_POST['pay_inv_id'];
		$add['method']			= $_POST['method'];
		$add['pay_date']		= $_POST['pay_date'];	
		$add['amount']			= $_POST['amount'];
		$add['pay_notes']		= $_POST['pay_notes'];
		$add['pay_rate']		= $_POST['pay_rate'];
		$add['pay_due_base']	= $_POST['pay_due_base'];
		$add['pay_currency']	= $_POST['pay_currency'];
		
		$this->createInvoiceTransaction($add);
		
		$res['invid'] = $_POST['pay_inv_id'];
		$res['status'] = true;
		return $res;
	}
	
	
	function doBulkPay(){
		foreach($_POST['pay_inv_id'] as $k=>$v){
			$add = array();	
			$add['user_id']			= $_SESSION['account']['user_id'];
			$add['pay_collab_id']	= $_POST['pay_collab_id'][$k];
			$add['pay_client_id']	= $_POST['pay_client_id'][$k];
			$add['pay_inv_id']		= $_POST['pay_inv_id'][$k];
			$add['method']			= $_POST['method'][$k];
			$add['pay_date']		= $_POST['pay_date'][$k];	
			$add['amount']			= $_POST['amount'][$k];
			$add['pay_notes']		= $_POST['pay_notes'][$k];
			$add['pay_rate']		= $_POST['pay_rate'][$k];
			$add['pay_due_base']	= $_POST['pay_due_base'][$k];
			$add['pay_currency']	= $_POST['pay_currency'][$k];
			
			$this->createInvoiceTransaction($add);
		}
		$res['status'] = true;
		return $res;
	}
	
	
	function createInvoiceTransaction($post=array()){
		
			$add['t_user_id']	= $post['user_id'];
			$add['t_collab_id']	= $post['pay_collab_id'];
			$add['t_client_id']	= $post['pay_client_id'];
			$add['t_inv_id']	= $post['pay_inv_id'];
			$add['t_method']	= $post['method'];
			$add['t_date']		= \date("Y-m-d",strtotime($post['pay_date']));	
			$add['t_amount']	= $post['amount'];
			$add['t_notes']		= $post['pay_notes'];
			$add['t_exchange_rate']	= $post['pay_rate'];
			$add['t_amount_base']= $post['pay_due_base'];
			$add['t_currency']= $post['pay_currency'];
			
			$this->trans->add($add);
				
			$inv = $this->inv->getByID($post['pay_inv_id']);
			
			$upd['inv_paid_amount_base'] = $post['amount']/$inv['inv_exchange_rate'];
			$upd['inv_paid_amount'] = $post['amount'];
			$upd['inv_total_due'] = $inv['inv_total_due']-$post['amount'];
			$upd['inv_total_due_base'] = $inv['inv_total_due_base']-$upd['inv_paid_amount_base'];
			if($post['amount']==$inv['inv_total_due'])$upd['inv_status'] = 2;
			if($post['amount']<$inv['inv_total_due'])$upd['inv_status'] = 3;
			$this->inv->update($post['pay_inv_id'],$upd);
			
			$addhistory['invhist_user_id']  = $post['user_id'];
			$addhistory['invhist_inv_id']  = $post['pay_inv_id'];
			$addhistory['invhist_action']  = "pay";
			$addhistory['invhist_amount']  = $post['amount'];
			$addhistory['invhist_currency']  = $inv['inv_currency'];
			$this->inv->addHistory($addhistory);
	}
	
}


