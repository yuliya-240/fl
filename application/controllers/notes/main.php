<?php
namespace floctopus\controllers\notes;

use \floctopus\application as app;
use \floctopus\models\common\adminController as adminController;
use \floctopus\models\orm\notes as OrmNotes;
use \floctopus\models\orm\accounts as OrmAccounts;
use \floctopus\models\libs\pagination as pagination;

class main extends adminController{

    function __before()
    {

        parent::__before();
        $this->view->lng_notes = app::$lang->notes;

        $this->view->setPath(app::$device . '/notes');

        if (!isset($_SESSION['NOTES_RET_URL'])) $_SESSION['NOTES_RET_URL'] = "/notes";
        $this->view->sbDoc = "active";
        $this->view->subNotes = "active";
        $this->notes = new OrmNotes();
        $this->acc = new OrmAccounts();
    }

    function __default($args = false)
    {
        if (isset($_SESSION['NOTES_SEARCH_STR'])) $this->view->searchstr = $_SESSION['NOTES_SEARCH_STR'];

        $this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('index.tpl');

        return $this->view;
    }

    function getContent(){

        //app::trace($_SESSION['NOTES_RET_URL']);
        $pages = new pagination();
        $where = array();
        $search = array();
        $where['note_trash'] = 0;
        $where['note_user_id'] = $_SESSION['account']['user_id'];

        if (isset($_GET['query'])) {
            $r = $this->makeTags($_GET['query']);

            $srch = $r['str'];
            if (strlen($srch)) {
                $_SESSION['NOTES_SEARCH_TAGS'] = array($srch);
                $_SESSION['NOTES_SEARCH_STR'] = $srch;

            } else {
                $_SESSION['NOTES_SEARCH_TAGS'] = NULL;
                $_SESSION['NOTES_SEARCH_STR'] = NULL;

            }
        }

        if (isset($_SESSION['NOTES_SEARCH_STR'])) {

            $totalRec = $this->notes->getListCountSearch($where, $_SESSION['NOTES_SEARCH_TAGS']);
        } else {

            $totalRec = $this->notes->getListCount($where, $search);
        }

        $totalRec = $this->notes->getListCount($where, $search);

        $page = isset($_GET['p']) && $_GET['p'] ? \abs((int)$_GET['p']) : 1;
        $itemsOnPage = $_SESSION["iop"];

        $pagination = $pages->calculate_pages($totalRec, $itemsOnPage, $page);

        //app::trace($pagination);

        $order = 'note_id ASC';

        //  $this->view->totalCount=$this->notes->getListCount();

        // $this->view->curl = $_SERVER['REQUEST_URI'];
        // $this->view->iop = $itemsOnPage;
        // $this->view->pagination = $pagination;
        //  $this->view->totalRec = $totalRec;

        $this->view->records = $this->notes->getList($where, $pagination['current'], $itemsOnPage, $search, $order);

        if (isset($_SESSION['NOTES_SEARCH_STR'])) {

            $this->view->records = $this->notes->getListSearch($where, $pagination['current'], $itemsOnPage, $_SESSION['NOTES_SEARCH_TAGS'], $order);
        } else {
            $this->view->records = $this->notes->getList($where, $pagination['current'], $itemsOnPage, $search, $order);
        }

        $this->view->setTemplate('page.tpl');
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');
        $footer = $this->view->render();

        $res['returl'] = $_SESSION['NOTES_RET_URL'];
        $res['footer'] = $footer;
        $res['html'] = $html;
        $res['count'] = $totalRec;
        $res['pag'] = $pagination;
        return $res;


    }

    function add()
    {

        $this->view->setTemplate('add.tpl');
        return $this->view;
    }

    function edit()
    {

        $id = $this->args[0];
        $this->view->notes = $this->notes->getByID($id);
        $this->view->setTemplate('edit.tpl');
        return $this->view;
    }

    function view()
    {

        $id = $_GET['id'];
        $note = $this->notes->getByID($id);
        $this->view->notes = $note;
        $this->view->setTemplate('view.tpl');
        $html = $this->view->render();
        $res['html'] = $html;
        $res['btnedit'] = app::$lang->common['btn_edit'];
        $res['btndel'] = app::$lang->common['btn_del'];
        $res['btnclose'] = app::$lang->common['btn_close'];
        if (!strlen($note['note_subj'])) {
            $res['title'] = app::$lang->notes['no_subj'];
        } else {
            $res['title'] = $note['note_subj'];
        }
        return $res;

    }

    function doAddNote()
    {
        $subj = app::strings_clear($_POST['note_subj']);


        $text = $_POST['note_text'];
        $add = array(
            "note_user_id" => $_SESSION['account']['user_id'],
            "note_text" => $text,
            "note_subj" => $subj,
            "note_tags" => $subj . " " . $text,
        );
        $this->notes->add($add);

        $res['status'] = true;
        return $res;
    }

    function doUpdate()
    {
        $subj = app::strings_clear($_POST['note_subj']);
        $text = $_POST['note_text'];
        $upd = array(
            "note_subj" => $subj,
            "note_text" => $text,
            "note_tags" => $subj . " " . $text
        );
        $this->notes->update($_POST['note_id'], $upd);
        //app::trace($upd);

        $res['status'] = true;

        return $res;
    }

    function del($id=0){
        if(!$id)$id=$_POST['id'];
        $upd['note_trash'] = 1;
        $this->notes->updateMass($id,$upd);
        $res['status'] = true;
        return $res;
    }

    function deleteBundle(){


        if(isset($_POST['row']) && count($_POST['row'])){
            // app::trace($_POST);
            foreach($_POST['row'] as $val){
                $this->del($val);
            }

            $res['status']=true;
            return $res;
        }else{
            $res['title']="Operation Failed";
            $res['status']=false;
            $res['msg']="No rows selected. Please select at least one template.";
            return $res;
        }
    }
}