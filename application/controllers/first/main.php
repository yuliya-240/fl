<?php      
namespace floctopus\controllers\first;

use \floctopus\application as app;     
use \jet\controller\http as HTTPController;
use \floctopus\models\logic\jetsession as Session;   
use \floctopus\models\orm\accounts as OrmAccounts;
use \floctopus\models\orm\settings as OrmSettings;
use \floctopus\models\orm\country as Countries; 
use \jet\language as lang;
use \jet\config as config;


class main extends HTTPController {
    
    function __before() {

    	
        $this->session = new Session();
        
        $this->session->refresh();
        if (!$this->session->isLogged() ) {
            \jet\redirect('/login/',true);
        }
        
        if(!$_SESSION['account']['user_first']){
	        
	       \jet\redirect('/',true); 
        }
    	$this->view = new \jet\twig();
        $this->view->setPath(app::$device.'/first');  
        $this->view->acs = $this->session->getData();   
        $this->skin=app::$device; 
        $this->view->skin = $this->skin; 
        $this->acc = new OrmAccounts();
        $this->set = new OrmSettings();
         
	}   
     
    function __default($args = false) {   
        
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

		switch ($lang){
		    case "ru":
		        
		        $lang = "ru";
		        break;
		    case "ua":
		        
		        $lang = "ua";
		        break;
		      
		    default:
		        
		        $lang = "en";
		        break;
		}
        
		app::$lang = new lang(app::$settings->lang_path._.$lang.'.lng');
	    $lng_first = app::$lang->first;
	    $this->view->lng_first=$lng_first;
	    $this->view->lng_common=app::$lang->common;
	    $this->view->page_title = $lng_first['title'];
	    $this->view->currlng = $lang;

	    $loc = app::ip_info();
		$country=new Countries;
        $this->view->countryList=$country->getCountriesList();
        
        $statesList = $country->getStateList($loc['country']);
		if($statesList) {
			$this->view->currState = $country->detectState($loc['state']);
		}else{
			
			$this->view->currState = $loc['state'];
		}
		$this->view->proffesionsList = $country->getProfList();
		
		$this->view->currCity = $loc['city'];
		$this->view->currCountry = $loc['country'];
		$this->view->statesList = $statesList;
        $this->view->currencyList = $country->getCurrencyList();
        $this->view->tzList = $country->getTZList();
        $this->view->d=time();
        $this->view->dnow = \date('Y-m-d H:i:s',\strtotime("now"));
		$this->view->location = $loc;
	    
	    $this->view->setTemplate('index.tpl');
        return $this->view;
    } 
    
    function setupacc(){
		$mf=$_POST['mf'];
		
		switch ($mf){
			
			case 1:
				$co_dsep='.';
				$co_tsep=',';
			break;	
			
			case 2:
				$co_dsep=',';
				$co_tsep='.';
			break;

			case 3:
				$co_dsep='.';
				$co_tsep=' ';
			break;
		}
		
		$name = \ucwords(app::strings_clear($_POST['user_name']));
		$username = \strtolower($_POST['user_username']);  
		$password = $_POST['user_password'];

		
		$update = array(
            'user_name' => $name,
            'user_username' => $username,
            'user_currency' => $_POST['user_currency'],
            'user_dformat' => $_POST['user_dformat'],
            'user_tformat' => $_POST['user_tformat'],
            'user_tz' => $_POST['user_tz'],
            'user_dsep' => $co_dsep,
            'user_tsep' => $co_tsep,
            'user_first' => 0,
            'user_wstart' => $_POST['user_wstart'],
            'user_proffession' => $_POST['user_profession'],
            'user_lang' => $_POST['user_lang']
        );
		$this->acc->update($_SESSION['account']['user_id'],$update);
		
		
		$this->acc->changePassword($_SESSION['account']['user_id'],$password);		

		$updSet['set_billing_name'] = $name;
		$this->set->updateByUserID($updSet);

		
		\setcookie("fllang",$_POST['user_lang'],time()+60*60*24*30 ,"/");
		 $this->session->refresh();
		$res['status'] = true;
		return $res;
    }

	function isusrnm(){

        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

		switch ($lang){
		    case "ru":
		        
		        $lang = "ru";
		        break;
		    case "ua":
		        
		        $lang = "ua";
		        break;
		      
		    default:
		        
		        $lang = "en";
		        break;
		}
        
        app::$lang = new config(app::$settings->lang_path._.$lang.'.lng');

		
		$uname = $_POST['user_username'];
		$isuname = $this->acc->isUnameExist($uname);	
		
		if($isuname){
			$res['status'] = false;
			$res['msg'] = app::$lang->first['user_exist'];
			return  'false';// app::$lang->first['user_exist'];
		}
		
		$res['status'] = true;
		return "true";
		
	}
    
    function logout(){
	

	    $this->session->logout();
	    \jet\redirect('/login',true);
    }
    
    function __format() {
        return array(
         
        );
    } 
    
}
