<?php      
namespace floctopus\controllers\vendors;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;  
use \floctopus\models\orm\vendors as OrmVendors;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	//$this->view->lng_invoices = app::$lang->invoices;
        $this->view->setPath(app::$device.'/vendors');   
    	$this->vendor = new OrmVendors();
	}   

    function vfilter(){
	    $res=array();
	    
	    $where['vendor_user_id'] = $_SESSION['account']['user_id'];
	    $where['vendor_trash'] = 0;
	    
	    $rz = $this->makeTags($_POST['query']);
		 
		$srch=$rz['str'];
	    
		$vendors = $this->vendor->getListSearch($where,1,21,array($srch),'vendor_name asc');
		
		    $res[0]['id']   = 0;
		    $res[0]['name'] = $_POST['query'];
	    
	    foreach($vendors as $key=>$val){
		   
		    $res[$key+1]['id']   = $val['vendor_id'];
		    $res[$key+1]['name'] = $val['vendor_name'];
	    }

	    
	    return $res;
    } 	
	
	
} 

