<?php
namespace floctopus\controllers\ppl;

use \floctopus\application as app;
use \floctopus\models\common\adminController as adminController;
use \floctopus\models\orm\portfolio as OrmPortfolio;
use \floctopus\models\orm\accounts as OrmAcc;
use \floctopus\models\orm\colloborators as OrmCollab;
use \floctopus\models\orm\misc as OrmMisc;
use \floctopus\models\orm\country as Countries;
use \floctopus\models\libs\pagination as pagination;


class main extends adminController {

    function __before() {
        parent::__before();
        $this->view->setPath(app::$device . '/ppl');
        $this->view->lng_profile = app::$lang->profile;
        $this->portfolio = new OrmPortfolio();
        $this->user = new OrmAcc();
        $this->collab = new OrmCollab();
        $this->misc = new OrmMisc();
        $this->country = new Countries();

    }

    function __default($args = false) {
        $id = $this->args[0];
        $user = $this->user->getAccount($id);
        //app::trace($user);
        $this->view->proff = $this->misc->getProfessionNameByID($user['user_proffession']);
        $this->view->country = $this->country->getCountryName($user['user_country']);
        $this->view->edu = $this->user->getEduList($id);
        $this->view->work = $this->user->getWorkList($id);
        $this->view->user = $user;
        $this->view->collabid = $id;
        $this->view->records = $this->portfolio->getAllList($id);
        $this->view->setTemplate('index.tpl');
        //app::trace($this->view);
        return $this->view;
    }


}