<?php      
namespace floctopus\controllers\invoices;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;  
use \floctopus\models\orm\accounts as OrmAccounts; 
use \floctopus\models\orm\invoicesrecurring as OrmInvoices;
//use \floctopus\models\orm\invoicesrecurring as OrmInvoicesrecurring;
use \floctopus\models\orm\services as OrmSrv;
use \floctopus\models\orm\clients as OrmClients;
use \floctopus\models\orm\calendar as OrmCalendar;
use \floctopus\models\orm\colloborators as OrmCollabs;
use \floctopus\models\orm\misc as OrmMisc;
use \floctopus\models\orm\transactions as OrmTrans;
use \floctopus\models\orm\country as OrmCountry; 
use \floctopus\models\orm\antispam as OrmAntispam; 
use \floctopus\models\orm\cron as OrmCron; 
use \floctopus\models\logic\validator as OrmValidator;
use \floctopus\models\libs\pagination as pagination;
use \floctopus\models\orm\projects as OrmProjects;
use \jet\language as lang;

class recurring extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_invoices = app::$lang->invoices;
        $this->view->setPath(app::$device.'/invoicesrec');   
    	$this->inv = new OrmInvoices();
    	//$this->invrec = new OrmInvoicesrecurring();
		$this->client = new OrmClients();
	    $this->collabs = new OrmCollabs();
		$this->misc = new OrmMisc();
		$this->trans = new OrmTrans();
		$this->country = new OrmCountry();	
		$this->prj = new OrmProjects();	
		if(!isset($_SESSION['INVREC_RET_URL']))$_SESSION['INVREC_RET_URL']="/invoices/recurring";
		if(!isset($_SESSION['INVREC_PAGE']))$_SESSION['INVREC_PAGE']=1;
		$this->view->sbMoney = "active";
		$this->view->subInvoicesRec = "active";
		$this->view->momentformat = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
		if(!isset($_SESSION['INVREC_LANG']))$_SESSION['INVREC_LANG'] = $_SESSION['account']['user_lang'];
		if(!isset($_SESSION['INVREC_SEARCH_STR']))$_SESSION['INVREC_SEARCH_STR']="";
        //app::trace($_SESSION['INVREC_ARC']);
	}   

    function __default($args = false) {   
 

	 	$this->view->searchstr = $_SESSION['INVREC_SEARCH_STR'];
		$this->view->fixed_footer = "am-sticky-footer";
        if(!isset($_SESSION['INVREC_PAGE']))$_SESSION['INVREC_PAGE']=1;
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
    function getContent(){
	    
    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['inv_trash']=0;
	    $where['inv_user_id']=$_SESSION['account']['user_id'];

    	if(isset($_GET['query'])){
	    	$r = $this->makeTags($_GET['query']);
		 
		    $srch=$r['str'];
		    if(strlen($srch)){
			    $_SESSION['INVREC_SEARCH_TAGS']=array($srch);
			    $_SESSION['INVREC_SEARCH_STR']=$srch;
			    
		    }else{
		    	$_SESSION['INVREC_SEARCH_TAGS']=NULL;
		    	$_SESSION['INVREC_SEARCH_STR']=NULL;
			    
		    }
    	}

    	

    	
     	if(isset($_SESSION['INVREC_SEARCH_STR']) ){
	    	
	    	$totalRec = $this->inv->getListCountSearch($where,$_SESSION['INVREC_SEARCH_TAGS']);
    	}else{
	    	
	    	$totalRec = $this->inv->getListCount($where,$search);
    	}

    	$page = $_SESSION['INVREC_PAGE'];
	    $itemsOnPage = $_SESSION["account"]['user_rop'];
	    
	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		$order ='inv_date ASC';

	    $this->view->totalCount=$totalRec;
		
	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
		
    	if(isset($_SESSION['INVREC_SEARCH_STR'])){
	    	
	    	$this->view->records = $this->inv->getListSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['INVREC_SEARCH_TAGS'],$order);
	    //	$this->view->totalPage = $this->inv->getTotalPageSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['INVREC_SEARCH_TAGS'],$order);
    	}else{
    		$this->view->records = $this->inv->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
    		//$this->view->totalPage = $this->inv->getTotalPage($where,$pagination['current'],$itemsOnPage,$search);
   		}
   		
   		//app::trace($this->view->totalPage);
	  //  app::trace($this->view->records);
	    
        $this->view->setTemplate('page.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;
        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
	    
    }
    
    function view(){
	    
	    if(!isset($this->args[0])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
	    }
	    
	    $id = $this->args[0];
	    
	    $invoice = $this->inv->getByID($id);
		
		if(!$invoice){
			
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}
		
		if($invoice['inv_client_id']>0){
			
			$recipient="cli";
			$recepientInfo = $this->client->getByID($invoice['inv_client_id']);
			$this->view->sendemail = $recepientInfo['client_email'];
			$this->view->sendemailcc = $recepientInfo['client_ccemail'];
			$this->view->postfix = $recepientInfo['client_id']."@cli";
		
		}else{
			$recepientInfo = $this->collabs->getByID($invoice['inv_collab_id']);
			$this->view->sendemail = $recepientInfo['user_email'];
			$this->view->sendemailcc = "";
			$this->view->postfix = $recepientInfo['cb_id']."@usr";
			$recipient ="collab";
		}
	
		

		$invlang = new lang(app::$settings->lang_path._.'inv.'.$invoice['inv_lang'].'.lng');
		$this->view->invlang = $invlang->details;
		
		
		$this->view->paymethod = $this->trans->getPaymentMethodsList();
	    $this->view->flist = $this->inv->getAttachList($id);

		$this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
		$selLines['invline_user_id'] = $_SESSION['account']['user_id'];
		$selLines['invline_inv_id'] = $id;
		$this->view->lines = $this->inv->getLinesList($selLines);

		$this->view->country = $this->country->getCountryName($invoice['inv_country']);
		$this->view->mycountry = $this->country->getCountryName($_SESSION['account']['set_billing_country']);
	


		$this->view->inv = $invoice;
        $this->view->setTemplate('view.tpl');  
        return $this->view;
    }
	
	function doUpdate(){
		
		$post = $_POST;
		
		$id = $this->updateInvoice($post);
		
		$res['id'] = $id;
		$res['status'] = true;
		return $res;
		
	}

	function updateInvoice($post=array()){
		$add = array();
    
		$str = $post['client'];
		list($cid,$at) = explode("@", $str);
		$id = $post['inv_id'];
		
		//$add['inv_num'] = $post['inv_num'];
		$add['inv_user_id'] = $_SESSION['account']['user_id'];
		$add['inv_name'] = $post['inv_name'];
		$add['inv_street'] = $post['inv_street'];
		$add['inv_city'] = $post['inv_city'];
		$add['inv_state'] = $post['inv_state'];
		$add['inv_zip'] = $post['inv_zip'];
		$add['inv_country'] = $post['inv_country'];
	//	$add['inv_date'] = \date("Y-m-d",strtotime($post['date']));
		
		if($at == "cli")$add['inv_client_id'] = $cid; else $add['inv_collab_id'] = $cid;
		
		$add['inv_subtotal'] = $post['subtotal'];
		$add['inv_taxes_cash'] = $post['tataltax_cash'];
		$add['inv_taxes_cash'] = $post['tataltax_cash'];
		$add['inv_discount_percentage'] = $post['discount'];		
		$add['inv_total'] = $post['total'];		
		$add['inv_paid_amount'] = $post['paid'];		
		$add['inv_total_due'] = $post['due'];		
		$add['inv_discount_cash'] = $post['discount_cash'];
		$add['inv_currency'] = $post['inv_currency'];
		$add['inv_prj'] = $post['inv_prj'];
		$add['inv_tags'] = $post['inv_name']." ".$post['total'];	
		$add['inv_total_base'] = $post['total_base'];
		$add['inv_total_due_base'] = $post['due_base'];
		$add['inv_exchange_rate'] = $post['inv_exchange_rate'];
		$add['inv_terms'] = app::strings_clear($post['inv_terms']);
		$add['inv_notes'] = app::strings_clear($post['inv_notes']);
		$add['inv_attach'] = 0;
		$add['inv_lang'] = $post['inv_lang'];
		$this->inv->update($id,$add);
		
		$this->inv->delLines($id);
		
		foreach($post['invrow_srv'] as $k=>$v){
			$addline = array();
		
			$addline['invline_user_id'] = $_SESSION['account']['user_id'];
			$addline['invline_inv_id']  = $id;
			$addline['invline_srv']  = app::strings_clear($v);
			$addline['invline_desc'] = app::strings_clear($post['invrow_desc'][$k]);
			$addline['invline_rate'] = $post['invrow_rate'][$k];
			$addline['invline_qty'] = $post['invrow_qty'][$k];
			$addline['invline_tax1_percent'] = $post['invrow_tax1'][$k];
			$addline['invline_tax1_cash'] = $post['invrow_tax1_cash'][$k];
			$addline['invline_tax2_percent'] = $post['invrow_tax2'][$k];
			$addline['invline_tax2_cash'] = $post['invrow_tax2_cash'][$k];
			$addline['invline_total'] = $post['invrow_total'][$k];
			
			$srvid = $this->handleService($addline);
			$addline['invline_srv_id'] = $srvid;
			$this->inv->addLine($addline);
		}
		
		$this->inv->delAttach($id);
		
		if(isset($post['attfile'])){
			
			foreach($post['attfile'] as $k=>$v){
				
				$addAtt['invatt_user_id']  = $_SESSION['account']['user_id'];
				$addAtt['invatt_inv_id'] = $id;
				$addAtt['invatt_fname'] = $v;
				$addAtt['invatt_type'] = $post['attfiletype'][$k];
				$this->inv->addAttach($addAtt);
			}
			
			$update['inv_attach'] = 1;
			$this->inv->update($id,$update);
		}
		
/*		$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['invhist_inv_id']  = $id;
		$addhistory['invhist_action']  = "update";
		$this->inv->addHistory($addhistory);*/
			
		$addRecurring['er_user_id'] = $_SESSION['account']['user_id'];
		$addRecurring['er_repeat'] = $post['er_repeat'];
		$addRecurring['er_date_start'] = \date("Y-m-d",strtotime($post['er_repeat']));
		
		if($post['er_repeat']=="w"){
			if(isset($post['er_week_day_mon']))$addRecurring['er_week_day_mon'] = 1;
			if(isset($post['er_week_day_tue']))$addRecurring['er_week_day_tue'] = 1;
			if(isset($post['er_week_day_wed']))$addRecurring['er_week_day_wed'] = 1;
			if(isset($post['er_week_day_thu']))$addRecurring['er_week_day_thu'] = 1;
			if(isset($post['er_week_day_fri']))$addRecurring['er_week_day_fri'] = 1;
			if(isset($post['er_week_day_sat']))$addRecurring['er_week_day_sat'] = 1;
			if(isset($post['er_week_day_sun']))$addRecurring['er_week_day_sun'] = 1;
		}

		if($post['er_repeat']=="m"){
			if(isset($post['er_month_day']))$addRecurring['er_month_day'] = $post['er_month_day'];
		}

		if($post['er_repeat']=="y"){
			if(isset($post['er_year_month']))$addRecurring['er_year_month'] = $post['er_year_month'];
			if(isset($post['er_year_day']))$addRecurring['er_year_day'] = $post['er_year_day'];
		}
		
		$addRecurring['er_end'] = $post['er_end'];
		
		if($post['er_end']==2){
			$addRecurring['er_end_date'] = \date("Y-m-d",strtotime($post['er_end_date']));
		}

		if($post['er_end']==3){
			$addRecurring['er_end_count'] = $post['er_end_count'];
		}

		$addRecurring['er_type'] = $post['er_type'];
		$addRecurring['er_invoice_id'] = $id;
		
		if(isset($post['er_send_auto'])){
			$addRecurring['er_send_auto'] = 1;
			$addRecurring['er_send_email'] = $post['er_send_email'];
		}
		
		$calObj = new OrmCalendar();
		
		$calObj->updRecurring($post['er_id'],$addRecurring);
		
		return $id;
	}
	
	function handleService($srv=array()){
		$srvobj = new OrmSrv();
		$where['srv_name'] = $srv['invline_srv'];
		$where['srv_user_id'] = $_SESSION['account']['user_id'];
		$where['srv_cost'] = $srv['invline_rate'];
		$ret = $srvobj->getInfo($where);
		
		if(!$ret){
		
			$add['srv_user_id'] = $srv['invline_user_id'];
			$add['srv_name'] = $srv['invline_srv'];
			$add['srv_desc'] = $srv['invline_desc'];
			$add['srv_cost'] = $srv['invline_rate'];
			$add['srv_tax1'] = $srv['invline_tax1_percent'];
			$add['srv_tax2'] = $srv['invline_tax2_percent'];
			$add['srv_tax_cash1'] = $srv['invline_tax1_cash'];
			$add['srv_tax_cash2'] = $srv['invline_tax2_cash'];
			$add['srv_currency'] = $_SESSION['account']['user_currency'];
			$id = $srvobj->add($add);
		}else{

			 
			 $id=$ret['srv_id'];
		}
		 
		return $id;
	}
	
	function doApplyFilter(){
		
		if(isset($_POST['invtypes'])){
			$_SESSION['INVREC_TYPE'] = array();
			$arr=array();
			foreach($_POST['invtypes'] as $k=>$v){
				
				$arr[]=$v;
				$_SESSION['INVREC_TYPE'] = $arr;
				
			}
		}
		
		$_SESSION['INVREC_ARC'] = $_POST['invarc'];
		
		$res['s'] = $_SESSION['INVREC_SEARCH_STR'];
		$res['status'] = true;
		return $res;
	}
	
	function delete($id=0){
		
		if(!$id)$id=$_POST['id'];
		
		$upd['inv_trash'] = 1;
		$this->inv->update($id,$upd);
		
		/*$updTrans['t_trash'] =1;
		$sel['t_inv_id'] =$id;
		$this->trans->updateMass($sel,$updTrans);*/
		
		$res['status'] = true;
		return $res;
		
	}
	
	function deleteBundle(){

	    
	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){
			    $this->delete($val);
		    }
		    
		    $res['status']=true;
		    return $res;
		    
		    
	    }else{
		    $res['title']="Operation Failed";
		    $res['status']=false;
		    $res['msg']="No rows selected. Please select at least one template.";
		    return $res;
	    }	    
		
		
	}
	
    function edit(){
	    
	    $rates = array();
	    if(!isset($this->args[0])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
	    }
	    $id = $this->args[0];
	    $inv = $this->inv->getByID($id);
	    
	    if(!$inv){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
	    }
	    
		$_SESSION['INVREC_LANG']  = $inv['inv_lang'];
		$invlang = new lang(app::$settings->lang_path._.'inv.'.$_SESSION['INVREC_LANG'].'.lng');

		$this->view->cinvlang = $_SESSION['INVREC_LANG'];
		$this->view->invlang = $invlang->details;

		$selPrj['project_user_id'] = $_SESSION['account']['user_id'];
		$selPrj['project_trash'] =0;
		$selPrj['project_default'] =0;
		$this->view->prj = $this->prj->getAllList($selPrj);
	    
	    $this->view->flist = $this->inv->getAttachList($id);
	    $this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
	    
	    $inv = $this->inv->getByID($id);
		$this->view->inv = $inv;
		
		$selLines['invline_user_id'] = $_SESSION['account']['user_id'];
		$selLines['invline_inv_id'] = $id;
		$this->view->lines = $this->inv->getLinesList($selLines);

		$allcurrencies = $this->misc->getCurrencies();

		foreach($allcurrencies as $k=>$v){
			$ra = $this->misc->getRateByCurrency($v['currency_iso'],$_SESSION['account']['user_currency']);
			$rates[$k]['curr'] = $v['currency_iso'];
			$rates[$k]['val'] = $ra['exr_rate'];
		}
		
		$this->view->rates = $rates;//$this->misc->getAllRates($_SESSION['account']['user_currency']);

	    $this->view->countryList=$this->country->getCountriesList();
	    $this->view->statesList=$this->country->getStateList($_SESSION['account']['user_country']);
		$this->view->langs = $this->misc->getAllLangs();
		$this->view->lng_clients = app::$lang->clients;
		$this->view->currencies = $allcurrencies;
		$this->view->months = $this->misc->getMonthsList();
	
		$selClients['client_collab'] = 0;
		$this->view->clients = $this->client->getAllList($selClients);
		$this->view->collabs = $this->collabs->getAllList(array(),array(),"users.user_name ASC");
		
        $this->view->setTemplate('edit.tpl');  
        return $this->view;
    }

	function changePage(){
		
		$_SESSION['INVREC_PAGE'] = $_POST['p'];
		
		$res['status'] = true;
		return $res;
	}

	function changeLang(){
		
		$lng = $_GET['lang'];
		
		$_SESSION['INVREC_LANG'] = $lng;
		$invlang = new lang(app::$settings->lang_path._.'inv.'.$_SESSION['INVREC_LANG'].'.lng');
		
		$res['lang'] = $invlang->details;
		$res['status'] = true;
		return $res;
		
	}



}
	
	
	
