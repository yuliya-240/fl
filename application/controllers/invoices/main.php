<?php      
namespace floctopus\controllers\invoices;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;  
use \floctopus\models\orm\accounts as OrmAccounts; 
use \floctopus\models\orm\invoices as OrmInvoices;
use \floctopus\models\orm\invoicesrecurring as OrmInvoicesrecurring;
use \floctopus\models\orm\services as OrmSrv;
use \floctopus\models\orm\clients as OrmClients;
use \floctopus\models\orm\calendar as OrmCalendar;
use \floctopus\models\orm\colloborators as OrmCollabs;
use \floctopus\models\orm\misc as OrmMisc;
use \floctopus\models\orm\transactions as OrmTrans;
use \floctopus\models\orm\country as OrmCountry; 
use \floctopus\models\orm\antispam as OrmAntispam; 
use \floctopus\models\orm\cron as OrmCron; 
use \floctopus\models\logic\validator as OrmValidator;
use \floctopus\models\libs\pagination as pagination;
use \floctopus\models\orm\projects as OrmProjects;
use \jet\language as lang;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_invoices = app::$lang->invoices;
    	 $this->view->lng_calendar = app::$lang->calendar;
        $this->view->setPath(app::$device.'/invoices');   
    	$this->inv = new OrmInvoices();
    	$this->invrec = new OrmInvoicesrecurring();
		$this->client = new OrmClients();
	    $this->collabs = new OrmCollabs();
		$this->misc = new OrmMisc();
		$this->trans = new OrmTrans();
		$this->country = new OrmCountry();	
		$this->prj = new OrmProjects();	
		if(!isset($_SESSION['INV_RET_URL']))$_SESSION['INV_RET_URL']="/invoices";
		if(!isset($_SESSION['INV_PAGE']))$_SESSION['INV_PAGE']=1;
		$this->view->sbMoney = "active";
		$this->view->subInvoices = "active";
		$this->view->momentformat = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
		if(!isset($_SESSION['INV_LANG']))$_SESSION['INV_LANG'] = $_SESSION['account']['user_lang'];
		if(!isset($_SESSION['INV_STARTDATE']))$_SESSION['INV_STARTDATE']="none";
		if(!isset($_SESSION['INV_ENDDATE']))$_SESSION['INV_ENDDATE']="none";
		if(!isset($_SESSION['INV_SEARCH_STR']))$_SESSION['INV_SEARCH_STR']="";
	    if(!isset($_SESSION['INV_TYPE'])){
		    $_SESSION['INV_TYPE'] = array(0);
    	}
    	
    	if(!isset($_SESSION['INV_ARC']))$_SESSION['INV_ARC']=0;
        //app::trace($_SESSION['INV_ARC']);
	}   

    function __default($args = false) {   
 
	 	$this->view->invstatuslist = $this->inv->getAllStatuses();
	 	$this->view->searchstr = $_SESSION['INV_SEARCH_STR'];
		$this->view->fixed_footer = "am-sticky-footer";
		$this->view->startdate = $_SESSION['INV_STARTDATE'];
		$this->view->enddate = $_SESSION['INV_ENDDATE'];
        if(!isset($_SESSION['INV_PAGE']))$_SESSION['INV_PAGE']=1;
    	$this->view->inv_arc = $_SESSION['INV_ARC'];
    	$this->view->inv_type = $_SESSION['INV_TYPE'];
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
    function getContent(){
	    
    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['inv_trash']=0;
	    $where['inv_user_id']=$_SESSION['account']['user_id'];

    	if(isset($_GET['query'])){
	    	$r = $this->makeTags($_GET['query']);
		 
		    $srch=$r['str'];
		    if(strlen($srch)){
			    $_SESSION['INV_SEARCH_TAGS']=array($srch);
			    $_SESSION['INV_SEARCH_STR']=$srch;
			    
		    }else{
		    	$_SESSION['INV_SEARCH_TAGS']=NULL;
		    	$_SESSION['INV_SEARCH_STR']=NULL;
			    
		    }
    	}

		if($_SESSION['INV_ARC']>0){
			if($_SESSION['INV_ARC']==1)$where['inv_arc']=1; else $where['inv_arc']=0; 
		}
    	
    	foreach($_SESSION['INV_TYPE'] as $i){
	    	
	    	if($i!=0)$where['inv_status'][]=$i;
    	}
    	
    	if($_SESSION['INV_STARTDATE']!="none"){
	    	
	    	$where['inv_date>']=$_SESSION['INV_STARTDATE'];
	    	$where['inv_date<']=$_SESSION['INV_ENDDATE'];
    	}

    	
     	if(isset($_SESSION['INV_SEARCH_STR']) ){
	    	
	    	$totalRec = $this->inv->getListCountSearch($where,$_SESSION['INV_SEARCH_TAGS']);
    	}else{
	    	
	    	$totalRec = $this->inv->getListCount($where,$search);
    	}

    	$page = $_SESSION['INV_PAGE'];
	    $itemsOnPage = $_SESSION["account"]['user_rop'];
	    
	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		$order ='inv_date ASC';

	    $this->view->totalCount=$totalRec;
		
	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
		
    	if(isset($_SESSION['INV_SEARCH_STR'])){
	    	
	    	$this->view->records = $this->inv->getListSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['INV_SEARCH_TAGS'],$order);
	    	$this->view->totalPage = $this->inv->getTotalPageSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['INV_SEARCH_TAGS'],$order);
    	}else{
    		$this->view->records = $this->inv->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
    		$this->view->totalPage = $this->inv->getTotalPage($where,$pagination['current'],$itemsOnPage,$search);
   		}
   		
   		//app::trace($this->view->totalPage);
	    
        $this->view->setTemplate('page.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;
        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
	    
    }

    function add($args = false) {   
		$rates = array();		
		if(isset($_GET['c']))$c=$_GET['c']; else $c=0;
		if(isset($_GET['u']))$c=$_GET['u']; else $u=0;
		
		$selPrj['project_user_id'] = $_SESSION['account']['user_id'];
		$selPrj['project_trash'] =0;
		$selPrj['project_default'] =0;
		$this->view->prj = $this->prj->getAllList($selPrj);

		$this->view->terms = $_SESSION['account']['set_inv_terms'];
		$this->view->notes = $_SESSION['account']['set_inv_notes'];
		
		$this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
		
		$allcurrencies = $this->misc->getCurrencies();

		foreach($allcurrencies as $k=>$v){
			$ra = $this->misc->getRateByCurrency($v['currency_iso'],$_SESSION['account']['user_currency']);
			$rates[$k]['curr'] = $v['currency_iso'];
			$rates[$k]['val'] = $ra['exr_rate'];
		}

		$_SESSION['INV_LANG']  = $_SESSION['account']['user_lang'];
		$invlang = new lang(app::$settings->lang_path._.'inv.'.$_SESSION['INV_LANG'].'.lng');

		$this->view->cinvlang = $_SESSION['INV_LANG'];
		$this->view->invlang = $invlang->details;
		
		$this->view->rates = $rates;//$this->misc->getAllRates($_SESSION['account']['user_currency']);
	    $this->view->countryList=$this->country->getCountriesList();
	    $this->view->statesList=$this->country->getStateList($_SESSION['account']['user_country']);
		$this->view->langs = $this->misc->getAllLangs();
		$this->view->lng_clients = app::$lang->clients;
		$this->view->currencies = $allcurrencies;
		$this->view->months = $this->misc->getMonthsList();
		$this->view->num = $this->inv->getLastNum();
		$selClients['client_collab'] = 0;
		$this->view->clients = $this->client->getAllList($selClients);
		
		$selClientsCollabs['cb_status']=3;
		$this->view->collabs = $this->collabs->getAllList($selClientsCollabs,array(),"users.user_name ASC");
		
		//app::trace($this->view->collabs);
		
		$this->view->selCollab = $u;
		$this->view->selClient = $c;

        $this->view->setTemplate('add.tpl');  
        return $this->view;
    } 
    
    function view(){
	    
	    if(!isset($this->args[0])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
	    }
	    
	    $id = $this->args[0];
	    
	    $invoice = $this->inv->getByID($id);
		
		if(!$invoice){
			
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}
		
		if($invoice['inv_client_id']>0){
			
			$recipient="cli";
			$recepientInfo = $this->client->getByID($invoice['inv_client_id']);
			$this->view->sendemail = $recepientInfo['client_email'];
			$this->view->sendemailcc = $recepientInfo['client_ccemail'];
			$this->view->postfix = $recepientInfo['client_id']."@cli";
		
		}else{
			$recepientInfo = $this->collabs->getByID($invoice['inv_collab_id']);
			$this->view->sendemail = $recepientInfo['user_email'];
			$this->view->sendemailcc = "";
			$this->view->postfix = $recepientInfo['cb_id']."@usr";
			$recipient ="collab";
		}
	
		
		// Total amount of all outstanding invoices
		$ar = array();
		$ar['inv_user_id']=$_SESSION['account']['user_id'];
		$ar['inv_status'][]=3;
		$ar['inv_status'][]=4;
		$ar['inv_status'][]=5;
		$ar['inv_id!']=$id;
		if($recipient=="cli"){ 
			$ar['inv_client_id']=$invoice['inv_client_id'];
		} else {
	   
			$ar['inv_collab_id']=$invoice['inv_collab_id'];
	    }
		$outstanding = $this->inv->getInvAmount($ar);		

		// Total amount of all paid invoices
		$ar = array();
		$ar['inv_user_id']=$_SESSION['account']['user_id'];
		$ar['inv_status'][]=3;
		$ar['inv_status'][]=2;

		$ar['inv_id!']=$id;
		if($recipient=="cli"){ 
			$ar['inv_client_id']=$invoice['inv_client_id'];
		} else {
	   
			$ar['inv_collab_id']=$invoice['inv_collab_id'];
	    }
		$paid= $this->inv->getInvPaidAmount($ar);		
		
		
		// PAST DUE
    	$selPD['inv_status']=4;
		if($recipient=="cli"){ 
			$ar['inv_client_id']=$invoice['inv_client_id'];
		} else {
	   
			$ar['inv_collab_id']=$invoice['inv_collab_id'];
	    }
    	$selPD['inv_user_id']=$_SESSION['account']['user_id'];
    	$pastdue = $this->inv->getInvAmount($selPD);

		$invlang = new lang(app::$settings->lang_path._.'inv.'.$invoice['inv_lang'].'.lng');
		$this->view->invlang = $invlang->details;
		
		
		$this->view->paymethod = $this->trans->getPaymentMethodsList();
	    $this->view->flist = $this->inv->getAttachList($id);

		$this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
		$selLines['invline_user_id'] = $_SESSION['account']['user_id'];
		$selLines['invline_inv_id'] = $id;
		$this->view->lines = $this->inv->getLinesList($selLines);

		$selHistory['invhist_user_id'] = $_SESSION['account']['user_id'];
		$selHistory['invhist_inv_id'] = $id;
		$this->view->history = $this->inv->getHistoryList($selHistory);
		
		
		$this->view->country = $this->country->getCountryName($invoice['inv_country']);
		$this->view->mycountry = $this->country->getCountryName($_SESSION['account']['set_billing_country']);
		$this->view->paid = $paid;
		$this->view->pastdue = $pastdue;
		$this->view->outstanding = $outstanding;
		$this->view->inv = $invoice;
        $this->view->setTemplate('view.tpl');  
        return $this->view;
    }
    
    function getNewLine(){
	    $this->view->id = uniqid();
        $this->view->setTemplate('newline.tpl');  
        $res['html'] = $this->view->render();
	    $res['status'] = true;
	    $res['id'] = $this->view->id;
	    return $res;
    }
	
	function doSafeDraft(){
		if(!isset($_POST['recurrance']))$_POST['recurrance']=0;
		
		$post = $_POST;
		
		$id = $this->createInvoice($post);
		
		$res['status'] = true;
		return $res;
	}
	
	function doEmailDraft(){
		if(!isset($_POST['recurrance']))$_POST['recurrance']=0;

		$valid = new OrmValidator();
		
		$email = strtolower($_POST['send_email']);		
		$emailcc = strtolower($_POST['send_email_cc']);		
		
		if(!$valid->email($email) || (strlen($emailcc) && !$valid->email($emailcc))){
			
			$res['status'] = false;
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->common['nve'];
			return $res;
			
		}

		$post = $_POST;
		
		$invid = $this->createInvoice($post);
		
		$as = new OrmAntispam();
		
			$Cron = new OrmCron();
			$add=array();
			
			$add['as_user_id'] = $_SESSION['account']['user_id'];
			$add['as_object'] = 'invoice';
			$add['as_object_id'] = $invid;
			$add['as_email'] = $email;
			$as->add($add);
			
			$addCron=array(
				"cronemail_user_id" => $_SESSION['account']['user_id'],
				"cronemail_object" => 'invoice',
				"cronemail_object_id" => $invid,
				"cronemail_temail_to" => $email
			);
			
			$Cron->addEmail($addCron);
			
			if(strlen($emailcc)){
				$add=array();
				$add['as_user_id'] = $_SESSION['account']['user_id'];
				$add['as_object'] = 'invoice';
				$add['as_object_id'] = $invid;
				$add['as_email'] = $emailcc;
				$as->add($add);

				$addCron=array(
					"cronemail_user_id" => $_SESSION['account']['user_id'],
					"cronemail_object" => 'invoice',
					"cronemail_object_id" => $invid,
					"cronemail_temail_to" => $emailcc
				);
				
				$Cron->addEmail($addCron);
	
				$addhistory['invhist_cc'] = $emailcc;
			}
		
			$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
			$addhistory['invhist_inv_id']  = $invid;
			$addhistory['invhist_action']  = "email";
			$addhistory['invhist_email']  = $email;
			$this->inv->addHistory($addhistory);
			
			$invoice = $this->inv->getByID($invid);
			
			if($invoice['inv_status']==1)$updInv['inv_status'] = 5;
			
			$updInv['inv_sent_email'] = 1;
			$this->inv->update($invid,$updInv);
			
			
			$res['title'] =app::$lang->invoices['sent_tit'];
			$res['msg'] =app::$lang->invoices['sent_txt'];
			$res['id'] = $invid;
			$res['status'] = true;
			return $res;
		
		
	}
	
	function doUpdate(){
		
		$post = $_POST;
		
		$id = $this->updateInvoice($post);
		
		$res['status'] = true;
		return $res;
		
	}
	
	function createInvoice($post=array()){
		$add = array();
    
		$str = $post['client'];
		list($cid,$at) = explode("@", $str);
		
		$add['inv_num'] = $post['inv_num'];
		$add['inv_user_id'] = $_SESSION['account']['user_id'];
		$add['inv_name'] = $post['inv_name'];
		$add['inv_street'] = $post['inv_street'];
		$add['inv_city'] = $post['inv_city'];
		$add['inv_state'] = $post['inv_state'];
		$add['inv_zip'] = $post['inv_zip'];
		$add['inv_country'] = $post['inv_country'];
		$add['inv_date'] = \date("Y-m-d",strtotime($post['date']));
		
		if($at == "cli")$add['inv_client_id'] = $cid; else $add['inv_collab_id'] = $cid;
		
		$add['inv_subtotal'] = $post['subtotal'];
		$add['inv_taxes_cash'] = $post['tataltax_cash'];
		$add['inv_taxes_cash'] = $post['tataltax_cash'];
		$add['inv_discount_percentage'] = $post['discount'];		
		$add['inv_total'] = $post['total'];		
		$add['inv_paid_amount'] = $post['paid'];		
		$add['inv_total_due'] = $post['due'];		
		$add['inv_discount_cash'] = $post['discount_cash'];
		$add['inv_currency'] = $post['inv_currency'];
		$add['inv_prj'] = $post['inv_prj'];
		$add['inv_tags'] = $post['inv_name']." ".$post['inv_num']." ".$post['total'];	
		$add['inv_total_base'] = $post['total_base'];
		$add['inv_total_due_base'] = $post['due_base'];
		$add['inv_exchange_rate'] = $post['inv_exchange_rate'];
		$add['inv_terms'] = app::strings_clear($post['inv_terms']);
		$add['inv_notes'] = app::strings_clear($post['inv_notes']);
		$add['inv_lang'] = $post['inv_lang'];
		$id = $this->inv->add($add);
		
		if(isset($post['recurrance']) &&  $post['recurrance']==1){
			
			$idrec = $this->invrec->add($add);
		}
			
		foreach($post['invrow_srv'] as $k=>$v){
			$addline = array();
		
			$addline['invline_user_id'] = $_SESSION['account']['user_id'];
			$addline['invline_inv_id']  = $id;
			$addline['invline_srv']  = app::strings_clear($v);
			$addline['invline_desc'] = app::strings_clear($post['invrow_desc'][$k]);
			$addline['invline_rate'] = $post['invrow_rate'][$k];
			$addline['invline_qty'] = $post['invrow_qty'][$k];
			$addline['invline_tax1_percent'] = $post['invrow_tax1'][$k];
			$addline['invline_tax1_cash'] = $post['invrow_tax1_cash'][$k];
			$addline['invline_tax2_percent'] = $post['invrow_tax2'][$k];
			$addline['invline_tax2_cash'] = $post['invrow_tax2_cash'][$k];
			$addline['invline_total'] = $post['invrow_total'][$k];
			
			$srvid = $this->handleService($addline);
			$addline['invline_srv_id'] = $srvid;
			$this->inv->addLine($addline);
			
			if(isset($post['recurrance']) &&  $post['recurrance']==1){
				$addline['invline_inv_id']  = $idrec;
				$idrec = $this->invrec->addLine($addline);
			}
		}
		
		if(isset($post['attfile'])){
			
			foreach($post['attfile'] as $k=>$v){
				
				$addAtt['invatt_user_id']  = $_SESSION['account']['user_id'];
				$addAtt['invatt_inv_id'] = $id;
				$addAtt['invatt_fname'] = $v;
				$addAtt['invatt_type'] = $post['attfiletype'][$k];
				$this->inv->addAttach($addAtt);
				
				if(isset($post['recurrance']) &&  $post['recurrance']==1){
					$addAtt['invatt_inv_id'] = $idrec;
					$idrec = $this->invrec-addAttach($addAtt);
				}
				
			}

			$update['inv_attach'] = 1;
			$this->inv->update($id,$update);
			if(isset($post['recurrance']) &&  $post['recurrance']==1){
				$this->invrec->update($idrec,$update);
			}

		}
		
		$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['invhist_inv_id']  = $id;
		$addhistory['invhist_action']  = "create";
		$this->inv->addHistory($addhistory);
		
		// GET Client or Collaborate Name
		
		if(isset($post['recurrance']) &&  $post['recurrance']==1){
			if($at == "cli"){
				
				$inv_name = $this->client->getNameByID($cid);
				$addRecurring['er_client_id'] = $cid;
			}else{
				$inv_name = $this->collabs->getNameByID($cid);
				$addRecurring['er_collab_id'] = $cid;
			}
					 
			
			$addr['er_user_id'] = $_SESSION['account']['user_id'];
			$addr['er_title'] = "Invoice for ".$inv_name;
			$addr['er_location'] = app::strings_clear($_POST['location']);
			$addr['er_type'] = $_POST['e_type'];
			$addr['er_freq'] = $_POST['er_repeat'];
			$addr['er_time'] = $_POST['deadline_time'];
			$addr['er_date_start'] = \date("Y-m-d",strtotime($_POST['er_date_start']));
			$addr['er_end'] = $_POST['er_end'];
			
			$startTS = strtotime($_POST['er_date_start']);
			
			if($_POST['er_end']==2){
				
				
				$endTS = strtotime($_POST['er_end_date']);
				
				if($startTS>$endTS){
					
					$res['status'] = false;
					$res['title'] = app::$lang->common['operation_failed'];
					$res['msg'] = app::$lang->common['stendt_err'];
					return $res;
				}
				
				$addr['er_end_date'] = \date("Y-m-d",strtotime($_POST['er_end_date']));
			}

			if($_POST['er_end']==3){
				if(!$_POST['er_end_count']){
					
					$res['status'] = false;
					$res['title'] = app::$lang->common['operation_failed'];
					$res['msg'] = app::$lang->common['no_count_err'];
					return $res;
					
				}
				
				$addr['er_end_count'] = $_POST['er_end_count'];
			}


			switch ($_POST['er_repeat']){
				
				case 'd':
					if(!isset($_POST['daily_days_count']) || !$_POST['daily_days_count'])$_POST['daily_days_count']=1;
					$addr['er_daily_option'] = $_POST['daily_option'];
					$addr['er_daily_days_count'] = $_POST['daily_days_count'];
				break;
				
				
				case 'w':
					if(!isset($_POST['weekly_count']) || !$_POST['weekly_count'])$_POST['weekly_count'] =1;
					$addr['er_weekly_count'] = $_POST['weekly_count'];
					foreach($_POST['weekday'] as $val){
						$addr['er_w'.$val]=1;
					}
					
				break;
				
				case 'm':
					if(!isset($_POST['month_count1']) || !$_POST['month_count1'])$_POST['month_count1']=1;
					if(!isset($_POST['month_day']) || !$_POST['month_day'])$_POST['month_day']=1;
					if(!isset($_POST['month_count2']) || !$_POST['month_count2'])$_POST['month_count2']=1;
					
					$addr['er_monthly_option'] = $_POST['monthly_option'];
					$addr['er_month_day'] = $_POST['month_day'];
					$addr['er_month_count1'] = $_POST['month_count1'];
					$addr['er_month_day_number'] = $_POST['month_day_number'];
					$addr['er_month_week_day'] = $_POST['month_week_day'];
					$addr['er_month_count2'] = $_POST['month_count2'];
				break;
				
				case 'y':
					if(!isset($_POST['year_day']) || !$_POST['year_day'])$_POST['year_day']=1;
					$addr['er_yarly_option'] = $_POST['yarly_option'];
					$addr['er_year_month1'] = $_POST['year_month1'];
					$addr['er_year_day'] = $_POST['year_day'];
					$addr['er_year_day_number'] = $_POST['year_day_number'];
					$addr['er_year_week_day'] = $_POST['year_week_day'];
					$addr['er_year_month2'] = $_POST['year_month2'];
					
				break;
				
			}
			

			
			$calObj = new OrmCalendar();
			
			$calObj->addRecurring($addr);
			
		}
		
		
		return $id;
	}

	function updateInvoice($post=array()){
		$add = array();
    
		$str = $post['client'];
		list($cid,$at) = explode("@", $str);
		$id = $post['inv_id'];
		
		$add['inv_num'] = $post['inv_num'];
		$add['inv_user_id'] = $_SESSION['account']['user_id'];
		$add['inv_name'] = $post['inv_name'];
		$add['inv_street'] = $post['inv_street'];
		$add['inv_city'] = $post['inv_city'];
		$add['inv_state'] = $post['inv_state'];
		$add['inv_zip'] = $post['inv_zip'];
		$add['inv_country'] = $post['inv_country'];
		$add['inv_date'] = \date("Y-m-d",strtotime($post['date']));
		
		if($at == "cli")$add['inv_client_id'] = $cid; else $add['inv_collab_id'] = $cid;
		
		$add['inv_subtotal'] = $post['subtotal'];
		$add['inv_taxes_cash'] = $post['tataltax_cash'];
		$add['inv_taxes_cash'] = $post['tataltax_cash'];
		$add['inv_discount_percentage'] = $post['discount'];		
		$add['inv_total'] = $post['total'];		
		$add['inv_paid_amount'] = $post['paid'];		
		$add['inv_total_due'] = $post['due'];		
		$add['inv_discount_cash'] = $post['discount_cash'];
		$add['inv_currency'] = $post['inv_currency'];
		$add['inv_prj'] = $post['inv_prj'];
		$add['inv_tags'] = $post['inv_name']." ".$post['inv_num']." ".$post['total'];	
		$add['inv_total_base'] = $post['total_base'];
		$add['inv_total_due_base'] = $post['due_base'];
		$add['inv_exchange_rate'] = $post['inv_exchange_rate'];
		$add['inv_terms'] = app::strings_clear($post['inv_terms']);
		$add['inv_notes'] = app::strings_clear($post['inv_notes']);
		$add['inv_attach'] = 0;
		$add['inv_lang'] = $post['inv_lang'];
		$this->inv->update($id,$add);
		
		$this->inv->delLines($id);
		
		foreach($post['invrow_srv'] as $k=>$v){
			$addline = array();
		
			$addline['invline_user_id'] = $_SESSION['account']['user_id'];
			$addline['invline_inv_id']  = $id;
			$addline['invline_srv']  = app::strings_clear($v);
			$addline['invline_desc'] = app::strings_clear($post['invrow_desc'][$k]);
			$addline['invline_rate'] = $post['invrow_rate'][$k];
			$addline['invline_qty'] = $post['invrow_qty'][$k];
			$addline['invline_tax1_percent'] = $post['invrow_tax1'][$k];
			$addline['invline_tax1_cash'] = $post['invrow_tax1_cash'][$k];
			$addline['invline_tax2_percent'] = $post['invrow_tax2'][$k];
			$addline['invline_tax2_cash'] = $post['invrow_tax2_cash'][$k];
			$addline['invline_total'] = $post['invrow_total'][$k];
			
			$srvid = $this->handleService($addline);
			$addline['invline_srv_id'] = $srvid;
			$this->inv->addLine($addline);
		}
		
		$this->inv->delAttach($id);
		
		if(isset($post['attfile'])){
			
			foreach($post['attfile'] as $k=>$v){
				
				$addAtt['invatt_user_id']  = $_SESSION['account']['user_id'];
				$addAtt['invatt_inv_id'] = $id;
				$addAtt['invatt_fname'] = $v;
				$addAtt['invatt_type'] = $post['attfiletype'][$k];
				$this->inv->addAttach($addAtt);
			}
			
			$update['inv_attach'] = 1;
			$this->inv->update($id,$update);

		}
		
		$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['invhist_inv_id']  = $id;
		$addhistory['invhist_action']  = "update";
		$this->inv->addHistory($addhistory);
		
		return $id;
	}
	
	function handleService($srv=array()){
		$srvobj = new OrmSrv();
		$where['srv_name'] = $srv['invline_srv'];
		$where['srv_user_id'] = $_SESSION['account']['user_id'];
		$where['srv_cost'] = $srv['invline_rate'];
		$ret = $srvobj->getInfo($where);
		
		if(!$ret){
		
			$add['srv_user_id'] = $srv['invline_user_id'];
			$add['srv_name'] = $srv['invline_srv'];
			$add['srv_desc'] = $srv['invline_desc'];
			$add['srv_cost'] = $srv['invline_rate'];
			$add['srv_tax1'] = $srv['invline_tax1_percent'];
			$add['srv_tax2'] = $srv['invline_tax2_percent'];
			$add['srv_tax_cash1'] = $srv['invline_tax1_cash'];
			$add['srv_tax_cash2'] = $srv['invline_tax2_cash'];
			$add['srv_currency'] = $_SESSION['account']['user_currency'];
			$id = $srvobj->add($add);
		}else{

			 
			 $id=$ret['srv_id'];
		}
		 
		return $id;
	}
	
	function getAddresstoInvoice(){
				
			$str = $_GET['id'];
			
			list($cid,$at) = explode("@", $str);
			
			if($at=="cli"){
	
		    	$customer = $this->client->getByID($cid);
		    	
		    	if ($customer){
			    		
			    	$html='';
			    	$html.='<strong>'.$customer['client_name'].'</strong><br>';
			    	$html.=$customer['client_street'].'<br>';
					$html.=$customer['client_city'].' '.$customer['client_state'].' '.$customer['client_zip'].'<br>';
					$html.=$customer['country_printable_name'].'<br>';
					$html.='<a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chAddr" >Edit Address</a>';
				

			    	$res['name'] = $customer['client_name'];
			    	$res['street'] = $customer['client_street'];
			    	$res['city'] = $customer['client_city'];
			    	$res['state'] = $customer['client_state'];
			    	$res['zip'] = $customer['client_zip'];
			    	$res['country'] = $customer['client_country'];
			    	
		    	}else{
			    	$html='';
		    	}
					
				$res['status'] = true;
				$res['html'] = $html;
				$res['id'] = $customer['client_id'];
				$res['email'] = $customer['client_email'];
				$res['discount'] = $customer['client_discount'];			
				
			}else{
				
		    	$customer = $this->collabs->getByID($cid);
		    	
		    	if ($customer){
			    		
			    	$html='';
			    	$html.='<strong>'.$customer['cb_billing_name'].'</strong> <br>';
			    	$html.=$customer['cb_billing_street'].'<br>';
					$html.=$customer['cb_billing_city'].' '.$customer['cb_billing_state'].' '.$customer['cb_billing_zip'].'<br>';
					$html.=$customer['country_printable_name'].'<br>';
					$html.='<a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chAddr" >Edit Address</a>';
			    	
			    	$res['name'] = $customer['cb_billing_name'];
			    	$res['street'] = $customer['cb_billing_street'];
			    	$res['city'] = $customer['cb_billing_city'];
			    	$res['state'] = $customer['cb_billing_state'];
			    	$res['zip'] = $customer['cb_billing_zip'];
			    	$res['country'] = $customer['cb_billing_country'];
			    	
		    	}else{
			    	$html='';
		    	}
					
				$res['status'] = true;
				$res['html'] = $html;
				$res['id'] = $customer['cb_id'];
				$res['email'] = $customer['user_email'];
				$res['discount'] = 0;			
			}
			    	    
			return $res;
		}	
		
	function getBillingAddr(){
		
			$str = $_GET['id'];
			list($cid,$at) = explode("@", $str);
			
			if($at=="cli"){
	
		    	$customer = $this->client->getByID($cid);
		    	
		    	if ($customer){
			    		

			    	$this->view->name = $customer['client_name'];
			    	$this->view->street = $customer['client_street'];
			    	$this->view->city = $customer['client_city'];
			    	$this->view->state = $customer['client_state'];
			    	$this->view->zip = $customer['client_zip'];
			    	$this->view->email = $customer['client_email'];
			    	$country = $customer['client_country'];
			    	
			    	
		    	}
			}else{
				
		    	$customer = $this->collabs->getByID($cid);
		    	
		    	if ($customer){
			    		
					$this->view->iscollab = 1;
			    	$this->view->name = $customer['cb_billing_name'];
			    	$this->view->street = $customer['cb_billing_street'];
			    	$this->view->city = $customer['cb_billing_city'];
			    	$this->view->state = $customer['cb_billing_state'];
			    	$this->view->zip = $customer['cb_billing_zip'];
			    	$this->view->email = $customer['user_email'];
			    	$country = $customer['cb_billing_country'];
			    	
		    	}
				
			}
			
			
			$this->view->id =$str;
		    $this->view->countryList=$this->country->getCountriesList();
		    $this->view->statesList=$this->country->getStateList($country);
			$this->view->country = $country;
			$this->view->setTemplate('billaddr.tpl');  
			$res['html'] = $this->view->render();
			$res['status'] = true;    	    
			return $res;
	}	

	function getMyBillingAddr(){
		    $this->view->countryList=$this->country->getCountriesList();
		    $this->view->statesList=$this->country->getStateList($_SESSION['account']['set_billing_country']);
			$this->view->country = $_SESSION['account']['set_billing_country'];
		
			$this->view->setTemplate('mybilladdr.tpl');  
			$res['html'] = $this->view->render();
			$res['status'] = true;    	    
			return $res;
		
	}

	function doChAddrView(){
		$str = $_POST['billid'];
		list($cid,$at) = explode("@", $str);
		
		if($at=="cli"){
			
			$upd['client_name'] = app::strings_clear($_POST['billing_name']);
			$upd['client_street'] = app::strings_clear($_POST['billing_street']);
			$upd['client_city'] = app::strings_clear($_POST['billing_city']);
			$upd['client_zip'] = app::strings_clear($_POST['billing_zip']);
			$upd['client_state'] = app::strings_clear($_POST['billing_state']);
			$upd['client_email'] = app::strings_clear($_POST['billing_email']);
			$upd['client_country'] = $_POST['billing_country'];
			$this->client->update($cid,$upd);
			
			$updInv['inv_name'] = $upd['client_name'];
			$updInv['inv_street'] = $upd['client_street'];
			$updInv['inv_city'] = $upd['client_city'];
			$updInv['inv_zip'] = $upd['client_zip'];
			$updInv['inv_state'] = $upd['client_state'];
			$updInv['inv_country'] = $upd['client_country'];
			$this->inv->update($_POST['invid'],$updInv);
			$country = $this->country->getCountryName($upd['client_country']);
			
	    	$html='';
	    	$html.='<span class="name">'.$upd['client_name'].'<span>';
	    	$html.='<span>'.$upd['client_street'].'</span>';
			$html.='<span>'.$upd['client_city'].' '.$upd['client_state'].' '.$upd['client_zip'].'</span>';
			$html.='<span>'.$country.'</span>';
			$html.=' <a href="#"  class="btn btn-sm " style="padding-right: 0;" id="chAddr" >Edit Address</a>';
			
		}else{
			
			$upd['cb_billing_name'] = app::strings_clear($_POST['billing_name']);
			$upd['cb_billing_street'] = app::strings_clear($_POST['billing_street']);
			$upd['cb_billing_city'] = app::strings_clear($_POST['billing_city']);
			$upd['cb_billing_zip'] = app::strings_clear($_POST['billing_zip']);
			$upd['cb_billing_state'] = app::strings_clear($_POST['billing_state']);
			$upd['cb_billing_country'] = $_POST['billing_country'];
			$this->collabs->update($cid,$upd);
			
			$country = $this->country->getCountryName($upd['cb_billing_country']);
			$updInv['inv_name'] = $upd['cb_billing_name'];
			$updInv['inv_street'] = $upd['cb_billing_street'];
			$updInv['inv_city'] = $upd['cb_billing_city'];
			$updInv['inv_zip'] = $upd['cb_billing_zip'];
			$updInv['inv_state'] = $upd['cb_billing_state'];
			$updInv['inv_country'] = $upd['cb_billing_country'];
			$this->inv->update($_POST['invid'],$updInv);
			
			if(strlen($upd['cb_billing_state']))$comma=', '; else $comma=' ';
	    	$html='';
	    	$html.='<span class="name">'.$upd['cb_billing_name'].'<span>';
	    	$html.='<span>'.$upd['cb_billing_street'].'</span>';
			$html.='<span>'.$upd['cb_billing_city'].$comma.$upd['cb_billing_state'].' '.$upd['cb_billing_zip'].'</span>';
			$html.='<span>'.$country.'</span>';
			$html.=' <a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chAddr" >Edit Address</a>';
		}
	
		$res['addr'] = $html;
	 
	
		return $res;
			
		
	}

	function doChMyAddrView(){

	    $acc = new  OrmAccounts();
	    
	    $upd['set_billing_name']= app::strings_clear($_POST['billing_name']);
	    $upd['set_billing_street']= app::strings_clear($_POST['billing_street']);
	    $upd['set_billing_city'] = app::strings_clear($_POST['billing_city']);
	    $upd['set_billing_state'] = app::strings_clear($_POST['billing_state']);
	    $upd['set_billing_zip'] = app::strings_clear($_POST['billing_zip']);
	    $upd['set_billing_country'] = app::strings_clear($_POST['billing_country']);
	    $acc->updateSettings($_SESSION['account']['user_id'],$upd);
	    
	    $this->session->refresh();

		$country = $this->country->getCountryName($upd['set_billing_country']);
		if(strlen($upd['set_billing_state']))$comma=', '; else $comma=' ';
		
    	$html='';
    	$html.='<span class="name">'.$upd['set_billing_name'].'<span>';
    	$html.='<span>'.$upd['set_billing_street'].'</span>';
		$html.='<span>'.$upd['set_billing_city'].$comma.$upd['set_billing_state'].' '.$upd['set_billing_zip'].'</span>';
		$html.='<span>'.$country.'</span>';
		$html.=' <a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chMyAddr" >Edit Address</a>';
	
		$res['addr'] = $html;
		return $res;
	}

	function doChAddr(){
    
	$str = $_POST['billid'];
	list($cid,$at) = explode("@", $str);
	
	if($at=="cli"){
		
		$upd['client_name'] = app::strings_clear($_POST['billing_name']);
		$upd['client_street'] = app::strings_clear($_POST['billing_street']);
		$upd['client_city'] = app::strings_clear($_POST['billing_city']);
		$upd['client_zip'] = app::strings_clear($_POST['billing_zip']);
		$upd['client_state'] = app::strings_clear($_POST['billing_state']);
		$upd['client_email'] = app::strings_clear($_POST['billing_email']);
		$upd['client_country'] = $_POST['billing_country'];
		$this->client->update($cid,$upd);

		
	}else{
		
		$upd['cb_billing_name'] = app::strings_clear($_POST['billing_name']);
		$upd['cb_billing_street'] = app::strings_clear($_POST['billing_street']);
		$upd['cb_billing_city'] = app::strings_clear($_POST['billing_city']);
		$upd['cb_billing_zip'] = app::strings_clear($_POST['billing_zip']);
		$upd['cb_billing_state'] = app::strings_clear($_POST['billing_state']);
		$upd['cb_billing_country'] = $_POST['billing_country'];
		$this->collabs->update($cid,$upd);

		
	}

    $selectbox='';

    
    $selCust['client_user_id'] = $_SESSION['account']['user_id'];
    $selCust['client_trash'] = 0;
    $selCust['client_collab'] = 0;
    $customersList = $this->client->getAllList($selCust);
    $selectbox.='<optgroup label="'.app::$lang->menu['clients'].'">';
    foreach($customersList as $v){
	    
	    if($v['client_id']==$cid && $at=="cli"){
		    $selectbox.='<option selected value="'.$v['client_id'].'@cli">'.$v['client_name'].'</option>';
			    
	    }else{
		     $selectbox.='<option value="'.$v['client_id'].'@cli">'.$v['client_name'].'</option>';
	    }
	    
    }
    $selectbox.='</optgroup>';
    
    
    $collabs = $this->collabs->getAllList(array(),"users.user_name ASC");
    
    if($collabs){
	    $selectbox.='<optgroup label="'.app::$lang->menu['colleague'].'">';
	    foreach($collabs as $v){

	    if($v['cb_id']==$cid && $at=="usr"){
		    $selectbox.='<option selected value="'.$v['cb_id'].'@usr">'.$v['cb_billing_name'].'</option>';
			    
	    }else{
		    $selectbox.='<option value="'.$v['cb_id'].'@usr">'.$v['cb_billing_name'].'</option>';
	    }
		    
	    }
	    $selectbox.='</optgroup>';
	    
    }

	
	$res['addr'] = $html;
    $res['selBox'] = $selectbox;
	$res['id'] = $str;
	return $res;
	}
	
	function changeDateRange(){
		
		$_SESSION['INV_STARTDATE'] = $_POST['start'];
		$_SESSION['INV_ENDDATE'] = $_POST['end'];
		
		$res['s'] = $_SESSION['INV_SEARCH_STR'];
		$res['status'] = true;
		return $res;
	}
	
	function doApplyFilter(){
		
		if(isset($_POST['invtypes'])){
			$_SESSION['INV_TYPE'] = array();
			$arr=array();
			foreach($_POST['invtypes'] as $k=>$v){
				
				$arr[]=$v;
				$_SESSION['INV_TYPE'] = $arr;
				
			}
		}
		
		$_SESSION['INV_ARC'] = $_POST['invarc'];
		
		$res['s'] = $_SESSION['INV_SEARCH_STR'];
		$res['status'] = true;
		return $res;
	}
	
	function delete($id=0){
		
		if(!$id)$id=$_POST['id'];
		
		$upd['inv_trash'] = 1;
		$this->inv->update($id,$upd);
		
		$updTrans['t_trash'] =1;
		$sel['t_inv_id'] =$id;
		$this->trans->updateMass($sel,$updTrans);
		
		$res['status'] = true;
		return $res;
		
	}
	
	function deleteBundle(){

	    
	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){
			    $this->delete($val);
		    }
		    
		    $res['status']=true;
		    return $res;
		    
		    
	    }else{
		    $res['title']="Operation Failed";
		    $res['status']=false;
		    $res['msg']="No rows selected. Please select at least one template.";
		    return $res;
	    }	    
		
		
	}
	
    function edit(){
	    $rates = array();
	    if(!isset($this->args[0])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
	    }
	    $id = $this->args[0];
	    $inv = $this->inv->getByID($id);
	    
	    if(!$inv){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
	    }
	    
		$_SESSION['INV_LANG']  = $inv['inv_lang'];
		$invlang = new lang(app::$settings->lang_path._.'inv.'.$_SESSION['INV_LANG'].'.lng');

		$this->view->cinvlang = $_SESSION['INV_LANG'];
		$this->view->invlang = $invlang->details;

		$selPrj['project_user_id'] = $_SESSION['account']['user_id'];
		$selPrj['project_trash'] =0;
		$selPrj['project_default'] =0;
		$this->view->prj = $this->prj->getAllList($selPrj);
	    
	    $this->view->flist = $this->inv->getAttachList($id);
	    $this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
	    
	    $inv = $this->inv->getByID($id);
		$this->view->inv = $inv;
		
		$selLines['invline_user_id'] = $_SESSION['account']['user_id'];
		$selLines['invline_inv_id'] = $id;
		$this->view->lines = $this->inv->getLinesList($selLines);

		$allcurrencies = $this->misc->getCurrencies();

		foreach($allcurrencies as $k=>$v){
			$ra = $this->misc->getRateByCurrency($v['currency_iso'],$_SESSION['account']['user_currency']);
			$rates[$k]['curr'] = $v['currency_iso'];
			$rates[$k]['val'] = $ra['exr_rate'];
		}
		
		$this->view->rates = $rates;//$this->misc->getAllRates($_SESSION['account']['user_currency']);

	    $this->view->countryList=$this->country->getCountriesList();
	    $this->view->statesList=$this->country->getStateList($_SESSION['account']['user_country']);
		$this->view->langs = $this->misc->getAllLangs();
		$this->view->lng_clients = app::$lang->clients;
		$this->view->currencies = $allcurrencies;
		$this->view->months = $this->misc->getMonthsList();
	
		$selClients['client_collab'] = 0;
		$this->view->clients = $this->client->getAllList($selClients);
		$this->view->collabs = $this->collabs->getAllList(array(),"users.user_name ASC");
		
        $this->view->setTemplate('edit.tpl');  
        return $this->view;
    }
    
	function copy(){
		
		if(!isset($this->args[0]) || (!$this->args[0])){
			 \jet\redirect('/invoices',true); 
		}
		
		$invoiceID = $this->copyInvoice($this->args[0]);
		
		//Get New Invoice
		
		
		\jet\redirect('/invoices/view/'.$invoiceID,true);
	}

	function copyInvoice($id=0){
		/***
			Get OLD invoice
		*/
		
		$invoiceOldID = $id;
		
		$invoice = $this->inv->getByID($invoiceOldID);
		
		if(!$invoice) return false;
		
	    $flist = $this->inv->getAttachList($id);
	
		$selLines['invline_user_id'] = $_SESSION['account']['user_id'];
		$selLines['invline_inv_id'] = $id;
		$lines = $this->inv->getLinesList($selLines);
		
		unset($invoice['inv_id']);
		$invoiceNew = $invoice;
		
		// Create New INvoice
				
		$newNum = $this->inv->getLastNum();
		$invoiceNew['inv_num'] = $newNum;
		$invoiceNew['inv_date'] =  \date('Y-m-d');
		$invoiceNew['inv_paid_amount'] = 0;
		$invoiceNew['inv_status'] = 1;
		$invoiceNew['inv_paid_amount_base'] = 0;
		$invoiceNew['inv_total_due'] = $invoice['inv_total'];
		$invoiceNew['inv_total_due_base'] = $invoice['inv_total_base'];
		$invoiceNew['inv_sent_email']=0;
		$invoiceNew['inv_view']=0;
		$invoiceNew['inv_arc']=0;
		$invoiceNew['inv_prj']=0;
		$invoiceNew['inv_paid_amount']	= 0;
		$invoiceNew['inv_recurring'] = 0;
		$invoiceNew['inv_tags']	= $invoice['inv_name']." ".$newNum." ".$invoice['inv_total'];
		
		$invoiceID = $this->inv->add($invoiceNew);
		
		foreach($lines as $val){
			
			$val['invline_inv_id'] = $invoiceID;
			unset($val['invline_id']);
			$this->inv->addLine($val);
			
		}

		
		if($flist){
			
			foreach($flist as $v){
				
				$v['invatt_inv_id'] = $invoiceID;
				unset($v['invatt_id']);
				$this->inv->addAttach($v);
				
			}

		}
		
		$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['invhist_inv_id']  = $invoiceID;
		$addhistory['invhist_action']  = "create";
		$this->inv->addHistory($addhistory);
			
		return $invoiceID;
		
	}
	
	function doarc(){
		$id = $_POST['id'];
		$act = $_POST['act'];
		$updInvoice['inv_arc'] = $act;
		$this->inv->update($id, $updInvoice);	
		
		$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['invhist_inv_id']  = $id;
		if($act==1)$addhistory['invhist_action']  = "arc";else $addhistory['invhist_action']  = "unarc";
		$this->inv->addHistory($addhistory);

		$res['status'] = true;
		return $res;	
		
		
	}
	
	function doEmail(){
		$valid = new OrmValidator();
		
		$email = strtolower($_POST['send_email']);		
		$emailcc = strtolower($_POST['send_email_cc']);		
		$invid = $_POST['email_inv_id'];
		
		if(!$valid->email($email) || (strlen($emailcc) && !$valid->email($emailcc))){
			
			$res['status'] = false;
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->common['nve'];
			return $res;
			
		}
		
		$as = new OrmAntispam();
		$selAS = $as->getLates($email,'invoice',$invid);
		if(!$selAS){
			$Cron = new OrmCron();
			$add=array();
			$add['as_user_id'] = $_SESSION['account']['user_id'];
			$add['as_object'] = 'invoice';
			$add['as_object_id'] = $invid;
			$add['as_email'] = $email;
			$as->add($add);
			
			$addCron=array(
				"cronemail_user_id" => $_SESSION['account']['user_id'],
				"cronemail_object" => 'invoice',
				"cronemail_object_id" => $invid,
				"cronemail_temail_to" => $email
			);
			
			$Cron->addEmail($addCron);
			
			if(strlen($emailcc)){
				$add=array();
				$add['as_user_id'] = $_SESSION['account']['user_id'];
				$add['as_object'] = 'invoice';
				$add['as_object_id'] = $invid;
				$add['as_email'] = $emailcc;
				$as->add($add);

				$addCron=array(
					"cronemail_user_id" => $_SESSION['account']['user_id'],
					"cronemail_object" => 'invoice',
					"cronemail_object_id" => $invid,
					"cronemail_temail_to" => $emailcc
				);
				
				$Cron->addEmail($addCron);
	
				$addhistory['invhist_cc'] = $emailcc;
			}
		
			$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
			$addhistory['invhist_inv_id']  = $invid;
			$addhistory['invhist_action']  = "email";
			$addhistory['invhist_email']  = $email;
			$this->inv->addHistory($addhistory);
			
			$invoice = $this->inv->getByID($invid);
			
			if($invoice['inv_status']==1)$updInv['inv_status'] = 5;
			
			$updInv['inv_sent_email'] = 1;
			$this->inv->update($invid,$updInv);
			
			
			$res['title'] =app::$lang->invoices['sent_tit'];
			$res['msg'] =app::$lang->invoices['sent_txt'];
			$res['id'] = $invid;
			$res['status'] = true;
			return $res;

		}else{
			
			
			$res['status'] = false;
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->common['spam_msg'];
			return $res;
		}
		
		
		
	}
	
	function payBundle(){
		$_SESSION['INV_PAY_IDS'] = array();
	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){
			    $_SESSION['INV_PAY_IDS'][] = $val;
		    }
		    
		    $res['url'] = "/invoices/bulkPayments";
		    $res['status']=true;
		    return $res;
		    
		    
	    }else{
		    $res['title']="Operation Failed";
		    $res['status']=false;
		    $res['msg']="No rows selected. Please select at least one template.";
		    return $res;
	    }	    
	}
	
	function bulkPayments(){
		
		if(!isset( $_SESSION['INV_PAY_IDS'])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}

		$where['inv_id'] = $_SESSION['INV_PAY_IDS'];
		$where['inv_user_id'] = $_SESSION['account']['user_id'];
		$where['inv_status!']=2;
		
		
		$this->view->paymethod = $this->trans->getPaymentMethodsList();
		$this->view->invoices = $this->inv->getAllList($where);
		//unset($_SESSION['INV_PAY_IDS']);
        $this->view->setTemplate('bulkpay.tpl');  
        return $this->view;
		
		
	}
	
	function sendBundle(){
		
		$errarr = array();
		$valid = new OrmValidator();
		$res['status'] = true;  
		$i=0; 
		$as = new OrmAntispam();
		$Cron = new OrmCron();
		
	    foreach($_POST['row'] as $val){
		   $inv = $this->inv->getByID($val);
		   
		   if($inv['inv_client_id']>0){
			   
			   $recepientInfo = $this->client->getByID($inv['inv_client_id']);
			   
			   $email = $recepientInfo['client_email'];
			   $emailcc = $recepientInfo['client_ccemail'];
			   
			   if(!$valid->email($email) || (strlen($emailcc) && !$valid->email($emailcc)) ){
			   		$res['status'] = false; 
			   		$errarr[$i]['id'] = $inv['inv_id'];
			   		$errarr[$i]['num'] = $inv['inv_num'];
			   		$errarr[$i]['name'] = $recepientInfo['client_name'];
			   		$errarr[$i]['msg'] = app::$lang->invoices['neip'];
			   		$i++;
			   }else{
					$selAS = $as->getLates($email,'invoice',$inv['inv_id']);
					if(!$selAS){   
						$addAs = array();
						$addAs['as_user_id'] = $_SESSION['account']['user_id'];
						$addAs['as_object'] = 'invoice';
						$addAs['as_object_id'] = $inv['inv_id'];
						$addAs['as_email'] = $email;
						$as->add($addAs);


						$addCron=array(
							"cronemail_user_id" => $_SESSION['account']['user_id'],
							"cronemail_object" => 'invoice',
							"cronemail_object_id" => $inv['inv_id'],
							"cronemail_temail_to" => $email
						);
						$Cron->addEmail($addCron);
		
						if(strlen($emailcc)){
							$addCron=array(
								"cronemail_user_id" => $_SESSION['account']['user_id'],
								"cronemail_object" => 'invoice',
								"cronemail_object_id" => $inv['inv_id'],
								"cronemail_temail_to" => $emailcc
							);
							
							$Cron->addEmail($addCron);
							$addhistory['invhist_cc'] = $emailcc;
						}
						
						$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
						$addhistory['invhist_inv_id']  = $inv['inv_id'];
						$addhistory['invhist_action']  = "email";
						$addhistory['invhist_email']  = $email;
						$this->inv->addHistory($addhistory);
						
						
						
						if($inv['inv_status']==1)$updInv['inv_status'] = 5;
						
						$updInv['inv_sent_email'] = 1;
						$this->inv->update($inv['inv_id'],$updInv);
							
					
					}	else{
						
				   		$res['status'] = false; 
				   		$errarr[$i]['id'] = $inv['inv_id'];
				   		$errarr[$i]['num'] = $inv['inv_num'];
				   		$errarr[$i]['name'] = $recepientInfo['client_name'];
				   		$errarr[$i]['msg'] = app::$lang->common['spam_msg'];
				   		$i++;
						
						
					}			   
				   
			   }
			   
		   }else{
			   
			   $recepientInfo = $this->collabs->getByID($inv['inv_collab_id']);

			   $email = $recepientInfo['user_email'];
			   if(!$valid->email($email)){
			   		$res['status'] = false; 
			   		$errarr[$i]['id'] = $inv['inv_id'];
			   		$errarr[$i]['num'] = $inv['inv_num'];
			   		$errarr[$i]['name'] = $recepientInfo['cb_billing_name'];
			   		$errarr[$i]['msg'] = app::$lang->common['neip'];
			   		$i++;
			   }else{
					$selAS = $as->getLates($email,'invoice',$inv['inv_id']);
					if(!$selAS){   
						$addAs = array();
						$addAs['as_user_id'] = $_SESSION['account']['user_id'];
						$addAs['as_object'] = 'invoice';
						$addAs['as_object_id'] = $inv['inv_id'];
						$addAs['as_email'] = $email;
						$as->add($addAs);
			   
						$addCron=array(
							"cronemail_user_id" => $_SESSION['account']['user_id'],
							"cronemail_object" => 'invoice',
							"cronemail_object_id" => $inv['inv_id'],
							"cronemail_temail_to" => $email
						);
						
						$Cron->addEmail($addCron);
						
						$addhistory['invhist_user_id']  = $_SESSION['account']['user_id'];
						$addhistory['invhist_inv_id']  = $inv['inv_id'];
						$addhistory['invhist_action']  = "email";
						$addhistory['invhist_email']  = $email;
						$this->inv->addHistory($addhistory);
						
						
						
						if($inv['inv_status']==1)$updInv['inv_status'] = 5;
						
						$updInv['inv_sent_email'] = 1;
						$this->inv->update($inv['inv_id'],$updInv);
	
				   }else{
						
				   		$res['status'] = false; 
				   		$errarr[$i]['id'] = $inv['inv_id'];
				   		$errarr[$i]['num'] = $inv['inv_num'];
				   		$errarr[$i]['name'] = $recepientInfo['cb_billing_name'];
				   		$errarr[$i]['msg'] = app::$lang->common['spam_msg'];
				   		$i++;
						
						
					}	
				   
			   }

		   }
		   
	    }
	    
	    if(!$res['status']){
		    $html ="";
		    $this->view->err = $errarr;
		    $this->view->setTemplate('errbulkemail.tpl'); 
		    $html = $this->view->render();
			$res['html'] = $html;
		    
	    }else{
			$res['title'] =app::$lang->invoices['sent_tit'];
			$res['msg'] =app::$lang->invoices['sent_txt'];
	    }
		
	    return $res;
		
	}
	
	function arcBundle(){
	    $where['inv_id'] = $_POST['row'];
	    $where['inv_user_id'] = $_SESSION['account']['user_id'];
	    $upd['inv_arc'] = $_POST['act'];
	    $this->inv->updateMass($where,$upd);
	    
	    $res['status']=true;
	    return $res;
	}

	function changePage(){
		
		$_SESSION['INV_PAGE'] = $_POST['p'];
		
		$res['status'] = true;
		return $res;
	}
	
	function getEmailsForSend(){
		
		$str = $_GET['id'];
		list($cid,$at) = explode("@", $str);
		
		if($at=="cli"){

	    	$customer = $this->client->getByID($cid);
	    	
	    	if ($customer){
		    	$res['email'] = $customer['client_email'];
		    	$res['emailcc'] = $customer['client_ccemail'];
	    	}
		}else{
			
	    	$customer = $this->collabs->getByID($cid);
	    	
	    	if ($customer){

		    	$res['email'] = $customer['user_email'];;
		    	$res['emailcc'] = "";
		    	
	    	}
			
		}
		
		$res['status'] = true;
		return $res;
	}

	function changeLang(){
		
		$lng = $_GET['lang'];
		
		$_SESSION['INV_LANG'] = $lng;
		$invlang = new lang(app::$settings->lang_path._.'inv.'.$_SESSION['INV_LANG'].'.lng');
		
		$res['lang'] = $invlang->details;
		$res['status'] = true;
		return $res;
		
	}

}
	
	
	
