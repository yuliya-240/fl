<?php      
namespace floctopus\controllers\projects;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;   
use \floctopus\models\orm\colloborators as OrmCollab;
use \floctopus\models\orm\projects as OrmProjects;
use \floctopus\models\orm\clients as OrmClients;
use \floctopus\models\orm\issues as OrmIssues;
use \floctopus\models\orm\accounts as OrmAccounts;
use \floctopus\models\orm\calendar as OrmCalendar;
use \floctopus\models\orm\country as OrmCountry; 
use \floctopus\models\logic\jetMail as jmail; 
use \floctopus\models\libs\pagination as pagination;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_projects = app::$lang->projects;
        $this->view->setPath(app::$device.'/projects');   
		$this->cal = new OrmCalendar();
    	$this->collab = new OrmCollab();
    	$this->country = new OrmCountry();	
    	$this->client = new OrmClients();
    	$this->prj = new OrmProjects();
		if(!isset($_SESSION['PRJ_RET_URL']))$_SESSION['PRJ_RET_URL']="/projects";
		$this->view->sbProjects = "active";
		$this->view->subPrj = "active";
		$this->acc = new OrmAccounts();

	}   

    function __default($args = false) {   
		
		$this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
	function getContent(){
		
		if(isset($_GET['p']) && is_numeric ($_GET['p'])){
			$_SESSION['PRJ_RET_URL'] = "/projects/?p=".$_GET['p'];
		}
		
    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['project_trash']=0;
	    $where['project_default']=0;
	    //$coll = $this->prj->getCollabIDS();
	    $prjids = $this->prj->getPrjByUserID();
	   // array_push($coll,$_SESSION['account']['user_id']);
	    
	    //app::trace($prjids);
	    
	    //app::trace($coll);
	    $where['project_id'] = $prjids;
	    $totalRec = $this->prj->getListCount($where,$search);
    	$page = isset($_GET['p']) && $_GET['p']  ? \abs((int)$_GET['p']) :1;
	    $itemsOnPage = isset($_GET['iop']) && $_GET['iop']  ? \abs((int)$_GET['iop']) : $_SESSION["iop"];

	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		
		$order ='project_id ASC';
	    
	    $this->view->totalCount=$this->prj->getListCount();

	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
 
    	$this->view->records = $this->prj->getList($where,$pagination['current'],$itemsOnPage,$search,$order);

        $this->view->setTemplate('page.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;
        $res['returl'] = $_SESSION['PRJ_RET_URL'];
        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
	} 
	
	function add(){
		$this->view->dpformat = app::dateFormatToDatePicker($_SESSION['account']['user_dformat']);
		
		$selClients['client_collab'] = 0;
		$this->view->clients = $this->client->getAllList($selClients);
		
		$selClientsCollabs['cb_status']=3;
		$this->view->clientcollabs = $this->collab->getAllList($selClientsCollabs,array(),"users.user_name ASC");
		
		$sel['cb_status']=3;
		$this->view->collabs = $this->collab->getListCount($sel);
		
	    $this->view->countryList=$this->country->getCountriesList();
	    $this->view->statesList=$this->country->getStateList($_SESSION['account']['user_country']);
	
		$this->view->lng_clients = app::$lang->clients;
		
		
        $this->view->setTemplate('add.tpl');  
        return $this->view;
	}  
	
	function doAdd(){
		
		$name = app::strings_clear($_POST['project_name']);
		$desc = \trim($_POST['project_desc']);
		$client = $_POST['client'];
		
		$str = $_POST['client'];
		if($str!="0"){
			list($cid,$at) = explode("@", $str);
			if($at == "cli")$add['project_client_id'] = $cid; else $add['project_collab_id'] = $cid;
			
		}
		$add['project_name'] = $name;
		$add['project_desc'] = $desc;
		
			
		$id = $this->prj->add($add);
		
		if(isset($_POST['milestone'])){
			
			foreach($_POST['milestone'] as $k=>$v){
				
				$addm['prjm_name'] = app::strings_clear($v);
				
				if($_SESSION['account']['user_dformat']=="d/m/Y"){
					list($day,$month,$year) = explode('/',$_POST['deadline'][$k]);
					$deadline = $year."-".$month."-".$day;
				}else{
					$deadline = $_POST['deadline'][$k];
				}
				$addm['prjm_deadline'] = \date("Y-m-d",strtotime($deadline));
				$idMilestone = $this->prj->addMilestone($addm,$id);
				
				$addCal['e_date'] = \date("Y-m-d H:i:s",strtotime($deadline));
				$addCal['e_user_id'] = $_SESSION['account']['user_id'];
				$addCal['e_type'] = 2;
				$addCal['e_title'] = $addm['prjm_name'];
				$addCal['e_object_id'] = $idMilestone;
				$this->cal->add($addCal);
				
				$addm=array();
				
			}
			
		}
		
		
		// Add owner
		$this->prj->addCollab($_SESSION['account']['user_id'],$id,1);
		
		
		// Add ppl involved in project if exsits
		if(isset($_POST['prju_collaborator_id'])){
			
			foreach($_POST['prju_collaborator_id'] as $k=>$v){
				
				if($v>0){
					$role = $_POST['prju_role'][$k];
					$this->prj->addCollab($v,$id,$role);
					
			        $addn['n_user_id'] = $v;
			        $addn['n_src'] = 1;
			        $addn['n_type'] = "prj_add";
			        $addn['n_link'] = "/projects/view/".$id;
					$this->notifications->add($addn);
				}
			}
			
		}
		
		$res['status'] = true;
		return $res;
	}
	
	function getMilestoneBox(){
		$this->view->uniq = uniqid();
         if(isset($_GET['src'])){
	        $this->view->setTemplate('mileston-box-edit.tpl');
        }else{
	        $this->view->setTemplate('mileston-box.tpl'); 
        }

        
        $html = $this->view->render();
		
		$res['html'] = $html;
		$res['status'] = true;
		return $res;
	} 	
	
	function delete($id=0){
		
		if(!$id)$id=$_POST['id'];
		$prj = $this->prj->getByID($id);
		
		if($prj['project_user_id']==$_SESSION['account']['user_id']){
			
			$this->prj->delete($id);
		}
		
		
		$res['status'] = true;
		return $res;
	}
	
    function deleteBundle(){
	    $failed = array();
	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){ $this->delete($val); }
		    $res['status']=true;
		    return $res;
		    
	    }else{
		    $res['title']="Operation Failed";
		    $res['status']=false;
		    $res['msg']="No rows selected. Please select at least one template.";
		    return $res;
	    }	    
	    
    }
	
	function view(){
		
		$id = $this->args[0];
		$this->view->statuses = $this->prj->getAllStatuses();
		$prj = $this->prj->getByID4View($id);
		$this->view->milestones = $this->prj->getMilestones($id);
			
		$role = $this->prj->getRole($id,$_SESSION['account']['user_id']);
		
		if(!$role){
			
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}
		
		$issue = new OrmIssues();

		$selT['issue_project_id'] = $id;
		$total = $issue->getListCount($selT);		

		$selIssue['issue_project_id'] = $id;
		$selIssue['issue_status'] = 10;
		$issue_opens = $issue->getListCount($selIssue);		
		
		if($issue_opens){
			
			$issue_opens_percent = round($issue_opens*100/$total);
			
		}else{
			$issue_opens_percent = 0;
		}
		$this->view->issue_opens = $issue_opens;
		$this->view->issue_opens_percent = $issue_opens_percent;
		
		$selIssue = array();
		$selIssue['issue_project_id'] = $id;
		$selIssue['issue_status'] = 50;
		$issue_closed = $issue->getListCount($selIssue);		

		if($issue_closed){
			$issue_closed_percent = round($issue_closed*100/$total);
			
		}else{
			
			$issue_closed_percent = 0;
		}
		$this->view->issue_closed_percent = $issue_closed_percent;
		$this->view->issue_closed = $issue_closed;

		$selIssue = array();
		$selIssue['issue_project_id'] = $id;
		$selIssue['issue_status'][] = 20;
		$selIssue['issue_status'][] = 30;
		$selIssue['issue_status'][] = 40;		
		$issue_other = $issue->getListCount($selIssue);		
		
		if($issue_other){
			
			$issue_other_percent = round($issue_other*100/$total);
		}else{
			
			$issue_other_percent = 0;
		}
		
		$this->view->issue_other_percent = $issue_other_percent;
		$this->view->issue_other = $issue_other;
		$this->view->role = $role;
	
		$this->view->prj = $prj;				
        $this->view->setTemplate('view.tpl');  
        return $this->view;
	}

	function edit(){
		$this->view->dpformat = app::dateFormatToDatePicker($_SESSION['account']['user_dformat']);
		$id = $this->args[0];
		
		$this->view->prj = $this->prj->getByID($id);
		$this->view->statuses = $this->prj->getAllStatuses();
		
		
		$sel['cb_status']=3;
		$this->view->collabs = $this->collab->getAllList($sel);
		$this->view->collabssel = $this->prj->getCollabSelIDS($id);
	
		$this->view->milestones = $this->prj->getMilestones($id);
				
        $this->view->setTemplate('edit.tpl');  
        return $this->view;
	}
	
	function getDetails(){
		$id = $_GET['id'];
		
		$prj = $this->prj->getByID($id);
		$this->view->prj = $prj;
		$this->view->milestones = $this->prj->getMilestones($id);
		
		$this->view->setTemplate('mileston-select.tpl');  
		$miles = $this->view->render();
		
	//	$collabs = $this->prj->getCollabs($id);


	    if($id==$_SESSION['DEFPRJ']){
		     
		    $selC['cb_status'] = 3;
		  //  $selC['cb_collobarator_id!'] = $_SESSION['account']['user_id'];
		    $this->view->collabs = $this->collab->getAllList($selC);
		    $this->view->defprj = 1;
		     
	    }else{
		    $this->view->collabs = $this->prj->getCollabs($id);
		    
	    }

		
		//$this->view->collabs = $collabs;
		
		$this->view->setTemplate('collabs-select.tpl');  
		$collabs = $this->view->render();
		
		$res['miles'] = $miles;
		$res['collabs'] = $collabs;
		$res['status'] = true;
		return $res;
		
	}
	
	function getIssuesChart(){
		$prj = $_GET['prj'];
		$issue = new OrmIssues();
		$istatuses = $issue->getAllStatuses();
		$allissue = array();
		$colors = array();
		
		$collaborators = $this->prj->getCollabArr($prj);
		$project = $this->prj->getByID($prj);
		$collaborators[]=$project['project_user_id'];
		
		if(!in_array($_SESSION['account']['user_id'], $collaborators)){
			
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
			
		}
		
		foreach($istatuses as $k=>$v){
			$selIssue = array();
			$iarr = array();
			
			$selIssue['issue_project_id'] = $prj;
			//$selIssue['issue_user_id'] = $_SESSION['account']['user_id'];
			$selIssue['issue_status'] = $v['issuestatus_status'];
			$count = $issue->getListCount($selIssue);
			
			$iarr['label'] = $v['issuestatus_name'];
			$iarr['data'] = (int)$count;
			$allissue[] = $iarr;
			$colors[] = $v['issuestatus_color_hex'];
			
		}
		
		$res['status'] = true;
		$res['issues'] = $allissue;
		$res['colors'] = $colors;
		return $res;
		
	}

	function getMilestones(){
		
		$this->view->dpformat = app::dateFormatToDatePicker($_SESSION['account']['user_dformat']);
		$id = $_GET["id"];
		$this->view->prj = $this->prj->getByID($id);		
		$this->view->milestones = $this->prj->getMilestones($id);		
        $this->view->setTemplate('editmiles.tpl');  
        
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return $res;
	}	

	function doUpdateMilestones(){
		
		$id = $_POST['id'];
		$this->prj->delMilestone($id);
		
		if(isset($_POST['milestone'])){
			
			foreach($_POST['milestone'] as $k=>$v){
				
				$addm['prjm_name'] = app::strings_clear($v);
				
				if($_SESSION['account']['user_dformat']=="d/m/Y"){
					list($day,$month,$year) = explode('/',$_POST['deadline'][$k]);
					$deadline = $year."-".$month."-".$day;
				}else{
					$deadline = $_POST['deadline'][$k];
				}
				
				$addm['prjm_deadline'] = \date("Y-m-d",strtotime($deadline));
				$idMilestone = $this->prj->addMilestone($addm,$id);
				
				$addCal['e_date'] = \date("Y-m-d H:i:s",strtotime($deadline));
				$addCal['e_user_id'] = $_SESSION['account']['user_id'];
				$addCal['e_type'] = 2;
				$addCal['e_title'] = $addm['prjm_name'];
				$addCal['e_object_id'] = $idMilestone;
				$this->cal->add($addCal);
				
				$addm=array();
				
			}
			
		}
		
		$prj = $this->prj->getByID($id);
		$this->view->milestones = $this->prj->getMilestones($id);
        $this->view->setTemplate('viewmiles.tpl');  
        
        $res['html'] = $this->view->render();
		$res['msg']	= 	app::$lang->projects['upd'];
		
		$res['status'] = true;
		return $res;
		
		
	}	
	
	
	//Details
	
	function doUpdateDetails(){
		
		$upd['project_name'] = $_POST['project_name'];
		$upd['project_status'] = $_POST['project_status'];
		$upd['project_desc'] = $_POST['project_desc'];
		
		$this->prj->updateArr($_POST['id'],$upd);
		
		$res['status'] = true;
		return $res;
		
	}
	
	function getDetailsView(){
		
		$id = $_GET['id'];
		
		$prj = $this->prj->getByID4View($id);
		
		$this->view->prj = $prj;
		$this->view->setTemplate('details_view.tpl');  
		$res['html'] = $this->view->render();
		$res['status'] = true;
		return $res;
	}

	function getDetailsEdit(){
		
		$id = $_GET['id'];

		$selClients['client_collab'] = 0;
		$this->view->clients = $this->client->getAllList($selClients);
		
		$selClientsCollabs['cb_status']=3;
		$this->view->clientcollabs = $this->collab->getAllList($selClientsCollabs,array(),"users.user_name ASC");

		
		$prj = $this->prj->getByID($id);
		$this->view->statuses = $this->prj->getAllStatuses();
		
		$this->view->prj = $prj;
		$this->view->setTemplate('details_edit.tpl');  
		$res['html'] = $this->view->render();
		$res['status'] = true;
		return $res;
	}
	
	// Collaborators
	
	function addNewCollabLine(){
		
		$id = uniqid();
		$this->view->id = $id;
		$nsel = array();
		$sel = array();
		
		$sel['cb_status']=3;
		if(isset($_GET['prju_collaborator_id'])){
			if(count($_GET['prju_collaborator_id'])>1){
				$nsel['cb_collobarator_id']=$_GET['prju_collaborator_id'];
			}else{
				
				$sel['cb_collobarator_id!']=$_GET['prju_collaborator_id'];
			}		
		}

		$collabs = $this->collab->getAllList($sel,$nsel);
		if(!$collabs){
			
			$res['status'] = false;
			return $res;
			
		}
		
		$this->view->collabs = $collabs;
		$this->view->setTemplate('collabs_new.tpl');  
        $res['html'] = $this->view->render();
        $res['status'] = true;
        $res['id'] = $id;
        return $res;

		
	}
	
	function getCollabListView(){
		
		$id = $_GET['id'];
		
		$collabs = $this->prj->getCollabs($id);
		
		foreach($collabs as $k=>$v){
			$issuesObj = new OrmIssues();
			$selIssueOpenCnt['issue_assign'] = $v['prju_collaborator_id'];
			$selIssueOpenCnt['issue_status'] = array(10,20);
			$selIssueOpenCnt['issue_trash'] = 0;
			$selIssueOpenCnt['issue_project_id'] = $id;
			$openIssueCount = $issuesObj->getListCount($selIssueOpenCnt);
			if(!$openIssueCount)$openIssueCount=0;
			
			$selIssueClosedCnt['issue_assign'] = $v['prju_collaborator_id'];
			$selIssueClosedCnt['issue_status'] = array(30, 50);
			$selIssueClosedCnt['issue_trash'] = 0;
			$selIssueClosedCnt['issue_project_id'] = $id;
			$closeIssueCount = $issuesObj->getListCount($selIssueClosedCnt);
			if(!$closeIssueCount)$closeIssueCount=0;
			
			$collabs[$k]['open'] = $openIssueCount;
			$collabs[$k]['close'] = $closeIssueCount;
			
		}
				
		
		$this->view->collabs = $collabs;
		
        $this->view->setTemplate('collabs_view.tpl');  
        
        $res['html'] = $this->view->render();
		$res['status'] = true;
		return $res;
	}
	
	function getCollabListEdit(){
		
		$id = $_GET['id'];
		
		$collabs = $this->prj->getCollabs($id);
		$this->view->prj = $this->prj->getByID($id);
		$this->view->prjcollabs = $this->prj->getCollabs($id);
		$this->view->collabs = $collabs;
        $this->view->setTemplate('collabs_edit.tpl');  
       
        $res['html'] = $this->view->render();
		$res['status'] = true;
		return $res;
				
		
	}
	
	function doUpdateCollab(){
		$newCollabs = array();
		$id = $_POST['id'];
		$prj = $this->prj->getByID($id);
		$currentPrjCollabs = $this->prj->getCollabsForUpdateIDS($id,$prj['project_user_id']);
		
		$this->prj->removeAllCollabs($id,$prj['project_user_id']);
		
		if(isset($_POST['prju_collaborator_id'])){
			
			foreach($_POST['prju_collaborator_id'] as $k=>$v){

				$role = $_POST['prju_role'][$k];
				$this->prj->addCollab($v,$id,$role);
				
				if($v>0){
					
					if(!in_array($v, $currentPrjCollabs)){
						
						//update
				        $addn['n_user_id'] = $v;
				        $addn['n_src'] = 1;
				        $addn['n_type'] = "prj_add";
				        $addn['n_link'] = "/projects/view/".$id;
						$this->notifications->add($addn);
							
					}
					
					
				}
			}
			
			$newCollabs = $_POST['prju_collaborator_id'];
		}
		
		//change Issu assignement if collaborator deleted
		
		$issueObj = new OrmIssues();
		foreach($currentPrjCollabs as $k=>$v){
			
			if(!in_array($v, $newCollabs)){
				
				$where['issue_project_id'] = $id;
				$where['issue_assign'] = $v;
				$where['issue_status!'] = 50;
				$update['issue_assign'] =$prj['project_user_id'] ;
				
				$issueObj->updateMass($where,$update);
			}
		}
		
		$res['status'] = true;
		return $res;
		
		
	}
	
}
