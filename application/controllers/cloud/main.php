<?php      
namespace floctopus\controllers\cloud;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;  
use \floctopus\models\libs\pagination as pagination;
use \floctopus\models\libs\jetgoogle as LibGoogle;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_cloud = app::$lang->cloud;
        $this->view->setPath(app::$device.'/cloud');   
		$this->google = new  LibGoogle();
		if(!isset($_SESSION['CLOUD_RET_URL']))$_SESSION['CLOUD_RET_URL']="/projects";
	}   

    function __default($args = false) {   
		
		$isauth = $this->google->isAuth();
		
		if(!$isauth)$this->view->authUrl = $this->google->getAuthUrl();
		$this->view->isauth = $isauth;
		
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 	
	
}
