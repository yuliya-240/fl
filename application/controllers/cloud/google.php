<?php      
namespace floctopus\controllers\cloud\google;

use \floctopus\application as app;     
use \jet\controller\http as HTTPController;   
use \floctopus\models\libs\jetgoogle as LibGoogle;
use \floctopus\models\orm\accounts as OrmAcc;

class main extends HTTPController {
	
    function __before() {
	    
		$this->google = new  LibGoogle();
		$this->users = new OrmAcc();	    
    }

    function __default($args = false){
	    
	    
	    
    }
    
    function oauth2callback(){
	    
	    if (isset($_GET['code'])) {
		    
		    $code = $_GET['code'];
		    $token = $this->google->fetchAccessTokenWithAuthCode($code);
		    $this->users->update($_SESSION['account']['user_id'],array("user_google_token"=>$token));
		    
		    \jet\redirect('/cloud/',true);
		    
	    }
	    
	    return false;
    }

	
}
