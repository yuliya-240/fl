<?php      
namespace floctopus\controllers\clients;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;  
use \floctopus\models\orm\clients as OrmClients; 
use \floctopus\models\orm\colloborators as OrmCollabs;
use \floctopus\models\orm\country as OrmCountry; 
use \floctopus\models\libs\pagination as pagination;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_clients = app::$lang->clients;
    	$this->view->lng_first = app::$lang->first;
        $this->view->setPath(app::$device.'/clients');   
		$this->client = new OrmClients();
	    $this->collabs = new OrmCollabs();
		$this->country = new OrmCountry();
		if(!isset($_SESSION['CLIENT_RET_URL']))$_SESSION['CLIENT_RET_URL']="/clients";
		$this->view->sbPeople = "active";
		$this->view->subClients = "active";
	}   

    function __default($args = false) {   
		
		if(isset($_SESSION['CLIENT_SEARCH_STR']))$this->view->searchstr = $_SESSION['CLIENT_SEARCH_STR'];
		$this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('index.tpl');
         
        return $this->view;
    } 

    function getContent(){

    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['client_trash']=0;
	    $where['client_collab']=0;
	    $where['client_user_id']=$_SESSION['account']['user_id'];

    	if(isset($_GET['query'])){
	    	$r = $this->makeTags($_GET['query']);
		 
		    $srch=$r['str'];
		    if(strlen($srch)){
			    $_SESSION['CLIENT_SEARCH_TAGS']=array($srch);
			    $_SESSION['CLIENT_SEARCH_STR']=$srch;
			    
		    }else{
		    	$_SESSION['CLIENT_SEARCH_TAGS']=NULL;
		    	$_SESSION['CLIENT_SEARCH_STR']=NULL;
			    
		    }
    	}
	    
     	if(isset($_SESSION['CLIENT_SEARCH_STR']) ){
	    	
	    	$totalRec = $this->client->getListCountSearch($where,$_SESSION['CLIENT_SEARCH_TAGS']);
    	}else{
	    	
	    	$totalRec = $this->client->getListCount($where,$search);
    	}
	    
	    
    	$page = isset($_GET['p']) && $_GET['p']  ? \abs((int)$_GET['p']) :1;
	    $itemsOnPage = isset($_GET['iop']) && $_GET['iop']  ? \abs((int)$_GET['iop']) : $_SESSION["iop"];

	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		
		$order ='client_name ASC';
	    
	    $this->view->totalCount=$totalRec;

	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;

    	if(isset($_SESSION['CLIENT_SEARCH_STR'])){
	    	
	    	$this->view->records = $this->client->getListSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['CLIENT_SEARCH_TAGS'],$order);
    	}else{
    		$this->view->records = $this->client->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
   		}

		
        $this->view->setTemplate('page.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;
        
        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
    }
    
    function add(){
	    
	    $this->view->countryList=$this->country->getCountriesList();
	    $this->view->statesList=$this->country->getStateList($_SESSION['account']['user_country']);
	    $this->view->currencyList = $this->country->getCurrencyList();
        $this->view->setTemplate('add.tpl');  
        return $this->view;
    }
	
	function doAdd(){
		
	    if(\trim($_POST['client_discount'])>100)$_POST['client_discount']=100;
		if(!strlen($_POST['client_discount']))$_POST['client_discount']=0;
	    $cemail = app::strings_clear($_POST['client_email']);
	    $cemailcc = app::strings_clear($_POST['client_ccemail']);
	    $ccity = app::strings_clear($_POST['client_city']);
	    $caddr1 = app::strings_clear($_POST['client_street']);
	    $cphone = app::strings_clear($_POST['client_phone']);
	  //  $cmobile = app::strings_clear($_POST['client_mobile']);
	    $cname = app::strings_clear($_POST['client_name']);
	    $czip = app::strings_clear($_POST['client_zip']);

	    $cpost = array(
	    	'client_user_id' => $_SESSION['account']['user_id'],
	    	'client_email' => $cemail,
	    	'client_country' => $_POST['client_country'],
	    	'client_state' => $_POST['client_state'],
	    	'client_city' => $ccity,
	    	'client_zip' => $czip,
	    	'client_street' => $caddr1,
	    	'client_phone' => $cphone,
	    //	'client_mobile' => $cmobile,
	    	'client_name' => $cname,
	    	'client_discount' => $_POST['client_discount'],
	    	'client_ccemail' => $cemailcc,
	    	'client_currency' => $_POST['client_currency'],
	    	'client_key' => uniqid(),
	    	'client_tags' => $cemail." ".$cname." ".$cphone." ".$caddr1." ".$ccity." ".$czip
	    );
	    $cid = $this->client->add($cpost);
	    
	    $res['status'] = true;
	    return $res;		
	}

	function doAddPopUp(){
	    
	    $cemail = app::strings_clear($_POST['client_email']);
	    $ccity = app::strings_clear($_POST['client_city']);
	    $caddr1 = app::strings_clear($_POST['client_street']);
	    $cphone = app::strings_clear($_POST['client_phone']);
	    $cname = app::strings_clear($_POST['client_name']);
	    $czip = app::strings_clear($_POST['client_zip']);

	    $cpost = array(
	    	'client_user_id' => $_SESSION['account']['user_id'],
	    	'client_email' => $cemail,
	    	'client_country' => $_POST['client_country'],
	    	'client_state' => app::strings_clear($_POST['client_state']),
	    	'client_city' => $ccity,
	    	'client_zip' => $czip,
	    	'client_street' => $caddr1,
	    	'client_phone' => $cphone,
	    	'client_name' => $cname,
	    	'client_discount' => 0,
	    	'client_tags' => $cemail." ".$cname." ".$cphone." ".$caddr1." ".$ccity." ".$czip
	    );
	    $newCustomID = $this->client->add($cpost);
	    $selectbox='';
	    $html='';
	    
	    $selCust['client_user_id'] = $_SESSION['account']['user_id'];
	    $selCust['client_trash'] = 0;
	    $selCust['client_collab'] = 0;
	    $customersList = $this->client->getAllList($selCust);
	    $selectbox.='<optgroup label="'.app::$lang->menu['clients'].'">';
	    foreach($customersList as $v){
		    
		    if($v['client_id']==$newCustomID){
			    $selectbox.='<option selected value="'.$v['client_id'].'@cli">'.$v['client_name'].'</option>';
 			    
		    }else{
			     $selectbox.='<option value="'.$v['client_id'].'@cli">'.$v['client_name'].'</option>';
		    }
		    
	    }
	    $selectbox.='</optgroup>';
	    $collabs = $this->collabs->getAllList(array(),array(),"users.user_name ASC");
	    
	    if($collabs){
		    $selectbox.='<optgroup label="'.app::$lang->menu['colleague'].'">';
		    foreach($collabs as $v){

				$selectbox.='<option value="'.$v['user_id'].'@usr">'.$v['user_name'].'</option>';
			    
			    
		    }
		    $selectbox.='</optgroup>';
		    
	    }

	    $res['selBox'] = $selectbox;
	    $res['html'] = $html;
	    $res['id']=$newCustomID."@cli";
	    $res['name']=$cname;
		return $res;
	}


	function view(){
		
		$id = $this->args[0];
		$client = $this->client->getByID($id);
		
		if(!$client){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}
		
		$this->view->client = $client;
        $this->view->setTemplate('view.tpl');  
        return $this->view;
	}

	function edit(){
		
		$id = $this->args[0];
		$client = $this->client->getByID($id);
		
		if(!$client){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}
	    $this->view->countryList=$this->country->getCountriesList();
	    $this->view->statesList=$this->country->getStateList($client['client_country']);
	    $this->view->currencyList = $this->country->getCurrencyList();
		
		$this->view->client = $client;
        $this->view->setTemplate('edit.tpl');  
        return $this->view;
	}

	function doUpdate(){
		
	    if(\trim($_POST['client_discount'])>100)$_POST['client_discount']=100;
		if(!strlen($_POST['client_discount']))$_POST['client_discount']=0;
	    $cemail = app::strings_clear($_POST['client_email']);
	    $cemailcc = app::strings_clear($_POST['client_ccemail']);
	    $ccity = app::strings_clear($_POST['client_city']);
	    $caddr1 = app::strings_clear($_POST['client_street']);
	    $cphone = app::strings_clear($_POST['client_phone']);
	  //  $cmobile = app::strings_clear($_POST['client_mobile']);
	    $cname = app::strings_clear($_POST['client_name']);
	    $czip = app::strings_clear($_POST['client_zip']);

	    $cpost = array(
	    	
	    	'client_email' => $cemail,
	    	'client_country' => $_POST['client_country'],
	    	'client_state' => $_POST['client_state'],
	    	'client_city' => $ccity,
	    	'client_zip' => $czip,
	    	'client_street' => $caddr1,
	    	'client_phone' => $cphone,
	    	//'client_mobile' => $cmobile,
	    	'client_name' => $cname,
	    	'client_discount' => $_POST['client_discount'],
	    	'client_ccemail' => $cemailcc,
	    	'client_currency' => $_POST['client_currency'],
	    	'client_tags' => $cemail." ".$cname." ".$cphone." ".$caddr1." ".$ccity." ".$czip
	    );
	    $cid = $this->client->update($_POST['client_id'],$cpost);
	    
	    $res['status'] = true;
	    return $res;		
	}

	function getDetails(){
		
		$id = $_GET['id'];
		$client = $this->client->getByID($id);
		
		if(!$client){
			
			$res['status'] = false;
			return $res;	    
		}
		
		$this->view->client = $client;
        $this->view->setTemplate('details_view.tpl');  
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return $res;
	}
	
	function getEditDetails(){
		
		$id = $_GET['id'];
		$client = $this->client->getByID($id);
		
		if(!$client){
			
			$res['status'] = false;
			return $res;	    
		}

	    $this->view->currencyList = $this->country->getCurrencyList();
		
		$this->view->client = $client;
        $this->view->setTemplate('details_edit.tpl');  
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return $res;
	}

	function doUpdateDetails(){
		

		$id = $_POST['id'];
		$client = $this->client->getByID($id);

		
	    if(\trim($_POST['client_discount'])>100)$_POST['client_discount']=100;
		if(!strlen($_POST['client_discount']))$_POST['client_discount']=0;
	    $cname = app::strings_clear($_POST['client_name']);
	    

	    $cpost = array(
	    	
	    	'client_name' => $cname,
	    	'client_discount' => $_POST['client_discount'],
	    	'client_currency' => $_POST['client_currency'],
	    	'client_tags' => $client['client_email']." ".$cname." ".$client['client_phone']." ".$client['client_street']." ".$client['client_city']." ".$client['client_zip']
	    );
	    $cid = $this->client->update($id,$cpost);
	    
	    $res['status'] = true;
	    return $res;		
		
		
	}


	function getEditAddr(){
		
		$id = $_GET['id'];
		$client = $this->client->getByID($id);
		
		if(!$client){
			
			$res['status'] = false;
			return $res;	    
		}
	    $this->view->countryList=$this->country->getCountriesList();
	    $this->view->statesList=$this->country->getStateList($client['client_country']);
		
		$this->view->client = $client;
        $this->view->setTemplate('addr_edit.tpl');  
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return $res;
	}


	function getAddr(){
		
		$id = $_GET['id'];
		$client = $this->client->getByID($id);
		
		if(!$client){
			
			$res['status'] = false;
			return $res;	    
		}
		
		$this->view->client = $client;
        $this->view->setTemplate('addr_view.tpl');  
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return $res;
	}

	function doUpdateAddr(){
		
		$id = $_POST['id'];
		$client = $this->client->getByID($id);
		
	    $ccity = app::strings_clear($_POST['client_city']);
	    $caddr1 = app::strings_clear($_POST['client_street']);
	    $czip = app::strings_clear($_POST['client_zip']);
	    $cstate = app::strings_clear($_POST['client_state']);

	    $cpost = array(
	    	
	    	'client_country' => $_POST['client_country'],
	    	'client_state' => $cstate,
	    	'client_city' => $ccity,
	    	'client_zip' => $czip,
	    	'client_street' => $caddr1,
	    	'client_tags' => $client['client_email']." ".$client['client_name']." ".$client['client_phone']." ".$caddr1." ".$ccity." ".$czip
	    );
	    $cid = $this->client->update($id,$cpost);
	    
	    $res['status'] = true;
	    return $res;		
		
		
	}
	
	// COntacts	
	function getContact(){
		
		$id = $_GET['id'];
		$client = $this->client->getByID($id);
		
		if(!$client){
			
			$res['status'] = false;
			return $res;	    
		}
		
		$this->view->client = $client;
        $this->view->setTemplate('contact_view.tpl');  
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return $res;
	}
	
	function getEditContact(){
		
		$id = $_GET['id'];
		$client = $this->client->getByID($id);
		
		if(!$client){
			
			$res['status'] = false;
			return $res;	    
		}
		
		$this->view->client = $client;
        $this->view->setTemplate('contact_edit.tpl');  
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return $res;
	}

	function doUpdateContact(){

		$id = $_POST['id'];
		$client = $this->client->getByID($id);
		
	    $cemail = app::strings_clear($_POST['client_email']);
	    $cemailcc = app::strings_clear($_POST['client_ccemail']);
	    $cphone = app::strings_clear($_POST['client_phone']);

	    $cpost = array(
	    	
	    	'client_email' => $cemail,
	    	'client_phone' => $cphone,
	    	'client_ccemail' => $cemailcc,
	    	'client_tags' => $cemail." ".$client['client_name']." ".$cphone." ".$client['client_street']." ".$client['client_city']." ".$client['client_zip']
	    );
	    $cid = $this->client->update($id,$cpost);
	    
	    $res['status'] = true;
	    return $res;		
	}	
	
	function deleteBundle(){
		
		  if(isset($_POST['row']) && count($_POST['row'])){
			$ids = array();  
		    foreach($_POST['row'] as $val){
			    $ids[] = $val;
		    }
		    
		    $sel['client_id'] = $ids;
		    $upd['client_trash'] = 1;
		    $this->client->updateMass($sel,$upd);
		  }
		  
		  $res['status'] = true;
		  return $res;
	}
	
	
	function delete(){
		
		$sel['client_id'] = $_POST['id'];
		$upd['client_trash'] = 1;
		$this->client->update($_POST['id'],$upd);

		$res['status'] = true;
		return $res;
	}
	

    /*
	    
function getAddresstoInvoice(){
			
		$cid = $_GET['id'];
    	$customer = $this->client->getByID($cid);
    	
    	if ($customer){
	    		
	    	$html='';
	    	$html.='<strong>'.$customer['client_name'].'</strong><br>';
	    	$html.=$customer['client_street'].'<br>';
	    		
			$html.=$customer['client_city'].' '.$customer['client_state'].' '.$customer['client_zip'].'<br>';
			$html.=$customer['country_printable_name'].'<br>';
			
			
	    	
    	}else{
	    	$html='';
    	}

			
		$res['status'] = true;
		$res['html'] = $html;
		$res['id'] = $customer['client_id'];
		$res['email'] = $customer['client_email'];
		$res['discount'] = $customer['client_discount'];
		//$res['terms'] = $customer['custom_inv_terms'];
		//$res['notes'] = $customer['custom_inv_notes'];

		//$res['defterms'] = $_SESSION['account']['set_inv_terms'];
		//$res['defnotes'] = $_SESSION['account']['set_inv_notes'];
		    	    
		return $res;
	}	    
	    
*/	    

	function newCustomerBox(){
		
        $this->view->setTemplate('newpopup.tpl');
        
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return $res;
		
		
	}
	
	
}

