<?php      
namespace floctopus\controllers\signup;

use \floctopus\application as app;     
use \jet\controller\http as HTTPController; 
use \floctopus\models\orm\accounts as OrmAccounts;
use \floctopus\models\logic\jetsession as Session;
use \floctopus\models\orm\notifications as OrmNotifications;

use \floctopus\models\orm\colloborators as OrmCollab;
use \floctopus\models\logic\jetMail as jmail;
use \jet\config as config;
use \jet\language as lang;

class main extends HTTPController {
    
    function __before() {
        $this->session = new Session();
        $this->session->refresh();
        if ($this->session->isLogged() ) {
            \jet\redirect('/',true);
        }
        $this->view = new \jet\twig();   

        $this->view->setPath(app::$device.'/signup');   
        $this->skin=app::$device; 
        $this->view->skin = $this->skin; 
        $this->collab = new OrmCollab();
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

		switch ($lang){
		    case "ru":
		        
		        $lang = "ru";
		        break;
		    case "ua":
		        
		        $lang = "ua";
		        break;
		      
		    default:
		        
		        $lang = "en";
		        break;
		}
        
        app::$lang = new lang(app::$settings->lang_path._.$lang.'.lng');
    }
    
    function __default($args = null) {  
		
		if(isset($_GET['key'])){
			$key = $_GET['key'];
		
			$this->view->email = $this->collab->getEmailInviteByKey($key);
		}
        $this->view->lng_signup = app::$lang->signup;
        $this->view->lng_common = app::$lang->common;
	    
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
    function doSignup(){
	    $notify = new OrmNotifications();
	    $acc = new OrmAccounts();
        $email  = \strtolower(\trim($_POST['email']));
        $pass   =  uniqid();
        
	    try {
          
		   $acc->createAccount($email, $pass);
                
        }
        catch (\Exception $e) {
	       $res['title'] = app::$lang->common['operation_failed']; 
		   $res['msg'] = app::$lang->login[$e->getMessage()];
		   $res['status'] = false;
           return $res;
        }
        
        
        $m = new jmail();
		$sent =$m->sendWelcome($email);
        
	   try{
	    	$account=$this->session->login($email,$pass);
	    }catch (\Exception $e) {
	       
           $res['status']=false;
           $res['msg']=app::$lang->signup[$e->getMessage()];
           $res['title'] =  app::$lang->common['operation_failed']; 
           return $res;
        }
        
        $addn['n_user_id'] = $_SESSION['account']['user_id'];
        $addn['n_src'] = 0;
        $addn['n_type'] = "acc_created";
        $addn['n_link'] = "/";
        $notify->add($addn);

		$this->collab->getSentInvites($_SESSION['account']['user_email'],$_SESSION['account']['user_id']);
        
        $res['status'] = true;
        $res['url'] = '/';
	    return $res;
    }
    
    function doSignupFB(){
	  
	   $Registration = new Registration();
        
       $account = array(
	       	'id'      => $_POST['id'],
            'email'   => $_POST['email'],
			'fname'	=> $_POST['fname'],
            'lname'	=> $_POST['lname'],
        );

        $r=$Registration->signupfb($account); 	    
        
        if(!$r['fail']){
						
		    $email=\strtolower(\trim($r['username']));
		    $pass=\trim($r['pass']);
		    $hash = false;
	        
		   try{
		    	$account=$this->session->login($email,$pass);
		    }catch (\Exception $e) {
		            
	           $res['status']=false;
	           $msg=$e->getMessage();
	           $res['msg']=app::$lang->login[$msg];
	           $res['title'] = 'Login Failed';
	           return $res;
	        }
	        
	        
        }       
	    return $r;
	    
    }
    
    function doSignupGOOGLE(){
	    
	   $Registration = new Registration();
        
       $account = array(
	       	'id'      => $_POST['id'],
            'email'   => $_POST['email'],
			'fname'	=> $_POST['fname'],
            'lname'	=> $_POST['lname'],
        );

        $r=$Registration->signupgoogle($account); 	    

        
        if(!$r['fail']){
						
		    $email=\strtolower(\trim($r['username']));
		    $pass=\trim($r['pass']);
		    $hash = false;
	        
		   try{
		    	$account=$this->session->login($email,$pass);
		    }catch (\Exception $e) {
		            
		           $res['status']=false;
		           $msg=$e->getMessage();
		           $res['msg']=app::$lang->login[$msg];
		           $res['title'] = 'Login Failed';
		           return $res;
	        }
	        
	        
        }       
	    return $r;

    }

	function createFolders($newco = array()){
		
        $uploaddir = $_SERVER['DOCUMENT_ROOT'].'/userfiles/'.$newco['coid'].'/' ;

		if (!file_exists($uploaddir)) { mkdir( $uploaddir, 0777); }; 


       $emaildir = $_SERVER['DOCUMENT_ROOT'].'/userfiles/'.$newco['coid'].'/emails/' ;

       if (!file_exists($emaildir)) {  
    			mkdir( $emaildir, 0777);  
	   };

       $emaildir = $_SERVER['DOCUMENT_ROOT'].'/userfiles/'.$newco['coid'].'/logo/' ;

       if (!file_exists($emaildir)) {  
    			mkdir( $emaildir, 0777);  
	   };

       $emaildir = $_SERVER['DOCUMENT_ROOT'].'/userfiles/'.$newco['coid'].'/pics/' ;

       if (!file_exists($emaildir)) {  
    			mkdir( $emaildir, 0777);  
	   };

     $csvdir = $_SERVER['DOCUMENT_ROOT'].'/userfiles/'.$newco['coid'].'/csvimport/' ;

     if (!file_exists($csvdir)) {  
    			mkdir( $csvdir, 0777);  
	   };
	   
 		
		return false;
		
	}


    
}
