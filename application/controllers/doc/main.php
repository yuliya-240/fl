<?php
namespace floctopus\controllers\doc;
use \floctopus\application as app;
use \floctopus\models\common\adminController as adminController;
use \floctopus\models\orm\docs as OrmDocs;
use \floctopus\models\orm\colloborators as OrmCollab;
use \floctopus\models\libs\pagination as pagination;

class main extends adminController{
	
    function __before(){

        parent::__before();
        $this->view->lng_doc = app::$lang->doc;

        $this->view->setPath(app::$device . '/doc');
    	$this->collab = new OrmCollab();
        if (!isset($_SESSION['DOC_RET_URL'])) $_SESSION['DOC_RET_URL'] = "/doc";
        if(!isset($_SESSION['DOC_PAGE']))$_SESSION['DOC_PAGE']=1;
        if(!isset($_SESSION['DOC_FOLDER']))$_SESSION['DOC_FOLDER']=0;
        if(!isset($_SESSION['DOC_SHARE_MODE']))$_SESSION['DOC_SHARE_MODE']=1;
        $this->view->sbDoc = "active";
        $this->view->subDoc = "active";
        $this->docs = new OrmDocs();
       
    }
	
    function __default($args = false){
        if (isset($_SESSION['DOC_SEARCH_STR'])) $this->view->searchstr = $_SESSION['DOC_SEARCH_STR'];
		$this->view->txtnf = app::$lang->doc['pop_nf_text'];
		$this->view->tnf = app::$lang->doc['pop_nf_title'];
		$this->view->sharemode = $_SESSION['DOC_SHARE_MODE'];
		//$this->view->cfolder = $_SESSION['DOC_FOLDER'];
        $this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('index.tpl');
		
        return $this->view;
    }
	
    function getContent(){

        $pages = new pagination();
        $where = array();
        $search = array();
        $where['doc_trash'] = 0;
        
        if($_SESSION['DOC_SHARE_MODE']==1){
	        $where['doc_user_id'] = $_SESSION['account']['user_id'];
        }else{
	        $docsids = $this->docs->getSharedDocsID();
	        
	      if(!empty($docsids))$where['doc_id'] = $docsids; else $where['doc_id'] = 0;
	        
	        
        }

        if (isset($_GET['query'])) {
            $r = $this->makeTags($_GET['query']);

            $srch = $r['str'];
            if (strlen($srch)) {
                $_SESSION['DOC_SEARCH_TAGS'] = array($srch);
                $_SESSION['DOC_SEARCH_STR'] = $srch;

            } else {
                $_SESSION['DOC_SEARCH_TAGS'] = NULL;
                $_SESSION['DOC_SEARCH_STR'] = NULL;

            }
        }
	        
	    $where['doc_folder'] = $_SESSION['DOC_FOLDER'];
		
		if($_SESSION['DOC_FOLDER']){
			
			$where['doc_type'] = 1;
			$folderinfo = $this->docs->getByID($_SESSION['DOC_FOLDER']);
			$res['foldername'] = $folderinfo['doc_name'];

			$wherecollab['cb_status'] = 3;
			$this->view->available_collabs = $this->collab->getAllList($wherecollab);
			
			$this->view->selCollab = $this->docs->getCollabSelIDS($_SESSION['DOC_FOLDER']);
	        $this->view->setTemplate('sharebox.tpl');
	        $sharebox = $this->view->render();
	        $res['sharebox'] = $sharebox;
		
		}
		
        if (isset($_SESSION['DOC_SEARCH_STR'])) {

            $totalRec = $this->docs->getListCountSearch($where, $_SESSION['NOTES_SEARCH_TAGS']);
        } else {

            $totalRec = $this->docs->getListCount($where, $search);
        }

        $page = $_SESSION['DOC_PAGE'];
        $itemsOnPage = $_SESSION['account']["user_rop"];

        $pagination = $pages->calculate_pages($totalRec, $itemsOnPage, $page);

        $order = 'doc_name ASC';

        $this->view->records = $this->docs->getList($where, $pagination['current'], $itemsOnPage, $search, $order);

        if (isset($_SESSION['DOC_SEARCH_STR'])) {

            $this->view->records = $this->docs->getListSearch($where, $pagination['current'], $itemsOnPage, $_SESSION['DOC_SEARCH_TAGS'], $order);
        } else {
            $this->view->records = $this->docs->getList($where, $pagination['current'], $itemsOnPage, $search, $order);
        }
		
		$this->view->sharemode = $_SESSION['DOC_SHARE_MODE'];
		$this->view->pagination = $pagination;
        $this->view->setTemplate('page.tpl');
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');
        $footer = $this->view->render();

        $res['sharemode'] = $_SESSION['DOC_SHARE_MODE'];
		$res['folder'] = $_SESSION['DOC_FOLDER'];
        $res['footer'] = $footer;
        $res['html'] = $html;
        $res['count'] = $totalRec;
        $res['pag'] = $pagination;
        return $res;
    }
	
	function add(){
		$_SESSION['DOC_SHARE_MODE']=1;
		$add['doc_folder'] = $_GET['f'];
		$add['doc_name'] = app::$lang->doc['btn_add'];
		$add['doc_user_id'] = $_SESSION['account']['user_id'];
		$add['doc_changed'] = \date("Y-m-d H:i:s");
		$res['id'] = $this->docs->add($add);
		$res['status'] = true;
        return $res;
	}

	function edit(){
        $this->view->fixed_footer = "am-sticky-footer";		
		$id = $this->args[0];
		
		$doc = $this->docs->getByID($id);

		if(!$doc){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}
		
		
		$where['cb_status'] = 3;
		$this->view->available_collabs = $this->collab->getAllList($where);
		
		$this->view->selCollab = $this->docs->getCollabSelIDS($id);
		
		$this->view->sharemode = $_SESSION['DOC_SHARE_MODE'];
		$this->view->doc = $doc;
        $this->view->setTemplate('edit.tpl');

        return $this->view;
		
	}
	
	function doUpdateName(){
		
		$upd['doc_name'] = $_POST['value'];
		$upd['doc_changed'] = \date("Y-m-d H:i:s");
		$this->docs->update($_POST['pk'],$upd);
		
		$res['status']=true;;
		return $res;
	}
	
	function doUpdateTxt(){
		$upd['doc_text'] = $_POST['txt'];
		$upd['doc_changed'] = \date("Y-m-d H:i:s");
		$this->docs->update($_POST['id'],$upd);
		
		$res['msg'] = app::$lang->doc['saved'];
		$res['status']=true;;
		return $res;
		
	}
	
	function delete($id=0){
		
		if(!$id)$id=$_POST['id'];
		
		$upd['doc_trash'] = 1;
		$this->docs->update($id,$upd);
		
		$res['status'] = true;
		return $res;
	}
	
	function changePage(){
		
		$_SESSION['DOC_PAGE'] = $_POST['p'];
		
		$res['status'] = true;
		return $res;
	}

    function deleteBundle(){
	  //  if($_SESSION['account']['user_role']>1 && $_SESSION['acl']['cfield']['acl_access']<2) die();
	    $failed = array();
	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){
			    $this->delete($val);
		    }
		    
		    $res['status']=true;
		    return $res;
		    
		    
	    }else{
		    $res['title']="Operation Failed";
		    $res['status']=false;
		    $res['msg']="No rows selected. Please select at least one template.";
		    return $res;
	    }	    
	    
    }
	
	function createFolder(){
		
		$add['doc_name'] = $_POST['name'];
		$add['doc_user_id'] = $_SESSION['account']['user_id'];
		$add['doc_type'] = 2;
		$add['doc_changed'] = \date("Y-m-d H:i:s");
		$res['id'] = $this->docs->add($add);
		$res['status'] = true;
        return $res;
		
	}
	
	function changeFolder(){
		
		$id = $_POST['id'];
		
		$_SESSION['DOC_FOLDER'] = $id;
		
		$res['status'] = true;
		return $res;
		
		
	}
	
	function getSharedBox(){
		$id = $_GET["id"];
		$where['cb_status'] = 3;
		$this->view->available_collabs = $this->collab->getAllList($where);
		
		$this->view->selCollab = $this->docs->getCollabSelIDS($id);
		
		$this->view->setTemplate('sharebox.tpl');
		
		$html = $this->view->render();
		
		$res['html'] = $html;
		$res['status'] = true;
		return $res;
	}
	
	function doShare(){
		
		$id = $_POST['docid'];
		$doc = $this->docs->getByID($id);
		$is_shared=0;
		$this->docs->delShare($id);
		if(isset($_POST['collabs'])){
			
			foreach($_POST['collabs'] as $k=>$v){
				
				$add['dc_user_id'] = $_SESSION['account']['user_id'];
				$add['dc_collab_id'] = $v;
				$add['dc_doc_id'] = $id;
				$this->docs->addShare($add);
				
				if($doc['doc_folder']){
					
					$addFolder['dc_user_id'] = $_SESSION['account']['user_id'];
					$addFolder['dc_collab_id'] = $v;
					$addFolder['dc_doc_id'] = $doc['doc_folder'];
					$this->docs->addShare($addFolder);
					
				}
			}
			$upd['doc_shared'] = 1;
			$this->docs->update($id,$upd);
		}
		
		
		if($doc['doc_folder']){
			$updf['doc_shared'] =  $is_shared;
			$this->docs->update($doc['doc_folder'],$updf);
		}	
		
		$res['status'] = true;
		return $res;
	}


	function doShareFolder(){
		
		$id = $_POST['docid'];
		$doc = $this->docs->getByID($id);
		$files_in_folder = $this->docs->getFolderDocIDS($id);
		
		
		
		$is_shared=0;
		$this->docs->delShare($id);
		
		foreach($files_in_folder as $v){
			
			$this->docs->delShare($v);
		}
		
		
		
		if(isset($_POST['collabs'])){
		
			foreach($files_in_folder as $d){	
			
				foreach($_POST['collabs'] as $k=>$v){
					
					$add['dc_user_id'] = $_SESSION['account']['user_id'];
					$add['dc_collab_id'] = $v;
					$add['dc_doc_id'] = $d;
						
					$id = $this->docs->addShare($add);
					
					$upd['doc_shared'] = 1;
					$this->docs->update($d,$upd);
					

				}
			}	
			$upd['doc_shared'] = 1;
			$this->docs->update($id,$upd);
		}
		
		$res['status'] = true;
		return $res;
	}

	
	function changeShareMode(){
		
        $_SESSION['DOC_PAGE']=1;
        $_SESSION['DOC_FOLDER']=0;
        $_SESSION['DOC_SHARE_MODE']=$_POST['mode'];
		
		$res['status'] = true;
		return $res;
		
	}
	
}
