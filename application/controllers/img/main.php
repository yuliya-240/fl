<?php      
namespace floctopus\controllers\img;

use \floctopus\application as app;     
use \jet\controller\http as HTTPController;   
use \floctopus\models\logic\jetsession as Session;     
use \floctopus\models\orm\accounts as OrmAccounts; 
use \floctopus\models\orm\pictures as OrmPic;

class main extends HTTPController {

    function __before() {
        
        $this->session = new Session();

        $this->session->refresh();
        if (!$this->session->isLogged() ) {
            \jet\redirect('/login/',true);
        }
         
		
		if (!file_exists($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles")) {
			@mkdir($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles", 0777, true);
		}

		if (!file_exists($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'])) {
			@mkdir($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'], 0777, true);
		}
    }	

	function uploaduserpic(){
	//  	$lng_profile = app::$lang->profile;
	//  	$lng_common = app::$lang->common;
	  	
	  	
	   $this->users = new OrmAccounts();

		if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic")) {
			@mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic", 0777, true);
		}

		
		$targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;		
		//app::trace($targetDir);
		
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
		
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
		
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;
		
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}
		
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
		if (!file_exists($targetDir))@mkdir($targetDir);

		// Remove old temp files	
		if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
					@unlink($tmpfilePath);
				}
			}
		
			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		
		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
		
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}
		
		
		list($width, $height, $type) = getimagesize($filePath);
		
		
		
		if($width<401 || $height<401){
			$res['status'] = false;
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg']= app::$lang->profile['err_les200'];
			return $res;
			
		}else{
			$dest_x=400; 
			$dest_y=400;
	
			$sourcefile = $filePath;
			$targetfile = $filePath;

			$arr = $this->resize($sourcefile, $dest_x, $dest_y, $targetfile);	
			
			$width=$arr['width'];
			$height=$arr['height'];
		
		}
		
		$res['status'] = true;
		$res['cid'] = $_SESSION['account']['user_id'];
		$res['cleanFileName'] = $fileName;
		$res['width'] = $width+40;
		$res['type'] = $type;
	
		$res['imghost'] = app::$proto.app::$config->site['userhost'].'/userfiles/' ;
		
		return $res;
			    
	}
	
	function createavatar(){
			$acc = new OrmAccounts();
			$targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;		
	
			$targ_w = $targ_h = 200;
		
			$src = $targetDir.DIRECTORY_SEPARATOR.$_POST['fname'];
			$nfname = uniqid().".jpg";
			$newfilename = $targetDir.DIRECTORY_SEPARATOR.$nfname;
			//app::trace($src);
			//$img_r = imagecreatefromjpeg($src);
			
		    switch ($_POST['ftype'])
		    {
		        case 1: $img_r = imagecreatefromgif($src); break;
		        case 2: $img_r = imagecreatefromjpeg($src);  break;
		        case 3: $img_r = imagecreatefrompng($src); break;
		       
		    }
			
			
			$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
			imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);
			
		    switch ($_POST['ftype'])
		    {
		        case 1: imagegif($dst_r,$newfilename); break;
		        case 2: imagejpeg($dst_r,$newfilename,100);  break;
		        case 3: imagepng($dst_r,$newfilename,0); break;
		       
		    }
			
			
			//imagejpeg($dst_r,$newfilename ,$jpeg_quality);
			
			$upd['user_pic'] = $nfname;
			$acc->update($_SESSION['account']['user_id'],$upd);
        $res['cid'] = $_SESSION['account']['user_id'];
        $res['fname']= $nfname;
            $res['status'] = true;
			return $res;
			
		
	}

    function delava(){
        $acc = new OrmAccounts();
        $upd['user_pic'] = "nopic.png";
        $acc->update($_SESSION['account']['user_id'],$upd);
        $res['status'] = true;
        return $res;
    }

    function deletepic(){
		$targetDir = app::$config->site['fpath'].$_SESSION['account']['co_id'].DIRECTORY_SEPARATOR."pics".DIRECTORY_SEPARATOR;//.$_POST["fname"] ;
		$pic = new OrmPic();		
		$delPic['user_id']=$_POST['id'];
		$pic->delete($delPic);

		\unlink($targetDir.$_POST["fname"] );
		\unlink($targetDir.'/tmb_'.$_POST["fname"] );
		
		$res['status'] = true;
		return $res;
	}

	function redactorUp(){

		if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."redactor")) {
			@mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."redactor", 0777, true);
		}
		
		$dir=  $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."redactor".DIRECTORY_SEPARATOR;  
		
		
		$_FILES[ 'file']['type']  =  strtolower( $_FILES[ 'file']['type'] );
		
		if ( $_FILES['file']['type']  == 'image/png' 
		|| $_FILES['file']['type']  == 'image/jpg' 
		|| $_FILES['file']['type']  == 'image/gif' 
		|| $_FILES['file']['type']  == 'image/jpeg' 
		|| $_FILES['file']['type']  == 'image/pjpeg')  {
			//settingfile'smysteriousname
			$info = getimagesize($_FILES['file']['tmp_name']);
			$extension = image_type_to_extension($info[2]);
			//app::trace($extension);
			$filename=  md5( date('YmdHis') ).$extension;  
			$filenamet=  "t_".md5( date('YmdHis') ).$extension;
			$file= $dir.$filename; 
			$filet= $dir.$filenamet; 
			//copying 
			move_uploaded_file( $_FILES['file']['tmp_name'] , $file) ;
			//displayingfile 
			
			if($info[0]>800 || $info[1]>600){
				$sourcefile = $file;
				$targetfile = $filet;
				$dest_x = 800;
				$dest_y = 600;
				$jpegqual =  app::$config->img['quality'];
				$this->resize($sourcefile, $dest_x, $dest_y, $targetfile);	
				
				$filename = $filenamet;
			}

			$array = array(
				"filelink" => app::$proto.app::$config->site['host']."/redactor/".$filename, // http://
			);
			header('Content-type: application/json');
			echo stripslashes(json_encode($array));   
			
			/*$res['filelink']="https://secure.managemart.com/redactor/".$filename;
			$res['status'] = true;
			return $res;*/
		
		}
		
		/*	
			$res['status'] = false;
			return $res;
		*/	
		
	}

	function uploadissueattach(){
		//------

		if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."attach")) {
			@mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."attach", 0777, true);
		}

		
		$targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."attach" ;		
		//app::trace($targetDir);
		
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
		
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
		
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;
		
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}
		
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
		if (!file_exists($targetDir))@mkdir($targetDir);

		// Remove old temp files	
		if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
					@unlink($tmpfilePath);
				}
			}
		
			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		
		if (isset($_SERVER["CONTENT_TYPE"]))$contentType = $_SERVER["CONTENT_TYPE"];
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
		
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}
		
		//--------
		
		
		$res['id'] = uniqid();
		$res['status'] = true;
		$res['cid'] = $_SESSION['account']['user_id'];
		$res['cleanFileName'] = $fileName;
		$res['fullpath'] = app::$proto.app::$config->site['userhost'].'/userfiles/'.$_SESSION['account']['user_id'].'/attach/'.$fileName ;
		
		return $res;
		
		
	}

	function uploadinvpic(){
	  
	    $this->pic = new OrmPic();
		if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic")) {
			@mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic", 0777, true);
		}

		
		$targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;
		// Settings
	
		
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
		
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
		
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;
		
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}
		
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
		if (!file_exists($targetDir))@mkdir($targetDir);

		// Remove old temp files	
		if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
					@unlink($tmpfilePath);
				}
			}
		
			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		
		if (isset($_SERVER["CONTENT_TYPE"]))$contentType = $_SERVER["CONTENT_TYPE"];
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
		
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}
		
		list($ft,$ftd) = explode("/",$_FILES['file']['type']);
		
		//app::trace($ftd);
		
		if($ft==="image"){
			$tn_prefix=app::$config->img['prefix'];
			$tnPath = $targetDir . DIRECTORY_SEPARATOR . $tn_prefix.$fileName;
			$tnFile= $tn_prefix.$fileName;
			$copy = copy($filePath, $tnPath);
			//$sourcefile = $tnPath;
			//$targetfile = $tnPath;
			$jpegqual =  app::$config->img['quality'];
			$this->resize($tnPath, 200, 200, $tnPath);	
		}	

        $this->view = new \jet\twig();  
        $this->view->setPath(app::$device.'/invoices');
        $this->view->setTemplate('img.tpl');  
        
		$this->view->ftype = $ft;
		$this->view->pid = uniqid();
		$this->view->filename = $fileName;
		//$this->view->thumbnail =$fileName;
		$this->view->uid = $_SESSION['account']['user_id'];
		$this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
		
		$html = $this->view->render();
		
		$res['html'] = $html;
		
		return $res;
			    
	}	

	function uploadestpic(){
	  
	    $this->pic = new OrmPic();
if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic")) {
			@mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic", 0777, true);
		}

		
		$targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;
		// Settings
	
		
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
		
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
		
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;
		
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}
		
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
		if (!file_exists($targetDir))@mkdir($targetDir);

		// Remove old temp files	
		if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
					@unlink($tmpfilePath);
				}
			}
		
			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		
		if (isset($_SERVER["CONTENT_TYPE"]))$contentType = $_SERVER["CONTENT_TYPE"];
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
		
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}
		
		list($ft,$ftd) = explode("/",$_FILES['file']['type']);
		
		//app::trace($ftd);
		
		if($ft==="image"){
			$tn_prefix=app::$config->img['prefix'];
			$tnPath = $targetDir . DIRECTORY_SEPARATOR . $tn_prefix.$fileName;
			$tnFile= $tn_prefix.$fileName;
			$copy = copy($filePath, $tnPath);
			//$sourcefile = $tnPath;
			//$targetfile = $tnPath;
			$jpegqual =  app::$config->img['quality'];
			$this->resize($tnPath, 200, 200, $tnPath);	
		}	

        $this->view = new \jet\twig();  
        $this->view->setPath(app::$device.'/estimates');
        $this->view->setTemplate('img.tpl');  
        
		$this->view->ftype = $ft;
		$this->view->pid = uniqid();
		$this->view->filename = $fileName;
		//$this->view->thumbnail =$fileName;
		$this->view->uid = $_SESSION['account']['user_id'];
		$this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
		
		$html = $this->view->render();
		
		$res['html'] = $html;
		
		return $res;
			    
	}

    function uploadportfoliopic(){
        $this->users = new OrmAccounts();

        if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic")) {
            @mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic", 0777, true);
        }


        $targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;
        //app::trace($targetDir);

        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);

        // Get parameters
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

        // Clean the fileName for security reasons
        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        // Make sure the fileName is unique but only if chunking is disabled
        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Create target dir
        if (!file_exists($targetDir))@mkdir($targetDir);

        // Remove old temp files
        if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                    @unlink($tmpfilePath);
                }
            }

            closedir($dir);
        } else
            die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');


        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];

        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                // Open temp file
                $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    fclose($in);
                    fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                fclose($in);
                fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
        }


        list($width, $height, $type) = getimagesize($filePath);



        if($width<401 || $height<401){
            $res['status'] = false;
            $res['title'] = app::$lang->common['operation_failed'];
            $res['msg']= app::$lang->profile['err_les200'];
            return $res;

        }else{
            $dest_x=400;
            $dest_y=400;

            $sourcefile = $filePath;
            $targetfile = $filePath;

            $arr = $this->resize($sourcefile, $dest_x, $dest_y, $targetfile);

            $width=$arr['width'];
            $height=$arr['height'];

        }

        $res['status'] = true;
        $res['cid'] = $_SESSION['account']['user_id'];
        $res['cleanFileName'] = $fileName;
        $res['width'] = $width+40;
        $res['type'] = $type;
        $res['imghost'] = app::$proto.app::$config->site['userhost'].'/userfiles/' ;

        return $res;

    }

    function uploadportfolioall(){
        $this->pic = new OrmPic();
        if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic")) {
            @mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic", 0777, true);
        }


        $targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;
        // Settings


        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);

        // Get parameters

        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

        // Clean the fileName for security reasons
        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        // Make sure the fileName is unique but only if chunking is disabled
        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Create target dir
        if (!file_exists($targetDir))@mkdir($targetDir);

        // Remove old temp files
        if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                    @unlink($tmpfilePath);
                }
            }

            closedir($dir);
        } else
            die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');


        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))$contentType = $_SERVER["CONTENT_TYPE"];

        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                // Open temp file
                $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    fclose($in);
                    fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                fclose($in);
                fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
        }

        list($width, $height, $type) = getimagesize($filePath);

        $this->view = new \jet\twig();
        $this->view->id = uniqid();
        $this->view->setPath(app::$device.'/portfolio');
        $this->view->setTemplate('img.tpl');
        $this->view->filename = $fileName;
        $this->view->uid = $_SESSION['account']['user_id'];
        $this->view->ihost =  app::$proto.app::$config->site['host'] ;
        $html = $this->view->render();

        $res['cleanFileName'] = $fileName;
        $res['type'] = $type;
        $res['html'] = $html;

        return $res;


    }

    function createportfoliopic(){
        //$acc = new OrmAccounts();
        $targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;

        $targ_w = $targ_h = 200;

        $src = $targetDir.DIRECTORY_SEPARATOR.$_POST['fname'];
        $nfname = uniqid().".jpg";
        $newfilename = $targetDir.DIRECTORY_SEPARATOR.$nfname;

        switch ($_POST['ftype'])
        {
            case 1: $img_r = imagecreatefromgif($src); break;
            case 2: $img_r = imagecreatefromjpeg($src);  break;
            case 3: $img_r = imagecreatefrompng($src); break;

        }


        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
        imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

        switch ($_POST['ftype'])
        {
            case 1: imagegif($dst_r,$newfilename); break;
            case 2: imagejpeg($dst_r,$newfilename,100);  break;
            case 3: imagepng($dst_r,$newfilename,0); break;

        }


        //imagejpeg($dst_r,$newfilename ,$jpeg_quality);

        $upd['portfolio_thumbnail'] = $nfname;
        $res['cid'] = $_SESSION['account']['user_id'];
        $res['fname']= $nfname;
        $res['status'] = true;
        return $res;


    }

    function deleteporfoliopic(){
       // $targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;
        $targetDir = app::$config->site['fpath'].$_SESSION['account']['co_id'].DIRECTORY_SEPARATOR."pics".DIRECTORY_SEPARATOR;//.$_POST["fname"] ;
        $pic = new OrmPic();
        $delPic['pic_id']=$_POST['id'];
        $pic->delete($delPic);

        \unlink($targetDir.$_POST["fname"] );
        \unlink($targetDir.'/tmb_'.$_POST["fname"] );
       // $res['fname']= nopic.jpg;
        $res['status'] = true;
        return $res;
    }

	function resize($img, $w, $h, $newfilename) {
		    //Check if GD extension is loaded
		    if (!extension_loaded('gd') && !extension_loaded('gd2')) {
		        trigger_error("GD is not loaded", E_USER_WARNING);
		        return false;
		    }
		     
		    //Get Image size info
		    $imgInfo = getimagesize($img);
		    
		    
		    
		    switch ($imgInfo[2]) {
		    
		        case 1: $im = imagecreatefromgif($img); break;
		        case 2: $im = imagecreatefromjpeg($img);  break;
		        case 3: $im = imagecreatefrompng($img); break;
		        default:  trigger_error('Unsupported filetype!', E_USER_WARNING);  break;
		    }
		     
		    //If image dimension is smaller, do not resize
		    if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
		        $nHeight = $imgInfo[1];
		        $nWidth = $imgInfo[0];
		    }
		    else{
		    // yeah, resize it, but keep it proportional
		        if ($w/$imgInfo[0] > $h/$imgInfo[1]) {
		            $nWidth = $imgInfo[0]*($h/$imgInfo[1]);
		            $nHeight = $h;            
		        }
		        else{
		            $nWidth = $w;
		            $nHeight = $imgInfo[1]*($w/$imgInfo[0]);
		        }
		    }
		     
		    $nWidth = round($nWidth);
		     
		    $nHeight = round($nHeight);
		     
		    $newImg = imagecreatetruecolor($nWidth, $nHeight);
		     
		    /* Check if this image is PNG or GIF, then set if Transparent*/  
		    
		    if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
		        imagealphablending($newImg, false);
		        imagesavealpha($newImg,true);
		        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
		        imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
		    }
		     
		    imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);
		     
		    //Generate the file, and rename it to $newfilename
		    switch ($imgInfo[2]) {
		        case 1: imagegif($newImg,$newfilename); break;
		        case 2: imagejpeg($newImg,$newfilename);  break;
		        case 3: imagepng($newImg,$newfilename); break;
		        default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
		    }
		    
		    $r['width'] =  $nWidth;
		    $r['height'] = $nHeight;
		    return $r;
		}

	function uploadchat(){
		
	
		if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."chat")) {
			@mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."chat", 0777, true);
		}

		
		$targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."chat" ;
		// Settings
	
		
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
		
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
		
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;
		
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}
		
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
		if (!file_exists($targetDir))@mkdir($targetDir);

		// Remove old temp files	
		if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
					@unlink($tmpfilePath);
				}
			}
		
			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		
		if (isset($_SERVER["CONTENT_TYPE"]))$contentType = $_SERVER["CONTENT_TYPE"];
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
		
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}
		
		list($ft,$ftd) = explode("/",$_FILES['file']['type']);
		
		
		//app::trace($size);
		//app::trace($ftd);
		
		if($ft==="image"){
			$tn_prefix=app::$config->img['prefix'];
			$tnPath = $targetDir . DIRECTORY_SEPARATOR . $tn_prefix.$fileName;
			$tnFile= $tn_prefix.$fileName;
			$copy = copy($filePath, $tnPath);
			//$sourcefile = $tnPath;
			//$targetfile = $tnPath;
			$jpegqual =  app::$config->img['quality'];
			$this->resize($tnPath, 300, 300, $tnPath);	
			$size = getimagesize($tnPath);
			$res['fsize'] = $size[0]."x".$size[1];
			$res['height'] = $size[1];
		}	
		
		
		
        $this->view = new \jet\twig();  
        $this->view->setPath(app::$device.'/messenger');
        $this->view->setTemplate('file.tpl');  
        
		$this->view->ftype = $ft;
	
		$this->view->filename = $fileName;
		//$this->view->filename = $fileName;
		//$this->view->thumbnail =$fileName;
		//$this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
		
		$html = $this->view->render();
		
		$res['prefix'] = $tn_prefix;
		$res['cleanFileName'] = $fileName;
		$res['html'] = $html;
		$res['status'] = true;
		return $res;
		
		
	}

    function uploadlogo(){
	   $acc = new OrmAccounts();
	  
		if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic")) {
			@mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic", 0777, true);
		}

		
		$targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;		
		
		//app::trace($targetDir);
		
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
		
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
		
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;
		
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}
		
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
		if (!file_exists($targetDir))@mkdir($targetDir);

		// Remove old temp files	
		if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
					@unlink($tmpfilePath);
				}
			}
		
			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		
		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
		
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}
		
		/***
		  200px logo
		*/
		
		$tn_prefix=app::$config->img['prefix'];
		
		$tnPath = $targetDir . DIRECTORY_SEPARATOR . $tn_prefix.$fileName;
		$tnFile= $tn_prefix.$fileName;
		$copy = copy($filePath, $tnPath);
		$sourcefile = $tnPath;
		$targetfile = $tnPath;
		$dest_x = app::$config->img['dest_x'];
		$dest_y = app::$config->img['dest_y'];
		$jpegqual =  app::$config->img['quality'];
		$this->resize($sourcefile, $dest_x, $dest_y, $targetfile);	
		
		$oldlogo = $_SESSION['account']['set_logo'];
		
		if($oldlogo!='nologo.png'){
			@unlink($targetDir.'/'.$oldlogo);
			@unlink($targetDir.'/'.$tn_prefix.$oldlogo);
		}
		

		$update = array(
		
		   'set_logo' => $fileName
		);
		
		$acc->updateSettings($_SESSION['account']['user_id'],$update);
		
		$res['cleanFileName'] = $tnFile;
		$res['uid'] = $_SESSION['account']['user_id'];
		$res['imghost'] = app::$proto.app::$config->site['userhost'] ;
		return $res;

		//die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
			    
	}

    function uploadexppic(){
        $this->users = new OrmAccounts();

        if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic")) {
            @mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic", 0777, true);
        }


        $targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;
        //app::trace($targetDir);

        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);

        // Get parameters
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

        // Clean the fileName for security reasons
        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        // Make sure the fileName is unique but only if chunking is disabled
        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Create target dir
        if (!file_exists($targetDir))@mkdir($targetDir);

        // Remove old temp files
        if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                    @unlink($tmpfilePath);
                }
            }

            closedir($dir);
        } else
            die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');


        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];

        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                // Open temp file
                $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    fclose($in);
                    fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                fclose($in);
                fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
        }


        list($width, $height, $type) = getimagesize($filePath);

        $res['status'] = true;
        $res['cid'] = $_SESSION['account']['user_id'];
        $res['cleanFileName'] = $fileName;
        $res['width'] = $width+40;
        $res['type'] = $type;
        $res['imghost'] = app::$proto.app::$config->site['userhost'].'/userfiles/' ;

        return $res;

    }

   
	
}

?>