<?php      
namespace floctopus\controllers\expenses;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;
use \floctopus\models\orm\exp_categories as OrmExpCat;
use \floctopus\models\orm\expenses as OrmExp;

use \floctopus\models\libs\pagination as pagination;

class categories extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_expenses = app::$lang->expenses;
        $this->view->setPath(app::$device.'/expenses');   
		//$this->misc = new OrmMisc();
        $this->expcat = new OrmExpCat();
        $this->exp = new OrmExp();
		if(!isset($_SESSION['EXPCAT_RET_URL']))$_SESSION['EXPCAT_RET_URL']="/expenses";
		if(!isset($_SESSION['EXPCAT_PAGE']))$_SESSION['EXPCAT_PAGE']=1;
		$this->view->sbMoney = "active";
		$this->view->subExpensesCat = "active";
		
		$catcount = $this->expcat->getListCount();
		if(!$catcount){
			
			$adddefcat['expcat_user_id'] = $_SESSION['account']['user_id'];;
			$adddefcat['expcat_default'] = 1;
			$adddefcat['expcat_name'] = app::$lang->expenses['defcat'];
			$this->expcat->add($adddefcat);
		}
	}   

    function __default($args = false) {

        $this->view->topCategory = "active";
        $this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('categories.tpl');
        return $this->view;
    }
    
    function getContent(){


        $pages = new pagination();
        $where = array();
        $search = array();
        $where['expcat_trash'] = 0;
        $where['expcat_parent_id'] = 0;
        $where['expcat_user_id'] = $_SESSION['account']['user_id'];

        $totalRec = $this->expcat->getListCount($where, $search);

        $page = isset($_GET['p']) && $_GET['p'] ? \abs((int)$_GET['p']) : 1;
        $itemsOnPage =  $_SESSION["iop"];

        $pagination = $pages->calculate_pages($totalRec, $itemsOnPage, $page);
        $order = 'expcat_id ASC';

        $records = $this->expcat->getList($where, $pagination['current'], $itemsOnPage, $search, $order);


		foreach($records as $k=>$v){
			
			$selSubCat['expcat_parent_id'] = $v['expcat_id'];
			$sub = $this->expcat->getAllList($selSubCat);
			$records[$k]['sub']  = $sub;
			
		}
		
		$this->view->records = $records;


        $this->view->setTemplate('page_categories.tpl');
        $html = $this->view->render();


        $res['returl'] = $_SESSION['EXPCAT_RET_URL'];
        $res['newCat'] = app::$lang->expenses['newCat'];
        $res['nameCat'] = app::$lang->expenses['nameCat'];
        $res['btnsave'] = app::$lang->common['btn_save'];
        $res['btnclose'] = app::$lang->common['btn_close'];
        $res['html'] = $html;
        $res['count'] = $totalRec;
        $res['pag'] = $pagination;
        return $res;


    }
	
	function doAdd(){
        $name = app::strings_clear($_POST['cat_name']);
        if($_POST['issub']==0)
        $add = array(
            "expcat_user_id" => $_SESSION['account']['user_id'],
            "expcat_name" =>  $name,
        );
        else
        $add = array(
            "expcat_user_id" => $_SESSION['account']['user_id'],
            "expcat_name" =>  $name,
            "expcat_parent_id" => $_POST['parent'],
        );
        $this->expcat->add($add);
        if($_POST['issub']==1){
            $res['issub'] = $_POST['issub'];
            $res['name'] = $name;
            $res['id'] = $_POST['parent'];
            $res['status'] = true;
            return $res;
        }else{
            $res['status'] = true;
            return $res;
        }
    }

    function doUpdate(){
        $name = app::strings_clear($_POST['cat_name']);

            $upd = array(
                "expcat_user_id" => $_SESSION['account']['user_id'],
                "expcat_name" =>  $name,
            );


        $this->expcat->update($_POST['expcat_id'], $upd);

        if($_POST['parent']==0){
            $res['status'] = true;
            return $res;
        }else{
            $res['issub'] = $_POST['parent'];
            $res['name'] = $name;
            $res['id'] = $_POST['expcat_id'];
            $res['status'] = true;
            return $res;
        }
    }

    function viewAddSubCat(){
        $id = $_GET['id'];
        $this->view->cat = $id;
        $where['expcat_parent_id']=0;
        $where['expcat_default']=0;

        $catlist = $this->expcat->getAllList($where);
        $s ="";
        foreach($catlist as $k=>$v){
            if($v['expcat_id']==$id)$selected = "selected"; else $selected="";

            $s.='<option '.$selected.' value="'.$v['expcat_id'].'">'.$v['expcat_name'].'</option>';

        }
        $res['html'] = $s;
        $res['status']= true;
        return $res;
    }

    function editCat(){
        $id = $_GET['id'];
        $this->view->expcat = $this->expcat->getByID($id);
       // app::trace($this->view->expcat);
        $this->view->setTemplate('edit_categories.tpl');
        $html = $this->view->render();
        $res['html'] = $html;
        $res['status'] = true;
        //app::trace($res);
        return $res;
    }

    function delSubCat(){
        $id = $_POST['id'];
        $this->expcat->delSubCat($id);
        $res['status'] = true;
        return $res;
    }

    function delCat(){

        $id = $_POST['id'];
        $this->expcat->delCat($id);
        //$this->delSubCat();
        $res['status'] = true;
        return $res;
    }


}
