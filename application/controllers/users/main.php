<?php      
namespace floctopus\controllers\users;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;   
use \floctopus\models\orm\notifications as OrmNotifications;  
use \floctopus\models\orm\accounts as OrmAcc;
use \floctopus\models\orm\colloborators as OrmCollab;
use \floctopus\models\orm\clients as OrmClients;
use \floctopus\models\orm\misc as OrmMisc; 
use \floctopus\models\orm\country as Countries; 
use \floctopus\models\logic\jetMail as jmail; 
use \floctopus\models\libs\pagination as pagination;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_users = app::$lang->users;
    	$this->view->lng_profile = app::$lang->profile;
        $this->view->setPath(app::$device.'/users');   
        $this->view->page_title = app::$lang->users['title'];
    	$this->users = new OrmAcc();
    	$this->collab = new OrmCollab();
		if(!isset($_SESSION['USER_RET_URL']))$_SESSION['USER_RET_URL']="/users";
		if(!isset($_SESSION['USER_PAGE']))$_SESSION['USER_PAGE']=1;
		if(!isset($_SESSION['USER_INVITE_PAGE']))$_SESSION['USER_INVITE_PAGE']=1;
		$this->view->invitecount = $this->collab->countIncomingInvites();
		$this->view->sbPeople = "active";
		
		
	}   
	
    function __default($args = false) {   
		$this->view->subCollabs = "active";
		$this->view->topAccepted = "active";
		$this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 

	function getContent(){
		
    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['cb_status'] = 3;	
	    $totalRec = $this->collab->getListCount($where,$search);
    	$page = $_SESSION['USER_PAGE'];
	    $itemsOnPage = $_SESSION["account"]['user_rop'];

	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		
		$order ='users.user_name ASC';
	    
	    $this->view->totalCount=$this->collab->getListCount();

	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
 
    	$this->view->records = $this->collab->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
   	
   		$this->view->meta_title = "Your Team";

        $this->view->setTemplate('page.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;
        $res['returl'] = $_SESSION['USER_RET_URL'];
        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
	}

	function getContentInvites(){

   
    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['cb_status'][] = 2;	
	    $where['cb_status'][] = 1;	
	    
	    $totalRec = $this->collab->getListCount($where,$search);
	
	    
    	$page = $_SESSION['USER_INVITE_PAGE'];
	    $itemsOnPage =  $_SESSION["account"]['user_rop'];
 
	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		
		$order ='cb_status DESC';
	    
	    $this->view->totalCount=$this->collab->getListCount($where);
	    
	    //app::trace($this->view->totalCount);
	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
 
    	$this->view->records = $this->collab->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
   	
   		//$this->view->meta_title = "Your Team";

        $this->view->setTemplate('page_invite.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer_invite.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;

        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
		
		
	}
	
	function invites(){
		$this->view->subInvites = "active";
		$this->view->topRquest = "active";
		$this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('invites.tpl');  
        return $this->view;
		
	}
	
	function filterz(){
		
		$s = $_GET['s'];
		
	    if(!strlen(trim($s))){
		    $res['status'] = false;
		    return $res;
	    }
		
	    $res = array();
	    $search = array();
	    $where['user_trash']=0; 
	    $rez = $this->makeTags($_GET['s']);		
	    $srch=$rez['tags'];
	    
	    
	    $where['user_id!'] = $_SESSION['account']['user_id'];
	    $search['user_username'] = $srch;
	    $search['user_email'] = $srch;
	    $search['user_name'] = $srch;
	    $users = $this->users->getList($where,1,20,$search);
	    
	    if(count($users)){
		    foreach($users as $k=>$v){
			    $res['email'][$k] = $v['user_email'];
			    $res['username'][$k] = $v['user_username'];
			    $res['name'][$k] = $v['user_name'];
			    $res['id'][$k] = $v['user_id'];
		    }
		     $res['status'] = true;
	    }else{
		    $res['status'] = false;
	    }
	    
	   
	    return $res;
	}
	
	function doSendInvite(){
		$this->view->subInvites = "active";
		$m = new jmail();
		$email = strtolower(trim($_POST['email']));
		
		if($email==$_SESSION['account']['user_email']){
			$res['err'] = "own";	

			$res['status'] = false;
			return $res;
			
		}
		
		$isregistered = $this->users->findByEmail($email);
		
		if($isregistered ){
			$collab = new OrmCollab();
			$isCollab = $collab->isCollab($_SESSION['account']['user_id'],$isregistered['user_id']);

			if($isCollab['cb_status']==3){
				
				$res['title'] = app::$lang->common['operation_failed'];
				$res['msg'] = app::$lang->users['user_collab'];
				$res['err'] = "friend";
				$res['status'] = false;
				return $res;
			}
			

			if($isCollab['cb_status']==1){
				
				$res['title'] = app::$lang->common['operation_failed'];
				$res['msg'] = app::$lang->users['user_already_invited'];
			    $res['err'] = "friend";
				$res['status'] = false;
				return $res;
					
			}

			if($isCollab['cb_status']==2){
				
				$res['title'] = app::$lang->common['operation_failed'];
				$res['msg'] = app::$lang->users['user_already_receive'];
			    $res['err'] = "friend";
				$res['status'] = false;
				return $res;
					
			}
			
			$res['err'] = "exist";	
			$res['msg'] = "";
			$res['user_name'] = $isregistered['user_name'];
			$res['user_username'] = $isregistered['user_username'];
			$res['user_email'] = $isregistered['user_email'];
			$res['user_id'] = $isregistered['user_id'];
			$this->view->u = $isregistered;
			
			$this->view->setTemplate('user_in_system.tpl'); 
			$res['html'] = $this->view->render();
			
			$res['status'] = false;
			return $res;
		}
		
		$key = uniqid();
		$sent =$m->sendInvite($email,$key);
		
		$addinv['invite_from'] = $_SESSION['account']['user_id'];
		$addinv['invite_email'] = $email;
		$addinv['invite_key'] = $key;
		$this->collab->addInvite($addinv);
		
		if($sent['status']){
			$res['title'] = app::$lang->users['inite_sent'];	
			$res['msg'] = "";		
			$res['status'] = true;
			$res['err'] = "sent";
			return $res;
			
		}else{
			
			$res['title'] = app::$lang->users['inite_not_sent'];	
			$res['msg'] = "";	
			$res['err'] = "not_send";	
			$res['status'] = false;
			return $res;
		}
	}
	
	function add(){
		$this->view->subCollabs = "active";
        $this->view->setTemplate('add.tpl');  
        return $this->view;
		
		
	}
	
	function doInvite(){
		$this->view->subInvites = "active";
	    $notify = new OrmNotifications();
		
		$collab = new OrmCollab();
		
		$isCollab = $collab->isCollab($_SESSION['account']['user_id'],$_POST['id']);
		
		if($isCollab){
			
			
			if($isCollab['cb_status']==3){
				
				$res['title'] = app::$lang->common['operation_failed'];
				$res['msg'] = app::$lang->users['user_collab'];
				$res['status'] = false;
				return $res;
					
			}
			

			if($isCollab['cb_status']==1){
				
				$res['title'] = app::$lang->common['operation_failed'];
				$res['msg'] = app::$lang->users['user_already_invited'];
				$res['status'] = false;
				return $res;
					
			}

			if($isCollab['cb_status']==2){
				
				$res['title'] = app::$lang->common['operation_failed'];
				$res['msg'] = app::$lang->users['user_already_receive'];
				$res['status'] = false;
				return $res;
					
			}

			
		}
		
		$add['cb_user_id'] = $_SESSION['account']['user_id'];
		$add['cb_collobarator_id'] = $_POST['id'];
		$collab->add($add);

		$add2['cb_user_id'] = $_POST['id'];
		$add2['cb_collobarator_id'] = $_SESSION['account']['user_id'];
		$add2['cb_status'] = 2;
		$collab->add($add2);
		
        $addn['n_user_id'] = $_POST['id'];
        $addn['n_src'] = 1;
        $addn['n_object_id'] = $_SESSION['account']['user_id'];
	    $addn['n_type'] = "invite_received";
	    $addn['n_link'] ="/users/invites";
	  
        $notify->add($addn);
		
		$u = $this->users->getAccount($_POST['id']);
		$m = new jmail();		
		$m->notifyInvite($u['user_email']);
		$res['status'] = true;
		return $res;
	}
	
	function doAccept($id=0){
		$this->view->subInvites = "active";
		$collab = new OrmCollab();
		$notify = new OrmNotifications();
		
		if(!$id)$id=$_POST['id'];
		$collabrec = $collab->getByID($id);
		
		if(!$collabrec){
			
			$res['status'] = false;
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->users['no_invite'];
			return $res;
		}
		
		$collabinfo = $this->users->getAccount($collabrec['cb_collobarator_id']);

	    if($collabinfo['user_addr_pub']){
		    $upd['cb_billing_name'] = $collabinfo['user_name'];
		    $upd['cb_billing_state'] = $collabinfo['user_state'];
		    $upd['cb_billing_city'] = $collabinfo['user_city'];
		    $upd['cb_billing_zip'] = $collabinfo['user_zip'];
		    $upd['cb_billing_street'] = $collabinfo['user_street'];
		    $upd['cb_billing_country'] = $collabinfo['user_country'];
	    }
		    
		$upd['cb_billing_name'] = $collabinfo['user_name'];
	    
		
		$upd['cb_status'] = 3;
		$collab->update($id,$upd);
		
		$selCol['cb_user_id'] = $collabrec['cb_collobarator_id'];
		$selCol['cb_collobarator_id'] = $collabrec['cb_user_id'];
		$selCol['cb_status'] = 1;
		$inviter = $collab->getInfo($selCol);
		
		
	    if($_SESSION['account']['user_addr_pub']){
		   
		    $upd2['cb_billing_state'] = $_SESSION['account']['user_state'];
		    $upd2['cb_billing_city'] = $_SESSION['account']['user_city'];
		    $upd2['cb_billing_zip'] = $_SESSION['account']['user_zip'];
		    $upd2['cb_billing_street'] = $_SESSION['account']['user_street'];
		    $upd2['cb_billing_country'] = $_SESSION['account']['user_country'];
	    }
	    $upd2['cb_billing_name'] = $_SESSION['account']['user_name'];
		$upd2['cb_status'] = 3;
		
		$collab->update($inviter['cb_id'],$upd2);

        $addn['n_user_id'] = $collabrec['cb_collobarator_id'];
        $addn['n_src'] = 1;
        $addn['n_object_id'] = $_SESSION['account']['user_id'];
        $addn['n_type'] = "invite_accept";
        $addn['n_link'] = "/users";
        $notify->add($addn);
		
		$res['status'] = true;
		return $res;
	}
	
	function delete($id=0){
		$notify = new OrmNotifications();
		if(!$id)$id = $_POST['id'];

		$collabrec = $this->collab->getByID($id);
		
		$selCol['cb_user_id'] = $collabrec['cb_collobarator_id'];
		$selCol['cb_collobarator_id'] = $collabrec['cb_user_id'];
		//$selCol['cb_status'] = 1;
		$inviter = $this->collab->getInfo($selCol);

		$this->collab->del($id);	
		$this->collab->del($inviter['cb_id']);
		$this->collab->delFromPrj($collabrec['cb_collobarator_id']);
		// Р”РѕР±Р°РІРёС‚СЊ СѓРґР°Р»РµРЅРёРµ РёР· РїСЂРѕРµРєС‚Р°
		
		
        $addn['n_user_id'] = $collabrec['cb_collobarator_id'];
        $addn['n_src'] = 1;
        $addn['n_object_id'] = $_SESSION['account']['user_id'];
        $addn['n_type'] = "colab_del";
        $addn['n_link'] = "/users";
        $notify->add($addn);
		
		$res['status'] = true;
		return $res;
	}

    function deleteBundle(){

	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){
			    $z = $this->delete($val);
			    
			    if($z['status'])$this->logit("users","del","User deleted",$val);
		    }
		    $res['status']=true;
		    return $res;

		    
	    }else{
		    
		    $res['status']=false;
		    $res['title']="Operation Failed";
		    $res['msg']="No rows selected. Please select at least one row.";
		    return $res;
	    }
    }
    
    function acceptBundle(){

	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){
			    $z = $this->doAccept($val);
			    
			    if($z['status'])$this->logit("users","accept","User Invitation Accepted",$val);
		    }
		    $res['status']=true;
		    return $res;

		    
	    }else{
		    
		    $res['status']=false;
		    $res['title']="Operation Failed";
		    $res['msg']="No rows selected. Please select at least one row.";
		    return $res;
	    }
    }

	function changePage(){
		
		$_SESSION['USER_PAGE'] = $_POST['p'];
		
		$res['status'] = true;
		return $res;
	}    
	
	function getProfile(){
		
		if(!isset($_GET['id'])){
			
			$res['status'] = false;
			return $res;
		}
		
		$id = $_GET['id'];
		$user = $this->collab->getByID($id);
		
		if(!$user){
			$res['status'] = false;
			return $res;
		}
		
		
		$misc = new OrmMisc(); 
		$country = new Countries();
    	$this->view->proff = $misc->getProfessionNameByID($user['user_proffession']);
    	$this->view->country = $country->getCountryName($user['user_country']);
		$this->view->user = $user;
		$this->view->collabid = $id;
        $this->view->setTemplate('profile_view.tpl');		
		
		$html = $this->view->render();
		
		$res['html'] = $html;
		$res['status'] = true;
		return $res;
	}
	
	
	function getUserProfile(){
		
		if(!isset($_GET['id'])){
			
			$res['status'] = false;
			return $res;
		}
		
		$id = $_GET['id'];
		$user = $this->users->getAccount($id);
		
		if(!$user){
			$res['status'] = false;
			return $res;
		}
		
		
		$misc = new OrmMisc(); 
		$country = new Countries();
    	$this->view->proff = $misc->getProfessionNameByID($user['user_proffession']);
    	$this->view->country = $country->getCountryName($user['user_country']);
		$this->view->user = $user;
		$this->view->collabid = $id;
        $this->view->setTemplate('profile_view.tpl');		
		
		$html = $this->view->render();
		
		$res['html'] = $html;
		$res['status'] = true;
		return $res;
	}
	
	
}