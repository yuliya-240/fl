<?
namespace floctopus\controllers\calendar;

use \floctopus\application as app;
use \floctopus\models\common\adminController as adminController;
use \floctopus\models\orm\clients as OrmClients;
use \floctopus\models\orm\colloborators as OrmCollabs;
use \floctopus\models\orm\calendar as OrmCalendar;
use \floctopus\models\orm\issues as OrmIssues;

class main extends adminController{

    function __before() {

        parent::__before();
        $this->view->lng_calendar = app::$lang->calendar;
        $this->view->lng_invoices = app::$lang->invoices;
        $this->view->setPath(app::$device.'/calendar');
        $this->cal = new OrmCalendar();
		$this->client = new OrmClients();
	    $this->collabs = new OrmCollabs();
		$this->issue = new OrmIssues();
		
        $this->view->sbCalendar = "active";
        $this->view->subCalendar = "active";
    }
    
    function __default($args = false) {   
		
		$this->view->mf = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
		
		$this->view->dttime = \date($_SESSION['account']['user_tformat'],strtotime("+1 hour"));
		$this->view->types = $this->cal->getEventsTypes();
		$selClients['client_collab'] = 0;
		$this->view->clients = $this->client->getAllList($selClients);
		$this->view->collabs = $this->collabs->getAllList(array(),array(),"users.user_name ASC");

		
        $this->view->setTemplate('index.tpl');
         
        return $this->view;
    } 

    function add() {   

		$this->view->dttime = \date($_SESSION['account']['user_tformat'],strtotime("+1 hour"));
		$this->view->types = $this->cal->getEventsTypes();
		$selClients['client_collab'] = 0;
		$this->view->clients = $this->client->getAllList($selClients);
		$this->view->collabs = $this->collabs->getAllList(array(),array(),"users.user_name ASC");
		
        $this->view->setTemplate('new_event.tpl');
         
        $res['html'] =  $this->view->render();
        $res['status'] = true;
        return $res;
    } 
	
	function doAdd(){

		if($_POST['client']!="0"){
			$str = $_POST['client'];
			list($cid,$at) = explode("@", $str);
			if($at == "cli")$addr['er_client_id'] = $cid; else $addr['er_collab_id'] = $cid;
			if($at == "cli")$add['e_client_id'] = $cid; else $add['e_collab_id'] = $cid;
		}
			
		if($_SESSION['account']['user_dformat']=="d/m/Y"){
			list($day,$month,$year) = explode('/',$_POST['deadline_date']);
			$deadline = $year."-".$month."-".$day;
		}else{
			$deadline = $_POST['deadline_date'];
		}
		
		if(!isset($_POST['recurrance']))$recurrance=0; else $recurrance = $_POST['recurrance'];
		
		if(!$recurrance){
			
			$add['e_user_id'] = $_SESSION['account']['user_id'];
			
			$add['e_type'] = $_POST['e_type'];
			$add['e_title'] = app::strings_clear($_POST['title']);
			$add['e_date'] = \date("Y-m-d H:i:s",strtotime($deadline." ".$_POST['deadline_time']));
			$add['e_location'] = app::strings_clear($_POST['location']);
			$add['e_desc'] = app::strings_clear($_POST['e_desc']);
			$this->cal->add($add);
			
		}else{
			
			$addr['er_user_id'] = $_SESSION['account']['user_id'];
			$addr['er_title'] = app::strings_clear($_POST['title']);
			$addr['er_location'] = app::strings_clear($_POST['location']);
			$addr['er_type'] = $_POST['e_type'];
			$addr['er_freq'] = $_POST['er_repeat'];
			$addr['er_time'] = $_POST['deadline_time'];
			$addr['er_date_start'] = \date("Y-m-d",strtotime($_POST['er_date_start']));
			$addr['er_end'] = $_POST['er_end'];
			$add['e_desc'] = app::strings_clear($_POST['e_desc']);
			$startTS = strtotime($_POST['er_date_start']);
			
			if($_POST['er_end']==2){
				
				
				$endTS = strtotime($_POST['er_end_date']);
				
				if($startTS>$endTS){
					
					$res['status'] = false;
					$res['title'] = app::$lang->common['operation_failed'];
					$res['msg'] = app::$lang->common['stendt_err'];
					return $res;
				}
				
				$addr['er_end_date'] = \date("Y-m-d",strtotime($_POST['er_end_date']));
			}

			if($_POST['er_end']==3){
				if(!$_POST['er_end_count']){
					
					$res['status'] = false;
					$res['title'] = app::$lang->common['operation_failed'];
					$res['msg'] = app::$lang->common['no_count_err'];
					return $res;
					
				}
				
				$addr['er_end_count'] = $_POST['er_end_count'];
			}


			switch ($_POST['er_repeat']){
				
				case 'd':
					if(!isset($_POST['daily_days_count']) || !$_POST['daily_days_count'])$_POST['daily_days_count']=1;
					$addr['er_daily_option'] = $_POST['daily_option'];
					$addr['er_daily_days_count'] = $_POST['daily_days_count'];
				break;
				
				
				case 'w':
					if(!isset($_POST['weekly_count']) || !$_POST['weekly_count'])$_POST['weekly_count'] =1;
					$addr['er_weekly_count'] = $_POST['weekly_count'];
					foreach($_POST['weekday'] as $val){
						$addr['er_w'.$val]=1;
					}
					
				break;
				
				case 'm':
					if(!isset($_POST['month_count1']) || !$_POST['month_count1'])$_POST['month_count1']=1;
					if(!isset($_POST['month_day']) || !$_POST['month_day'])$_POST['month_day']=1;
					if(!isset($_POST['month_count2']) || !$_POST['month_count2'])$_POST['month_count2']=1;
					
					$addr['er_monthly_option'] = $_POST['monthly_option'];
					$addr['er_month_day'] = $_POST['month_day'];
					$addr['er_month_count1'] = $_POST['month_count1'];
					$addr['er_month_day_number'] = $_POST['month_day_number'];
					$addr['er_month_week_day'] = $_POST['month_week_day'];
					$addr['er_month_count2'] = $_POST['month_count2'];
				break;
				
				case 'y':
					if(!isset($_POST['year_day']) || !$_POST['year_day'])$_POST['year_day']=1;
					$addr['er_yarly_option'] = $_POST['yarly_option'];
					$addr['er_year_month1'] = $_POST['year_month1'];
					$addr['er_year_day'] = $_POST['year_day'];
					$addr['er_year_day_number'] = $_POST['year_day_number'];
					$addr['er_year_week_day'] = $_POST['year_week_day'];
					$addr['er_year_month2'] = $_POST['year_month2'];
					
				break;
				
			}
			$recEventID = $this->cal->addRecurring($addr);
			
		}
		
		
		
		$res['status'] = true;
		return $res;

	}    
    
    function getEvents(){
	    
	   $where['DATE(e_date)>'] = $_GET['start'];
	   $where['DATE(e_date)<'] = $_GET['end'];
	   
	   $this->cal->fillRecurring($_GET['start'],$_GET['end'],$_SESSION['account']['user_id']);
	   
	   $a = $this->cal->getEvents($where);

		foreach($a as $k=>$v){
			
			$a[$k]['allDay'] = intval($v['allDay']);
			$a[$k]['close'] = intval($v['close']);
			
		}
		
	    return $a;
	    
    }
    
    function getEvent(){
	    $where['e_id'] = $_GET['id'];
	    
	    $r = $this->cal->getEventInfo($where);
	    
	    if($r['e_type']==3){
		    
		    $issue = $this->issue->getByID($r['e_object_id']);
		    $issue['moment'] = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
		    $res['issue'] = $issue;
		    
	    }
	    
	
	    
	    $res['status'] = true;
	    $res['info'] = $r;
	    return  $res;
	   
    }
    
    function deleteEvent(){
	    $where['e_id'] = $_POST['e_id'];
	    $where['e_user_id'] = $_SESSION['account']['user_id'];	
	    
	    return $this->cal->deleteEvent($where);
    }
    
    function completeEvent(){
	    $where=array(
		    'e_id' => $_POST['e_id'],
		    'e_user_id' => $_SESSION['account']['user_id']
	    );
	    $data=array(
		    'e_result' => $_POST['e_result'],
		    'e_close' => 1
	    );
	    
	    return $this->cal->completeEvent($where, $data);
    }
    
    function editEvent(){
	    if($_SESSION['account']['user_dformat']=="d/m/Y"){
			list($day,$month,$year) = explode('/',$_POST['deadline_date']);
			$deadline = $year."-".$month."-".$day;
		}else{
			$deadline = $_POST['deadline_date'];
		}

	    $where=array(
		    'e_id' => $_POST['e_id'],
		    'e_user_id' => $_SESSION['account']['user_id']
	    );
	    
	    $data=array(
		    'e_type' => $_POST['e_type'],
			'e_title' => app::strings_clear($_POST['title']),
			'e_date' => \date("Y-m-d H:i:s",strtotime($deadline." ".$_POST['deadline_time'])),
			'e_location' => app::strings_clear($_POST['location']),
			'e_desc' => app::strings_clear($_POST['e_desc'])

	    );
	    if ($_POST['rec_id'] == 0) 
	    	return $this->cal->editEvent($where, $data);
		else {
			$data['e_recurring_id'] = $_POST['rec_id'];
			$data['e_user_id'] = $_SESSION['account']['user_id'];
			
			$this->cal->deleteEvent($where);
			
			$this->cal->add($data);
			
			return true;
			};
	    //app::trace($data['e_date']);
	        }
    
    function dropEvent(){
	    $where=array(
		    'e_id' => $_POST['e_id'],
		    'e_user_id' => $_SESSION['account']['user_id']
	    );
	    
	    $data=array(
			'e_date' => \date("Y-m-d H:i:s",strtotime($_POST['new_date']))
		);
	    
	    $event = $this->cal->getEventInfo($where);
	    
	    if ($event['e_recurring_id'] == 0) {
			$this->cal->editEvent($where, $data);
	    	return $where['e_id']; 
	    }
	    	
		else {
			$data['e_type'] = $event['e_type'];
			$data['e_title'] = $event['e_title'];
			$data['e_location'] = $event['e_location'];
			$data['e_desc'] = $event['e_desc'];
			$data['e_user_id'] = $_SESSION['account']['user_id'];
			$data['e_recurring_id'] = $event['e_recurring_id'];
			
			$this->cal->deleteEvent($where);
			
			return $this->cal->add($data);
			
			};
	    	
    }
    
    function reopenEvent(){
	    $where=array(
		    'e_id' => $_POST['e_id'],
		    'e_user_id' => $_SESSION['account']['user_id']
	    );
	    $data['e_close'] = 0;
	    
	    return $this->cal->editEvent($where, $data);
    }
    
    

}

