<?php      
namespace instantinvoices\controllers\cron;

use \instantinvoices\application as app;     
use \jet\controller\http as HTTPController;   
use \jet\libs\pquery as PARSER; 
use pQuery;

class crawler extends HTTPController {

    function __before() {
        

		PARSER::load();
    }
    
    function __default($args = null) {  
		
		
		
		$this->upWork();
		//$this->freelancer();
    } 
     
    function getContent($url){
	    
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL,$url );
		curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		$result = curl_exec($curl);
		
		return $result;
	    
    }
    
    function upWork(){

		//https://www.upwork.com/o/jobs/browse/c/web-mobile-software-dev/?page=2
		//https://www.upwork.com/o/jobs/browse/c/web-mobile-software-dev/?page=3
		$url="https://www.upwork.com/o/jobs/browse/c/web-mobile-software-dev/";
		$result = $this->getContent($url);

		$arr = array();		
		
 		$doc = \phpQuery::newDocumentHTML($result);
 		
 		
 		foreach( \pq('a.break',$doc->getDocumentID()) as $key=>$element) {
			
			$arr[$key]['title']=trim($element->textContent);
			$arr[$key]['url']="https://www.upwork.com".trim($element->getAttribute("href"));
 		}

		foreach( \pq('.js-type',$doc->getDocumentID()) as $key=>$element) {

			$arr[$key]['jobtype']=trim($element->textContent);
 		}
 		
 		foreach( \pq("time[itemprop='datePosted']") as $key=>$element) {
			$arr[$key]['time']=trim($element->getAttribute("datetime"));
 		}

 		foreach(  \pq('span[itemprop="baseSalary"]',$doc->getDocumentID()) as $key=>$element) {
			$budget[]=trim($element->textContent);
 		}
		$i=0;
		
		if(count($budget)){
			
			foreach($arr as $k=>$v){
				
				if($v['jobtype']=="Fixed-Price"){
					$arr[$k]['budget']=$budget[$i];
					$i++;
				}
			}
		}
		
 		foreach(  \pq('.description]',$doc->getDocumentID()) as $key=>$element) {
			$arr[$key]['desc']=app::strings_clear($element->textContent);
 		}
 		
  		foreach(  \pq('.js-skills]',$doc->getDocumentID()) as $key=>$element) {
			
			$dr = explode("\n",\trim($element->textContent));
			$tags = "";
			foreach($dr as $z=>$h){
				if($z>0){
					$st=\trim($h);
					if(strlen($st))$tags.=$st.",";
				}
			}
			$arr[$key]['tags'] = $tags;

 		}
		
 	    
	    app::trace($arr);
    }
    
    function freelancer(){
	   
		//$url="https://www.freelancer.com/jobs/1/";
		//$d = $this->crawl_page($url);

		//https://www.upwork.com/o/jobs/browse/c/web-mobile-software-dev/?page=2
		//https://www.upwork.com/o/jobs/browse/c/web-mobile-software-dev/?page=3
		$url="https://www.freelancer.com/jobs/1/";
		$result = $this->getContent($url);

		$arr = array();		
		
 		$doc = \phpQuery::newDocumentHTML($result);
 		
 		
 		foreach( \pq('td',$doc->getDocumentID()) as $key=>$element) {
			
			
			app::trace($element);
			//$arr[$key]['title']=trim($element->textContent);
			//$arr[$key]['url']="https://www.upwork.com".trim($element->getAttribute("href"));
 		}

		
		
		
		/*$tables = $d->getElementsByTagName('table');
		$rows = $tables->item(0)->getElementsByTagName('tr');
			foreach ($rows as $row) {
			    $cols = $row->getElementsByTagName('td');
			    $betreffzeile.=$cols->item(0)->nodeValue;
			    
			}		
		app::trace($betreffzeiles);
 		*/
	    
    }

	function crawl_page($url, $depth = 1)
	{
	    static $seen = array();
	    if (isset($seen[$url]) || $depth === 0) {
	        return;
	    }
	
	    $seen[$url] = true;
	
	    $dom = new \DOMDocument('1.0');
	    @$dom->loadHTMLFile($url);
		
		
	    //$html = $dom->getElementsByTagName('a');
	   
	   /* foreach ($anchors as $element) {
	        $href = $element->getAttribute('href');
	        if (0 !== strpos($href, 'http')) {
	            $path = '/' . ltrim($href, '/');
	            if (extension_loaded('http')) {
	                $href = http_build_url($url, array('path' => $path));
	            } else {
	                $parts = parse_url($url);
	                $href = $parts['scheme'] . '://';
	                if (isset($parts['user']) && isset($parts['pass'])) {
	                    $href .= $parts['user'] . ':' . $parts['pass'] . '@';
	                }
	                $href .= $parts['host'];
	                if (isset($parts['port'])) {
	                    $href .= ':' . $parts['port'];
	                }
	                $href .= $path;
	            }
	        }
	        crawl_page($href, $depth - 1);
	    }*/
	    
	    return $dom;
	    //echo "URL:",$url,PHP_EOL,"CONTENT:",PHP_EOL,$dom->saveHTML(),PHP_EOL,PHP_EOL;
	}    
    
}
