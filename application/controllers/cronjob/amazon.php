<?php      
namespace floctopus\controllers\cronjob;

use \floctopus\application as app;
use \jet\controller\http as HTTPController;  
use \floctopus\models\logic\session as Session;
use \floctopus\models\orm\failmail as OrmFM;
use \floctopus\models\orm\sentmail as OrmSM;
use \floctopus\models\orm\invoices as OrmInv;
use \floctopus\models\orm\notifications as OrmNotify;
use \jet\libs\Amazon as AWAMZON;
use Aws\Common\Credentials\Credentials;
use Aws\Sqs\SqsClient;
use \floctopus\models\common\jetMail as jmail;

class amazon extends HTTPController {
	
	
	function endpoint4(){
					
			//For Debugging.
			$logToFile = true;
			
			//Should you need to check that your messages are coming from the correct topicArn
			$restrictByTopic = false;
			$allowedTopic = "arn:aws:sns:us-east-1:102027900582:ses-bounce";
			
			//For security you can (should) validate the certificate, this does add an additional time demand on the system.
			//NOTE: This also checks the origin of the certificate to ensure messages are signed by the AWS SNS SERVICE.
			//Since the allowed topicArn is part of the validation data, this ensures that your request originated from
			//the service, not somewhere else, and is from the topic you think it is, not something spoofed.
			$verifyCertificate = true;
			$sourceDomain = "sns.us-east-1.amazonaws.com";
			 
			
			//////
			//// OPERATION
			//////
			
			$signatureValid = false;
			$safeToProcess = true; //Are Security Criteria Set Above Met? Changed programmatically to false on any security failure.
			
			if($logToFile){
				////LOG TO FILE:
				$dateString = date("Ymd");
				$dateString = $_SERVER['DOCUMENT_ROOT'].'/logs/'.$dateString."_r.txt";
			
				$myFile = $dateString;
				$fh = fopen($myFile, 'w') or die("Log File Cannot Be Opened.");
			}
			
			
			//Get the raw post data from the request. This is the best-practice method as it does not rely on special php.ini directives
			//like $HTTP_RAW_POST_DATA. Amazon SNS sends a JSON object as part of the raw post body.
			$json = json_decode(file_get_contents("php://input"));
			
			//app::Log($json);
			
			//Check for Restrict By Topic
			
			//Check for Verify Certificate
			if($verifyCertificate){
			
				//Check For Certificate Source
				
				
				
				//Build Up The String That Was Originally Encoded With The AWS Key So You Can Validate It Against Its Signature.
				if($json->Type == "SubscriptionConfirmation"){
					$validationString = "";
					$validationString .= "Message\n";
					$validationString .= $json->Message . "\n";
					$validationString .= "MessageId\n";
					$validationString .= $json->MessageId . "\n";
					$validationString .= "SubscribeURL\n";
					$validationString .= $json->SubscribeURL . "\n";
					$validationString .= "Timestamp\n";
					$validationString .= $json->Timestamp . "\n";
					$validationString .= "Token\n";
					$validationString .= $json->Token . "\n";
					$validationString .= "TopicArn\n";
					$validationString .= $json->TopicArn . "\n";
					$validationString .= "Type\n";
					$validationString .= $json->Type . "\n";
				}else{
					$validationString = "";
					$validationString .= "Message\n";
					$validationString .= $json->Message . "\n";
					$validationString .= "MessageId\n";
					$validationString .= $json->MessageId . "\n";
					if($json->Subject != ""){
						$validationString .= "Subject\n";
						$validationString .= $json->Subject . "\n";
					}
					$validationString .= "Timestamp\n";
					$validationString .= $json->Timestamp . "\n";
					$validationString .= "TopicArn\n";
					$validationString .= $json->TopicArn . "\n";
					$validationString .= "Type\n";
					$validationString .= $json->Type . "\n";
				}
				
				if($logToFile){
					fwrite($fh, "Data Validation String:");
					fwrite($fh, $validationString);
				}
				
				
			}
			
			if($safeToProcess){
			
				//Handle A Subscription Request Programmatically
				if($json->Type == "SubscriptionConfirmation"){
					//RESPOND TO SUBSCRIPTION NOTIFICATION BY CALLING THE URL
					
					if($logToFile){
						fwrite($fh, $json->SubscribeURL);
					}
					
					$curl_handle=curl_init();
					curl_setopt($curl_handle,CURLOPT_URL,$json->SubscribeURL);
					curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
					curl_exec($curl_handle);
					curl_close($curl_handle);	
				}
				
				
				//Handle a Notification Programmatically
				if($json->Type == "Notification"){
					//Do what you want with the data here.
					//fwrite($fh, $json->Subject);
					//fwrite($fh, $json->Message);
				}
			}
			
			//Clean Up For Debugging.
			if($logToFile){
				ob_start();
				print_r( $json );
				$output = ob_get_clean();
			
				fwrite($fh, $output);
			
				////WRITE LOG
				fclose($fh);
			}
			
		
	}
	
	function aws(){
		AWAMZON::load();
		$fm = new OrmFM();
		$sm = new OrmSM();
		$inv = new OrmInv();
		
		$notify = new OrmNotify();
		//$credentials = new Credentials('AKIAI2GSN4VGMZWEKVHQ', '2T7cnYPHMV4nh3UfsljMw2f5rGTv5eKAXFG8DFqx');
		$credentials = new Credentials(app::$config->amazon['amazon_access_key'], app::$config->amazon['amazon_secret_key']);

		$client = SqsClient::factory(array(
			'credentials' => $credentials,
			'region' => 'us-east-1'	
		));	
		
		$queueUrl = 'https://sqs.us-east-1.amazonaws.com/102027900582/ses-bounces-queue';
		
		$result = $client->receiveMessage(array(
			'QueueUrl' => $queueUrl,
			'MaxNumberOfMessages' => 10,
			'VisibilityTimeout' => 30
		));
		//$a = $result->getPath('Messages/0');
		//app::trace($result);
		/*$messageId = $result->getPath('Messages/0/MessageId');
		$ReceiptHandle = $result->getPath('Messages/0/ReceiptHandle');
		app::trace($messageId);
		
		$b = $result->getPath('Messages/0/Body');
		
	     $obj = json_decode($b);
	
		app::trace( $obj);*/
		
		
		$interate = $result->getPath('Messages');
		
		
		if($interate){
		
		foreach ($result->getPath('Messages') as $message) {
			// Do something with the message
			//$msg['MessageId'] = $message['MessageId'];
			$ReceiptHandle = $message['ReceiptHandle'];
			
			//app::trace($ReceiptHandle);
			
			$body = json_decode($message['Body']);
			//app::trace($body);
			$bounceobj = json_decode($body->Message);
			
			if ($bounceobj->notificationType=='Bounce'){
				
				//$bj = app::trace($body->Message);
				//app::Log($bj);
				
				$bounceType = $bounceobj->bounce->bounceType;
				//$reportingMTA = $bounceobj->bounce->reportingMTA;
				
				if(isset($bounceobj->bounce->bouncedRecipients[0]))$bouncedRecipientsObj = $bounceobj->bounce->bouncedRecipients[0];
				if(isset($bouncedRecipientsObj->action))$bouncedRecipients_action = $bouncedRecipientsObj->action;
				if(isset($bouncedRecipientsObj->emailAddress))$bouncedRecipients_email = $bouncedRecipientsObj->emailAddress;
				if(isset($bouncedRecipientsObj->status))$bouncedRecipients_status = $bouncedRecipientsObj->status;
				if(isset($bouncedRecipientsObj->diagnosticCode))$bouncedRecipients_code = $bouncedRecipientsObj->diagnosticCode;
				
				$time = $bounceobj->bounce->timestamp;
				$datetime = \date('Y-m-d H:i:s',\strtotime($time));
				$bounceMailObj = $bounceobj->mail;
				$source = $bounceMailObj->source;
				$email_id = $bounceMailObj->messageId;
				
				$src = app::extractquotes($source);
				
				$add['fm_date'] = $datetime;
				$add['fm_type'] = $bounceType;
				//$add['fm_reportMTA'] = $reportingMTA;
				if (isset($bouncedRecipientsObj->action))$add['fm_action'] = $bouncedRecipients_action;
				$add['fm_email'] = $bouncedRecipients_email;
				if(isset($bouncedRecipientsObj->status))$add['fm_status'] = $bouncedRecipients_status;
				if(isset($bouncedRecipientsObj->diagnosticCode))$add['fm_code'] = $bouncedRecipients_code;
				if($src)$add['fm_source'] = app::extractquotes($source);
				$add['fm_mail_id'] = $email_id;
				$add['fm_fail_type'] = $bounceobj->notificationType;
				
				$fm->add($add);
				
				$result = $client->deleteMessage(array(
					'QueueUrl' => $queueUrl,
					'ReceiptHandle' => $ReceiptHandle
				));
				
				$email = $sm->getEmailByID($email_id);
				
				if($email){
				
				$m = new jmail();
				
				$sent =$m->sendFailedEmailNotification($email);

				$addnote['n_co_id'] = $email['sm_co_id'];
				$addnote['n_subj'] = 'Bad email address';
				$addnote['n_date'] = \date('Y-m-d H:i:s');
				$addnote['n_text'] = 'Invoice #<a href="https://secure.lawnprosoftware.com/finances/invoices/view/'.$email['sm_inv_id'].'">'.$email['sm_inv_num'].'</a> which was emailed to '.$email['sm_email'].' was returned as a bad email address.';
				
				$notify->add($addnote);
				
				
				$addIH['invhist_co_id'] = $email['sm_co_id'];
				$addIH['invhist_inv_id'] = $email['sm_inv_id'];
				$addIH['invhist_inv_id'] = $email['sm_inv_id'];
				$addIH['invhist_action'] = 'bounce';
				$addIH['invhist_comment'] = 'Invoice # '.$email['sm_inv_num'].' which was emailed to '.$email['sm_email'].' was returned as a bad email address.';
				$addIH['invhist_date'] = \date('Y-m-d H:i:s');
 				$inv->addInvHistory($addIH);
				
				}
			}
			
			
			
			//app::trace($bounce);
		}
		
		}
	}

		

}