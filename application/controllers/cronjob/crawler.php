<?php      
namespace instantinvoices\controllers\cron;

use \instantinvoices\application as app;     
use \jet\controller\http as HTTPController;  
use \instantinvoices\models\orm\jobs as OrmJobs; 
use \jet\libs\htmlparser as PARSER; 
use FastSimpleHTMLDom\Document;

class crawler extends HTTPController {

    function __before() {
        date_default_timezone_set ( "UTC" );
		$this->job = new OrmJobs();
		PARSER::load();
    }
    
    function __default($args = null) {  
			
		
		//$this->upWork();
		$this->freelancer();
		//$this->guru();
		//$this->pph();
		//$this->flexjobs();
    } 
    
    function upWork_html(){
		$data = array();

		
				
		
		$upurls = array(
			"https://www.upwork.com/o/jobs/browse/c/web-mobile-software-dev/",
			"https://www.upwork.com/o/jobs/browse/c/it-networking/",
			"https://www.upwork.com/o/jobs/browse/c/data-science-analytics/",
			"https://www.upwork.com/o/jobs/browse/c/engineering-architecture/",
			"https://www.upwork.com/o/jobs/browse/c/design-creative/",
			"https://www.upwork.com/o/jobs/browse/c/writing/",
			"https://www.upwork.com/o/jobs/browse/c/writing/",
			"https://www.upwork.com/o/jobs/browse/c/legal/",
			"https://www.upwork.com/o/jobs/browse/c/admin-support/",
			"https://www.upwork.com/o/jobs/browse/c/customer-service/",
			"https://www.upwork.com/o/jobs/browse/c/sales-marketing/",
			"https://www.upwork.com/o/jobs/browse/c/accounting-consulting/",
		);
		
		
		foreach($upurls as $upurl){

			
			$html = \FastSimpleHTMLDom\Document::file_get_html($upurl);
			
			foreach($html->find('.job-tile') as $key=>$v){
				$data[$key]['id'] = $v->id;   
				$a = $v->find('a.break',0);
				if(is_object($a)){
					   	
					$data[$key]['title'] =   trim($a->plaintext); 
					$data[$key]['url'] =   "https://www.upwork.com".trim($a->href); 
					
				}
				$type = \trim($v->find('.js-type',0)->plaintext);
				$data[$key]['type'] = $type;
					
				if($type=="Hourly"){
					$b = \trim($v->find('.js-duration',0)->plaintext);
					 $budget = explode(",",$b);
					 
					 $data[$key]['duration'] = \trim($budget[1]);
					 $data[$key]['budget'] = "";
					
				}else{
					 $data[$key]['budget']= \trim($v->find('span[itemprop="baseSalary"]',0)->plaintext);
					 $data[$key]['duration'] = "";
				}
				
				$data[$key]['desc'] = trim($v->find('.description',0)->plaintext);		   
				$datetag = $v->find('time',0);
					
				if(is_object($datetag)){
						
					$data[$key]['date'] = $datetag->datetime;
				}
				
				$tags = "";
				$tagsarr = array();
				foreach($v->find('.o-tag-skill') as $tag){
					$t = \strtolower(\trim( $tag->children(1)->plaintext));
					$tags.=  $t.",";
					$tagsarr[] = $t;
				}
				$data[$key]['tags'] = $tags;
				
				foreach($tagsarr as $tt){
					$this->job->addTag($tt);
				}
				
			}
			
			
			foreach($data as $d){
				
				$j = $this->job->getByProviderID($d['id']);
				
				
				if(!$j){
					$datearr = explode("T",$d['date']);
					$timearr = explode("+",$datearr[1]);
					$date = $datearr[0]." ".$timearr[0];
					
					$add = array(
						
						"job_provider" => "UPWK",
						"job_provider_id" => $d['id'],
						"job_title"	   => $d['title'],
						"job_url"	   => $d['url'],
						"job_type"	   => $d['type'],
						"job_duration" => $d['duration'],
						"job_budget"   => $d['budget'],
						"job_tags"	   => $d['tags'],
						"job_desc"	   => $d['desc'],
						"job_date"	   => $date
					);
					
					$this->job->add($add);
					
					
					
				}
			}
	
			
		}
		
		//app::trace($data);
			// echo("UPWORK - DONE!");
		
	}
    
    function upwork(){
	    $data = array();
	    
	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=web_mobile_software_dev&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	  // $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=it_networking&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	   /*  $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=data_science_analytics&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=engineering_architecture&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";
	    
	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=design_creative&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=writing&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=translation&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=legal&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=admin_support&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=customer_service&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=sales_marketing&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";

	    $rss[] = "https://www.upwork.com/ab/feed/jobs/rss?category2=accounting_consulting&api_params=1&q=&securityToken=17e564cb91f19d4f17b6f91dce81753fb1396641dd46be8e7e3768cb52c06894803609698e0e929a30243dc37f112afdbf95194e172c7ec78983e615ad7ebb6d&userUid=424120941751287808";*/

		foreach($rss as $rkey=>$r){
			$data = array();
			$xml = @simplexml_load_file( $r);
			foreach($xml->xpath('//item') as $key=>$item){
				
				$data[$key]['title'] = \trim(str_replace(" - Upwork","",$item->title));
				$url = \trim(str_replace ( "?source=rss" , "" , $item->link ));
				$data[$key]['url'] = $url;
				$d = str_replace ( " +0000" , "" , $item->pubDate );
				$data[$key]['date'] = \date("Y-m-d H:i:s",strtotime($d));
				$urlarr = explode("_%7",$url);
				$data[$key]['id'] = "_~".$urlarr[1];
				$desc =  \trim($item->description);
				$descarr = explode("<b>", $desc);
				$data[$key]['desc'] = \trim(\strip_tags( $descarr[0]));
				//$data[$key]['budget'] = $descarr;
				for($i=1;$i<count($descarr);$i++){
					$s = \trim(app::strings_clear($descarr[$i]));
					$isbudget = strpos( $s , "Budget:" );
					if($isbudget!==false){
						
						
						$data[$key]['budget'] = \trim(str_replace ( 'Budget:' , "" , $s ));
					}

					$istags = strpos( $s , "Skills:" );
					
					if($istags!==false){
						
						$ts = \trim(str_replace ( 'Skills:' , "" , $s ));
						$tarr = explode(",", $ts);
						foreach($tarr as $k=>$v){
							
							$narr[$k] = trim($v); 
							
						}
						
						$data[$key]['tags'] = implode(",", $narr);
						
						foreach($narr as $tt){
							$this->job->addTag($tt);
						}
						
					}
					
				}
				
				if(isset($data[$key]['budget'])){
					$data[$key]['type']="Fixed price"; 
					$data[$key]['duration'] = "";
				}else{
					 $data[$key]['type']="Hourly";
					 $data[$key]['budget'] = "";
					 $data[$key]['duration'] = "30 - 80 hr/week";
				}
				
				if(!isset($data[$key]['tags'])){
					if($rkey==0){
						$data[$key]['tags']="web development,HTML,CSS,web design";
						$tagsarr=array("web development","HTML","CSS","web design");

						foreach($tagsarr as $tt){
							$this->job->addTag($tt);
						}
					}
					
				}
				
				
			}
		
			
			foreach($data as $d){
				
				$j = $this->job->getByProviderID($d['id']);
				
				
				if(!$j){
					
					$add = array(
						
						"job_provider" => "UPWK",
						"job_provider_id" => $d['id'],
						"job_title"	   => $d['title'],
						"job_url"	   => $d['url'],
						"job_type"	   => $d['type'],
						"job_duration" => $d['duration'],
						"job_budget"   => $d['budget'],
						"job_tags"	   => $d['tags'],
						"job_desc"	   => $d['desc'],
						"job_date"	   => $d['date']
					);
					
					$this->job->add($add);
					
					
					
				}
			}
			
				//app::trace($data);
			
			
		}
	    
    }
     
    function freelancer(){
			   
		$html = \FastSimpleHTMLDom\Document::file_get_html('https://www.freelancer.com/jobs/1/');
		foreach($html->find('table.ProjectTable tr') as $tr) {
		   
		   foreach($tr->find('td') as $k=>$td){
			  
			   if($k==0){
				   $urls[] =$td->children(0)->href;
			    }
		   }
		}
		
		$data = array();
		
		foreach($urls as $key=>$link){
			
			$data[$key]['url'] = $link;
		
			$page = \FastSimpleHTMLDom\Document::file_get_html($link);

			$title = $page->find('.project-view-project-title',0);
			if(is_object($title)){
				$f = $page->find('meta[property="al:android:url"]',0);
				if(is_object($f)){
					$ae = explode("/",$f->content);	
					$data[$key]['id'] =  end($ae);		
				}
				
				$data[$key]['title'] = \trim($title->plaintext);
	
				$budget = $page->find('.project-statistic-value',0);
				
				if(is_object($budget)){
					$b = \trim($budget->plaintext);
					$h = explode("/",$b);
					if(isset($h[1])){
						
						$data[$key]['type'] = "Hourly";
						$data[$key]['budget'] = "";
						$data[$key]['duration'] = $b;

					}else{
						
						$data[$key]['type'] = "Fixed-Price";
						$data[$key]['budget'] = $b;
						$data[$key]['duration'] = "";
						
					}
					
				}
	
				$descr= $page->find('.project-description',0);
				
				if(is_object($descr)){
					
					$description = $descr->find('p');
					
					$descstr="";
					foreach($description as $kd=>$desc){
						
						if($kd>0)$descstr.= \trim($desc->plaintext)." ";
					}
					$data[$key]['desc'] = $descstr;
				}
				
				$tags = $page->find('.simple-tag');
				$tagsstr ="";
				$tagsarr = array();
				foreach($tags as $tag){
					if(is_object($tag)){
						$t = \strtolower(\trim($tag->plaintext));
						$tagsstr.=$tag->plaintext.",";
						$tagsarr[] = $t;
					}
					
				}
				$data[$key]['tags'] = $tagsstr;

				foreach($tagsarr as $tt){
					$this->job->addTag($tt);
				}

				
				$datetags = $page->find('time');
				
				foreach($datetags as $datetag){
					
					if(is_object($datetag)){
						
						$data[$key]['date'] = $datetag->datetime;
					}
				}
			}
			
			//	 echo("FREELANCER - DONE!");
			
		}
		
		foreach($data as $d){
			if(isset($d['id'])){
			$j = $this->job->getByProviderID($d['id']);
			
			if(!$j){

			
					if(isset($d['date'])){
						$datearr = explode("T",$d['date']);
						$timearr = explode("-",$datearr[1]);
						$date = $datearr[0]." ".$timearr[0];
						
					}else{
						
						$date = \date("Y-m-d H:i:s");
					}
				
					$add = array(
						
						"job_provider" => "FLCR",
						"job_provider_id" => $d['id'],
						"job_title"	   => $d['title'],
						"job_url"	   => $d['url'],
						"job_type"	   => $d['type'],
						"job_duration" => $d['duration'],
						"job_budget"   => $d['budget'],
						"job_tags"	   => $d['tags'],
						"job_desc"	   => $d['desc'],
						"job_date"	   => $date
					);
					
					$this->job->add($add);
			}
				
			
			}
			
		}
		
				
		
		
	    
    }

	function guru(){
		$data = array();
		$html = \FastSimpleHTMLDom\Document::file_get_html('http://www.guru.com/d/jobs/');
		
		foreach($html->find('.servTitle') as $key=>$div) {
		   
		   $data[$key]['url']='https://www.guru.com'.$div->children(0)->href;
		   $data[$key]['title']=trim($div->children(0)->plaintext);
		   $data[$key]['id']=$div->children(2)->children(1)->id;
		}

		foreach($html->find('.desc') as $key=>$div) {
		   
		
		   $data[$key]['desc']=trim($div->plaintext);
		   
		}

		foreach($html->find('.projAttributes') as $key=>$attr) {
		
		  $type =trim($attr->children(0)->plaintext);
		  $data[$key]['type'] = $type;  
		  
		  if($type=="Hourly"){
			  $data[$key]['duration'] = trim($attr->children(4)->plaintext).", ".trim($attr->children(2)->plaintext)." / hr";
			  $data[$key]['budget'] = "";
		  }else{
			   $data[$key]['duration']="";
			   $data[$key]['budget'] = trim($attr->children(2)->plaintext);
		  }
		  $data[$key]['date'] = $attr->find('.reltime_new',0)->getAttribute("data-date");
		  
		}

		foreach($html->find('.skills') as $key=>$ul) {
		    $tags = "";
			$lis = $ul->children();
			$tagsarr = array();
			foreach($lis as $li){
				
					$t = \strtolower(\trim( $li->children(0)->plaintext));
					$tags.=  $t.",";
					$tagsarr[] = $t;
				
			}
		  
		
		  $data[$key]['tags']=$tags;
				foreach($tagsarr as $tt){
					$this->job->addTag($tt);
				}
		   
		}
		
		foreach($data as $d){
			if(isset($d['id'])){
			$j = $this->job->getByProviderID($d['id']);
			
			if(!$j){

			
					$add = array(
						
						"job_provider" => "GURU",
						"job_provider_id" => $d['id'],
						"job_title"	   => $d['title'],
						"job_url"	   => $d['url'],
						"job_type"	   => $d['type'],
						"job_duration" => $d['duration'],
						"job_budget"   => $d['budget'],
						"job_tags"	   => $d['tags'],
						"job_desc"	   => $d['desc'],
						"job_date"	   => $d['date']
					);
					
					$this->job->add($add);
			}
				
			
			}
			
		}
			// echo("GURU - DONE!");
	}
	
	function pph(){
		$data = array();
		$html = \FastSimpleHTMLDom\Document::file_get_html('https://www.peopleperhour.com/freelance-jobs');
		
		foreach($html->find('.job-list-item') as $key=>$div) {
		   $a = $div->find('.job',0);
		   $p = $div->find('.price-tag',0);
		   $str = explode("-",$a->href);
		   $data[$key]['id'] = end($str);
		   $data[$key]['url']=$a->href;
		   $data[$key]['title']=$a->plaintext;
		   $type = \trim($p->children(1)->plaintext);
		   
		   if($type=="Per Hour"){
			  $data[$key]['type'] = "Hourly";
			  $data[$key]['budget'] = "";
			  $data[$key]['duration'] =\trim($p->children(3)->plaintext);

		   }else{
			  $data[$key]['type'] = "Fixed Price"; 
			  $data[$key]['budget'] = \trim($p->children(3)->plaintext);
			  $data[$key]['duration'] ="";
		   }
		   
		   $t = $div->find('time.crop',0)->plaintext;
		   
		   $data[$key]['date'] = \date("Y-m-d H:i:s",strtotime($t));
		}

		foreach($data as $k=>$v){
			   $tags="";
			   $html = \FastSimpleHTMLDom\Document::file_get_html($v['url']);
			   $data[$k]['desc'] = app::strings_clear($html->find('.project-description',0)->plaintext);

			   $tagsarr = array();
			   foreach($html->find('.tag-item') as $tag){
				   $t=\strtolower(\trim( $tag->plaintext));
					$tags.=  $t.",";
					$tagsarr[] = $t;
				   
			   }

				foreach($tagsarr as $tt){
					$this->job->addTag($tt);
				}

			   
			   $data[$k]['tags']=$tags;
		}
		foreach($data as $d){
			if(isset($d['id'])){
			$j = $this->job->getByProviderID($d['id']);
			
			if(!$j){

			
					$add = array(
						
						"job_provider" => "PEPH",
						"job_provider_id" => $d['id'],
						"job_title"	   => $d['title'],
						"job_url"	   => $d['url'],
						"job_type"	   => $d['type'],
						"job_duration" => $d['duration'],
						"job_budget"   => $d['budget'],
						"job_tags"	   => $d['tags'],
						"job_desc"	   => $d['desc'],
						"job_date"	   => $d['date']
					);
					
					$this->job->add($add);
			}
				
			
			}
			
		}
			// echo("PPH - DONE!");
		//app::trace($data);
	}
	
	function flexjobs(){
		$data = array();
		$html = \FastSimpleHTMLDom\Document::file_get_html('https://www.flexjobs.com/jobs/web-design?page=1');
		
		foreach($html->find('.list-group-item') as $key=>$div) {
		   $h5 = $div->find('h5',0);
		   if(is_object($h5)){
			   $a = $h5->children(0);
			   $str = explode("-",$a->href);
			   $data[$key]['id'] = end($str);
			   $data[$key]['url']='https://www.flexjobs.com'.$a->href;
			   $data[$key]['title']=$a->plaintext;
			   $data[$key]['desc'] = app::strings_clear(trim($div->find('.job-description',0)->plaintext)); 
			   $data[$key]['type'] = \trim($div->find('.text-danger',0)->plaintext);
			   $data[$key]['duration'] = "";
			   $data[$key]['budget'] = "";
		   }

		}
		
			foreach($data as $k=>$v){
				   $tags="";
				   $html = \FastSimpleHTMLDom\Document::file_get_html($v['url']);
				   $tr = $html->find('table tr');
				   
				   foreach($tr->find('td') as $kz=>$td){
			  
					   if($kz==0){
						  $data[$k]['date'] =\trim($td->plaintext);
					    }

					   if($kz==1){
						  $tagsstr=\strtolower(\trim($td->plaintext));
						  $tagsarr = explode(", ",$tagsstr);
						  $tags = implode(",", $tagsarr);
						  $data[$k]['tags'] = $tags;
						  foreach($tagsarr as $tt){
						  	$this->job->addTag($tt);
						  }

					    
					    }
					    
					    

				   }

				   
			
			}
		
		foreach($data as $d){
			if(isset($d['id'])){
			$j = $this->job->getByProviderID($d['id']);
			
			if(!$j){

					$day  =  \date("Y-m-d",strtotime($d['date']));
					$hr  =  \date("H:i:s",strtotime(rand ( 12 , 58 )." minutes ago"));

					$add = array(
						
						"job_provider" => "FLXJ",
						"job_provider_id" => $d['id'],
						"job_title"	   => $d['title'],
						"job_url"	   => $d['url'],
						"job_type"	   => $d['type'],
						"job_duration" => $d['duration'],
						"job_budget"   => $d['budget'],
						"job_tags"	   => $d['tags'],
						"job_desc"	   => $d['desc'],
						"job_date"	   => $day." ".$hr,
					);
					
					$this->job->add($add);
			}
				
			
			}
			
		}
		
		// echo("FLEXJOBS - DONE!");
		
		
	}
    
}
