<?php      
namespace floctopus\controllers\cronjob;

use \floctopus\application as app;     
use \jet\controller\http as HTTPController;   
use \floctopus\models\logic\jetsession as Session;
use \floctopus\models\orm\clients as OrmClients; 
use \floctopus\models\orm\accounts as OrmAcc; 
use \floctopus\models\orm\misc as OrmMisc; 
use \floctopus\models\orm\colloborators as OrmCollab;
use \floctopus\models\orm\rates as OrmRates;
use \floctopus\models\orm\accounts as OrmUsers;
use \floctopus\models\orm\cron as OrmCron; 
use \floctopus\models\logic\jetMail as jmail;

class main extends HTTPController {
	

    function __before() {
        
		$this->client = new OrmClients();
    	$this->collab = new OrmCollab();
		$this->user = new OrmUsers();
		$this->misc = new OrmMisc();
	
    }

    function __default($args = null) {  
		
		
    } 
	
	
	function sendemail(){
		
		$Cron = new OrmCron();
		
		$p=0;
		$iop = 500;
		$go = true;
		while($go){
			
			$emails = $Cron->getList(array(),$p,$iop);
			
			if($emails){
				$m = new jmail();
				$p++;
				foreach($emails as $email){
					if($email['cronemail_object']=="invoice"){
						$m->sendInvoice($email['cronemail_temail_to'],$email['cronemail_object_id'],$email['cronemail_user_id'],$msg="");
						
					}
					$upd['cronemail_status'] = 1;
					$Cron->updateEmail($email['cronemail_id'], $upd);
				}
				
				
			}else{
				
				$go=false;
			}
		}
		
		
		app::trace($p);
		
	}
	
	/*function createChannels(){
		
	    $users = $this->user->getAllList();
	    $clients = $this->client->getAllAllList();
		
		foreach($users as $user){
			$upd = array();
			$upd['user_pusher_channel'] = "private-".\uniqid();
			$this->user->update($user['user_id'],$upd);
		}		

		foreach($clients as $client){
			$upd = array();
			$upd['client_pusher_channel'] = "private-".\uniqid();
			$this->client->update($client['client_id'],$upd);
		}		
	}*/
	
/*	function createBillingAddr(){
		
		$acc = new OrmAcc();
		
		$users = $acc->getList($where=array(),0,1000) ;
		
		foreach($users as $u){
			
			$upd['set_billing_name'] = $u['user_name'];
			$upd['set_billing_street'] = $u['user_street'];
			$upd['set_billing_city'] = $u['user_city'];
			$upd['set_billing_state'] = $u['user_state'];
			$upd['set_billing_zip'] = $u['user_zip'];
			$upd['set_billing_country'] = $u['user_country'];
			$acc->updateSettings($u['user_id'],$upd);
		}
		
	}
	*/
	function getRates(){
		$rates = new OrmRates();
		$endpoint = 'live';
		$access_key = '863d5bcfcf72f18cc339dedfd9b490a5';
		
		// Initialize CURL:
		$c = $this->misc->getCurrencies();
		$str="";
		foreach($c as $v1){
			
			$str.=$v1['currency_iso'].",";
		}
		
		foreach($c as $v){
			
			$ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'&source='.$v['currency_iso'].'&currencies='.$str);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$json = curl_exec($ch);
			curl_close($ch);
			
			$exchangeRates = json_decode($json, true);
			$rates->delete($exchangeRates['source']);
			foreach($exchangeRates['quotes'] as $k=>$v ){
				//app::trace($exchangeRates['source']);
				
				$curr = substr($k, 3,3);
				$add['exr_base'] = $exchangeRates['source'];
				$add['exr_currency'] = $curr;
				$add['exr_rate'] = $v;
				$rates->add($add);
				
			}
			
			
		}
		
		
		// Access the exchange rate values, e.g. GBP:
		app::trace($exchangeRates);
		//app::trace($exchangeRates['quotes']['USDEUR']);
		
		//$upd['c_rub'] = $exchangeRates['quotes']['USDRUB'];
		//$upd['c_eur'] = $exchangeRates['quotes']['USDEUR'];
		//$this->cur->update($upd);
		
	}
	
}