<?php      
namespace floctopus\controllers\issues;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;   
use \floctopus\models\orm\notifications as OrmNotifications;  
use \floctopus\models\orm\colloborators as OrmCollab;
use \floctopus\models\orm\accounts as OrmAcc;
use \floctopus\models\orm\issues as OrmIssues;
use \floctopus\models\orm\projects as OrmProjects;
use \floctopus\models\orm\calendar as OrmCalendar;
use \floctopus\models\logic\jetMail as jmail; 
use \floctopus\models\libs\pagination as pagination;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_issues = app::$lang->issues;
		$this->view->lng_projects = app::$lang->projects;
        $this->view->setPath(app::$device.'/issues');   
    	$this->collab = new OrmCollab();
    	$this->prj = new OrmProjects();
    	$this->issue = new OrmIssues();
    	$this->cal = new OrmCalendar();
    	$this->acc = new OrmAcc();
		if(!isset($_SESSION['ISSUE_RET_URL']))$_SESSION['ISSUE_RET_URL']="/issues";
		if(!isset($_SESSION['ISSUE_PAGE']))$_SESSION['ISSUE_PAGE']=1;
		if(!isset($_SESSION['ISSUE_PRJ']))$_SESSION['ISSUE_PRJ']=0;
		if(!isset($_SESSION['ISSUE_MILES']))$_SESSION['ISSUE_MILES']=0;		
		if(!isset($_SESSION['ISSUE_COLLAB']))$_SESSION['ISSUE_COLLAB']=0;
		if(!isset($_SESSION['ISSUE_PRIOR']))$_SESSION['ISSUE_PRIOR']=0;
		if(!isset($_SESSION['ISSUE_CAT']))$_SESSION['ISSUE_CAT']=0;
		if(!isset($_SESSION['ISSUE_STATUS']))$_SESSION['ISSUE_STATUS']=10;
		if(!isset($_SESSION['ISSUE_SEARCH_STR']))$_SESSION['ISSUE_SEARCH_STR']="";
		if(!isset($_SESSION['ISSUE_STARTDATE']))$_SESSION['ISSUE_STARTDATE']="none";
		if(!isset($_SESSION['ISSUE_ENDDATE']))$_SESSION['ISSUE_ENDDATE']="none";
		$this->view->momentformat = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
		$this->view->sbProjects = "active";
		$this->view->subIssues = "active";
		
	}   

    function __default($args = false) {   
	    
	    if(isset($_GET['prj']))$_SESSION['ISSUE_PRJ']=$_GET['prj'];
	    if(isset($_GET['status']))$_SESSION['ISSUE_STATUS']=$_GET['status'];

		$this->view->startdate = $_SESSION['ISSUE_STARTDATE'];
		$this->view->enddate = $_SESSION['ISSUE_ENDDATE'];

	 	$this->view->searchstr = $_SESSION['ISSUE_SEARCH_STR'];	    
	    $this->view->defprj = $_SESSION['DEFPRJ'];
		$this->view->statuses = $this->issue->getAllStatuses();
	    $this->view->projects = $this->prj->getAllAvailProjects();
	    $this->view->cats = $this->issue->getAllCatList();
	    $this->view->priorities = $this->issue->getAllPriorList();
	    
	    if($_SESSION['ISSUE_PRJ']>0){
		     $this->view->collabs = $this->prj->getCollabsForIssues($_SESSION['ISSUE_PRJ'],$_SESSION['account']['user_id']);
		     $this->view->miles = $this->prj->getMilestones($_SESSION['ISSUE_PRJ']);
	    }else{
		    $selC['cb_status'] = 3;
		    $selC['cb_collobarator_id!'] = $_SESSION['account']['user_id'];
		    $this->view->collabs = $this->collab->getAllList($selC);
	    }
		$this->view->fixed_footer = "am-sticky-footer";
		$this->view->cstatus = $_SESSION['ISSUE_STATUS'];
		$this->view->cprj = $_SESSION['ISSUE_PRJ'];
		$this->view->cmiles = $_SESSION['ISSUE_MILES'];
		$this->view->ccollab = $_SESSION['ISSUE_COLLAB'];
		$this->view->cprior = $_SESSION['ISSUE_PRIOR'];
		$this->view->ccat = $_SESSION['ISSUE_CAT'];
        $this->view->setTemplate('index.tpl');  

        return $this->view;
    }	
    
    function getContent(){

    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['issue_trash']=0;

    	if(isset($_GET['query'])){
	    	$r = $this->makeTags($_GET['query']);
		 
		    $srch=$r['str'];
		    if(strlen($srch)){
			    $_SESSION['ISSUE_SEARCH_TAGS']=array($srch);
			    $_SESSION['ISSUE_SEARCH_STR']=$srch;
			    
		    }else{
		    	$_SESSION['ISSUE_SEARCH_TAGS']=NULL;
		    	$_SESSION['ISSUE_SEARCH_STR']=NULL;
			    
		    }
    	}

	   
	    if($_SESSION['ISSUE_PRJ']){
			$where['issue_project_id'] = $_SESSION['ISSUE_PRJ'];
	    }else{
		    $projects = $this->prj->getAllAvailProjectsIDS();
		    $where['issue_project_id']=$projects;
	    }

	    if($_SESSION['ISSUE_MILES']){
			$where['issue_milestone_id'] = $_SESSION['ISSUE_MILES'];
	    }


	    if($_SESSION['ISSUE_COLLAB']){
			

			$where['issue_assign'] = $_SESSION['ISSUE_COLLAB'];
			
			
	    }
		
	    if($_SESSION['ISSUE_PRIOR']){
			$where['issue_priority'] = $_SESSION['ISSUE_PRIOR'];
	    }

	    if($_SESSION['ISSUE_CAT']){
			$where['issue_cat'] = $_SESSION['ISSUE_CAT'];
	    }

	    if($_SESSION['ISSUE_STATUS']>0){
		    
			$where['issue_status'] = $_SESSION['ISSUE_STATUS'];
	    }

    	if($_SESSION['ISSUE_STARTDATE']!="none"){
	    	
	    	$where['DATE(issue_deadline)>']=$_SESSION['ISSUE_STARTDATE'];
	    	$where['DATE(issue_deadline)<']=$_SESSION['ISSUE_ENDDATE'];
    	}

	    
     	if(isset($_SESSION['ISSUE_SEARCH_STR']) ){
	    	
	    	$totalRec = $this->issue->getListCountSearch($where,$_SESSION['ISSUE_SEARCH_TAGS']);
	    	
    	}else{
	    	
	    	$totalRec = $this->issue->getListCount($where,$search);
    	}
	    
    	$page = $_SESSION['ISSUE_PAGE'];
	    $itemsOnPage = $_SESSION["account"]['user_rop'];

	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		
		$order ='issue_deadline DESC';
	    
	    $this->view->totalCount=$totalRec;

	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;

    	if(isset($_SESSION['ISSUE_SEARCH_STR'])){
	    	
	    	$this->view->records = $this->issue->getListSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['ISSUE_SEARCH_TAGS'],$order);
    	}else{
    		
    		$this->view->records = $this->issue->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
   		}
		
        $this->view->setTemplate('page.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;
      
        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
    }
    
    function add(){
	    
	    if(isset($_GET['prj']))$this->view->cprj = $_GET['prj']; else $this->view->cprj = 1;
	    $this->view->defprj = $_SESSION['DEFPRJ'];
	    $this->view->projects = $this->prj->getAllAvailProjects();
		$this->view->deadline_time = \date($_SESSION['account']['user_tformat'],strtotime("20:00:00"));
		$this->view->deadline_date = \date($_SESSION['account']['user_dformat'],strtotime("+1 day"));		
	    $this->view->cats = $this->issue->getAllCatList();
	    $this->view->priorities = $this->issue->getAllPriorList();
	    	
    //	$selC['cb_status'] = 3;
	   // $selC['cb_collobarator_id!'] = $_SESSION['account']['user_id'];
	  //  $this->view->collabs = $this->collab->getAllList($selC);

        $this->view->setTemplate('add.tpl');  
        return $this->view;
    }
    
    function doAdd(){

		$add['issue_subj'] = app::strings_clear($_POST['issue_subj']);  
		$add['issue_text'] = $_POST['issue_text'];   
		$add['issue_project_id'] = $_POST['issue_project_id'];  
		if(!isset($_POST['issue_milestone_id']))$_POST['issue_milestone_id']=0;
		$add['issue_milestone_id'] = $_POST['issue_milestone_id'];  
		$add['issue_cat'] = $_POST['issue_cat'];    
		$add['issue_priority'] = $_POST['issue_priority']; 
		$add['issue_assign'] = $_POST['issue_assign'];   
		$add['issue_user_id'] = $_SESSION['account']['user_id']; 

		if($_SESSION['account']['user_dformat']=="d/m/Y"){
			list($day,$month,$year) = explode('/',$_POST['deadline_date']);
			$deadline = $year."-".$month."-".$day;
		}else{
			$deadline = $_POST['deadline_date'];
		}
		
		$add['issue_deadline'] = \date("Y-m-d H:i:s",strtotime($deadline." ".$_POST['deadline_time']));
		$iid = $this->issue->add($add);
		
		$addEvent['e_user_id']	= $_SESSION['account']['user_id']; 
		$addEvent['e_type'] = 3;
		$addEvent['e_date'] = \date("Y-m-d H:i:s",strtotime($deadline." ".$_POST['deadline_time']));
		$addEvent['e_allday'] =0;
		$addEvent['e_title'] = $add['issue_subj'];
		$addEvent['e_object_id'] = $iid;
		$eid = $this->cal->add($addEvent);

		if($add['issue_assign']!=$_SESSION['account']['user_id']){
			
			$addEventAssign['e_user_id']	= $add['issue_assign']; 
			$addEventAssign['e_type'] = 3;
			$addEventAssign['e_date'] = \date("Y-m-d H:i:s",strtotime($deadline." ".$_POST['deadline_time']));
			$addEventAssign['e_allday'] =0;
			$addEventAssign['e_title'] = $add['issue_subj'];
			$addEventAssign['e_object_id'] = $iid;
			$eid = $this->cal->add($addEventAssign);
			
		}

		
		if(isset($_POST['attachments'])){
			
			$addf['issueatt_issue_id'] = $iid;
			$addf['issueatt_user_id'] = $_SESSION['account']['user_id'];
			
			foreach($_POST['attachments'] as $file){
				
				$addf['issueatt_filename'] = $file;
				$this->issue->addattach($addf);	
			}
		}

		$m = new jmail();

		if($add['issue_assign']!=$_SESSION['account']['user_id']){
			
			$u = $this->acc->getAccount($add['issue_assign']);
			$sent =$m->sendAddIssueNotify($u['user_email'],$add['issue_subj'],$add['issue_text'],$iid);
		}
		

		
		$res['status'] = true;
		return $res;
    }
    
    function doUpdate(){
	    
		$add['issue_subj'] = app::strings_clear($_POST['issue_subj']);  
		$add['issue_text'] = $_POST['issue_text'];   
		$this->issue->update($_POST['id'],$add);
		
		$i = $this->issue->getByID($_POST['id']);
		
		$m = new jmail();
			
		$u = $this->acc->getAccount($i['issue_assign']);
		$sent =$m->sendAddIssueUpdNotify($u['user_email'],$i['issue_subj'],$i['issue_text'],$i['issue_id']);
		
		$res['status'] = true;
		return $res;
    }
    
    function view(){

		if(!isset($this->args[0])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}

	    $id = $this->args[0];
	    $this->view->issue_id = $id; 
	    $this->view->statuses = $this->issue->getAllStatuses();
	    $i = $this->issue->getByID($id);

		if(!$i){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}
		
		$role = $this->prj->getRole($i['issue_project_id'],$_SESSION['account']['user_id']);
		
		if(!$role){
			
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}


	    if($i['project_name']=="Default")$i['project_name']=app::$lang->issues['select_project'];
	    $this->view->i = $i;		
		$this->view->milestones = $this->prj->getMilestones($i['issue_project_id']);
		$this->view->defprj = $_SESSION['DEFPRJ'];
	    $this->view->priorities = $this->issue->getAllPriorList();
	    $this->view->cats = $this->issue->getAllCatList();
	    $this->view->projects = $this->prj->getAllAvailProjects();
	    $this->view->role = $role;
        $this->view->setTemplate('view.tpl');  
        return $this->view;
    }
    
    function getView(){
	    $id = $_GET['id'];
	   
	    $selAtt['issueatt_issue_id'] = $id;
		$this->view->att = $this->issue->getListAttach($selAtt);
	   
	    $this->view->statuses = $this->issue->getAllStatuses();
	    $this->view->i = $this->issue->getByID($id);
		$this->view->history = $this->issue->getHistory($id);
		
        $this->view->setTemplate('view_content.tpl');  
 
        $res['html'] =  $this->view->render();
	    $res['status'] = true;
	    return $res;
	    
    }
    
    function doAddComment(){
	    
	    $id=$_POST['issue_id'];
	    $msg = trim($_POST['comment']);
	    if(!strlen($msg)){
		    $res['status'] = false;
		    $res['title'] = app::$lang->common['operation_failed'];
		    $res['msg'] = app::$lang->common['no_empty_text'];
		    return $res;
	    }
	    
	    $this->issue->addMessage($id,$msg);

		$i = $this->issue->getByID($id);
		
		$upd['issue_msg'] = $i['issue_msg']+1;
		$this->issue->update($id,$upd);

		$m = new jmail();
		
		if($i['issue_user_id']!=$_SESSION['account']['user_id']){
			
			$u = $this->acc->getAccount($i['issue_user_id']);
			$sent =$m->sendIssueCommentNotify($u['user_email'],$msg,$id);
		}

		if($i['issue_assign']!=$_SESSION['account']['user_id']){
			
			$u = $this->acc->getAccount($i['issue_assign']);
			$sent =$m->sendIssueCommentNotify($u['user_email'],$msg,$id);
		}
	    
		$res['status'] = true;
		return $res;
    }
    
    function changeStatus(){
	    $id=$_POST['id'];
	    $status = $_POST['status'];
	    
	    $add['ihist_status'] = $status;
	    $add['ihist_user_id'] = $_SESSION['account']['user_id'];
	    $add['ihist_issue_id'] = $id;
	    $this->issue->addHistory($add);
		
		$upd['issue_status'] = $status;
		$this->issue->update($id,$upd);

		$i = $this->issue->getByID($id);
		$this->sendNotify($i['issue_user_id'],$i['issue_assign'],$id,'status');
		
		$res['status'] = true;
		return $res;
    }

    function changeAssign(){
	    
	    $id=$_POST['id'];
	    $assign = $_POST['assign'];
	    
	    $add['ihist_assign'] = $assign;
	    $add['ihist_user_id'] = $_SESSION['account']['user_id'];
	    $add['ihist_issue_id'] = $id;
	    $this->issue->addHistory($add);
		
		$oldassigni = $this->issue->getByID($id);
		
		$selEvent['e_user_id'] = $oldassigni['issue_assign'];
		$selEvent['e_type'] = 3;
		$selEvent['e_object_id'] = $id;
		$event = $this->cal->getEventInfo($selEvent);
		
		$updEvent['e_user_id'] = $oldassigni['issue_assign'];
		$this->cal->update($event['e_id'],$updEvent);
		
		$upd['issue_assign'] = $assign;
		$this->issue->update($id,$upd);
		
		$u = $this->acc->getAccount($assign);
		
		$m = new jmail();		
		$sent =$m->sendIssueAssigntNotify($u['user_email'],$id);
		
		if($u['user_pic']=="nopic.png")$src = "/img/avatar/avatar1x50.jpg"; else $src = '/userfiles/'.$assign.'/pic/'.$u['user_pic'];
		$uname = '<span >'.$u['user_username'].'</span>';
		$img = '<img src="'.$src.'">'.$uname;
		
		$res['img'] = $img;
		$res['status'] = true;
		return $res;
		
	    
    }
    
    function changePriority(){
	    
	    $id=$_POST['id'];
	    $p = $_POST['p'];
	    
	    $add['ihist_priority'] = $p;
	    $add['ihist_user_id'] = $_SESSION['account']['user_id'];
	    $add['ihist_issue_id'] = $id;
	    $this->issue->addHistory($add);
		
		$upd['issue_priority'] = $p;
		$this->issue->update($id,$upd);
		
		$i = $this->issue->getByID($id);
		$this->sendNotify($i['issue_user_id'],$i['issue_assign'],$id,'priority');
		
		$html = '<span class="badge badge-'.$i['issueprior_color'].'" style="margin-bottom: 3px; font-size:larger;">'.$i['issueprior_name'].'</span>';
		
		$res['html'] = $html;
		$res['status'] = true;
		return $res;
    }

	function changeCategory(){
		
	    $id=$_POST['id'];
	    $c = $_POST['c'];
	    
	    $add['ihist_cat'] = $c;
	    $add['ihist_user_id'] = $_SESSION['account']['user_id'];
	    $add['ihist_issue_id'] = $id;
	    $this->issue->addHistory($add);
		
		$upd['issue_cat'] = $c;
		$this->issue->update($id,$upd);
		
		$i = $this->issue->getByID($id);
		$this->sendNotify($i['issue_user_id'],$i['issue_assign'],$id,'category');
		
		
		$html = '<span style="font-weight: 600; font-size:larger;" class="text-'.$i['issuecat_color'].'">'.$i['issuecat_name'].'</span>';
		
		$res['html'] = $html;
		$res['status'] = true;
		return $res;
	}
	
	function changePrj(){
	    
	    $id=$_POST['id'];
	    $p = $_POST['p'];
	    
	    $add['ihist_prj'] = $p;
	    $add['ihist_user_id'] = $_SESSION['account']['user_id'];
	    $add['ihist_issue_id'] = $id;
	    $this->issue->addHistory($add);
		
		$upd['issue_project_id'] = $p;
		$upd['issue_milestone_id'] = 0;
		$this->issue->update($id,$upd);
		
		$i = $this->issue->getByID($id);
		$this->sendNotify($i['issue_user_id'],$i['issue_assign'],$id,'project');
		
		
		$milestones = $this->prj->getMilestones($p);
		
		$mstr = '<option value="0">'. app::$lang->issues['entire_prj'].'</option>';
		foreach($milestones as $m){
			
			$mstr.='<option value="'.$m['prjm_id'].'">'.$m['prjm_name'].'</option>';
		}
		
		if($i['project_name']=="Default")$i['project_name']=app::$lang->issues['select_project'];
		
		$html = '<span style="font-weight: 600; font-size:larger;">'.$i['project_name'].'</span>';
		
		$res['miles_show'] = '<span style="font-weight: 600; font-size:larger;">'. app::$lang->issues['entire_prj'].'</span>';
		$res['miles'] = $mstr;
		$res['html'] = $html;
		$res['status'] = true;
		return $res;
	}
	
	function changeMiles(){
		
	    $id=$_POST['id'];
	    $m = $_POST['m'];
	    
	    $add['ihist_milestone'] = $m;
	    $add['ihist_user_id'] = $_SESSION['account']['user_id'];
	    $add['ihist_issue_id'] = $id;
	    $add['ihist_milestone_change'] = 1;
	    $this->issue->addHistory($add);
		
		$upd['issue_milestone_id'] = $m;
		$this->issue->update($id,$upd);
		
		$i = $this->issue->getByID($id);
		$this->sendNotify($i['issue_user_id'],$i['issue_assign'],$id,'milestone');

		if($i['issue_milestone_id']){
			$html = '<span style="font-weight: 600; font-size:larger;">'.$i['prjm_name'].'</span>';
		}else{
			
			$html = '<span style="font-weight: 600; font-size:larger;">'.app::$lang->issues['entire_prj'].'</span>';
		}
		
		
		$res['html'] = $html;
		$res['status'] = true;
		return $res;
	}
    
    function closeIssue(){
	    
	    $id=$_POST['id'];
	    $status = $_POST['status'];
	    
	    $add['ihist_status'] = $status;
	    $add['ihist_user_id'] = $_SESSION['account']['user_id'];
	    $add['ihist_issue_id'] = $id;
	    $this->issue->addHistory($add);
		
		$upd['issue_status'] = $status;
		$this->issue->update($id,$upd);

		$i = $this->issue->getByID($id);
		$this->sendNotify($i['issue_user_id'],$i['issue_assign'],$id,'closed');
		
		$res['status'] = true;
		return $res;
    }
    
    function getAssignSel(){
	    
	    $id = $_GET['prj'];
	    $iid = $_GET['iid'];
	    $colls =  $this->prj->getCollabs($id);
	    $isue = $this->issue->getByID($iid);
	   
	    if($isue['issue_user_id']==$isue['issue_assign'])$selcreator=" selected "; else $selcreator=" ";
	    
	  //  $html='<option '.$selcreator.' value="'.$isue['user_id'].'">'.$isue['user_username'].'</option> ';
	  $html ='<option value="0">'.app::$lang->projects['not_assigned'].'</option>';
	    foreach($colls as $v){
		    if($v['user_id']==$isue['issue_assign'])$selcreator=" selected "; else $selcreator=" ";
		    $html.='<option '.$selcreator.' value="'.$v['user_id'].'">'.$v['user_username'].'</option> ';
		    
	    }
	    
	    $res['html'] = $html;
	    $res['status'] = true;
	    return $res;
	    
    }
	
	function delete(){
		
		$id = $_POST['id'];
		
		$this->issue->delete($id);
		
		$res['status'] = true;
		return $res;
	}
	
	function getTextIssue(){
		
	    $id = $_GET['id'];

	    $i = $this->issue->getByID($id);
	    
	    $res['subj'] = $i['issue_subj'];
		$res['text'] = $i['issue_text'];
		$res['status'] = true;
		return $res;
		
	}

	function sendNotify($usr,$assignusr,$id,$obj){

		$m = new jmail();
		
		if($usr!=$_SESSION['account']['user_id']){
			
			$u = $this->acc->getAccount($usr);
			$sent =$m->sendIssueNotify($u['user_email'],$id,$obj);
		}

		if($assignusr!=$_SESSION['account']['user_id']){
			
			$u = $this->acc->getAccount($assignusr);
			$sent =$m->sendIssueNotify($u['user_email'],$id,$obj);
		}

		
		return false;
	}
	
	function changeOptionsPrj(){
		
		$_SESSION['ISSUE_PRJ'] = $_POST['issue_project'];
		$_SESSION['ISSUE_COLLAB'] = 0;
		$_SESSION['ISSUE_MILES'] = 0;
		
	    if($_SESSION['ISSUE_PRJ']>0 && $_SESSION['ISSUE_PRJ']!=$_SESSION['DEFPRJ']){
		     $this->view->collabs = $this->prj->getCollabsForIssues($_SESSION['ISSUE_PRJ'],$_SESSION['account']['user_id']);
		     $this->view->miles = $this->prj->getMilestones($_SESSION['ISSUE_PRJ']); 
	    }else{
		    $selC['cb_status'] = 3;
		    $selC['cb_collobarator_id!'] = $_SESSION['account']['user_id'];
		    $this->view->collabs = $this->collab->getAllList($selC);
	    }

		
				
        $this->view->setTemplate('selparticipants.tpl');
        $res['html'] = $this->view->render();

        $this->view->setTemplate('selmiles.tpl');
        
        if($_SESSION['DEFPRJ']==$_SESSION['ISSUE_PRJ']) $res['prj']=0; else $res['prj'] = $_SESSION['ISSUE_PRJ'];
        $res['htmlmiles'] = $this->view->render();
		$res['status'] = true;
		return $res;
		
	}

	function changeOptions(){
		
		$_SESSION['ISSUE_MILES'] = $_POST['issue_milestone_id'];
		$_SESSION['ISSUE_COLLAB'] = $_POST['issue_collab'];
		$_SESSION['ISSUE_PRIOR'] = $_POST['issue_priority'];
		$_SESSION['ISSUE_CAT'] = $_POST['issue_category'];
		$_SESSION['ISSUE_STATUS' ]= $_POST['issue_status'];
		
		$res['status'] = true;
		return $res;
		
	}

	
	function changePage(){
		
		$_SESSION['ISSUE_PAGE'] = $_POST['p'];
		
		$res['status'] = true;
		return $res;
	}
	
	function changeDateRange(){
		
		$_SESSION['ISSUE_STARTDATE'] = $_POST['start'];
		$_SESSION['ISSUE_ENDDATE'] = $_POST['end'];
		
		$res['s'] = $_SESSION['ISSUE_SEARCH_STR'];
		$res['status'] = true;
		return $res;
	}		

	function updateDeadline(){
		
		if($_SESSION['account']['user_dformat']=="d/m/Y"){
			list($day,$month,$year) = explode('/',$_POST['date']);
			$deadline = $year."-".$month."-".$day;
		}else{
			$deadline = $_POST['date'];
		}
		
		$id = $_POST['id'];
		
		$upd['issue_deadline'] = \date("Y-m-d H:i:s",strtotime($deadline." ".$_POST['time']));
		$this->issue->update($id,$upd);		

	    $add['ihist_deadline'] = \date("Y-m-d H:i:s",strtotime($deadline." ".$_POST['time']));
	    $add['ihist_user_id'] = $_SESSION['account']['user_id'];
	    $add['ihist_issue_id'] = $id;
	    $add['ihist_deadline_change'] = 1;
	    $this->issue->addHistory($add);

		$i = $this->issue->getByID($id);
		$this->sendNotify($i['issue_user_id'],$i['issue_assign'],$id,'deadline');

		
		$this->view->issue_deadline = \date("Y-m-d H:i:s",strtotime($deadline." ".$_POST['time']));
		$this->view->issue_status = $_POST['status'];
		
        $this->view->setTemplate('deadlineview.tpl');  
        $html = $this->view->render();
		
		$res['status'] = true;
		$res['html'] = $html;
		return $res;
		
	}




}