<?php      
namespace floctopus\controllers\profile;

use \floctopus\application as app;     
use \floctopus\models\orm\accounts as OrmAccounts;
use \floctopus\models\common\adminController as adminController;     
use \floctopus\models\orm\country as Countries; 
use \floctopus\models\orm\misc as OrmMisc; 

class main extends adminController {
    
    function __before() {
    	
    	parent::__before();
    	$this->country = new Countries();
    	$this->view->lng_profile = app::$lang->profile;
    	$this->view->lng_settings = app::$lang->settings;    	
    	$this->view->lng_first = app::$lang->first;
    	$this->view->lng_users = app::$lang->users;
        $this->view->setPath(app::$device.'/profile');   
        $this->view->page_title = app::$lang->profile['title'];
    	$this->misc = new OrmMisc();
		$this->acc = new OrmAccounts();
	}   
     
    function __default($args = false){   
	
		$this->view->today = time();
    	$country = new Countries();
    	$wheretz['tz_olson']=$_SESSION['account']['user_tz'];
    	$this->view->tz = $country->getTZ($wheretz);
    	
    	$this->view->proff = $this->misc->getProfessionName();
    	$this->view->country = $country->getCountryName();

		$this->view->fulllang = $this->misc->getLanguageISO($_SESSION['account']['user_lang']);

		$this->view->topProf = 'active';

        $this->view->setTemplate('index.tpl');  
        $this->view->d=time();
        return $this->view;
    } 
    
    function editpref(){
		$country=new Countries;
        $this->view->currencyList = $country->getCurrencyList();
        $this->view->tzList = $country->getTZList();
        $this->view->d=time();
        $this->view->dnow = \date('Y-m-d H:i:s',\strtotime("now"));
        $this->view->countryList=$country->getCountriesList();
    	$this->view->smSettings = 'active';
        $this->view->setTemplate('pref_edit.tpl');  
        return $this->view;
    }
 
    function editinfo(){
	
	    $ts = strtotime($_SESSION['account']['user_bday']);
	    $this->view->bday = \date("d",$ts);
	    $this->view->bmon = \date("m",$ts);
	    $this->view->byear = \date("Y",$ts);
	 	$country=new Countries;
	 	$this->view->countries = $country->getCountriesList();
	 	$this->view->monlist = $this->misc->getMonthsList();
	 	$this->view->proffesionsList = $country->getProfList();
        $this->view->setTemplate('acc_edit.tpl');  
       
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return $res;
        
    }
    
    function updateAcc(){
		
		$d = $_POST['bday'];
		$m = $_POST['bmon'];
		$y = $_POST['byear'];
		
		$bdate = \date("Y-m-d",strtotime($y."-".$m."-".$d));
	    $ccity = app::strings_clear($_POST['user_city']);
	    $caddr = app::strings_clear($_POST['user_street']);
	    $czip = app::strings_clear($_POST['user_zip']);
		$state = app::strings_clear($_POST['user_state']);		
		$phone = app::strings_clear($_POST['user_phone']);
		$name = app::strings_clear($_POST['user_name']);
		$www = app::strings_clear($_POST['user_www']);
		$skype = app::strings_clear($_POST['user_skype']);
		$twitter = app::strings_clear($_POST['user_twitter']);
		
		$update = array(
			'user_bday' => $bdate,
            'user_name' => $name,
            'user_country' => $_POST['user_country'],
            'user_state' => $state,
            'user_street' =>  $caddr,
            'user_city' => $ccity,
            'user_zip' => $czip,
			'user_phone' => $phone,
			'user_proffession' => $_POST['user_profession'],
			'user_about' => $_POST['user_about'],
			'user_www' => $www,
			'user_skype' => $skype,
			'user_twitter' => $twitter
        );
        

		$this->acc->update($_SESSION['account']['user_id'],$update);
		
		$res['status']=true;
		return $res;
    }
    
    function updateProfile(){
	    
		$mf=$_POST['mf'];
		
		switch ($mf){
			
			case 1:
				$co_dsep='.';
				$co_tsep=',';
			break;	
			
			case 2:
				$co_dsep=',';
				$co_tsep='.';
			break;

			case 3:
				$co_dsep='.';
				$co_tsep=' ';
			break;
		}
		
		
		$update = array(
            'user_currency' => $_POST['user_currency'],
            'user_dformat' => $_POST['user_dformat'],
            'user_tformat' => $_POST['user_tformat'],
            'user_tz' => $_POST['user_tz'],
            'user_dsep' => $co_dsep,
            'user_tsep' => $co_tsep,
            'user_wstart' => $_POST['user_wstart'],
            'user_lang' => $_POST['user_lang']
        );
		$this->acc->update($_SESSION['account']['user_id'],$update);
		
		\setcookie("fllang",$_POST['user_lang'],time()+60*60*24*30 ,"/");
		 $this->session->refresh();
		$res['status'] = true;
		return $res;
	    
    }
    
    function changeEmail(){
	    
	    $lemail =trim(strtolower($_POST['loginemail']));

				
		$selEmail['user_email'] = $lemail;
		$selEmail['user_id!'] = $_SESSION['account']['user_id'];
	
		$emp = $this->acc->getAccountInfo($selEmail);
				
		if($emp){
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->profile['email_taken'];
			return $res;
				
		}
				
		$updEmp['user_email'] = $lemail;

		$this->acc->update($_SESSION['account']['user_id'],$updEmp);		

	    $res['status']=true;
	    
	    $res['email'] = $lemail;
	    $res['html'] = '<button type="button" id="chEmail" class="btn btn-space btn-alt4 pull-right btn-sm">'.app::$lang->common['change'].'</button><input type="hidden" id="login-email" value="'.$lemail.'">';

	    $res['msg'] = app::$lang->profile['msg_email_upd'];
	    return $res;		
	    
	    
    }
    
   	function getBoxPass(){
	    
	    $this->view->setTemplate('set_password.tpl');
	    $html=$this->view->render();
	    $res['title'] = app::$lang->profile['ch_pass'];
		$res['html'] = $html;
		$res['title'] = app::$lang->profile['ch_pass'];
	    return $res;
		
	}	
	
	function changePassword(){
		$res = array();
		$res['status'] = false;		
		$lpass  = \trim($_POST['loginpassword']);
		
		
		
		if(!strlen($lpass)){
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->profile['ch_pass_err'];
			return $res;
		}
		
		if(!$this->acc->isPassword($_POST['cpass'])){
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->profile['wr_pass'];
			return $res;
			
		}
		
		$this->acc->changePassword($_SESSION['account']['user_id'],$lpass);
		$res['status'] = true;
	
		$res['msg'] = app::$lang->profile['pass_msg'];
		return $res;
	}

	function getUnameBox(){
		
	    $this->view->setTemplate('unamebox.tpl');
	    $html=$this->view->render();

		$res['html'] = $html;
		$res['title'] = app::$lang->profile['ch_un'];
	    return $res;
	}    

	function isusrnm(){
		
		$uname = \trim($_GET['user_username']);
		$isuname = $this->acc->isUnameExist($uname);	
		
		if($isuname){
			$res['status'] = false;
			$res['msg'] = app::$lang->first['user_exist'];
			return  $res;// app::$lang->first['user_exist'];
		}
		
		$res['status'] = true;
		return $res;;
		
	}
	
	function changeUname(){
		$uname = \trim($_POST['user_username']);
		$updEmp['user_username'] = $uname;

		$this->acc->update($_SESSION['account']['user_id'],$updEmp);		

	    $res['status']=true;
	    
	    $res['uname'] = $uname;
	    $res['html'] = '<button type="button" id="chUsername" class="btn btn-space btn-alt4 pull-right btn-sm">'.app::$lang->common['change'].'</button><input type="hidden" id="login-uname" value="'.$uname.'">';

	    $res['msg'] = app::$lang->profile['msg_unamel_upd'];
	    return $res;		
		
		
	}
	
	function updPub(){
		
		$name = $_POST['name'];
		$update[$name] = $_POST['val'];
		$this->acc->update($_SESSION['account']['user_id'],$update);
		
		$res['status'] = true;
		$res['msg'] =  app::$lang->profile['acc_upd'];
		return $res;
	}
	
	function getBillAddr(){

	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    $this->view->countryList=$this->country->getCountriesList();

        $this->view->setTemplate('editbilladdr.tpl');  
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
		
		
	}	

	function doUpdateBillAddr(){
		
	    
	    //$acc = new  OrmAccounts();
	    
	    $upd['set_billing_name']= $_POST['set_billing_name'];
	    $upd['set_billing_street']= $_POST['set_billing_street'];
	    $upd['set_billing_city'] = $_POST['set_billing_city'];
	    $upd['set_billing_state'] = $_POST['set_billing_state'];
	    $upd['set_billing_zip'] = $_POST['set_billing_zip'];
	    $upd['set_billing_country'] = $_POST['set_billing_country'];
	    $this->acc->updateSettings($_SESSION['account']['user_id'],$upd);
	    
	    $this->session->refresh();
	    
	    $this->view->acs = $_SESSION['account'];
	    $this->view->country = $this->country->getCountryName($upd['set_billing_country']);
        $this->view->setTemplate('viewbillingaddr.tpl');  
	    $html = $this->view->render();
	    
	    $res['status'] = true;
	    $res['html'] = $html;
	    return $res;
		
		
	}
	
	function getAccView(){

		$this->view->today = time();
    	$country = new Countries();
    	$wheretz['tz_olson']=$_SESSION['account']['user_tz'];
    	$this->view->tz = $country->getTZ($wheretz);
		$this->view->amount = 19876543.21;
    	$this->view->proff = $this->misc->getProfessionName();
    	$this->view->country = $country->getCountryName();
		
		$this->view->setTemplate('acc_view.tpl'); 
		$res['html'] = $this->view->render();
		$res['status'] = true;
		return  $res;
	}
	
	function getEditPref(){
		
		$country=new Countries;
        $this->view->currencyList = $country->getCurrencyList();
        $this->view->tzList = $country->getTZList();
        $this->view->d=time();
        $this->view->dnow = \date('Y-m-d H:i:s',\strtotime("now"));
        $this->view->countryList=$country->getCountriesList();
		
		$this->view->setTemplate('pref_edit.tpl'); 
		$res['html'] = $this->view->render();
		$res['status'] = true;
		return  $res;
		
	}
	
	function getPref(){

		$this->view->fulllang = $this->misc->getLanguageISO($_SESSION['account']['user_lang']);
		$this->view->today = time();
    	$country = new Countries();
    	$wheretz['tz_olson']=$_SESSION['account']['user_tz'];
    	$this->view->tz = $country->getTZ($wheretz);
    			
		$this->view->setTemplate('acc_pref_view.tpl'); 
		$res['html'] = $this->view->render();
		$res['status'] = true;
		return  $res;
	}
	
	function getEduList(){
		
		$this->view->records = $this->acc->getEduList($_SESSION['account']['user_id']);
		
		
		
		$this->view->setTemplate('acc_edu_view.tpl'); 
		$res['html'] = $this->view->render();
		$res['status'] = true;
		return  $res;
	}


	function getEduEdit(){
		$this->view->months = $this->misc->getMonthsList(); 
		$this->view->records = $this->acc->getEduList($_SESSION['account']['user_id']);
		$this->view->setTemplate('acc_edu_edit.tpl'); 
		$res['html'] = $this->view->render();
		$res['status'] = true;
		return  $res;
	}
	
	function getNewEduRecord(){
		
		$this->view->uniqid = \uniqid();
		$this->view->months = $this->misc->getMonthsList(); 
		$this->view->setTemplate('acc_edu_new_record.tpl'); 
		$res['html'] = $this->view->render();
		$res['status'] = true;
		return  $res;
	}


	function updEdu(){
		
		
		
		if(isset($_POST['edu_school'])){
			$this->acc->delEdu();

	
			foreach($_POST['edu_school'] as $k=>$v){
				$add=array();
				$add['edu_school'] = $v;
				$add['edu_user_id'] = $_SESSION['account']['user_id'];
				$add['edu_year_from'] = $_POST['edu_year_from'][$k];
				$add['edu_month_from'] = $_POST['edu_month_from'][$k];
				$add['edu_year_to'] = $_POST['edu_year_to'][$k];
				$add['edu_month_to'] = $_POST['edu_month_to'][$k];
				$add['edu_degree'] = $_POST['edu_degree'][$k];
				$add['edu_area'] = $_POST['edu_area'][$k];
				$add['edu_desc'] = $_POST['edu_desc'][$k];
				$this->acc->addEdu($add);
					
			}
			
		}
		
		$res['status'] = true;
		return  $res;
		
	}

    function deleteEdu(){
        $id = $_POST['id'];
        $this->acc->delEdu($id);
        $res['status'] = true;
        return $res;
    }

    function getWorkList(){

        $this->view->records = $this->acc->getWorkList($_SESSION['account']['user_id']);
        $this->view->setTemplate('acc_work_view.tpl');
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return  $res;
    }


    function getWorkEdit(){
        $this->view->months = $this->misc->getMonthsList();
        $this->view->records = $this->acc->getWorkList($_SESSION['account']['user_id']);
    
        $this->view->setTemplate('acc_work_edit.tpl');
        $res['btnyes'] = app::$lang->common['btn_yes'];
        $res['btnno'] = app::$lang->common['btn_no'];
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return  $res;
    }

    function getNewWorkRecord(){

        $this->view->uniqid = \uniqid();
        $this->view->months = $this->misc->getMonthsList();
        $this->view->setTemplate('acc_work_new_record.tpl');
        $res['btnyes'] = app::$lang->common['btn_yes'];
        $res['btnno'] = app::$lang->common['btn_no'];
        $res['html'] = $this->view->render();
        $res['status'] = true;
        return  $res;
    }


    function updWork(){
	
        if(isset($_POST['work_position'])) {

            $this->acc->delWork();

            if ($_POST['work_year_from'] > $_POST['work_year_to']) {
                $res['title'] = "Operation Failed";
                $res['status'] = false;
                $res['msg'] = app::$lang->profile['data_false'];;
                return $res;
            } else {
                foreach ($_POST['work_position'] as $k => $v) {
                    $add = array();
                    $add['work_position'] = $v;
                    $add['work_user_id'] = $_SESSION['account']['user_id'];
                    $add['work_year_from'] = $_POST['work_year_from'][$k];
                    $add['work_month_from'] = $_POST['work_month_from'][$k];
                    $add['work_company'] = $_POST['work_company'][$k];
                    $add['work_location'] = $_POST['work_location'][$k];
                    $add['work_desc'] = $_POST['work_desc'][$k];


                    if ($_POST['work_current'][$k]) {
                        $add['work_current'] = 1;
                    } else {
                        $add['work_year_to'] = $_POST['work_year_to'][$k];
                        $add['work_month_to'] = $_POST['work_month_to'][$k];
                        $add['work_current'] = 0;
                    }
//				$t = app::tracevar($add);
//				app::Log($t);
                    $this->acc->addWork($add);

                }
                $res['status'] = true;
                return $res;


            }
        }
    }

    function deleteWork(){
        $id = $_POST['id'];
        $this->acc->delWork($id);
        $res['status'] = true;
        return $res;
    }
    
}
