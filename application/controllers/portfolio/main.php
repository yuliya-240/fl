<?php
namespace floctopus\controllers\portfolio;

use \floctopus\application as app;
use \floctopus\models\common\adminController as adminController;
use \floctopus\models\orm\portfolio as OrmPortfolio;
//use \floctopus\models\orm\portfolio_pic as OrmPortfolio_pic;
use \floctopus\models\orm\accounts as OrmAccounts;
use \floctopus\models\libs\pagination as pagination;

class main extends adminController
{

    function __before() {

        parent::__before();
        $this->view->lng_portfolio = app::$lang->portfolio;
        $this->view->setPath(app::$device . '/portfolio');
        if (!isset($_SESSION['PORTFOLIO_RET_URL'])) $_SESSION['PORTFOLIO_RET_URL'] = "/portfolio";
        $this->view->sbPortfolio = "active";
        $this->portfolio = new OrmPortfolio();
        $this->acc = new OrmAccounts();
		$this->view->sbRes = "active";
		$this->view->subPortfolio = "active";
        
    }

    function __default($args = false){
       if (isset($_SESSION['PORTFOLIO_SEARCH_STR'])) $this->view->searchstr = $_SESSION['PORTFOLIO_SEARCH_STR'];

        $this->view->fixed_footer = "am-sticky-footer";
        $this->view->setTemplate('index.tpl');

        return $this->view;
    }

    function getContent(){


        $pages = new pagination();
        $where = array();
        $search = array();
        $where['portfolio_trash'] = 0;
        $where['portfolio_user_id'] = $_SESSION['account']['user_id'];

        if (isset($_GET['query'])) {
            $r = $this->makeTags($_GET['query']);

            $srch = $r['str'];
            if (strlen($srch)) {
                $_SESSION['PORTFOLIO_SEARCH_TITLE'] = array($srch);
                $_SESSION['PORTFOLIO_SEARCH_STR'] = $srch;

            } else {
                $_SESSION['PORTFOLIO_SEARCH_TITLE'] = NULL;
                $_SESSION['PORTFOLIO_SEARCH_STR'] = NULL;

            }
        }

        if (isset($_SESSION['PORTFOLIO_SEARCH_STR'])) {

            $totalRec = $this->portfolio->getListCountSearch($where, $_SESSION['PORTFOLIO_SEARCH_TITLE']);
        } else {

            $totalRec = $this->portfolio->getListCount($where, $search);
        }

        $totalRec = $this->portfolio->getListCount($where, $search);

        $page = isset($_GET['p']) && $_GET['p'] ? \abs((int)$_GET['p']) : 1;
        $itemsOnPage = $_SESSION["iop"];

        $pagination = $pages->calculate_pages($totalRec, $itemsOnPage, $page);


        $order = 'portfolio_id ASC';

        $this->view->records = $this->portfolio->getList($where, $pagination['current'], $itemsOnPage, $search, $order);

        if (isset($_SESSION['PORTFOLIO_SEARCH_STR'])) {

            $this->view->records = $this->portfolio->getListSearch($where, $pagination['current'], $itemsOnPage, $_SESSION['PORTFOLIO_SEARCH_TITLE'], $order);
        } else {
            $this->view->records = $this->portfolio->getList($where, $pagination['current'], $itemsOnPage, $search, $order);
        }

        $this->view->setTemplate('page.tpl');
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');
        $footer = $this->view->render();

        $res['returl'] = $_SESSION['PORTFOLIO_RET_URL'];
        $res['footer'] = $footer;
        $res['html'] = $html;
        $res['count'] = $totalRec;
        $res['pag'] = $pagination;
        return $res;


    }

    function add()
    {

        $this->view->setTemplate('add.tpl');
        return $this->view;
    }

    function edit()
    {

        $id = $this->args[0];
        $this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
        $this->view->portfolio = $this->portfolio->getByID($id);
        $this->view->portfolio_pic = $this->portfolio->getPortfoliopic($id);
        $this->view->setTemplate('edit.tpl');
        return $this->view;
    }

    function doAdd()
    {

        $title = app::strings_clear($_POST['portfolio_title']);
        $desc = $_POST['portfolio_desc'];

        $link = strtolower($_POST['portfolio_link']);

        $http = substr($link, 0,7);
        $https = substr($link, 0,8);

        if($https=="https://" || $http=="http://"){

            $l = $link;
        }else{

            $l = "http://".$link;

        }
        $add = array(
            "portfolio_user_id" => $_SESSION['account']['user_id'],
            "portfolio_title" => $title,
            "portfolio_customer" => $_POST['portfolio_customer'],
            "portfolio_link" => $l,
            "portfolio_desc" => $desc,
            "portfolio_thumbnail" => $_POST['fcover'],
        );
        $id=$this->portfolio->add($add);
        if(isset($_POST['fpic'])){
            foreach($_POST['fpic'] as $k=>$v){
	            
                $addp["portfoliopic_pic"] = $v;
                $this->portfolio->addPortfoliopic($addp,$id);
                $addp=array();

            }

        }

        $res['status'] = true;
        return $res;
    }

    function doUpdate(){
        $title = app::strings_clear($_POST['portfolio_title']);
        $desc = $_POST['portfolio_desc'];
        $id=$_POST['portfolio_id'];
        $link = strtolower($_POST['portfolio_link']);

        $http = substr($link, 0,7);
        $https = substr($link, 0,8);

        if($https=="https://" || $http=="http://"){

            $l = $link;
        }else{

            $l = "http://".$link;

        }
        $upd = array(
            "portfolio_user_id" => $_SESSION['account']['user_id'],
            "portfolio_title" => $title,
            "portfolio_customer" => $_POST['portfolio_customer'],
            "portfolio_link" => $l,
            "portfolio_desc" => $desc,
            "portfolio_thumbnail" => $_POST['fcover'],
        );
        
        $this->portfolio->update($id, $upd);
       
        $this->portfolio->delPortfoliopic($id);
        
        if(isset($_POST['fpic'])){
            foreach($_POST['fpic'] as $k=>$v){
				
			
                $addp["portfoliopic_pic"] = $v;
                $this->portfolio->addPortfoliopic($addp,$id);
                $addp=array();

            }

        }

        $res['status'] = true;

        return $res;
    }

    function del($id=0){
        if(!$id)$id=$_POST['id'];
        $upd['portfolio_trash'] = 1;
        $this->portfolio->updateMass($id,$upd);
        $res['status'] = true;
        return $res;
    }

    function deleteBundle(){


        if(isset($_POST['row']) && count($_POST['row'])){
           // app::trace($_POST);
            foreach($_POST['row'] as $val){
                $this->del($val);
            }

            $res['status']=true;
            return $res;
        }else{
            $res['title']="Operation Failed";
            $res['status']=false;
            $res['msg']="No rows selected. Please select at least one template.";
            return $res;
        }
    }

}