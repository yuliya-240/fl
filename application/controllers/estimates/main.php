<?php      
namespace floctopus\controllers\estimates;

use \floctopus\application as app;     
use \floctopus\models\common\adminController as adminController;  
use \floctopus\models\orm\accounts as OrmAccounts; 
use \floctopus\models\orm\invoices as OrmInvoices;
use \floctopus\models\orm\estimates as OrmEstimates;
use \floctopus\models\orm\services as OrmSrv;
use \floctopus\models\orm\clients as OrmClients;
use \floctopus\models\orm\colloborators as OrmCollabs;
use \floctopus\models\orm\misc as OrmMisc;
use \floctopus\models\orm\transactions as OrmTrans;
use \floctopus\models\orm\country as OrmCountry; 
use \floctopus\models\orm\antispam as OrmAntispam; 
use \floctopus\models\orm\cron as OrmCron; 
use \floctopus\models\logic\validator as OrmValidator;
use \floctopus\models\libs\pagination as pagination;
use \floctopus\models\orm\projects as OrmProjects;
use \jet\language as lang;

class main extends adminController {
	
    function __before() {
    	
    	parent::__before();
    	$this->view->lng_estimates = app::$lang->estimates;
 		//app::trace(app::$lang->estimates );   	
    	
    	
    	$this->view->lng_clients = app::$lang->clients;
        $this->view->setPath(app::$device.'/estimates');   
    	$this->inv = new OrmInvoices();
    	$this->est = new OrmEstimates();
		$this->client = new OrmClients();
	    $this->collabs = new OrmCollabs();
		$this->misc = new OrmMisc();
		$this->trans = new OrmTrans();
		$this->country = new OrmCountry();	
		$this->prj = new OrmProjects();	
		if(!isset($_SESSION['EST_RET_URL']))$_SESSION['EST_RET_URL']="/estimates";
		if(!isset($_SESSION['EST_PAGE']))$_SESSION['EST_PAGE']=1;
		$this->view->sbMoney = "active";
		$this->view->subEstimates = "active";
		$this->view->momentformat = app::dateFormatToMoment($_SESSION['account']['user_dformat']);
		if(!isset($_SESSION['EST_STARTDATE']))$_SESSION['EST_STARTDATE']="none";
		if(!isset($_SESSION['EST_ENDDATE']))$_SESSION['EST_ENDDATE']="none";
		if(!isset($_SESSION['EST_SEARCH_STR']))$_SESSION['EST_SEARCH_STR']="";
		if(!isset($_SESSION['EST_LANG']))$_SESSION['EST_LANG'] = $_SESSION['account']['user_lang'];
	    if(!isset($_SESSION['EST_TYPE'])){
		    $_SESSION['EST_TYPE'] = array(0);
    	
    	}
    	
    	if(!isset($_SESSION['EST_ARC']))$_SESSION['EST_ARC']=0;
	}   

    function __default($args = false) {   
 
	 	$this->view->eststatuslist = $this->est->getAllStatuses();
	 	$this->view->searchstr = $_SESSION['EST_SEARCH_STR'];
		$this->view->fixed_footer = "am-sticky-footer";
		$this->view->startdate = $_SESSION['EST_STARTDATE'];
		$this->view->enddate = $_SESSION['EST_ENDDATE'];
        if(!isset($_SESSION['EST_PAGE']))$_SESSION['EST_PAGE']=1;
    	$this->view->est_arc = $_SESSION['EST_ARC'];
    	$this->view->est_type = $_SESSION['EST_TYPE'];
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
    function getContent(){
	    
    	$pages = new pagination();
    	$where=array();
    	$search=array();
	    $where['est_trash']=0;
	    $where['est_user_id']=$_SESSION['account']['user_id'];

    	if(isset($_GET['query'])){
	    	$r = $this->makeTags($_GET['query']);
		 
		    $srch=$r['str'];
		    if(strlen($srch)){
			    $_SESSION['EST_SEARCH_TAGS']=array($srch);
			    $_SESSION['EST_SEARCH_STR']=$srch;
			    
		    }else{
		    	$_SESSION['EST_SEARCH_TAGS']=NULL;
		    	$_SESSION['EST_SEARCH_STR']=NULL;
			    
		    }
    	}

		if($_SESSION['EST_ARC']>0){
			if($_SESSION['EST_ARC']==1)$where['est_arc']=1; else $where['est_arc']=0; 
		}
    	
    	foreach($_SESSION['EST_TYPE'] as $i){
	    	
	    	if($i!=0)$where['est_status'][]=$i;
    	}
    	
    	if($_SESSION['EST_STARTDATE']!="none"){
	    	
	    	$where['est_date>']=$_SESSION['EST_STARTDATE'];
	    	$where['est_date<']=$_SESSION['EST_ENDDATE'];
    	}

    	
     	if(isset($_SESSION['EST_SEARCH_STR']) ){
	    	
	    	$totalRec = $this->est->getListCountSearch($where,$_SESSION['EST_SEARCH_TAGS']);
    	}else{
	    	
	    	$totalRec = $this->est->getListCount($where,$search);
    	}

    	$page = $_SESSION['EST_PAGE'];
	    $itemsOnPage = $_SESSION["account"]['user_rop'];
	    
	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		$order ='est_date ASC';

	    $this->view->totalCount=$totalRec;
		
	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
		
    	if(isset($_SESSION['EST_SEARCH_STR'])){
	    	
	    	$this->view->records = $this->est->getListSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['EST_SEARCH_TAGS'],$order);
	    	$this->view->totalPage = $this->est->getTotalPageSearch($where,$pagination['current'],$itemsOnPage,$_SESSION['EST_SEARCH_TAGS'],$order);
    	}else{
    		$this->view->records = $this->est->getList($where,$pagination['current'],$itemsOnPage,$search,$order);
    		$this->view->totalPage = $this->est->getTotalPage($where,$pagination['current'],$itemsOnPage,$search);
   		}
   		
   		//app::trace($this->view->totalPage);
	    
        $this->view->setTemplate('page.tpl');  
        $html = $this->view->render();

        $this->view->setTemplate('footer.tpl');  
        $footer = $this->view->render();

        $res['footer'] = $footer;
        $res['count'] = $totalRec;
        $res['html'] = $html;
        return $res;
	    
    }

    function add($args = false) {   
		$_SESSION['EST_LANG']  = $_SESSION['account']['user_lang'];
		$invlang = new lang(app::$settings->lang_path._.'inv.'.$_SESSION['EST_LANG'].'.lng');
		$rates = array();		
		if(isset($_GET['c']))$c=$_GET['c']; else $c=0;
		if(isset($_GET['u']))$c=$_GET['u']; else $u=0;
		
		$selPrj['project_user_id'] = $_SESSION['account']['user_id'];
		$selPrj['project_trash'] =0;
		$selPrj['project_default'] =0;
		$this->view->prj = $this->prj->getAllList($selPrj);

		$this->view->terms = $_SESSION['account']['set_est_terms'];
		$this->view->notes = $_SESSION['account']['set_est_notes'];
		
		$this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
		
		$allcurrencies = $this->misc->getCurrencies();

		foreach($allcurrencies as $k=>$v){
			$ra = $this->misc->getRateByCurrency($v['currency_iso'],$_SESSION['account']['user_currency']);
			$rates[$k]['curr'] = $v['currency_iso'];
			$rates[$k]['val'] = $ra['exr_rate'];
		}
		
		$this->view->cinvlang = $_SESSION['EST_LANG'];
		$this->view->invlang = $invlang->details;
		$this->view->rates = $rates;//$this->misc->getAllRates($_SESSION['account']['user_currency']);
	    $this->view->countryList=$this->country->getCountriesList();
	    $this->view->statesList=$this->country->getStateList($_SESSION['account']['user_country']);
		$this->view->langs = $this->misc->getAllLangs();
		$this->view->lng_clients = app::$lang->clients;
		$this->view->currencies = $allcurrencies;
		$this->view->months = $this->misc->getMonthsList();
		$this->view->num = $this->est->getLastNum();
		$selClients['client_collab'] = 0;
		$this->view->clients = $this->client->getAllList($selClients);
		$this->view->collabs = $this->collabs->getAllList(array(),"users.user_name ASC");
		
		$this->view->selCollab = $u;
		$this->view->selClient = $c;

        $this->view->setTemplate('add.tpl');  
        return $this->view;
    } 
    
    function view(){
	    
	    if(!isset($this->args[0])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
	    }
	    
	    $id = $this->args[0];
	    
	    $estimate = $this->est->getByID($id);
		
		if(!$estimate){
			
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}
		
		if($estimate['est_client_id']>0){
			
			$recipient="cli";
			$recepientInfo = $this->client->getByID($estimate['est_client_id']);
			$this->view->sendemail = $recepientInfo['client_email'];
			$this->view->sendemailcc = $recepientInfo['client_ccemail'];
			$this->view->postfix = $recepientInfo['client_id']."@cli";
		
		}else{
			$recepientInfo = $this->collabs->getByID($estimate['est_collab_id']);
			$this->view->sendemail = $recepientInfo['user_email'];
			$this->view->sendemailcc = "";
			$this->view->postfix = $recepientInfo['cb_id']."@usr";
			$recipient ="collab";
		}
		
		// Total amount of all outstanding estimates
		$ar = array();
		$ar['inv_user_id']=$_SESSION['account']['user_id'];
		$ar['inv_status'][]=3;
		$ar['inv_status'][]=4;
		$ar['inv_status'][]=5;
	
		if($recipient=="cli"){ 
			$ar['inv_client_id']=$estimate['est_client_id'];
		} else {
	   
			$ar['inv_collab_id']=$estimate['est_collab_id'];
	    }
		$outstanding = $this->inv->getInvAmount($ar);		

		// Total amount of all paid estimates
		$ar = array();
		$ar['inv_user_id']=$_SESSION['account']['user_id'];
		$ar['inv_status'][]=3;
		$ar['inv_status'][]=2;

		if($recipient=="cli"){ 
			$ar['inv_client_id']=$estimate['est_client_id'];
		} else {
	   
			$ar['inv_collab_id']=$estimate['est_collab_id'];
	    }
		$paid= $this->inv->getInvPaidAmount($ar);		
		
		
		// PAST DUE
    	$selPD['inv_status']=4;
		if($recipient=="cli"){ 
			$ar['inv_client_id']=$estimate['est_client_id'];
		} else {
	   
			$ar['inv_collab_id']=$estimate['est_collab_id'];
	    }
    	$selPD['inv_user_id']=$_SESSION['account']['user_id'];
    	$pastdue = $this->inv->getInvAmount($selPD);
		
		$invlang = new lang(app::$settings->lang_path._.'inv.'.$estimate['est_lang'].'.lng');
		$this->view->invlang = $invlang->details;
		
		$this->view->paymethod = $this->trans->getPaymentMethodsList();
	    $this->view->flist = $this->est->getAttachList($id);

		$this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
		$selLines['estline_user_id'] = $_SESSION['account']['user_id'];
		$selLines['estline_est_id'] = $id;
		$this->view->lines = $this->est->getLinesList($selLines);

		$selHistory['esthist_user_id'] = $_SESSION['account']['user_id'];
		$selHistory['esthist_est_id'] = $id;
		$this->view->history = $this->est->getHistoryList($selHistory);
		
		$this->view->country = $this->country->getCountryName($estimate['est_country']);
		$this->view->mycountry = $this->country->getCountryName($_SESSION['account']['set_billing_country']);
		$this->view->paid = $paid;
		$this->view->pastdue = $pastdue;
		$this->view->outstanding = $outstanding;
		$this->view->est = $estimate;
        $this->view->setTemplate('view.tpl');  
        return $this->view;
    }
    
    function getNewLine(){
	    $this->view->id = uniqid();
        $this->view->setTemplate('newline.tpl');  
        $res['html'] = $this->view->render();
	    $res['status'] = true;
	    $res['id'] = $this->view->id;
	    return $res;
    }
	
	function doSafeDraft(){
		if(!isset($_POST['recurrance']))$_POST['recurrance']=0;
		
		$post = $_POST;
		
		$id = $this->createEstimate($post);
		
		$res['status'] = true;
		return $res;
	}
	
	function doEmailDraft(){
		if(!isset($_POST['recurrance']))$_POST['recurrance']=0;

		$valid = new OrmValidator();
		
		$email = strtolower($_POST['send_email']);		
		$emailcc = strtolower($_POST['send_email_cc']);		
		
		if(!$valid->email($email) || (strlen($emailcc) && !$valid->email($emailcc))){
			
			$res['status'] = false;
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->common['nve'];
			return $res;
			
		}

		$post = $_POST;
		
		$estid = $this->createEstimate($post);
		
		$as = new OrmAntispam();
		
			$Cron = new OrmCron();
			$add=array();
			
			$add['as_user_id'] = $_SESSION['account']['user_id'];
			$add['as_object'] = 'estimate';
			$add['as_object_id'] = $estid;
			$add['as_email'] = $email;
			$as->add($add);
			
			$addCron=array(
				"cronemail_user_id" => $_SESSION['account']['user_id'],
				"cronemail_object" => 'estimate',
				"cronemail_object_id" => $estid,
				"cronemail_temail_to" => $email
			);
			
			$Cron->addEmail($addCron);
			
			if(strlen($emailcc)){
				$add=array();
				$add['as_user_id'] = $_SESSION['account']['user_id'];
				$add['as_object'] = 'estimate';
				$add['as_object_id'] = $estid;
				$add['as_email'] = $emailcc;
				$as->add($add);

				$addCron=array(
					"cronemail_user_id" => $_SESSION['account']['user_id'],
					"cronemail_object" => 'estimate',
					"cronemail_object_id" => $estid,
					"cronemail_temail_to" => $emailcc
				);
				
				$Cron->addEmail($addCron);
	
				$addhistory['esthist_cc'] = $emailcc;
			}
		
			$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
			$addhistory['esthist_est_id']  = $estid;
			$addhistory['esthist_action']  = "email";
			$addhistory['esthist_email']  = $email;
			$this->est->addHistory($addhistory);
			
			$estimate = $this->est->getByID($estid);
			
			if($estimate['est_status']==1)$updEst['est_status'] = 5;
			
			$updEst['est_sent_email'] = 1;
			$this->est->update($estid,$updEst);
			
			
			$res['title'] =app::$lang->estimates['sent_tit'];
			$res['msg'] =app::$lang->estimates['sent_txt'];
			$res['id'] = $estid;
			$res['status'] = true;
			return $res;
		
		
	}
	
	function doUpdate(){
		
		$post = $_POST;
		
		$id = $this->updateEstimate($post);
		
		$res['status'] = true;
		return $res;
		
	}
	
	function createEstimate($post=array()){
		$add = array();
    
		$str = $post['client'];
		list($cid,$at) = explode("@", $str);
		
		$add['est_num'] = $post['est_num'];
		$add['est_user_id'] = $_SESSION['account']['user_id'];
		$add['est_name'] = $post['est_name'];
		$add['est_street'] = $post['est_street'];
		$add['est_city'] = $post['est_city'];
		$add['est_state'] = $post['est_state'];
		$add['est_zip'] = $post['est_zip'];
		$add['est_country'] = $post['est_country'];
		$add['est_date'] = \date("Y-m-d",strtotime($post['date']));
		
		if($at == "cli")$add['est_client_id'] = $cid; else $add['est_collab_id'] = $cid;
		
		$add['est_subtotal'] = $post['subtotal'];
		$add['est_taxes_cash'] = $post['tataltax_cash'];
		$add['est_taxes_cash'] = $post['tataltax_cash'];
		$add['est_discount_percentage'] = $post['discount'];		
		$add['est_total'] = $post['total'];		
		$add['est_total_due'] = $post['due'];		
		$add['est_discount_cash'] = $post['discount_cash'];
		$add['est_currency'] = $post['est_currency'];
		$add['est_prj'] = $post['est_prj'];
		$add['est_tags'] = $post['est_name']." ".$post['est_num']." ".$post['total'];	
		$add['est_total_base'] = $post['total_base'];
		$add['est_total_due_base'] = $post['due_base'];
		$add['est_exchange_rate'] = $post['est_exchange_rate'];
		$add['est_terms'] = app::strings_clear($post['est_terms']);
		$add['est_notes'] = app::strings_clear($post['est_notes']);
		$add['est_lang'] = $post['est_lang'];	
		
		$id = $this->est->add($add);
		
		foreach($post['estrow_srv'] as $k=>$v){
			$addline = array();
		
			$addline['estline_user_id'] = $_SESSION['account']['user_id'];
			$addline['estline_est_id']  = $id;
			$addline['estline_srv']  = app::strings_clear($v);
			$addline['estline_desc'] = app::strings_clear($post['estrow_desc'][$k]);
			$addline['estline_rate'] = $post['estrow_rate'][$k];
			$addline['estline_qty'] = $post['estrow_qty'][$k];
			$addline['estline_tax1_percent'] = $post['estrow_tax1'][$k];
			$addline['estline_tax1_cash'] = $post['estrow_tax1_cash'][$k];
			$addline['estline_tax2_percent'] = $post['estrow_tax2'][$k];
			$addline['estline_tax2_cash'] = $post['estrow_tax2_cash'][$k];
			$addline['estline_total'] = $post['estrow_total'][$k];
		
			$srvid = $this->handleService($addline);
			$addline['estline_srv_id'] = $srvid;
			$this->est->addLine($addline);
		}
		
		if(isset($post['attfile'])){
			
			foreach($post['attfile'] as $k=>$v){
				
				$addAtt['estatt_user_id']  = $_SESSION['account']['user_id'];
				$addAtt['estatt_est_id'] = $id;
				$addAtt['estatt_fname'] = $v;
				$addAtt['estatt_type'] = $post['attfiletype'][$k];
				$this->est->addAttach($addAtt);
			}

			$update['est_attach'] = 1;
			$this->est->update($id,$update);

		}
		
		$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['esthist_est_id']  = $id;
		$addhistory['esthist_action']  = "create";
		$this->est->addHistory($addhistory);
		
		return $id;
	}

	function updateEstimate($post=array()){
		$add = array();
    
		$str = $post['client'];
		list($cid,$at) = explode("@", $str);
		$id = $post['est_id'];
		
		$add['est_num'] = $post['est_num'];
		$add['est_user_id'] = $_SESSION['account']['user_id'];
		$add['est_name'] = $post['est_name'];
		$add['est_street'] = $post['est_street'];
		$add['est_city'] = $post['est_city'];
		$add['est_state'] = $post['est_state'];
		$add['est_zip'] = $post['est_zip'];
		$add['est_country'] = $post['est_country'];
		$add['est_date'] = \date("Y-m-d",strtotime($post['date']));
		
		if($at == "cli")$add['est_client_id'] = $cid; else $add['est_collab_id'] = $cid;
		
		$add['est_subtotal'] = $post['subtotal'];
		$add['est_taxes_cash'] = $post['tataltax_cash'];
		$add['est_taxes_cash'] = $post['tataltax_cash'];
		$add['est_discount_percentage'] = $post['discount'];		
		$add['est_total'] = $post['total'];		
		
		$add['est_total_due'] = $post['due'];		
		$add['est_discount_cash'] = $post['discount_cash'];
		$add['est_currency'] = $post['est_currency'];
		$add['est_prj'] = $post['est_prj'];
		$add['est_tags'] = $post['est_name']." ".$post['est_num']." ".$post['total'];	
		$add['est_total_base'] = $post['total_base'];
		$add['est_total_due_base'] = $post['due_base'];
		$add['est_exchange_rate'] = $post['est_exchange_rate'];
		$add['est_terms'] = app::strings_clear($post['est_terms']);
		$add['est_notes'] = app::strings_clear($post['est_notes']);
		$add['est_attach'] = 0;
		$add['est_lang'] = $post['est_lang'];	
		$this->est->update($id,$add);
		
		$this->est->delLines($id);
		
		foreach($post['estrow_srv'] as $k=>$v){
			$addline = array();
		
			$addline['estline_user_id'] = $_SESSION['account']['user_id'];
			$addline['estline_est_id']  = $id;
			$addline['estline_srv']  = app::strings_clear($v);
			$addline['estline_desc'] = app::strings_clear($post['estrow_desc'][$k]);
			$addline['estline_rate'] = $post['estrow_rate'][$k];
			$addline['estline_qty'] = $post['estrow_qty'][$k];
			$addline['estline_tax1_percent'] = $post['estrow_tax1'][$k];
			$addline['estline_tax1_cash'] = $post['estrow_tax1_cash'][$k];
			$addline['estline_tax2_percent'] = $post['estrow_tax2'][$k];
			$addline['estline_tax2_cash'] = $post['estrow_tax2_cash'][$k];
			$addline['estline_total'] = $post['estrow_total'][$k];
			
			$srvid = $this->handleService($addline);
			$addline['estline_srv_id'] = $srvid;
			$this->est->addLine($addline);
		}
		
		$this->est->delAttach($id);
		
		if(isset($post['attfile'])){
			
			foreach($post['attfile'] as $k=>$v){
				
				$addAtt['estatt_user_id']  = $_SESSION['account']['user_id'];
				$addAtt['estatt_est_id'] = $id;
				$addAtt['estatt_fname'] = $v;
				$addAtt['estatt_type'] = $post['attfiletype'][$k];
				$this->est->addAttach($addAtt);
			}
			
			$update['est_attach'] = 1;
			$this->est->update($id,$update);

		}
		
		$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['esthist_est_id']  = $id;
		$addhistory['esthist_action']  = "update";
		$this->est->addHistory($addhistory);
		
		return $id;
	}
	
	function handleService($srv=array()){
		$srvobj = new OrmSrv();
		$where['srv_name'] = $srv['estline_srv'];
		$where['srv_user_id'] = $_SESSION['account']['user_id'];
		$where['srv_cost'] = $srv['estline_rate'];
		$ret = $srvobj->getInfo($where);
		
		if(!$ret){
		
			$add['srv_user_id'] = $srv['estline_user_id'];
			$add['srv_name'] = $srv['estline_srv'];
			$add['srv_desc'] = $srv['estline_desc'];
			$add['srv_cost'] = $srv['estline_rate'];
			$add['srv_tax1'] = $srv['estline_tax1_percent'];
			$add['srv_tax2'] = $srv['estline_tax2_percent'];
			$add['srv_tax_cash1'] = $srv['estline_tax1_cash'];
			$add['srv_tax_cash2'] = $srv['estline_tax2_cash'];
			$add['srv_currency'] = $_SESSION['account']['user_currency'];
			$id = $srvobj->add($add);
		}else{

			 
			 $id=$ret['srv_id'];
		}
		 
		return $id;
	}
	
	function getAddresstoEstimate(){
				
			$str = $_GET['id'];
			
			list($cid,$at) = explode("@", $str);
			
			if($at=="cli"){
	
		    	$customer = $this->client->getByID($cid);
		    	
		    	if ($customer){
			    		
			    	$html='';
			    	$html.='<strong>'.$customer['client_name'].'</strong><br>';
			    	$html.=$customer['client_street'].'<br>';
					$html.=$customer['client_city'].' '.$customer['client_state'].' '.$customer['client_zip'].'<br>';
					$html.=$customer['country_printable_name'].'<br>';
					$html.='<a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chAddr" >Edit Address</a>';
				

			    	$res['name'] = $customer['client_name'];
			    	$res['street'] = $customer['client_street'];
			    	$res['city'] = $customer['client_city'];
			    	$res['state'] = $customer['client_state'];
			    	$res['zip'] = $customer['client_zip'];
			    	$res['country'] = $customer['client_country'];
			    	
		    	}else{
			    	$html='';
		    	}
					
				$res['status'] = true;
				$res['html'] = $html;
				$res['id'] = $customer['client_id'];
				$res['email'] = $customer['client_email'];
				$res['discount'] = $customer['client_discount'];			
				
			}else{
				
		    	$customer = $this->collabs->getByID($cid);
		    	
		    	if ($customer){
			    		
			    	$html='';
			    	$html.='<strong>'.$customer['cb_billing_name'].'</strong> <br>';
			    	$html.=$customer['cb_billing_street'].'<br>';
					$html.=$customer['cb_billing_city'].' '.$customer['cb_billing_state'].' '.$customer['cb_billing_zip'].'<br>';
					$html.=$customer['country_printable_name'].'<br>';
					$html.='<a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chAddr" >Edit Address</a>';
			    	
			    	$res['name'] = $customer['cb_billing_name'];
			    	$res['street'] = $customer['cb_billing_street'];
			    	$res['city'] = $customer['cb_billing_city'];
			    	$res['state'] = $customer['cb_billing_state'];
			    	$res['zip'] = $customer['cb_billing_zip'];
			    	$res['country'] = $customer['cb_billing_country'];
			    	
		    	}else{
			    	$html='';
		    	}
					
				$res['status'] = true;
				$res['html'] = $html;
				$res['id'] = $customer['cb_id'];
				$res['email'] = $customer['user_email'];
				$res['discount'] = 0;			
			}
			    	    
			return $res;
		}	
		
	function getBillingAddr(){
		
			$str = $_GET['id'];
			list($cid,$at) = explode("@", $str);
			
			if($at=="cli"){
	
		    	$customer = $this->client->getByID($cid);
		    	
		    	if ($customer){
			    		

			    	$this->view->name = $customer['client_name'];
			    	$this->view->street = $customer['client_street'];
			    	$this->view->city = $customer['client_city'];
			    	$this->view->state = $customer['client_state'];
			    	$this->view->zip = $customer['client_zip'];
			    	$this->view->email = $customer['client_email'];
			    	$country = $customer['client_country'];
			    	
			    	
		    	}
			}else{
				
		    	$customer = $this->collabs->getByID($cid);
		    	
		    	if ($customer){
			    		
					$this->view->iscollab = 1;
			    	$this->view->name = $customer['cb_billing_name'];
			    	$this->view->street = $customer['cb_billing_street'];
			    	$this->view->city = $customer['cb_billing_city'];
			    	$this->view->state = $customer['cb_billing_state'];
			    	$this->view->zip = $customer['cb_billing_zip'];
			    	$this->view->email = $customer['user_email'];
			    	$country = $customer['cb_billing_country'];
			    	
		    	}
				
			}
			
			
			$this->view->id =$str;
		    $this->view->countryList=$this->country->getCountriesList();
		    $this->view->statesList=$this->country->getStateList($country);
			$this->view->country = $country;
			$this->view->setTemplate('billaddr.tpl');  
			$res['html'] = $this->view->render();
			$res['status'] = true;    	    
			return $res;
	}	

	function getMyBillingAddr(){
		    $this->view->countryList=$this->country->getCountriesList();
		    $this->view->statesList=$this->country->getStateList($_SESSION['account']['set_billing_country']);
			$this->view->country = $_SESSION['account']['set_billing_country'];
		
			$this->view->setTemplate('mybilladdr.tpl');  
			$res['html'] = $this->view->render();
			$res['status'] = true;    	    
			return $res;
		
	}

	function doChAddrView(){
		$str = $_POST['billid'];
		list($cid,$at) = explode("@", $str);
		
		if($at=="cli"){
			
			$upd['client_name'] = app::strings_clear($_POST['billing_name']);
			$upd['client_street'] = app::strings_clear($_POST['billing_street']);
			$upd['client_city'] = app::strings_clear($_POST['billing_city']);
			$upd['client_zip'] = app::strings_clear($_POST['billing_zip']);
			$upd['client_state'] = app::strings_clear($_POST['billing_state']);
			$upd['client_email'] = app::strings_clear($_POST['billing_email']);
			$upd['client_country'] = $_POST['billing_country'];
			$this->client->update($cid,$upd);
			
			$updEst['est_name'] = $upd['client_name'];
			$updEst['est_street'] = $upd['client_street'];
			$updEst['est_city'] = $upd['client_city'];
			$updEst['est_zip'] = $upd['client_zip'];
			$updEst['est_state'] = $upd['client_state'];
			$updEst['est_country'] = $upd['client_country'];
			$this->est->update($_POST['estid'],$updEst);
			$country = $this->country->getCountryName($upd['client_country']);
			
	    	$html='';
	    	$html.='<span class="name">'.$upd['client_name'].'<span>';
	    	$html.='<span>'.$upd['client_street'].'</span>';
			$html.='<span>'.$upd['client_city'].' '.$upd['client_state'].' '.$upd['client_zip'].'</span>';
			$html.='<span>'.$country.'</span>';
			$html.=' <a href="#"  class="btn btn-sm " style="padding-right: 0;" id="chAddr" >Edit Address</a>';
			
		}else{
			
			$upd['cb_billing_name'] = app::strings_clear($_POST['billing_name']);
			$upd['cb_billing_street'] = app::strings_clear($_POST['billing_street']);
			$upd['cb_billing_city'] = app::strings_clear($_POST['billing_city']);
			$upd['cb_billing_zip'] = app::strings_clear($_POST['billing_zip']);
			$upd['cb_billing_state'] = app::strings_clear($_POST['billing_state']);
			$upd['cb_billing_country'] = $_POST['billing_country'];
			$this->collabs->update($cid,$upd);
			
			$country = $this->country->getCountryName($upd['cb_billing_country']);
			$updEst['est_name'] = $upd['cb_billing_name'];
			$updEst['est_street'] = $upd['cb_billing_street'];
			$updEst['est_city'] = $upd['cb_billing_city'];
			$updEst['est_zip'] = $upd['cb_billing_zip'];
			$updEst['est_state'] = $upd['cb_billing_state'];
			$updEst['est_country'] = $upd['cb_billing_country'];
			$this->est->update($_POST['estid'],$updEst);
			
			if(strlen($upd['cb_billing_state']))$comma=', '; else $comma=' ';
	    	$html='';
	    	$html.='<span class="name">'.$upd['cb_billing_name'].'<span>';
	    	$html.='<span>'.$upd['cb_billing_street'].'</span>';
			$html.='<span>'.$upd['cb_billing_city'].$comma.$upd['cb_billing_state'].' '.$upd['cb_billing_zip'].'</span>';
			$html.='<span>'.$country.'</span>';
			$html.=' <a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chAddr" >Edit Address</a>';
		}
	
		$res['addr'] = $html;
	 
	
		return $res;
			
		
	}

	function doChMyAddrView(){

	    $acc = new  OrmAccounts();
	    
	    $upd['set_billing_name']= app::strings_clear($_POST['billing_name']);
	    $upd['set_billing_street']= app::strings_clear($_POST['billing_street']);
	    $upd['set_billing_city'] = app::strings_clear($_POST['billing_city']);
	    $upd['set_billing_state'] = app::strings_clear($_POST['billing_state']);
	    $upd['set_billing_zip'] = app::strings_clear($_POST['billing_zip']);
	    $upd['set_billing_country'] = app::strings_clear($_POST['billing_country']);
	    $acc->updateSettings($_SESSION['account']['user_id'],$upd);
	    
	    $this->session->refresh();

		$country = $this->country->getCountryName($upd['set_billing_country']);
		if(strlen($upd['set_billing_state']))$comma=', '; else $comma=' ';
		
    	$html='';
    	$html.='<span class="name">'.$upd['set_billing_name'].'<span>';
    	$html.='<span>'.$upd['set_billing_street'].'</span>';
		$html.='<span>'.$upd['set_billing_city'].$comma.$upd['set_billing_state'].' '.$upd['set_billing_zip'].'</span>';
		$html.='<span>'.$country.'</span>';
		$html.=' <a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chMyAddr" >Edit Address</a>';
	
		$res['addr'] = $html;
		return $res;
	}

	function doChAddr(){
    
	$str = $_POST['billid'];
	list($cid,$at) = explode("@", $str);
	
	if($at=="cli"){
		
		$upd['client_name'] = app::strings_clear($_POST['billing_name']);
		$upd['client_street'] = app::strings_clear($_POST['billing_street']);
		$upd['client_city'] = app::strings_clear($_POST['billing_city']);
		$upd['client_zip'] = app::strings_clear($_POST['billing_zip']);
		$upd['client_state'] = app::strings_clear($_POST['billing_state']);
		$upd['client_email'] = app::strings_clear($_POST['billing_email']);
		$upd['client_country'] = $_POST['billing_country'];
		$this->client->update($cid,$upd);

		
	}else{
		
		$upd['cb_billing_name'] = app::strings_clear($_POST['billing_name']);
		$upd['cb_billing_street'] = app::strings_clear($_POST['billing_street']);
		$upd['cb_billing_city'] = app::strings_clear($_POST['billing_city']);
		$upd['cb_billing_zip'] = app::strings_clear($_POST['billing_zip']);
		$upd['cb_billing_state'] = app::strings_clear($_POST['billing_state']);
		$upd['cb_billing_country'] = $_POST['billing_country'];
		$this->collabs->update($cid,$upd);

		
	}

    $selectbox='';

    
    $selCust['client_user_id'] = $_SESSION['account']['user_id'];
    $selCust['client_trash'] = 0;
    $selCust['client_collab'] = 0;
    $customersList = $this->client->getAllList($selCust);
    $selectbox.='<optgroup label="'.app::$lang->menu['clients'].'">';
    foreach($customersList as $v){
	    
	    if($v['client_id']==$cid && $at=="cli"){
		    $selectbox.='<option selected value="'.$v['client_id'].'@cli">'.$v['client_name'].'</option>';
			    
	    }else{
		     $selectbox.='<option value="'.$v['client_id'].'@cli">'.$v['client_name'].'</option>';
	    }
	    
    }
    $selectbox.='</optgroup>';
    
    
    $collabs = $this->collabs->getAllList(array(),"users.user_name ASC");
    
    if($collabs){
	    $selectbox.='<optgroup label="'.app::$lang->menu['colleague'].'">';
	    foreach($collabs as $v){

	    if($v['cb_id']==$cid && $at=="usr"){
		    $selectbox.='<option selected value="'.$v['cb_id'].'@usr">'.$v['cb_billing_name'].'</option>';
			    
	    }else{
		    $selectbox.='<option value="'.$v['cb_id'].'@usr">'.$v['cb_billing_name'].'</option>';
	    }
		    
	    }
	    $selectbox.='</optgroup>';
	    
    }

	
//	$res['addr'] = $html;
    $res['selBox'] = $selectbox;
	$res['id'] = $str;
	return $res;
	}
	
	function changeDateRange(){
		
		$_SESSION['EST_STARTDATE'] = $_POST['start'];
		$_SESSION['EST_ENDDATE'] = $_POST['end'];
		
		$res['s'] = $_SESSION['EST_SEARCH_STR'];
		$res['status'] = true;
		return $res;
	}
	
	function doApplyFilter(){
		
		if(isset($_POST['esttypes'])){
			$_SESSION['EST_TYPE'] = array();
			$arr=array();
			foreach($_POST['esttypes'] as $k=>$v){
				
				$arr[]=$v;
				$_SESSION['EST_TYPE'] = $arr;
				
			}
		}
		
		$_SESSION['EST_ARC'] = $_POST['estarc'];
		
		
		$res['s'] = $_SESSION['EST_SEARCH_STR'];
		$res['status'] = true;
		return $res;
	}
	
	function delete($id=0){
		
		if(!$id)$id=$_POST['id'];
		
		$upd['est_trash'] = 1;
		$this->est->update($id,$upd);
		
		$res['status'] = true;
		return $res;
		
	}
	
	function deleteBundle(){

	    
	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){
			    $this->delete($val);
		    }
		    
		    $res['status']=true;
		    return $res;
		    
		    
	    }else{
		    $res['title']="Operation Failed";
		    $res['status']=false;
		    $res['msg']="No rows selected. Please select at least one template.";
		    return $res;
	    }	    
		
		
	}
	
    function edit(){
	    
	    $rates = array();
	    if(!isset($this->args[0])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
	    }
	    $id = $this->args[0];
	    $est = $this->est->getByID($id);
	    
	    if(!$est){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
	    }
	    
		$selPrj['project_user_id'] = $_SESSION['account']['user_id'];
		$selPrj['project_trash'] =0;
		$selPrj['project_default'] =0;
		$this->view->prj = $this->prj->getAllList($selPrj);
	    
	    $this->view->flist = $this->est->getAttachList($id);
	    $this->view->ihost =  app::$proto.app::$config->site['userhost'] ;
	    
	    $est = $this->est->getByID($id);
		$this->view->est = $est;
		
		$selLines['estline_user_id'] = $_SESSION['account']['user_id'];
		$selLines['estline_est_id'] = $id;
		$this->view->lines = $this->est->getLinesList($selLines);

		$allcurrencies = $this->misc->getCurrencies();

		foreach($allcurrencies as $k=>$v){
			$ra = $this->misc->getRateByCurrency($v['currency_iso'],$_SESSION['account']['user_currency']);
			$rates[$k]['curr'] = $v['currency_iso'];
			$rates[$k]['val'] = $ra['exr_rate'];
		}

		$invlang = new lang(app::$settings->lang_path._.'inv.'.$est['est_lang'].'.lng');
		$this->view->invlang = $invlang->details;

		
		$this->view->rates = $rates;//$this->misc->getAllRates($_SESSION['account']['user_currency']);

	    $this->view->countryList=$this->country->getCountriesList();
	    $this->view->statesList=$this->country->getStateList($_SESSION['account']['user_country']);

		$this->view->langs = $this->misc->getAllLangs();
		$this->view->lng_clients = app::$lang->clients;
		$this->view->currencies = $allcurrencies;
		$this->view->months = $this->misc->getMonthsList();
	
		$selClients['client_collab'] = 0;
		$this->view->clients = $this->client->getAllList($selClients);
		$this->view->collabs = $this->collabs->getAllList(array(),"users.user_name ASC");
		
        $this->view->setTemplate('edit.tpl');  
        return $this->view;
    }
    
	function copy(){
		
		if(!isset($this->args[0]) || (!$this->args[0])){
			 \jet\redirect('/estimates',true); 
		}
		
		$estimateID = $this->copyEstimate($this->args[0]);
		
		//Get New Estimate
		
		
		\jet\redirect('/estimates/view/'.$estimateID,true);
	}

	function copyEstimate($id=0){
		/***
			Get OLD estoice
		*/
		
		$estimateOldID = $id;
		
		$estimate = $this->est->getByID($estimateOldID);
		
		if(!$estimate) return false;
		
	    $flist = $this->est->getAttachList($id);
	
		$selLines['estline_user_id'] = $_SESSION['account']['user_id'];
		$selLines['estline_est_id'] = $id;
		$lines = $this->est->getLinesList($selLines);
		
		unset($estimate['est_id']);
		$estimateNew = $estimate;
		
		// Create New estoice
				
		$newNum = $this->est->getLastNum();
		$estimateNew['est_num'] = $newNum;
		$estimateNew['est_date'] =  \date('Y-m-d');
		$estimateNew['est_status'] = 1;
		$estimateNew['est_total_due'] = $estimate['est_total'];
		$estimateNew['est_total_due_base'] = $estimate['est_total_base'];
		$estimateNew['est_sent_email']=0;
		$estimateNew['est_view']=0;
		$estimateNew['est_arc']=0;
		$estimateNew['est_prj']=0;
		$estimateNew['est_recurring'] = 0;
		$estimateNew['est_tags']	= $estimate['est_name']." ".$newNum." ".$estimate['est_total'];
		
		$estimateID = $this->est->add($estimateNew);
		
		foreach($lines as $val){
			
			$val['estline_est_id'] = $estimateID;
			unset($val['estline_id']);
			$this->est->addLine($val);
		}
		
		if($flist){
			
			foreach($flist as $v){
				
				$v['estatt_est_id'] = $estimateID;
				unset($v['estatt_id']);
				$this->est->addAttach($v);
				
			}

		}
		
		$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['esthist_est_id']  = $estimateID;
		$addhistory['esthist_action']  = "create";
		$this->est->addHistory($addhistory);
			
		return $estimateID;
		
	}
	
	function doarc(){
		$id = $_POST['id'];
		$act = $_POST['act'];
		$updEstimate['est_arc'] = $act;
		$this->est->update($id, $updEstimate);	
		
		$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['esthist_est_id']  = $id;
		if($act==1)$addhistory['esthist_action']  = "arc";else $addhistory['esthist_action']  = "unarc";
		$this->est->addHistory($addhistory);

		$res['status'] = true;
		return $res;	
	}
	
	function doEmail(){
		$valid = new OrmValidator();
		
		$email = strtolower($_POST['send_email']);		
		$emailcc = strtolower($_POST['send_email_cc']);		
		$estid = $_POST['email_est_id'];
		
		if(!$valid->email($email) || (strlen($emailcc) && !$valid->email($emailcc))){
			
			$res['status'] = false;
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->common['nve'];
			return $res;
		}
		
		$as = new OrmAntispam();
		$selAS = $as->getLates($email,'estimate',$estid);
		if(!$selAS){
			$Cron = new OrmCron();
			$add=array();
			$add['as_user_id'] = $_SESSION['account']['user_id'];
			$add['as_object'] = 'estimate';
			$add['as_object_id'] = $estid;
			$add['as_email'] = $email;
			$as->add($add);
			
			$addCron=array(
				"cronemail_user_id" => $_SESSION['account']['user_id'],
				"cronemail_object" => 'estimate',
				"cronemail_object_id" => $estid,
				"cronemail_temail_to" => $email
			);
			
			$Cron->addEmail($addCron);
			
			if(strlen($emailcc)){
				$add=array();
				$add['as_user_id'] = $_SESSION['account']['user_id'];
				$add['as_object'] = 'estimate';
				$add['as_object_id'] = $estid;
				$add['as_email'] = $emailcc;
				$as->add($add);

				$addCron=array(
					"cronemail_user_id" => $_SESSION['account']['user_id'],
					"cronemail_object" => 'estimate',
					"cronemail_object_id" => $estid,
					"cronemail_temail_to" => $emailcc
				);
				
				$Cron->addEmail($addCron);
	
				$addhistory['esthist_cc'] = $emailcc;
			}
		
			$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
			$addhistory['esthist_est_id']  = $estid;
			$addhistory['esthist_action']  = "email";
			$addhistory['esthist_email']  = $email;
			$this->est->addHistory($addhistory);
			
			$estimate = $this->est->getByID($estid);
			
			if($estimate['est_status']==1)$updEst['est_status'] = 5;
			
			$updEst['est_sent_email'] = 1;
			$this->est->update($estid,$updEst);
			
			
			$res['title'] =app::$lang->estimates['sent_tit'];
			$res['msg'] =app::$lang->estimates['sent_txt'];
			$res['id'] = $estid;
			$res['status'] = true;
			return $res;

		}else{
			
			
			$res['status'] = false;
			$res['title'] = app::$lang->common['operation_failed'];
			$res['msg'] = app::$lang->common['spam_msg'];
			return $res;
		}
		
		
		
	}
	
	function payBundle(){
		$_SESSION['EST_PAY_IDS'] = array();
	    if(isset($_POST['row']) && count($_POST['row'])){
		    
		    foreach($_POST['row'] as $val){
			    $_SESSION['EST_PAY_IDS'][] = $val;
		    }
		    
		    $res['url'] = "/estimates/bulkPayments";
		    $res['status']=true;
		    return $res;
		    
		    
	    }else{
		    $res['title']="Operation Failed";
		    $res['status']=false;
		    $res['msg']="No rows selected. Please select at least one template.";
		    return $res;
	    }	    
	}
	
	function bulkPayments(){
		
		if(!isset( $_SESSION['EST_PAY_IDS'])){
			$this->view->setPath(app::$device.'/common'); 
			$this->view->setTemplate('404.tpl');
			return $this->view;	    
		}

		$where['est_id'] = $_SESSION['EST_PAY_IDS'];
		$where['est_user_id'] = $_SESSION['account']['user_id'];
		$where['est_status!']=2;
		
		
		$this->view->paymethod = $this->trans->getPaymentMethodsList();
		$this->view->estimates = $this->est->getAllList($where);
		//unset($_SESSION['EST_PAY_IDS']);
        $this->view->setTemplate('bulkpay.tpl');  
        return $this->view;
		
		
	}
	
	function sendBundle(){
		
		$errarr = array();
		$valid = new OrmValidator();
		$res['status'] = true;  
		$i=0; 
		$as = new OrmAntispam();
		$Cron = new OrmCron();
		
	    foreach($_POST['row'] as $val){
		   $est = $this->est->getByID($val);
		   
		   if($est['est_client_id']>0){
			   
			   $recepientInfo = $this->client->getByID($est['est_client_id']);
			   
			   $email = $recepientInfo['client_email'];
			   $emailcc = $recepientInfo['client_ccemail'];
			   
			   if(!$valid->email($email) || (strlen($emailcc) && !$valid->email($emailcc)) ){
			   		$res['status'] = false; 
			   		$errarr[$i]['id'] = $est['est_id'];
			   		$errarr[$i]['num'] = $est['est_num'];
			   		$errarr[$i]['name'] = $recepientInfo['client_name'];
			   		$errarr[$i]['msg'] = app::$lang->estimates['neip'];
			   		$i++;
			   }else{
					$selAS = $as->getLates($email,'estimate',$est['est_id']);
					if(!$selAS){   
						$addAs = array();
						$addAs['as_user_id'] = $_SESSION['account']['user_id'];
						$addAs['as_object'] = 'estimate';
						$addAs['as_object_id'] = $est['est_id'];
						$addAs['as_email'] = $email;
						$as->add($addAs);


						$addCron=array(
							"cronemail_user_id" => $_SESSION['account']['user_id'],
							"cronemail_object" => 'estimate',
							"cronemail_object_id" => $est['est_id'],
							"cronemail_temail_to" => $email
						);
						$Cron->addEmail($addCron);
		
						if(strlen($emailcc)){
							$addCron=array(
								"cronemail_user_id" => $_SESSION['account']['user_id'],
								"cronemail_object" => 'estimate',
								"cronemail_object_id" => $est['est_id'],
								"cronemail_temail_to" => $emailcc
							);
							
							$Cron->addEmail($addCron);
							$addhistory['esthist_cc'] = $emailcc;
						}
						
						$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
						$addhistory['esthist_est_id']  = $est['est_id'];
						$addhistory['esthist_action']  = "email";
						$addhistory['esthist_email']  = $email;
						$this->est->addHistory($addhistory);
						
						
						
						if($est['est_status']==1)$updEst['est_status'] = 5;
						
						$updEst['est_sent_email'] = 1;
						$this->est->update($est['est_id'],$updEst);
							
					
					}	else{
						
				   		$res['status'] = false; 
				   		$errarr[$i]['id'] = $est['est_id'];
				   		$errarr[$i]['num'] = $est['est_num'];
				   		$errarr[$i]['name'] = $recepientInfo['client_name'];
				   		$errarr[$i]['msg'] = app::$lang->common['spam_msg'];
				   		$i++;
						
						
					}			   
				   
			   }
			   
		   }else{
			   
			   $recepientInfo = $this->collabs->getByID($est['est_collab_id']);

			   $email = $recepientInfo['user_email'];
			   if(!$valid->email($email)){
			   		$res['status'] = false; 
			   		$errarr[$i]['id'] = $est['est_id'];
			   		$errarr[$i]['num'] = $est['est_num'];
			   		$errarr[$i]['name'] = $recepientInfo['cb_billing_name'];
			   		$errarr[$i]['msg'] = app::$lang->common['neip'];
			   		$i++;
			   }else{
					$selAS = $as->getLates($email,'estimate',$est['est_id']);
					if(!$selAS){   
						$addAs = array();
						$addAs['as_user_id'] = $_SESSION['account']['user_id'];
						$addAs['as_object'] = 'estimate';
						$addAs['as_object_id'] = $est['est_id'];
						$addAs['as_email'] = $email;
						$as->add($addAs);
			   
						$addCron=array(
							"cronemail_user_id" => $_SESSION['account']['user_id'],
							"cronemail_object" => 'estimate',
							"cronemail_object_id" => $est['est_id'],
							"cronemail_temail_to" => $email
						);
						
						$Cron->addEmail($addCron);
						
						$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
						$addhistory['esthist_est_id']  = $est['est_id'];
						$addhistory['esthist_action']  = "email";
						$addhistory['esthist_email']  = $email;
						$this->est->addHistory($addhistory);
						
						
						
						if($est['est_status']==1)$updEst['est_status'] = 5;
						
						$updEst['est_sent_email'] = 1;
						$this->est->update($est['est_id'],$updEst);
	
				   }else{
						
				   		$res['status'] = false; 
				   		$errarr[$i]['id'] = $est['est_id'];
				   		$errarr[$i]['num'] = $est['est_num'];
				   		$errarr[$i]['name'] = $recepientInfo['cb_billing_name'];
				   		$errarr[$i]['msg'] = app::$lang->common['spam_msg'];
				   		$i++;
						
						
					}	
				   
			   }

		   }
		   
	    }
	    
	    if(!$res['status']){
		    $html ="";
		    $this->view->err = $errarr;
		    $this->view->setTemplate('errbulkemail.tpl'); 
		    $html = $this->view->render();
			$res['html'] = $html;
		    
	    }else{
			$res['title'] =app::$lang->estimates['sent_tit'];
			$res['msg'] =app::$lang->estimates['sent_txt'];
	    }
		
	    return $res;
		
	}
	
	function arcBundle(){
	    $where['est_id'] = $_POST['row'];
	    $where['est_user_id'] = $_SESSION['account']['user_id'];
	    $upd['est_arc'] = $_POST['act'];
	    $this->est->updateMass($where,$upd);
	    
	    $res['status']=true;
	    return $res;
	}

	function changePage(){
		
		$_SESSION['EST_PAGE'] = $_POST['p'];
		
		$res['status'] = true;
		return $res;
	}
	
	function getEmailsForSend(){
		
		$str = $_GET['id'];
		list($cid,$at) = explode("@", $str);
		
		if($at=="cli"){

	    	$customer = $this->client->getByID($cid);
	    	
	    	if ($customer){
		    	$res['email'] = $customer['client_email'];
		    	$res['emailcc'] = $customer['client_ccemail'];
	    	}
		}else{
			
	    	$customer = $this->collabs->getByID($cid);
	    	
	    	if ($customer){

		    	$res['email'] = $customer['user_email'];;
		    	$res['emailcc'] = "";
		    	
	    	}
			
		}
		
		$res['status'] = true;
		return $res;
	}

	function changeLang(){
		
		$lng = $_GET['lang'];
		
		$_SESSION['EST_LANG'] = $lng;
		$invlang = new lang(app::$settings->lang_path._.'inv.'.$_SESSION['EST_LANG'].'.lng');
		
		$res['lang'] = $invlang->details;
		$res['status'] = true;
		return $res;
		
	}
	
	function doAccept(){
		
		$upd['est_status'] = 2;
		$this->est->update($_POST['id'],$upd);
		
		$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['esthist_est_id']  = $_POST['id'];
		$addhistory['esthist_action']  = "accept";
		$this->est->addHistory($addhistory);
		
		$res['status'] = true;
		return  $res;
		
	}

	function doDecline(){
		
		$upd['est_status'] = 3;
		$this->est->update($_POST['id'],$upd);
		
		$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['esthist_est_id']  = $_POST['id'];
		$addhistory['esthist_action']  = "decline";
		$this->est->addHistory($addhistory);
		
		$res['status'] = true;
		return  $res;
		
	}

	function doPending(){
		
		$upd['est_status'] = 5;
		$this->est->update($_POST['id'],$upd);
		
		$addhistory['esthist_user_id']  = $_SESSION['account']['user_id'];
		$addhistory['esthist_est_id']  = $_POST['id'];
		$addhistory['esthist_action']  = "pending";
		$this->est->addHistory($addhistory);
		
		$res['status'] = true;
		return  $res;
		
	}


}
	
	
	
