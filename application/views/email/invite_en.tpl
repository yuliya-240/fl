<p>Please join me in new amazing online service for Freelancers & Creatives - <a href="http://floctopus.com">Floctopus.com</a>. This is a place where everyone can collaborate on projects, coordinate tasks and schedules, manage his freelancer's business. it's easy to use.<p>

<a href="{{link}}">{{link}}</a>

<p>To authorize, enter your email address as the login name.  Upon your first login, you will be asked to create your password which will be known only to you.</p>

Regards,
{{acs.user_name}}<br>

<p>This email has been sent automatically.  Please do not respond to it.</p>