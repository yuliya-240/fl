Hello,<br>
									
<p>My name is Val, I’m the founder and CEO of <a href="http://floctopus.com">Floctopus.com</a>. I’d like to personally welcome you to our community and share some introductory tips with you.<p><br>
	<p>Link to Login page: <a href="http://secure.floctopus.com">http://secure.floctopus.com</a></p>
<p>Link to Help Center: <a href="http://help.floctopus.com">http://help.floctopus.com</a></p>
<p>Link to Our Blog: <a href="http://floctopus.com/blog">http://www.floctopus.com/blog</a></p>
<p>Link to Our Facebook page: <a href="http://facebook.com/floctopus">http://facebook.com/floctopus</a></p><br>

<p>If you ever have a suggestion for a new feature, just let me know. Even bad ideas come from an unmet desire, and I am confident we can deliver.</p><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
    <tr>
        <td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
           <a href="http://www.floctopus.com">http://www.floctopus.com</a>
            <br>
			Thank you,<br/>
			Floctopus.com Team <br/>
        </td>
    </tr>
</table>