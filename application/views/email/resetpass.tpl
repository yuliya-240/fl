Hello,

Someone has requested a link to change your password, and you can do this through the link below.<br>

<p><a href="{{proto}}{{host}}/login/resetp/?k={{key}}">Change my password</a><p><br>
or use this link:<br><br>
<a href="{{proto}}{{host}}/login/resetp/?k={{key}}">{{proto}}{{host}}/login/resetp/?k={{key}}</a><br><br>

<p>If you didn't request this, please ignore this email.</p>

<p>Your password won't change until you access the link above and create a new one.</p><br><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="max-width: 500px;" class="responsive-table">
    <tr>
        <td style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
           <a href="http://www.floctopus.com">http://www.floctopus.com</a>
            <br>
			Thank you,<br/>
			Floctopus.com Team <br/>
        </td>
    </tr>
</table>