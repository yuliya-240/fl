{{uname}} update  Issue #{{id}}:<br>
Title: {{subj}}<br>
{{msg}}
<br>
Link: <a href="{{host}}/issues/view/{{id}}">{{host}}/issues/view/{{id}}</a>
<br><br>

---<br>
You are receiving this because you are involved to issue #{{id}}.<br>
DO NOT reply to this email directly, view it on <a href="{{host}}/issues/view/{{id}}">Floctopus.com</a>





 