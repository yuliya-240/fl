 New Invoice #{{inv.inv_num}} Has Arrived !<br><br>
	                                 
{%if msg%} {{msg|nl2br}}<br><br>{%endif%}

To view your invoice from {{acs.user_name}} or to download a PDF copy for your records, click the link below:<br/>
<a href="{{link}}">{{link}}</a><br/>
<br/>	   