{% extends skin~"/common/root.tpl" %}
{%block headcss%}
    <link rel="stylesheet" type="text/css" href="/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    	<link href="/assets/lib/multiselect/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
{%endblock%}

{%block content%}
<input type="hidden" id="dpformat" value="{{dpformat}}">
<div class="main-content" >
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<input type="hidden" name="project_id" value="{{prj.project_id}}">	
			
		<div class="panel panel-default panel-borders" style="margin-bottom:5px;">
	        <div class="panel-heading">
		        <span class="title">{{lng_projects.btn_edit}}</span>
		    </div>
	        <div class="panel-body">
				
                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_projects.name}}<span style="color: red;">*</span></label>
                  <div class="col-sm-9">
                    <input class="form-control" name="project_name" value="{{prj.project_name}}" data-errorbox="#errname" data-msg-required="{{lng_projects.name_req}}"  required="" placeholder="{{lng_projects.ph_name}}" type="text">
                    <div id="errname" style="color:red;"></div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_projects.tbl_status}}</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="project_status">
	                    {%for s in statuses%}
	                    	<option {%if s.prjstatus_status==prj.project_status%}selected{%endif%} value="{{s.prjstatus_status}}">{{s.prjstatus_name}}</option>
	                    {%endfor%}
                    </select>    
                    
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_projects.desc}}</label>
                  <div class="col-sm-9">
                    <textarea name="project_desc" class="form-control">{{prj.project_desc}}</textarea>
                    
                  </div>
                </div>
	
	        </div>
		</div>
		
		<div class="panel panel-default panel-borders" style="margin-bottom:5px;" id="_mil">
	        <div class="panel-heading">
		        <span class="title">{{lng_projects.title_milestones}}</span>
		    </div>
	        <div class="panel-body">
				<div id="milestones-box">
                	{%for m in milestones%}
					<div class="form-group" id="{{m.prjm_id}}" >
					  
						<div class="col-sm-8">
						    <label class="col-xs-12" style="padding-left:0px;">{{lng_projects.ml_name}}<span style="color: red;">*</span></label>
						    <input class="form-control" name="milestone[{{m.prjm_id}}]" data-errorbox="#err{{m.prjm_id}}" data-msg-required="{{lng_projects.name_req}}"  required="" placeholder="{{lng_projects.ml_name}}" value="{{m.prjm_name}}" type="text">
						    <div id="err{{m.prjm_id}}" style="color:red;"></div>
						</div>
						<div class="col-sm-3">
						    <label class="col-xs-12" style="padding-left:0px;">{{lng_projects.deadline}}<span style="color: red;">*</span></label>
						    <input class="form-control datetimepicker" data-min-view="2" readonly="" value="{{m.prjm_deadline|date(acs.user_dformat)}}"   name="deadline[{{m.prjm_id}}]"    type="text">
						</div>
						<div class="col-sm-1">
							<a href="#" data-id="{{m.prjm_id}}" class="text-danger"><i class="material-icons">close</i></a>
						</div>	
					</div>
					
					{%endfor%}
				</div>
				<div class="text-center">
				<button id="addMilestone" class="btn btn-success">{{lng_projects.btn_add_ms}}</button>
				</div>
	        </div>
		</div>
		
		<div class="panel panel-default panel-borders" id="_collab" style="margin-bottom:5px;" >
	        <div class="panel-heading">
		        <span class="title">{{lng_projects.title_collaborators}}</span>
		    </div>
	        <div class="panel-body">
		        {%if collabs%}
				<div class="form-group">
				

					<div class="col-sm-12">

						<select multiple="" class="tag-input-style form-control" name="collabs[]"  id="emplist" data-placeholder="{{lng_projects.ch_empl}}">
							{%for e in collabs%}
								<option {%if e.user_id in collabssel%}selected{%endif%} value="{{e.user_id}}">{{e.user_username}} ({{e.user_name}})</option>
							{%endfor%}
						</select>								
					</div>
					
					
				</div>
				{%else%}
				<div class="text-center">{{lng_common.norecords}} <a href="/users/add">{{lng_common.title_add_user}}</a></div>
				
				{%endif%}
		        
	        </div>
		</div>    
	
		<button type="submit" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
		<a href="/projects" class="btn btn-default btn-lg pull-left">{{lng_common.btn_cancel}}</a>
		<div style="clear:both;margin-bottom: 20px;"></div>	
			
		</form>
	</div>
</div>		
</div>

{%endblock%}
{%block js%}

	<script src="/assets/lib/quicksearch/jquery.quicksearch.js"></script>
	<script src="/assets/lib/multiselect/js/jquery.multi-select.js"></script>

    <script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/js/dev/projects_edit.js"></script>
{%endblock%}

