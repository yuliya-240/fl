<div class="form-group" id="{{uniq}}">
  
    <div class="col-sm-8" style="margin-bottom:5px;" >
	    
	    <input class="form-control" name="milestone[{{uniq}}]" data-errorbox="#err{{uniq}}" data-msg-required="{{lng_projects.name_req}}"  required="" placeholder="{{lng_projects.ml_name}}" type="text">
	    <div id="err{{uniq}}" style="color:red;"></div>
    </div>
    <div class="col-sm-4" style="margin-bottom:5px;">
	    
	   {#<input class="form-control datetimepicker pull-left" data-min-view="2" readonly="" value="{{today}}"   name="deadline[{{uniq}}]" type="text">#}
	   
	<div class="input-group pull-left">
		<span class="input-group-addon">
			<i class="fa fa-calendar bigger-110"></i>
		</span>
		<input type="text" data-date-format="{{dpformat}}" readonly="" value="{{today|date(acs.user_dformat)}}" name="deadline[{{uniq}}]" class="form-control date-picker">
		
	</div>
	   

<button class="btn pull-left btn-space btn-default text-danger del-mil" type="button" data-id="{{uniq}}" style="padding-top: 7px !important; border: none !important; position: absolute; background: none !important;  padding-left: 6px;" ><span class="s7-close-circle" style="font-size:28px; font-weight: 700;"></span></button>		

    </div>
	

</div>
