{% extends skin~"/common/root.tpl" %}

{%block preheadcss%}
     <link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.min.css"/>
{%endblock%}



{%block content%}
<input type="hidden" id="dpformat" value="{{dpformat}}">
<div class="main-content" >
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
			
			
		<div class="panel panel-default panel-borders" style="margin-bottom:5px;">
	        <div class="panel-heading">
		        <span class="title">{{lng_projects.btn_add}}</span>
		    </div>
	        <div class="panel-body">
				
                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_projects.name}}<span style="color: red;">*</span></label>
                  <div class="col-sm-9">
                    <input class="form-control" name="project_name" data-errorbox="#errname" data-msg-required="{{lng_projects.name_req}}"  required="" placeholder="{{lng_projects.ph_name}}" type="text">
                    <div id="errname" style="color:red;"></div>
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_projects.desc}}</label>
                  <div class="col-sm-9">
                    <textarea name="project_desc" class="form-control"></textarea>
                    
                  </div>
                </div>

				<div class="form-group">
					<label class="col-sm-3 control-label">{{lng_common.client}}</label>
		    		<div class="col-sm-6" style="padding-right: 4px;">

				    	<select id="client" name="client" class="select2">
					    	
				    		<option value="0">{{lng_projects.not_assigned}}</option>
				    		{%if clients%}
				    		<optgroup label="{{lng_menu.clients}}">
				    		{%for c in clients%}
				    			<option {%if selClient == c.client_id%}selected{%endif%} value="{{c.client_id}}@cli">{{c.client_name}}</option>
				    		{%endfor%}
				    		</optgroup>
				    		{%endif%}
				    		{%if clientcollabs%}
				    		<optgroup label="{{lng_menu.colleague}}">
				    		{%for c in clientcollabs%}
				    			<option {%if selCollab == c.cb_id%}selected{%endif%} value="{{c.cb_id}}@usr">{{c.cb_billing_name}}</option>
				    		{%endfor%}
				    		</optgroup>
				    		{%endif%}
				    		
				    	</select>
			    	
			    	
		    		</div>
		    		<div class="hidden-xs col-sm-1" style="padding-left: 0;">
			    		
			    		<button type="button" class="btn btn-sm btn-success" id="a-c" style="padding: 6px 9px;"><i class="material-icons">person_add</i></button>
		    		</div>
			    </div>                   
	
	        </div>
		</div>
		
		<div class="panel panel-default panel-borders" style="margin-bottom:5px;">
	        <div class="panel-heading">
		        <span class="title">{{lng_projects.title_milestones}}</span>
		    </div>
	        <div class="panel-body">
				<div id="milestones-box">
				</div>
				<div class="text-center">
				<button id="addMilestone" type="button" class="btn btn-success">{{lng_projects.btn_add_ms}}</button>
				</div>
	        </div>
		</div>
		
		
		<div class="panel panel-default panel-borders" style="margin-bottom:5px;" >
	        <div class="panel-heading">
		        <span class="title">{{lng_projects.title_collaborators}}</span>
		    </div>
	        <div class="panel-body">
		        {%if collabs%}
		        <div id="collab-list">
			        
		        </div>    
				<div class="text-center" style="margin-top: 6px;">
					<button class="btn btn-success btn-add-line">{{lng_projects.add_collab}}</button>
				</div>	
				{%else%}
				<div class="text-center">{{lng_common.norecords}} <a href="/users/add">{{lng_common.title_add_user}}</a></div>
				{%endif%}
			
		        
	        </div>
		</div>    
	
		<button type="submit" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
		<a href="/projects" class="btn btn-default btn-lg pull-left">{{lng_common.btn_cancel}}</a>
		<div style="clear:both;margin-bottom: 20px;"></div>	
			
		</form>
	</div>
</div>		
</div>

     <div id="share-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_common.new_customer}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12">

				  <form id="fnewcustomerpopup" class="form-horizontal" role="form">
						
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">{{lng_clients.tbl_name}}<span style="color: red;">*</span></label>
	                  <div class="col-sm-9">
	                    <input class="form-control" name="client_name" data-errorbox="#errname-client" data-msg-required="{{lng_clients.name_req}}"  required=""  type="text">
	                    <div id="errname-client" style="color:red;"></div>
	                  </div>
	                </div>
	
					
						<input type="hidden" value="1" name="status">
						<input type="hidden"  name="discount" id="discount" value="0"> 
						<input type="hidden"  name="notes" value="">

					<div class="form-group" >
					<label class="col-sm-3 control-label no-padding-right ">Email</label>
					<div class="col-sm-6">
						<input type="email"  name="client_email" data-errorbox="#errname-email" data-msg-email="{{lng_common.nve}}" class="form-control"> 									<div id="errname-email" style="color:red;"></div>
					</div>
					</div>
						

					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.country}}</label>
					 <div class="col-sm-5">
						<select id="country" class="form-control" name="client_country" >
							{%for cl in countryList%}
							<option {%if cl.country_iso==acs.user_country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
							{%endfor%}
						</select>
					</div>
					</div>
					
					
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.state}}</label>
					<div class="col-sm-5">
					 {%if  statesList%}
						<select title="State"  id="region"  name="client_state" class="form-control">
							{%for sl in statesList%}
							<option {%if sl.state_code == acs.co_state%}selected{%endif%} value="{{sl.state_code}}">{{sl.state_name}}</option>
							{%endfor%}
						</select>
						<input type="text" style="display:none;" id="inputstate" title="State" class="form-control"  name=""  />
					{%else%}
						<input type="text" id="inputstate" title="State" class="form-control"  name="client_state"  value="{{acs.co_state}}">
						<select title="State"  style="display:none;"  id="region"  name="client_state" class="form-control">
						</select>
					{%endif%}	
					</div>
					</div>
				
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{lng_common.address}} </label>
					    <div class="col-sm-9">
						    <input type="text" name="client_street"  class="form-control" >
						</div>
					</div>

					
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.city}} </label>
					<div class="col-sm-5">
	
						<input type="text" name="client_city"   class="form-control" >
					</div>
					</div>
						
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.zip}} </label>
					<div class="col-sm-3">
						<input type="text"  class="form-control" name="client_zip"  > 						
					</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right ">{{lng_common.phone}}</label>
					<div class="col-sm-4">
	
						<input type="text"  name="client_phone" class="form-control " > 
					</div>
					</div>
		
		</form>


		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" id="send-invite"   class="btn btn-primary doAddClient btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  
{%endblock%}
{%block js%}
    <script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
   <script src="/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
   <script src="/js/dev/projects_add.js"></script>
{%endblock%}

