<table class="table">


{%for c in collabs%}
<tr>
	<td>{{c.user_username}} ({{c.user_name}})</td>
	<td><span class="text-danger">{{c.open}}</span> / <span class="text-success">{{c.close}}</span></td>
	<td align="right">
		{%if c.prju_role == 1%}{{lng_projects.role_owner}}{%endif%}
		{%if c.prju_role == 10%}{{lng_projects.role_manager}}{%endif%}
		{%if c.prju_role == 20%}{{lng_projects.role_collab}}{%endif%}
		
	</td>
</tr>
{%endfor%}
</table>
	
