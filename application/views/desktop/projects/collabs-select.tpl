<select class="form-control" name="issue_assign">
	
	{%if defprj%}
	<option selected value="{{acs.user_id}}">Me ({{ acs.user_name}})</option>

	{%endif%}
	
	{%for c in collabs%}
	<option {%if c.user_id==acs.user_id%}selected{%endif%} value="{{c.user_id}}">{%if c.user_id==acs.user_id%}Me {%else%}{{c.user_username}} {%endif%}({{c.user_name}})</option>
	{%endfor%}
	
</select>    
