{% extends skin~"/common/root.tpl" %}
{%block headcss%}
    
{%endblock%}

{%block content%}
<input type="hidden" id="_pdt" value="{{lng_projects.del_bprj_title}}">
<input type="hidden" id="_pdm" value="{{lng_projects.del_bprj_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_projects.title}}
		<a href="/projects/add" class="btn btn-head btn-success pull-right" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_projects.btn_add}}" >{{lng_projects.btn_add}}</a>

		
	</h3>
</div>	


<div class="main-content">
<div class="row">
	<div class="col-xs-12">
		<form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<div class="panel panel-default panel-borders">
	       {# <div class="panel-heading">
		        <div class="tools">
			        <a href="/projects/add" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_projects.btn_add}}" style="padding-top:1px; padding-bottom: 1px;"><i class="material-icons">add</i><i class="material-icons">extension</i></a>
			    </div>
		        <span class="title">{{lng_projects.title}}</span>
		    </div>#}
	        <div class="panel-body">
			
					<div id="d-content">
					
					</div>
			   
	
	        </div>
		</div>
		</form>
	</div>
</div>		
</div>

{%endblock%}

{%block footer%}
<div id="megafooter">
	
</div>	
{%endblock%}

{%block js%}

   <script src="/js/dev/projects.js"></script>
{%endblock%}
