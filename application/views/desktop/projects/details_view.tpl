<table id="prj-details-tbl" class="table">
    <tr>
        <td width="20%" ><small class="text-muted">{{lng_projects.tbl_name}}</small></td>
        <td id="tbl-prj-name">{{prj.project_name}}</td>
    </tr>

    <tr>
        <td><small class="text-muted">{{lng_projects.client}}</small></td>
        <td >

	    	{%if prj.project_client_id==0 and prj.project_collab_id==0 %}
			<span>{{lng_common.noassign}}</span>
			{%else%}
	        	{%if prj.project_client_id%}
	        		{{prj.client_name}}
	        	{%else%}
	        		{{prj.user_name}}
	        	{%endif%}
	        {%endif%}
	    </td>
    </tr>


    <tr>
        <td><small class="text-muted">{{lng_projects.tbl_status}}</small></td>
        <td class="text-{{prj.prjstatus_color}}" id="tbl-prj-status">{{prj.prjstatus_name}}</td>
    </tr>


	<tr>
		<td colspan="2">
        {%if prj.project_desc%}
		{{prj.project_desc|nl2br}}
		{%else%}
			<div class="text-center text-danger">{{lng_projects.no_desc}}</div>
		{%endif%}
		</td>
		
	</tr>	
</table>   