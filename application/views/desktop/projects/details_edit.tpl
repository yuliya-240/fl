<form id="fdata" class="form-horizontal">
	<input type="hidden" name="id" value="{{prj.project_id}}">
<div class="form-group">
  <label class="col-sm-3 control-label">{{lng_projects.name}}<span style="color: red;">*</span></label>
  <div class="col-sm-9">
    <input class="form-control" name="project_name" value="{{prj.project_name}}" data-errorbox="#errname" data-msg-required="{{lng_projects.name_req}}"  required="" placeholder="{{lng_projects.ph_name}}" type="text">
    <div id="errname" style="color:red;"></div>
  </div>
</div>

<div class="form-group">
	<label class="col-sm-3 control-label">{{lng_common.client}}</label>
	<div class="col-sm-8" style="padding-right: 4px;">

    	<select id="client" name="client" class="select2">
	    	
    		<option value="0">{{lng_projects.not_assigned}}</option>
    		{%if clients%}
    		<optgroup label="{{lng_menu.clients}}">
    		{%for c in clients%}
    			<option {%if selClient == c.client_id%}selected{%endif%} value="{{c.client_id}}@cli">{{c.client_name}}</option>
    		{%endfor%}
    		</optgroup>
    		{%endif%}
    		{%if clientcollabs%}
    		<optgroup label="{{lng_menu.colleague}}">
    		{%for c in clientcollabs%}
    			<option {%if selCollab == c.cb_id%}selected{%endif%} value="{{c.cb_id}}@usr">{{c.cb_billing_name}}</option>
    		{%endfor%}
    		</optgroup>
    		{%endif%}
    		
    	</select>
	
	
	</div>
	{#<div class="hidden-xs col-sm-1" style="padding-left: 0;">
		
		<button type="button" class="btn btn-sm btn-success" id="a-c" style="padding: 6px 9px;"><i class="material-icons">person_add</i></button>
	</div>#}
</div>                   

<div class="form-group">
  <label class="col-sm-3 control-label">{{lng_projects.tbl_status}}</label>
  <div class="col-sm-5">
    <select class="form-control" name="project_status">
        {%for s in statuses%}
        	<option {%if s.prjstatus_status==prj.project_status%}selected{%endif%} value="{{s.prjstatus_status}}">{{s.prjstatus_name}}</option>
        {%endfor%}
    </select>    
    
  </div>
</div>

<div class="form-group">
  <label class="col-sm-3 control-label">{{lng_projects.desc}}</label>
  <div class="col-sm-9">
    <textarea name="project_desc" class="form-control">{{prj.project_desc}}</textarea>
  </div>
</div>

</form>	
<hr>
<a href="#" id="btn-cancel-details" style="margin-bottom: 5px;" class="btn btn-lg btn-space btn-default">{{lng_common.btn_cancel}}</a>
<button id="btn-save-details" type="button" style="margin-bottom: 5px;" class="btn btn-lg btn-space pull-right btn-primary">{{lng_common.btn_save}}</button>
