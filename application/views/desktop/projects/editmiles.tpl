<form id="fmiles" class="form-horizontal">
<div id="milestones-box">


<div class="form-group" id="0" >
	<div class="col-sm-7">
		<label class="col-xs-12" style="padding-left:0px;">{{lng_projects.ml_name}}<span style="color: red;">*</span></label>
	</div>
	<div class="col-sm-5">
		 <label class="col-xs-12" style="padding-left:0px;">{{lng_projects.deadline}}<span style="color: red;">*</span></label>
	</div>

	
</div>	
<hr style="margin-top: 0;">
{%for m in milestones%}
<div class="form-group" id="{{m.prjm_id}}" >
  
	<div class="col-sm-7">
	    
	    <input class="form-control" name="milestone[{{m.prjm_id}}]" data-errorbox="#err{{m.prjm_id}}" data-msg-required="{{lng_projects.name_req}}"  required="" placeholder="{{lng_projects.ml_name}}" value="{{m.prjm_name}}" type="text">
	    <div id="err{{m.prjm_id}}" style="color:red;"></div>
	</div>
	
	<div class="col-sm-5">
	  
	<div class="input-group pull-left">
		<span class="input-group-addon">
			<i class="fa fa-calendar bigger-110"></i>
		</span>
		<input type="text" data-date-format="{{dpformat}}" readonly="" value="{{m.prjm_deadline|date(acs.user_dformat)}}" name="deadline[{{m.prjm_id}}]" class="form-control date-picker">
		
	</div>
		<button class="btn pull-left btn-space btn-default text-danger del-mil" type="button" data-id="{{m.prjm_id}}" style="padding-top: 7px !important; border: none !important; position: absolute; background: none !important;  padding-left: 6px;" ><span class="s7-close-circle" style="font-size:28px; font-weight: 700;"></span></button>		



	</div>
	
</div>

{%endfor%}


</div>
</form>	
<div class="text-center">
<button id="addMilestone" class="btn btn-success">{{lng_projects.btn_add_ms}}</button>
</div>
<hr>
<button type="submit" id="doSaveMilestone" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
<a href="#" id="cancelEditMilestone" class="btn btn-default btn-lg pull-left cancel-ed" >{{lng_common.btn_cancel}}</a>
