{% extends skin~"/common/root.tpl" %}

{%block preheadcss%}
     <link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.min.css"/>
{%endblock%}


{%block headcss%}
<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
<link rel="stylesheet" href="/css/growl.css">
{%endblock%}
 
 {%block content%}
<input type="hidden" id="_pdt" value="{{lng_projects.del_prj_title}}">
<input type="hidden" id="_pdm" value="{{lng_projects.del_prj_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="prj_id" value="{{prj.project_id}}">

<div class="page-head">
  <h3 style="margin-top:5px;">{{prj.project_name}}  
	  {%if prj.	project_user_id == acs.user_id%}
      <a href="#" data-id="{{prj.project_id}}" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.btn_del}}" class="btn btn-head btn-space btn-danger pull-right delprj">{{lng_common.btn_del}}</a>
  {%endif%}
      <a href="/issues/add/?prj={{prj.project_id}}" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_projects.cr_issue}}" class="btn btn-head btn-space btn-success pull-right">{{lng_projects.cr_issue}}</a>

     
      <a href="/projects" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.back_list}}"  class="btn btn-dark btn-head btn-space pull-right"><i class="material-icons btn-head-s">arrow_back</i></a>
      
      
  </h3>
</div>

 <div class="main-content" style="margin-bottom: 50px;">
	 <input type="hidden" id="prj_id" value="{{prj.project_id}}">
	 <div class="row">
		 <div class="col-md-5 col-sm-12">
			<div class="panel panel-default panel-borders" style="margin-bottom:5px;">
	        <div class="panel-heading">
		         {%if role<20%}
		        <div class="tools" ><a  href="#" id="btn-edit-details" data-id="{{prj.project_id}}" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.btn_edit}}"><span class="icon s7-note" ></span></a></div>
		        {%endif%}
		        <span class="title">{{lng_projects.title_details}}</span>
		    </div>
	        <div class="panel-body" >
		       
		        <div id="details-view"></div> 
				<div id="details-edit" style="display: none;"></div>
		        
	        </div>
			</div>    

			<div class="panel panel-default panel-borders" id="m-block" style="margin-bottom:5px;">
	        <div class="panel-heading">
		        {%if role<20%}
		        <div class="tools" >
			        <a href="#" data-toggle="tooltip" id="miles-edit-btn"  data-placement="bottom" data-original-title="{{lng_common.btn_edit}}">
				        <span class="icon s7-note" ></span>
				    </a>
				</div>
		        {%endif%}
		        <span class="title">{{lng_projects.tbl_miles}}</span>
		    </div>
	        <div class="panel-body">
				<div id="miles-view-box" class="table-responsive view-box">
					{% include skin~"/projects/viewmiles.tpl" %}
				</div>
				
				<div id="miles-edit-box" class="edit-box">
					
				</div>
				
	        </div>
			</div>    
			 
		 </div>
		 <div class="col-md-7 col-sm-12">
			<div class="row"> 
              <div class="col-md-7">
              	<div class="widget widget-pie " style="padding-bottom: 60px;">
                <div class="widget-head"><span class="title">{{lng_projects.issue_status}}</span></div>
                 <div class="row chart-container">
                  <div class="col-xs-6">
                    <div id="issue-chart" class="chart"></div>
                  </div>
                  <div class="col-xs-6">
                    <div class="legend"></div>
                  </div>
                </div>
               
              </div>	
              </div>
              <div class="col-md-5">
               <div class="widget widget-tile g-l box-go" data-go="/issues/?prj={{prj.project_id}}&status=10" style="margin-bottom: 4px; padding-top: 7px; padding-bottom: 7px;">
	              <div class="data-info">
	                <div data-toggle="counter" data-end="156" class="value">{{issue_opens}} <span class="text-muted" style="font-size: smaller;">({{issue_opens_percent}}%)</span></div>
	                <div class="desc" style="color: red;">{{lng_projects.open_tasks}}</div>
	              </div>
	              <div class="icon" style="color: red;"><span class="s7-config"></span></div>
	            </div>
	            
               <div class="widget widget-tile g-l box-go" data-go="/issues/?prj={{prj.project_id}}&status=50"  style="margin-bottom: 4px; padding-top: 7px; padding-bottom: 7px;">
	              <div class="data-info">
	                <div data-toggle="counter" data-end="156" class="value">{{issue_closed}} <span class="text-muted" style="font-size: smaller;">({{issue_closed_percent}}%)</span> </div>
	                <div class="desc text-success">{{lng_projects.closed_tasks}}</div>
	              </div>
	              <div class="icon "  ><span class="s7-check text-success"></span></div>
	            </div>
               
               
               <div class="widget widget-tile g-l box-go" data-go="/issues/?prj={{prj.project_id}}" style="margin-bottom: 4px; padding-top: 7px; padding-bottom: 7px;">
	              <div class="data-info">
	                <div data-toggle="counter" data-end="156" class="value">{{issue_other}} <span class="text-muted" style="font-size: smaller;">({{issue_other_percent}}%)</span></div>
	                <div class="desc ">{{lng_projects.other_tasks}}</div>
	              </div>
	              <div class="icon "><span class="s7-plugin "></span></div>
	            </div>
               <div class="text-center">
	               <a class="g-l" data-go="/issues/?prj={{prj.project_id}}" href="#">{{lng_projects.vt}}</a>
               </div>    
              </div>    

			</div>  
			
			<div class="panel panel-default panel-borders" style="margin-bottom:5px;">
	        <div class="panel-heading">
		        {%if role<20%}
		        <div class="tools"><a href="#" data-id="{{prj.project_id}}" id="btn-collab-edit" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.btn_edit}}"><span class="icon s7-note" ></span></a></div>
		        {%endif%}
		        <span class="title">{{lng_projects.tbl_collabs }}</span>
		    </div>
	        <div class="panel-body">
				<div id="collab-view"></div>
				<div id="collab-edit" style="display: none;"></div>
	        </div>
			</div> 
			   		 
		 </div>	
	 </div>	 
 </div>	 
 
 {%endblock%}
{%block js%}
<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>	
    <script src="/assets/lib/bootstrap-growl/bootstrap-growl.min.js"></script>
    <script src="/assets/lib/jquery-flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/assets/lib/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/assets/lib/jquery-flot/jquery.flot.resize.js" type="text/javascript"></script>    
        <script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
   <script src="/js/dev/projects_view.js?v={{hashver}}"></script>
{%endblock%}
