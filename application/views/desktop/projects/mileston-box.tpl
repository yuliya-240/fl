<div class="form-group" id="{{uniq}}">
  
    <div class="col-sm-8" style="margin-bottom:5px;" >
	    <label class="col-xs-12" style="padding-left:0px;">{{lng_projects.ml_name}}<span style="color: red;">*</span></label>
	    <input class="form-control" name="milestone[{{uniq}}]" data-errorbox="#err{{uniq}}" data-msg-required="{{lng_projects.name_req}}"  required="" placeholder="{{lng_projects.ml_name}}" type="text">
	    <div id="err{{uniq}}" style="color:red;"></div>
    </div>
    <div class="col-sm-3" style="margin-bottom:5px;">
	    <label class="col-xs-12" style="padding-left:0px;">{{lng_projects.deadline}}</label>
	    <input class="form-control datetimepicker" data-min-view="2" readonly="" value="{{today}}"   name="deadline[{{uniq}}]" type="text">
    </div>
	<div class="col-sm-1" style="margin-bottom:5px;">
		<label class="col-xs-12 hidden-xs" >&nbsp;</label>
		<a href="#" data-id="{{uniq}}"  class="btn btn-link hidden-xs del-mil" style="color:red; " ><i class="material-icons">close</i></a>
		<button data-id="{{uniq}}"  class="btn btn-block hidden-sm hidden-md hidden-lg  btn-danger del-mil "  ><i class="material-icons">close</i></button>
	</div>	

</div>
