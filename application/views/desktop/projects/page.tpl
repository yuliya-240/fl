{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs">

			</th>
			<th>{{lng_projects.tbl_status}}</th>
			<th>{{lng_projects.tbl_name}}</th>
			<th>{{lng_projects.tbl_desc}}</th>
			<th>{{lng_projects.tbl_creator}}</th>
			<th width="1%"></th>
		</tr>
	</thead>
	<tbody>
		{%for r in records%}
		<tr>
			<td class="text-center hidden-xs" style="vertical-align:middle;">
	           {%if r.project_user_id==acs.user_id%}
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.project_id}}" class="check" name="row[]" value="{{r.project_id}}">
	              <label for="chk{{r.project_id}}"></label>
	            </div>
	            {%endif%}
			</td>
			<td class="tbl-pointer text-{{r.prjstatus_color}}" data-id="{{r.project_id}}" >{{r.prjstatus_name}}</td>
			<td class="tbl-pointer" data-id="{{r.project_id}}">{{r.project_name}}</td>
			<td class="tbl-pointer" data-id="{{r.project_id}}">{{r.project_desc}}</td>
			<td class="tbl-pointer" data-id="{{r.project_id}}">{{r.user_username}}</td>
			<td class="tbl-pointer" data-id="{{r.project_id}}"><a href="#" class="del" data-id="{{r.project_id}}"><i class="material-icons">visibility</i></a></td>
		</tr>
		{%endfor%}
	</tbody>
</table>				
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
