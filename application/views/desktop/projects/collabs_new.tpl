<div id="collab-list-item{{id}}" class="collab-list-item bg" style="border-bottom: 1px dashed #ccc;">
	<div class="form-group">
		<div class="col-sm-8">
			<select class="form-control collab-item-sel" data-id="{{id}}" id="prju_collaborator_id{{id}}" name="prju_collaborator_id[]">
				<option value="0">{{lng_projects.btn_add_ms}}</option>
				{%for c in collabs%}
				<option value="{{c.user_id}}" >{{c.user_name}} ({{c.user_username}})</option>
				{%endfor%}
			</select>	
			
			<h4 id="collab-name-text{{id}}" style="display: none;"></h4>
		
			
		</div>	
		<div class="col-sm-3">
			<select class="form-control" name="prju_role[]">
				<option value="20">{{lng_projects.role_collab}}</option>
				<option value="10">{{lng_projects.role_manager}}</option>
			</select>	
		
		</div>
		<div class="col-sm-1">
			<a href="#" data-id="{{id}}"  class="btn btn-link hidden-xs del-collab-line" style="color:red; " ><i class="material-icons">close</i></a>
		</div>
	</div>
</div>	