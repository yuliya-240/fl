{%if milestones%}
<table class="table">
	{%for m in milestones%}
	<tr>
		<td width="60%">{{m.prjm_name}}</td>
		<td align="right">{{m.prjm_deadline|date(acs.user_dformat)}}</td>
	</tr>		
	{%endfor%}
</table>	
{%else%}
<div class="text-center text-danger">{{lng_projects.miles_not_def}}</div>
{%endif%}
