<form id="fdata" class="form-horizontal">
	<input type="hidden" name="id" value="{{prj.project_id}}">
{%if collabs%}
<div id="collab-list">
    {%for c in prjcollabs%}
    	{%if c.prju_collaborator_id != c.	prju_user_id%}
		<div id="collab-list-item{{c.prju_id}}" class="collab-list-item bg" style="border-bottom: 1px dashed #ccc;">
			<div class="form-group">
				<div class="col-sm-8">
					<input type="hidden"  name="prju_collaborator_id[]" value="{{c.prju_collaborator_id}}">
					
					<h4 id="collab-name-text{{c.prju_id}}" >{{c.user_name}} ({{c.user_username}})</h4>
				
					
				</div>	
				<div class="col-sm-3">
					
					<select class="form-control" name="prju_role[]">
						<option {%if c.prju_role==20%}selected{%endif%} value="20">Collaborator</option>
						<option {%if c.prju_role==10%}selected{%endif%} value="10">Manager</option>
					</select>	
				
				</div>
				<div class="col-sm-1">
					<a href="#" data-id="{{c.prju_id}}"  class="btn btn-link hidden-xs del-collab-line" style="color:red; " ><i class="material-icons">close</i></a>
				</div>
			</div>
		</div>	
		{%endif%}    
    {%endfor%}
</div>    
<div class="text-center" style="margin-top: 6px;">
	<button class="btn btn-success btn-add-line">Add Collaborator</button>
</div>	
{%else%}
<div class="text-center">{{lng_common.norecords}} <a href="/users/add">{{lng_common.title_add_user}}</a></div>
{%endif%}
</form>
<hr>
<a href="#" id="btn-cancel-collab" style="margin-bottom: 5px;" class="btn btn-lg btn-space btn-default">{{lng_common.btn_cancel}}</a>
<button id="btn-save-collab" type="button" style="margin-bottom: 5px;" class="btn btn-lg btn-space pull-right btn-primary">{{lng_common.btn_save}}</button>

