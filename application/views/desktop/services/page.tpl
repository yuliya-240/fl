{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs"></th>
			
			<th>{{lng_services.name}}</th>
			<th>{{lng_services.desc}}</th>
			<th>{{lng_services.cost}}</th>
			<th>{{lng_services.taxes}}</th>
			<th width="1%"></th>
		</tr>
	</thead>
	<tbody>
		{%for r in records%}
		<tr>
			<td class="text-center hidden-xs" style="vertical-align:middle;">
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.srv_id}}" class="check" name="row[]" value="{{r.srv_id}}">
	              <label for="chk{{r.srv_id}}"></label>
	            </div>
			</td>
		
			<td class="tbl-pointer g-l" data-id="{{r.srv_id}}" data-go="/services/edit/{{r.srv_id}}">{{r.srv_name}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.srv_id}}" data-go="/services/edit/{{r.srv_id}}">{{r.srv_desc|truncate(100)}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.srv_id}}" data-go="/services/edit/{{r.srv_id}}">{{r.srv_cost|number_format(2,acs.co_dsep,acs.co_tsep)}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.srv_id}}" data-go="/services/edit/{{r.srv_id}}">
				{{r.srv_tax1|number_format(2,acs.co_dsep,acs.co_tsep)}}<br>
				{{r.srv_tax2|number_format(2,acs.co_dsep,acs.co_tsep)}}
				
			</td>
			<td class="tbl-pointer" data-id="{{r.srv_id}}">
				<a href="#" class="g-l" data-go="/services/edit/{{r.srv_id}}" data-id="{{r.srv_id}}"><i class="material-icons">create</i></a>
			</td>
		</tr>
		{%endfor%}
	</tbody>
</table>				
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
