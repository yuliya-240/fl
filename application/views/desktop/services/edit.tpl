{% extends skin~"/common/root.tpl" %}
{%block headcss%}

{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_services.title_edit}}</h3>
</div>	

<div class="main-content">
<div class="row">
	<div class="col-xs-12 col-md-8 col-md-offset-2">
		<form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
			<input type="hidden" name="srv_id" value="{{srv.srv_id}}">
		<div class="panel panel-default panel-borders">

	        <div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_services.name}} <span style="color: red;">*</span></label>
				    <div class="col-sm-9">
                          <input class="form-control" data-errorbox="#errname" value="{{srv.srv_name}}" data-msg-required="{{lng_services.name_req}}"  required="" name="srv_name" type="text">
                          <div id="errname" style="color:red;"></div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_services.desc}} </label>
				    <div class="col-sm-9">
					
                        <textarea  name="srv_desc" class="form-control" rows=3>{{srv.srv_desc}}</textarea>
                        
					</div>
				</div>


				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_services.cost}} </label>
				    <div class="col-sm-4">
					
                          <input class="form-control price" name="srv_cost" value="{{srv.srv_cost}}" type="text">
                        
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_services.tax1}} </label>
				    <div class="col-sm-3">
					
					<div class="input-group xs-mb-15"><span class="input-group-addon">%</span>
                          <input  class="form-control price" value="{{srv.srv_tax1}}" name="srv_tax1" type="text">
                        </div>                        
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_services.tax2}} </label>
				    <div class="col-sm-3">
					
					<div class="input-group xs-mb-15"><span class="input-group-addon">%</span>
                          <input  class="form-control price" name="srv_tax2" value="{{srv.srv_tax2}}" type="text">
                        </div>                        
					</div>
				</div>

	
	        </div>
		</div>
		<button type="submit" id="doSave" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
		<a href="#" class="btn btn-default btn-lg pull-left g-l" data-go="/services">{{lng_common.btn_cancel}}</a>
		<div style="clear:both;margin-bottom: 40px;"></div>	
		
		</form>
		
	</div>
</div>
</div>	

{%endblock%}

{%block js%}
	<script src="/assets/lib/mask/jquery.mask.js"></script>
    <script src="/js/dev/services_edit.js?v={{hashver}}"></script>
{%endblock%}
