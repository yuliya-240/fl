{% extends skin~"/common/root.tpl" %}
{%block headcss%}

{%endblock%}

{%block content%}

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_services.title}} 
		 <a href="#" class="btn btn-head btn-success pull-right g-l" data-go="/services/add" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_services.btn_add}}">{{lng_services.btn_add}}</a>
	</h3>
</div>	

<div class="main-content">
<div class="row">
	<div class="col-xs-12">
		<input type="hidden" id="_bdt" value="{{lng_services.bdtitle}}">
		<input type="hidden" id="_bdm" value="{{lng_services.bdmsg}}">
		<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
		
		<form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<div class="panel panel-default panel-borders">

	        <div class="panel-body">
			
				<div id="d-content">
		
				</div>
	
	        </div>
		</div>
		</form>
		
	</div>
</div>	
</div>	


{%endblock%}

{%block footer%}
<div id="megafooter">
	
</div>	
{%endblock%}

{%block js%}
   <script src="/js/dev/services.js?v={{hashver}}"></script>
{%endblock%}
