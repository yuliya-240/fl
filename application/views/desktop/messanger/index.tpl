<div class="head-chat-w">
	<button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
	
	<div id="chat-w-h">
	   <div class="user">
	      <a href="#"  style="padding: 3px 0 0 0 !important; cursor: default;">
	        <img id="chat-head-user-image" id="head-usr-pic" src="/img/floctoid.jpg">
			<div class="user-data">
			  
	          <span class="name" id="chat-head-usr-name" style="color:#000;">Floctoid</span>
	          <span class="message text-muted" id="chat-head-usr-title" >Online</span>
	        </div>
	      </a>
	  </div>
	</div>   
           
</div>

<div class="aside-chat-toolbar">
{#<button type="button" class="btn-chat-serach"><i class="icon s7-search"></i></button>#}
<button type="button" class="btn-sound-off btn-chat-serach pull-left" style="margin-left: 20px;"><i class="icon s7-volume"></i> </button>
<button type="button" class="btn-sound-on btn-chat-serach pull-left" style="margin-left: 20px; display: none;"><i class="icon s7-mute"></i></button>

{#<div class="btn-group btn-hspace">
  <button type="button"  class="btn btn-default chat-profile-btn" >
  <img class="img-circle" style="width: 40px; height: 40px;" src="/userfiles/{{acs.user_id}}/pic/{{acs.user_pic}}">
   <span class="angle-down s7-angle-down" ></span></button>

</div>#}
</div>	

<div class="aside-chat ">

    <div class="chat-contacts">
                  
              	
			  		<div class="chat-contact-list " id="chat-contact-lis">
			  	
                      <div class="user  ch-usr-item" id="floctoid" >
                          <a href="#" class="ch-usr" data-ch="floctoid" data-title="ChatBot" id="floctoidlnk" data-pic="/img/floctoid.jpg" data-name="Floctoid" >
	                        <img src="/img/floctoid.jpg">
							<div class="user-data">
							   <span  id="unreadfloctoid"  class="status badge badge-danger">{%if floctoidunrcont%}{{loctoidunrcont}}{%endif%}</span>
                              <span class="name">Floctoid</span>
                              <span class="message " style="color: green;">ChatBot</span>
                            </div>
                          </a>
                      </div>
			  		
			  		{%if users%}
	                    {%for u in users%}
                      <div class="user ch-usr-item" id="{{u.user_pusher_channel}}">
                          <a href="#" class="ch-usr" id="{{u.user_pusher_channel}}lnk" data-ch="{{u.user_pusher_channel}}" data-pic="{%if u.user_pic=="nopic.png"%}/img/avatar/avatar2x200.jpg{%else%}/userfiles/{{u.user_id}}/pic/{{u.user_pic}}{%endif%}" data-name="{{u.user_name}}" data-title="{{u.user_username}}" data-status="online">
	                        <img {%if u.user_pic=="nopic.png"%}src="/img/avatar/avatar2x200.jpg"{%else%}src="/userfiles/{{u.user_id}}/pic/{{u.user_pic}}"{%endif%}>
							<div class="user-data">
							<span  id="unread{{u.user_pusher_channel}}"  class="status badge badge-danger">{%if u.unrcont%}{{u.unrcont}}{%endif%}</span>
							 <input type="hidden" id="unreadval{{u.user_pusher_channel}}" value="{{u.unrcont}}">
                              <span class="name">{{u.user_name|truncate(20)}}</span>
                              <span class="message text-muted" >{{u.user_username|truncate(20)}}</span>
                            </div>
                          </a>
                      </div>
                      {%endfor%}
                    
                    {%endif%}
                    
			  		{%if clients%}
               
	                 {%for c in clients%}
                      <div class="user ch-usr-item" id="{{c.client_pusher_channel}}">
                          <a href="#" class="ch-usr" id="{{c.client_pusher_channel}}lnk" data-title="{{lng_common.client}}" data-ch="{{c.client_pusher_channel}}" data-pic="/img/clientava.jpg" data-name="{{c.client_name}}" >
	                        <img src="/img/clientava.jpg">
							<div class="user-data">
							  <span  id="unread{{c.client_pusher_channel}}"  class="status badge badge-danger ">{%if c.unrcont%}{{c.unrcont}}{%endif%}</span>
							  <input type="hidden" id="unreadval{{c.client_pusher_channel}}" value="{{c.unrcont}}">
                              <span class="name">{{c.client_name|truncate(20)}}</span>
                              <span class="message text-muted" >{{lng_common.client}}</span>
                            </div>
                          </a>
                      </div>
                     {%endfor%}
                   
                    
                    {%endif%}
                    
			  		</div>
                  
              
                
              </div>
    <div style="clear:both; margin-top: 100px;">&nbsp;</div>
</div> 

<div class="chat-main-box" id="chat-main-box" style="background: transparent;">
	<div style="width: 100%">
				<div class="chat-messages">

                          <ul id="chat-flow">
                          </ul>
                        </div>
                    </div>
                    <div style="clear:both; margin-top: 30px;">&nbsp;</div>
                  
</div>

<div class="chat-input-box">

	<form class="form-horizontal">
		
		<div class="col-sm-1 " id="btn-cha-attach" style="border-right: 1px solid #dedede; text-align: center;  padding-bottom:25px; padding-top: 15px; background: #f8f8f8; ">
			<button type="button" aria-hidden="true" id="chAttach" class="btn-chat-serach" style="margin-right: 7px;"><i class="icon s7-paperclip"></i></button>
		</div>	
			
		<div class="col-sm-10 ch-in-wrapper" id="ch-in-b" style="padding-right: 0;">
		<textarea rows="2" class="form-control ch-in" name="m" id="chat-m" style="padding-right: 0px;" onkeypress="onTestChange();" placeholder="Your message here...."></textarea>
		</div>
		<div class="col-sm-1 " id="btn-chat-s" style="text-align: center; border-left: 1px solid #dedede; padding-bottom:20px; padding-left:11px; background: #f8f8f8;">
			<button type="button" class="btn btn-primary" id style="border-radius: 50%; margin-top: 12px; padding-top: 11px;"><i class="material-icons">send</i></button>
		</div>	
	</form>	
	
</div>

<input type="hidden" id="_pu-ch" value="{{currchannel}}">

