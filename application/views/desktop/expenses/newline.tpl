<tr id="line{{id}}">
	<td class="td-a-t" width="80%">
		<input type="text" name="el_desc[]"  id="eldescc{{id}}" data-id="{{id}}" data-errorbox="#errsrvline{{id}}" data-msg-required="{{lng_common.fr}}"  class="form-control  " required>
		<div id="errsrvline{{id}}" style="color:red;"></div>
	</td>
	<td class="td-a-t" >
		<input type="text" value="0.00" name="el_amount[]" id="elamount{{id}}" data-id="{{id}}" class="form-control dec pull-left"  >
		<button class="btn btn-space btn-default text-danger delline" type="button" data-id="{{id}}" style="padding-top: 7px !important; border: none !important; position: absolute; background: none !important;  padding-left: 6px;" ><span class="s7-close-circle" style="font-size:28px; font-weight: 700;"></span></button>

	</td>
</tr>
