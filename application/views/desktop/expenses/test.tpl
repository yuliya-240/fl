<div id="li{{ id }}">
    <div class="col-xs-1" style="padding-right: 0; padding-top: 10px;"><span class="s7-angle-right" style="font-size: 25px;"></span></div>
    <div class="col-xs-11" style="padding-left: 0; padding-top: 3px;">
        <a class="list-group-item " data-id="{{ id }}" >{{id.expcat_name}}
            <span class="pull-right s7-trash" id="delSubCat" data-id="{{ id }}" style="font-size: 20px; color: red;"></span>
            <span class="pull-right s7-note" id="editCat" data-id="{{ id }}" style="font-size: 20px; color: #0000ff;"></span>
        </a>
    </div>
</div>