<ul class="nav nav-tabs">
    <li class="{{topExpenses}}"><a href="/expenses">{{lng_expenses.title}}</a></li>
    <li class="{{topCategory}}"><a href="/expenses/categories">{{lng_expenses.category}}</a></li>
</ul>
