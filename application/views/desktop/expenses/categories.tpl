{% extends skin~"/common/root.tpl" %}
{% block headcss %}

{% endblock %}
{% block content %}

    <div class="am-content" style="margin-left: 0;">
        <div class="page-head">

            <h2>{{lng_expenses.ex_cat}}
	            <a href="#" class="btn btn-success btn-head pull-right btn-n-c"  data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_expenses.btn_add}}">{{lng_expenses.btn_add}}</a>
	            
            </h2>
            </div>
        <div class="main-content">
            <!--Tabs-->
    
            <div class="row">
                <div class="col-sm-12">

                          
                               
                            
            <div id="d-content" style="margin-top:20px;">
	            
	            
            </div>
               
                </div>
            </div>

 
        </div>
    </div>

     <div id="new-cat-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_expenses.btn_add}}</h3>
          </div>

         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12">
					<form id="fdata" class="form-horizontal" role="form">
                        <input type="hidden" name="issub" id="issub" value="0">
                        <div class="form-group" id="catlist-box" style="display:none;">
                            <label class="col-sm-3 control-label no-padding-right ">{{lng_expenses.category}}</label>
                            <div class="col-sm-5">
                                <select id="catlist-sel" name="parent" class="form-control">


                                </select>
                            </div>
                        </div>

						<div class="form-group edit-cat">
						<label class="col-sm-3 control-label no-padding-right ">{{lng_expenses.name}}</label>
						<div class="col-sm-9">
							<input type="text" id="cat_name" name="cat_name" class="form-control">

                        </div>
						</div>
					</form>

		          </div>
	          </div>
          </div>

          <div class="modal-footer" >

            <button type="submit" id="doSave"   class="btn btn-primary doShare btn-md ">{{lng_common.btn_save}}</button>
            <button type="submit" id="doEdit" style="display: none"  class="btn btn-primary doShare btn-md ">{{lng_common.btn_edit}}</button>

            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>



        </div>
      </div>
     </div>



{% endblock %}


{% block js %}
    <script src="/js/dev/expenses_categories.js?v={{ hashver }}"></script>
{% endblock %}