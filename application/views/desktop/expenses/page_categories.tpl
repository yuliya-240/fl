

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">


              <div id="accordion3" class="panel-group accordion ">
			  	{%for r in records %}
			  	
			  	{%if r.expcat_default%}
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
	                    <a  data-parent="#accordion3" href="#{{r.expcat_id}}" class="collapsed">
		                     {{r.expcat_name}}
		                </a>
		            </h4>
                  </div>
                 
                </div>
			  	
			  	{%else%}
                <div class="panel panel-default" id="ac{{r.expcat_id}}">
                  <div class="panel-heading">
                    <h4 class="panel-title">
	                    <a data-toggle="collapse" data-parent="#accordion3" href="#{{r.expcat_id}}" class="collapsed">
		                    <i class="icon s7-angle-down"></i> {{r.expcat_name}}
                            <span class="pull-right s7-trash" id="delCat" data-id="{{ r.expcat_id }}" style="font-size: 20px; color: red;"></span>
                            <span class="pull-right s7-note" id="editCat" data-id="{{ r.expcat_id }}" style="font-size: 20px; color:#0000ff;"></span>
		                </a>
		            </h4>
                  </div>
                  <div id="{{r.expcat_id}}" class="panel-collapse collapse">
                    <div class="panel-body" style="background-color: #f0f0f0; padding-left: 0;" >
                        <div id="subcatlist{{r.expcat_id}}">
	                	{%for s in r.sub%}
                        <div id="li{{ s.expcat_id }}">
	                	  <div class="col-xs-1" style="padding-right: 0; padding-top: 10px;"><span class="s7-angle-right" style="font-size: 25px;"></span></div>
	                	     <div class="col-xs-11" style="padding-left: 0; padding-top: 3px;">
	                	        <a class="list-group-item " data-id="{{ s.expcat_id }}" >{{s.expcat_name}}
                                 <span class="pull-right s7-trash" id="delSubCat" data-id="{{ s.expcat_id }}" style="font-size: 20px; color: red;"></span>
                                 <span class="pull-right s7-note" id="editCat" data-id="{{ s.expcat_id }}" style="font-size: 20px; color: #0000ff;"></span>
                                </a>
	                	    </div>
                        </div>
	                	{%endfor%}
	                	</div>
	             
                        <div class="tools" style="text-align:center;padding-bottom: 0px;font-size:35px">
                                <a href="#" data-toggle="modal" data-placement="bottom" data-target="#new-sub-cat-box" data-original-title="{{lng_common.btn_add}}">
                                    <span class="icon s7-plus n-s-c" data-id="{{ r.expcat_id }}" style="color: #7ACCBE;"></span>
                                </a>
                            </div>

	                </div>
                  </div>
                </div>
                {%endif%}
                {%endfor%}
         
              </div>
            </div>

    </div>





