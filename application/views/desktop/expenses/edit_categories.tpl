<input type="hidden" name="expcat_id" value="{{ expcat.expcat_id }}">
<input type="hidden" name="parent" value="{{ expcat.expcat_parent_id }}">

<label class="col-sm-3 control-label no-padding-right "> Name</label>
<div class="col-sm-9">
    <input type="text" id="cat_name" name="cat_name" class="form-control" value="{{ expcat.expcat_name }}">
</div>