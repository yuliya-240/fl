{% extends skin~"/common/root.tpl" %}
{%block preheadcss%}
    <link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.min.css"/>
{%endblock%}
{% block headcss %}
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
{% endblock %}
{% block content %}
    <input type="hidden" id="_pdt" value="{{lng_expenses.del_bundle_title}}">
    <input type="hidden" id="_pdm" value="{{lng_expenses.del_bundle_message}}">
    <input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">

    <div class="am-content" style="margin-left: 0;">
        <div class="page-head">
            <h2>{{ lng_expenses.title }}
	            
				<a href="#" class="btn btn-success btn-head pull-right g-l" data-go="/expenses/add" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_expenses.add_exp}}">{{lng_expenses.add_exp}}</a>
		            
	            
            </h2>

        </div>
        <div class="main-content">
		<input type="hidden" id="moment-format" value="{{momentformat}}">
		<input type="hidden" id="startDate" value="{{startdate}}">
		<input type="hidden" id="endDate" value="{{enddate}}">
			
			<div class="row">
				<div class="col-sm-8">
					<form id="fsearch"  class="form-horizontal group-border-dashed">
						<div class="form-group">
						<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon " style="background-color:#ccc; color:#fff; " ><span class="s7-search" style="font-size: 24px;"></span></span>
			                      <input class="form-control" id="s" placeholder="{{lng_expenses.search_ph}}" {%if searchstr%}value="{{searchstr}}"{%endif%} type="text" style="border-width:1px;" >
			                <span class="input-group-addon " id="idrp"  style="background-color:#4e91ff; color:#fff; cursor: pointer;" ><span class="s7-date" style="font-size: 24px;"></span></span>
			                       
			                    </div>
						</div>		
						</div>		
					</form>
					
				</div>

                <div class="col-sm-4" style="padding-top: 10px;">
                    <select id="cat" name="cat" class="select2">
	                    <option value="0@0">{{lng_expenses.all_cat}}</option>
                        {%for c in cats%}
                            <option {%if ccat==c.expcat_id%}selected{%endif%} value="{{c.expcat_id}}@{{c.expcat_parent_id}}">{{c.expcat_name}}</option>
                            {%for s in c.sub%}
                                <option {%if ccat==s.expcat_id%}selected{%endif%} value="{{s.expcat_id}}@{{s.expcat_parent_id}}">{{s.expcat_name}}</option>
                            {%endfor%}
                        {%endfor%}
                    </select>
                </div>

			</div>	
			
			
            <div class="row">
                <div class="col-sm-12">
                               
				<form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
				<div class="panel panel-default panel-borders">
		
			        <div class="panel-body">
					
						<div id="d-content">
				
						</div>
			
			        </div>
				</div>
				</form>
                </div>
            </div>
        </div>
    </div>
    <div id="expenses-view" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
        <div style="width: 100%;" class="modal-dialog">
            <div class="modal-content">
	   
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">{{lng_expenses.details}}</h3>
                </div>

                <div class="modal-body" id="view-exp-details-body" >

                </div>
			     <div class="modal-footer" >
			        <a href="#" class="btn btn-primary btn-md" id="editExpeFromView"  >{{lng_common.btn_edit}}</a>
			        <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>
			        <button type="button"  class="btn btn-danger btn-md  pull-left del">{{lng_common.btn_del}}</button>

			    </div>
                
            </div>
        </div>
    </div>

{% endblock %}
{%block footer%}
    <div id="megafooter">

    </div>
{%endblock%}


{% block js %}
	<script src="/assets/lib/date-time/moment.min.js"></script>
    <script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>   
    <script src="/js/dev/expenses.js" type="text/javascript"></script>


{% endblock %}


