{% extends skin~"/common/root.tpl" %}
{%block preheadcss%}
    <link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.min.css"/>
{%endblock%}

{%block headcss%}

    <link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
    <link href="/assets/lib/venobox/venobox.css" rel="stylesheet" />

{%endblock%}

{%block content%}

    <div class="page-head">
        <h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_expenses.titlenew}} </h3>
    </div>
    <div class="main-content">
        <div class="row">
            <div class="col-xs-12 col-md-8">
	            <form id="fexpense" class="form-horizontal">
		            
                <div class="panel panel-default panel-borders">

                    <div class="panel-heading">
                        <span class="title">{{lng_expenses.details}}</span>
                    </div>

                    <div class="panel-body">

                        
                            <input type="hidden" name="exp_id" id="exp_id" value="{{exp.exp_id}}">
                            <input type="hidden" id="vendor_id" name="vendor_id" value="{{ exp.exp_vendor_id }}">
                            <input type="hidden" id="pic" name="pic" value="{{ exp.exp_pic }}">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right " >{{lng_expenses.date}}</label>

                                <div class="col-sm-4 col-md-3" >
                                    <div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-calendar bigger-110"></i>
						</span>
                                        <input type="text" data-date-format="{{dpformat}}" readonly="" value="{{exp.exp_date}}" name="date" class="form-control date-picker">

                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right " >{{lng_expenses.cat}}</label>

                                <div class="col-sm-5" >
                                    <select id="cat" name="cat" class="select2" value="{{ exp.expcat_name }}">
                                        {%for c in cats%}
                                            <option {% if  exp.exp_category==c.expcat_id%}selected{% endif %} value="{{c.expcat_id}}@{{c.expcat_parent_id}}">{{c.expcat_name}}</option>
                                            {%for s in c.sub%}
                                                <option {% if  exp.exp_category==s.expcat_id%}selected{% endif %} value="{{s.expcat_id}}@{{s.expcat_parent_id}}">{{s.expcat_name}}</option>
                                            {%endfor%}
                                        {%endfor%}
                                    </select>
                                </div>
                                <div class="hidden-xs col-sm-1" style="padding-left: 0;">
                                    <a href="#" data-toggle="modal" data-placement="bottom" data-target="#new-cat-box" data-original-title="{{lng_common.btn_add}}" >
                                        <button class="btn btn-sm btn-success n-s-c" id="add-cat" style="padding: 6px 9px;"><i class="material-icons">add</i></button>
                                    </a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{lng_expenses.vendor}}</label>
                                <div class="col-sm-6">
                                    <input class="form-control" name="vendor" id="vendor" value="{{ exp.exp_vendor }}" type="text">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{lng_expenses.desc}}<span style="color: red;">*</span></label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="name" value="{{ exp.exp_desc }}" placeholder="{{lng_expenses.ph_desc}}" data-errorbox="#errname" data-msg-required="{{lng_clients.name_req}}"  required="" placeholder="{{lng_projects.ph_name}}" type="text">
                                    <div id="errname" style="color:red;"></div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right " >{{lng_expenses.am}}</label>

                                <div class="col-md-2 col-sm-3" >
                                    <input class="form-control dec" name="amount"  id="amount" value="{{ exp.exp_amount }}" type="text">

                                </div>

                                <div class="col-sm-4">
                                    <select class="form-control" id="currency" name="currency">
                                        {%for c in currencies%}
                                            <option {%if  c.currency_iso==exp.exp_currency%}selected{%endif%} value="{{c.currency_iso}}"> {{c.currency_iso}} {{c.currency_name}} </option>
                                        {%endfor%}

                                    </select>
                                </div>
                                <div class="col-sm-3">

                                    <select name="method" id="method" class="form-control">
                                        {%for pm in paymethod%}
                                            <option {%if  pm.paymethod_method==exp.exp_method%}selected{%endif%} value="{{pm.paymethod_method}}">{{pm.paymethod_name}}</option>
                                        {%endfor%}
                                    </select>
                                </div>

                            </div>

                            <div class="form-group" style="display:none;" id="rate-box">
                                <label class="col-sm-3 control-label no-padding-right " >{{lng_expenses.rate}}</label>

                                <div class="col-md-4 col-sm-5" >
                                    <div class="input-group xs-mb-15"  >
                                        <span class="input-group-addon" id="currency-label">{{acs.user_currency}}</span>
                                        <input type="text" id="exchange_rate" autocomplete="off" name="exchange_rate" class="form-control dec" value="1">
                                    </div>
                                    <div id="errrate" style="color:red;"></div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="input-group xs-mb-15"  >
                                        <input class="form-control" name="base" id="base" readonly="" type="text">
                                        <span class="input-group-addon" >{{acs.user_currency}}</span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right " >{{lng_expenses.tax}}</label>
                                <div class="col-md-3 col-sm-4" >
                                    <div class="input-group xs-mb-15">
                                        <span class="input-group-addon" id="selcurr">{{acs.user_currency}}</span>
                                        <input class="form-control tax" name="tax" value="{{ exp.exp_tax1_cash }}" type="text">
                                    </div>
                                </div>
                            </div>


                        
                    </div>
                </div>
                
				<div class="panel panel-default panel-borders">
		
			        <div class="panel-heading">
				        <span class="title">{{lng_expenses.details}}</span>
			        </div>   
			        <div class="panel-body">
				        
				        <table class="table exp-details" id="exp-lines">
					        {%for el in explines%}
							<tr id="line{{el.el_id}}">
								<td class="td-a-t" width="80%">
									<input type="text" name="el_desc[]" required  id="eldescc{{id}}" data-id="{{id}}" data-errorbox="#errsrvline{{el.el_id}}" data-msg-required="{{lng_common.fr}}" value="{{el.el_desc}}"  class="form-control  " >
									<div id="errsrvline{{el.el_id}}" style="color:red;"></div>
								</td>
								<td class="td-a-t" >
									<input type="text" value="{{el.el_amount}}" name="el_amount[]" id="elamount{{el.el_id}}" data-id="{{el.el_id}}" class="form-control dec pull-left"  >
									<button class="btn btn-space btn-default text-danger delline" type="button" data-id="{{el.el_id}}" style="padding-top: 7px !important; border: none !important; position: absolute; background: none !important;  padding-left: 6px;" ><span class="s7-close-circle" style="font-size:28px; font-weight: 700;"></span></button>
							
								</td>
							</tr>
					        
					        {%endfor%}
					        
				        </table>
				        <div style="clear: both;">&nbsp;</div>
				        <div class="text-center">
				        <button type="button" id="addLine" class="btn btn-success btn-lg ">{{lng_expenses.btn_add_exp_details}}</button>
				        </div>
			        </div>
			    </div>

                </form>
                
                
                <button type="button" id="doSave" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
                <a href="#" class="btn btn-default btn-lg pull-left g-l" data-go="/expenses">{{lng_common.btn_cancel}}</a>
                <div style="clear:both;margin-bottom: 80px;"></div>


            </div>
            <div class="col-xs-12 col-md-4">

                <div class="panel panel-default panel-borders">

                    <div class="panel-heading">
                        <span class="title">{{lng_expenses.receipt}}</span>
                    </div>

                    <div class="panel-body">

                        <div class="pic" id="counteinerpic">
                            <img class="img-responsive center-block" id="receipt" {% if exp.exp_pic=="nopic.png" %} src="/img/r256.png" {% else %} src="/userfiles/{{ acs.user_id }}/pic/{{ exp.exp_pic }}" {% endif %}>
                        </div>
                        <hr>

                        <button class="btn btn-success pull-right" id="pickfiles2" style="margin-top: 20px;">{{ lng_expenses.up_receipt }}</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {%for r in rates%}
        <input type="hidden" id="rate{{r.curr}}" value="{{r.val}}">
    {%endfor%}

    <div id="new-cat-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
        <div style="width: 100%;" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">{{lng_expenses.btn_add}}</h3>
                </div>

                <div class="modal-body"  >
                    <div class="row">
                        <div class="col-xs-12">
                            <form id="fdata" class="form-horizontal" role="form">
                                <input type="hidden" name="exp_id" id="exp_id" value="{{exp.exp_id}}">
                                <div class="form-group" id="catlist-box">
                                    <label class="col-sm-3 control-label no-padding-right ">{{lng_expenses.category}}</label>
                                    <div class="col-sm-5">
                                        <select id="catlist-sel" name="parent" class="form-control">
                                            <option value="0">Select category</option>
                                            {%for c in cats%}
                                                <option value="{{c.expcat_id}}">{{c.expcat_name}}</option>
                                            {%endfor%}
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group edit-cat">
                                    <label class="col-sm-3 control-label no-padding-right ">{{lng_expenses.name}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="cat_name" name="cat_name" class="form-control">

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

                <div class="modal-footer" >

                    <button type="submit" id="doSaveCat"   class="btn btn-primary doShare btn-md ">{{lng_common.btn_save}}</button>
                    {#<button type="submit" id="doEditCat" style="display: none"  class="btn btn-primary doShare btn-md ">{{lng_common.btn_edit}}</button>#}

                    <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

                </div>



            </div>
        </div>
    </div>


{%endblock%}



{%block js%}

    <script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>
    <script src="/assets/lib/livequery/jquery.livequery.js"></script>
    <script src="/assets/lib/typeahead/bootstrap3-typeahead.min.js?v={{hashver}}"></script>
    <script src="/assets/lib/mask/jquery.mask.js"></script>
    <script src="/assets/lib/plupload-2.3.1/js/plupload.full.min.js"></script>
    <script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="/assets/lib/venobox/venobox.min.js"></script>
    <script src="/js/dev/expenses_edit.js?v={{hashver}}"></script>
{%endblock%}

