{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs"></th>
			
			<th>{{lng_expenses.date}}</th>
			<th>{{lng_expenses.desc}}</th>
			<th>{{lng_expenses.cat}}</th>
			<th>{{lng_expenses.vendor}}</th>
			<th>{{lng_expenses.paymethod}}</th>

			<th style="text-align: right;">{{lng_expenses.total}}</th>
			<th width="1%"></th>
		</tr>
	</thead>
	<tbody>
		{%for r in records%}
		<tr>
			<td class="text-center hidden-xs" style="vertical-align:middle;">
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.exp_id}}" class="check" name="row[]" value="{{r.exp_id}}">
	              <label for="chk{{r.exp_id}}"></label>
	            </div>
			</td>
	
			<td class="tbl-pointer view " data-id="{{r.exp_id}}">{{r.exp_date|date(acs.user_dformat)}}</td>
			<td class="tbl-pointer view " data-id="{{r.exp_id}}">{{r.exp_desc}}</td>
			<td class="tbl-pointer view" data-id="{{r.exp_id}}" style="font-weight: 600">
				{%if r.exp_category_parent%}
					{{r.parentname}} > {{r.catname}}
				{%else%}
					{{r.catname}}
				{%endif%}
			</td>
			<td class="tbl-pointer view" data-id="{{r.exp_id}}">{{r.exp_vendor}}</td>

            <td class="tbl-pointer ">{%if r.exp_method%}{{r.paymethod_name }}{%endif%}</td>

			<td class="tbl-pointer view g-l" data-id="{{r.exp_id}}" style="text-align: right;"><span style="font-weight: 600">{{r.exp_amount|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{r.exp_currency}}</small>
				{%if r.exp_currency!=acs.user_currency%}
				<br><small>{{r.exp_amount_base|number_format(2,acs.user_dsep,acs.user_tsep)}}</small> <small>{{acs.user_currency}}</small>
				{%endif%}
			</td>
			<td class="tbl-pointer view" data-id="{{r.exp_id}}">
				<a href="#"   data-id="{{r.exp_id}}" class="view"><i class="material-icons">visibility</i></a>
			</td>
		</tr>
		{%endfor%}
	</tbody>
	<tfoot>
		<tr style="background-color: #fff7eb;">
			<td colspan="6" >{{lng_expenses.total}}</td>
			<td  style="text-align: right;"><span style="font-weight: 600; ">{{totalPage|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{acs.user_currency}}</small></td>
			<td style=" background-color: #fff7eb;"></td>
		</tr>	
	
	</tfoot>	
</table>	

			
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
