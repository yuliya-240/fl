<div id="{{uniqid}}">
	<hr>	
	<div class="col-xs-12" style="padding-right: 0;">
		<a href="#" data-id="{{uniqid}}" class="text-danger pull-right del-work-record"><i class="fa fa-times"></i> Delete</a>
		<div class="clearfix" style="margin-bottom: 20px;"></div>
	</div>


    <div class="form-group">
        <label class="col-sm-2 control-label">{{lng_profile.company}}</label>
        <div class="col-sm-10">
            <input class="form-control" name="work_company[]"  type="text">
        </div>
    </div>

	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.position}}<span style="color: red;">*</span></label>
	  <div class="col-sm-10">
	    <input class="form-control" name="work_position[]" data-errorbox="#errname{{uniqid}}" data-msg-required="{{lng_common.fr}}"  required="" placeholder="{{lng_profile.position_name}}" type="text">
	    <div id="errname{{uniqid}}" style="color:red;"></div>
	  </div>
	
	</div>

    <div class="form-group">
        <label class="col-sm-2 control-label">{{lng_profile.desc}}</label>
        <div class="col-sm-10">
            <textarea name="work_desc[]" class="form-control"></textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">{{lng_profile.location}}</label>
        <div class="col-sm-10">
            <input class="form-control" name="work_location[]"  type="text">
        </div>
    </div>


    <div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.timeperiodfrom}}</label>
	  <div class="col-sm-2">
	    <select name="work_year_from[]" class="form-control">
		    {%for yf in 1960..2017%}
		    <option {%if yf==2017%}selected=""{%endif%} value="{{yf}}">{{yf}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	  <div class="col-sm-2">
	    <select name="work_month_from[]" class="form-control">
		    {%for mf in months%}
		    <option  value="{{mf.mon_num}}">{{mf.mon_name}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	  <div id="work-to-box{{uniqid}}">
		  <div class="col-sm-1 control-label">To</div>
		  <div class="col-sm-2">
		    <select name="work_year_to[]" class="form-control">
			    {%for yf in 1960..2017%}
			    <option {%if yf==2017%}selected=""{%endif%} value="{{yf}}">{{yf}}</option>
			    {%endfor%}
		    </select>    
		  </div>
		  <div class="col-sm-2">
		    <select name="work_month_to[]" class="form-control">
			    {%for mf in months%}
			    <option  value="{{mf.mon_num}}">{{mf.mon_name}}</option>
			    {%endfor%}
		    </select>    
		  </div>
	    </div>
	</div>

    <div class="form-group">
	    <label class="col-sm-2 control-label">&nbsp;</label>
        <div class="am-checkbox col-sm-10" style="padding-left: 15px;">
            <label  for="check">{{lng_profile.present_time}}</label>
            <input type="checkbox" class="check-work-now" data-id="{{uniqid}}" value="1">
            <input type="hidden" name="work_current[]" id="wcv{{uniqid}}" value="0">
        </div>
    </div>

</div>






	  