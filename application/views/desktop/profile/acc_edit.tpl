<form id="facc" style="border-radius: 0px;" class="form-horizontal ">


    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label"><span style="color: red;">*</span> {{lng_first.form_name}}</label>
      <div class="col-sm-6">
        <input type="text" name="user_name" required value="{{acs.user_name}}" class="form-control" data-errorbox="#errname-acc" data-msg-required="{{lng_common.fr}}">
        <div id="errname-acc" style="color:red;"></div>
      </div>
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">{{lng_profile.write_about}}</label>
      <div class="col-sm-9">
        <textarea  name="user_about"  class="form-control">{{acs.user_about}}</textarea>
      </div>
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">{{lng_profile.birthday}}</label>
      <div class="col-sm-2">
        <select name="bday" class="form-control">
            {%for i in 1..31%}
            <option {%if bday==i%}selected{%endif%} value="{{i}}">{{i}}</option>
            {%endfor%}
        </select>
      </div>  
      <div class="col-sm-4">
        <select name="bmon" class="form-control">
            {%for m in monlist%}
            <option {%if bmon==m.mon_num%}selected{%endif%} value="{{m.mon_num}}">{{m.mon_full_name}}</option>
            {%endfor%}
        </select>
      </div>  
      <div class="col-sm-3">
        <select name="byear" class="form-control">
            {%for i in 1920..2010%}
            <option {%if byear==i%}selected{%endif%} value="{{i}}">{{i}}</option>
            {%endfor%}
        </select>
      </div>  

   
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">{{lng_first.form_prof}}</label>
      <div class="col-sm-6">
        <select class="form-control" name="user_profession" >
			{%for p in proffesionsList%}
			<option {%if p.pro_id==acs.user_proffession%}selected{%endif%} value="{{p.pro_id}}">{{p.pro_name}}</option>
			{%endfor%}
        </select>
      </div>
    </div>
    
    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">{{lng_profile.phone}}</label>
      <div class="col-sm-4">
        <input type="text" name="user_phone" value="{{acs.user_phone}}" class="form-control">
      </div>
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">{{lng_profile.address}}</label>
      <div class="col-sm-6">
        <input type="text" name="user_street" value="{{acs.user_street}}" class="form-control">
      </div>
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">{{lng_profile.city}}</label>
      <div class="col-sm-4">
        <input type="text" name="user_city" value="{{acs.user_city}}" class="form-control">
      </div>
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">{{lng_profile.state}}</label>
      <div class="col-sm-4">
        <input type="text" name="user_state" value="{{acs.user_state}}" class="form-control">
      </div>
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">{{lng_profile.zip}}</label>
      <div class="col-sm-4">
        <input type="text" name="user_zip" value="{{acs.user_zip}}" class="form-control">
      </div>
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">{{lng_profile.country}}</label>
      <div class="col-sm-6">
        <select class="form-control" name="user_country" >
			{%for c in countries%}
			<option {%if c.country_iso==acs.user_country%}selected{%endif%} value="{{c.country_iso}}">{{c.country_printable_name}}</option>
			{%endfor%}
            
        </select>
      </div>
    </div>
  				
    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">Website</label>
      <div class="col-sm-6">
        <input type="text" name="user_www" value="{{acs.user_www}}" class="form-control">
      </div>
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">Skype</label>
      <div class="col-sm-5">
        <input type="text" name="user_skype" value="{{acs.user_skype}}" class="form-control">
      </div>
    </div>

    <div class="form-group" style="padding-top: 0;">
      <label class="col-sm-3 control-label">Twitter</label>
      <div class="col-sm-4">
        <input type="text" name="user_twitter" value="{{acs.user_twitter}}" class="form-control">
      </div>
    </div>
  	<hr>				

	<button type="button"  style="margin-bottom: 20px;" id="btn-cancel-acc-edit" class="btn btn-lg btn-space btn-default">{{lng_common.btn_cancel}}</button>
	<button type="button" style="margin-bottom: 20px;" id="btn-save-acc-edit" class="btn btn-lg btn-space pull-right btn-primary">{{lng_common.btn_save}}</button>
</form>

