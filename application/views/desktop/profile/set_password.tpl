<form id="floginpass" class="form-horizontal" role="form">
	<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" >{{lng_profile.сpass}} </label>
			<div class="col-sm-7">
				<input type="password" required="" name="cpass" class="form-control" data-errorbox="#error-box-cpass" data-msg-required="{{lng_common.fr}}">
				<div id="error-box-cpass"  style="color:red;"></div>
			</div>
	</div>

	<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" >{{lng_profile.newpass}} </label>
			<div class="col-sm-7">
				<input type="password" required="" name="loginpassword" class="form-control"  data-errorbox="#error-box-npass" data-msg-required="{{lng_common.fr}}">
				<div id="error-box-npass"  style="color:red;"></div>
			</div>
	</div>
</form>

