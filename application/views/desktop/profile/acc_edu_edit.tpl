<form class="form-horizontal" id="fedu">
<div id="edu-edit-list">
{%if records%}

{%for r in records%}	
<div id="{{r.edu_id}}">
	{%if loop.index>1%}<hr>{%endif%}	
	<div class="col-xs-12" style="padding-right: 0;">
		<a href="#" data-id="{{r.edu_id}}" class="text-danger pull-right del-edu-record"><i class="fa fa-times"></i> Delete</a>
		<div class="clearfix" style="margin-bottom: 20px;"></div>
	</div>		
		
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.school}}<span style="color: red;">*</span></label>
	  <div class="col-sm-10">
	    <input class="form-control" name="edu_school[]" data-errorbox="#errname{{r.edu_id}}" data-msg-required="{{lng_common.fr}}"  required="" placeholder="{{lng_profile.school_name}}" type="text" value="{{r.edu_school}}">
	    <div id="errname{{r.edu_id}}" style="color:red;"></div>
	  </div>
	
	</div>	 
	
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.timeperiodfrom}}</label>
	  <div class="col-sm-2">
	    <select name="edu_year_from[]" class="form-control">
		    {%for yf in 1960..2017%}
		    <option {%if r.edu_year_from==yf%}selected=""{%endif%} value="{{yf}}">{{yf}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	
	  <div class="col-sm-2">
	    <select name="edu_month_from[]" class="form-control">
		    {%for mf in months%}
		    <option {%if r.edu_month_from==mf.mon_num%}selected=""{%endif%} value="{{mf.mon_num}}">{{mf.mon_name}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	  <div class="col-sm-1 control-label">{{lng_profile.timeperiodto}}</div>
	  <div class="col-sm-2">
	    <select name="edu_year_to[]" class="form-control">
		    {%for yf in 1960..2017%}
		    <option {%if yf==r.edu_year_to%}selected=""{%endif%} value="{{yf}}">{{yf}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	
	  <div class="col-sm-2">
	    <select name="edu_month_to[]" class="form-control">
		    {%for mf in months%}
		    <option {%if mf.mon_num==r.edu_month_to%}selected=""{%endif%} value="{{mf.mon_num}}">{{mf.mon_name}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	
	</div>	
	
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.degree}}</label>
	  <div class="col-sm-6">
	    <input class="form-control" name="edu_degree[]"  type="text" value="{{r.edu_degree}}">
	  </div>
	</div>
	
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.study}}</label>
	  <div class="col-sm-6">
	    <input class="form-control" name="edu_area[]"  type="text" value="{{r.edu_area}}">
	  </div>
	</div>
	
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.desc}}</label>
	  <div class="col-sm-10">
	    <textarea name="edu_desc[]" class="form-control">{{r.edu_desc|nl2br}}</textarea>
	  </div>
	</div>


</div>
{%endfor%}

{%endif%}
</div>
</form>
<div class="text-center">
	
	<a href="#" class="btn btn-sm btn-success btn-edu-add-record">Add Record</a>
</div>	

<hr>
<a href="#" id="btn-cancel-edu" style="margin-bottom: 5px;" class="btn btn-lg btn-space btn-default">{{lng_common.btn_cancel}}</a>
<button id="btn-save-edu" type="button" style="margin-bottom: 5px;" class="btn btn-lg btn-space pull-right btn-primary">{{lng_common.btn_save}}</button>
