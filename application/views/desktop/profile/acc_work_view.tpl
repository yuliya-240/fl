{%if records%}

	<table class="table">
		{%for r in records%}
		<tr>
			<td>
				
				<h3>{{r.work_company}}</h3>
				<p>{%if r.work_position%}<strong>{{r.work_position}}</strong>{%endif%}&nbsp;&nbsp; {{r.mfrom}}, {{r.work_year_from}} - {%if r.work_current!=0%} {{lng_profile.present_time}} {%else%} {{r.mto}}, {{r.work_year_to}}{%endif%} {{r.work_location}}</p>
				{%if r.work_desc%}<p class="text-muted">{{r.work_desc}}</p>{%endif%}
			</td>
		</tr>
		{%endfor%}
	</table>	

{%else%}
<div class="text-center">
	<p>{{lng_common.norecords}}</p>
	
</div>	
{%endif%}
<div class="text-center">
<a href="#" class=" btn-work-add">Add Record</a>
</div>