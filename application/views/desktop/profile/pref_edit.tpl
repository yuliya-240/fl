
<form id="fpref" style="border-radius: 0px;" class="form-horizontal group-border-dashed">

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-6">
        <h4 id="dateExample" class="text-warning ">{{d|date('M d, Y')}} {{d|date('g:i a')}}</h4>
      </div>
    </div>


    <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_first.form_lang}}</label>
      <div class="col-sm-6">
        <select class="form-control" name="user_lang" >
				<option  value="en">English</option>
				<option {%if acs.user_lang=="ru"%}selected{%endif%} value="ru">Русский</option>
        </select>
      </div>
    </div>

     <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_first.form_currency}}</label>
      <div class="col-sm-9">
        <select id="currency" name="user_currency" class="form-control">
           {%for cul in currencyList%}
           <option {%if acs.user_currency==cul.currency_iso%}selected{%endif%} value="{{cul.currency_iso}}"> {{cul.currency_iso}} {{cul.currency_name}}</option>
           {%endfor%}
        </select>
      </div>
    </div>

    
     <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_first.form_tz}}</label>
      <div class="col-sm-9">
        <select id="tz" name="user_tz" class="form-control">
           {%for tz in tzList%}
            <option  {%if acs.user_tz==tz.tz_olson%}selected{%endif%} value="{{tz.tz_olson}}"> {{tz.tz_name}}</option>
           {%endfor%}
        </select>
      </div>
    </div>
    
     <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_first.form_dformat}}</label>
      <div class="col-sm-5">
            <select id="dformat" name="user_dformat" class="form-control">
                <option value="M d, Y">{{dnow|date('M d, Y')}}</option>
                <option {%if acs.user_dformat=="m/d/Y"%}selected{%endif%} value="m/d/Y">{{dnow|date('m/d/Y')}}</option>
                <option {%if acs.user_dformat=="d-M-Y"%}selected{%endif%} value="d-M-Y">{{dnow|date('d-M-Y')}}</option>
				<option {%if acs.user_dformat=="d.m.Y"%}selected{%endif%} value="d.m.Y">{{dnow|date('d.m.Y')}}</option>
            </select>
      </div>
    </div>
    
     <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_first.form_tformat}}</label>
      <div class="col-sm-5">
           <select id="tformat" name="user_tformat" class="form-control">
                <option value="g:i a">{{dnow|date('g:i a')}}</option>
                <option {%if acs.user_tformat=='H:i'%}selected{%endif%} value="H:i">{{dnow|date('H:i')}} (24hr., leading zero)</option>
           </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_first.form_wstart}}</label>
      <div class="col-sm-5">
		<select class="form-control" name="user_wstart" autocomplete="off">
			<option value="0">{{lng_first.form_wstart_sun}}</option>
			<option {%if acs.user_wstart==1%}selected{%endif%} value="1">{{lng_first.form_wstart_mon}}</option>
		</select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_first.form_format_currency}}</label>
      <div class="col-sm-5">
          <select id="mf" name="mf" class="form-control">
             <option value="1"> 1,000,000.99</option>
             <option {%if acs.user_dsep==',' and acs.user_tsep=='.'%}selected{%endif%}  value="2"> 1.000.000,99</option>
             <option {%if acs.user_dsep=='.' and acs.user_tsep==' '%}selected{%endif%} value="3"> 1 000 000.99</option>
          </select>
      </div>
    </div>

<hr>
<a href="#" id="btn-cancel-acc-pref" style="margin-bottom: 5px;" class="btn btn-lg btn-space btn-default">{{lng_common.btn_cancel}}</a>
<button id="btn-save-acc-pref" type="button" style="margin-bottom: 5px;" class="btn btn-lg btn-space pull-right btn-primary">{{lng_common.btn_save}}</button>
</form>

