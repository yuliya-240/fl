<form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
	
    <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_first.form_username}} <span style="color: red;">*</span></label>
      <div class="col-sm-6">
        <input class="form-control" name="user_username" id="username" data-errorbox="#error-box" data-msg-minlength="{{lng_first.username_minlength}}" data-msg-required="{{lng_first.username_req}}" data-msg-remote="{{lng_first.user_exist}}" required="" value="{{acs.user_username}}" type="text">
        <div id="error-box"  style="color:red;"></div>
      </div>
    </div>
	
</form>