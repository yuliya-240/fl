<table class="no-border no-strip skills">
  <tbody class="no-border-x no-border-y">
    <tr>
      <td class="item">{{lng_profile.pro_lang}}<span class="icon s7-world"></span></td>
      <td>{{fulllang}}</td>
    </tr>
    <tr>
      <td class="item">{{lng_first.form_currency}}<span class="icon s7-cash"></span></td>
      <td>{{acs.user_currency}}</td>
    </tr>
    <tr>
      <td class="item">{{lng_first.form_format_currency}}<span class="icon s7-cash"></span></td>
      <td>{{7351674|number_format(2,acs.user_dsep,acs.user_tsep)}}</td>
    </tr>


    <tr>
      <td class="item">{{lng_first.form_tz}}<span class="icon s7-wristwatch"></span></td>
      <td>{{tz.tz_name}}</td>
    </tr>
    
    <tr>
      <td class="item">{{lng_first.form_dformat}}<span class="icon s7-date"></span></td>
      <td>{{today|date(acs.user_dformat)}}</td>
    </tr>
    
    <tr>
      <td class="item">{{lng_first.form_tformat}}<span class="icon s7-alarm"></span></td>
      <td>{{today|date(acs.user_tformat)}}</td>
    </tr>
    
    <tr>
      <td class="item">{{lng_first.form_wstart}}<span class="icon s7-date"></span></td>
      <td>{%if acs.user_wstart==1%}{{lng_first.form_wstart_mon}}{%else%}{{lng_first.form_wstart_sun}}{%endif%}</td>
    </tr>
    
  </tbody>
</table>