<form class="form-horizontal" id="fwork">
	
<div id="work-edit-list">
{%if records%}

{%for r in records%}	
<div id="{{r.work_id}}">
	{%if loop.index>1%}<hr>{%endif%}
    <input type="hidden" name="work_id[]" value="{{r.work_id}}">
	<div class="col-xs-12" style="padding-right: 0;">
		<a href="#" data-id="{{r.work_id}}" class="text-danger pull-right del-work-record"><i class="fa fa-times"></i> Delete</a>
		<div class="clearfix" style="margin-bottom: 20px;"></div>
	</div>

    <div class="form-group">
        <label class="col-sm-2 control-label">{{lng_profile.company}}</label>
        <div class="col-sm-10">
            <input class="form-control" name="work_company[]"  type="text" value="{{r.work_company}}">
        </div>
    </div>

	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.position}}<span style="color: red;">*</span></label>
	  <div class="col-sm-10">
	    <input class="form-control" name="work_position[]" data-errorbox="#errname{{r.work_id}}" data-msg-required="{{lng_common.fr}}"  required="" placeholder="{{lng_profile.position_name}}" type="text" value="{{r.work_position}}">
	    <div id="errname{{r.work_id}}" style="color:red;"></div>
	  </div>
	
	</div>

    <div class="form-group">
        <label class="col-sm-2 control-label">{{lng_profile.desc}}</label>
        <div class="col-sm-10">
            <textarea name="work_desc[]" class="form-control">{{r.work_desc|nl2br}}</textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">{{lng_profile.location}}</label>
        <div class="col-sm-10">
            <input class="form-control" name="work_location[]"  type="text" value="{{r.work_location}}">
        </div>
    </div>


	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.timeperiodfrom}}</label>
	  <div class="col-sm-2">
	    <select name="work_year_from[]" class="form-control">
		    {%for yf in 1960..2017%}
		    <option {%if r.work_year_from==yf%}selected=""{%endif%} value="{{yf}}">{{yf}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	
	  <div class="col-sm-2">
	    <select name="work_month_from[]" class="form-control">
		    {%for mf in months%}
		    <option {%if r.work_month_from==mf.mon_num%}selected=""{%endif%} value="{{mf.mon_num}}">{{mf.mon_name}}</option>
		    {%endfor%}
	    </select>    
	  </div>

    <div id="work-to-box{{r.work_id}}" {%if r.work_current%}style="display: none;"{%endif%} >

	  <div class="col-sm-1 control-label">{{lng_profile.timeperiodto}}</div>
	  <div class="col-sm-2">
	    <select name="work_year_to[]" class="form-control">
		    {%for yf in 1960..2017%}
		    <option {%if yf==r.work_year_to%}selected=""{%endif%} value="{{yf}}">{{yf}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	
	  <div class="col-sm-2">
	    <select name="work_month_to[]" class="form-control">
		    {%for mf in months%}
		    <option {%if mf.mon_num==r.work_month_to%}selected=""{%endif%} value="{{mf.mon_num}}">{{mf.mon_name}}</option>
		    {%endfor%}
	    </select>    
	  </div>
    </div>

    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="am-checkbox col-sm-10">
            <label for="check-work-now">{{lng_profile.present_time}}</label>
            
            <input type="checkbox" data-id="{{r.work_id}}" value="1" {%if r.work_current%}checked{%endif%} class="check-work-now"  >
            <input type="hidden" name="work_current[]" id="wcv{{r.work_id}}" value="{{r.work_current}}">
        </div>
    </div>

</div>
{%endfor%}

{%endif%}
</div>
</form>
<div class="text-center">
	
	<a href="#" class="btn btn-sm btn-success btn-work-add-record">Add Record</a>
</div>	

<hr>
<a href="#" id="btn-cancel-work" style="margin-bottom: 5px;" class="btn btn-lg btn-space btn-default">{{lng_common.btn_cancel}}</a>
<button id="btn-save-work" type="button" style="margin-bottom: 5px;" class="btn btn-lg btn-space pull-right btn-primary">{{lng_common.btn_save}}</button>
