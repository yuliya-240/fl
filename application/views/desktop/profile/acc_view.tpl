		                <span class="description">{%if acs.user_about%}{{acs.user_about}}{%else%}{{lng_profile.write_about}}{%endif%}</span>
	                    <table class="no-border no-strip skills">
	                      <tbody class="no-border-x no-border-y">
	                        <tr>
	                          <td  class="item">{{lng_profile.occupation}}<span class="icon s7-portfolio"></span></td>
	                          <td >{{proff}}</td>
	                          <td width="20%" align="right">
                        
		                        <input type="checkbox" value="1" {%if acs.user_bday_pub%}checked{%endif%} class="pub"  name="user_proffession_pub">
								  </td>
	                        </tr>
	                        <tr>
	                          <td class="item">{{lng_profile.birthday}}<span class="icon s7-gift"></span></td>
	                          <td>{{acs.user_bday|date(acs.user_dformat)}}</td>
	                          <td width="20%" align="right">
                        
		                          
		                         <input type="checkbox" value="1" {%if acs.user_bday_pub%}checked{%endif%} class="pub"  name="user_bday_pub"></td>
	                        </tr>
	                        <tr>
	                          <td class="item">{{lng_profile.phone}}<span class="icon s7-phone"></span></td>
	                          <td>{%if acs.user_phone%}{{acs.user_phone}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>
	                          <td width="20%" align="right"><input type="checkbox" value="1" {%if acs.user_phone_pub%}checked{%endif%} class="pub"  name="user_phone_pub"></td>
	                        </tr>
	                        <tr>
	                          <td class="item">{{lng_profile.location}}<span class="icon s7-map-marker"></span></td>
	                          <td>
		                      	{%if acs.user_street or acs.user_city or acs.user_state or acs.user_zip%}
		                          <address>{{acs.user_street}}  {{acs.user_city}}  {{acs.user_state}} {{acs.user_zip}}</address>
		                        {%else%}
		                        	<span class="text-danger">{{lng_common.ns}}</span>
		                        {%endif%}
		                      </td>
		                      <td width="20%" align="right"><input type="checkbox" value="1" {%if acs.user_addr_pub%}checked{%endif%} class="pub"  name="user_addr_pub"></td>
	                        </tr>
	                        <tr>
	                          <td class="item">{{lng_profile.country}}<span class="icon s7-map-marker"></span></td>
	                          <td>{{country}}</td>
	                          <td width="20%" align="right"><input type="checkbox" disabled="" value="1" {%if acs.user_country_pub%}checked{%endif%}  class="pub"  name="user_country_pub"></td>
	                        </tr>
	
	                        <tr>
	                          <td class="item">{{lng_profile.website}}<span class="icon s7-global"></span></td>
	                          <td>{%if acs.user_www%}{{acs.user_www}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>
	                          <td width="20%" align="right"><input type="checkbox" value="1" {%if acs.user_www_pub%}checked{%endif%} class="pub"  name="user_www_pub"></td>
	                        </tr>
	
	                        <tr>
	                          <td class="item">Skype<span class="icon s7-global"></span></td>
	                          <td>{%if acs.user_skype%}{{acs.user_skype}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>
	                          <td width="20%" align="right"><input type="checkbox" value="1" {%if acs.user_skype_pub%}checked{%endif%} class="pub"  name="user_skype_pub"></td>
	                        </tr>
	                        <tr>
	                          <td class="item">Twitter<span class="icon s7-global"></span></td>
	                          <td>{%if acs.user_twitter%}{{acs.user_twitter}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>
	                          <td width="20%" align="right"><input type="checkbox" value="1" {%if acs.user_twitter_pub%}checked{%endif%} class="pub"  name="user_twitter_pub"></td>
	                        </tr>
	
	
	                      </tbody>
	                    </table>
