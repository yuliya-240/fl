<div id="{{uniqid}}">
	<hr>	
	<div class="col-xs-12" style="padding-right: 0;">
		<a href="#" data-id="{{uniqid}}" class="text-danger pull-right del-edu-record"><i class="fa fa-times"></i> Delete</a>
		<div class="clearfix" style="margin-bottom: 20px;"></div>
	</div>		
		
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.school}}<span style="color: red;">*</span></label>
	  <div class="col-sm-10">
	    <input class="form-control" name="edu_school[]" data-errorbox="#errname{{uniqid}}" data-msg-required="{{lng_common.fr}}"  required="" placeholder="{{lng_profile.school_name}}" type="text">
	    <div id="errname{{uniqid}}" style="color:red;"></div>
	  </div>
	
	</div>	 
	
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.timeperiodfrom}}</label>
	  <div class="col-sm-2">
	    <select name="edu_year_from[]" class="form-control">
		    {%for yf in 1960..2017%}
		    <option {%if yf==2017%}selected=""{%endif%} value="{{yf}}">{{yf}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	
	  <div class="col-sm-2">
	    <select name="edu_month_from[]" class="form-control">
		    {%for mf in months%}
		    <option value="{{mf.mon_num}}">{{mf.mon_name}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	  <div class="col-sm-1 control-label">{{lng_profile.timeperiodto}}</div>
	  <div class="col-sm-2">
	    <select name="edu_year_to[]" class="form-control">
		    {%for yf in 1960..2017%}
		    <option {%if yf==2017%}selected=""{%endif%} value="{{yf}}">{{yf}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	
	  <div class="col-sm-2">
	    <select name="edu_month_to[]" class="form-control">
		    {%for mf in months%}
		    <option value="{{mf.mon_num}}">{{mf.mon_name}}</option>
		    {%endfor%}
	    </select>    
	  </div>
	
	</div>	
	
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.degree}}</label>
	  <div class="col-sm-6">
	    <input class="form-control" name="edu_degree[]"  type="text">
	  </div>
	</div>
	
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.study}}</label>
	  <div class="col-sm-6">
	    <input class="form-control" name="edu_area[]"  type="text">
	  </div>
	</div>
	
	<div class="form-group">
	  <label class="col-sm-2 control-label">{{lng_profile.desc}}</label>
	  <div class="col-sm-10">
	    <textarea name="edu_desc[]" class="form-control"></textarea>
	  </div>
	</div>


</div>






	  