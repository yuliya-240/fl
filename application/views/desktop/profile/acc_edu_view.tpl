{%if records%}

	<table class="table">
		{%for r in records%}
		<tr>
			<td>
				
				<h3>{{r.edu_school}}</h3>
				<p>{%if r.edu_degree%}<strong>{{r.edu_degree}}</strong>{%endif%}&nbsp;&nbsp; {{ r.mfrom}}, {{r.edu_year_from}} - {{r.mto}}, {{r.edu_year_to}} {{r.edu_area}}</p>
				{%if r.edu_desc%}<p class="text-muted">{{r.edu_desc}}</p>{%endif%}
			</td>
		</tr>
		{%endfor%}
	</table>	

{%else%}
<div class="text-center">
	<p>{{lng_common.norecords}}</p>
	
</div>	
{%endif%}
<div class="text-center">
<a href="#" class=" btn-edu-add">Add Record</a>
</div>