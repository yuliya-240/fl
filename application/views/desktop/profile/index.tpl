{% extends skin~"/common/root.tpl" %}
{%block headcss%}
<link href="/assets/lib/switch/css/bootstrap-switch.min.css" rel="stylesheet" />
	
    <link rel="stylesheet" href="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/css/Jcrop.css" type="text/css">
    	
		<link rel="stylesheet" href="/css/growl.css">
    <link rel="stylesheet" href="/css/profile.css" type="text/css"/>
   
{%endblock%}

{%block content%}
        <div class="main-content">
	    <input type="hidden" id="_ne" value="{{lng_users.not_email}}">
	    <input type="hidden" id="_er" value="{{lng_users.email_req}}">
	    <input type="hidden" id="_le" value="{{lng_profile.le}}">
	    <input type="hidden" id="_ur" value="{{lng_first.name_req}}">
	    <input type="hidden" id="_nu" value="{{lng_profile.not_uname}}">
        <input type="hidden" id="name-ava" value="{{acs.user_pic}}">
	    <input type="hidden" id="_tit" value="{{lng_profile.ch_email}}">
	     <div class="row">
          <div class="col-xs-12">
          <div class="user-profile">
            <div class="user-display" >
              <div style="margin-top: 50px;"></div>
              <div class="bottom fw-grid-gallery" style="border:1px solid #ccc; border-radius: 3px;">
                <div class="user-avatar fw-block-3">
	                {#<span class="status"></span>#}
	                
	                <a href="#" data-toggle="modal" data-target="#md-custom">

						<img class="img-responsible" id="u-ava" {%if acs.user_pic=="nopic.png"%}src="/img/avatar/avatar2x200.jpg"{%else%}src="/userfiles/{{acs.user_id}}/pic/{{acs.user_pic}}"{%endif%}>
	                <span><i class="icon s7-camera"></i></span>
	                
	                </a>
	            </div>    
                <div class="user-info">
                  <h4><a target="_blank" href="http://floctopus.com/{{acs.user_username}}">{{acs.user_name}} </a> <span>{{acs.user_}}</span> </h4>
                  <span>{{acs.user_username}}</span>
                </div>
              </div>
            </div>
	        <div class="row">
	              <div class="col-md-7 col-sm-12" >
	                
	                <div class="info-block panel panel-default panel-borders">
	                  <div class="panel-heading">
		                  <div class="tools"><a href="#" class="btn-edit-acc"><span class="icon s7-note"></span></a></div>
	                    <h4>{{lng_profile.about_me}}</h4>
	                  </div>
	                  <div class="panel-body">
		                <div id="infoacc-view-box">

		                </div>
		                
		                <div id="infoacc-edit-box" style="display: none;"></div>
	                    
	                  </div>
	                </div>
	                
	                <div class="panel panel-default panel-borders" id="prof-edu-box">
	                  <div class="panel-heading">
		                  <div class="tools"><a href="#" class="btn-edu-edit"><span class="icon s7-note"></span></a></div>
	                    <h4>{{lng_profile.education}}</h4>
	                  </div>
		              <div class="panel-body">
		              	<div id="edu-view-box"></div>
		              	<div id="edu-edit-box"></div>
		              </div>     
		                
	                </div>

                    <div class="panel panel-default panel-borders" id="prof-work-box">
	                  <div class="panel-heading">
		                  <div class="tools"><a href="#" class="btn-work-edit"><span class="icon s7-note"></span></a></div>
	                    <h4>{{lng_profile.work}}</h4>
	                  </div>
		              <div class="panel-body">
		              	<div id="work-view-box"></div>
		              	<div id="work-edit-box"></div>
		              </div>

	                </div>
					<div style="margin-bottom: 60px;"></div>
	              </div>
	              <div class="col-md-5 col-sm-12" id="bill-panel">
		              
	                <div class="info-block panel panel-default panel-borders">
	                  <div class="panel-heading">
		                  
	                    <h4>{{lng_profile.pro_login}}</h4>
	                  </div>
	                  <div class="panel-body">
	                    <table class="no-border no-strip skills">
	                      <tbody class="no-border-x no-border-y">


	                        <tr>
	                          <td width="10%" class="item">{{lng_first.form_username}}<span class="icon s7-user"></span></td>
	                          <td id="l-u">{{acs.user_username}} 
		                          <button type="button" id="chUsername" class="btn btn-space btn-primary pull-right btn-sm"><i class="material-icons" style="font-size: 18px;">create</i></button>
		                          <input type="hidden" id="login-uname" value="{{acs.user_username}}">
	                          </td>
							</tr>
	                        
	                        <tr>
	                          <td width="10%" class="item">Email<span class="icon s7-mail"></span></td>
	                          <td id="l-e">{{acs.user_email}} 
		                          <button type="button" id="chEmail" class="btn btn-space btn-primary pull-right btn-sm"><i class="material-icons" style="font-size: 18px;">create</i></button>
		                          <input type="hidden" id="login-email" value="{{acs.user_email}}">
	                          </td>
							</tr>
							
	                        <tr>
	                          <td class="item">{{lng_profile.password}}<span class="icon s7-unlock"></span></td>
	                          <td>********** <button type="button" id="chPass" class="btn btn-space btn-primary btn-sm pull-right"><i class="material-icons" style="font-size: 18px;">create</i></button></td>
							</tr>


	                      </tbody>
	                    </table>
		                  	                
	                  </div>
	                </div>
		              
		              
		              
	                <div class="info-block panel panel-default panel-borders" id="prof-pref-box">
	                  <div class="panel-heading">
		                   <div class="tools"><a href="#" class="btn-pref-edit"><span class="icon s7-note"></span></a></div>
	                    <h4>{{lng_first.panel_title_pref}}</h4>
	                  </div>
	                  <div class="panel-body">
		                <div id="acc-pref-view">   
	                  
		               </div>
		               <div id="acc-pref-edit" style="display: none;"> </div>
	                  </div>
	                </div>

	                <div class="panel panel-default panel-borders" id="prof-bill-box">
	                  <div class="panel-heading">
		                   <div class="tools"><a href="#" id="billing-addr-edit"><span class="icon s7-note"></span></a></div>
	                    <h4>{{lng_profile.bill_address}}</h4>
	                  </div>
	                  <div class="panel-body">
		               <div id="billing-addr-view-box">
			               
			               {% include skin~"/profile/viewbillingaddr.tpl" %}
			               
		               </div>
		               <div id="billing-addr-edit-box" style="display: none;"> </div>
	                  </div>
	                </div>
	                <div class="clearfix" style="margin-bottom: 30px;"></div>
	                
	                
	              </div>
	            </div>
          </div>
          </div>
	     </div>      
		</div>

    {%endblock%}

{%block js%}
<script src="/assets/lib/switch/js/bootstrap-switch.min.js"></script>
    <script src="/assets/lib/bootstrap-growl/bootstrap-growl.min.js"></script>
  	{#<script src="/assets/lib/plupload-2.3.1/js/plupload.full.min.js"></script>#}
   <script src="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/js/Jcrop.js"></script>
   <script src="/js/dev/profile.js?v=2{{hashver}}"></script>
    
{%endblock%}

 {%block modals%}
 
 	<form id="fcoord">
	 	<input type="hidden" name="x" id="x">
	 	<input type="hidden" name="y" id="y">
	 	<input type="hidden" name="w" id="w">
	 	<input type="hidden" name="h" id="h">
	 	<input type="hidden" name="fname" id="fname" value="{{acs.user_pic}}">
	 	<input type="hidden" name="ftype" id="ftype">
 	</form> 	
	 	
    <div id="md-custom" tabindex="-1" role="dialog" class="modal fade" >
        {#<input type="hidden" name="name" id="name" value="{{acs.user_pic}}">#}
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         {# <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
          </div>#}
          <div class="modal-body" >
            <div class="text-center" >
              <img class="img-responsible center-block" id="target" {%if acs.user_pic=="nopic.png"%}src="/img/avatar/avatar2x200.jpg"{%else%}src="/userfiles/{{acs.user_id}}/pic/{{acs.user_pic}}"{%endif%}>
			
            </div>  
          </div>
          <div class="modal-footer" id="counteinerpic" >
 				<div class="progress  progress-striped active" id="progress1" style="display:none;">
					<div id="progressSlide" style="width: 0%;" class="progress-bar progress-bar-success">Loading...</div>
				</div>
         
          <div id="btns">
            
            <button type="button" id="btnCancel" class="btn btn-default pull-left"><i class="material-icons">close</i></button>

            <button type="button" id="del-pic" class="btn btn-danger pull-left" style="display: none;"><i class="material-icons">delete_forever</i></button>
            
            <button type="button" id="pickfiles"  class="btn btn-primary"><i class="material-icons">file_upload</i></button>
            
            <button type="button" id="picSave"  class="btn btn-success" style="display: none;"><i class="material-icons">save</i></button>
          </div>
          </div>
        </div>
      </div>
    </div>

 {%endblock%}
