{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs"></th>
			<th>#</th>
			<th>Date</th>
			<th>Recipient</th>
			<th>Status</th>
			<th style="text-align: right;">Total</th>
		
			<th width="1%"></th>
		</tr>
	</thead>
	<tbody>
		{%for r in records%}
		<tr>
			<td class="text-center hidden-xs" style="vertical-align:middle;">
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.inv_id}}" class="check" name="row[]" value="{{r.inv_id}}">
	              <label for="chk{{r.inv_id}}"></label>
	            </div>
			</td>
			<td class="tbl-pointer g-l" data-id="{{r.inv_id}}" data-go="/invoices/view/{{r.inv_id}}">{{r.inv_num}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.inv_id}}" data-go="/invoices/view/{{r.inv_id}}">{{r.inv_date|date(acs.user_dformat)}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.inv_id}}" data-go="/invoices/view/{{r.inv_id}}" style="font-weight: 600">{{r.inv_name}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.inv_id}}" data-go="/invoices/view/{{r.inv_id}}">
				{%if r.inv_arc%}
					<span class="badge">{{lng_common.arc}}</span>
				{%endif%}
				{%if r.inv_status==1%}
				<span class="badge">{{r.invstatus_name}}</span>
				{%endif%}
				{%if r.inv_status==2%}
				<span class="badge badge-success">{{r.invstatus_name}}</span>
				{%endif%}

				{%if r.inv_status==3%}
				<span class="badge badge-warning">{{r.invstatus_name}}</span>
				{%endif%}

				{%if r.inv_status==4%}
				<span class="badge badge-danger">{{r.invstatus_name}}</span>
				{%endif%}

				{%if r.inv_status==5%}
				<span class="badge badge-primary">{{r.invstatus_name}}</span>
				{%endif%}

			</td>
			<td class="tbl-pointer g-l" data-id="{{r.inv_id}}" data-go="/invoices/view/{{r.inv_id}}" style="text-align: right;"><span style="font-weight: 600">{{r.inv_total|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{r.inv_currency}}</small>
				{%if r.inv_currency!=acs.user_currency%}
				<br><small>{{r.inv_total_base|number_format(2,acs.user_dsep,acs.user_tsep)}}</small> <small>{{acs.user_currency}}</small>
				{%endif%}
			</td>
			<td class="tbl-pointer" data-id="{{r.inv_id}}">
				<a href="#" class="g-l" data-go="/invoices/view/{{r.inv_id}}" data-id="{{r.inv_id}}"><i class="material-icons">visibility</i></a>
			</td>
		</tr>
		{%endfor%}
	</tbody>
	<tfoot>
		
		
		<tr style="background-color: #fff7eb;">
			<td colspan="5" >Total</td>
			<td  style="text-align: right; "><span style="font-weight: 600; ">{{totalPage|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{acs.user_currency}}</small></td>
			<td style="background-color: #fff7eb;"></td>
		</tr>	
	
	</tfoot>	
</table>	

			
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
