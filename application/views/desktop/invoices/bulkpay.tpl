{% extends skin~"/common/root.tpl" %}
{%block headcss%}
<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">Add Payments to Invoices 
		 <a href="#" data-go="/invoices" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.back_list}}" class="btn btn-head btn-space btn-dark pull-right g-l"><i class="material-icons btn-head-s">arrow_back</i></a>
		
	</h3>
</div>	


<div class="main-content">
	
  <div class="row">
	<div class="col-xs-12 col-md-8 col-md-offset-2">
		<form id="ftrans" class="form-horizontal">
		{%for i in invoices%}
		<div id="pay-box{{i.inv_id}}" class="panel panel-default panel-borders">
			<div class="panel-heading"> 
                  <div class="tools">
	                  <span class="icon s7-close btn-del-pay" data-id="{{i.inv_id}}"></span>
	              </div>
                  <span class="title " >#{{i.inv_num}} <strong>{{i.inv_name}}</strong>, Due: {{i.inv_total_due}} {{i.inv_currency}}</span>
                </div>
	        <div class="panel-body">	
				
					
		 		<input type="hidden" name="pay_inv_id[]" value="{{i.inv_id}}">
		 		<input type="hidden" name="pay_client_id[]" value="{{i.inv_client_id}}">
		 		<input type="hidden" name="pay_collab_id[]" value="{{i.inv_collab_id}}">
		 		<input type="hidden" name="pay_currency[]" value="{{i.inv_currency}}">
		 		<input type="hidden" id="pay_due{{i.inv_id}}" value="{{i.inv_total_due}}">
		 		
		 		<div class="form-group">
				<div class="col-xs-12 col-sm-4 " >
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-calendar bigger-110"></i>
						</span>

						<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="pay_date[]" class="form-control date-picker">
					</div>							
				</div>
 		
 		
				<div class="col-xs-12 col-sm-4 " >
					<div class="input-group">
						<span class="input-group-addon">{{i.inv_currency}}</span>

						<input type="text" name="amount[]" id="pay_amount{{i.inv_id}}" value="{{i.inv_total_due}}"  class="form-control dec" required="" data-errorbox="#errpayamnt{{i.inv_id}}" data-msg-required="{{lng_transactions.amount_req}}">
						
					</div>	

					<div class="am-checkbox">
                      <input type="checkbox"  checked="checked" class="payinfull" data-id="{{i.inv_id}}" id="pf{{i.inv_id}}">
                      <label for="pf{{i.inv_id}}">Pay in Full</label>
                    </div>
                    <div id="errpayamnt{{i.inv_id}}" style="color:red;"></div>
				</div>

				<div class="col-xs-12 col-sm-4 " >
					<select name="method[]" id="method" class="form-control">
						{%for pm in paymethod%}
						<option {%if pm.paymethod_method==2%}selected{%endif%} value="{{pm.paymethod_method}}">{{pm.paymethod_name}}</option>
						{%endfor%}
					</select>
				</div>
			 		
		 		</div>
		 		{%if acs.user_currency!=i.inv_currency%}
		 		<div class="form-group" >
		 			<label class="col-sm-3 control-label no-padding-left" style="text-align: left;" >Base Currency</label>
		 			<div class="col-xs-12 col-sm-5">
					<div class="input-group">
						<span class="input-group-addon">{{i.inv_currency}}/{{acs.user_currency}}</span>

						<input type="text" name="pay_rate[]" data-id="{{i.inv_id}}" id="pay_rate{{i.inv_id}}" autocomplete="off" value="{{i.inv_exchange_rate}}" class="form-control xr" required="" data-errorbox="#errrate{{i.inv_id}}" data-msg-required="{{lng_common.fr}}">
						
					</div>
					 <div id="errrate{{i.inv_id}}" style="color:red;"></div>	
		 			</div>
		 			
		 		
		 			<div class="col-xs-12 col-sm-4">
		 				<div class="input-group">
						<span class="input-group-addon">{{acs.user_currency}}</span>

						<input type="text" name="pay_due_base[]" id="pay_due_base{{i.inv_id}}" value="{{i.inv_total_due_base}}" readonly="" class="form-control dec">
						
						</div>
						
		 			</div>
		 			
		 		</div>	
		 		{%else%}
		 			<input type="hidden" name="pay_rate[]" value="{{i.inv_exchange_rate}}"> 
		 			<input type="hidden" name="pay_due_base[]" value="{{i.inv_total_due_base}}">	
		 		{%endif%}
		 		<div class="form-group">
			 		
					<div class="col-sm-12">
						<input type="text" class="form-control" id="pay_notes"  placeholder="Payment Notes"  name="pay_notes[]">
						<div id="errnotes{{i.inv_id}}" style="color:red;"></div>
					</div>
			 		
		 		</div>	
	        </div>    
		</div>
		{%endfor%}
		</form>
		<button type="button" id="doPayAll" class="btn btn-primary btn-lg pull-right" >Pay All</button>
		<a href="#" class="btn btn-default btn-lg pull-left g-l" data-go="/invoices">{{lng_common.btn_cancel}}</a>
		<div style="clear:both;margin-bottom: 40px;"></div>	
	</div>
  </div>
  	 
</div>  
{%endblock%}


{%block js%}
	<script src="/assets/lib/mask/jquery.mask.js"></script>	
<script src="/assets/lib/livequery/jquery.livequery.js"></script>
	<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>
	<script src="/js/dev/invoices_pay_bulk.js?v={{hashver}}"></script>
 {%endblock%}
