{% extends skin~"/common/root.tpl" %}
{%block headcss%}
<link href="/assets/lib/venobox/venobox.css" rel="stylesheet" />
<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
<style>
	
.photo{
	border:1px solid #ccc;
}
	
#plist{
	list-style-type: none;
}	
	
.ic-flist{
	font-size: 22px;
}	
	
.form-horizontal .form-group {
	padding: 4px 0 !important; 
}

</style>	

{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 1px;">Invoice #{{inv.inv_num}} 
	
		<div class="btn-group  btn-hspace pull-right" >
              <button type="button" data-toggle="dropdown" class="btn btn-default btn-head dropdown-toggle dropdowndoc" aria-expanded="false"  style="border-color: #7a7a7a;">&nbsp;More Actions&nbsp;&nbsp;<span class="caret"></span></button>
              <ul role="menu" id="act-box" class="dropdown-menu">
                {%if inv.inv_status==1%}<li><a href="#" class="g-l" data-go="/invoices/edit/{{inv.inv_id}}" >&nbsp;Edit</a></li>{%endif%}
                <li><a href="#" class="copy">&nbsp;Copy</a></li>
                {%if inv.inv_arc%}
                <li><a href="#" class="unarc">&nbsp;Unarchive</a></li>
                {%else%}
                <li><a href="#" class="arc">&nbsp;Archive</a></li>
                {%endif%}
                <li><a href="#" class="">&nbsp;PDF</a></li>
                <li><a href="#" data-go="/settings" class="g-l">&nbsp;Settings</a></li>
                <li class="divider"></li>
                <li><a href="#" style="color:red;" class="del">&nbsp;Delete</a></li>
              </ul>
        </div>

		{%if inv.inv_status!=2%}<a href="#" class="btn btn-head btn-success  btn-space pull-right pay">Pay</a>{%endif%}
		<a href="#" class="btn btn-head btn-primary sendemail btn-space pull-right">Send</a>

      <a href="#" data-go="/invoices" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.back_list}}" class="btn btn-head btn-space btn-dark pull-right g-l"><i class="material-icons btn-head-s">arrow_back</i></a>
		
	</h3>
	
</div>	

<input type="hidden" id="inv_id" value="{{inv.inv_id}}">
<input type="hidden" id="_idt" value="{{lng_invoices.del_title}}">
<input type="hidden" id="_idm" value="{{lng_invoices.del_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="_cb" value="{{lng_common.btn_cancel}}">

<input type="hidden" id="_copy_tit" value="{{lng_invoices.copy_title}}">
<input type="hidden" id="_copy_msg" value="{{lng_invoices.copy_msg}}">	

<input type="hidden" id="_arc_tit" value="{{lng_invoices.arc_title}}">
<input type="hidden" id="_arc_msg" value="{{lng_invoices.arc_msg}}">	

<input type="hidden" id="_uarc_tit" value="{{lng_invoices.uarc_title}}">
<input type="hidden" id="_uarc_msg" value="{{lng_invoices.uarc_msg}}">	
	
<input type="hidden" id="client" value="{{postfix}}">

<div class="main-content">
  <div class="row">
	<div class="col-xs-12 col-md-9">
		{%if inv.inv_arc%}
		  <div role="alert" class="alert alert-info alert-icon alert-border-color ">
            <div class="icon"><span class="s7-info"></span></div>
            <div class="message">
              <strong>Archived Invoice!</strong> Do you want to unarchive it? <a href="#" class="unarc">Click Here</a>
            </div>
          </div>		
		
		{%endif%}
		<div class="panel panel-default panel-borders {%if inv.inv_status==6%}chargeoff{%endif%} {%if inv.inv_status==1%}draft{%endif%} {%if inv.inv_status==2%}paid{%endif%} {%if inv.inv_status==4%}pastdue{%endif%} {%if inv.inv_status==5%}sent{%endif%} {%if inv.inv_status==3%}partial{%endif%}">
	        <div class="panel-body">
              <div class="invoice" style="background: none !important; padding: 20px 20px;">
                <div class="row invoice-header">
                  <div class="col-xs-7">
                    <div class="invoice-logo">
	                	{%if acs.set_logo!="nologo.png"%}
	                    <img class="img-responsive" src="/userfiles/{{acs.user_id}}/pic/{{acs.set_logo}}">
	                    {%endif%}
	                </div>
                  </div>
                  <div class="col-xs-5 invoice-order"><span class="invoice-id">{{invlang.invoice}} #{{inv.inv_num}}</span><span class="incoice-date">{{inv.inv_date|date(acs.user_dformat)}}</span></div>
                </div>
                <div class="row invoice-data">
                  <div class="col-sm-5 invoice-person" id="inv-my-addr">
	                  
	                  <span class="name">{{acs.set_billing_name}} </span>
	                  <span>{{acs.set_billing_street}}</span>
	                  <span>{{acs.set_billing_city}}{%if acs.set_billing_state%}, {{acs.set_billing_state}}{%endif%} {{acs.set_billing_zip}}</span>
	                  <span>{{mycountry}}</span>
	                  <a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chMyAddr" >Edit Address</a>
	                
	              </div>
                  <div class="col-sm-2 invoice-payment-direction"><i class="icon mdi mdi-chevron-right"></i></div>
                  <div class="col-sm-5 invoice-person" id="recipient-addr">
	                  
	                  <span class="name"> {{inv.inv_name}} </span>
	                  <span>{{inv.inv_street}}</span>
	                  <span>{{inv.inv_city}}{%if inv.inv_state%}, {{inv.inv_state}}{%endif%} {{inv.inv_zip}}</span>
	                  <span>{{country}}</span>
	                  <a href="#"  class="btn btn-sm " style="padding-right: 0;" id="chAddr" >Edit Address</a>
	                
	              </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <table class="invoice-details">
                      <tr>
                        <th style="width:60%">{{invlang.desc}}</th>
                        <th style="width:15%" class="hours">{{invlang.rate}}</th>
                        <th style="width:10%" class="hours">{{invlang.qty}}</th>
                        <th style="width:15%" class="amount">{{invlang.am}}</th>
                      </tr>
                      {%for l in lines%}
                      <tr>
	                        <td class="description">{{l.invline_srv}}
		                        <br>{{l.invline_desc}}
	                        </td>
	                        <td class="hours">{{l.invline_rate}}</td>
	                        <td class="hours">{{l.invline_qty}}</td>
	                        <td class="amount">{{l.invline_total}}</td>
                      </tr>
                      {%endfor%}
                      <tr>
                        <td></td>
                     
                        <td colspan="2" class="summary">{{invlang.subtotal}} <small style="font-size: smaller">{{inv.inv_currency}}</small>
	                        
	                        
                        </td>
                        <td class="amount">{{inv.inv_subtotal}}</td>
                      </tr>
					   {%if inv.inv_taxes_cash>0%}
                       <tr>
                        <td></td>
                     
                        <td colspan="2" class="summary">{{invlang.taxes}}  <small style="font-size: smaller">{{inv.inv_currency}}</small></td>
                        <td class="amount">{{inv.inv_taxes_cash}}</td>
                      </tr>
					  {%endif%}
                      {%if inv.inv_discount_percentage>0%}
                      
                      <tr>
                        <td></td>
                    
                        <td colspan="2" class="summary">{{invlang.discount}} ({{inv.inv_discount_percentage}}%)</td>
                        <td class="amount">{{inv.inv_discount_cash}}</td>
                      </tr>
                      {%endif%}
                      
                      <tr>
                        <td></td>
          
                        <td colspan="2" class="summary total" >
	                        {{invlang.total}} <small style="font-size: smaller">{{inv.inv_currency}}</small>
	                    </td>

                        <td class="amount total-value" style="color:#000;">{{inv.inv_total}}</td>
                      </tr>
                        
                      <tr>
                        <td></td>
          
                        <td colspan="2" class="summary">{{invlang.paid}} <small style="font-size: smaller">{{inv.inv_currency}}</small></td>
                        <td class="amount text-success">{{inv.inv_paid_amount}}</td>
                      </tr>
                      <tr>
                        <td></td>
          
                        <td colspan="2" class="summary total">{{invlang.due}} <small style="font-size: smaller">{{inv.inv_currency}}</small></td>
                        <td class="amount text-danger">{{inv.inv_total_due}}</td>
                      </tr>
                      
                    </table>
                  </div>
                </div>
				
				{%if inv.inv_terms %}
                <div class="row" style="margin-top: 20px; ">
                  <div class="col-md-12 invoice-message" style="margin-bottom: 20px;"><span class="title">{{invlang.terms}}</span>
                    <p>{{inv.inv_terms}}</p>
                  </div>
                </div>
				{%endif%}
				
				{%if inv.inv_notes%}
                <div class="row" >
                  <div class="col-md-12 invoice-message" style="margin-bottom: 20px;"><span class="title">{{invlang.notes}}</span>
                    <p>{{inv.inv_notes}}</p>
                  </div>
                </div>
				{%endif%}
{#
                <div class="row invoice-company-info">
                  {%if acs.set_logo!="nologo.png"%}
                  <div class="col-sm-2 logo">
	                	
	                    <img class="img-responsive" src="/userfiles/{{acs.user_id}}/pic/{{acs.set_logo}}">
	                    
	              </div>
	              {%endif%}
                  <div class="col-sm-4 summary"><span class="title">Amaretti Company</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                  </div>
                  <div class="col-sm-3 phone">
                    <ul class="list-unstyled">
                      <li>+1(535)-8999278</li>
                      <li>+1(656)-3558302</li>
                    </ul>
                  </div>
                  <div class="col-sm-3 email">
                    <ul class="list-unstyled">
                      <li>amaretti@company.co</li>
                      <li>amaretti@support.co</li>
                    </ul>
                  </div>
                </div>
         #}
              </div>
	        </div>
		</div>   
		
		{%if flist%}
		<div class="panel panel-default panel-borders ">
<div class="panel-heading">Attached Files</div>
	        <div class="panel-body">	
	        
		        
			<ul id="plist">
				{%for f in flist%}
					<li style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px #ccc dotted;" id="attitem{{f.invatt_id}}">
						<div class="pull-left" style="margin-right: 5px; padding-top: 5px;">{%if f.invatt_type=="image"%}<span class="s7-photo ic-flist"></span>{%else%} <span class="s7-file ic-flist"></span>{%endif%}</div> 
						
						<div class="pull-left" style="padding-top: 7px;">	<a href="{{ihost}}userfiles/{{acs.user_id}}/pic/{{f.invatt_fname}}" {%if f.invatt_type=="image"%}class="venobox"{%else%}target="_blank"{%endif%} >{{f.invatt_fname}}</a></div>
						
						
						<div style="clear: both;"></div>

					</li>

				
				{%endfor%}
			</ul>				
	        
	        </div>
		</div>  
		{%endif%}      	
		   		
	</div>	  
	<div class="col-xs-12 col-md-3">
		<legend style="padding-bottom: 10px;">Account Standing</legend>

		<div class="widget widget-tile" style="margin-bottom: 5px;">
          <div class="data-info">
            <div data-toggle="counter" data-end="156" class="value">{{outstanding|number_format(2,acs.user_dsep,acs.user_tsep)}}</div>
            <div class="desc">Outstanding <span class="text-muted">{{acs.user_currency}}</span></div>
          </div>
          <div class="icon "><span class="s7-clock"></span></div>
        </div>	
 
 		<div class="widget widget-tile" style="margin-bottom: 5px;">
          <div class="data-info">
            <div data-toggle="counter" data-end="156" class="value">{{pastdue|number_format(2,acs.user_dsep,acs.user_tsep)}}</div>
            <div class="desc">Past Due <span class="text-muted">{{acs.user_currency}}</span></div>
          </div>
          <div class="icon "><span class="s7-attention text-danger"></span></div>
        </div>	
  
  		<div class="widget widget-tile">
          <div class="data-info">
            <div data-toggle="counter" data-end="156" class="value">{{paid|number_format(2,acs.user_dsep,acs.user_tsep)}}</div>
            <div class="desc">Paid <span class="text-muted">{{acs.user_currency}}</span></div>
          </div>
          <div class="icon "><span class="s7-piggy text-success"></span></div>
        </div>	
		<legend style="padding-bottom: 10px;">Invoice History</legend>
 		<div class="panel panel-default panel-borders">

	        <div class="panel-body">
		    
			        {%for h in history%}
			        <div class="history-box">
						
				        <small class="text-muted"><time datetime="{{h.invhist_date|date('Y-m-d')}}T{{h.invhist_date|date('H:i:s')}}" class="timeago"></time></small>
				        <br><span {%if h.invhist_action=="pay"%}class="text-success"{%endif%} style="font-weight: 600; font-size: 16px;">{{h.invact_desc}}</span>
				        {%if h.invhist_action=="pay"%}
				        <br><small><span class="text-muted">Amount</span> {{h.invhist_amount|number_format(2,acs.user_dsep,acs.user_tsep)}} {{h.invhist_currency}}</small>
				        {%endif%}
				        
				        {%if h.invhist_action=="email"%}
				        <br><small><span class="text-muted">to</span> {{h.invhist_email}} </small>
				        {%if h.invhist_cc%}
				         <br><small><span class="text-muted">cc</span> {{h.invhist_cc}} </small>
				        {%endif%}
				        {%endif%}
				        
			        </div>	
			        {%endfor%} 
		          
	        </div>
 		</div>    
    </div>
	
	
  </div>
</div>

     <div id="pay-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">Add Payment</h3>
          </div>
           
         <div class="modal-body">

	        <div role="alert" class="alert alert-danger pay-err" id="more-due" style="display: none;">
                    <span class="icon s7-close-circle"></span><strong>Oooops!</strong> Looks like pay amount is more then due amount.
            </div>
	         
	         	 <div class="row">
	         	 	<div class="col-xs-12">
			 	 	<div role="alert" class="alert alert-success alert-icon alert-border-color">
                    <div class="icon"><span class="s7-piggy" style="font-size: 24px;"></span></div>
                    <div class="message">
                      <strong>Invoices# {{inv.inv_num}}</strong> Total Due: {{inv.inv_total_due|number_format(2,acs.user_dsep,acs.user_tsep)}} {{inv.inv_currency}}
                    </div>
                  </div>		         	 	
	         	 	</div>
	         	 </div>
	         
				<form id="ftrans" class="form-horizontal">
					
		 		<input type="hidden" name="pay_inv_id" value="{{inv.inv_id}}">
		 		<input type="hidden" name="pay_client_id" value="{{inv.inv_client_id}}">
		 		<input type="hidden" name="pay_collab_id" value="{{inv.inv_collab_id}}">
		 		<input type="hidden" id="pay_due" value="{{inv.inv_total_due}}">
		 		<input type="hidden" name="pay_currency" value="{{inv.inv_currency}}">
		 		{#<input type="hidden" id="pay_rate" value="{{inv.inv_exchange_rate}}">	
		 		<input type="hidden" id="pay_due_base" value="{{inv.inv_total_due_base}}">#}
		 		
		 		<div class="form-group">
				<div class="col-xs-12 col-sm-4 " >
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-calendar bigger-110"></i>
						</span>

						<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="pay_date" class="form-control date-picker">
					</div>							
				</div>
 		
 		
				<div class="col-xs-12 col-sm-4">
					<div class="input-group">
						<span class="input-group-addon">{{inv.inv_currency}}</span>

						<input type="text" name="amount" id="pay_amount" value="{{inv.inv_total_due}}" readonly="" class="form-control dec" required="" data-errorbox="#errpayamnt" data-msg-required="{{lng_transactions.amount_req}}">
						
					</div>	

					<div class="am-checkbox">
                      <input type="checkbox"  checked="checked" id="payinfull">
                      <label for="payinfull">Pay in Full</label>
                    </div>
                    <div id="errpayamnt" style="color:red;"></div>
				</div>
				
				<div class="col-xs-12 col-sm-4 " >
					<select name="method" id="method" class="form-control">
						{%for pm in paymethod%}
						<option value="{{pm.paymethod_method}}">{{pm.paymethod_name}}</option>
						{%endfor%}
					</select>
				</div>
			 		
		 		</div>
		 		
		 		<div class="form-group" {%if acs.user_currency==inv.inv_currency%}style="display: none;"{%endif%}>
		 			<label class="col-sm-3 control-label no-padding-left" style="text-align: left;" >Base Currency</label>
		 			<div class="col-xs-12 col-sm-5">
					<div class="input-group">
						<span class="input-group-addon">{{inv.inv_currency}}/{{acs.user_currency}}</span>

						<input type="text" name="pay_rate" id="pay_rate" autocomplete="off" value="{{inv.inv_exchange_rate}}" class="form-control xr" required="" data-errorbox="#errrate" data-msg-required="{{lng_common.fr}}">
						
					</div>
					 <div id="errrate" style="color:red;"></div>	
		 			</div>
		 			
		 		
		 			<div class="col-xs-12 col-sm-4">
		 				<div class="input-group">
						<span class="input-group-addon">{{acs.user_currency}}</span>

						<input type="text" name="pay_due_base" id="pay_due_base" value="{{inv.inv_total_due_base}}" readonly="" class="form-control dec">
						
						</div>
						
		 			</div>
		 			
		 		</div>
		 		
		 		<div class="form-group">
			 		
					<div class="col-sm-12">
						<input type="text" class="form-control" id="pay_notes"  placeholder="Payment Notes"  name="pay_notes">
						
					</div>
			 		
		 		</div>	
		 		</form>	
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" id="doAddTrans"   class="btn btn-primary  btn-md ">Add Payment</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>
          </div>
        </div>
      </div>
     </div>  

     <div id="send-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">Email Invoice</h3>
          </div>
           
         <div class="modal-body">
	         
				<form id="femail" class="form-horizontal">
					
		 		<input type="hidden" name="email_inv_id" value="{{inv.inv_id}}">
			 		
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">Email </label>
				    <div class="col-sm-8">
					    <input type="email" name="send_email" value="{{sendemail}}" required=""  class="form-control" data-errorbox="#erremail" data-msg-required="{{lng_invoices.email_req}}" data-msg-email="{{lng_common.nve}}" >
					    <div id="erremail" style="color:red;"></div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">CC:Email  </label>
				    <div class="col-sm-8">
					    <input type="email" name="send_email_cc" value="{{sendemailcc}}" class="form-control" data-errorbox="#erremailcc"  data-msg-email="{{lng_common.nve}}">
					    <div id="erremailcc" style="color:red;"></div>
					</div>
				</div>
		 		</form>	
	               
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" id="doSend"   class="btn btn-primary  btn-md ">Send</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>
          </div>
         
         
         
        </div>
      </div>
     </div>  

     <div id="addr-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_invoices.chaddr}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="bill-adr-box">



		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button"   class="btn btn-primary doChAddr btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  

     <div id="my-addr-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_invoices.chaddr}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="my-bill-adr-box">



		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button"   class="btn btn-primary doChMyAddr btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left" data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  


{%endblock%}


{%block js%}
<script src="/assets/lib/mask/jquery.mask.js"></script>	
<script src="/assets/lib/livequery/jquery.livequery.js"></script>
<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>
<script src="/assets/lib/venobox/venobox.min.js"></script>	
<script src="/js/dev/invoices_view.js?v={{hashver}}"></script>
 
{%endblock%}

