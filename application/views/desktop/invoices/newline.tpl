<tr id="line{{id}}">
	<td class="description td-a-t" >
		<input type="text" name="invrow_srv[]" placeholder="{{lng_invoices.ph_srv}}" id="name{{id}}" data-id="{{id}}" data-errorbox="#errsrvline{{id}}" data-msg-required="{{lng_invoices.srv_req}}"  class="form-control td-b linefilter b-r-r-0 b-b-l-r-0 " >
		
		<textarea id="irdesc{{id}}" style="resize: none;" name="invrow_desc[]" rows="2" placeholder="{{lng_invoices.ph_desc}}" class="form-control td-b td-t-n-b b-t-r-0 "></textarea>
		<div id="errsrvline{{id}}" style="color:red;"></div>
	</td>
	<td class="hours td-a-t" style="text-align: center;">
		<input type="text" id="irrate{{id}}" data-id="{{id}}" name="invrow_rate[]" class="form-control td-b td-l-n-b dec b-l-r-0 b-r-r-0" placeholder="0.00">
		
	</td>
	<td class="hours td-a-t" >
		<input type="text" value="1" id="irqty{{id}}" data-id="{{id}}" name="invrow_qty[]" class="form-control td-b td-l-n-b td-r-n-b dec b-l-r-0 b-r-r-0" >
		
	</td>

	<td class="hours td-a-t" >
		<input type="text" value="0.00" id="irtax1{{id}}" data-id="{{id}}" name="invrow_tax1[]" class="form-control td-b  tax b-l-r-0 b-r-r-0" >
		<input type="text" value="0.00" id="irtax2{{id}}" data-id="{{id}}" name="invrow_tax2[]" class="form-control td-b td-t-n-b tax b-t-r-0" >
		<input type="hidden" id="taxcash1{{id}}" value="0" class="tax1" data-id="{{id}}" name="invrow_tax1_cash[]"> 
		<input type="hidden" id="taxcash2{{id}}" value="0" class="tax2" data-id="{{id}}" name="invrow_tax2_cash[]"> 
	</td>

	<td class="amount td-a-t" >
		<input type="text" value="0.00" name="invrow_total[]" id="irtotal{{id}}" data-id="{{id}}" class="form-control td-b td-l-n-b i-txt-r linetotal pull-left b-l-r-0" readonly=""  >
		<button class="btn btn-space btn-default text-danger delline" type="button" data-id="{{id}}" style="padding-top: 7px !important; border: none !important; position: absolute; background: none !important;  padding-left: 6px;" ><span class="s7-close-circle" style="font-size:28px; font-weight: 700;"></span></button>

	</td>
	</tr>
