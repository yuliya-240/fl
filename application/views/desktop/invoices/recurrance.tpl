<div class="panel panel-default panel-borders">

	<div class="panel-heading">{{lng_invoices.rec}} <span class="pull-right"><input type="checkbox" value="1"  class="pub "  name="recurrance"></span></div>
    <div class="panel-body">
      <div id="rec-off" class="text-center">
        	<p>{{lng_invoices.rec_off}}</p>
      </div>

      <div id="rec-on" style="display: none;">
        	

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" >{{lng_invoices.next_date_issue}}</label>
					
						<div class="col-sm-3" >
							<div class="input-group">
								<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="date" class="form-control date-picker">
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>							
						</div>
				</div>


				<div class="form-group">
				 	<label class="col-sm-3 control-label no-padding-right"> {{lng_invoices.freq}}</label>
				 	<div class="col-sm-4">
				 			<select id="recOptions" name="recoptions"  class="form-control">
					
								<option value="w">{{lng_invoices.weekly}}</option>
								<option value="m">{{lng_invoices.monthly}}</option>
								<option value="y">{{lng_invoices.yearly}}</option>
				 			
				 			</select>
				 		</div>
				</div>	

				<div id="recurrenceOptions" >
						
						<div id="weekOptionBox" class="optBox" >


		                    <div class="form-group">
		                      <label class="col-sm-3 control-label">{{lng_invoices.whichday}}</label>
		                      <div class="col-sm-9">
		                        <div class="am-checkbox inline">
		                          <input id="check1" name="weekday[]" value="1" checked="" type="radio">
		                          <label for="check1">{{lng_common.mon_short}}</label>
		                        </div>
		                        <div class="am-checkbox inline">
		                          <input id="check2" name="weekday[]" value="1" type="radio">
		                          <label for="check2">{{lng_common.tue_short}}</label>
		                        </div>
		                        <div class="am-checkbox inline">
		                          <input id="check3" name="weekday[]" value="1" type="radio">
		                          <label for="check3">{{lng_common.wed_short}}</label>
		                        </div>

		                        <div class="am-checkbox inline">
		                          <input id="check4" name="weekday[]" value="1" type="radio">
		                          <label for="check4">{{lng_common.thu_short}}</label>
		                        </div>

		                        <div class="am-checkbox inline">
		                          <input id="check5" name="weekday[]" value="1" type="radio">
		                          <label for="check5">{{lng_common.fri_short}}</label>
		                        </div>

		                        <div class="am-checkbox inline">
		                          <input id="check6" name="weekday[]" value="1" type="radio">
		                          <label for="check6">{{lng_common.sat_short}}</label>
		                        </div>
		                        <div class="am-checkbox inline">
		                          <input id="check7" name="weekday[]" value="1" type="radio">
		                          <label for="check7">{{lng_common.sun_short}}</label>
		                        </div>


		                      </div>
		                    </div>
							
						</div>
						
						<div id="monthOptionBox" class="optBox" style="display:none;">
							<div class="form-group ">
								<label class="col-sm-3 control-label">{{lng_invoices.whichday}}</label>
								<div class="col-sm-2">
								<select class="form-control" name="month_day" >
									{%for j in 1..30%}
										<option style="padding: 3px 3px 3px 7px" value="{{j}}">{{j}}</option>
									{%endfor%}
								</select>
								</div>
								
							
							</div>
						
											
						</div>
						
						<div id="yearOptionBox" class="optBox" style="display:none;">
							<div class="form-group ">
								<label class="col-sm-3 control-label">{{lng_invoices.whichday}}</label>
								<div class="col-sm-3">
								<select class="form-control" name="year_month">
									{%for m in months%}
									<option value="{{m.mon_num}}">{{m.mon_full_name}}</option>
									{%endfor%}
								</select>
								</div>
								<div class="col-sm-2">
								<select class="form-control" name="year_day" >
									<option value="first">{{lng_invoices.first}}</option>
									<option value="15">15</option>
									<option value="last">{{lng_invoices.last}}</option>
								</select>
								</div>
								
							</div>
							
												
						</div>

						
						
					</div>			        	

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" >{{lng_invoices.endofrec }}</label>
						<div class="col-sm-3" >
							<select class="form-control" name="inv_rec_end" id="ir">
								<option value="1">{{lng_invoices.ne }}</option>
								<option value="2">{{lng_invoices.sad }}</option>
								<option value="3">{{lng_invoices.sac }}</option>

							</select>	
						</div>
							
						<div class="col-sm-3" style="display: none;" id="rsd">
							<div class="input-group">
								<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="date" class="form-control date-picker">
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>							
						</div>

						<div class="col-sm-3 col-md-2" style="display: none;" id="rsc">
							
								<input type="text" name="inv_rec_stop_after_count" value="1" class="form-control">
						</div>

				</div>

	        	
        	
      </div>

    </div>
</div> 