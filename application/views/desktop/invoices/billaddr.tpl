<form id="fbilladdr" class="form-horizontal" role="form">
	<input type="hidden" name="billid" value="{{id}}">
	
	
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right">{{lng_common.name}} <span style="color: red;">*</span> </label>
	    <div class="col-sm-9">
		    <input type="text"  data-errorbox="#errname4" data-msg-required="{{lng_clients.name_req}}"  name="billing_name"   id="billing_name" required=""  value="{{name}}" class="form-control" >
			<div id="errname4" style="color:red;"></div>
		</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.country}}</label>
	 <div class="col-sm-5">
		<select id="country2" class="form-control" name="billing_country" >
			{%for cl in countryList%}
			<option {%if cl.country_iso==country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
			{%endfor%}
		</select>
	</div>
	</div>


	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.state}}</label>
	<div class="col-sm-5">
	 {%if  statesList%}
		<select title="State"  id="region2"  name="billing_state" class="form-control">
			{%for sl in statesList%}
			<option {%if sl.state_code == state%}selected{%endif%} value="{{sl.state_code}}">{{sl.state_name}}</option>
			{%endfor%}
		</select>
		<input type="text" style="display:none;" id="inputstate2" title="State" class="form-control"  name=""  />
	{%else%}
		<input type="text" id="inputstate2" title="State" class="form-control"  name="billing_state"  value="{{state}}">
		<select title="State"  style="display:none;"  id="region2"  name="billing_state" class="form-control">
		</select>
	{%endif%}	
	</div>
	</div>
	
	
	


	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right">{{lng_common.address}} </label>
	    <div class="col-sm-9">
		    <input type="text" name="billing_street" value="{{street}}" class="form-control" >
		</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.city}} </label>
	<div class="col-sm-5">

		<input type="text" name="billing_city"  value="{{city}}" class="form-control" >
	</div>
	</div>
	
	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.zip}} </label>
	<div class="col-sm-3">
		<input type="text"  class="form-control" value="{{zip}}" name="billing_zip"  > 						
	</div>
	</div>
	
	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">Email</label>
	<div class="col-sm-7">
		<input type="text"  class="form-control" {%if iscollab%}readonly=""{%endif%} value="{{email}}" name="billing_email"  > 						
	</div>
	</div>
	

		
</form>
