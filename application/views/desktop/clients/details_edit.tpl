<form id="fdata" class="form-horizontal">
	<input type="hidden" name="id" value="{{client.client_id}}">
    <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_clients.tbl_name}}<span style="color: red;">*</span></label>
      <div class="col-sm-9">
        <input class="form-control" name="client_name" value="{{client.client_name}}" data-errorbox="#errname" data-msg-required="{{lng_clients.name_req}}"  required="" placeholder="{{lng_projects.ph_name}}" type="text" >
        <div id="errname" style="color:red;"></div>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label">{{lng_first.form_currency}}</label>
      <div class="col-sm-6">
        <select id="currency" name="client_currency" class="form-control">
           {%for cul in currencyList%}
           <option {%if client.client_currency==cul.currency_iso%}selected{%endif%} value="{{cul.currency_iso}}"> {{cul.currency_iso}} {{cul.currency_name}}</option>
           {%endfor%}
        </select>
      </div>
    </div>
   
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right">{{lng_clients.dis}} </label>
	    <div class="col-sm-4">
			<div class="input-group xs-mb-15">
              <input class="form-control" value="{{client.client_discount}}" name="client_discount" id="dis" type="text"><span class="input-group-addon right-rounded">%</span>
            </div>
		</div>
	</div>
	
	
	
</form>	
<hr>
<a href="#" id="btn-cancel-details" style="margin-bottom: 5px;" class="btn btn-lg btn-space btn-default">{{lng_common.btn_cancel}}</a>
<button id="btn-save-details" type="button" style="margin-bottom: 5px;" class="btn btn-lg btn-space pull-right btn-primary">{{lng_common.btn_save}}</button>
