{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs"></th>
			<th>ID</th>
			<th>{{lng_clients.tbl_name}}</th>
			<th>{{lng_clients.tbl_addr}}</th>
			<th>{{lng_clients.tbl_contact}}</th>
			<th width="1%"></th>
		</tr>
	</thead>
	<tbody>
		{%for r in records%}
		<tr>
			<td class="text-center hidden-xs" style="vertical-align:middle;">
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.client_id}}" class="check" name="row[]" value="{{r.client_id}}">
	              <label for="chk{{r.client_id}}"></label>
	            </div>
			</td>
			<td class="tbl-pointer g-l" data-id="{{r.client_id}}" data-go="/clients/view/{{r.client_id}}">{{r.client_id}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.client_id}}" data-go="/clients/view/{{r.client_id}}">{{r.client_name}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.client_id}}" data-go="/clients/view/{{r.client_id}}">{{r.client_street}} {{r.client_state}} {{r.client_city}} {{r.client_zip}} {{r.country_printable_name}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.client_id}}" data-go="/clients/view/{{r.client_id}}">
				{%if r.client_email%}<p><small >Email:</small> <a href="emailto:{{r.client_email}}">{{r.client_email}}</a></p>{%endif%}
				{%if r.client_ccemail%}<p><small >Email CC:</small> <a href="emailto:{{r.client_ccemail}}">{{r.client_ccemail}}</a></p>{%endif%}
				{%if r.client_phone%}<p><small >Phone:</small> <a href="tel:{{r.client_phone}}">{{r.client_phone}}</p>{%endif%}
				{%if r.client_skype%}<p><small >Skyp:</small> <a href="tel:{{r.client_skype}}">{{r.client_skype}}</p>{%endif%}	
			</td>
			<td class="tbl-pointer" data-id="{{r.client_id}}">
				<a href="#" class="g-l" data-go="/clients/view/{{r.client_id}}" data-id="{{r.client_id}}"><i class="material-icons">visibility</i></a>
			</td>
		</tr>
		{%endfor%}
	</tbody>
</table>				
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
