<table id="prj-details-tbl" class="table">
    <tr>
        <td width="20%"><small class="text-muted">Email</small></td>
        <td >{%if client.client_email%}{{client.client_email}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>
        
    </tr>

    <tr>
        <td ><small class="text-muted">CC Email</small></td>
        <td >
	        
	        {%if client.client_ccemail%}{{client.client_ccemail}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}
        </td>
        
    </tr>

    <tr>
        <td ><small class="text-muted">{{lng_common.phone}}</small></td>
        <td >
	        
	        
	        {%if client.client_phone%}{{client.client_phone}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}
        </td>
        
    </tr>
    <tr>
        <td><small class="text-muted">Skype</small></td>
        <td>
	        
	         {%if client.client_skype%}{{client.client_skype}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}
        </td>
        
    </tr>

</table>    
