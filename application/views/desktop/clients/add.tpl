{% extends skin~"/common/root.tpl" %}
{%block headcss%}
    
{%endblock%}

{%block content%}

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_clients.title}} </h3>
</div>	


<div class="main-content">
<div class="row">
	<div class=" col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
		<form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<div class="panel panel-default panel-borders">
	        <div class="panel-heading">
		        <span class="title">{{lng_clients.details}}</span>
		    </div>

	        <div class="panel-body">
		        
                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_clients.tbl_name}}<span style="color: red;">*</span></label>
                  <div class="col-sm-9">
                    <input class="form-control" name="client_name" data-errorbox="#errname" data-msg-required="{{lng_clients.name_req}}"  required="" placeholder="{{lng_projects.ph_name}}" type="text">
                    <div id="errname" style="color:red;"></div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_first.form_currency}}</label>
                  <div class="col-sm-6">
                    <select id="currency" name="client_currency" class="form-control">
                       {%for cul in currencyList%}
                       <option {%if acs.user_currency==cul.currency_iso%}selected{%endif%} value="{{cul.currency_iso}}"> {{cul.currency_iso}} {{cul.currency_name}}</option>
                       {%endfor%}
                    </select>
                  </div>
                </div>
               
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_clients.dis}} </label>
				    <div class="col-sm-4 col-md-3">
						<div class="input-group xs-mb-15">
                          <input class="form-control" value="0" name="client_discount" id="dis" type="text"><span class="input-group-addon">%</span>
                        </div>
					</div>
				</div>

	        </div>
		</div>    	
		      
		      
		<div class="panel panel-default panel-borders">
	        <div class="panel-heading">
		        <span class="title">{{lng_clients.tbl_contact}}</span>
		    </div>

	        <div class="panel-body">
		        
                
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.address}} </label>
				    <div class="col-sm-9">
					    <input type="text" name="client_street"  class="form-control" >
					</div>
				</div>

				<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right">{{lng_common.city}} </label>
				<div class="col-sm-5">

					<input type="text" name="client_city"   class="form-control" >
				</div>
				</div>
				
				<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right">{{lng_common.zip}} </label>
				<div class="col-sm-3">
					<input type="text"  class="form-control" name="client_zip"  > 						
				</div>
				</div>
				
				
				<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right">{{lng_common.state}}</label>
				<div class="col-sm-5">
				 {%if  statesList%}
					<select title="State"  id="region"  name="client_state" class="form-control">
						{%for sl in statesList%}
						<option {%if sl.state_code == acs.co_state%}selected{%endif%} value="{{sl.state_code}}">{{sl.state_name}}</option>
						{%endfor%}
					</select>
					<input type="text" style="display:none;" id="inputstate" title="State" class="form-control"  name=""  />
				{%else%}
					<input type="text" id="inputstate" title="State" class="form-control"  name="client_state"  value="{{acs.co_state}}">
					<select title="State"  style="display:none;"  id="region"  name="client_state" class="form-control">
					</select>
				{%endif%}	
				</div>
				</div>
				
				
				
				<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right">{{lng_common.country}}</label>
				 <div class="col-sm-5">
					<select id="country" class="form-control" name="client_country" >
						{%for cl in countryList%}
						<option {%if cl.country_iso==acs.user_country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
						{%endfor%}
					</select>
				</div>
				</div>

				<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right ">Email</label>
				<div class="col-sm-6">
					<input type="email"  name="client_email" class="form-control"> 						
				</div>
				</div>

				<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right ">{{lng_common.ccemail}}</label>
				<div class="col-sm-6">
					<input type="email"  name="client_ccemail" class="form-control"> 						
				</div>
				</div>



				<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right ">{{lng_common.phone}}</label>
				<div class="col-sm-4">

					<input type="text"  name="client_phone" class="form-control " > 
				</div>
				</div>


				<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right ">Skype</label>
				<div class="col-sm-4">
					<input type="text"  name="client_skype" class="form-control"> 						
				</div>
				</div>

			
	
	        </div>
		</div>
		
		<button type="submit" id="doSave" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
		<a href="#" class="btn btn-default btn-lg pull-left g-l" data-go="/clients">{{lng_common.btn_cancel}}</a>
		<div style="clear:both;margin-bottom: 80px;"></div>	
		
		</form>
		
	</div>
</div>	
</div>	
{%endblock%}



{%block js%}

   <script src="/assets/lib/mask/jquery.mask.min.js?v={{hashver}}"></script>
   <script src="/js/dev/clients_add.js?v={{hashver}}"></script>
{%endblock%}
