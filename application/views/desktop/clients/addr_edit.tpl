<form id="fdata" class="form-horizontal">
	<input type="hidden" name="id" value="{{client.client_id}}">
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right">{{lng_common.address}} </label>
	    <div class="col-sm-9">
		    <input type="text" name="client_street" value="{{client.client_street}}" class="form-control" >
		</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.city}} </label>
	<div class="col-sm-8">

		<input type="text" name="client_city"  value="{{client.client_city}}" class="form-control" >
	</div>
	</div>
	
	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.zip}} </label>
	<div class="col-sm-4">
		<input type="text"  class="form-control" value="{{client.client_zip}}" name="client_zip"  > 						
	</div>
	</div>
	
	
	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.state}}</label>
	<div class="col-sm-7">
	 {%if  statesList%}
		<select title="State"  id="region"  name="client_state" class="form-control">
			{%for sl in statesList%}
			<option {%if sl.state_code == client.client_state%}selected{%endif%} value="{{sl.state_code}}">{{sl.state_name}}</option>
			{%endfor%}
		</select>
		<input type="text" style="display:none;" id="inputstate" title="State" class="form-control"  name=""  />
	{%else%}
		<input type="text" id="inputstate" title="State" class="form-control"  name="client_state"  value="{{client.client_state}}">
		<select title="State"  style="display:none;"  id="region"  name="client_state" class="form-control">
		</select>
	{%endif%}	
	</div>
	</div>
	
	
	
	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.country}}</label>
	 <div class="col-sm-6">
		<select id="country" class="form-control" name="client_country" >
			{%for cl in countryList%}
			<option {%if cl.country_iso==client.client_country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
			{%endfor%}
		</select>
	</div>
	</div>

</form>	

<hr>
<a href="#" id="btn-cancel-addr" style="margin-bottom: 5px;" class="btn btn-lg btn-space btn-default">{{lng_common.btn_cancel}}</a>
<button id="btn-save-addr" type="button" style="margin-bottom: 5px;" class="btn btn-lg btn-space pull-right btn-primary">{{lng_common.btn_save}}</button>
