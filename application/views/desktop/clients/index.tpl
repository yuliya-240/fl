{% extends skin~"/common/root.tpl" %}
{%block headcss%}
    <style>
.input-group-btn .btn {
    height: 42px;
}	    
	</style>    
{%endblock%}

{%block content%}
<input type="hidden" id="_pdt" value="{{lng_clients.del_bundle_title}}">
<input type="hidden" id="_pdm" value="{{lng_clients.del_bundle_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="_no" value="{{lng_common.btn_no}}">

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_clients.title}} 
		 <a href="#" class="btn btn-head btn-success pull-right g-l" data-go="/clients/add" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_clients.btn_add}}">{{lng_clients.btn_add}}</a>
	</h3>
</div>	


<div class="main-content">
<div class="row">
	<div class="col-xs-12">
		
		<form id="fsearch" style="border-radius: 0px;" class="form-horizontal group-border-dashed">

			
				<div class="form-group">
				
					<div class="col-sm-8">
						<div class="input-group">
							<span class="input-group-addon srch-left-addon">
								<span class="s7-search" style="font-size: 24px;"></span>
							</span>
                          <input class="form-control" id="s" placeholder="{{lng_clients.search_ph}}" {%if searchstr%}value="{{searchstr}}"{%endif%} type="text" style="border-width:1px; border-bottom-right-radius: 3px; border-top-right-radius: 3px;" ><span class="input-group-btn"></span>
                    

                        </div>
					</div>    				
				</div>		
		</form>
		
		
		<form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<div class="panel panel-default panel-borders">

	        <div class="panel-body">
			
				<div id="d-content">
		
				</div>
	
	        </div>
		</div>
		</form>
		
	</div>
</div>	
</div>	
{%endblock%}

{%block footer%}
<div id="megafooter">
	
</div>	
{%endblock%}

{%block js%}
   <script src="/js/dev/clients.js?v={{hashver}}"></script>
{%endblock%}
