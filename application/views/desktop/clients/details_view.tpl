<table id="prj-details-tbl" class="table">
    <tr>
        <td width="40%"><small class="text-muted">{{lng_clients.tbl_name}}</small></td>
        <td id="tbl-prj-name">{{client.client_name}}</td>
        
    </tr>
    <tr>
        <td><small class="text-muted">{{lng_clients.dis}}</small></td>
        <td >{{client.client_discount}} %</td>
        
    </tr>
    <tr>
        <td><small class="text-muted">{{lng_first.form_currency}}</small></td>
        <td>{{client.client_currency}} {{client.currency_name}}</td>
        
    </tr>

</table>    
