<form id="fdata" class="form-horizontal">
	<input type="hidden" name="id" value="{{client.client_id}}">
	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right ">Email</label>
	<div class="col-sm-9">
		<input type="email"  name="client_email" value="{{client.client_email}}" class="form-control"> 						
	</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right ">{{lng_common.ccemail}}</label>
	<div class="col-sm-9">
		<input type="email"  name="client_ccemail" value="{{client.client_ccemail}}" class="form-control"> 						
	</div>
	</div>



	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right ">{{lng_common.phone}}</label>
	<div class="col-sm-6">

		<input type="text"  name="client_phone" value="{{client.client_phone}}" class="form-control " > 
	</div>
	</div>

	{#<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right ">{{lng_common.mob}}</label>
	<div class="col-sm-4">
		<input type="text"  name="client_mobile" value="{{client.client_mobile}}" class="form-control"> 						
	</div>
	</div>#}

	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right ">Skype</label>
	<div class="col-sm-9">
		<input type="text"  name="client_skype" value="{{client.client_skype}}" class="form-control"> 						
	</div>
	</div>
	
</form>
	
<hr>
<a href="#" id="btn-cancel-contact" style="margin-bottom: 5px;" class="btn btn-lg btn-space btn-default">{{lng_common.btn_cancel}}</a>
<button id="btn-save-contact" type="button" style="margin-bottom: 5px;" class="btn btn-lg btn-space pull-right btn-primary">{{lng_common.btn_save}}</button>
