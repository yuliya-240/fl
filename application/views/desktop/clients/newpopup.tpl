		<form id="fnewcustomerpopup" class="form-horizontal" role="form">
						
					    <div class="form-group">
							<label class="col-sm-3 control-label no-padding-right">{{ lng_clients.cust_name }}<span class="text-danger">*</span></label>

							<div class="col-sm-9">
								<input type="text" class="form-control" id="company_name" required="required"   name="company_name">
							</div>
						</div>

						<input type="hidden" name="ctype" value="{{ctype}}">
						<input type="hidden" value="1" name="status">
						<input type="hidden"  name="discount" id="discount" value="0"> 
						<input type="hidden"  name="notes" value="">

						<div class="form-group">
					    <label class="col-sm-3 control-label no-padding-right">Email </label>
					    <div class="col-sm-5">
						    <input type="email" name="email" class="form-control"  id="email">
						</div>
						</div>
						

						<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{ lng_clients.country }}</label>
						 <div class="col-sm-5">
							<select id="country" class="form-control" name="country" >
								{%for cl in countryList%}
								<option {%if cl.country_iso==acs.co_country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
								{%endfor%}
							</select>
						</div>
						</div>
					
					
						<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{ lng_clients.state }}</label>
						<div class="col-sm-5">
						 {%if  statesList%}
							<select title="State"  id="region"  name="state" class="form-control">
								{%for sl in statesList%}
								<option {%if sl.state_code == acs.co_state%}selected{%endif%} value="{{sl.state_code}}">{{sl.state_name}}</option>
								{%endfor%}
							</select>
							<input type="text" style="display:none;" id="inputstate" title="State" class="form-control"  name=""  />
						{%else%}
							<input type="text" id="inputstate" title="State" class="form-control"  name="state"  value="{{acs.co_state}}">
							<select title="State"  style="display:none;"  id="region"  name="state" class="form-control">
							</select>
						{%endif%}	
						</div>
						</div>
				
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right">{{ lng_clients.adr }}</label>
						    <div class="col-sm-9">
							    <input type="text" name="street1"  class="form-control"   id="street1">
							</div>
						</div>

					
						<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{ lng_clients.city }} </label>
						<div class="col-sm-5">

							<input type="text" name="city" id="city" value="{{acs.co_city}}"   class="form-control" >
						</div>
						</div>
						
						<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{ lng_clients.zip }} </label>
						<div class="col-sm-3">
							<input type="text"  class="form-control" name="zip"  value="{{acs.co_zip}}" id="zip" > 						
						</div>
						</div>

						<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{ lng_clients.ph }}</label>
						<div class="col-sm-5">

							<input type="text"  name="phone" class="form-control" > 
						</div>
						</div>

						<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{ lng_clients.mb_ph }}</label>
						<div class="col-sm-5">
							<input type="text"  name="mobile" class="form-control"> 						
						</div>
						</div>
						
						<input type="hidden" name="custom_notes" value="">
		
		</form>
