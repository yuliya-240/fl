{% extends skin~"/common/root.tpl" %}
{%block headcss%}

<style>
.skeleton-item {
    background-color: #ccc;
    border-radius: 3px;
    display: inline-block;
    height: 14px;
    margin-bottom: 4px;
    margin-left: 16px;
    width: 50%;	
	}
</style>	

{%endblock%}

{%block content%}
<input type="hidden" id="client_id" value="{{client.client_id}}">

<input type="hidden" id="_pdt" value="{{lng_clients.del_title}}">
<input type="hidden" id="_pdm" value="{{lng_clients.del_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="_no" value="{{lng_common.btn_no}}">


<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{client.client_name}} 

     <a href="#" data-id="{{client.client_id}}" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.btn_del}}"  class="btn btn-head btn-danger pull-right del">{{lng_common.btn_del}}</a>

      <a href="#" data-go="/clients" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.back_list}}" class="btn btn-head btn-space btn-dark pull-right g-l"><i class="material-icons btn-head-s">arrow_back</i></a>
		
	</h3>
</div>	

<div class="main-content" style="margin-bottom: 50px;">
	 <div class="row">
		 <div class="col-md-5">
			 
			<div class="panel panel-default panel-borders" id="details-box" style="margin-bottom:5px;">
	        <div class="panel-heading">
	
		        <div class="tools" ><a  href="#" id="btn-edit-details" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_common.btn_edit}}"><span class="icon s7-note" ></span></a></div>
		     
		        <span class="title">{{lng_clients.details}}</span>
		    </div>
	        <div class="panel-body" >
		
				<div id="details-view">
					
				</div>
				<div id="details-edit" style="display: none;"></div>
		        
	        </div>
			</div>    

			<div class="panel panel-default panel-borders" style="margin-bottom:5px;">
	        <div class="panel-heading">
		
		        <div class="tools" ><a id="btn-edit-addr" href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_common.btn_edit}}"><span class="icon s7-note" ></span></a></div>

		        <span class="title">{{lng_clients.tbl_addr }}</span>
		    </div>
	        <div class="panel-body">
				<div id="addr-view">
				</div>
				<div id="addr-edit" style="display: none;"></div>
	        </div>
			</div>    


			<div class="panel panel-default panel-borders" style="margin-bottom:5px;">
	        <div class="panel-heading">
		        <div class="tools" ><a id="btn-edit-contact" href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_common.btn_edit}}"><span class="icon s7-note" ></span></a></div>
		        <span class="title">{{lng_clients.tbl_contact}}</span>
		    </div>
	        <div class="panel-body">
		
				<div id="contact-view"></div>
				<div id="contact-edit" style="display: none;"></div>
	
	        </div>
			</div>    

		 </div>
	 </div>

</div>

{%endblock%}
{%block js%}
   <script src="/assets/lib/mask/jquery.mask.min.js?v={{hashver}}"></script>
   <script src="/js/dev/clients_view.js?v={{hashver}}"></script>
{%endblock%}
