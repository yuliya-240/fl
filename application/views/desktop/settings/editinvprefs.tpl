<form id="fpref" class="form-horizontal">
<div class="form-group">
  <label class="col-sm-5 control-label">{{lng_settings.tit_inv_num}}<span style="color: red;">*</span></label>
  <div class="col-sm-4">
    <input class="form-control inum" name="set_inv_number_start" data-errorbox="#errname-inv-num" data-msg-required="Start Number is required"  required="" value="{{acs.set_inv_number_start}}" type="text">
    <div id="errname-inv-num" style="color:red;"></div>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">{{lng_settings.tit_est_num}}<span style="color: red;">*</span></label>
  <div class="col-sm-4">
    <input class="form-control inum" name="set_est_number_start" data-errorbox="#errname-est-num" data-msg-required="Start Number is required"  required="" value="{{acs.set_est_number_start}}"  type="text">
    <div id="errname-est-num" style="color:red;"></div>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">{{lng_settings.past_d_terms}}<span style="color: red;">*</span></label>
  <div class="col-sm-4">
    <select class="form-control" name="set_past_due_terms">
        {% for i in range(10, 60, 10) %}
    	<option {%if acs.set_past_due_terms==i%}selected=""{%endif%} value="{{i}}">{{i}} Days</option>
		{%endfor%}
    </select>    
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">{{lng_settings.balance}}<span style="color: red;">*</span></label>
  <div class="col-sm-6">
  <input type="checkbox" {%if acs.set_show_outstanding%}checked=""{%endif%} value="1"  class="pub"  name="set_show_outstanding">
  </div>
</div>


<div class="form-group">
  <label class="col-sm-5 control-label">{{lng_settings.stub}}<span style="color: red;">*</span></label>
  <div class="col-sm-6">
  <input type="checkbox" {%if acs.set_show_payment_stub%}checked=""{%endif%} value="1"  class="pub"  name="set_show_payment_stub">
  </div>
</div>
<hr>
<button type="submit" id="doSaveInvPref" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
<a href="#" id="cancelSaveInvPref" class="btn btn-default btn-lg pull-left cancel-ed" >{{lng_common.btn_cancel}}</a>

</form>  	
