{% extends skin~"/common/root.tpl" %}
{%block headcss%}
<link href="/assets/lib/switch/css/bootstrap-switch.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/growl.css">
<link rel="stylesheet" href="/css/profile.css" type="text/css"/>
<link rel="stylesheet" href="/assets/lib/colorpicker/jquery.simplecolorpicker.css" type="text/css"/>
<style>
.tab-content {
  background: transparent;
  padding: 0px;
  /*margin-bottom: 40px;*/
}   

.color{
	height:25px;
	}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
	
	border-bottom: 1px solid #ccc;
	color: #000;
	background-color: #c8c8c8;
	
	
}

.nav-tabs > li{
	margin-bottom: 1px;
	}

</style>   
{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_settings.title}}</h3>
</div>	

<div class="main-content">


          <div class="tab-container">
            <ul class="nav nav-tabs" style="border: 1px solid #ccc;">
              <li class="{{topInvoices}}"><a href="#inv" data-toggle="tab">{{lng_settings.tab_invoices}}</a></li>
               <li class="{{topCalendar}}" ><a href="#cal" data-toggle="tab">Calendar</a></li>
              <li class="{{topPayments}}" ><a href="#pay" data-toggle="tab">Online Payments</a></li>
            </ul>
            <div class="tab-content">
                <div id="inv" class="tab-pane cont {{topInvoices}}">
					<div class="row" style="margin-top: 10px;">	    
						<div class="col-sm-6">
				
							<div class="panel panel-default panel-borders">
				                <div class="panel-heading">
					            <div class="tools"><a href="#" id="edit-inv-pref"><span class="icon s7-note"></span></a></div>   
					            <span class="title ">Invoice & Estimate Preferences</span>
					            </div>
				                
				                <div class="panel-body">
				
										<div class="table-responsive view-box" id="pref-view">
										{% include skin~"/settings/viewinvprefs.tpl" %}
										</div>	
									  	<div id="pref-edit" class="edit-box" style="display: none;">
									  	</div>  	
									  	 	
								  </div>               
				            </div>	
						   
				
							<div class="panel panel-default" id="interms-panel">
				                <div class="panel-heading">
					            <div class="tools"><a href="#" id="edit-inv-terms"><span class="icon s7-note"></span></a></div>   
					            <span class="title">Invoice Default Messages</span>
					                
				                </div>
				                <div class="panel-body">
										<div class="table-responsive view-box" id="def-terms-inv-view">
										{% include skin~"/settings/viewinvterms.tpl" %}
										</div>	
									  	<div id="def-terms-inv-edit" class="edit-box" style="display: none;">
									  	</div>  	
									
				                </div>
							</div>    				  		  
				
							<div class="panel panel-default" id="estterms-panel">
				                <div class="panel-heading">
					            <div class="tools"><a href="#" id="edit-est-terms"><span class="icon s7-note"></span></a></div>   
					            <span class="title">Estimate Default Messages</span>
					                
				                </div>
				                <div class="panel-body">
										<div class="table-responsive view-box" id="def-terms-est-view">
										{% include skin~"/settings/viewestterms.tpl" %}
										</div>	
									  	<div id="def-terms-est-edit" class="edit-box" style="display: none;">
									  	</div>  	
				                </div>
							</div>    				  		  
				               
				          
				              </div>
						<div class="col-sm-6">
							<div class="panel panel-default">
				                <div class="panel-heading">
					              
					            <span class="title ">Logo</span>
					            </div>
				                
				                <div class="panel-body">
					                <div id="logo-box">
					                <img {%if acs.set_logo=="nologo.png"%}src="/img/nologo.png"{%else%}src="/userfiles/{{acs.user_id}}/pic/pic_{{acs.set_logo}}"{%endif%} class="img-responsive center-block">
					                </div>
									<hr>
									<div id="containerpic">
									<button type="button" id="pickfiles" class="btn btn-primary btn-lg pull-right">Upload</button>
									
									<button type="button" id="delpic" class="btn btn-danger btn-lg pull-left cancel-ed" {%if acs.set_logo=="nologo.png"%}style="display:none;"{%endif%} >Delete</button>
									
									</div>
									<div  class="progress progress-striped active" id="progress1" style="display:none;">
										<div style="width: 0%;" id="progressSlide" class="progress-bar progress-bar-success"></div>
									</div>					
				
				                </div>
							</div> 
							   
										  		  
							
						</div>	
							

					</div>
                   
                   
                  </div>
                <div id="cal" class="tab-pane cont {{topCalendar}}">
					<div class="row" style="margin-top: 10px;">	 
						<div class="col-sm-4">
							<div class="panel panel-default">
				                <div class="panel-heading">
					            {#<div class="tools"><a href="#" id="edit-ev-color"><span class="icon s7-edit"></span></a></div>#}   
					            <span class="title ">Colors</span>
					            </div>
				                
				                <div class="panel-body">
				
										<div class="table-responsive view-box" id="ev-view-color">
										{% include skin~"/settings/viewevcolor.tpl" %}
										</div>	
									  	 	
								  </div>               
				            </div>	
						
						
						</div>
					</div>	
                </div>

                <div id="pay" class="tab-pane {{topPayments}}">
                    

                </div>
            </div>
            
          </div>

	
</div>	

{%endblock%}

{%block js%}


   <script src="/assets/lib/mask/jquery.mask.js"></script>	
   <script src="/assets/lib/colorpicker/jquery.simplecolorpicker.js"></script>	
   <script src="/assets/lib/switch/js/bootstrap-switch.min.js"></script>
   <script src="/assets/lib/bootstrap-growl/bootstrap-growl.min.js"></script>
   <script src="/assets/lib/plupload-2.3.1/js/plupload.full.min.js"></script>
   <script src="/js/dev/settings.js"></script>
    
{%endblock%}
