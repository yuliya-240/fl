<table class="table">
	<tr>
		<td>{{ lng_settings.terms }}</td>
		<td style="{%if not acs.set_inv_terms%}text-align: right;{%endif%}">
		
		{%if acs.set_inv_terms%}
			{{acs.set_inv_terms|nl2br}}
		{%else%}
		
			<span class="text-danger">{{ lng_settings.not_set }}</span>
		{%endif%}	
		</td>
	</tr>
	<tr>
		<td >{{ lng_settings.notes }}</td>
		<td {%if not acs.set_inv_notes%}style="text-align: right"{%endif%}>
		{%if acs.set_inv_notes%}
			{{acs.set_inv_notes|nl2br}}
		{%else%}
		
			<span class="text-danger">{{ lng_settings.not_set }}</span>
		{%endif%}	
			
			
		</td>
	</tr>
	</tr>

</table>  
