<form id="fbill" class="form-horizontal">
<div class="form-group">
  <label class="col-sm-4 control-label">{{ lng_settings.name }}<span style="color: red;">*</span></label>
  <div class="col-sm-8">
    <input class="form-control" name="set_billing_name" data-errorbox="#errname-bill-name" data-msg-required="Name is required"  required="" value="{{acs.set_billing_name}}" type="text">
    <div id="errname-bill-name" style="color:red;"></div>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-4 control-label">{{ lng_settings.adr }}</label>
  <div class="col-sm-8">
    <input class="form-control" name="set_billing_street"  value="{{acs.set_billing_street}}"  type="text">
    
  </div>
</div>

<div class="form-group">
  <label class="col-sm-4 control-label">{{ lng_settings.city }}</label>
  <div class="col-sm-5">
    <input class="form-control" name="set_billing_city"  value="{{acs.set_billing_city}}"  type="text">
   
  </div>
</div>

<div class="form-group">
  <label class="col-sm-4 control-label">{{ lng_settings.state }}</label>
  <div class="col-sm-5">
    <input class="form-control" name="set_billing_state"  value="{{acs.set_billing_state}}"  type="text">
   
  </div>
</div>

<div class="form-group">
  <label class="col-sm-4 control-label">{{ lng_settings.zip }}</label>
  <div class="col-sm-4">
    <input class="form-control" name="set_billing_zip"  value="{{acs.set_billing_zip}}"  type="text">
   
  </div>
</div>

<div class="form-group">
  <label class="col-sm-4 control-label">{{ lng_settings.country }}</label>
  <div class="col-sm-6">
   
 		<select id="country2" class="form-control" name="set_billing_country" >
			{%for cl in countryList%}
			<option {%if cl.country_iso==acs.set_billing_country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
			{%endfor%}
		</select>
  
  </div>
</div>


<hr>
<button type="submit" id="doSaveBillAddr" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
<a href="#" id="cancelSaveBillAddr" class="btn btn-default btn-lg pull-left cancel-ed" >{{lng_common.btn_cancel}}</a>

</form>  	
