<form id="fterms" class="form-horizontal">
<div class="form-group">
  <label class="col-sm-12">{{ lng_settings.terms }}</label>
  <div class="col-sm-12">
    <textarea class="form-control" name="set_inv_terms" rows="3">{{acs.set_inv_terms}}</textarea>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-12 ">{{ lng_settings.notes }}</label>
  <div class="col-sm-12">
    <textarea class="form-control" name="set_inv_notes" rows="3">{{acs.set_inv_notes}}</textarea>
  </div>
</div>

<hr>
<button type="submit" id="doSaveInvTerms" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
<a href="#" id="cancelSaveInvTerms" class="btn btn-default btn-lg pull-left" >{{lng_common.btn_cancel}}</a>

</form>  	
