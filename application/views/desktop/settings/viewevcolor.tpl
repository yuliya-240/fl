
<table class="table form">
	
	<tr>
		<td>{{lng_settings.event}}</td>
		<td>
			
		<select class="form-control color" id="set_event_color" >
			{%for c in colors%}
			<option {%if c.color_hex==acs.set_event_color%}selected=""{%endif%}  value="{{c.color_hex}}">{{c.color_hex}}</option>
			
			{%endfor%}
		</select>		
			
			
		</td>
	</tr>
	
	
	<tr>
		<td  width="30%" >{{lng_settings.task}}</td>
		<td  style="text-align: right;">
		<select class="form-control color" id="set_task_color" >
			{%for c in colors%}
			<option {%if c.color_hex==acs.set_task_color%}selected=""{%endif%}  value="{{c.color_hex}}">{{c.color_hex}}</option>
			
			{%endfor%}
		</select>		
		</td>
	</tr>

	<tr>
		<td>{{lng_settings.meeting}}</td>
		<td>
			
		<select class="form-control color" id="set_meeting_color" color="{{acs.set_meeting_color}}" >
			{%for c in colors%}
			<option {%if c.color_hex==acs.set_meeting_color%}selected=""{%endif%}  value="{{c.color_hex}}">{{c.color_hex}}</option>
			
			{%endfor%}
		</select>		
		</td>
	</tr>

	<tr>
		<td>{{lng_settings.phone}}</td>
		<td>
			
		<select class="form-control color" id="set_phone_color" >
			{%for c in colors%}
			<option {%if c.color_hex==acs.set_phone_color%}selected=""{%endif%}  value="{{c.color_hex}}">{{c.color_hex}}</option>
			
			{%endfor%}
		</select>		
		</td>
	</tr>



	<tr>
		<td>{{lng_settings.issue}}</td>
		<td>
			
		<select class="form-control color" id="set_issue_color" >
			{%for c in colors%}
			<option {%if c.color_hex==acs.set_issue_color%}selected=""{%endif%}  value="{{c.color_hex}}">{{c.color_hex}}</option>
			
			{%endfor%}
		</select>		
		</td>
	</tr>

	<tr>
		<td>{{lng_settings.milestone}}</td>
		<td>
			
		<select class="form-control color" id="set_milestone_color" >
			{%for c in colors%}
			<option {%if c.color_hex==acs.set_milestone_color%}selected=""{%endif%}  value="{{c.color_hex}}">{{c.color_hex}}</option>
			
			{%endfor%}
		</select>		
			
			
		</td>
	</tr>


</table>