<table class="table">
	<tr>
		<td>{{lng_settings.tit_inv_num}}</td>
		<td style="text-align: right;">{{acs.set_inv_number_start}}</td>
	</tr>
	<tr>
		<td >{{lng_settings.tit_est_num}}</td>
		<td style="text-align: right">{{acs.set_est_number_start}}</td>
	</tr>
	<tr>
		<td >{{lng_settings.past_d_terms}}</td>
		<td style="text-align: right">{{acs.set_past_due_terms}}</td>
	</tr>
	<tr>
		<td >{{lng_settings.balance}}</td>
		<td style="text-align: right">
  		{%if acs.set_show_outstanding%}
            {{lng_common.btn_yes}}
  			
  		{%else%}
            {{lng_common.btn_no}}
  		{%endif%}
  	</td>
	</tr>

	<tr>
		<td >{{lng_settings.stub}}</td>
		<td style="text-align: right">
  		{%if acs.set_show_payment_stub%}
            {{lng_common.btn_yes}}
  		{%else%}
            {{lng_common.btn_no}}
  		{%endif%}
  		
  	</td>
	</tr>

</table>  
