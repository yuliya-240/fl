<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{page_title}}</title>
    <link rel="stylesheet" type="text/css" href="/assets/lib/stroke-7/style.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/lib/jquery.nanoscroller/css/nanoscroller.css"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/assets/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/css/themes/theme-google.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/lib/sweetalert/sweetalert.min.css" type="text/css"/>
    <link rel="stylesheet" href="/css/app.css" type="text/css"/>
	<style>
		.error{
			color:red;
			}
	</style>	
  </head>
  <body>
    <div class="am-wrapper  am-nosidebar-left">

	<div id="process-loader" class="fade">
	    <div class="material-loader">
	        <svg class="circular" viewBox="25 25 50 50">
	            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
	        </svg>
	        <div class="message">{{lng_common.loader}}</div>
	    </div>
	</div>
	
	<nav class="navbar navbar-default navbar-fixed-top am-top-header" id="#top">
	    <div class="container-fluid">
	      <div class="navbar-header">
	        <div class="page-title"><span>{{page_title}}</span></div><a href="#" class="navbar-brand"></a>
	      </div>
	      
	      
	      <a href="/first/logout" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_common.exit}}" class="am-toggle-right-sidebar"> <span class="icon s7-power"></span></a>
	
	
	    </div>
	  </nav>

        <div class="page-head">
          <h2>{{lng_first.welcome}}</h2>
          <ol class="breadcrumb">

            <li class="active">{{lng_first.welcome_sub}}</li>
          </ol>
        </div>
        <div class="main-content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
				
				<div class="panel panel-default panel-borders">
                <div class="panel-heading"><span class="title">{{lng_first.panel_title}}</span></div>
                <div class="panel-body">
				
                    <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_name}}<span style="color: red;">*</span></label>
                      <div class="col-sm-6">
                        <input class="form-control" name="user_name" data-msg-required="{{lng_first.name_req}}"  required="" placeholder="{{lng_first.ph_name}}" type="text">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_username}} <span style="color: red;">*</span></label>
                      <div class="col-sm-6">
                        <input class="form-control" name="user_username" id="username" data-msg-minlength="{{lng_first.username_minlength}}" data-msg-required="{{lng_first.username_req}}" data-msg-remote="{{lng_first.user_exist}}" required="" placeholder="{{lng_first.ph_uname}}" type="text">
                        <div id="user-error-box"  style="display:none;color:red;"></div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_password}}<span style="color: red;">*</span></label>
                      <div class="col-sm-6">
                        <input  class="form-control" required="" data-msg-required="{{lng_first.pass_req}}" type="password" name="user_password">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_prof}}</label>
                      <div class="col-sm-6">
                        <select class="form-control" name="user_profession" >
								{%for p in proffesionsList%}
								<option  value="{{p.pro_id}}">{{p.pro_name}}</option>
								{%endfor%}
	                        
                        </select>
                      </div>
                    </div>

                </div>
				</div>

				<div class="panel panel-default panel-borders">
                <div class="panel-heading"><span class="title">{{lng_first.panel_title_pref}}</span></div>
                <div class="panel-body">

                    <div class="form-group">
                      <label class="col-sm-3 control-label"></label>
                      <div class="col-sm-6">
                        <h3 id="dateExample" class="text-warning ">{{d|date('M d, Y')}} {{d|date('g:i a')}}</h3>
                      </div>
                    </div>

				
                    <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_lang}}</label>
                      <div class="col-sm-6">
                        <select class="form-control" name="user_lang" id="user_lang">
								<option  value="en">English</option>
								<option {%if currlng=="ru"%}selected{%endif%} value="ru">Русский</option>
                        </select>
                      </div>
                    </div>
 
                     <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_currency}}</label>
                      <div class="col-sm-6">
	                    <select id="currency" name="user_currency" class="form-control">
	                       {%for cul in currencyList%}
	                       <option {%if acs.user_currency==cul.currency_iso%}selected{%endif%} value="{{cul.currency_iso}}"> {{cul.currency_iso}} {{cul.currency_name}}</option>
	                       {%endfor%}
	                    </select>
                      </div>
                    </div>

                    
                     <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_tz}}</label>
                      <div class="col-sm-6">
	                    <select id="tz" name="user_tz" class="form-control">
	                       {%for tz in tzList%}
	                        <option  {%if acs.user_tz==tz.tz_olson%}selected{%endif%} value="{{tz.tz_olson}}"> {{tz.tz_name}}</option>
	                       {%endfor%}
	                    </select>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_dformat}}</label>
                      <div class="col-sm-6">
	                        <select id="dformat" name="user_dformat" class="form-control">
	                            <option value="M d, Y">{{dnow|date('M d, Y')}}</option>
	                            <option value="m/d/Y">{{dnow|date('m/d/Y')}}</option>
	                            {#<option value="d/m/Y">{{dnow|date('d/m/Y')}}</option>#}
	                            <option value="d-M-Y">{{dnow|date('d-M-Y')}}</option>
								<option value="d.m.Y">{{dnow|date('d.m.Y')}}</option>
	                        </select>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_tformat}}</label>
                      <div class="col-sm-6">
                           <select id="tformat" name="user_tformat" class="form-control">
                                <option value="g:i a">{{dnow|date('g:i a')}}</option>
                                <option {%if co.co_tformat=='H:i'%}selected{%endif%} value="H:i">{{dnow|date('H:i')}} (24hr., leading zero)</option>
                           </select>
                      </div>
                    </div>
 
                    <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_wstart}}</label>
                      <div class="col-sm-6">
						<select class="form-control" name="user_wstart" autocomplete="off">
							<option value="0">{{lng_first.form_wstart_sun}}</option>
							<option {%if acs.user_wstart==1%}selected{%endif%} value="1">{{lng_first.form_wstart_mon}}</option>
						</select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">{{lng_first.form_format_currency}}</label>
                      <div class="col-sm-6">
		                <select id="mf" name="mf" class="form-control">
		                    <option value="1"> 1,000,000.99</option>
		                    <option {%if co.co_dsep==',' and co.co_tsep=='.'%}selected{%endif%}  value="2"> 1.000.000,99</option>
		                    <option {%if co.co_dsep=='.' and co.co_tsep==' '%}selected{%endif%} value="3"> 1 000 000.99</option>
		                </select>
                      </div>
                    </div>

                    

                </div>
				</div>
				<button type="submit" style="margin-bottom: 20px;" class="btn btn-lg btn-space pull-right btn-success">{{lng_first.btn_go}}</button>
				</form>
			</div>	
		</div>
        </div>
	
    </div>	
    
    <script src="/assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js" type="text/javascript"></script>
    <script src="/assets/js/main.js" type="text/javascript"></script>
    <script src="/assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/assets/lib/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>
	<script src="/assets/lib/sweetalert/sweetalert.min.js" type="text/javascript"></script>

    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
      });
      
    </script>
	<script src="/js/dev/first.js?v=djkd" type="text/javascript"></script>
    
  </body>
</html>    