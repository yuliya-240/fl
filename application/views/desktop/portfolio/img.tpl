<input type="hidden" name="fpic[]" id="inp_{{ id }}" value="{{ filename }}">

<div class="item" id="item{{id}}" data-id="{{id}}">
    <div class="photo">
        <div class="img">
            <img id="pic" src="{{ihost}}/userfiles/{{uid}}/pic/{{filename}}" >
            <div class="over" style="background:  rgba(25, 52, 98, 0.8);">
                <div class="func">
                    <a class="image-zoom"><i class="icon s7-trash" style="font-size:40px; color: #ed1515b3; line-height:1.2" data-id="{{id}}"></i></a>
                    <a href="{{ihost}}/userfiles/{{uid}}/pic/{{filename}}" class="image-zoom2"><i class="icon s7-expand1" style="font-size:40px; line-height:1.2"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>


