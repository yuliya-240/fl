{% extends skin~"/common/root.tpl" %}
{% block headcss %}
    <link href="/assets/lib/switch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/assets/lib/redactor2/redactor.css?v={{ hashver }}"/>
    <link rel="stylesheet" href="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/css/Jcrop.css" type="text/css">
    <link rel="stylesheet" href="/css/profile.css" type="text/css"/>
    <link  href="/assets/lib/venobox/venobox.css" rel="stylesheet">
    <style>
    body.vbox-open {
        overflow: visible;
    }
    </style>



{% endblock %}

{% block content %}
    <div class="main-content col-md-12">
        <div class="row">
            <div class="col-sm-12">
                <form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                    <input type="hidden" id="fcover" name="fcover" value="nopic.jpg">

                    <div class="panel panel-default panel-borders" style="margin-bottom:5px;">
                        <div class="panel-heading">
                            <span class="title">{{ lng_portfolio.btn_add }}</span>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12">
                            <div class="col-sm-8">
                                <div class="form-group ">
                                    <label class="col-sm-3 control-label">{{ lng_portfolio.subj_title }}<span style="color: red;">*</span></label>

                                    <div class="col-sm-9">

                                        <input class="form-control" name="portfolio_title" required=""
                                               data-errorbox="#errname"
                                               data-msg-required="{{ lng_portfolio.text_req }}"
                                               placeholder="{{ lng_portfolio.subj }}" type="text">

                                        <div id="errname" style="color:red;"></div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="col-sm-3 control-label">{{ lng_portfolio.customer }}</label>

                                    <div class="col-sm-9">
                                        <input class="form-control" name="portfolio_customer" placeholder="{{ lng_portfolio.c_work }}"  type="text">
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="col-sm-3 control-label">{{ lng_portfolio.link }}</label>

                                    <div class="col-sm-9">
                                        <input class="form-control" name="portfolio_link" placeholder="http://www.somesite.com" type="text">
                                    </div>
                                </div>


                                <div class="form-group ">

                                    <div class="col-xs-12">
                                    <textarea name="portfolio_desc" id="notes" class="form-control"
                                              rows="6"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4" style="text-align: center;">
                                <legend class="text-blue text-left">{{ lng_portfolio.portfolio_cover }}</legend>
                                <div id="cover-pic" class="bottom fw-grid-gallery">

                                    <img class="img-responsible" id="u-ava" src="/img/avatar/avatar2x200.jpg">

                                </div>
                                <div style="clear:both;margin-top: 20px;"></div>
                                <a href="#" class="btn btn-block btn-primary" data-toggle="modal"
                                   data-target="#md-custom">{{ lng_portfolio.edit_cover }}</a>

                            </div>
                            </div>
                        </div>
                    </div>
                        <div class="panel panel-default panel-borders" style="margin-bottom:5px;">
                            <div class="panel-heading">
                                <span class="title">{{ lng_portfolio.pic_portfolio }}</span>
                            </div>
                            <div class="panel-body">
                            <div class="col-sm-12 gallery-container" id="d-content" style="padding-left: 0px; padding-right: 0px"></div>
                            

                            <div class="col-sm-12 text-center">

                                <div class="progress  progress-striped active" id="progress2" style="display:none;">
                                    <div id="progressSlide2" style="width: 0%;" class="progress-bar progress-bar-success">{{ lng_common.load }}</div>
                                </div>

                                <div id="counteinerpic2">

                                    <button style="clear:both;" class="btn btn-success btn-lg"
                                                id="pickfiles2">{{ lng_portfolio.load_pic }}</button>
                                    </div>

                            </div>
                            </div>
                        </div>




                    <div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;">
                        <button type="button" id="doSave"
                                class="btn btn-primary btn-lg pull-right">{{ lng_common.btn_save }}</button>
                        <a href="/portfolio" class="btn btn-default btn-lg pull-left">{{ lng_common.btn_cancel }}</a>
                        <div style="clear:both;margin-bottom: 20px;"></div>
                    </div>
                </form>
            </div>

        </div>
    </div>


{% endblock %}



{% block js %}
    <script src="/assets/lib/plupload-2.3.1/js/plupload.full.min.js"></script>
    <script src="http://jcrop-cdn.tapmodo.com/v2.0.0-RC1/js/Jcrop.js"></script>
    <script type="text/javascript" src="/assets/lib/redactor2/redactor.js?v={{ hashver }}"></script>
    <script src="/assets/lib/switch/js/bootstrap-switch.min.js"></script>
    <script src="/assets/lib/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="/assets/lib/venobox/venobox.min.js"></script>
    <script src="/js/dev/portfolio_add.js"></script>



{% endblock %}
{% block modals %}

    <form id="fcoord">
        <input type="hidden" name="x" id="x">
        <input type="hidden" name="y" id="y">
        <input type="hidden" name="w" id="w">
        <input type="hidden" name="h" id="h">
        <input type="hidden" name="fname" id="fname">
        <input type="hidden" name="ftype" id="ftype">
    </form>

    <div id="md-custom" tabindex="-1" role="dialog" class="modal fade">
        <div style="width: 100%;" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <img class="img-responsible center-block" id="target"
                             {% if portfolio.portfolio_thumbnail=="nopic.jpg" %}src="/img/avatar/avatar2x200.jpg"{% endif %}>

                    </div>
                </div>
                <div class="modal-footer" id="counteinerpic">
                    <div class="progress  progress-striped active" id="progress1" style="display:none;">
                        <div id="progressSlide" style="width: 0%;" class="progress-bar progress-bar-success">
                            Loading...
                        </div>
                    </div>

                    <div id="btns">

                        <button type="button" id="btnCancel" class="btn btn-default pull-left"><i
                                    class="material-icons">close</i></button>

                        <button type="button" class="btn btn-danger pull-left" id="del-pic" style="display: none;"><i
                                    class="material-icons">delete_forever</i></button>


                        <button type="button" id="pickfiles" class="btn btn-primary"><i class="material-icons">file_upload</i>
                        </button>

                        <button type="button" id="picSave" class="btn btn-success" style="display: none;"><i
                                    class="material-icons">save</i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

{% endblock %}
