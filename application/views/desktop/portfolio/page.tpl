{% for r in records %}

<div class="item div{{ r.portfolio_id }}" data-id="{{ r.portfolio_id }}">
    <div class="photo panel panel-default panel-borders">
        <div class="img">
            <img {%if r.portfolio_thumbnail=="nopic.jpg"%}src="/img/avatar/avatar2x200.jpg"
                    {% else %}
                        src="/userfiles/{{r.portfolio_user_id}}/pic/{{r.portfolio_thumbnail}}"
                    {%endif%}>
            <div class="over" style="background: rgba(25, 52, 98, 0.8);">
                <div class="func">
                    <a href="#" class="image-zoom"><i class="icon s7-trash" style="font-size:40px; color: #ed1515b3; line-height:1.2" data-id="{{ r.portfolio_id }}"></i></a>
                    <a href="#" class="image-zoom"><i class="icon s7-edit" style="font-size:40px; line-height:1.2" data-id="{{ r.portfolio_id }}"></i></a>
                </div>
            </div>
        </div>
        <div class="description">

            <div class="am-checkbox" style="padding-left: 7px;">
                <input type="checkbox" id="chk{{r.portfolio_id}}" class="check" name="row[]" value="{{r.portfolio_id}}">
                <label for="chk{{r.portfolio_id}}"></label>
            </div>

            <div class="desc">
                <span>{{ r.portfolio_title }}</span>
            </div>
        </div>
    </div>
</div>
{% endfor %}
<input type="hidden" name="portfolio_id" data-id="{{ portfolio.portfolio_id}}" value="{{ portfolio.portfolio_id }}">
<input type="hidden" id="_idt" value="{{ lng_portfolio.del_portfolio_title }}">
<input type="hidden" id="_idm" value="{{ lng_portfolio.del_portfolio_message }}">
<input type="hidden" id="_yes" value="{{ lng_common.btn_yes }}">
<input type="hidden" id="_cb" value="{{ lng_common.btn_cancel }}">