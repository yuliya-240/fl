{% extends skin~"/common/root.tpl" %}
{% block headcss %}
<style>
    body.vbox-open {
        overflow: visible;
    }
</style>
{% endblock %}
{% block content %}
    <input type="hidden" id="_pdt" value="{{lng_portfolio.del_bundle_title}}">
    <input type="hidden" id="_pdm" value="{{lng_portfolio.del_bundle_message}}">
    <input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">

    <div class="page-head">
        <h3 style="margin-bottom: 2px; margin-top: 2px;">{{ lng_portfolio.title }}
            <a href="#" class="btn btn-head btn-success pull-right g-l" data-go="/portfolio/add" data-toggle="tooltip"
               data-placement="bottom" data-original-title="{{ lng_portfolio.btn_add }}">{{ lng_portfolio.btn_add }}</a>
        </h3>
    </div>
    
    
    <div class="main-content">
        <form id="fsearch" class="form-horizontal group-border-dashed">

            <div class="form-group">
                <div class="col-xs-12">

                    <div class="input-group">

                        <span class="input-group-addon btn-primary"
                              style="background-color:#ccc; color:#fff; font-size: 20px;"><span
                                    class="s7-search"></span></span>
                        <input class="form-control" id="s" placeholder="{{ lng_portfolio.search_ph }}"
                               {% if searchstr %}value="{{ searchstr }}"{% endif %} type="text"
                               style="border-width:1px;">


                    </div>
                </div>
            </div>
        </form>

<form id="flist">
        <div class="gallery-container" id="d-content"></div>
        <div style="text-align:center">
            <button id="load-more" data-next="" style="display:none;" class="btn btn-lg btn-primary">{{ lng_portfolio.load }}</button>

        </div>
</form>
    </div>

{% endblock %}

{%block footer%}
    <div id="megafooter">

    </div>
{%endblock%}


{% block js %}
    <script src="/assets/lib/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="/js/dev/portfolio.js?v={{ hashver }}"></script>
    {#<script src="/js/dev/portfolio_edit.js"></script>#}



{% endblock %}