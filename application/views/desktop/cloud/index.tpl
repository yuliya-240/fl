{% extends skin~"/common/root.tpl" %}
{%block headcss%}
    
{%endblock%}

{%block content%}

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">Cloud Connection</h3>
</div>	


<div class="main-content">
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<div class="panel panel-default panel-borders">
	        <div class="panel-body">
				
				<table class="table">
					<tr>
						<td>Google</td>
						<td>
							{%if isauth%}
								Connected 
							{%else%}
								<span class="text-danger">Not Connected</span>
							{%endif%}
						</td>
						
						<td width="1%">
							{%if isauth%}
								<a href="/cloud/google" class="btn btn-primary">Sync</a>
							{%else%}
								<a href="{{authUrl}}" class="btn btn-success">Connect</a>
							{%endif%}
							
							
						</td>
					</tr>
				</table>
	
	        </div>
		</div>
		</form>
	</div>
</div>		
</div>

{%endblock%}


{%block js%}

   <script src="/js/dev/projects.js"></script>
{%endblock%}
