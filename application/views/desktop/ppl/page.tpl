{% for r in records %}
    <div data-id="{{ r.portfolio_id }}">
        <div class="photo panel panel-default panel-borders">
            <div class="img">
                <img {%if r.portfolio_thumbnail=="nopic.jpg"%}src="/img/avatar/avatar2x200.jpg"
                        {% else %}
                            src="/userfiles/{{r.portfolio_user_id}}/pic/{{r.portfolio_thumbnail}}"
                        {%endif%}>

            </div>
            <div class="description">
                <div class="desc">
                    <span>{{ r.portfolio_title }}</span>
                </div>
            </div>
        </div>
    </div>
{% endfor %}