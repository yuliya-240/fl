{% extends skin~"/common/root.tpl" %}
{%block headcss%}
    <link rel="stylesheet" href="/css/profile.css" type="text/css"/>
    <style>
        .gl{
            cursor: pointer;
        }

    </style>
{%endblock%}

{%block content%}

    <div class="page-head">

    </div>


    <div class="main-content" id="main-content-box">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default panel-borders" >

                    <div class="panel-body">
                        <div id="d-content">
                            <input type="hidden" value="{{collabid}}" id="vcollab_id">
                            <div class="user-profile">
                                <div class="user-display">
                                    <div style="margin-top: 50px;"></div>
                                    <div class="bottom fw-grid-gallery">
                                        <div class="user-avatar fw-block-3">

                                            <img class="img-responsible" id="u-ava" {%if user.user_pic=="nopic.png"%}src="/img/avatar/avatar2x200.jpg"{%else%}src="/userfiles/{{user.user_id}}/pic/{{user.user_pic}}"{%endif%}>

                                        </div>
                                        <div class="user-info">
                                            <h4><a target="_blank" href="http://floctopus.com/{{user.user_username}}">{{user.user_name}} </a> </h4>
                                            <span>{{user.user_username}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12" >
                                        <div class="info-block panel panel-default">
                                            <div class="panel-body">
                                                {%if user.user_about%}
                                                    <span class="description">{{user.user_about}}</span>
                                                {%endif%}

                                                <table class="table">
                                                    <tr>
                                                        <td  class="item" >{{lng_profile.occupation}}<span class="icon s7-portfolio"></span></td>
                                                        <td >{{proff}}</td>
                                                    </tr>

                                                    {%if user.user_bday_pub%}
                                                        <tr>
                                                            <td class="item">{{lng_profile.birthday}}<span class="icon s7-gift"></span></td>
                                                            <td>{{user.user_bday|date(acs.user_dformat)}}</td>
                                                        </tr>
                                                    {%endif%}

                                                    {%if user.user_phone_pub%}
                                                        <tr>
                                                            <td class="item">{{lng_profile.phone}}<span class="icon s7-phone"></span></td>
                                                            <td>{%if user.user_phone%}{{user.user_phone}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>

                                                        </tr>
                                                    {%endif%}

                                                    {%if user.user_addr_pub%}
                                                        <tr>
                                                            <td class="item">{{lng_profile.location}}<span class="icon s7-map-marker"></span></td>
                                                            <td>
                                                                {%if user.user_street or user.user_city or user.user_state or user.user_zip%}
                                                                    <address>{{user.user_street}}  {{user.user_city}}  {{user.user_state}} {{user.user_zip}}</address>
                                                                {%else%}
                                                                    <span class="text-danger">{{lng_common.ns}}</span>
                                                                {%endif%}
                                                            </td>

                                                        </tr>
                                                    {%endif%}

                                                    {%if user.user_country_pub%}
                                                        <tr>
                                                            <td class="item">{{lng_profile.country}}<span class="icon s7-map-marker"></span></td>
                                                            <td>{{country}}</td>

                                                        </tr>
                                                    {%endif%}


                                                    {%if user.user_www_pub%}
                                                        <tr>
                                                            <td class="item">{{lng_profile.website}}<span class="icon s7-global"></span></td>
                                                            <td>{%if user.user_www%}{{user.user_www}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>

                                                        </tr>
                                                    {%endif%}


                                                    {%if user.user_skype_pub%}
                                                        <tr>
                                                            <td class="item">Skype<span class="icon s7-global"></span></td>
                                                            <td>{%if user.user_skype%}{{user.user_skype}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>

                                                        </tr>
                                                    {%endif%}

                                                    {%if user.user_twitter_pub%}
                                                        <tr>
                                                            <td class="item">Twitter<span class="icon s7-global"></span></td>
                                                            <td>{%if user.user_twitter%}{{acs.user_twitter}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>

                                                        </tr>
                                                    {%endif%}


                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12">
                                            <div class="gallery-container">
                                                {% for r in records %}

                                                    <div class="item" data-id="{{ r.portfolio_id }}">
                                                        <div class="photo panel panel-default panel-borders">
                                                            <div class="img">
                                                                <img {%if r.portfolio_thumbnail=="nopic.jpg"%}src="/img/avatar/avatar2x200.jpg"
                                                                        {% else %}
                                                                            src="/userfiles/{{r.portfolio_user_id}}/pic/{{r.portfolio_thumbnail}}"
                                                                        {%endif%}>

                                                            </div>
                                                            <div class="description">
                                                                <div class="desc">
                                                                    <span>{{ r.portfolio_title }}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                {% endfor %}
                                            </div>
                                        <table class="table">
                                            {%for r in edu%}
                                                <tr>
                                                    <td>

                                                        <h3>{{r.edu_school}}</h3>
                                                        <p>{%if r.edu_degree%}<strong>{{r.edu_degree}}</strong>{%endif%}&nbsp;&nbsp; {{ r.mfrom}}, {{r.edu_year_from}} - {{r.mto}}, {{r.edu_year_to}} {{r.edu_area}}</p>
                                                        {%if r.edu_desc%}<p class="text-muted">{{r.edu_desc}}</p>{%endif%}
                                                    </td>
                                                </tr>
                                            {%endfor%}
                                        </table>
                                        <table class="table">
                                            {%for r in work%}
                                                <tr>
                                                    <td>

                                                        <h3>{{r.work_company}}</h3>
                                                        <p>{%if r.work_position%}<strong>{{r.work_position}}</strong>{%endif%}&nbsp;&nbsp; {{r.mfrom}}, {{r.work_year_from}} - {%if r.work_current!=0%} {{lng_profile.present_time}} {%else%} {{r.mto}}, {{r.work_year_to}}{%endif%} {{r.work_location}}</p>
                                                        {%if r.work_desc%}<p class="text-muted">{{r.work_desc}}</p>{%endif%}
                                                    </td>
                                                </tr>
                                            {%endfor%}
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


{%endblock%}

{%block js%}
    <script src="/assets/lib/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="/js/dev/ppl.js?v={{ hashver }}"></script>
{%endblock%}
