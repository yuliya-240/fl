<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{lng_login.title}}</title>
    <link rel="stylesheet" type="text/css" href="/assets/lib/stroke-7/style.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/lib/jquery.nanoscroller/css/nanoscroller.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" href="/assets/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/css/themes/theme-google.min.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/lib/sweetalert/sweetalert.min.css" type="text/css"/>
    <link rel="stylesheet" href="/css/app.css" type="text/css"/>
	<link rel="stylesheet" href="/css/login.css" type="text/css"/>
  </head>
  <body class="am-splash-screen">
	<!-- begin #page-loader -->

	<div id="process-loader" class="fade">
	    <div class="material-loader">
	        <svg class="circular" viewBox="25 25 50 50">
	            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
	        </svg>
	        <div class="message">{{lng_common.loader}}</div>
	    </div>
	</div>
	<!-- end #page-loader -->
	<div id="login-box" class="login  animated fadeInDown">  
    <div class="am-wrapper am-login">
      <div class="am-content">
        <div class="main-content">
          <div class="login-container">
            <div class="panel panel-default">
              <div class="panel-heading"><img src="/logo/login_signup_black.png" alt="logo"  class="logo-img"><span>{{lng_login.desc}}</span></div>
              <div class="panel-body">
                <form  id="flogin" class="form-horizontal">
                  <div class="login-form">
                    <div class="form-group">
	                    
                      <div id="email-handler" class="input-group"><span class="input-group-addon"><i class="icon s7-mail"></i></span>
                        <input type="email" id="email" name="email" required="" data-errorbox="#error-box"  placeholder="E-mail" autocomplete="off" data-msg-required="{{lng_login.email_req}}" data-msg-email="{{lng_login.not_email}}" autofocus="" class="form-control">
                      </div>
                      <div id="error-box" style="margin-top:5px;"></div>
                    </div>
                    <div class="form-group">
	                    
                        <div id="password-handler" class="input-group"><span class="input-group-addon"><i class="icon s7-lock"></i></span>
                          <input name="password" id="pass" type="password" data-errorbox="#error-box-pass"  placeholder="{{lng_login.placeholder_pass}}" data-msg-required="{{lng_login.pass_req}}"  required="" class="form-control">
                        </div>
                        <div id="error-box-pass" style="margin-top:5px;"></div>
                    </div>
                    <div class="form-group login-submit">
                      <button data-dismiss="modal" type="submit" class="btn btn-primary btn-lg">{{lng_login.btn_login}}</button>
                    </div>
                    <div class="form-group footer row">
                      <div class="col-xs-6"><a href="#" id="act-forget">{{lng_login.btn_forget}}</a></div>
                      <div class="col-xs-6 remember">
                        <label for="remember">{{lng_login.btn_remember}}</label>
                        <div class="am-checkbox">
                          <input type="checkbox" id="remember">
                          <label for="remember"></label>
                        </div>
                      </div>
                    </div>

                    <div class="form-group footer row">
                      <div class="col-xs-12 text-center"><a href="/signup">{{lng_login.btn_createaccount}}</a></div>
                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
	
	<div id="reset-box" class="login  animated fadeInDown" style="display:none;">
    
    <div class="am-wrapper am-login">
      <div class="am-content">
        <div class="main-content">
          <div class="login-container">
            <div class="panel panel-default">
              <div class="panel-heading"><img src="/logo/login_signup_black.png" alt="logo"  class="logo-img"><span>{{ lng_login.res_pass }}</span></div>
              <div class="panel-body">
                <form  id="freset" class="form-horizontal">
                  <div class="login-form">
	                <div role="alert" class="alert alert-warning alert-icon alert-dismissible">
                    <div class="icon"><span class="s7-attention"></span></div>
                    <div class="message">
                      <strong>{{ lng_login.fear_not }}</strong>{{ lng_login.instr }}
                    </div>
                  </div>
                    <div class="form-group">
	                    
                      <div id="reset-email-handler" class="input-group"><span class="input-group-addon"><i class="icon s7-mail"></i></span>
                        <input type="email" id="remail" name="remail" required="" data-errorbox="#reset-error-box"  placeholder="E-mail" autocomplete="off" data-msg-required="{{lng_login.email_req}}" data-msg-email="{{lng_login.not_email}}"  class="form-control">
                      </div>
                      <div id="reset-error-box" style="margin-top:5px;"></div>
                    </div>
                  
                    <div class="form-group login-submit">
                      <button data-dismiss="modal" type="submit" class="btn btn-primary btn-lg">{{ lng_login.res_pass }}</button>
                    </div>
              

                    <div class="form-group footer row">
                      <div class="col-xs-12 text-center"><a href="#" class="back-login">{{ lng_login.let_log }}</a></div>
                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
	</div>

	<div id="done-box" class="login  animated fadeInDown" style="display: none;">  
    <div class="am-wrapper am-login">
      <div class="am-content">
        <div class="main-content">
          <div class="login-container">
            <div class="panel panel-default">
              <div class="panel-heading"><img src="/logo/login_signup_black.png" alt="logo"  class="logo-img"></div>
              <div class="panel-body">
            
	            
                  <div class="login-form">
            
				  <div role="alert" class="alert alert-success alert-icon alert-border-color alert-dismissible">
                    <div class="icon"><span class="s7-check"></span></div>
                    <div class="message">
                     <strong>{{ lng_login.good }}</strong>{{ lng_login.sent_inst }}
                    </div>
                  </div>                  

                    <div class="form-group footer row">
                      <div class="col-xs-12 text-center"><a class="back-login" href="#">{{ lng_login.log_now }}</a></div>
                    </div>

                  </div>
    
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
	
    <script src="/assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js" type="text/javascript"></script>
    <script src="/assets/js/main.js" type="text/javascript"></script>
    <script src="/assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/assets/lib/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>
	<script src="/assets/lib/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<script src="/js/dev/login.js" type="text/javascript"></script>
  </body>
</html>