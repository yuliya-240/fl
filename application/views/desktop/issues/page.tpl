{%if records%}
<div class="table-responsive">

<table class="table  table-hover" style="margin-bottom: 0;">

	<tbody>
		{%for r in records%}
		<tr class="tbl-pointer" onclick="javascript:showProcess();window.location.href='/issues/view/{{r.issue_id}}'">
		
			<td {%if loop.first%}style=" vertical-align: middle;"{%else%}style="vertical-align: middle;"{%endif%} align="center" width="1%">
				<span class="badge badge-{{r.issueprior_color}}" style="margin-bottom: 3px;">{{r.issueprior_name}}</span><br>
				<span class="text-{{r.issuecat_color}}" style="font-weight: 500;">{{r.issuecat_name}}</span>
			</td>
			<td {%if loop.first%}style="vertical-align: middle;"{%else%}style="vertical-align: middle;"{%endif%}>
				<p>
				
				{%if r.project_default==0%}
				<a href="/issues/?prj={{r.issue_project_id}}">{{r.project_name}}</a> 
				{%if r.issue_milestone_id%} <i class="fa fa-long-arrow-right "></i>{{r.prjm_name}} {%endif%}
				{%else%}
				<a href="/issues/?prj={{r.issue_project_id}}">{{lng_issues.select_project}}</a>
				{%endif%}
					
				</p>
				<p style="font-weight: 600; font-size: larger; margin-bottom: 1px;">
					<span class="text-muted">#{{r.issue_id}}</span> 
					<button type="button" class="btn btn-space btn-default text-{{r.issuestatus_color}} btn-xs" >
					<span style="font-weight: 700;">{{r.issuestatus_name}}</span>
					</button>{{r.issue_subj}} 
				</p>
				
				{{lng_issues.created}} <b>{{r.user_username}}</b>, 
				{{lng_issues.assign}}: <a href="#" style="font-weight: 700;">{{r.assigned}}</a>
				, {{lng_projects.deadline}} : 
				<span attr-today="{{today|date(acs.user_dformat)}}"
				{%if r.issue_deadline|date(acs.user_dformat)==today|date(acs.user_dformat) and r.issue_status!=50%}class="text-warning"{%endif%} 
				{%if r.issue_deadline|date(acs.user_dformat)<today|date(acs.user_dformat) and r.issue_status!=50%}class="text-danger"{%endif%} 
				{%if r.issue_deadline|date(acs.user_dformat)>today|date(acs.user_dformat) and r.issue_status!=50%}class="text-success"{%endif%}>{{r.issue_deadline|date(acs.user_dformat)}} {{r.issue_deadline|date(acs.user_tformat)}}</span>
				
			</td>
			<td width="2%" {%if loop.first%}style="vertical-align: middle;"{%else%}style="vertical-align: middle;"{%endif%} align="center">{%if r.issue_msg>0%}<i style="font-size: xx-large" class="icon s7-chat"></i> {{r.issue_msg}}{%endif%}</td>
		</tr>
		{%endfor%}
	</tbody>
</table>				

</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
