<select class="form-control opt-filter" id="participant" name="issue_collab" style="margin-bottom: 5px;">
	<option value="0">{{lng_issues.sel_all_participants}}</option>
	
	<option value="{{acs.user_id}}">{{lng_issues.my_issues}}</option>
	{%for c in collabs%}
	<option {%if ccollab==c.user_id%}selected{%endif%} value="{{c.user_id}}">{{c.user_username}} ({{c.user_name}})</option>
	{%endfor%}
</select>	
