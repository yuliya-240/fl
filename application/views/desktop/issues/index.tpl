{% extends skin~"/common/root.tpl" %}
{%block headcss%}
 
 <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
 
 <style>
	 hr.style-three {
    border: 0;
    border-bottom: 1px dashed #a5a5a5;
    background: #ccc;
    margin-bottom: 11px;
    margin-top: 11px;
}
</style>	 
 
{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_issues.title}} 
		
		<a href="/issues/add" class="btn btn-success btn-head pull-right" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_issues.btn_add}}" id="btn-issue-add">{{lng_issues.btn_add}}</a>
			
		
		
	</h3>
</div>	

<div class="main-content" >
<input type="hidden" id="moment-format" value="{{momentformat}}">
<input type="hidden" id="startDate" value="{{startdate}}">
<input type="hidden" id="endDate" value="{{enddate}}">

<div class="row">
	<div class="col-sm-10 ">
	<form id="fsearch"  class="form-horizontal group-border-dashed">
		<div class="form-group">
			<div class="col-sm-10">
				<div class="input-group">
					<span class="input-group-addon srch-left-addon">
						<span class="s7-search" style="font-size: 24px;"></span>
					</span>
                      <input class="form-control" id="s" placeholder="{{lng_issues.search_ph}}" {%if searchstr%}value="{{searchstr}}"{%endif%} type="text" style="border-width:1px;" >
					  <span class="input-group-addon daterange-right-addon" id="idrp">
					 	 <span class="s7-date" style="font-size: 24px;"></span>
					  </span>
                       
                </div>
			</div>		
			{#
			<div class="col-sm-2" style="padding-left: 0;">
				
				<div class="btn-group btn-block ">
                      <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle btn-block " aria-expanded="false" style="padding-top:11px; padding-bottom: 11px; ">Deadline 1 > 0 <span class="caret"></span></button>
                      <ul role="menu" class="dropdown-menu">
                        <li><a href="#">Deadline 0 > 1</a></li>
                        <li><a href="#">Title A > Z</a></li>
                        <li><a href="#">Title Z > A</a></li>
                      
                        
                      </ul>
                    </div>
				
			</div>
			#}
		</div>		
	</form>		
		
		<form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<div class="panel panel-default panel-borders" style="border-radius: 3px;">

	        <div class="panel-body" >
			
				<div id="d-content">
		
				</div>
	
	        </div>
		</div>
		</form>
	</div>
	<div class="col-sm-2">
	<legend class="blue" >{{lng_common.filter}}</legend>
	<form id="options" class="form-horizontal">
		
		<button class="btn btn-block btn-primary btn-my-tasks" style="margin-bottom: 10px;">{{lng_issues.my_issues}}</button>
		<hr class="style-three">
		<select class="form-control opt-filter-prj" id="project" name="issue_project" style="margin-bottom: 5px;">
			<option {%if cprj==0%}selected{%endif%} value="0">{{lng_issues.sel_allprojects}}</option>
			<option {%if cprj==defprj%}selected{%endif%} value="{{defprj}}">{{lng_issues.select_project}}</option>
			{%for p in projects%}
			<option {%if cprj==p.project_id%}selected{%endif%} value="{{p.project_id}}">{{p.project_name}}</option>
			{%endfor%}
		</select>	

		<div id="miles-box" {%if cprj==0%}style="display:none;"{%endif%}>
		{%include skin~"/issues/selmiles.tpl"%}	
		</div>	


		<div id="usr-box">
		{%include skin~"/issues/selparticipants.tpl"%}
		</div>
		
		
		<hr class="style-three">
		<select class="form-control opt-filter" id="status" name="issue_status" style="margin-bottom: 5px;">
			<option value="0">{{lng_issues.sel_all_statuses}}</option>
        	{%for c in statuses%}
        	<option {%if cstatus==c.issuestatus_status%}selected{%endif%}  value="{{c.issuestatus_status}}">{{c.issuestatus_name}}</option>
        	{%endfor%}
			
		</select>	


		
		<select class="form-control opt-filter" id="priority" name="issue_priority" style="margin-bottom: 5px;">
			<option value="0">{{lng_issues.sel_all_priority}}</option>
        	{%for p in priorities%}
        	<option {%if cprior==p.issueprior_prior%}selected{%endif%} value="{{p.issueprior_prior}}">{{p.issueprior_name}}</option>
        	{%endfor%}
			
		</select>	

		<select class="form-control opt-filter" id="category" name="issue_category" style="margin-bottom: 5px;">
			<option value="0">{{lng_issues.sel_all_cats}}</option>
        	{%for c in cats%}
        	<option value="{{c.issuecat_cat}}">{{c.issuecat_name}}</option>
        	{%endfor%}
			
		</select>	


		</form>	

</div>
		
</div>
</div>

{%endblock%}
{%block footer%}
<div id="megafooter">
	
</div>	
{%endblock%}

{%block js%}
	<script src="/assets/lib/date-time/moment.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
   <script src="/js/dev/issues.js?v={{hashver}}"></script>
{%endblock%}
