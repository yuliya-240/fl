<select class="form-control opt-filter" id="issue_milestone_id" name="issue_milestone_id" style="margin-bottom: 5px;">
	<option value="0">{{lng_issues.sel_all_miles}}</option>
	
	{%for m in miles%}
	<option {%if cmiles==m.prjm_id%}selected{%endif%} value="{{m.prjm_id}}">{{m.prjm_name}}</option>
	{%endfor%}
</select>	
