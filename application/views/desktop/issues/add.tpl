{% extends skin~"/common/root.tpl" %}
{%block headcss%}
 	<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
 	<link rel="stylesheet" href="/assets/lib/redactor2/redactor.css?v={{hashver}}" />
{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_issues.btn_add}} </h3>
</div>	


<div class="main-content" >
	
<div class="row">
	<div class="col-md-8 col-md-offset-2">
	<form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<input type="hidden" id="defprj" value="{{defprj}}">
		
		<div class="panel panel-default panel-borders" style="margin-bottom:5px;">

	        <div class="panel-body">
		        
               <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_issues.subj}}<span style="color: red;">*</span></label>
                  <div class="col-sm-9">
                    <input class="form-control" name="issue_subj" data-errorbox="#errname" data-msg-required="{{lng_issues.subj_req}}"  required=""  type="text">
                    <div id="errname" style="color:red;"></div>
                  </div>
                </div>

                <div class="form-group">
               
                  <div class="col-xs-12">
				  <textarea class="form-control" id="notes" name="issue_text" rows="8"></textarea>
                  </div>
                </div>
                
             
                
		        
	        </div>
		</div> 
		<div class="panel panel-default panel-borders" style="margin-bottom:5px;">

	        <div class="panel-body">
		        
		        <table class="table" id="attach-files">
			        <tr id="0"></tr>
			        
		        </table>
		        <div class="text-center" id="counteinerpic">
			        <button type="button" id="pickfiles" class="btn btn-info">{{lng_issues.attach}}</button>
		        </div>
		        
 				<div class="progress  progress-striped active" id="progress1" style="display:none;">
					<div id="progressSlide" style="width: 0%;" class="progress-bar progress-bar-success">{{lng_common.loader}}</div>
				</div>
		        
	        </div>
		</div>    
		<div class="panel panel-default panel-borders" style="margin-bottom:5px;">

	        <div class="panel-body">
               <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_issues.project}}<span style="color: red;">*</span></label>
                  <div class="col-sm-6">
                    <select class="form-control" id="project-sel" name="issue_project_id">
                    	
                    	<option value="{{defprj}}">{{lng_issues.select_project}}</option>
						{%for p in projects%}
						<option {%if cprj==p.project_id%}selected{%endif%} value="{{p.project_id}}">{{p.project_name}}</option>
						{%endfor%}
                    	<option disabled>------------</option>
                    	<option value="-1">{{lng_issues.btn_add_prj}}</option>
                    </select>    
                  </div>
                </div>
			      
                <div class="form-group" id="miles-box" style="display: none;">
                  <label class="col-sm-3 control-label">{{lng_issues.milestones}}</label>
                  <div class="col-sm-4" id="sel-miles-box">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_issues.category}}</label>
                  <div class="col-sm-4">
                    <select class="form-control"  name="issue_cat">
                    	{%for c in cats%}
                    	<option value="{{c.issuecat_cat}}">{{c.issuecat_name}}</option>
                    	{%endfor%}
                    </select>    
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_issues.priority}}</label>
                  <div class="col-sm-4">
                    <select class="form-control" id="project" name="issue_priority">
                    	{%for p in priorities%}
                    	<option value="{{p.issueprior_prior}}">{{p.issueprior_name}}</option>
                    	{%endfor%}
                    	
                    </select>    
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_issues.assign}}</label>
                  <div class="col-sm-6" id="sel-collab-box">
					<select class="form-control" name="issue_assign">
						
						
						
						<option value="{{acs.user_id}}">Me ({{acs.user_name}})</option>
						
						
					</select>    

                  </div>
                </div>
				
				<div class="form-group">
					
					<label class="col-sm-3 control-label">{{lng_projects.deadline}}</label>
					
					<div class="col-md-3 col-sm-4">
						<div class="input-group pull-left">
							<span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
							<input type="text" data-date-format="{{dpformat}}" readonly="" value="{{deadline_date}}" name="deadline_date" class="form-control date-picker">
							
						</div>						
					</div>
						
					<div class="col-md-3 col-sm-4">
						<div class="input-group pull-left">
							<span class="input-group-addon">
								<i class="fa fa-clock-o bigger-110"></i>
							</span>
							<input type="text" value="{{deadline_time}}"  name="deadline_time" class="form-control time-picker">
						</div>						
					</div>	
						
				</div>
	        </div>
		</div> 
		
		  
		<button type="button" id="doSave" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
		<a href="/issues" class="btn btn-default btn-lg pull-left">{{lng_common.btn_cancel}}</a>
		<div style="clear:both;margin-bottom: 40px;"></div>	
		  
	</form>	
	</div>
</div>		
</div>

{%endblock%}

{%block js%}
  	<script src="/assets/lib/plupload-2.3.1/js/plupload.full.min.js"></script>
	<script type="text/javascript" src="/assets/lib/redactor2/redactor.js?v={{hashver}}"></script>	
	<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>	
	<script src="/assets/lib/date-time/bootstrap-timepicker.js"></script>
    <script src="/js/dev/issue_add.js?v={{hashver}}"></script>
{%endblock%}

