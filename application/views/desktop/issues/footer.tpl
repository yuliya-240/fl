<footer class="hidden-xs" >

	{%if pagination.previous%} 
	<a href="#" data-page="{{pagination.previous}}" class=" prevp" ><i class="fa fa-arrow-left"></i></a>
	{%endif%}
	<span >{{pagination.info}}</span>
	{%if pagination.next%}
	<a href="#" data-page="{{pagination.next}}" class="nextp" ><i class="fa fa-arrow-right"></i></a>
	{%endif%}

</footer>


