		<input type="hidden" id="prj-id" value="{{i.issue_project_id}}">
		<div class="issue-timeline" style="margin-top: 0;" >
			<div class="user">
	        <a href="#" class="view-u-profile" data-id="{{i.user_id}}">
	        <img {%if i.user_pic=='nopic.png'%}src="/img/avatar/avatar1x50.jpg"{%else%}src="/userfiles/{{i.user_id}}/pic/{{i.user_pic}}"{%endif%}>
            <span >{{i.user_username}}</span>
            </a>
            
            {{lng_issues.created_task}} 
            <time datetime="{{i.issue_created|date('Y-m-d')}}T{{i.issue_created|date('H:i:s')}}" class="timeago"></time>
           
            </div>
		</div>
		
		<div class="panel panel-default panel-borders panel-issue" id="issue-text" >
           <div class="panel-heading ">
	           {%if acs.user_id == i.issue_user_id%}
	           <div class="tools">
		           <span id="edit-issue" class="icon s7-note"></span>
		       </div>
		       {%endif%}
	           <span>{{i.issue_subj}}</span>
	        </div>
           <div class="panel-body">
	     
	           {{i.issue_text}}
           </div>
		</div>   
		
		{%if att%}
		
		<div class="panel panel-default panel-borders">
			<div class="panel-body">
				{%for a in att%}
				<p><a target="_blank" href="/userfiles/{{acs.user_id}}/attach/{{a.issueatt_filename}}">{{a.issueatt_filename}}</a></p>
				{%endfor%}
			</div>	
		</div>
		{%endif%}
		
		<div id="edit-issue-box" style="display: none;">
		<form id="feditissue" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
 		<div class="panel panel-default panel-borders"  >

	        <div class="panel-body">
		        
                <div class="form-group">
                  <label class="col-sm-3 control-label">{{lng_issues.subj}}<span style="color: red;">*</span></label>
                  <div class="col-sm-9">
                    <input class="form-control" name="issue_subj" id="issue-edit-subj" data-errorbox="#errname" data-msg-required="{{lng_issues.subj_req}}"  required=""  type="text">
                    <div id="errname" style="color:red;"></div>
                  </div>
                </div>

                <div class="form-group">
               
                  <div class="col-xs-12">
				  <textarea class="form-control" id="issue-edit-text" name="issue_text" rows="6"></textarea>
                  </div>
                </div>
                
                <button type="button" id="doSaveEditIssue" class="btn btn-primary btn-lg pull-right">{{lng_common.btn_save}}</button>
				<a href="#" class="btn btn-default btn-lg pull-left" id="cancel-edit-issue">{{lng_common.btn_cancel}}</a>

		        
	        </div>
	        
		</div> 
		
		 
		</form>
		</div>


		{%for h in history%}
			{%if h.ihist_msg%}

				<div {%if loop.last%}id="last"{%endif%} class="issue-timeline" >
					<div class="user">
			        <a href="#">
			        <img {%if h.user_pic=='nopic.png'%}src="/img/avatar/avatar1x50.jpg"{%else%}src="/userfiles/{{h.user_id}}/pic/{{h.user_pic}}"{%endif%}>
		            <span >{{h.user_username}}</span>
		            </a>
		            
		            {{lng_issues.commented_task}} 
		            <time datetime="{{h.ihist_date|date('Y-m-d')}}T{{h.ihist_date|date('H:i:s')}}" class="timeago"></time>
		           
		            </div>
				</div>

				<div class="panel panel-default panel-borders panel-issue" >
		
		           <div class="panel-body">
			     
			           {{h.ihist_message}}
		           </div>
				</div>  
			{%endif%}
			
			{%if h.ihist_status>0%}
				<div {%if loop.last%}id="last"{%endif%} class="issue-timeline" >
					<div class="user">
			        <a href="#">
			        <img {%if h.user_pic=='nopic.png'%}src="/img/avatar/avatar1x50.jpg"{%else%}src="/userfiles/{{h.user_id}}/pic/{{h.user_pic}}"{%endif%}>
		            <span >{{h.user_username}}</span>
		            </a>
		            
		            {{lng_issues.changed_status}} <button type="button" class="btn btn-space btn-default text-{{h.issuestatus_color}} btn-xs">{{h.issuestatus_name}}</button>
		            <time datetime="{{h.ihist_date|date('Y-m-d')}}T{{h.ihist_date|date('H:i:s')}}" class="timeago"></time>
		           
		            </div>
				</div>
			
			{%endif%}

			{%if h.ihist_priority>0%}
				<div {%if loop.last%}id="last"{%endif%} class="issue-timeline" >
					<div class="user">
			        <a href="#">
			        <img {%if h.user_pic=='nopic.png'%}src="/img/avatar/avatar1x50.jpg"{%else%}src="/userfiles/{{h.user_id}}/pic/{{h.user_pic}}"{%endif%}>
		            <span >{{h.user_username}}</span>
		            </a>
		            
		            {{lng_issues.changed_priority}} <span class="badge badge-{{h.issueprior_color}}" style="margin-bottom: 3px; ">{{h.issueprior_name}}</span>
		            		            <time datetime="{{h.ihist_date|date('Y-m-d')}}T{{h.ihist_date|date('H:i:s')}}" class="timeago"></time>
		           
		            </div>
				</div>
			
			{%endif%}


			{%if h.ihist_cat>0%}
				<div {%if loop.last%}id="last"{%endif%} class="issue-timeline" >
					<div class="user">
			        <a href="#">
			        <img {%if h.user_pic=='nopic.png'%}src="/img/avatar/avatar1x50.jpg"{%else%}src="/userfiles/{{h.user_id}}/pic/{{h.user_pic}}"{%endif%}>
		            <span >{{h.user_username}}</span>
		            </a>
		            
		            {{lng_issues.changed_cat}} <span style="font-weight: 600; font-size:larger;" class="text-{{h.issuecat_color}}">{{h.issuecat_name}}</span>
		            		            <time datetime="{{h.ihist_date|date('Y-m-d')}}T{{h.ihist_date|date('H:i:s')}}" class="timeago"></time>
		           
		            </div>
				</div>
			
			{%endif%}


			{%if h.ihist_deadline_change>0%}
				<div {%if loop.last%}id="last"{%endif%} class="issue-timeline" >
					<div class="user">
			        <a href="#">
			        <img {%if h.user_pic=='nopic.png'%}src="/img/avatar/avatar1x50.jpg"{%else%}src="/userfiles/{{h.user_id}}/pic/{{h.user_pic}}"{%endif%}>
		            <span >{{h.user_username}}</span>
		            </a>
		            
		            {{lng_issues.changed_deadline}} <span style="font-weight: 600; " >{{h.ihist_deadline|date(acs.user_dformat)}} {{h.ihist_deadline|date(acs.user_tformat)}}</span>
		            	<time datetime="{{h.ihist_date|date('Y-m-d')}}T{{h.ihist_date|date('H:i:s')}}" class="timeago"></time>
		           
		            </div>
				</div>
			
			{%endif%}


			{%if h.ihist_prj>0%}
				<div {%if loop.last%}id="last"{%endif%} class="issue-timeline" >
					<div class="user">
			        <a href="#">
			        <img {%if h.user_pic=='nopic.png'%}src="/img/avatar/avatar1x50.jpg"{%else%}src="/userfiles/{{h.user_id}}/pic/{{h.user_pic}}"{%endif%}>
		            <span >{{h.user_username}}</span>
		            </a>
		            
		            {{lng_issues.changed_prj}} <span style="font-weight: 600; ">{{h.project_name}}</span>
		            		            <time datetime="{{h.ihist_date|date('Y-m-d')}}T{{h.ihist_date|date('H:i:s')}}" class="timeago"></time>
		           
		            </div>
				</div>
			
			{%endif%}

			{%if h.ihist_milestone_change>0%}
				<div {%if loop.last%}id="last"{%endif%} class="issue-timeline" >
					<div class="user">
			        <a href="#">
			        <img {%if h.user_pic=='nopic.png'%}src="/img/avatar/avatar1x50.jpg"{%else%}src="/userfiles/{{h.user_id}}/pic/{{h.user_pic}}"{%endif%}>
		            <span >{{h.user_username}}</span>
		            </a>
		            
		            {{lng_issues.changed_miles}} <span style="font-weight: 600; ">{%if h.ihist_milestone%}{{h.prjm_name}}{%else%}{{lng_issues.entire_prj}}{%endif%}</span>
		            		            <time datetime="{{h.ihist_date|date('Y-m-d')}}T{{h.ihist_date|date('H:i:s')}}" class="timeago"></time>
		           
		            </div>
				</div>
			
			{%endif%}

			
		
		{%endfor%}

        
		<form id="fcomment" class="form-horizontal panel-issue-comment">
			<input type="hidden" name="issue_id" value="{{i.issue_id}}">
                <div class="form-group" style="padding-bottom: 0;">
               
                  <div class="col-xs-12">
				  <textarea class="form-control" id="comment" name="comment" rows="6"></textarea>
                  </div>
                </div>
                
                
		</form>
		
		<div class="issue-comment-btn" style="padding-top: 5px;">   	
			<button type="button" id="doSave" class="btn btn-primary btn-lg pull-right" >{{lng_common.btn_comment}}</button>
			<a href="/issues" class="btn btn-default btn-lg pull-left">{{lng_common.btn_cancel}}</a>		
		</div>
		
		{%if ismobphone%}
		<div style="clear:both; height: 20px; ">&nbsp;</div>
		{%endif%}