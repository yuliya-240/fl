{% extends skin~"/common/root.tpl" %}
{%block headcss%}
     	<link rel="stylesheet" href="/assets/lib/redactor2/redactor.css?v={{hashver}}" />
 	<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
 	<link rel="stylesheet" href="/assets/lib/redactor2/redactor.css?v={{hashver}}" />
     	<style>
	     	.redactor-box{
		     	margin-bottom: 0;
		     }	
	    </style> 	
{%endblock%}
{%block content%}

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">Issue #{{i.issue_id}} 
		{%if i.issue_user_id==acs.user_id%}
		 <a href="#"  class="btn btn-danger btn-head pull-right del" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_common.btn_del}}">{{lng_common.btn_del}}</a>
		{%endif%}

		 <a href="/issues" class="btn btn-dark btn-head btn-space pull-right" data-toggle="tooltip" id="bk" data-placement="bottom" data-original-title="{{lng_common.back_list}}"><i class="material-icons btn-head-s">arrow_back</i></a>
	
		<a href="#" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.back_list}}"  class="btn btn-head btn-space  btn-dark pull-right" id="btn-profile-close" style="display: none;">Close</a>
	
	
	</h3>
</div>	
<div class="main-content" id="main-content-box">
	<input type="hidden" id="issue_id" value="{{i.issue_id}}">
	<input type="hidden" id="issue_status" value="{{i.issue_status}}">
	<input type="hidden" id="_idt" value="{{lng_issues.del_issue_title}}">
	<input type="hidden" id="_idm" value="{{lng_issues.del_issue_message}}">
	<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
	<input type="hidden" id="_cb" value="{{lng_common.btn_cancel}}">
	
<div class="row">

	<div class="col-sm-9 " id="i-content">
	

	</div>
	<div class="col-sm-3 " >
		
		<div class="panel panel-default panel-borders" >
			<div class="panel-body">
				
				{%if i.issue_user_id==acs.user_id or role<20%}
				<button {%if i.issue_status!=50%}style="display:none;"{%endif%} id="i-open" class="btn btn-danger btn-block">{{lng_common.open}}</button>
		
				<button {%if i.issue_status==50%}style="display:none;"{%endif%} id="i-close" class="btn btn-success btn-block">{{lng_common.close}}</button>
				{%endif%}
				
				
				<form id="foption" {%if i.issue_status==50%}style="display: none;"{%endif%} class="form-horizontal">
					
				<div class="form-group">
					 <div class="col-xs-12">
					<select class="form-control" id="i-status">
						{%for s in statuses%}
						{%if s.issuestatus_status!=50%}
						<option {%if s.issuestatus_status==i.issue_status%}selected{%endif%} value="{{s.issuestatus_status}}">{{s.issuestatus_name}}</option>
						{%endif%}
						{%endfor%}
					</select>	
					 </div>
					 
				</div>	
				</form>
				
				
				<table class="table">


					<tr>
						<td class="assign-td" style="padding-right: 0; padding-left: 0;">
							
							<p id="edit-deadline-link">
								<a href="#" id="i-ch-deadline" class="edit-issue-option">
									<small>{{lng_issues.deadline}}</small>
									{%if i.issue_user_id==acs.user_id or role<20%}<i style="font-size: larger;" class="icon s7-note pull-right"></i>{%endif%}
								</a>
							</p>
							<div id="i-deadline-date">
				            <span  
				            {%if i.issue_deadline|date(acs.user_dformat)==today|date(acs.user_dformat) and i.issue_status!=50%}class="text-warning"{%endif%} 
				            {%if i.issue_deadline|date(acs.user_dformat)<today|date(acs.user_dformat) and i.issue_status!=50%}class="text-danger"{%endif%} 
				            {%if i.issue_deadline|date(acs.user_dformat)>today|date(acs.user_dformat) and i.issue_status!=50%}class="text-success"{%endif%}>
				            {{i.issue_deadline|date(acs.user_dformat)}} {{i.issue_deadline|date(acs.user_tformat)}}
				            
				            </span>
							</div>
				      	
				           <div class="form-group" id="i-deadline-edit-box" style="display: none;">
							<div class="input-group pull-left">
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
								<input type="text" data-date-format="{{dpformat}}" readonly="" value="{{i.issue_deadline|date(acs.user_dformat)}}" name="deadline_date" id="deadline_date" class="form-control date-picker">
								
							</div>						       


							<div class="input-group pull-left" style="margin-top: 10px;">
								<span class="input-group-addon">
									<i class="fa fa-clock-o bigger-110"></i>
								</span>
								<input type="text" value="{{i.issue_deadline|date(acs.user_tformat)}}"  name="deadline_time" class="form-control time-picker" id="deadline_time">
							</div>						

					       <button id="btn-deadline-cancel" class="btn btn-default btn-sm pull-left" style="margin-top:5px;">Cancel</button>
					       <button id="btn-deadline-save" class="btn btn-primary btn-sm pull-right" style="margin-top:5px;">Save</button>
					       <div class="clearfix"></div>
				           </div>
								
				            </select>    
				            						
						</td>
					</tr>	


					<tr>
						<td class="assign-td" style="padding-right: 0; padding-left: 0;">
							<p id="edit-assign-link">
								<a href="#" id="i-ch-assign" class="edit-issue-option">
									<small>{{lng_issues.assign}}</small>
									{%if i.issue_user_id==acs.user_id or role<20%}
									<i style="font-size: larger;" class="icon s7-note pull-right"></i>
									{%endif%}
								</a>
							</p>

							<p id="cancel-assign-select" style="display: none;">
								<a href="#" id="i-ch-assign-sel" class="edit-issue-option">
									<small>{{lng_common.close}}</small>
									<i style="font-size: larger;" class="icon s7-close-circle pull-right"></i>
								</a>
							</p>

							
					        <a href="#" class="view-u-profile" data-id="{{i.assigid}}" id="i-assign-user">
					        <img {%if i.assignpic=='nopic.png'%}src="/img/avatar/avatar1x50.jpg"{%else%}src="/userfiles/{{i.assigid}}/pic/{{i.assignpic}}"{%endif%}>
				            <span >{{i.assignuser}}</span>
				            
				            </a>	
				            <select class="form-control" id="i-assign-user-sel" style="display: none;">
								
				            </select>    
				            						
						</td>
					</tr>	
					<tr>
						<td style="padding-right: 0; padding-left: 0;">
						<p id="edit-priority-link">
							<a href="#" id="i-ch-priority" class="edit-issue-option">
								<small>{{lng_issues.priority}}</small>
								{%if i.issue_user_id==acs.user_id or role<20%}<i style="font-size: larger;" class="icon s7-note pull-right"></i>{%endif%}
							</a>
						</p>

						<p id="cancel-priority-select" style="display: none;">
							<a href="#" id="i-ch-priority-sel" class="edit-issue-option">
								<small>{{lng_common.close}}</small>
								<i style="font-size: larger;" class="icon s7-close-circle pull-right"></i>
							</a>
						</p>
						
						<div id="i-priority">
						<span class="badge badge-{{i.issueprior_color}}" style="margin-bottom: 3px; font-size:larger;">{{i.issueprior_name}}</span>
						</div>
						
						<select id="i-sel-priority-box" class="form-control" style="display: none;">
							{%for p in priorities%}
							<option {%if p.issueprior_prior==i.issue_priority%}selected{%endif%} value="{{p.issueprior_prior}}">{{p.issueprior_name}}</option>
							{%endfor%}
						</select>	
						</td>
					</tr>	




					<tr>
						<td style="padding-right: 0; padding-left: 0;">
							
							<p id="edit-category-link">
								<a href="#" id="i-ch-category" class="edit-issue-option">
									<small>{{lng_issues.category}}</small>
									{%if i.issue_user_id==acs.user_id or role<20%}<i style="font-size: larger;" class="icon s7-note pull-right"></i>{%endif%}
								</a>
							</p>

							<p id="cancel-category-select" style="display: none;">
								<a href="#" id="i-ch-category-sel" class="edit-issue-option">
									<small>{{lng_common.close}}</small>
									<i style="font-size: larger;" class="icon s7-close-circle pull-right"></i>
								</a>
							</p>

							<div id="i-category">
							<span style="font-weight: 600; font-size:larger;" class="text-{{i.issuecat_color}}">{{i.issuecat_name}}</span>
							</div>

							<select id="i-sel-category-box" class="form-control" style="display: none;">
								{%for p in cats%}
								<option {%if p.issuecat_cat==i.issue_cat%}selected{%endif%} value="{{p.issuecat_cat}}">{{p.issuecat_name}}</option>
								{%endfor%}
							</select>	
							
							
						</td>
					</tr>	
					<tr>
						
						<td style="padding-right: 0; padding-left: 0;" >
							
							<p id="edit-project-link">
								<a href="#" id="i-ch-project" class="edit-issue-option">
									<small>{{lng_issues.project}}</small>
									{%if i.issue_user_id==acs.user_id or role<20%}<i style="font-size: larger;" class="icon s7-note pull-right"></i>{%endif%}
								</a>
							</p>

							<p id="cancel-project-select" style="display: none;">
								<a href="#" id="i-ch-project-sel" class="edit-issue-option">
									<small>{{lng_common.close}}</small>
									<i style="font-size: larger;" class="icon s7-close-circle pull-right"></i>
								</a>
							</p>
							
							<div id="i-project">
								<a href="/projects/view/{{i.project_id}}" span style="font-weight: 600; font-size:larger;">{{i.project_name}}</a>
							</div>	

							<select id="i-sel-project-box" class="form-control" style="display: none;">
								<option {%if i.issue_project_id==defprj%}selected{%endif%} value="{{defprj}}">{{lng_issues.select_project}}</option>
								{%for p in projects%}
								<option {%if p.project_id==i.issue_project_id%}selected{%endif%} value="{{p.project_id}}">{{p.project_name}}</option>
								{%endfor%}
							</select>	

							
						</td>
					</tr>	
				
					<tr>
						<td style="padding-right: 0; padding-left: 0;">
							<p id="edit-miles-link">
								<a href="#" id="i-ch-miles" class="edit-issue-option">
									<small>{{lng_issues.milestones}}</small>
									{%if i.issue_user_id==acs.user_id or role<20%}<i style="font-size: larger;" class="icon s7-note pull-right"></i>{%endif%}
								</a>
							</p>

							<p id="cancel-miles-select" style="display: none;">
								<a href="#" id="i-ch-miles-sel" class="edit-issue-option">
									<small>{{lng_common.close}}</small>
									<i style="font-size: larger;" class="icon s7-close-circle pull-right"></i>
								</a>
							</p>

							<div id="i-miles">
								{%if i.issue_milestone_id%}
								<span style="font-weight: 600; font-size:larger;">{{i.prjm_name}}</span>
								{%else%}
								<span style="font-weight: 600; font-size:larger;">{{lng_issues.entire_prj}}</span>
								{%endif%}
							</div>
							
							<select id="i-sel-miles-box" class="form-control" style="display: none;">
								<option value="0">{{lng_issues.entire_prj}}</option>
								{%for m in milestones%}
								<option {%if m.prjm_id==i.issue_milestone_id%}selected{%endif%} value="{{m.prjm_id}}">{{m.prjm_name}}</option>
								{%endfor%}
							</select>	
							
						</td>
					</tr>	
					

				</table>	

			</div>
		</div>	
	</div>
</div>	


	<div style="margin-top: 50px; background-color: #f0f0f0;">&nbsp;</div>
</div>

{%endblock%}

{%block js%}

	<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>	
	<script src="/assets/lib/date-time/bootstrap-timepicker.js"></script>


	<script type="text/javascript" src="/assets/lib/redactor2/redactor.js?v={{hashver}}"></script>
   <script src="/js/dev/issue_view.js"></script>
{%endblock%}
