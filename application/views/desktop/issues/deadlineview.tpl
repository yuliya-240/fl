<span  
{%if issue_deadline|date(acs.user_dformat)==today|date(acs.user_dformat) and issue_status!=50%}class="text-warning"{%endif%} 
{%if issue_deadline|date(acs.user_dformat)<today|date(acs.user_dformat) and issue_status!=50%}class="text-danger"{%endif%} 
{%if issue_deadline|date(acs.user_dformat)>today|date(acs.user_dformat) and issue_status!=50%}class="text-success"{%endif%}>

{{issue_deadline|date(acs.user_dformat)}} {{issue_deadline|date(acs.user_tformat)}}

</span>