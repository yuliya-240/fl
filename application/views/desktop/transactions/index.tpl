{% extends skin~"/common/root.tpl" %}
{%block headcss%}
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <style>
        .input-group-btn .btn {
            height: 42px;
        }
    </style>
{%endblock%}

{%block content%}

    <div class="page-head">
        <h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_transactions.title}}
            <a href="#" class="btn btn-success btn-head btn-space pull-right g-l" data-go="/invoices/add" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_invoices.btn_add}}" >Add Income</a>
            <a href="#" class="btn btn-danger btn-head btn-space pull-right g-l" data-go="/expenses/add" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_expenses.add_exp}}" > Add Expense</a>

        </h3>
    </div>
    <input type="hidden" id="moment-format" value="{{momentformat}}">
    <input type="hidden" id="startDate" value="{{startdate}}">
    <input type="hidden" id="endDate" value="{{enddate}}">

    <form id="foptions">

        <input type="hidden" id="type" name="type" value="{{ ttype }}">

        {%if 0 in t_method%}
            <input class="tmethod-val" id="tma" type="hidden" name="tmethod[]" value="0">
        {%endif%}

        {%for m in tmethodlist%}

             {%if m in t_method%}

                <input class="tmethod-val" id="tt{{m}}" type="hidden" name="tmethod[]" value="{{m}}">

            {%endif%}
        {%endfor%}




    </form>

    <div class="main-content">
        <div class="row">
            <div class="col-sm-10">

                <form id="fsearch"  class="form-horizontal group-border-dashed">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon " style="background-color:#ccc; color:#fff; " ><span class="s7-search" style="font-size: 24px;"></span></span>
                                <input class="form-control" id="s" placeholder="{{lng_transaction.search_ph}}" {%if searchstr%}value="{{searchstr}}"{%endif%} type="text" style="border-width:1px;" >
                                <span class="input-group-addon " id="tdrp"  style="background-color:#4e91ff; color:#fff; cursor: pointer;" ><span class="s7-date" style="font-size: 24px;"></span></span>

                            </div>
                        </div>
                    </div>
                </form>
                <form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                    <div class="panel panel-default panel-borders">

                        <div class="panel-body">

                            <div id="d-content">

                            </div>

                        </div>
                    </div>
                </form>

            </div>

            <div class="col-sm-2" style="padding-top: 10px;">
                <legend class="blue" >Filter</legend>
                <div class="btn-group-vertical btn-block ">
                    <button type="button" class="btn btn-default type {%if ttype==0%}active{%endif%}"  data-val="0">All Types</button>
                    <button type="button" class="btn btn-default type {%if ttype==1%}active{%endif%}"  data-val="1">Income</button>
                    <button type="button" class="btn btn-default type {%if ttype==2%}active{%endif%}"  data-val="2">Expenses</button>
                </div>

                <div class="btn-group-vertical btn-block ">

                    <button type="button" class="btn btn-default tma {%if 0 in t_method%}active{%endif%}"  data-val="0">All Methods</button>

                    {%for m in tmethodlist%}

                        <button type="button" class="btn btn-default tm{%if m.paymethod_method in t_method%}active{%endif%}" data-val="{{m.paymethod_method}}" >{{m.paymethod_name}}</button>
                    {%endfor%}

                </div>
                <button class="btn btn-primary btn-block doApplyFilter">Apply Filter</button>
            </div>
        </div>

    </div>

    <div id="view-details" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
        <div style="width: 100%;" class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">details</h3>
                </div>

                <div class="modal-body" id="view-details-body" >

                </div>
                <div class="modal-footer" >
                    <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

                </div>

            </div>
        </div>
    </div>


{%endblock%}

{%block footer%}
    <div id="megafooter">
    </div>
{%endblock%}

{%block js%}
    <script src="/assets/lib/date-time/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script src="/js/dev/transactions.js?v={{hashver}}"></script>
{%endblock%}
