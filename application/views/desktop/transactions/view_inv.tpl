<input type="hidden" id="inv_id" value="{{ inv.inv_id }}">

    <div class="panel panel-default panel-borders paid">
        <div class="panel-body">
            <div class="invoice" style="background: none !important; padding: 20px 20px;">
                <div class="row invoice-header">
                    <div class="col-xs-7">
                        <div class="invoice-logo">
                            {%if acs.set_logo!="nologo.png"%}
                                <img class="img-responsive" src="/userfiles/{{acs.user_id}}/pic/{{acs.set_logo}}">
                            {%endif%}
                        </div>
                    </div>
                    <div class="col-xs-5 invoice-order"><span class="invoice-id">{{lng_invoices.invoice}} #{{inv.inv_num}}</span><span class="incoice-date">{{inv.inv_date|date(acs.user_dformat)}}</span></div>
                </div>
                <div class="row invoice-data">
                    <div class="col-sm-5 invoice-person" id="inv-my-addr">

                        <span class="name">{{acs.set_billing_name}} </span>
                        <span>{{acs.set_billing_street}}</span>
                        <span>{{acs.set_billing_city}}{%if acs.set_billing_state%}, {{acs.set_billing_state}}{%endif%} {{acs.set_billing_zip}}</span>
                        <span>{{mycountry}}</span>

                    </div>
                    <div class="col-sm-2 invoice-payment-direction"><i class="icon mdi mdi-chevron-right"></i></div>
                    <div class="col-sm-5 invoice-person" id="recipient-addr">

                        <span class="name"> {{inv.inv_name}} </span>
                        <span>{{inv.inv_street}}</span>
                        <span>{{inv.inv_city}}{%if inv.inv_state%}, {{inv.inv_state}}{%endif%} {{inv.inv_zip}}</span>
                        <span>{{country}}</span>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="invoice-details">
                            <tr>
                                <th style="width:60%">{{lng_invoices.desc}}</th>
                                <th style="width:15%" class="hours">{{lng_invoices.rate}}</th>
                                <th style="width:10%" class="hours">{{lng_invoices.qty}}</th>
                                <th style="width:15%" class="amount">{{lng_invoices.am}}</th>
                            </tr>
                            {%for l in lines%}
                                <tr>
                                    <td class="description">{{l.invline_srv}}
                                        <br>{{l.invline_desc}}
                                    </td>
                                    <td class="hours">{{l.invline_rate}}</td>
                                    <td class="hours">{{l.invline_qty}}</td>
                                    <td class="amount">{{l.invline_total}}</td>
                                </tr>
                            {%endfor%}
                            <tr>
                                <td></td>

                                <td colspan="2" class="summary">{{lng_invoices.subtotal}} <small style="font-size: smaller">{{inv.inv_currency}}</small>


                                </td>
                                <td class="amount">{{inv.inv_subtotal}}</td>
                            </tr>
                            {%if inv.inv_taxes_cash>0%}
                                <tr>
                                    <td></td>

                                    <td colspan="2" class="summary">{{lng_invoices.taxes}}  <small style="font-size: smaller">{{inv.inv_currency}}</small></td>
                                    <td class="amount">{{inv.inv_taxes_cash}}</td>
                                </tr>
                            {%endif%}
                            {%if inv.inv_discount_percentage>0%}

                                <tr>
                                    <td></td>

                                    <td colspan="2" class="summary">{{lng_invoices.discount}} ({{inv.inv_discount_percentage}}%)</td>
                                    <td class="amount">{{inv.inv_discount_cash}}</td>
                                </tr>
                            {%endif%}

                            <tr>
                                <td></td>

                                <td colspan="2" class="summary total" >
                                    {{lng_invoices.total}} <small style="font-size: smaller">{{inv.inv_currency}}</small>
                                </td>

                                <td class="amount total-value" style="color:#000;">{{inv.inv_total}}</td>
                            </tr>

                            <tr>
                                <td></td>

                                <td colspan="2" class="summary">{{lng_invoices.paid}} <small style="font-size: smaller">{{inv.inv_currency}}</small></td>
                                <td class="amount text-success">{{inv.inv_paid_amount}}</td>
                            </tr>
                            
                        </table>
                    </div>
                </div>

                {%if inv.inv_terms %}
                    <div class="row" style="margin-top: 20px; ">
                        <div class="col-md-12 invoice-message" style="margin-bottom: 20px;"><span class="title">{{lng_invoices.terms}}</span>
                            <p>{{inv.inv_terms}}</p>
                        </div>
                    </div>
                {%endif%}

                {%if inv.inv_notes%}
                    <div class="row" >
                        <div class="col-md-12 invoice-message" style="margin-bottom: 20px;"><span class="title">{{lng_invoices.notes}}</span>
                            <p>{{inv.inv_notes}}</p>
                        </div>
                    </div>
                {%endif%}
            </div>
        </div>
    </div>

    {%if flist%}
        <div class="panel panel-default panel-borders ">
            <div class="panel-heading">Attached Files</div>
            <div class="panel-body">


                <ul id="plist">
                    {%for f in flist%}
                        <li style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px #ccc dotted;" id="attitem{{f.invatt_id}}">
                            <div class="pull-left" style="margin-right: 5px; padding-top: 5px;">{%if f.invatt_type=="image"%}<span class="s7-photo ic-flist"></span>{%else%} <span class="s7-file ic-flist"></span>{%endif%}</div>

                            <div class="pull-left" style="padding-top: 7px;">	<a href="{{ihost}}userfiles/{{acs.user_id}}/pic/{{f.invatt_fname}}" {%if f.invatt_type=="image"%}class="venobox"{%else%}target="_blank"{%endif%} >{{f.invatt_fname}}</a></div>


                            <div style="clear: both;"></div>

                        </li>


                    {%endfor%}
                </ul>

            </div>
        </div>
    {%endif%}

