<input type="hidden" id="exp_id" value="{{ exp.exp_id }}">
<table class="table  table-hover">
    <tbody>
    <tr>
        <td class="tbl-pointer " width="20%">{{lng_expenses.date}}</td >
        <td class="tbl-pointer ">{{exp.exp_date|date(acs.user_dformat)}}</td>
    </tr>
    <tr>
        <td class="tbl-pointer " width="20%">{{lng_expenses.cat}}</td >
        <td class="tbl-pointer">
            {%if exp.exp_category_parent%}
                {{exp.parentname}} > {{exp.catname}}
            {%else%}
                {{exp.catname}}
            {%endif%}</td>
    </tr>
    <tr>
        <td class="tbl-pointer " width="20%">{{lng_expenses.vendor}}</td >
        <td class="tbl-pointer ">{{ exp.exp_vendor }}</td>
    </tr>
    <tr>
        <td class="tbl-pointer " width="20%">{{lng_expenses.desc}}</td >
        <td class="tbl-pointer ">{{ exp.exp_desc }}</td>
    </tr>

    <tr>
        <td class="tbl-pointer " width="20%">{{lng_expenses.am}}</td >
        <td class="tbl-pointer "><span style="font-weight: 600">{{exp.exp_amount|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{exp.exp_currency}}</small>
            {%if exp.exp_currency!=acs.user_currency%}
                <br><small>{{exp.exp_amount_base|number_format(2,acs.user_dsep,acs.user_tsep)}}</small> <small>{{acs.user_currency}}</small>
            {%endif%}</td>
    </tr>
    <tr>
        <td class="tbl-pointer " width="20%">{{lng_expenses.paymethod}}</td >
        <td class="tbl-pointer ">{%if exp.exp_method%}{{exp.paymethod_name }}{%endif%}</td>
    </tr>
    <tr>
        <td class="tbl-pointer " width="20%">{{lng_expenses.tax}}</td >
        <td class="tbl-pointer ">
            <span style="font-weight: 600">{{exp.exp_tax1_cash|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{exp.exp_currency}}</small>
        </td>
    </tr>
    </tbody>
    <tfoot>

    {% if exp.exp_pic != "nopic.png" %}
        <tr>
            <td colspan="2"><span class="col-sm-12 title" style="text-align: center; font-weight: 600;">{{lng_expenses.receipt}}</span>
                <img class="img-responsive center-block" src="/userfiles/{{exp.exp_user_id}}/pic/{{exp.exp_pic}}">
            </td>

        </tr>
    {%endif%}
    </tfoot>
</table>


