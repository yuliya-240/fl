{%if records%}
    <div class="table-responsive">
        <form id="flist">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th width="1%" class="text-center hidden-xs"></th>
                    <th>Type</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Method</th>
                    <th>Amount</th>
                    <th width="1%"></th>
                </tr>
                </thead>
                <tbody>
                {%for r in records%}
                    <tr>
                        <td class="text-center hidden-xs" style="vertical-align:middle;"></td>
                        <td class="tbl-pointer g-l" style="font-weight: 600">
                            {% if r.t_inv_id != 0 %}
                                <span class="badge badge-success">Income</span>
                            {% else %}
                                <span class="badge badge-danger">Expenses</span>
                            {% endif %}
                        </td>

                        <td class="tbl-pointer g-l">{{r.t_date|date(acs.user_dformat)}}</td>
                        <td class="tbl-pointer g-l" style="font-weight: 600">
                            {% if r.t_exp_id != 0 %}
                                {#{%if r.exp_category_parent%}#}
                                    {#<p value="{{ r.t_exp_id }}">{{r.parentname}} > {{r.catname}}</p>#}
                                {#{%else%}#}
                                    {#<p value="{{ r.t_exp_id }}">{{exp.catname}}</p>#}
                                {#{%endif%}#}
                                {{ r.t_notes }}
                            {% else %}
                                Invoice # <a>{{ r.inv_num }}</a> <br> {{ r.inv_name }}

                            {% endif %}
                        </td>
                        <td class="tbl-pointer g-l">
                        {%if r.t_method%}{{ r.paymethod_name }}{%endif%}

                        </td>
                        <td class="tbl-pointer g-l" style="text-align: right;"><span style="font-weight: 600">{{r.t_amount|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{r.t_currency}}</small>
                            {%if r.t_currency!=acs.user_currency%}
                                <br><small>{{r.t_amount_base|number_format(2,acs.user_dsep,acs.user_tsep)}}</small> <small>{{acs.user_currency}}</small>
                            {%endif%}
                        </td>


                        <td class="tbl-pointer">
                            {#<a href="#" class="view" data-id="{{ r.t_id }}"><i class="material-icons">visibility</i></a>#}
                            {% if r.t_exp_id!=0 %}
                            <a href="#" class="view" data-id="{{r.t_exp_id}}@exp"><i class="material-icons">visibility</i></a>
                            {% else %}
                            <a href="#" class="view" data-id="{{r.t_inv_id}}@inv"><i class="material-icons">visibility</i></a>
                            {% endif %}
                        </td>
                    </tr>
                {%endfor%}
                </tbody>
                <tfoot>


                <tr style="background-color: #fff7eb;">
                    <td colspan="5"  >Total</td>
                    <td  style="text-align: right;  "><span style="font-weight: 600; ">{{totalPage|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{acs.user_currency}}</small></td>
                    <td style=" background-color: #fff7eb;"></td>
                </tr>

                </tfoot>
            </table>


        </form>
    </div>
{%else%}
    <div class="text-center">
        {{lng_common.norecords}}
    </div>

{%endif%}