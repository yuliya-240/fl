{% extends skin~"/common/root.tpl" %}
{%block preheadcss%}
     <link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.min.css"/>
    {%endblock%}
{%block headcss%}
<link rel="stylesheet" href="/css/inv.css" type="text/css"/>
<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
<link href="/assets/lib/switch/css/bootstrap-switch.min.css" rel="stylesheet" />
<link href="/assets/lib/venobox/venobox.css" rel="stylesheet" />

<style>
	

.photo{
	border:1px solid #ccc;
	}
	
#plist{
	list-style-type: none;
	}	
	
.ic-flist{
	font-size: 22px;
}	
	
.form-horizontal .form-group {
	padding: 4px 0 !important; 
	}
</style>	
{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_invoices.title_new}} </h3>
</div>	

<div class="main-content">
  <div class="row">
	  <div class="col-xs-12 col-md-10 col-md-offset-1">
		
		<input type="hidden" id="btnYes" value="{{lng_common.btn_yes}}">
		<input type="hidden" id="btnNo" value="{{lng_common.btn_no}}">
		<input type="hidden" id="_tit" value="{{lng_invoices.errtit}}">
		<input type="hidden" id="_txt" value="{{lng_invoices.errtxt}}">
		<input type="hidden" id="_ctit" value="{{lng_invoices.cerrtit}}">
		<input type="hidden" id="_ctxt" value="{{lng_invoices.cerrtxt}}">
		<input type="hidden" id="_ltit" value="{{lng_invoices.lerrtit}}">
		<input type="hidden" id="_ltxt" value="{{lng_invoices.lerrtxt}}">
		
		<form id="finvoice" class="form-horizontal">
			<input type="hidden" name="inv_id" id="inv_id" value="{{inv.inv_id}}">	
			 <input type="hidden" name="er_type"	value="6">		
		<div class="row form-horizontal" style="margin-bottom: 5px;">
			
			<div class="col-xs-12 col-sm-4">
				 <select class="form-control" id="inv_lang" name="inv_lang" >
					 {%for l in langs%}
					 <option {%if l.lang_iso==cinvlang%}selected{%endif%} value="{{l.lang_iso}}">{{l.lang_name}}</option>
					 {%endfor%}
				</select>
			</div>	
			
			<div class="col-xs-12 col-sm-4">
				<select class="form-control" id="inv_currency" name="inv_currency" >
					 
					 {%for c in currencies%}
					 <option {%if c.currency_iso==inv.inv_currency%}selected{%endif%} value="{{c.currency_iso}}"> {{c.currency_iso}} {{c.currency_name}}</option>
					 {%endfor%}
				</select>

				<div class="input-group xs-mb-15" id="rate-box" style="margin-top:5px; margin-bottom:0 !important; {%if inv.inv_currency==acs.user_currency%} display: none;{%endif%}" >
					<span class="input-group-addon" id="currency-label">{{inv.inv_currency}}/{{acs.user_currency}}</span>
                    <input type="text" id="inv_exchange_rate" autocomplete="off" name="inv_exchange_rate" class="form-control xr" required="" data-errorbox="#errrate" data-msg-required="{{lng_common.fr}}" value="{{inv.inv_exchange_rate}}">
                </div>
				<div id="errrate" style="color:red;"></div>
					 
			</div>
	
			<div class="col-xs-12 col-sm-4" align="right">
				 <select class="form-control" id="inv_prj" name="inv_prj" >
					 <option value="0">Not assigned to Project</option>
					 {%for p in prj%}
					 
					 <option {%if p.project_id==inv.inv_prj%}selected{%endif%} value="{{p.project_id}}">{{p.project_name}}</option>
					 {%endfor%}

					 </select>

			</div>	
		</div>

		<input type="hidden" id="inv_name" name="inv_name" value="{{inv.inv_name}}" >
		<input type="hidden" id="inv_street" name="inv_street" value="{{inv.inv_street}}">
		<input type="hidden" id="inv_city" name="inv_city" value="{{inv.inv_city}}">
		<input type="hidden" id="inv_state" name="inv_state" value="{{inv.inv_state}}">
		<input type="hidden" id="inv_zip" name="inv_zip" value="{{inv.inv_zip}}">
		<input type="hidden" id="inv_country" name="inv_country" value="{{inv.inv_country}}">
		

			
		<div class="panel panel-default panel-borders">
	        <div class="panel-body">

			<div class="row">
						
				<div class="col-sm-6">
					<div class="form-horizontal">
				    	<div class="form-group">
				    		<div class="col-sm-10" style="padding-right: 4px;">
		
						    	<select id="client" name="client" class="select2">
							    	
						    		<option value="0">{{lng_invoices.selto}}</option>
						    		{%if clients%}
						    		<optgroup label="{{lng_menu.clients}}">
						    		{%for c in clients%}
						    			<option {%if inv.inv_client_id > 0 and inv.inv_client_id == c.client_id%}selected{%endif%} value="{{c.client_id}}@cli">{{c.client_name}}</option>
						    		{%endfor%}
						    		</optgroup>
						    		{%endif%}
						    		{%if collabs%}
						    		<optgroup label="{{lng_menu.colleague}}">
						    		{%for c in collabs%}
						    			<option {%if inv.inv_collab_id > 0 and inv.inv_collab_id == c.cb_id%}selected{%endif%} value="{{c.cb_id}}@usr">{{c.cb_billing_name}}</option>
						    		{%endfor%}
						    		</optgroup>
						    		{%endif%}
						    		
						    	</select>
					    	
					    	
				    		</div>
				    		<div class="hidden-xs col-sm-1" style="padding-left: 0;">
					    		
					    		<button class="btn btn-sm btn-success" id="a-c" style="padding: 6px 9px;"><i class="material-icons">person_add</i></button>
				    		</div>
					    </div>
					</div>
						
					    <address id="address" style="margin-top:20px; font-size: 16px; ">
					    
					    <strong>{{inv.inv_name}} </strong><br>
					    {{inv.inv_street}} <br>
					    {{inv.inv_city}}{%if inv.inv_state%}, {{inv.inv_state}}{%endif%} {{inv.inv_zip}}<br>
					    {{inv.inv_country}}<br>
					    <a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chAddr" >Edit Address</a>
					  
					    </address>
				
				</div>
				
				<div class="col-sm-6">
					<div class="form-horizontal">
						{#
						<div class="form-group">
							<label class="col-sm-6 control-label no-padding-right invlang_invoice">{{invlang.invoice}}</label>
							<div class="col-sm-6">
								<div class="input-group">
									<span class="input-group-addon">#</span>
									<input type="text" class="form-control inum" maxlength="7" required="" data-errorbox="#errnum" data-msg-required="{{lng_invoices.num_req}}" name="inv_num"  value="{{inv.inv_num}}" >
								</div>							
		
								<div id="errnum" style="color:red;"></div>
							</div>
						</div>
		
						<div class="form-group">
							<label class="col-sm-6 control-label no-padding-right invlang_di">{{invlang.di}}</label>
							
								<div class="col-sm-6" >
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
										<input type="text" data-date-format="{{dpformat}}" readonly="" value="{{inv.inv_date|date(acs.user_dformat)}}" name="date" class="form-control date-picker">
										
									</div>							
								</div>
						</div>
						#}
				    </div> 
					
			   </div>
			</div>
			
			<div class="row" style="margin-top:40px; margin-bottom: 20px;">		
				<div class="col-xs-12">
				<table class="invoice-details" id="inv-lines">
					<tbody>
						<tr>
							<th style="width:60%" class="invlang_desc">{{invlang.desc}}</th>
							<th class="hours invlang_rate" style="width:10%; text-align: center;">{{invlang.rate}}</th>
							<th class="hours invlang_qty" style="width:9%; text-align: center;">{{invlang.qty}}</th>
							<th class="hours invlang_tax" style="width:10%; text-align: center;">{{invlang.tax}} %</th>
							<th class="amount invlang_am" style="width:10%">{{invlang.am}}</th>		
							{#<th style="width:1%"></th>	#}				
						</tr>	
						
						{%for l in lines%}
						<tr id="line{{id}}">
							<td class="description td-a-t" >
								<input type="text" name="invrow_srv[]" placeholder="{{lng_invoices.ph_srv}}" id="name{{l.inv_id}}" data-id="{{l.inv_id}}" data-errorbox="#errsrvline{{l.inv_id}}" data-msg-required="{{lng_invoices.srv_req}}"  class="form-control td-b linefilter" value="{{l.invline_srv}}" >
								
								<textarea id="irdesc{{l.inv_id}}" style="resize: none;" name="invrow_desc[]" rows="2" placeholder="{{lng_invoices.ph_desc}}" class="form-control td-b td-t-n-b">{{l.invline_desc}}</textarea>
								<div id="errsrvline{{l.inv_id}}" style="color:red;"></div>
							</td>
							<td class="hours td-a-t" style="text-align: center;">
								<input type="text" id="irrate{{l.inv_id}}" data-id="{{l.inv_id}}" name="invrow_rate[]" class="form-control td-b td-l-n-b dec" placeholder="0.00" value="{{l.invline_rate}}">
								
							</td>
							<td class="hours td-a-t" >
								<input type="text"  id="irqty{{l.inv_id}}" data-id="{{l.inv_id}}" name="invrow_qty[]" class="form-control td-b td-l-n-b td-r-n-b dec"  value="{{l.invline_qty}}">
								
							</td>
						
							<td class="hours td-a-t" >
								<input type="text" id="irtax1{{l.inv_id}}" data-id="{{l.inv_id}}" name="invrow_tax1[]" class="form-control td-b  tax"  value="{{l.invline_tax1_percent}}">
								<input type="text"  id="irtax2{{l.inv_id}}" data-id="{{l.inv_id}}" name="invrow_tax2[]" class="form-control td-b td-t-n-b tax" value="{{l.invline_tax2_percent}}" >
								<input type="hidden" id="taxcash1{{l.inv_id}}"  class="tax1" data-id="{{l.inv_id}}" name="invrow_tax1_cash[]" value="{{l.invline_tax1_cash}}"> 
								<input type="hidden" id="taxcash2{{l.inv_id}}"  class="tax2" data-id="{{l.inv_id}}" name="invrow_tax2_cash[]" value="{{l.invline_tax2_cash}}"> 
							</td>
						
							<td class="amount td-a-t" >
								<input type="text" value="{{l.invline_total}}" name="invrow_total[]" id="irtotal{{l.inv_id}}" data-id="{{l.inv_id}}" class="form-control td-b td-l-n-b i-txt-r linetotal pull-left" readonly=""  >
								<button class="btn btn-space btn-default text-danger delline" type="button" data-id="{{l.inv_id}}" style="padding-top: 7px !important; border: none !important; position: absolute; background: none !important;  padding-left: 6px;" ><span class="s7-close-circle" style="font-size:28px; font-weight: 700;"></span></button>
						
							</td>
								
					
						</tr>
						
						{%endfor%}
					</tbody>	
				</table>	
				</div>	
			</div>		
		
			<div class="row">
				<div class="col-xs-12 text-center m-t-15">
				<button type="button" data-mob="0" class="btn btn-sm btn-success addline"><i class="ace-icon fa fa-plus"></i> {{lng_invoices.add_line}}</button>
				</div>
			</div>
						        
			<br>     
			<div class="row">
				<div class="col-sm-7"></div>	

				<div class="col-sm-5">
					<table class="table">
					<tr>
						<td width="60%"><span class="invlang_subtotal">{{invlang.subtotal}}</span> <span class="sel-curr">{{inv.inv_currency}}</span></td>
						<td width="40%" align="right" id="subtotal-txt" >{{inv.inv_subtotal}}</td>
						<input type="hidden" name="subtotal" id="subtotal" value="{{inv.inv_subtotal}}">
						<input type="hidden" name="discount_cash" id="disc-cash" value="{{inv.inv_discount_cash}}">
					</tr>	
					<tr>
						<td><span class="invlang_taxes">{{invlang.taxes}}</span> <span class="sel-curr">{{inv.inv_currency}}</span></td>
						<td align="right" id="totaltax-txt">{{inv.inv_taxes_cash}}</td>
						<input type="hidden" name="tataltax_cash" id="totaltax" value="{{inv.inv_taxes_cash}}">
					</tr>	
					<tr>
						<td class="invlang_discount">{{invlang.discount}} </td>
						<td align="right" style="padding-right: 0;">
							<div class="input-group input-group-sm">
								<span class="input-group-addon">%</span>
								<input style="text-align: right;" class="form-control pricedis dis" id="discount" value="{{inv.inv_discount_percentage}}" name="discount"  type="text" >
							</div>
						</td>
					</tr>	

					
					<tr class="" style="background:#e6f9ff; font-weight: 600;">
						<td style="font-weight: 600;">
							<span class="invlang_total">{{invlang.total}}</span> <span class="sel-curr">{{inv.inv_currency}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted">{{lng_invoices.total}} {{acs.user_currency}}</span>
							</div>	
							
						</td>
						<td style="font-weight: 600;" align="right" >
							<span id="total-txt">{{inv.inv_total}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted" id="total-txt-base">{{inv.inv_total_base}}</span>
							</div>	
	
						</td>
						<input type="hidden" name="total" id="total" value="{{inv.inv_total}}">
						<input type="hidden" name="total_base" id="total_base" value="{{inv.inv_total_base}}">
					</tr>	

					<tr class="" style="background: #ccffeb; font-weight: 600;">
						<td style="font-weight: 600;">
							<span class="invlang_paid">{{invlang.paid}}</span> <span class="sel-curr">{{inv.inv_currency}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted">{{lng_invoices.paid}} {{acs.user_currency}}</span>
							</div>	
						
						</td>
						<td style="font-weight: 600;" align="right" >
							<span>{{inv.inv_paid_amount}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted">{{inv.inv_paid_amount_base}}</span>
							</div>	
						
						</td>
						<input type="hidden" name="paid" id="paid" value="{{inv.inv_paid_amount}}">
						<input type="hidden" name="paid_base" id="paid_base" value="{{inv.inv_paid_amount_base}}">
					</tr>	
					<tr class="" style="background: #ffeaea;">
						<td style="font-weight: 600;">
							<span class="invlang_due">{{invlang.due}}</span> <span class="sel-curr">{{inv.inv_currency}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted">{{lng_invoices.due}} {{acs.user_currency}}</span>
							</div>	
						
						</td>
						<td style="font-weight: 600;" align="right"  style="font-weight: 600;">
							<span id="due-txt">{{inv.inv_total_due}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span id="due-txt-base" class="text-muted" id="">{{inv.inv_total_due_base}}</span>
							</div>	
						
						</td>
						<input type="hidden" name="due" id="due" value="{{inv.inv_total_due}}">
						<input type="hidden" name="due_base" id="due_base" value="{{inv.inv_total_due_base}}">
					</tr>	

					</table>	
				</div>	

			</div>	
			
			<div class="row">
				<div class="col-sm-6">
				<label class="invlang_terms">{{invlang.terms}}</label>
				<textarea class="form-control" style="resize: none;" name="inv_terms">{{inv.inv_terms}}</textarea>	
				</div>	
				<div class="col-sm-6">
				<label class="invlang_notes">{{invlang.notes}}</label>
				<textarea class="form-control" style="resize: none;" name="inv_notes">{{inv.inv_notes}}</textarea>	

				</div>	
			</div>	
			
			  
	        </div>
		</div> 

		<div class="panel panel-default panel-borders">

			<div class="panel-heading">{{lng_invoices.attf}}</div>
	        <div class="panel-body">
		        
			<ul id="plist">
				{%for f in flist%}
					<li style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px #ccc dotted;" id="attitem{{f.invatt_id}}">
						<div class="pull-left" style="margin-right: 5px; padding-top: 5px;">{%if f.invatt_type=="image"%}<span class="s7-photo ic-flist"></span>{%else%} <span class="s7-file ic-flist"></span>{%endif%}</div> 
						
						<div class="pull-left" style="padding-top: 7px;">	<a href="{{ihost}}userfiles/{{acs.user_id}}/pic/{{f.invatt_fname}}" {%if f.invatt_type=="image"%}class="venobox"{%else%}target="_blank"{%endif%} >{{f.invatt_fname}}</a></div>
						
						<div class="pull-right">
							<button class="btn btn-sm btn-space btn-default text-danger delatt" style="border: none !important;" type="button" data-id="{{f.invatt_id}}"  ><span class="s7-trash" style="font-size:22px; font-weight: 700;"></span></button>
						</div>	
						
						<div style="clear: both;"></div>
						<input type="hidden" name="attfile[]" id="attfile{{f.invatt_id}}" value="{{f.invatt_fname}}">
						<input type="hidden" name="attfiletype[]" id="attfiletype{{f.invatt_id}}" value="{{f.invatt_type}}">
					</li>

				
				{%endfor%}
			</ul>				
		        
			<div class="progress  progress-striped active" id="progress1" style="display:none;">
				<div id="progressSlide" style="width: 0%;" class="progress-bar progress-bar-success">{{lng_invoices.loader}}</div>
			</div>
				
			<div id="containerpic" class="text-center">
				
				<button id="pickfiles" class="btn btn-md btn-success"  ><i class="fa fa-cloud-upload" aria-hidden="true"></i>&nbsp; {{lng_invoices.attach}}</button>
			
			</div>
		        
	    </div>
		</div>
		
		
		<div class="panel panel-default panel-borders">
		
			<div class="panel-heading">{{lng_invoices.rec}} </div>
		    <div class="panel-body">
				<input type="hidden" name="er_id" value="{{inv.er_id}}">
		
		        <div id="rec-on">
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" >{{lng_invoices.next_date_issue}}</label>
						
						<div class="col-sm-3" >
							<div class="input-group">
								<input type="text" data-date-format="{{dpformat}}" readonly value="{{inv.er_date_start|date(acs.user_dformat)}}" name="er_date_start" class="form-control date-picker">
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>							
						</div>
					</div>
		
		
					<div class="form-group">
					 	<label class="col-sm-3 control-label no-padding-right"> {{lng_invoices.freq}}</label>
					 	<div class="col-sm-4">
				 			<select id="recOptions" name="er_repeat"  class="form-control">
					
								<option {%if inv.er_repeat=="w"%}selected{%endif%} value="w">{{lng_invoices.weekly}}</option>
								<option {%if inv.er_repeat=="m"%}selected{%endif%} value="m">{{lng_invoices.monthly}}</option>
								<option {%if inv.er_repeat=="y"%}selected{%endif%} value="y">{{lng_invoices.yearly}}</option>
				 			
				 			</select>
					 	</div>
					</div>	
		
					<div id="recurrenceOptions" >
							
						<div id="weekOptionBox" class="optBox" {%if inv.er_repeat!="w"%}style="display: none;"{%endif%}>
		                    <div class="form-group">
		                      <label class="col-sm-3 control-label">{{lng_invoices.whichday}}</label>
		                      <div class="col-sm-9">
		                        <div class="am-checkbox inline">
		                          <input id="check1" name="er_week_day_mon" value="1" {%if inv.er_week_day_mon=="1"%}checked=""{%endif%} type="checkbox">
		                          <label for="check1">{{lng_common.mon_short}}</label>
		                        </div>
		                        <div class="am-checkbox inline">
		                          <input id="check2" name="er_week_day_tue" value="1" {%if inv.er_week_day_tue=="1"%}checked=""{%endif%} type="checkbox">
		                          <label for="check2">{{lng_common.tue_short}}</label>
		                        </div>
		                        <div class="am-checkbox inline">
		                          <input id="check3" name="er_week_day_wed" value="1" {%if inv.er_week_day_wed=="1"%}checked=""{%endif%}  type="checkbox">
		                          <label for="check3">{{lng_common.wed_short}}</label>
		                        </div>

		                        <div class="am-checkbox inline">
		                          <input id="check4" name="er_week_day_thu" value="1" {%if inv.er_week_day_thu=="1"%}checked=""{%endif%}  type="checkbox">
		                          <label for="check4">{{lng_common.thu_short}}</label>
		                        </div>

		                        <div class="am-checkbox inline">
		                          <input id="check5" name="er_week_day_fri" value="1" {%if inv.er_week_day_fri=="1"%}checked=""{%endif%}  type="checkbox">
		                          <label for="check5">{{lng_common.fri_short}}</label>
		                        </div>

		                        <div class="am-checkbox inline">
		                          <input id="check6" name="er_week_day_sat" value="1" {%if inv.er_week_day_sat=="1"%}checked=""{%endif%}  type="checkbox">
		                          <label for="check6">{{lng_common.sat_short}}</label>
		                        </div>
		                        <div class="am-checkbox inline">
		                          <input id="check7" name="er_week_day_sun" value="1" {%if inv.er_week_day_sun=="1"%}checked=""{%endif%}  type="checkbox">
		                          <label for="check7">{{lng_common.sun_short}}</label>
		                        </div>
		                      </div>
		                    </div>
						</div>
						
						<div id="monthOptionBox" class="optBox" {%if inv.er_repeat!="m"%}style="display: none;"{%endif%}>
							<div class="form-group ">
								<label class="col-sm-3 control-label">{{lng_invoices.whichday}}</label>
								<div class="col-sm-2">
								<select class="form-control" name="er_month_day" >
									{%for j in 1..30%}
										<option {%if j==inv.er_month_day%}selected{%endif%} style="padding: 3px 3px 3px 7px" value="{{j}}">{{j}}</option>
									{%endfor%}
								</select>
								</div>
							</div>
						</div>
						
						<div id="yearOptionBox" class="optBox" {%if inv.er_repeat!="y"%}style="display: none;"{%endif%}>
							<div class="form-group ">
								<label class="col-sm-3 control-label">{{lng_invoices.whichday}}</label>
								<div class="col-sm-3">
								<select class="form-control" name="er_year_month">
									{%for m in months%}
									<option {%if inv.er_year_month==m%}selected{%endif%} value="{{m.mon_num}}">{{m.mon_full_name}}</option>
									{%endfor%}
								</select>
								</div>
								<div class="col-sm-2">
								<select class="form-control" name="er_year_day" >
									<option {%if inv.er_year_day==1%}selected{%endif%} value="{{m.mon_num}}" value="first">{{lng_invoices.first}}</option>
									<option {%if inv.er_year_day==15%}selected{%endif%} value="15">15</option>
									<option {%if inv.er_year_day==30%}selected{%endif%} value="30">{{lng_invoices.last}}</option>
								</select>
								</div>
							</div>
						</div>
							
						</div>			        	
	
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" >{{lng_invoices.endofrec }}</label>
							<div class="col-sm-3" >
								<select class="form-control" name="er_end" id="ir">
									<option {%if inv.er_end==1%}selected{%endif%} value="1">{{lng_invoices.ne }}</option>
									<option {%if inv.er_end==2%}selected{%endif%} value="2">{{lng_invoices.sad }}</option>
									<option {%if inv.er_end==3%}selected{%endif%} value="3">{{lng_invoices.sac }}</option>
	
								</select>	
							</div>
								
							<div class="col-sm-3" {%if inv.er_end!=2%}style="display: none;"{%endif%} id="rsd">
								<div class="input-group">
									<input type="text" data-date-format="{{dpformat}}" readonly value="{{inv.er_end_date|date(acs.user_dformat)}}" name="er_end_date" class="form-control date-picker">
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>							
							</div>
	
							<div class="col-sm-3 col-md-2" {%if inv.er_end!=3%}style="display: none;"{%endif%} id="rsc">
								
									<input type="text" name="er_end_count" value="{{inv.er_end_count}}" class="form-control">
							</div>
	
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{lng_invoices.send_automat}}</label>
						<div class="col-sm-2">
							<input type="checkbox" value="1" {%if inv.er_send_auto%}checked{%endif%}  class="asend"  name="er_send_auto">
						</div>
					</div>
					
					<div class="form-group" id="asend-email-box" {%if inv.er_send_auto==0%}style="display: none;"{%endif%}>
						<label class="col-sm-3 control-label">Email</label>

						<div class="col-sm-4" >
							<input type="email" name="er_send_email" data-errorbox="#erraemail" data-msg-required="{{lng_common.fr}}" required="" id="er_send_email" value="{{inv.er_send_email}}" class="form-control">
							<div id="erraemail" style="color:red;"></div>
						</div>

					</div>	
		        	
		      </div>
		
		    </div>
		</div> 		
		
		</form>    
		
		<button type="button" id="doSave" class="btn btn-primary btn-lg pull-right" style="margin-right: 5px;">{{lng_common.btn_save}}</button>
	
		<a href="#" class="btn btn-default btn-lg pull-left g-l" data-go="/invoices/recurring">{{lng_common.btn_cancel}}</a>
		<div style="clear:both;margin-bottom: 40px;"></div>	
		
	   
	  </div>	  
  </div>	         
</div>

     <div id="share-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_invoices.new_customer}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12">

				  <form id="fnewcustomerpopup" class="form-horizontal" role="form">
						
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">{{lng_clients.tbl_name}}<span style="color: red;">*</span></label>
	                  <div class="col-sm-9">
	                    <input class="form-control" name="client_name" data-errorbox="#errname" data-msg-required="{{lng_clients.name_req}}"  required="" placeholder="{{lng_projects.ph_name}}" type="text">
	                    <div id="errname" style="color:red;"></div>
	                  </div>
	                </div>
	
					
						<input type="hidden" value="1" name="status">
						<input type="hidden"  name="discount" id="discount" value="0"> 
						<input type="hidden"  name="notes" value="">

					<div class="form-group" >
					<label class="col-sm-3 control-label no-padding-right ">Email</label>
					<div class="col-sm-6">
						<input type="email"  name="client_email" data-errorbox="#errname-email" data-msg-email="{{lng_common.nve}}" class="form-control"> 									<div id="errname-email" style="color:red;"></div>
					</div>
					</div>
						

					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.country}}</label>
					 <div class="col-sm-5">
						<select id="country" class="form-control" name="client_country" >
							{%for cl in countryList%}
							<option {%if cl.country_iso==acs.user_country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
							{%endfor%}
						</select>
					</div>
					</div>
					
					
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.state}}</label>
					<div class="col-sm-5">
					 {%if  statesList%}
						<select title="State"  id="region"  name="client_state" class="form-control">
							{%for sl in statesList%}
							<option {%if sl.state_code == acs.co_state%}selected{%endif%} value="{{sl.state_code}}">{{sl.state_name}}</option>
							{%endfor%}
						</select>
						<input type="text" style="display:none;" id="inputstate" title="State" class="form-control"  name=""  />
					{%else%}
						<input type="text" id="inputstate" title="State" class="form-control"  name="client_state"  value="{{acs.co_state}}">
						<select title="State"  style="display:none;"  id="region"  name="client_state" class="form-control">
						</select>
					{%endif%}	
					</div>
					</div>
				
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{lng_common.address}} </label>
					    <div class="col-sm-9">
						    <input type="text" name="client_street"  class="form-control" >
						</div>
					</div>

					
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.city}} </label>
					<div class="col-sm-5">
	
						<input type="text" name="client_city"   class="form-control" >
					</div>
					</div>
						
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.zip}} </label>
					<div class="col-sm-3">
						<input type="text"  class="form-control" name="client_zip"  > 						
					</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right ">{{lng_common.phone}}</label>
					<div class="col-sm-4">
	
						<input type="text"  name="client_phone" class="form-control " > 
					</div>
					</div>
		
		</form>


		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" id="send-invite"   class="btn btn-primary doAddClient btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  

     <div id="addr-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_invoices.chaddr}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="bill-adr-box">



		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button"   class="btn btn-primary doChAddr btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  
	 {%for r in rates%}
	 <input type="hidden" id="rate{{r.curr}}" value="{{r.val}}">
	 {%endfor%}

{%endblock%}


{%block js%}

<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>	
<script src="/assets/lib/livequery/jquery.livequery.js"></script>
<script src="/assets/lib/typeahead/bootstrap3-typeahead.min.js?v={{hashver}}"></script>
<script src="/assets/lib/mask/jquery.mask.js"></script>	
<script src="/assets/lib/switch/js/bootstrap-switch.min.js"></script>
{# <script src="/assets/lib/jquery.magnific-popup/jquery.magnific-popup.min.js" type="text/javascript"></script>#}
{# <script src="/assets/js/app-page-gallery.js" type="text/javascript"></script>#}
<script src="/assets/lib/plupload-2.3.1/js/plupload.full.min.js"></script>	
    <script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/lib/venobox/venobox.min.js"></script>	
<script src="/js/dev/invoicesrec_edit.js?v={{hashver}}"></script>
 
{%endblock%}
