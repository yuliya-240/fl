{% extends skin~"/common/root.tpl" %}
{%block headcss%}
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<style>
	.input-group-btn .btn {
	    height: 42px;
	}	    

    .modal-body {
        max-height: calc(100vh - 310px);
        overflow-y: auto;
    }

</style>    
{%endblock%}

{%block content%}
<input type="hidden" id="_pdt" value="{{lng_invoices.del_bundle_title}}">
<input type="hidden" id="_pdm" value="{{lng_invoices.del_bundle_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="_no" value="{{lng_common.btn_no}}">

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_invoices.rec_main_title}} 
		 <a href="#" class="btn btn-success btn-head pull-right g-l" data-go="/invoices/add" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_invoices.btn_add}}"> {{lng_invoices.newinv}}</a>

		
	</h3>
</div>	
<input type="hidden" id="moment-format" value="{{momentformat}}">

<div class="main-content">
<div class="row">
	<div class="col-md-8 ">
		
		<form id="fsearch"  class="form-horizontal group-border-dashed">
			<div class="form-group">
			<div class="col-xs-12">
					<div class="input-group">
						<span class="input-group-addon " style="background-color:#ccc; color:#fff; " ><span class="s7-search" style="font-size: 24px;"></span></span>
                      <input class="form-control" id="s" placeholder="" {%if searchstr%}value="{{searchstr}}"{%endif%} type="text" style="border-width:1px;" >
             
                       
                    </div>
			</div>		
			</div>		
		</form>
	</div>
</div>	
<div class="row">
	<div class="col-xs-12 ">
		
		
		<form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<div class="panel panel-default panel-borders">

	        <div class="panel-body">
			
				<div id="d-content">
		
				</div>
	
	        </div>
		</div>
		</form>
		
	</div>
	
	<div class="col-sm-2" style="padding-top: 10px;">

	</div>	
</div>

</div>	

     <div id="send-err-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header" style="background-color: #ef6262;">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_invoices.err}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="err-list-box">

		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" data-dismiss="modal"  class="btn btn-danger btn-md ">OK</button>
           

          </div>
         
         
         
        </div>
      </div>
     </div> 

{%endblock%}

{%block footer%}
<div id="megafooter">
	
</div>	
{%endblock%}

{%block js%}
	<script src="/assets/lib/date-time/moment.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
   <script src="/js/dev/invoicesrec.js?v={{hashver}}"></script>
{%endblock%}
