{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs"></th>
			{#<th>#</th>#}
			<th>Start</th>
			<th>Repeat</th>
			<th>Recipient</th>
			<th style="text-align: right;">Total</th>
			<th width="1%"></th>
		</tr>
	</thead>
	<tbody>
		{%for r in records%}
		<tr>
			<td class="text-center hidden-xs" style="vertical-align:middle;">
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.inv_id}}" class="check" name="row[]" value="{{r.inv_id}}">
	              <label for="chk{{r.inv_id}}"></label>
	            </div>
			</td>
			<td class="tbl-pointer g-l" data-id="{{r.inv_id}}" data-go="/invoices/view/{{r.inv_id}}">{{r.er_date_start|date(acs.user_dformat)}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.inv_id}}" data-go="/invoices/recurring/view/{{r.inv_id}}">
				{%if r.er_repeat=="w"%}{{lng_common.weekly}}:
					{%if r.er_week_day_mon%}{{lng_common.mon_short}}&nbsp;{%endif%}
					{%if r.er_week_day_tue%}{{lng_common.tue_short}}&nbsp;{%endif%}
					{%if r.er_week_day_wed%}{{lng_common.wed_short}}&nbsp;{%endif%}
					{%if r.er_week_day_thu%}{{lng_common.thu_short}}&nbsp;{%endif%}
					{%if r.er_week_day_fri%}{{lng_common.fri_short}}&nbsp;{%endif%}
					{%if r.er_week_day_sat%}{{lng_common.sat_short}}&nbsp;{%endif%}
					{%if r.er_week_day_sun%}{{lng_common.sun_short}}{%endif%}
				
				{%endif%}
				{%if r.er_repeat=="m"%}{{lng_common.monthly}}: {{lng_common.every}} {{r.er_month_day}} {{lng_common.day}} {%endif%}
				{%if r.er_repeat=="y"%}{{lng_common.yearly}}: {{lng_common.every}}
				 
				 {{r.mon_name}} 
				 
				 {{r.er_year_day}} 
				{%endif%}
				
			</td>
			<td class="tbl-pointer g-l" data-id="{{r.inv_id}}" data-go="/invoices/recurring/view/{{r.inv_id}}" style="font-weight: 600">{{r.inv_name}}</td>
			
			<td class="tbl-pointer g-l" data-id="{{r.inv_id}}" data-go="/invoices/recurring/view/{{r.inv_id}}" style="text-align: right;"><span style="font-weight: 600">{{r.inv_total|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{r.inv_currency}}</small>
				{%if r.inv_currency!=acs.user_currency%}
				<br><small>{{r.inv_total_base|number_format(2,acs.user_dsep,acs.user_tsep)}}</small> <small>{{acs.user_currency}}</small>
				{%endif%}
			</td>
			<td class="tbl-pointer" data-id="{{r.inv_id}}">
				<a href="#" class="g-l" data-go="/invoices/recurring/view/{{r.inv_id}}" data-id="{{r.inv_id}}"><i class="material-icons">visibility</i></a>
			</td>
		</tr>
		{%endfor%}
	</tbody>
</table>	

			
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
