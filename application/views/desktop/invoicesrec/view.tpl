{% extends skin~"/common/root.tpl" %}
{%block headcss%}
<link href="/assets/lib/venobox/venobox.css" rel="stylesheet" />
<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
<style>
	
.photo{
	border:1px solid #ccc;
}
	
#plist{
	list-style-type: none;
}	
	
.ic-flist{
	font-size: 22px;
}	
	
.form-horizontal .form-group {
	padding: 4px 0 !important; 
}

</style>	

{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 1px;">{{lng_invoices.rec_title}} #{{inv.inv_num}} 
	
		<div class="btn-group  btn-hspace pull-right" >
              <button type="button" data-toggle="dropdown" class="btn btn-default btn-head dropdown-toggle dropdowndoc" aria-expanded="false"  style="border-color: #7a7a7a;">&nbsp;{{lng_invoices.mact}}&nbsp;&nbsp;<span class="caret"></span></button>
              <ul role="menu" id="act-box" class="dropdown-menu">
          
                <li><a href="#" class="copy">&nbsp;{{lng_invoices.cp}}</a></li>
                <li class="divider"></li>
                <li><a href="#" style="color:red;" class="del">&nbsp;{{lng_invoices.del}}</a></li>
              </ul>
        </div>

		
		<a href="#"  data-go="/invoices/recurring/edit/{{inv.inv_id}}" class="btn btn-head btn-primary sendemail btn-space pull-right g-l">{{lng_invoices.ed}}</a>

      <a href="#" data-go="/invoices/recurring" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.back_list}}" class="btn btn-head btn-space btn-dark pull-right g-l"><i class="material-icons btn-head-s">arrow_back</i></a>
		
	</h3>
	
</div>	

<input type="hidden" id="inv_id" value="{{inv.inv_id}}">
<input type="hidden" id="_idt" value="{{lng_invoices.del_title}}">
<input type="hidden" id="_idm" value="{{lng_invoices.del_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="_cb" value="{{lng_common.btn_cancel}}">

<input type="hidden" id="_copy_tit" value="{{lng_invoices.copy_title}}">
<input type="hidden" id="_copy_msg" value="{{lng_invoices.copy_msg}}">	

	
<input type="hidden" id="client" value="{{postfix}}">

<div class="main-content">
  <div class="row">
	<div class="col-xs-12 col-md-9">
		<div class="panel panel-default panel-borders">
	        <div class="panel-body">
              <div class="invoice" style="background: none !important; padding: 20px 20px;">
                <div class="row invoice-header">
                  <div class="col-xs-7">
                    <div class="invoice-logo">
	                	{%if acs.set_logo!="nologo.png"%}
	                    <img class="img-responsive" src="/userfiles/{{acs.user_id}}/pic/{{acs.set_logo}}">
	                    {%endif%}
	                </div>
                  </div>
                  <div class="col-xs-5 invoice-order">
	                  <span class="invoice-id">{{invlang.invoice}} # .....</span>
	                  <span class="incoice-date">{{inv.inv_date|date(acs.user_dformat)}}</span>
	              </div>
                </div>
                <div class="row invoice-data">
                  <div class="col-sm-5 invoice-person" id="inv-my-addr">
	                  
	                  <span class="name">{{acs.set_billing_name}} </span>
	                  <span>{{acs.set_billing_street}}</span>
	                  <span>{{acs.set_billing_city}}{%if acs.set_billing_state%}, {{acs.set_billing_state}}{%endif%} {{acs.set_billing_zip}}</span>
	                  <span>{{mycountry}}</span>
	                  <a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chMyAddr" >Edit Address</a>
	                
	              </div>
                  <div class="col-sm-2 invoice-payment-direction"><i class="icon mdi mdi-chevron-right"></i></div>
                  <div class="col-sm-5 invoice-person" id="recipient-addr">
	                  
	                  <span class="name"> {{inv.inv_name}} </span>
	                  <span>{{inv.inv_street}}</span>
	                  <span>{{inv.inv_city}}{%if inv.inv_state%}, {{inv.inv_state}}{%endif%} {{inv.inv_zip}}</span>
	                  <span>{{country}}</span>
	                  <a href="#"  class="btn btn-sm " style="padding-right: 0;" id="chAddr" >Edit Address</a>
	                
	              </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <table class="invoice-details">
                     
                      <tr>
                        <th style="width:60%">{{invlang.desc}}</th>
                        <th style="width:15%" class="hours">{{invlang.rate}}</th>
                        <th style="width:10%" class="hours">{{invlang.qty}}</th>
                        <th style="width:15%" class="amount">{{invlang.am}}</th>
                      </tr>
                      
                      {%for l in lines%}
                      <tr>
	                        <td class="description">{{l.invline_srv}}
		                        <br>{{l.invline_desc}}
	                        </td>
	                        <td class="hours">{{l.invline_rate}}</td>
	                        <td class="hours">{{l.invline_qty}}</td>
	                        <td class="amount">{{l.invline_total}}</td>
                      </tr>
                      {%endfor%}
                      <tr>
                        <td></td>
                        <td colspan="2" class="summary">{{invlang.subtotal}} <small style="font-size: smaller">{{inv.inv_currency}}</small></td>
                        <td class="amount">{{inv.inv_subtotal}}</td>
                      </tr>
					   {%if inv.inv_taxes_cash>0%}
                       <tr>
                        <td></td>
                     
                        <td colspan="2" class="summary">{{invlang.taxes}}  <small style="font-size: smaller">{{inv.inv_currency}}</small></td>
                        <td class="amount">{{inv.inv_taxes_cash}}</td>
                      </tr>
					  {%endif%}
                      {%if inv.inv_discount_percentage>0%}
                      
                      <tr>
                        <td></td>
                    
                        <td colspan="2" class="summary">{{invlang.discount}} ({{inv.inv_discount_percentage}}%)</td>
                        <td class="amount">{{inv.inv_discount_cash}}</td>
                      </tr>
                      {%endif%}
                      
                      <tr>
                        <td></td>
          
                        <td colspan="2" class="summary total" >
	                        {{invlang.total}} <small style="font-size: smaller">{{inv.inv_currency}}</small>
	                    </td>

                        <td class="amount total-value" style="color:#000;">{{inv.inv_total}}</td>
                      </tr>
                        
                      <tr>
                        <td></td>
          
                        <td colspan="2" class="summary">{{invlang.paid}} <small style="font-size: smaller">{{inv.inv_currency}}</small></td>
                        <td class="amount text-success">{{inv.inv_paid_amount}}</td>
                      </tr>
                      <tr>
                        <td></td>
          
                        <td colspan="2" class="summary total">{{invlang.due}} <small style="font-size: smaller">{{inv.inv_currency}}</small></td>
                        <td class="amount text-danger">{{inv.inv_total_due}}</td>
                      </tr>
                      
                    </table>
                  </div>
                </div>
				
				{%if inv.inv_terms %}
                <div class="row" style="margin-top: 20px; ">
                  <div class="col-md-12 invoice-message" style="margin-bottom: 20px;"><span class="title">{{invlang.terms}}</span>
                    <p>{{inv.inv_terms}}</p>
                  </div>
                </div>
				{%endif%}
				
				{%if inv.inv_notes%}
                <div class="row" >
                  <div class="col-md-12 invoice-message" style="margin-bottom: 20px;"><span class="title">{{invlang.notes}}</span>
                    <p>{{inv.inv_notes}}</p>
                  </div>
                </div>
				{%endif%}
{#
                <div class="row invoice-company-info">
                  {%if acs.set_logo!="nologo.png"%}
                  <div class="col-sm-2 logo">
	                	
	                    <img class="img-responsive" src="/userfiles/{{acs.user_id}}/pic/{{acs.set_logo}}">
	                    
	              </div>
	              {%endif%}
                  <div class="col-sm-4 summary"><span class="title">Amaretti Company</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                  </div>
                  <div class="col-sm-3 phone">
                    <ul class="list-unstyled">
                      <li>+1(535)-8999278</li>
                      <li>+1(656)-3558302</li>
                    </ul>
                  </div>
                  <div class="col-sm-3 email">
                    <ul class="list-unstyled">
                      <li>amaretti@company.co</li>
                      <li>amaretti@support.co</li>
                    </ul>
                  </div>
                </div>
         #}
              </div>
	        </div>
		</div>   
		
		{%if flist%}
		<div class="panel panel-default panel-borders ">
<div class="panel-heading">Attached Files</div>
	        <div class="panel-body">	
	        
		        
			<ul id="plist">
				{%for f in flist%}
					<li style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px #ccc dotted;" id="attitem{{f.invatt_id}}">
						<div class="pull-left" style="margin-right: 5px; padding-top: 5px;">{%if f.invatt_type=="image"%}<span class="s7-photo ic-flist"></span>{%else%} <span class="s7-file ic-flist"></span>{%endif%}</div> 
						
						<div class="pull-left" style="padding-top: 7px;">	<a href="{{ihost}}userfiles/{{acs.user_id}}/pic/{{f.invatt_fname}}" {%if f.invatt_type=="image"%}class="venobox"{%else%}target="_blank"{%endif%} >{{f.invatt_fname}}</a></div>
						
						
						<div style="clear: both;"></div>

					</li>

				
				{%endfor%}
			</ul>				
	        
	        </div>
		</div>  
		{%endif%}      	
		   		
	</div>	  
	<div class="col-xs-12 col-md-3">
		<legend style="padding-bottom: 10px;">{{lng_invoices.rec_opt}}</legend>
		
 		<div class="panel panel-default panel-borders">

	        <div class="panel-body">
		    
			       
			  <table class="table">     
				<tr>
					<td colspan="2">
				
						
				{%if inv.er_repeat=="w"%}
					<h4>{{lng_common.weekly}}</h4>
					{%if inv.er_week_day_mon%}{{lng_common.mon_short}},{%endif%}
					{%if inv.er_week_day_tue%}{{lng_common.tue_short}},{%endif%}
					{%if inv.er_week_day_wed%}{{lng_common.wed_short}},{%endif%}
					{%if inv.er_week_day_thu%}{{lng_common.thu_short}},{%endif%}
					{%if inv.er_week_day_fri%}{{lng_common.fri_short}},{%endif%}
					{%if inv.er_week_day_sat%}{{lng_common.sat_short}},{%endif%}
					{%if inv.er_week_day_sun%}{{lng_common.sun_short}}{%endif%}
				
				{%endif%}
				{%if inv.er_repeat=="m"%}<h4>{{lng_common.monthly}}</h4> {{lng_common.every}} {{inv.er_month_day}} {{lng_common.day}} {%endif%}
				{%if inv.er_repeat=="y"%}<h4>{{lng_common.yearly}}</h4> {{lng_common.every}}
				 
				 {{inv.mon_name}} 
				 
				 {{inv.er_year_day}} 
				{%endif%}
					</td>
				</tr>     
				<tr>
					<td>{{lng_invoices.begin}}</td>
					<td>{{inv.er_date_start|date(acs.user_dformat)}}</td>
					
				</tr>  				        

				<tr>
					<td>{{lng_invoices.end}}</td>
					<td>
						{%if inv.er_end==1%}{{lng_invoices.never}}{%endif%}
						{%if inv.er_end==2%}{{lng_invoices.after}} {{inv.er_end_date|date(acs.user_dformat)}}{%endif%}
						{%if inv.er_end==3%}{{lng_invoices.after}} {{inv.er_end_count}} {{lng_invoices.times}}{%endif%}
					</td>
					
				</tr>  				        
				
				<tr>
					<td>{{lng_invoices.send_automat}}</td>
					<td>{%if inv.er_send_auto%}<span class="text-success">Yes</span>{%else%}<span class="text-danger">No</span>{%endif%}</td>
				</tr>	
				
				{%if inv.er_send_auto%}
				<tr>
					<td>Email</td>
					<td>{{inv.er_send_email}}</td>
				</tr>
				
					
				
				{%endif%}

			  </table>      	
			        
		          
	        </div>
 		</div>    
    </div>
	
  </div>
</div>

     <div id="addr-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade">
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_invoices.chaddr}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="bill-adr-box">

		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button"   class="btn btn-primary doChAddr btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  

     <div id="my-addr-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_invoices.chaddr}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="my-bill-adr-box">



		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button"   class="btn btn-primary doChMyAddr btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left" data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  


{%endblock%}


{%block js%}
<script src="/assets/lib/mask/jquery.mask.js"></script>	
<script src="/assets/lib/livequery/jquery.livequery.js"></script>
<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>
<script src="/assets/lib/venobox/venobox.min.js"></script>	
<script src="/js/dev/invoices_view.js?v={{hashver}}"></script>
 
{%endblock%}

