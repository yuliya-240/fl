  <div class="row">

  	<div class="col-md-8 col-md-offset-2">


<form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">

	<div class="panel panel-default panel-borders" style="margin-bottom:5px;">

        <div class="panel-body">

			<div class="form-group">
				
				<label class="col-sm-3 control-label">{{lng_calendar.when}}</label>
				
				<div class="col-md-3 col-sm-4" id="e-date">
					<div class="input-group pull-left">
						<span class="input-group-addon">
							<i class="fa fa-calendar bigger-110"></i>
						</span>
						<input type="text" data-date-format="{{dpformat}}" readonly="" value="{{today}}" name="deadline_date" class="form-control date-picker">
						
					</div>						
				</div>
					
				<div class="col-md-3 col-sm-4">
					<div class="input-group pull-left">
						<span class="input-group-addon">
							<i class="fa fa-clock-o bigger-110"></i>
						</span>
						<input type="text" value="{{dttime}}"  name="deadline_time" class="form-control time-picker">
					</div>						
				</div>	
					
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">{{lng_calendar.type}}</label>
				<div class="col-sm-4">
					<select class="form-control" id="e_type" name="e_type">
						{%for t in types%}
						<option value="{{t.etype_type}}" style="padding-top: 5px;">{{t.etype_name}}</option>
						{%endfor%}
					</select>	  
				</div>	  
			</div>				

            <div class="form-group">
              <label class="col-sm-3 control-label">{{lng_calendar.title}}<span style="color: red;">*</span></label>
              <div class="col-sm-9">
                <input class="form-control" name="title" data-errorbox="#errname" data-msg-required="{{lng_common.fr}}"  required=""  type="text">
                <div id="errname" style="color:red;"></div>
              </div>
            </div>
           
			<div class="form-group">
				<label class="col-sm-3 control-label">{{lng_calendar.loc}}</label>
				<div class="col-sm-8">
				   <input type="text" name="location" class="form-control">
				</div>	  
			</div>				

		   <div class="form-group">
			   <label class="col-sm-3 control-label">{{lng_calendar.asign}}</label>
			   <div class="col-sm-5">
			    	<select id="client" name="client" class="select2">
				    	
			    		<option value="0">{{lng_calendar.assignto}}</option>
			    		{%if clients%}
			    		<optgroup label="{{lng_menu.clients}}">
			    		{%for c in clients%}
			    			<option {%if selClient == c.client_id%}selected{%endif%} value="{{c.client_id}}@cli">{{c.client_name}}</option>
			    		{%endfor%}
			    		</optgroup>
			    		{%endif%}
			    		{%if collabs%}
			    		<optgroup label="{{lng_menu.colleague}}">
			    		{%for c in collabs%}
			    			<option {%if selCollab == c.cb_id%}selected{%endif%} value="{{c.cb_id}}@usr">{{c.cb_billing_name}}</option>
			    		{%endfor%}
			    		</optgroup>
			    		{%endif%}
			    		
			    	</select>
				  
			   </div>	   
		   </div>	   
		   
		   <div class="form-group">
			   <label class="col-sm-3 control-label">{{lng_calendar.desc}}</label>
			   <div class="col-sm-9">
				   <textarea name="e_desc" class="form-control" rows="3"></textarea>
			   </div>	   
		   </div>
		   
		   
		   	   
	        
        </div>
	</div> 

	<div class="panel panel-default panel-borders">

			<div class="panel-heading">{{lng_invoices.rec}} <span class="pull-right"><input type="checkbox" value="1"  class="pub"  name="recurrance"></span></div>
	        <div class="panel-body">
		      <div id="rec-off" class="text-center">
		        	<p>{{lng_invoices.rec_off}}</p>
		      </div>

		      <div id="rec-on" style="display: none;">
		        	

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" >{{lng_calendar.start_date}}</label>
							
								<div class="col-sm-4" >
									<div class="input-group">
										<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="er_date_start" class="form-control date-picker">
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>							
								</div>
						</div>


						<div class="form-group" >
						 	<label class="col-sm-3 control-label no-padding-right"> {{lng_invoices.freq}}</label>
						 	<div class="col-sm-4">
						 			<select id="recOptions" name="er_repeat"  class="form-control">
							
										<option value="d">{{lng_calendar.daily}}</option>
										<option value="w">{{lng_calendar.weekly}}</option>
										<option value="m">{{lng_calendar.monthly}}</option>
										<option value="y">{{lng_calendar.yearly}}</option>
						 			
						 			</select>
						 		</div>
						</div>	

						<div id="recurrenceOptions" >
								
								<div id="dayOptionBox" class="optBox">
									
									<div class="form-group">
										<label class="col-sm-3 control-label">&nbsp;</label>
										<div class="col-sm-9">
											<div class="am-radio">	
					                          <input id="check1" name="daily_option" value="1" checked="" type="radio">
					                          <label for="check1">Every Weekday</label>
											</div>	
										</div>	
									</div>
									<div class="form-group form-inline">
										<label class="col-sm-3 control-label">&nbsp;</label>
										<div class="col-sm-9">
										<div class="am-radio inline">
				                          <input type="radio" name="daily_option" id="rad4" value="0" >
				                          <label for="rad4">Every</label>
				                        </div>
				                        <select class="form-control inline" name="daily_days_count">
											{%for i in 1..30%}
											<option value="{{i}}">{{i}}</option>
											{%endfor%}
				                        </select>   
				                        <label class="inline"> Day(s)</label> 
										</div>
									</div>

								</div>
								
								<div id="weekOptionBox" class="optBox" style="display: none;">

									<div class="form-group">
										  <label class="col-sm-3 control-label">Every</label>
										<div class="col-sm-2 ">
											
											<select class="form-control" name="weekly_count" >
											{%for j in 1..10%}
												<option value="{{j}}">{{j}}</option>
											{%endfor%}
										</select>

										</div>
										<label class="col-sm-2 control-label" style="text-align:left;">Week(s) </label>
									</div>	 
				                    <div class="form-group">
				                      <label class="col-sm-3 control-label">on</label>
				                      <div class="col-sm-9">
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="checkm" name="weekday[]" value="1" checked="" type="checkbox">
				                          <label for="checkm">{{lng_common.mon_short}}</label>
				                        </div>
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check2" name="weekday[]" value="2" type="checkbox">
				                          <label for="check2">{{lng_common.tue_short}}</label>
				                        </div>
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check3" name="weekday[]" value="3" type="checkbox">
				                          <label for="check3">{{lng_common.wed_short}}</label>
				                        </div>

				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check4" name="weekday[]" value="4" type="checkbox">
				                          <label for="check4">{{lng_common.thu_short}}</label>
				                        </div>

				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check5" name="weekday[]" value="5" type="checkbox">
				                          <label for="check5">{{lng_common.fri_short}}</label>
				                        </div>

				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check6" name="weekday[]" value="6" type="checkbox">
				                          <label for="check6">{{lng_common.sat_short}}</label>
				                        </div>
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check7" name="weekday[]" value="7" type="checkbox">
				                          <label for="check7">{{lng_common.sun_short}}</label>
				                        </div>


				                      </div>
				                    </div>
									
								</div>
								
								<div id="monthOptionBox" class="optBox" style="display:none;">
									
									<div class="form-group form-inline">
										<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
										<div class="col-sm-9">
										<div class="am-radio inline">
											
													<input  type="radio" id="mo1" value="1" checked="checked" name="monthly_option">
													<label for="mo1">&nbsp;On Day</label>
												
										</div>
										
										<select class="form-control" name="month_day" >
											{%for j in 1..30%}
												<option value="{{j}}">{{j}}</option>
											{%endfor%}
										</select>
										&nbsp;of every 
										<select class="form-control" name="month_count1" >
											{%for m in 1..12%}
												<option value="{{m}}">{{m}}</option>
											{%endfor%}
										</select>&nbsp;month(s)
										</div>
										
									
									</div>
								
									<div class="form-group form-inline">
										<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
										<div class="col-sm-9">
											<div class="am-radio inline">
										
													<input id="mo2" type="radio" class="ace" value="2" name="monthly_option">
													<label for="mo2">&nbsp;On Day</label>
										
											</div>
											
											<select name="month_day_number" class="form-control">
												<option value="first">First</option>
												<option value="second">Second</option>
												<option value="third">Third</option>
												<option value="fourth">Fourth</option>
												<option value="last">Last</option>
											</select>
											
											<select name="month_week_day" class="form-control">
												<option value="mon">Monday</option>
												<option value="tue">Tuesday</option>
												<option value="wed">Wednesday</option>
												<option value="thu">Thursday</option>
												<option value="fri">Friday</option>
												<option value="sat">Saturday</option>
												<option value="sun">Sunday</option>
											</select>
											of every
											<select class="form-control" name="month_count2" >
												{%for m in 1..12%}
													<option value="{{m}}">{{m}}</option>
												{%endfor%}
											</select>&nbsp;month(s)
										</div>
										
										
									
									</div>						
								
													
								</div>
								
								<div id="yearOptionBox" class="optBox" style="display:none;">

									<div class="form-group form-inline">
										<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
										<div class="col-sm-9">
											<div class="am-radio inline">
													
												<input  type="radio" value="1" checked="checked" name="yarly_option" id="yo1">
												<label for="yo1">&nbsp;On Day</label>
													
											</div>
											<select class="form-control" name="year_month1">
												<option value="jan">January</option>
												<option value="feb">February</option>
												<option value="mar">March</option>
												<option value="apr">April</option>
												<option value="may">May</option>
												<option value="jun">Jun</option>
												<option value="jul">July</option>
												<option value="aug">August</option>
												<option value="sep">September</option>
												<option value="oct">October</option>
												<option value="nov">November</option>
												<option value="dec">December</option>
											</select>&nbsp;
											<select class="form-control" name="year_day" >
												{%for m in 1..31%}
													<option value="{{m}}">{{m}}</option>
												{%endfor%}
											</select>&nbsp;month(s)
										</div>
										
									</div>
									
									<div class="form-group form-inline">
											<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
											<div class="col-sm-9">
											<div class="am-radio inline">
												
													<input type="radio" id="yo2" value="2" name="yarly_option">
													<label for="yo2">&nbsp;On Day</label>
												
												</div>
											
											<select name="year_day_number" class="form-control">
												<option value="first">First</option>
												<option value="second">Second</option>
												<option value="third">Third</option>
												<option value="fourth">Fourth</option>
												<option value="last">Last</option>
											</select>&nbsp;
											<select name="year_week_day" class="form-control">
												<option value="mon">Monday</option>
												<option value="tue">Tuesday</option>
												<option value="wed">Wednesday</option>
												<option value="thu">Thursday</option>
												<option value="fri">Friday</option>
												<option value="sat">Saturday</option>
												<option value="sun">Sunday</option>
											</select>&nbsp;of&nbsp;
											<select class="form-control" name="year_month2">
												<option value="jan">January</option>
												<option value="feb">February</option>
												<option value="mar">March</option>
												<option value="apr">April</option>
												<option value="may">May</option>
												<option value="jun">Jun</option>
												<option value="jul">July</option>
												<option value="aug">August</option
												<option value="sep">September</option>
												<option value="oct">October</option>
												<option value="nov">November</option>
												<option value="dec">December</option>
											</select>&nbsp;
											</div>
									</div>							

								</div>
							</div>			        	

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" >{{lng_invoices.endofrec }}</label>
								<div class="col-sm-3" >
									<select class="form-control" name="er_end" id="ir">
										<option value="1">{{lng_invoices.ne }}</option>
										<option value="2">{{lng_invoices.sad }}</option>
										<option value="3">{{lng_invoices.sac }}</option>

									</select>	
								</div>
									
								<div class="col-sm-3" style="display: none;" id="rsd">
									<div class="input-group">
										<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="er_end_date" class="form-control date-picker">
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>							
								</div>

								<div class="col-sm-3 col-md-2" style="display: none;" id="rsc">
									<input type="text" name="er_end_count" value="1" class="form-control">
								</div>
						</div>
						
		      </div>

	        </div>
		</div>  	
	
</form>	
	<button type="button" class="btn btn-primary btn-lg pull-right doSave" style="margin-right: 5px;">{{lng_common.btn_save}}</button>
	<a href="#" class="btn btn-default btn-lg pull-left cancelAddEvent"  >{{lng_common.btn_cancel}}</a>
	<div style="clear:both;margin-bottom: 40px;"></div>	
  	</div>  	
  </div>	  
