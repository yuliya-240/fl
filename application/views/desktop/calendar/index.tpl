{% extends skin~"/common/root.tpl" %}
{%block preheadcss%}
   <link rel="stylesheet" type="text/css" href="/assets/lib/fullcalendar-3.6.1/fullcalendar.min.css"/>
   <link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.min.css"/>
 {#  <link rel="stylesheet" type="text/css" href="/assets/lib/fullcalendar-1.6.7/fullcalendar/fullcalendar.css"/>#}
  
{%endblock%}
{%block headcss%}
 	<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
 	<link rel="stylesheet" href="/assets/lib/redactor2/redactor.css?v={{hashver}}" />
 	<link href="/assets/lib/switch/css/bootstrap-switch.min.css" rel="stylesheet" />
<style>
.fc-title {
    white-space: normal !important;
}
.fc-event {
    font-size: 0.9em;
    
}
button.fc-next-button.fc-button.fc-state-default.fc-corner-right {
	border-top-right-radius: 4px!important;
	border-bottom-right-radius: 4px!important;
	border-top-left-radius: 0px;
	border-bottom-left-radius: 0px;

}
button.fc-prev-button.fc-button.fc-state-default.fc-corner-left {
	border-top-left-radius: 4px;
	border-bottom-left-radius: 4px;
	border-top-right-radius: 0px;
	border-bottom-right-radius: 0px;
	}

#new-event-modal, #show-event-modal {
	width: 90%;
	position: absolute;
	top: -100px;
	left: 100%;
	z-index: 9999;	
	background-color: white;
	overflow-y: scroll;
	
}
#new-event-modal-back, #show-event-modal-back {
	z-index: 9997;
	width: 120%;
	height: 100%;
	position: absolute;
	top: -100px;
	left: -100px;
	background-color: rgba(0, 0, 0, 0.3);
	transform: scale(1, 10)

	}
#new-event-content, #show-event-content {
	position: relative;
	width: 90%;
	margin-top: 50px;
	overflow: hidden;
	}
#new-event-modal-close, #show-event-modal-close {
	position:absolute;
	z-index: 9998;
	background-color: transparent;
	color: white;
	font-size: 5em;
	top:-50px;
	left:-50px;
}
#new-event-inner-content, #show-event-inner-content {
	overflow: hidden;	
	
}
#new-event-modal-close span, #show-event-modal-close span {
	font-weight: 800;
	}
.show-event-table, .show-issue-table {
	border-bottom: #e6e6e6 1px solid;
	margin: 20px;	
}
div.row.show-issue-table:nth-last-child(4){
	border-bottom: #FFF 0px solid;
	}

#show-event-edit-event {
	margin-left: 20px;
	margin-right:20px;
	}
.show-event-buttons, #show-event-save-edited-event, .show-issue-buttons, .edit-recuring-buttons {
	margin-top:50px;
	margin-left:20px;
	margin-right:20px;
	margin-bottom: 10px;	
}
.show-event-buttons > .btn {
	margin:5px;
	}
.empty-p {
	color: #EF6262;
	}
@media only screen and (max-width: 768px) {
    /* For mobile phones: */
    #new-event-modal-close, #show-event-modal-close {
	    font-size: 5em;
	    left: 10px;
	    }
}
@media only screen and (max-width: 420px) {
    /* For mobile phones: */
    #new-event-modal-close, #show-event-modal-close {
	    font-size: 3em;
	    left: 10px;
	    }
}
</style>	 

{%endblock%}
{%block content%}
<input type="hidden" id="ev-task-color" value="{{acs.set_task_color}}">
<input type="hidden" id="ev-issue-color" value="{{acs.set_issue_color}}">
<input type="hidden" id="ev-mil-color" value="{{acs.set_milestone_color}}">
<input type="hidden" id="ev-phone-color" value="{{acs.set_phone_color}}">
<input type="hidden" id="ev-meet-color" value="{{acs.set_meeting_color}}">
<input type="hidden" id="_cal-title" value="{{lng_calendar.title1}}">
<input type="hidden" id="_cal-n-e" value="{{lng_calendar.title_new}}">

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;"><span id="cal-title">{{lng_calendar.title1}} </span>
		<a href="#" class="btn btn-success btn-head pull-right newEvent" > {{lng_calendar.btn_add}}</a>
	</h3>
</div>	
<input type="hidden" id="mf" value="{{mf}}">
		<input type="hidden" id="btnYes" value="{{lng_common.btn_yes}}">
		<input type="hidden" id="btnNo" value="{{lng_common.btn_no}}">

<div class="main-content">
	
  <div class="row full-calendar" id="calendar-box">
    <div class="col-xs-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div id="calendar"></div>
        </div>
      </div>
    </div>
  </div>
  
  <div id="new-event-box" style="display: none;">
	  
  </div>	  
  
  
</div>

<div id="new-event-modal-back" class="hidden">
</div>
<div id="new-event-modal-close" class="hidden">
	<span class="s7-close-circle">  </span>
</div>
<div id="new-event-modal" class="hidden">
	<div class="container" id="new-event-content">
	  <div class="row" id="new-event-inner-content">

  	<div class="">


<form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">

	<div class="panel panel-default panel-borders" style="margin-bottom:5px;">

        <div class="panel-body">

			<div class="form-group">
				
				<label class="col-sm-3 control-label">{{lng_calendar.when}}</label>
				
				<div class="col-md-3 col-sm-4" id="e-date">
					<div class="input-group pull-left">
						<span class="input-group-addon">
							<i class="fa fa-calendar bigger-110"></i>
						</span>
						<input type="text" data-date-format="{{dpformat}}" readonly="" value="{{today}}" name="deadline_date" class="form-control date-picker">
						
					</div>						
				</div>
					
				<div class="col-md-3 col-sm-4">
					<div class="input-group pull-left">
						<span class="input-group-addon">
							<i class="fa fa-clock-o bigger-110"></i>
						</span>
						<input type="text" value="{{dttime}}"  name="deadline_time" class="form-control time-picker">
					</div>						
				</div>	
					
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">{{lng_calendar.type}}</label>
				<div class="col-sm-4">
					<select class="form-control" id="e_type" name="e_type">
						{%for t in types%}
						<option value="{{t.etype_type}}" style="padding-top: 5px;">{{t.etype_name}}</option>
						{%endfor%}
					</select>	  
				</div>	  
			</div>				

            <div class="form-group">
              <label class="col-sm-3 control-label">{{lng_calendar.title}}<span style="color: red;">*</span></label>
              <div class="col-sm-9">
                <input class="form-control" name="title" data-errorbox="#errname" data-msg-required="{{lng_common.fr}}"  required=""  type="text">
                <div id="errname" style="color:red;"></div>
              </div>
            </div>
           
			<div class="form-group">
				<label class="col-sm-3 control-label">{{lng_calendar.loc}}</label>
				<div class="col-sm-8">
				   <input type="text" name="location" class="form-control">
				</div>	  
			</div>				

		   <div class="form-group">
			   <label class="col-sm-3 control-label">{{lng_calendar.asign}}</label>
			   <div class="col-sm-5">
			    	<select id="client" name="client" class="select2">
				    	
			    		<option value="0">{{lng_calendar.assignto}}</option>
			    		{%if clients%}
			    		<optgroup label="{{lng_menu.clients}}">
			    		{%for c in clients%}
			    			<option {%if selClient == c.client_id%}selected{%endif%} value="{{c.client_id}}@cli">{{c.client_name}}</option>
			    		{%endfor%}
			    		</optgroup>
			    		{%endif%}
			    		{%if collabs%}
			    		<optgroup label="{{lng_menu.colleague}}">
			    		{%for c in collabs%}
			    			<option {%if selCollab == c.cb_id%}selected{%endif%} value="{{c.cb_id}}@usr">{{c.cb_billing_name}}</option>
			    		{%endfor%}
			    		</optgroup>
			    		{%endif%}
			    		
			    	</select>
				  
			   </div>	   
		   </div>	   
		   
		   <div class="form-group">
			   <label class="col-sm-3 control-label">{{lng_calendar.desc}}</label>
			   <div class="col-sm-9">
				   <textarea name="e_desc" class="form-control" rows="3"></textarea>
			   </div>	   
		   </div>
		   
		   
		   	   
	        
        </div>
	</div> 

	<div class="panel panel-default panel-borders">

			<div class="panel-heading">{{lng_invoices.rec}} <span class="pull-right"><input type="checkbox" value="1"  class="pub"  name="recurrance"></span></div>
	        <div class="panel-body">
		      <div id="rec-off" class="text-center">
		        	<p>{{lng_invoices.rec_off}}</p>
		      </div>

		      <div id="rec-on" style="display: none;">
		        	

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" >{{lng_calendar.start_date}}</label>
							
								<div class="col-sm-4" >
									<div class="input-group">
										<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="er_date_start" class="form-control date-picker">
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>							
								</div>
						</div>


						<div class="form-group" >
						 	<label class="col-sm-3 control-label no-padding-right"> {{lng_invoices.freq}}</label>
						 	<div class="col-sm-4">
						 			<select id="recOptions" name="er_repeat"  class="form-control">
							
										<option value="d">{{lng_calendar.daily}}</option>
										<option value="w">{{lng_calendar.weekly}}</option>
										<option value="m">{{lng_calendar.monthly}}</option>
										<option value="y">{{lng_calendar.yearly}}</option>
						 			
						 			</select>
						 		</div>
						</div>	

						<div id="recurrenceOptions" >
								
								<div id="dayOptionBox" class="optBox">
									
									<div class="form-group">
										<label class="col-sm-3 control-label">&nbsp;</label>
										<div class="col-sm-9">
											<div class="am-radio">	
					                          <input id="check1" name="daily_option" value="1" checked="" type="radio">
					                          <label for="check1">{{lng_calendar.rec_every_weekday}}</label>
											</div>	
										</div>	
									</div>
									<div class="form-group form-inline">
										<label class="col-sm-3 control-label">&nbsp;</label>
										<div class="col-sm-9">
										<div class="am-radio inline">
				                          <input type="radio" name="daily_option" id="rad4" value="0" >
				                          <label for="rad4">{{lng_calendar.rec_every}}</label>
				                        </div>
				                        <select class="form-control inline" name="daily_days_count">
											{%for i in 1..30%}
											<option value="{{i}}">{{i}}</option>
											{%endfor%}
				                        </select>   
				                        <label class="inline">{{lng_calendar.rec_days}}</label> 
										</div>
									</div>

								</div>
								
								<div id="weekOptionBox" class="optBox" style="display: none;">

									<div class="form-group">
										  <label class="col-sm-3 control-label">{{lng_calendar.rec_every}}</label>
										<div class="col-sm-2 ">
											
											<select class="form-control" name="weekly_count" >
											{%for j in 1..10%}
												<option value="{{j}}">{{j}}</option>
											{%endfor%}
										</select>

										</div>
										<label class="col-sm-2 control-label" style="text-align:left;">{{lng_calendar.rec_weeks}}</label>
									</div>	 
				                    <div class="form-group">
				                      <label class="col-sm-3 control-label">{{lng_calendar.rec_on}}</label>
				                      <div class="col-sm-9">
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="checkm" name="weekday[]" value="1" checked="" type="checkbox">
				                          <label for="checkm">{{lng_common.mon_short}}</label>
				                        </div>
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check2" name="weekday[]" value="2" type="checkbox">
				                          <label for="check2">{{lng_common.tue_short}}</label>
				                        </div>
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check3" name="weekday[]" value="3" type="checkbox">
				                          <label for="check3">{{lng_common.wed_short}}</label>
				                        </div>

				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check4" name="weekday[]" value="4" type="checkbox">
				                          <label for="check4">{{lng_common.thu_short}}</label>
				                        </div>

				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check5" name="weekday[]" value="5" type="checkbox">
				                          <label for="check5">{{lng_common.fri_short}}</label>
				                        </div>

				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check6" name="weekday[]" value="6" type="checkbox">
				                          <label for="check6">{{lng_common.sat_short}}</label>
				                        </div>
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check7" name="weekday[]" value="7" type="checkbox">
				                          <label for="check7">{{lng_common.sun_short}}</label>
				                        </div>


				                      </div>
				                    </div>
									
								</div>
								
								<div id="monthOptionBox" class="optBox" style="display:none;">
									
									<div class="form-group form-inline">
										<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
										<div class="col-sm-9">
										<div class="am-radio inline">
											
													<input  type="radio" id="mo1" value="1" checked="checked" name="monthly_option">
													<label for="mo1">&nbsp;{{lng_calendar.rec_onday}}</label>
												
										</div>
										
										<select class="form-control" name="month_day" >
											{%for j in 1..30%}
												<option value="{{j}}">{{j}}</option>
											{%endfor%}
										</select>
										&nbsp;of every 
										<select class="form-control" name="month_count1" >
											{%for m in 1..12%}
												<option value="{{m}}">{{m}}</option>
											{%endfor%}
										</select>&nbsp;month(s)
										</div>
										
									
									</div>
								
									<div class="form-group form-inline">
										<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
										<div class="col-sm-9">
											<div class="am-radio inline">
										
													<input id="mo2" type="radio" class="ace" value="2" name="monthly_option">
													<label for="mo2">&nbsp;{{lng_calendar.rec_onday}}</label>
										
											</div>
											
											<select name="month_day_number" class="form-control">
												<option value="first">{{lng_calendar.rec_first}}</option>
												<option value="second">{{lng_calendar.rec_second}}</option>
												<option value="third">{{lng_calendar.rec_third}}</option>
												<option value="fourth">{{lng_calendar.rec_fourth}}</option>
												<option value="last">{{lng_calendar.rec_last}}</option>
											</select>
											
											<select name="month_week_day" class="form-control">
												<option value="mon">{{lng_calendar.rec_monday}}</option>
												<option value="tue">{{lng_calendar.rec_tuesday}}</option>
												<option value="wed">{{lng_calendar.rec_wednesday}}</option>
												<option value="thu">{{lng_calendar.rec_thursday}}</option>
												<option value="fri">{{lng_calendar.rec_friday}}</option>
												<option value="sat">{{lng_calendar.rec_sat}}</option>
												<option value="sun">{{lng_calendar.rec_sun}}</option>
											</select>
											of every
											<select class="form-control" name="month_count2" >
												{%for m in 1..12%}
													<option value="{{m}}">{{m}}</option>
												{%endfor%}
											</select>&nbsp;{{lng_calendar.rec_months}}
										</div>
										
										
									
									</div>						
								
													
								</div>
								
								<div id="yearOptionBox" class="optBox" style="display:none;">

									<div class="form-group form-inline">
										<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
										<div class="col-sm-9">
											<div class="am-radio inline">
													
												<input  type="radio" value="1" checked="checked" name="yarly_option" id="yo1">
												<label for="yo1">&nbsp;{{lng_calendar.rec_onday}}</label>
													
											</div>
											<select class="form-control" name="year_month1">
												<option value="jan">January</option>
												<option value="feb">{{lng_calendar.rec_feb}}</option>
												<option value="mar">{{lng_calendar.rec_mar}}</option>
												<option value="apr">{{lng_calendar.rec_apr}}</option>
												<option value="may">{{lng_calendar.rec_may}}</option>
												<option value="jun">{{lng_calendar.rec_jun}}</option>
												<option value="jul">{{lng_calendar.rec_jul}}</option>
												<option value="aug">{{lng_calendar.rec_aug}}</option>
												<option value="sep">{{lng_calendar.rec_sep}}</option>
												<option value="oct">{{lng_calendar.rec_oct}}</option>
												<option value="nov">{{lng_calendar.rec_nov}}</option>
												<option value="dec">{{lng_calendar.rec_dec}}</option>
											</select>&nbsp;
											<select class="form-control" name="year_day" >
												{%for m in 1..31%}
													<option value="{{m}}">{{m}}</option>
												{%endfor%}
											</select>&nbsp;{{lng_calendar.rec_months}}
										</div>
										
									</div>
									
									<div class="form-group form-inline">
											<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
											<div class="col-sm-9">
											<div class="am-radio inline">
												
													<input type="radio" id="yo2" value="2" name="yarly_option">
													<label for="yo2">&nbsp;{{lng_calendar.rec_onday}}</label>
												
												</div>
											
											<select name="year_day_number" class="form-control">
												<option value="first">{{lng_calendar.rec_first}}</option>
												<option value="second">{{lng_calendar.rec_second}}</option>
												<option value="third">{{lng_calendar.rec_third}}</option>
												<option value="fourth">{{lng_calendar.rec_fourth}}</option>
												<option value="last">{{lng_calendar.rec_last}}</option>
											</select>&nbsp;
											<select name="year_week_day" class="form-control">
												<option value="mon">{{lng_calendar.rec_monday}}</option>
												<option value="tue">{{lng_calendar.rec_tuesday}}</option>
												<option value="wed">{{lng_calendar.wednesday}}</option>
												<option value="thu">{{lng_calendar.rec_thursday}}</option>
												<option value="fri">{{lng_calendar.rec_friday}}</option>
												<option value="sat">{{lng_calendar.rec_sat}}</option>
												<option value="sun">{{lng_calendar.rec_sun}}</option>
											</select>&nbsp;of&nbsp;
											<select class="form-control" name="year_month2">
												<option value="jan">{{lng_calendar.rec_jan}}</option>
												<option value="feb">{{lng_calendar.rec_feb}}</option>
												<option value="mar">{{lng_calendar.rec_mar}}</option>
												<option value="apr">{{lng_calendar.rec_apr}}</option>
												<option value="may">{{lng_calendar.rec_may}}</option>
												<option value="jun">{{lng_calendar.rec_jun}}</option>
												<option value="jul">{{lng_calendar.rec_jul}}</option>
												<option value="aug">{{lng_calendar.rec_aug}}</option
												<option value="sep">{{lng_calendar.rec_sep}}</option>
												<option value="oct">{{lng_calendar.rec_oct}}</option>
												<option value="nov">{{lng_calendar.rec_nov}}</option>
												<option value="dec">{{lng_calendar.rec_dec}}</option>
											</select>&nbsp;
											</div>
									</div>							

								</div>
							</div>			        	

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" >{{lng_invoices.endofrec }}</label>
								<div class="col-sm-3" >
									<select class="form-control" name="er_end" id="ir">
										<option value="1">{{lng_invoices.ne }}</option>
										<option value="2">{{lng_invoices.sad }}</option>
										<option value="3">{{lng_invoices.sac }}</option>

									</select>	
								</div>
									
								<div class="col-sm-3" style="display: none;" id="rsd">
									<div class="input-group">
										<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="er_end_date" class="form-control date-picker">
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>							
								</div>

								<div class="col-sm-3 col-md-2" style="display: none;" id="rsc">
									<input type="text" name="er_end_count" value="1" class="form-control">
								</div>
						</div>
						
		      </div>

	        </div>
		</div>  	
	
</form>	
	<button type="button" class="btn btn-primary btn-lg pull-right doSave" style="margin-right: 5px;">{{lng_common.btn_save}}</button>
	<a href="#" class="btn btn-default btn-lg pull-left cancelAddEvent"  >{{lng_common.btn_cancel}}</a>
	<div style="clear:both;margin-bottom: 40px;"></div>	
  	</div>  	
  </div>	  
</div>
</div>

<div id="show-event-modal-back" class="hidden">
</div>
<div id="show-event-modal-close" class="hidden">
	<span class="s7-close-circle">  </span>
</div>
<div id="show-event-modal" class="hidden" data-not-set='{{lng_common.ns}}' data-delete-msg='{{lng_calendar.msg_delete}}' data-delete-description='{{lng_calendar.delete_description}}'>
	<div class="container" id="show-event-content">
	  <div class="row" id="show-event-inner-content">
		<div class="row show-event-table">
			<h3 class="col-sm-4"> 
		  	{{lng_calendar.title}}:  
	  		</h3>
	  		<h3 class="col-sm-8" id="show-event-e_title"> 
		  	TITLE
	  		</h3>
		</div>
	  	<div class="row show-event-table">
			<p class="col-sm-4"> 
		  	{{lng_calendar.when}}
	  		</p>
	  		<p class="col-sm-8" id="show-event-e_date"> DATE</p>	
		</div>
	  	<div class="row show-event-table">
			<p class="col-sm-4"> 
		  	{{lng_calendar.type}}:
	  		</p>
	  		<p class="col-sm-8" id="show-event-etype_name"> DATE</p>	
		</div>
		<div class="row show-event-table">
			<p class="col-sm-4"> 
		  	{{lng_calendar.loc}}:
	  		</p>
	  		<p class="col-sm-8" id="show-event-e_location"> LOCATION</p>	
		</div>
		<div class="row show-event-table">
			<p class="col-sm-4"> 
		  	{{lng_calendar.asign}}:
	  		</p>
	  		<p class="col-sm-8" id="show-event-e_client_id"> </p>	
		</div>
		<div class="row show-event-table">
			<p class="col-sm-4"> 
		  	{{lng_calendar.desc}}:
	  		</p>
	  		<p class="col-sm-8" id="show-event-e_desc"> </p>	
		</div>
		<div class="row show-event-table">
			<label class="col-sm-4"> {{lng_calendar.result}} </label>
			<p class="col-sm-8" id="show-event-complete-e_result_p" style="display: none"> </p>
			<textarea id="show-event-complete-e_result" class="col-md-12 form-control" rows="3"> </textarea>
			
		</div>
		<div id="show-event-edit-event" class="form-horizontal group-border-dashed" style="display: none">
			<div class="" style="">

        <div class="panel-body">

			<div class="form-group">
				
				<label class="col-sm-3 control-label">{{lng_calendar.when}}</label>
				
				<div class="col-md-3 col-sm-4" id="e-date">
					<div class="input-group pull-left">
						<span class="input-group-addon">
							<i class="fa fa-calendar bigger-110"></i>
						</span>
						<input type="text"
								id="edit_deadline_date" 
								data-date-format="{{dpformat}}" 
								readonly="" 
								value="{{today}}" 
								name="deadline_date" 
								class="form-control date-picker"
						>
						
					</div>						
				</div>
					
				<div class="col-md-3 col-sm-4">
					<div class="input-group pull-left">
						<span class="input-group-addon">
							<i class="fa fa-clock-o bigger-110"></i>
						</span>
						<input type="text" 
								id="edit-deadline_time" 
								value="{{dttime}}"  
								name="deadline_time" 
								class="form-control time-picker">
					</div>						
				</div>	
					
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">{{lng_calendar.type}}</label>
				<div class="col-sm-4">
					<select id="edit_type" 
							class="form-control" 
							name="e_type">
						{%for t in types%}
						<option value="{{t.etype_type}}" style="padding-top: 5px;">{{t.etype_name}}</option>
						{%endfor%}
					</select>	  
				</div>	  
			</div>				

            <div class="form-group">
              <label class="col-sm-3 control-label">{{lng_calendar.title}}<span style="color: red;">*</span></label>
              <div class="col-sm-9">
                <input class="form-control" 
                		id="edit_title"
                		name="title" 
                		data-errorbox="#errname" 
                		data-msg-required="{{lng_common.fr}}"  
                		required=""  
                		type="text"
                	>
                <div id="edit_errname" style="color:red;"></div>
              </div>
            </div>
           
			<div class="form-group">
				<label class="col-sm-3 control-label">{{lng_calendar.loc}}</label>
				<div class="col-sm-8">
				   <input id="edit_location" type="text" name="location" class="form-control">
				</div>	  
			</div>				

		   <div class="form-group">
			   <label class="col-sm-3 control-label">{{lng_calendar.asign}}</label>
			   <div class="col-sm-5">
			    	<select id="edit_client" name="client" class="select2">
				    	
			    		<option value="0">{{lng_calendar.assignto}}</option>
			    		{%if clients%}
			    		<optgroup label="{{lng_menu.clients}}">
			    		{%for c in clients%}
			    			<option {%if selClient == c.client_id%}selected{%endif%} value="{{c.client_id}}@cli">{{c.client_name}}</option>
			    		{%endfor%}
			    		</optgroup>
			    		{%endif%}
			    		{%if collabs%}
			    		<optgroup label="{{lng_menu.colleague}}">
			    		{%for c in collabs%}
			    			<option {%if selCollab == c.cb_id%}selected{%endif%} value="{{c.cb_id}}@usr">{{c.cb_billing_name}}</option>
			    		{%endfor%}
			    		</optgroup>
			    		{%endif%}
			    		
			    	</select>
				  
			   </div>	   
		   </div>	   
		   
		   <div class="form-group">
			   <label class="col-sm-3 control-label">{{lng_calendar.desc}}</label>
			   <div class="col-sm-9">
				   <textarea id="edit_desc" name="e_desc" class="form-control" rows="3"></textarea>
			   </div>	   
		   </div>
        </div>	
		</div>
		</div>
		<form id="edit-recurring" accept-charset="utf-8">
		<div class="">

	        <div class="panel-body">
		      

		      <div id="rec-on" >
		        	

						<div class="row form-group">
							<label class="col-sm-3 control-label no-padding-right" >{{lng_calendar.start_date}}</label>
								<div class="col-sm-4" >
									<div class="input-group">
										<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="er_date_start" class="form-control date-picker">
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>							
								</div>
									
						</div>
						<div id="" class="row form-group">
							<label class="col-sm-3 control-label no-padding-right">{{lng_calendar.time}}</label>
							
							<div class="col-sm-4">
									
									<div  class="input-group">
										<input type="text" value="{{dttime}}"  name="deadline_time" class="form-control time-picker">
										<span class="input-group-addon">
										<i class="fa fa-clock-o bigger-110"></i>
									</span>
									</div>
									
							</div>
						</div>
						

						<div class="row form-group" >
						 	<label class="col-sm-3 control-label no-padding-right"> {{lng_invoices.freq}}</label>
						 	<div class="col-sm-4">
						 			<select id="recOptions" name="er_repeat"  class="form-control">
							
										<option value="d">{{lng_calendar.daily}}</option>
										<option value="w">{{lng_calendar.weekly}}</option>
										<option value="m">{{lng_calendar.monthly}}</option>
										<option value="y">{{lng_calendar.yearly}}</option>
						 			
						 			</select>
						 		</div>
						</div>	

						<div id="recurrenceOptions" >
								
								<div id="dayOptionBox" class="optBox">
									
									<div class="form-group">
										<label class="col-sm-3 control-label">&nbsp;</label>
										<div class="col-sm-9">
											<div class="am-radio">	
					                          <input id="check1" name="daily_option" value="1" checked="" type="radio">
					                          <label for="check1">{{lng_calendar.rec_every_weekday}}</label>
											</div>	
										</div>	
									</div>
									<div class="form-group form-inline">
										<label class="col-sm-3 control-label">&nbsp;</label>
										<div class="col-sm-9">
										<div class="am-radio inline">
				                          <input type="radio" name="daily_option" id="rad4" value="0" >
				                          <label for="rad4">{{lng_calendar.rec_every}}</label>
				                        </div>
				                        <select class="form-control inline" name="daily_days_count">
											{%for i in 1..30%}
											<option value="{{i}}">{{i}}</option>
											{%endfor%}
				                        </select>   
				                        <label class="inline">{{lng_calendar.rec_days}}</label> 
										</div>
									</div>

								</div>
								
								<div id="weekOptionBox" class="optBox" style="display: none;">

									<div class="form-group">
										  <label class="col-sm-3 control-label">{{lng_calendar.rec_every}}</label>
										<div class="col-sm-2 ">
											
											<select class="form-control" name="weekly_count" >
											{%for j in 1..10%}
												<option value="{{j}}">{{j}}</option>
											{%endfor%}
										</select>

										</div>
										<label class="col-sm-2 control-label" style="text-align:left;">{{lng_calendar.rec_weeks}}</label>
									</div>	 
				                    <div class="form-group">
				                      <label class="col-sm-3 control-label">{{lng_calendar.rec_on}}</label>
				                      <div class="col-sm-9">
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="checkm" name="weekday[]" value="1" checked="" type="checkbox">
				                          <label for="checkm">{{lng_common.mon_short}}</label>
				                        </div>
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check2" name="weekday[]" value="2" type="checkbox">
				                          <label for="check2">{{lng_common.tue_short}}</label>
				                        </div>
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check3" name="weekday[]" value="3" type="checkbox">
				                          <label for="check3">{{lng_common.wed_short}}</label>
				                        </div>

				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check4" name="weekday[]" value="4" type="checkbox">
				                          <label for="check4">{{lng_common.thu_short}}</label>
				                        </div>

				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check5" name="weekday[]" value="5" type="checkbox">
				                          <label for="check5">{{lng_common.fri_short}}</label>
				                        </div>

				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check6" name="weekday[]" value="6" type="checkbox">
				                          <label for="check6">{{lng_common.sat_short}}</label>
				                        </div>
				                        <div class="am-checkbox inline" style="margin-left: 8px;">
				                          <input id="check7" name="weekday[]" value="7" type="checkbox">
				                          <label for="check7">{{lng_common.sun_short}}</label>
				                        </div>


				                      </div>
				                    </div>
									
								</div>
								
								<div id="monthOptionBox" class="optBox" style="display:none;">
									
									<div class="form-group form-inline">
										<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
										<div class="col-sm-9">
										<div class="am-radio inline">
											
													<input  type="radio" id="mo1" value="1" checked="checked" name="monthly_option">
													<label for="mo1">&nbsp;{{lng_calendar.rec_onday}}</label>
												
										</div>
										
										<select class="form-control" name="month_day" >
											{%for j in 1..30%}
												<option value="{{j}}">{{j}}</option>
											{%endfor%}
										</select>
										&nbsp;of every 
										<select class="form-control" name="month_count1" >
											{%for m in 1..12%}
												<option value="{{m}}">{{m}}</option>
											{%endfor%}
										</select>&nbsp;month(s)
										</div>
										
									
									</div>
								
									<div class="form-group form-inline">
										<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
										<div class="col-sm-9">
											<div class="am-radio inline">
										
													<input id="mo2" type="radio" class="ace" value="2" name="monthly_option">
													<label for="mo2">&nbsp;{{lng_calendar.rec_onday}}</label>
										
											</div>
											
											<select name="month_day_number" class="form-control">
												<option value="first">{{lng_calendar.rec_first}}</option>
												<option value="second">{{lng_calendar.rec_second}}</option>
												<option value="third">{{lng_calendar.rec_third}}</option>
												<option value="fourth">{{lng_calendar.rec_fourth}}</option>
												<option value="last">{{lng_calendar.rec_last}}</option>
											</select>
											
											<select name="month_week_day" class="form-control">
												<option value="mon">{{lng_calendar.rec_monday}}</option>
												<option value="tue">{{lng_calendar.rec_tuesday}}</option>
												<option value="wed">{{lng_calendar.rec_wednesday}}</option>
												<option value="thu">{{lng_calendar.rec_thursday}}</option>
												<option value="fri">{{lng_calendar.rec_friday}}</option>
												<option value="sat">{{lng_calendar.rec_sat}}</option>
												<option value="sun">{{lng_calendar.rec_sun}}</option>
											</select>
											of every
											<select class="form-control" name="month_count2" >
												{%for m in 1..12%}
													<option value="{{m}}">{{m}}</option>
												{%endfor%}
											</select>&nbsp;{{lng_calendar.rec_months}}
										</div>
										
										
									
									</div>						
								
													
								</div>
								
								<div id="yearOptionBox" class="optBox" style="display:none;">

									<div class="form-group form-inline">
										<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
										<div class="col-sm-9">
											<div class="am-radio inline">
													
												<input  type="radio" value="1" checked="checked" name="yarly_option" id="yo1">
												<label for="yo1">&nbsp;{{lng_calendar.rec_onday}}</label>
													
											</div>
											<select class="form-control" name="year_month1">
												<option value="jan">January</option>
												<option value="feb">{{lng_calendar.rec_feb}}</option>
												<option value="mar">{{lng_calendar.rec_mar}}</option>
												<option value="apr">{{lng_calendar.rec_apr}}</option>
												<option value="may">{{lng_calendar.rec_may}}</option>
												<option value="jun">{{lng_calendar.rec_jun}}</option>
												<option value="jul">{{lng_calendar.rec_jul}}</option>
												<option value="aug">{{lng_calendar.rec_aug}}</option>
												<option value="sep">{{lng_calendar.rec_sep}}</option>
												<option value="oct">{{lng_calendar.rec_oct}}</option>
												<option value="nov">{{lng_calendar.rec_nov}}</option>
												<option value="dec">{{lng_calendar.rec_dec}}</option>
											</select>&nbsp;
											<select class="form-control" name="year_day" >
												{%for m in 1..31%}
													<option value="{{m}}">{{m}}</option>
												{%endfor%}
											</select>&nbsp;{{lng_calendar.rec_months}}
										</div>
										
									</div>
									
									<div class="form-group form-inline">
											<label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
											<div class="col-sm-9">
											<div class="am-radio inline">
												
													<input type="radio" id="yo2" value="2" name="yarly_option">
													<label for="yo2">&nbsp;{{lng_calendar.rec_onday}}</label>
												
												</div>
											
											<select name="year_day_number" class="form-control">
												<option value="first">{{lng_calendar.rec_first}}</option>
												<option value="second">{{lng_calendar.rec_second}}</option>
												<option value="third">{{lng_calendar.rec_third}}</option>
												<option value="fourth">{{lng_calendar.rec_fourth}}</option>
												<option value="last">{{lng_calendar.rec_last}}</option>
											</select>&nbsp;
											<select name="year_week_day" class="form-control">
												<option value="mon">{{lng_calendar.rec_monday}}</option>
												<option value="tue">{{lng_calendar.rec_tuesday}}</option>
												<option value="wed">{{lng_calendar.wednesday}}</option>
												<option value="thu">{{lng_calendar.rec_thursday}}</option>
												<option value="fri">{{lng_calendar.rec_friday}}</option>
												<option value="sat">{{lng_calendar.rec_sat}}</option>
												<option value="sun">{{lng_calendar.rec_sun}}</option>
											</select>&nbsp;of&nbsp;
											<select class="form-control" name="year_month2">
												<option value="jan">{{lng_calendar.rec_jan}}</option>
												<option value="feb">{{lng_calendar.rec_feb}}</option>
												<option value="mar">{{lng_calendar.rec_mar}}</option>
												<option value="apr">{{lng_calendar.rec_apr}}</option>
												<option value="may">{{lng_calendar.rec_may}}</option>
												<option value="jun">{{lng_calendar.rec_jun}}</option>
												<option value="jul">{{lng_calendar.rec_jul}}</option>
												<option value="aug">{{lng_calendar.rec_aug}}</option
												<option value="sep">{{lng_calendar.rec_sep}}</option>
												<option value="oct">{{lng_calendar.rec_oct}}</option>
												<option value="nov">{{lng_calendar.rec_nov}}</option>
												<option value="dec">{{lng_calendar.rec_dec}}</option>
											</select>&nbsp;
											</div>
									</div>							

								</div>
							</div>			        	

						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" >{{lng_invoices.endofrec }}</label>
								<div class="col-sm-3" >
									<select class="form-control" name="er_end" id="ir">
										<option value="1">{{lng_invoices.ne }}</option>
										<option value="2">{{lng_invoices.sad }}</option>
										<option value="3">{{lng_invoices.sac }}</option>

									</select>	
								</div>
									
								<div class="col-sm-3" style="display: none;" id="rsd">
									<div class="input-group">
										<input type="text" data-date-format="{{dpformat}}" readonly value="{{today}}" name="er_end_date" class="form-control date-picker">
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>							
								</div>

								<div class="col-sm-3 col-md-2" style="display: none;" id="rsc">
									<input type="text" name="er_end_count" value="1" class="form-control">
								</div>
						</div>
						
		      </div>

	        </div>
		</div>  	
		<div class="edit-recuring-buttons">
			<button class="pull-left btn btn-success" id="cancel-rec-event">{{lng_common.btn_cancel}}</button>  	
			<button class="pull-right btn btn-info" id="edit-recuring-event">{{lng_common.btn_save}}</button>
		</div>
	</form>
		<div class="row show-issue-table">
			<h3 class="col-sm-4"> 
		  	{{lng_calendar.iss_subj}}:  
	  		</h3>
	  		<h3 class="col-sm-8" id="show-issue-title"> 
		  	TITLE
	  		</h3>
		</div>
	  	<div class="row show-issue-table">
			<p class="col-sm-4"> 
		  	{{lng_calendar.iss_created}}
	  		</p>
	  		<p class="col-sm-8" id="show-issue-cr_date"> DATE</p>	
		</div>
		<div class="row show-issue-table">
			<p class="col-sm-4"> 
		  	{{lng_calendar.iss_deadline}}
	  		</p>
	  		<p class="col-sm-8" id="show-issue-dl_date"> DATE</p>	
		</div>
		
		<div class="row show-issue-table">
	  		<p class="col-sm-12" id="show-issue-desc"> </p>	
		</div>
		
		<div class="row show-event-buttons">
			<button id="show-event-delete-button" class="btn btn-danger col-md-2">{{lng_common.btn_del}}</button>
			<button id="show-event-complete-button" class="btn btn-success col-md-2">{{lng_calendar.btn_complete}}</button>	
			
			<button id="show-event-edit-button"class="pull-right btn btn-primary col-md-2">{{lng_common.btn_edit}}</button>
			<button id="show-event-edit-rec-button" class="pull-right btn btn-info col-md-2">{{lng_calendar.btn_edit_rec}}</button>
			<button id="show-event-reopen" class="pull-right btn btn-info col-md-2">{{lng_calendar.reopen_btn}}</button>

		</div>
		<div id="show-event-save-edited-event" style="display: none">
			<button id="show-event-cancel-edit" class="pull-left btn btn-success col-md-2">{{lng_common.btn_cancel}}</button>
			<button id="show-event-save-edited-event-button" class="pull-right btn btn-primary col-md-2">{{lng_common.btn_save}}</button>
	  </div>
	  <div class="show-issue-buttons">
		  <button id="show-issue-resolve" class="pull-left btn btn-success col-md-2">{{lng_calendar.iss_complete}}</button>
		  <button id="show-issue-details" class="pull-right btn btn-info col-md-2">{{lng_calendar.iss_details}}</button></a>

	  </div>
	</div>
	
</div>
{%endblock%}

{%block js%}

    <script src="/assets/lib/date-time/moment.min.js"></script>
	<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>	
	<script src="/assets/lib/date-time/bootstrap-timepicker.js"></script>
    <script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>	
    <script src="/assets/lib/switch/js/bootstrap-switch.min.js"></script>
   {# <script src="/assets/lib/fullcalendar-1.6.7/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>#}
    <script src="/assets/lib/fullcalendar-3.6.1/fullcalendar.min.js" type="text/javascript"></script>
   {%if acs.user_lang!="en" %}<script src="/assets/lib/fullcalendar-3.6.1/locale/{{acs.user_lang}}.js"></script>{%endif%}
    <script src="/js/dev/calendar.js?v={{hashver}}"></script>
{%endblock%}

