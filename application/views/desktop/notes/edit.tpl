{% extends skin~"/common/root.tpl" %}
{% block headcss %}

    <link rel="stylesheet" href="/assets/lib/redactor2/redactor.css?v={{ hashver }}"/>
{% endblock %}

{% block content %}
    <div class="main-content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form id="fdata" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                    <input type="hidden" name="note_id" value="{{ notes.note_id }}">
                    <div class="panel panel-default panel-borders" style="margin-bottom:5px;">
                        <div class="panel-heading">
                            <span class="title">{{ lng_notes.btn_add }}</span>
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ lng_notes.subj }}</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="note_subj" type="text"
                                           value="{{ notes.note_subj }}">
                                </div>
                            </div>


                            <div class="form-group">

                                <div class="col-xs-12">
                                    <textarea name="note_text" id="notes" rows="8" class="form-control"
                                              required>{{ notes.note_text }}</textarea>

                                </div>
                            </div>

                        </div>
                    </div>

                    <button type="button" id="doSave"
                            class="btn btn-primary btn-lg pull-right">{{ lng_common.btn_save }}</button>
                    <a href="/notes" class="btn btn-default btn-lg pull-left">{{ lng_common.btn_cancel }}</a>
                    <div style="clear:both;margin-bottom: 20px;"></div>
                </form>
            </div>
        </div>
    </div>


{% endblock %}



{% block js %}
    <script type="text/javascript" src="/assets/lib/redactor2/redactor.js?v={{ hashver }}"></script>
    <script src="/js/dev/notes_edit.js"></script>
{% endblock %}
