{% extends skin~"/common/root.tpl" %}

{% block headcss %}
    <style>
        .wells {

            padding: 5px 15px 10px 15px;
            background: white;
            box-shadow: 0 0 5px rgba(122, 122, 122, 0.5);
            word-wrap: break-word;
            cursor: pointer;

        }

        .m-r-5 {

            margin-right: 5px;
        }

        body.modal-open {
            overflow: visible;
        }

        .modal-header {

            padding-bottom: 10px;;

        }

        .modal-footer {
            padding-top: 7px;
        }

        .modal-body {
            max-height: calc(100vh - 210px);
            overflow-y: auto;
        }

    </style>
{% endblock %}

{% block content %}
    <input type="hidden" id="_ndt" value="{{lng_notes.del_bundle_title}}">
    <input type="hidden" id="_ndm" value="{{lng_notes.del_bundle_message}}">
    <input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">

    <div class="page-head">
        <h3 style="margin-bottom: 2px; margin-top: 2px;">{{ lng_notes.title }}
            <a href="#" class="btn btn-head btn-success pull-right g-l" data-go="/notes/add" data-toggle="tooltip"
               data-placement="bottom" data-original-title="{{ lng_notes.btn_add }}">{{ lng_notes.btn_add }}</a>
        </h3>
    </div>
    <div class="main-content">
        <form id="fsearch" class="form-horizontal group-border-dashed">


            <div class="form-group">
                <div class="col-xs-12">

                    <div class="input-group">

                        <span class="input-group-addon btn-primary"
                              style="background-color:#ccc; color:#fff; font-size: 20px;"><span
                                    class="s7-search"></span></span>
                        <input class="form-control" id="s" placeholder="{{ lng_notes.search_ph }}"
                               {% if searchstr %}value="{{ searchstr }}"{% endif %} type="text"
                               style="border-width:1px;">


                    </div>
                </div>
            </div>
        </form>

        <form id="flist">
        <div class="gallery-container" id="d-content">
        </div>
        <div style="text-align:center">
            <button id="load-more" data-next="" style="display:none;" class="btn btn-lg btn-primary">{{ lng_notes.load }}</button>

        </div>
        </form>
    </div>

{% endblock %}

{%block footer%}
    <div id="megafooter">

    </div>
{%endblock%}

{% block js %}
	{#<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>#}
    <script src="/assets/lib/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="/js/dev/notes.js?v={{ hashver }}"></script>



{% endblock %}

