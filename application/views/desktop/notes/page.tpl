{% for r in records %}

    <div class="item">
        <div id="short{{ r.note_id }}" class="wells">
            <div class="col-sm-10 item1 div{{ r.note_id }}" data-id="{{ r.note_id }}">
            <h4 class="short_subj">{% if r.note_subj %}{{ r.note_subj|truncate(40) }}{% else %}<span
                            class="text-muted">{{ lng_notes.no_subj }}</span>{% endif %}</h4>
            </div>
            <div class="am-checkbox col-sm-2" style="padding-left: 7px;">
                <input type="checkbox" id="chk{{r.note_id}}" class="check" name="row[]" value="{{r.note_id}}">
                <label for="chk{{r.note_id}}"></label>
            </div>
            <div class="item1 div{{ r.note_id }}" data-id="{{ r.note_id }}">

            <p class="short_text">{{ r.note_text|truncate(400) }}</p>
        </div>
        </div>
    </div>


{% endfor %}
         
          