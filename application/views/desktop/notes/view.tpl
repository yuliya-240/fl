<input type="hidden" name="note_id" data-id="{{ notes.note_id }}" value="{{ notes.note_id }}">
<input type="hidden" id="_idt" value="{{ lng_notes.del_note_title }}">
<input type="hidden" id="_idm" value="{{ lng_notes.del_note_message }}">
<input type="hidden" id="_yes" value="{{ lng_common.btn_yes }}">
<input type="hidden" id="_cb" value="{{ lng_common.btn_cancel }}">
<div class="col-sm-12">
    {{ notes.note_text }}
</div>

<div style="clear:both;"></div>

