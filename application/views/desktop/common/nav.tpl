      <nav class="navbar navbar-default navbar-fixed-top am-top-header">
        <div class="container-fluid">
          <div class="navbar-header">
            <div class="page-title"><span>{{page_title}}</span></div>
            <a href="#" class="am-toggle-left-sidebar navbar-toggle collapsed">
	            <span class="icon-bar">
	            <span></span>
	            <span></span>
	            <span></span>
	            </span></a>
				<a href="/" class="navbar-brand"></a>
          </div>
          {#<a href="#" class="am-toggle-right-sidebar"><span class="icon s7-menu2"></span></a>#}
          <a href="#" data-toggle="collapse" data-target="#am-navbar-collapse" class="am-toggle-top-header-menu collapsed">
	          <span class="icon s7-angle-down"></span>
	      </a>
          <div id="am-navbar-collapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right am-user-nav">
              <li class="dropdown">
              <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
	              <img id="u-ava-min" class="img-responsible" {%if acs.user_pic=="nopic.png"%}src="/img/avatar/avatar2x200.jpg"{%else%}src="/userfiles/{{acs.user_id}}/pic/{{acs.user_pic}}"{%endif%}>
	              <span class="user-name">{{acs.user_name}}</span><span class="angle-down s7-angle-down"></span>
	          </a>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="/profile"> <span class="icon s7-user"></span>{{lng_menu.profile}}</a></li>
                  <li><a href="/settings"> <span class="icon s7-config"></span>{{lng_menu.settings}}</a></li>
                  <li><a href="/logout"> <span class="icon s7-power"></span>{{lng_common.exit}}</a></li>
                </ul>
              </li>
            </ul>
            
            <ul class="nav navbar-nav am-nav-right">
              <li><a href="/">{{lng_menu.dash}}</a></li>
              <li><a href="/notes">{{lng_menu.notes}}</a></li>
              <li><a href="/portfolio">{{lng_menu.portfolio}}</a></li>
       
             {# <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">Services <span class="angle-down s7-angle-down"></span></a>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="#">UI Consulting</a></li>
                  <li><a href="#">Web Development</a></li>
                  <li><a href="#">Database Management</a></li>
                  <li><a href="#">Seo Improvement</a></li>
                </ul>
              </li>
              <li><a href="#">Support</a></li>#}
            </ul>
            
            <ul class="nav navbar-nav navbar-right am-icons-nav">
	           {# <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon s7-diamond"></span> <span class="text-success">10</span></a>
                <ul class="dropdown-menu am-connections">
                  <li>
                    <div class="title">{{lng_common.gems}}</div>
                    <div class="list">
                      <div class="content">
                        <ul>
                          <li>
                            <div class="logo"><span style="font-size: 28px;" class="icon s7-diamond"></span></div>
                            <div class="field"><span>{{lng_common.add_gems}}</span>
                              
                            </div>
                          </li>
                          <li>
                            <div class="logo"><img src="assets/img/bitbucket.png"></div>
                            <div class="field"><span>{{lng_common.send_gems}}</span>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="footer"> <a href="#">{{lng_common.gems_help}}</a></div>
                  </li>
                </ul>
              </li>#}
              <li class="dropdown"><a href="#"  role="button" id="showMessanger" ><span class="icon s7-comment"></span><span id="msgUnreadCount" {%if not isUnread%}style="display: none;"{%endif%} class="indicator"></span></a>
               </li>
              <li class="dropdown"><a href="#" id="notifyBox" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon s7-bell"></span><span id="notifyCount" class="indicator" {%if not previewNotifyCount%}style="display:none;"{%endif%}></span></a>
                <ul class="dropdown-menu am-notifications">
                  <li>
                    <div class="title">{{lng_menu.n_title}}<span class="badge">{{previewNotifyCount}}</span></div>
                    <div class="list">
                      <div class="am-scroller nano">
                        <div class="content nano-content">
                          <ul>
	                        {%for n in noteficationsList%}  
                            <li {%if n.n_status_preview==0%}class="active n-list"{%endif%}>
                            	<a href="{{n.n_link}}">
	                                <div class="logo">
		                            {%if n.n_src==0%}
		                                <span class="icon s7-gleam"></span>
		                            {%endif%}
		                            
		                            {%if n.n_src==1%}
		                                <span class="icon s7-id"></span>
		                            {%endif%}
		                            
		                            </div>
	                                <div class="user-content">
		                                {%if n.n_status_preview==0%}<span class="circle"></span>{%endif%}
		                         
		                                <span class="text-content"> {{n.ntype_desc}}</span>
		                                <span class="date"><time datetime="{{n.n_date|date('Y-m-d')}}T{{n.n_date|date('H:i:s')}}" class="timeago"></time></span>
		                            </div>
	                            </a>
	                        </li>
	                        {%endfor%}
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="footer"> <a href="#">{{lng_menu.n_view_all}}</a></div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
