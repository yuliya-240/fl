{% extends skin~"/common/root.tpl" %}

{%block content%}
      <div class="am-content">
        <div class="main-content">
          <div class="error-container">
            <div class="error-image"></div>
            <div class="error-number">404</div>
            <p class="error-description">The page you are looking for might have been removed.</p>
            <div class="error-goback-text">Would you like to go <a href="/">home</a>?</div>
            
          </div>
        </div>
      </div>
{%endblock%}