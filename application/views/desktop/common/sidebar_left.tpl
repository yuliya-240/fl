      <div class="am-left-sidebar">
        <div class="content">
          <div class="am-logo"></div>
          <ul class="sidebar-elements">
     
	        <li class="{{sbPeople}}">
	          <a href="#"><i class="icon s7-users"></i><span>{{lng_menu.ppl}}</span></a>
	          <ul class="sub-menu">
	           <li class="{{subClients}}"><a href="#" class="g-l" data-go="/clients">{{lng_menu.clients}}</a></li>
	           <li class="{{subCollabs}}"><a href="#" class="g-l" data-go="/users">{{lng_menu.colleague}}</a></li>
	           <li class="{{subInvites}}"><a href="#" class="g-l" data-go="/users/invites">{{lng_menu.invites}} {%if countInvites%}&nbsp;<span class="text-danger">{{countInvites}}</span>{%endif%}</a></li>
	          </ul>
	        </li>
	       
	        <li class="{{sbMoney}}"><a href="#"><i class="icon s7-cash"></i><span>{{lng_menu.money}}</span></a>
	          <ul class="sub-menu">
	           <li class="{{subInvoices}}"><a href="#" class="g-l" data-go="/invoices">{{lng_menu.inv}}</a></li>
	           <li class="{{subInvoicesRec}}"><a href="#" class="g-l" data-go="/invoices/recurring">{{lng_menu.invrec}}</a></li>
	           <li class="{{subEstimates}}"><a href="/estimates">{{lng_menu.est}}</a></li>
	           <li class="{{subTransactions}}"><a href="/transactions">{{lng_menu.trans}}</a></li>
	           <li class="{{subExpenses}}"><a href="/expenses">{{lng_menu.exp}}</a></li>
               <li class="{{subExpensesCat}}"><a href="/expenses/categories">{{lng_menu.expcat}}</a></li>
	           
	          </ul>
	        </li>
            
            <li class="{{sbRes}}"><a href="#"><i class="icon s7-plugin"></i><span>{{lng_menu.res}}</span></a>
              <ul class="sub-menu">
	            <li class="{{subPortfolio}}"><a href="/portfolio">{{lng_menu.portfolio}}</a></li>
                <li class="{{subSrv}}"><a href="/services">{{lng_menu.srv}}</a></li>
                <li class="{{subEquip}}"><a href="/equip">{{lng_menu.equip}}</a></li>
              </ul>
            </li>

            <li class="{{sbProjects}}"><a href="#"><i class="icon s7-portfolio"></i><span>{{lng_menu.projects}}</span></a>
              <ul class="sub-menu">
                <li class="{{subPrj}}"><a href="/projects">{{lng_menu.projects}}</a></li>
                <li class="{{subIssues}}"><a href="/issues">{{lng_menu.bugtracker}}</a></li>
              </ul>
            </li>
            
            <li class="{{sbCalendar}}"><a href="#"><i class="icon icon s7-date"></i><span>{{lng_menu.cal}}</span></a>
              <ul class="sub-menu">
	            <li class="{{subDoc}}"><a href="/calendar">{{lng_menu.cal_shedule}}</a></li>
                <li class="{{subNotes}}"><a href="/calendar/recurring">{{lng_menu.cal_rec}}</a></li>
              </ul>
            </li>

            <li class="{{sbDoc}}"><a href="#"><i class="icon icon s7-note"></i><span>{{lng_menu.doc}}</span></a>
              <ul class="sub-menu">
	            <li class="{{subDoc}}"><a href="/doc">{{lng_menu.doc}}</a></li>
                <li class="{{subNotes}}"><a href="/notes">{{lng_menu.notes}}</a></li>
              </ul>
            </li>
            
          </ul>
          <!--Sidebar bottom content-->
        </div>
      </div>