<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Floctopus</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/assets/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/lib/stroke-7/style.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/lib/jquery.nanoscroller/css/nanoscroller.css"/>
    <!--[if lt IE 9]>
	    
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {%block preheadcss%}{%endblock%}
    
    {#<link rel="stylesheet" href="/assets/css/style.css" type="text/css"/>#}
    <link rel="stylesheet" href="/assets/css/themes/theme-google.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/lib/sweetalert/sweetalert.css" type="text/css"/>
    <link rel="stylesheet" href="/css/app.css" type="text/css"/>
	<link rel="stylesheet" href="/assets/css/chat.css" type="text/css"/>
	
	{%block headcss%}{%endblock%}
	
	
	<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  </head>
  <body>
    <div class="am-wrapper am-white-header am-fixed-sidebar {{fixed_footer}}">
	
	<div id="process-loader" class="fade">
	    <div class="material-loader">
	        <svg class="circular" viewBox="25 25 50 50">
	            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
	        </svg>
	        <div class="message">{{lng_common.loader}}</div>
	    </div>
	</div>
	
	<input type="hidden" id="curl" value="{{curl}}">
	<input type="hidden" id="lang" value="{{acs.user_lang}}">
	<input type="hidden" id="_mch" value="{{channel}}">	   
	<input type="hidden" id="_broadcast" value="broadcast">	
	<input type="hidden" id="_chsound" value="{{chsound}}">
	<input type="hidden" id="chatopen" value="0">	
	<input type="hidden" id="c-u" value="{{acs.user_id}}">
	<input type="hidden" id="basecurrency" value="{{acs.user_currency}}">
	<input type="hidden" id="weekstart" value="{{acs.user_wstart}}">
	 <input type="hidden" id="tformat" value="{{acs.user_tformat}}">
    	{% include skin~"/common/nav.tpl" %} 
    	{% include skin~"/common/sidebar_left.tpl" %} 
    	<div class="am-content">
	    	
	    	{%block content%}{%endblock%}

			 <div id="msg-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
			  <div style="width: 100%;" class="modal-dialog">
			    <div class="modal-content" id="ch-c" style="max-width: 90%; background-image: url(/img/chatbg4.jpg); background-color: #e7e7e7; position: relative;">
				
			    </div>
			  </div>
			 </div>   

	    	{%block footer%}{%endblock%}	
	    	
	    	<div class="main-content" id="profile-view-box"  style="display: none;">
	    		<div class="row" >
					<div class="col-xs-12" id="profile-view-box-content">
					  	
					</div>	
				</div>	
	    	</div>
    	</div>
		<!-- right sidebar -->
    </div>

    {#<script src="/assets/lib/jquery/jquery.min.js" type="text/javascript"></script>#}
    <script  src="http://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="/assets/lib/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/assets/lib/touch/jquery.ui.touch.js"></script>
    <script src="/assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js" type="text/javascript"></script>
    <script src="/assets/js/main.js" type="text/javascript"></script>
    <script src="/assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function(){
	    App.init({
	      openLeftSidebarOnClick: true
	    });      
    });
      
    </script>
    	
	<script src="/assets/lib/jquery-validate/jquery.validate.min.js"></script>
	<script src="/assets/lib/sweetalert/sweetalert.min.js"></script>
	{#<script src="/assets/lib/bootbox/bootbox.min.js"></script>	#}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
	<script src="/assets/lib/timeago/jquery.timeago.js"></script>
	<script src="/assets/lib/timeago/locales/jquery.timeago.{{acs.user_lang}}.js"></script>
	<script src="/js/dev/common.js?v={{hashver}}"></script>
	<script src="/assets/lib/plupload-2.3.1/js/plupload.full.min.js"></script>
	<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>

	<script src="/js/dev/chat.js?v={{hashver}}"></script>	

    {%block js%}{%endblock%}
    
    {%block modals%}{%endblock%}
  </body>

</html>
