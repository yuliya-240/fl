{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs">

			</th>
			<th></th>
			<th>{{ lng_users.name }}</th>
			<th>{{ lng_users.username }}</th>
		
			<th>{{ lng_users.address }}</th>
			<th width="1%"></th>
		</tr>
	</thead>
	<tbody>
		{%for r in records%}
		<tr>
			<td class="text-center hidden-xs" style="vertical-align:middle;">
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.cb_id}}" class="check" name="row[]" value="{{r.cb_id}}">
	              <label for="chk{{r.cb_id}}"></label>
	            </div>
			</td>
			<td class="view gl" data-id="{{r.user_id}}" style="text-align: center;">
				<img class="img-circle" src="{%if r.user_pic=="nopic.png"%}/img/avatar/avatar2x200.jpg{%else%}/userfiles/{{r.user_id}}/pic/{{r.user_pic}}{%endif%}" style="height: 47px; width: 47px;">
			</td>
			<td class="view gl" data-id="{{r.user_id}}">{{r.user_name}}</td>
			<td class="view gl" data-id="{{r.user_id}}">{{r.user_username}}<br>{{r.user_email}}</td>
		
			<td class="view gl" data-id="{{r.user_id}}" style="vertical-align: middle;">

				
				{%if r.cb_billing_street or  r.cb_billing_city or r.cb_billing_state or r.cb_billing_zip %}
			
				{%if r.cb_billing_street%}
				{{r.cb_billing_street}},
				{%endif%}
				{%if r.cb_billing_city or r.cb_billing_state or r.cb_billing_zip%}
				 {{r.cb_billing_city}}  {{r.cb_billing_state}} {{r.cb_billing_zip}}, 
				{%endif%} 
				 {{r.cb_billing_country}}
						 
				{%else%}
				<span class="text-danger">Not Set</span>
			
				{%endif%}

			</td>
			{#<td class="text-danger"><a href="#" class="del text-danger" data-id="{{r.cb_id}}"><i class="material-icons">close</i></a></td>#}
			
			<td style="vertical-align: middle;">
				<a href="#" class="view" data-id="{{r.user_id}}"><i class="material-icons">visibility</i></a>
				
			</td>
		</tr>
		{%endfor%}
	</tbody>
</table>				
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>

{%endif%}
