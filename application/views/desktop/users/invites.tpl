{% extends skin~"/common/root.tpl" %}
{%block headcss%}
<style>
	
</style>	
{%endblock%}

{%block content%}
<input type="hidden" id="_udt" value="{{lng_users.del_invite_title}}">
<input type="hidden" id="_udm" value="{{lng_users.del_invite_message}}">
<input type="hidden" id="_bd" value="{{lng_users.btn_del}}">
<input type="hidden" id="_bc" value="{{lng_common.btn_cancel}}">
<input type="hidden" id="_at" value="{{lng_users.accept_bundle_title}}">
<input type="hidden" id="_am" value="{{lng_users.accept_bundle_msg}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="_dt" value="{{lng_users.delete_bundle_title}}">
<input type="hidden" id="_dm" value="{{lng_users.delete_bundle_msg}}">
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_users.title_invites}}
		<a href="#" class="btn btn-head btn-success   pull-right g-l" data-go="/users/add" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_users.add}}"> {{lng_users.add}}</a>

		
	</h3>
</div>	

<div class="main-content">
	<div class="row">
		<div class="col-xs-12">
		<div class="panel panel-default panel-borders">

	        <div class="panel-body">

				 <div id="d-content">
				 	
		         </div>
	        </div>
		</div>
		</div>
	</div>	
</div>    

{%endblock%}
{%block footer%}
<div id="megafooter">
	
</div>	
{%endblock%}

{%block js%}

   <script src="/js/dev/users_invites.js"></script>
    
{%endblock%}
