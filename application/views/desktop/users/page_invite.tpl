{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
{#	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs"></th>
			<th></th>
            <th>{{ lng_users.name }}</th>
            <th>{{ lng_users.username }}</th>
			<th>Email</th>
			
			<th width="15%"></th>
		</tr>
	</thead>#}
	<tbody>
		{%for r in records%}
		<tr {%if loop.index==1%} class="td-first" {%endif%}>
			<td class="text-center hidden-xs" style="vertical-align:middle;" >
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.cb_id}}" class="check" name="row[]" value="{{r.cb_id}}">
	              <label for="chk{{r.cb_id}}"></label>
	            </div>
			</td>
			<td>{%if r.cb_status==2%}<span class="text-danger">{{lng_users.irec}}</span>{%else%}{{lng_users.isent}}{%endif%}</td>
			<td>{{r.user_name}}</td>
			<td>{{r.user_username}}</td>
			<td>{{r.user_email}}</td>
			
			<td class="text-danger text-right">
				{%if r.cb_status==2%}<a href="#" class="accept-collab btn btn-sm btn-space btn-default text-success" data-id="{{r.cb_id}}">{{lng_common.accept}}</a>{%endif%}
				
				<a href="#" class="del text-danger btn-space pull-right" data-id="{{r.cb_id}}" style="padding-top: 5px; margin-left: 10px;">{{lng_common.del}}</a>
			</td>
		</tr>
		{%endfor%}
	</tbody>
</table>				
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
