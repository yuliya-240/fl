{% extends skin~"/common/root.tpl" %}
{%block headcss%}
    <link rel="stylesheet" href="/css/tt.css" type="text/css"/>
{%endblock%}

{%block content%}
<input type="hidden" id="_cet" value="{{lng_users.invite_user}}">
<input type="hidden" id="_uau" value="{{lng_users.add_user_question}}">
<input type="hidden" id="_yb" value="{{lng_users.btn_yes_invite}}">
<input type="hidden" id="_cb" value="{{lng_common.btn_cancel}}">

<div class="page-head">
    <h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_users.title_add}} 
	    <button data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_users.send_invent}}"  class="btn btn-head btn-success  pull-right sendInvite">{{lng_users.send_invent}}</button>
	    
	    
	    
	  <a href="/users" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.back_list}}"  class="btn btn-head btn-space  btn-dark pull-right"><i class="material-icons btn-head-s">arrow_back</i></a>
	</h3>
		  
</div>

<div class="main-content">
	
	<div class="row">
		
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default" style="margin-bottom:5px;">
               
                <div class="panel-body">
	                <div class="form-group">
                     
                      <div class="col-xs-12">
					  	<div class="input-group "><span class="input-group-addon"><i class="material-icons">group</i></span>
                          <input placeholder="{{lng_users.srch_ph}}" name="s" id="s" class="form-control" type="text">
                        </div>                       
                      </div>
                    </div>
                </div>      
            </div>	
			<div class="panel panel-default "  id="ulist-res-box" style="display: none;" >
                <div id="ulist-box" class="panel-body" style="display:none;">
	                
                </div>
                
                <div id="unet-box" class="panel-body text-center" style="display:none;">
	                <p >{{lng_common.norecords}} <a class="sendInvite" href="#">{{lng_users.send_invent}}</a></p>
                </div>
			</div>        
	
        </div>	
	</div>	
</div>   


 
 {%endblock%}
 {%block js%}


   <script src="/js/dev/users_add.js?v={{hashver}}"></script>
    
{%endblock%}

  {%block modals%}
     <div id="invite-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_users.send_invent_title}}</h3>
          </div>
           <form  id="fsend" class="form-horizontal">
          <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-10 col-xs-offset-1">
				  
                    <div class="form-group">
	                    
                      <div id="email-handler" class="input-group"><span class="input-group-addon"><i class="icon s7-mail" style="font-size:24px;;"></i></span>
                        <input type="email" id="email" name="email" required=""  data-errorbox="#error-box"  placeholder="E-mail" autocomplete="off" data-msg-required="{{lng_users.email_req}}" data-msg-email="{{lng_users.not_email}}"   class="form-control">
                      </div>
                      <div id="error-box" style="margin-top:5px;"></div>
                    </div>
			  	 
				 
		          </div>
	          </div>
	          <div class="row" id="user-own-email" style="margin-left:5px; margin-right: 5px; display:none;">
	          	<div class="col-xs-12">
	          		<div role="alert" class="alert alert-warning alert-icon alert-border-color ">
                    <div class="icon"><span class="s7-attention"></span></div>
                    <div class="message">
                      <strong>{{lng_users.common_warning}}</strong> {{lng_users.user_own_email}}
                    </div>
                  </div>
	          	</div>  	
	          </div>
	          
	          <div class="row" id="user-exist-box" style="margin-left:5px; margin-right: 5px; display:none;">
		          <div class="col-xs-12">
			          <div role="alert" class="alert alert-warning alert-icon alert-border-color">
                    <div class="icon"><span class="s7-attention"></span></div>
                    <div class="message">
                      <strong>{{lng_users.common_warning}}</strong> {{lng_users.user_exist}}
                    </div>
                  </div>
                  <div id="ex-u-b">
			          <table class="table">
				          <tr >
					          <td id="invite_username"></td>
					          <td id="invite_email"></td>
					          <td width="1%">
						          <a href="#" class="add-user text-success" >
						          <i class="material-icons">add_circle_outline</i>
						          </a>
						      </td>
				          </tr>
			          </table>    
                  </div>    
		          </div>    
		          
	          </div>
	          
	          
	          
          </div>
          <div class="modal-footer" >
            
            <button type="submit" id="send-invite"   class="btn btn-primary ">{{lng_users.send_invent}}</button>
            
            <button type="button"  class="btn btn-default  pull-left" data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
           </form>
        </div>
      </div>
    </div>
 
   {%endblock%}
