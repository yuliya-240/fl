{% extends skin~"/common/root.tpl" %}
{%block headcss%}
    <link rel="stylesheet" href="/css/profile.css" type="text/css"/>
<style>
.gl{
 cursor: pointer;	   
}
	   
 </style>	
{%endblock%}

{%block content%}

<input type="hidden" id="_udt" value="{{lng_users.del_user_title}}">
<input type="hidden" id="_udm" value="{{lng_users.del_user_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="_cb" value="{{lng_common.btn_cancel}}">
<input type="hidden" id="_bdt" value="{{lng_users.delete_users_bundle_title}}">
<input type="hidden" id="_bdm" value="{{lng_users.delete_users_bundle_msg}}">

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_users.title}}
		
		<a href="#" class="btn btn-head btn-success  pull-right g-l" data-go="/users/add" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_users.add}}" id="btn-add-collab"> {{lng_users.add}}</a>
		<button class="btn btn-head btn-danger pull-right del" id="btn-del" style="display: none;">Delete</button>
		
		<a href="#" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.back_list}}"  class="btn btn-head btn-space  btn-dark pull-right" id="btn-back-list" style="display: none;"><i class="material-icons btn-head-s">arrow_back</i></a>

		
	</h3>
</div>	


<div class="main-content" id="main-content-box">
	<div class="row">
		<div class="col-xs-12">
		<div class="panel panel-default panel-borders" >

	        <div class="panel-body">
				 <div id="d-content">
				 	
		         </div>
                </div>
            </div>
		</div>
	</div>	
	
	{#<div class="row" id="profile-view-box" style="display: none;">
	<div class="col-xs-12" id="profile-view-box-content">
	  	
	</div>	
	</div>	#}
		
</div>    


{%endblock%}
{%block footer%}
<div id="megafooter">
	
</div>	
{%endblock%}

{%block js%}

   <script src="/js/dev/users.js"></script>
{%endblock%}
