<input type="hidden" value="{{collabid}}" id="vcollab_id">
<div class="user-profile">
    <div class="user-display">
      <div style="margin-top: 50px;"></div>
      <div class="bottom fw-grid-gallery">
        <div class="user-avatar fw-block-3">
            {#<span class="status"></span>#}

			<img class="img-responsible" id="u-ava" {%if user.user_pic=="nopic.png"%}src="/img/avatar/avatar2x200.jpg"{%else%}src="/userfiles/{{user.user_id}}/pic/{{user.user_pic}}"{%endif%}>

        </div>    
        <div class="user-info">
          <h4><a target="_blank" href="http://floctopus.com/{{user.user_username}}">{{user.user_name}} </a> </h4>
          <span>{{user.user_username}}</span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-sm-12" >
          <div class="info-block panel panel-default">
	          <div class="panel-body">
		          {%if user.user_about%}
			  	<span class="description">{{user.user_about}}</span>		          
		          {%endif%}
		          
		          <table class="table">
			          <tr>
				          <td  class="item" >{{lng_profile.occupation}}<span class="icon s7-portfolio"></span></td>
				          <td >{{proff}}</td>
			          </tr>   
			          
			          {%if user.user_bday_pub%} 
			          <tr>
				          <td class="item">{{lng_profile.birthday}}<span class="icon s7-gift"></span></td>
				          <td>{{user.user_bday|date(acs.user_dformat)}}</td>
			          </tr>   
			          {%endif%}
			          
			          {%if user.user_phone_pub%} 
                        <tr>
                          <td class="item">{{lng_profile.phone}}<span class="icon s7-phone"></span></td>
                          <td>{%if user.user_phone%}{{user.user_phone}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>
                      
                        </tr>
                        {%endif%}
                        
                        {%if user.user_addr_pub%} 
                        <tr>
                          <td class="item">{{lng_profile.location}}<span class="icon s7-map-marker"></span></td>
                          <td>
	                      	{%if user.user_street or user.user_city or user.user_state or user.user_zip%}
	                          <address>{{user.user_street}}  {{user.user_city}}  {{user.user_state}} {{user.user_zip}}</address>
	                        {%else%}
	                        	<span class="text-danger">{{lng_common.ns}}</span>
	                        {%endif%}
	                      </td>
	                      
                        </tr>
                        {%endif%}
                        
                         {%if user.user_country_pub%} 
                        <tr>
                          <td class="item">{{lng_profile.country}}<span class="icon s7-map-marker"></span></td>
                          <td>{{country}}</td>
                          
                        </tr>
                        {%endif%}
                        
                        
						{%if user.user_www_pub%} 
                        <tr>
                          <td class="item">{{lng_profile.website}}<span class="icon s7-global"></span></td>
                          <td>{%if user.user_www%}{{user.user_www}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>
                          
                        </tr>
						{%endif%}
						
						
						{%if user.user_skype_pub%} 
                        <tr>
                          <td class="item">Skype<span class="icon s7-global"></span></td>
                          <td>{%if user.user_skype%}{{user.user_skype}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>
                          
                        </tr>
                        {%endif%}
                        
                        {%if user.user_twitter_pub%} 
                        <tr>
                          <td class="item">Twitter<span class="icon s7-global"></span></td>
                          <td>{%if user.user_twitter%}{{acs.user_twitter}}{%else%}<span class="text-danger">{{lng_common.ns}}</span>{%endif%}</td>
                          
                        </tr>
						{%endif%}


		          </table>     
		          
	          </div>    
          </div>    
      </div>
    </div>      
</div>