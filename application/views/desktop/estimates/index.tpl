{% extends skin~"/common/root.tpl" %}
{%block headcss%}
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<style>
.input-group-btn .btn {
    height: 42px;
}	    

.modal-body {
    max-height: calc(100vh - 310px);
    overflow-y: auto;
}

</style>    
{%endblock%}

{%block content%}
<input type="hidden" id="_bst" value="{{lng_estimates.send_bundle_title}}">
<input type="hidden" id="_bsm" value="{{lng_estimates.send_bundle_message}}">

<input type="hidden" id="_bat" value="{{lng_estimates.arc_bundle_title}}">
<input type="hidden" id="_bam" value="{{lng_estimates.arc_bundle_message}}">

<input type="hidden" id="_buat" value="{{lng_estimates.uarc_bundle_title}}">
<input type="hidden" id="_buam" value="{{lng_estimates.uarc_bundle_message}}">


<input type="hidden" id="_bpt" value="{{lng_estimates.pay_bundle_title}}">
<input type="hidden" id="_bpm" value="{{lng_estimates.pay_bundle_message}}">

<input type="hidden" id="_pdt" value="{{lng_estimates.del_bundle_title}}">
<input type="hidden" id="_pdm" value="{{lng_estimates.del_bundle_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="_no" value="{{lng_common.btn_no}}">

<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_estimates.title}} 
		<a href="#" class="btn btn-success btn-head pull-right g-l" data-go="/estimates/add" data-toggle="tooltip" data-placement="bottom" data-original-title="{{lng_estimates.btn_add}}" > Create Estimate</a>

		
	</h3>
</div>	
<input type="hidden" id="moment-format" value="{{momentformat}}">
<input type="hidden" id="startDate" value="{{startdate}}">
<input type="hidden" id="endDate" value="{{enddate}}">
<form id="foptions">
	     {%if 0 in est_type%}
	     <input class="esttypes-val" id="ita" type="hidden" name="esttypes[]" value="0">
	     {%endif%}
	     
	     {%for s in eststatuslist%}
	     
	     	{%if s in est_type%}
	     		
	     		<input class="esttypes-val" id="it{{s}}" type="hidden" name="esttypes[]" value="{{s}}">
	     	
	     	{%endif%}
	     {%endfor%}
	     <input type="hidden" id="estarc" name="estarc" value="{{est_arc}}">
</form>	


<div class="main-content">
<div class="row">
	<div class="col-sm-10">
		
		<form id="fsearch"  class="form-horizontal group-border-dashed">
			<div class="form-group">
			<div class="col-xs-12">
					<div class="input-group">
						<span class="input-group-addon " style="background-color:#ccc; color:#fff; " ><span class="s7-search" style="font-size: 24px;"></span></span>
                      <input class="form-control" id="s" placeholder="{{lng_estimates.search_ph}}" {%if searchstr%}value="{{searchstr}}"{%endif%} type="text" style="border-width:1px;" >
                <span class="input-group-addon " id="idrp"  style="background-color:#4e91ff; color:#fff; cursor: pointer;" ><span class="s7-date" style="font-size: 24px;"></span></span>
                       
                    </div>
			</div>		
			</div>		
		</form>
		<form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<div class="panel panel-default panel-borders">

	        <div class="panel-body">
			
				<div id="d-content">
		
				</div>
	
	        </div>
		</div>
		</form>
		
	</div>
	
	<div class="col-sm-2" style="padding-top: 10px;">
		<legend class="blue" >Filter</legend>
		<div class="btn-group-vertical btn-block ">

               <button type="button" class="btn btn-default ita {%if 0 in est_type%}active{%endif%}"  data-val="0">All</button>
          
              {%for s in eststatuslist%}
             
              	<button type="button" class="btn btn-default it {%if s.eststatus_status in est_type%}active{%endif%}" data-val="{{s.eststatus_status}}" >{{s.eststatus_name}}</button>
              {%endfor%}
              
       </div>

		<div class="btn-group-vertical btn-block ">
              <button type="button" class="btn btn-default iarc {%if est_arc==0%}active{%endif%}"  data-val="0">All</button>
              <button type="button" class="btn btn-default iarc {%if est_arc==1%}active{%endif%}" data-val="1">Archived</button>
              <button type="button" class="btn btn-default iarc {%if est_arc==2%}active{%endif%}" data-val="2">Non Archived</button>
       </div>

	   <button class="btn btn-primary btn-block doApplyFilter">Apply Filter</button>
	</div>	
</div>

</div>	

     <div id="send-err-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header" style="background-color: #ef6262;">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_estimates.err}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="err-list-box">



		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" data-dismiss="modal"  class="btn btn-danger btn-md ">OK</button>
           

          </div>
         
         
         
        </div>
      </div>
     </div> 

{%endblock%}

{%block footer%}
<div id="megafooter">
	
</div>	
{%endblock%}

{%block js%}
	<script src="/assets/lib/date-time/moment.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
   <script src="/js/dev/estimates.js?v={{hashver}}"></script>
{%endblock%}
