<form id="fmybilladdr" class="form-horizontal" role="form">

	
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right">{{lng_common.name}} <span style="color: red;">*</span> </label>
	    <div class="col-sm-9">
		    <input type="text"  data-errorbox="#errname4" data-msg-required="{{lng_clients.name_req}}"  name="billing_name"   id="billing_name" required=""  value="{{acs.set_billing_name}}" class="form-control" >
			<div id="errname4" style="color:red;"></div>
		</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.country}}</label>
	 <div class="col-sm-5">
		<select class="form-control" name="billing_country" >
			{%for cl in countryList%}
			<option {%if cl.country_iso==country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
			{%endfor%}
		</select>
	</div>
	</div>


	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.state}}</label>
	<div class="col-sm-4">

		<input type="text" id="inputstate2" title="State" class="form-control"  name="billing_state"  value="{{acs.set_billing_state}}">
	
	</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right">{{lng_common.address}} </label>
	    <div class="col-sm-9">
		    <input type="text" name="billing_street" value="{{acs.set_billing_street}}" class="form-control" >
		</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.city}} </label>
	<div class="col-sm-5">

		<input type="text" name="billing_city"  value="{{acs.set_billing_city}}" class="form-control" >
	</div>
	</div>
	
	<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right">{{lng_common.zip}} </label>
	<div class="col-sm-3">
		<input type="text"  class="form-control" value="{{acs.set_billing_zip}}" name="billing_zip"  > 						
	</div>
	</div>
	
	

		
</form>
