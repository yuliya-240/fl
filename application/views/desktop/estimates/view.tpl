{% extends skin~"/common/root.tpl" %}
{%block headcss%}
<link href="/assets/lib/venobox/venobox.css" rel="stylesheet" />
<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
<style>

.photo{
	border:1px solid #ccc;
}
	
#plist{
	list-style-type: none;
}	
	
.ic-flist{
	font-size: 22px;
}	
	
.form-horizontal .form-group {
	padding: 4px 0 !important; 
}
</style>	

{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 1px;">{{invlang.estimate}} #{{est.est_num}} 
	
		<div class="btn-group  btn-hspace pull-right" >
              <button type="button" data-toggle="dropdown" class="btn btn-default btn-head dropdown-toggle dropdowndoc" aria-expanded="false"  style="border-color: #7a7a7a;">&nbsp;More Actions&nbsp;&nbsp;<span class="caret"></span></button>
              <ul role="menu" id="act-box" class="dropdown-menu">
                {%if est.est_status==1 or est.est_status==5%}<li><a href="#" class="g-l" data-go="/estimates/edit/{{est.est_id}}" >&nbsp;Edit</a></li>{%endif%}
                <li><a href="#" class="copy">&nbsp;Copy</a></li>
                {%if est.est_arc%}
                <li><a href="#" class="unarc">&nbsp;Unarchive</a></li>
                {%else%}
                <li><a href="#" class="arc">&nbsp;Archive</a></li>
                {%endif%}
                <li><a href="#" class="">&nbsp;PDF</a></li>
                <li><a href="#" data-go="/settings" class="g-l">&nbsp;Settings</a></li>
                <li class="divider"></li>
                <li><a href="#" style="color:red;" class="del">&nbsp;Delete</a></li>
              </ul>
        </div>


	<a href="#" class="btn btn-head btn-primary sendemail btn-space pull-right">Send</a>
    <a href="#" data-go="/estimates" data-toggle="tooltip" data-placement="bottom" title=""  data-original-title="{{lng_common.back_list}}" class="btn btn-head btn-space btn-dark pull-right g-l"><i class="material-icons btn-head-s">arrow_back</i></a>
		
	</h3>
	
</div>	

<input type="hidden" id="est_id" value="{{est.est_id}}">
<input type="hidden" id="_idt" value="{{lng_estimates.del_title}}">
<input type="hidden" id="_idm" value="{{lng_estimates.del_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
<input type="hidden" id="_cb" value="{{lng_common.btn_cancel}}">

<input type="hidden" id="_copy_tit" value="{{lng_estimates.copy_title}}">
<input type="hidden" id="_copy_msg" value="{{lng_estimates.copy_msg}}">	

<input type="hidden" id="_arc_tit" value="{{lng_estimates.arc_title}}">
<input type="hidden" id="_arc_msg" value="{{lng_estimates.arc_msg}}">	

<input type="hidden" id="_uarc_tit" value="{{lng_estimates.uarc_title}}">
<input type="hidden" id="_uarc_msg" value="{{lng_estimates.uarc_msg}}">	
	
<input type="hidden" id="client" value="{{postfix}}">

<div class="main-content">
  <div class="row">
	<div class="col-xs-12 col-md-9">
		{%if est.est_arc%}
		  <div role="alert" class="alert alert-info alert-icon alert-border-color ">
            <div class="icon"><span class="s7-info"></span></div>
            <div class="message">
              <strong>Archived Estimate!</strong> Do you want to unarchive it? <a href="#" class="unarc">Click Here</a>
            </div>
          </div>		
		
		{%endif%}
		
		<div class="text-center" style="margin-top: 5px; margin-bottom: 5px;">
			{%if est.est_status!=2%}
			<button type="button" class="btn btn-head btn-success btn-space doaccept">{{lng_estimates.accept}}</button>
			{%endif%}
			
			{%if est.est_status!=3%}
			<button type="button" class="btn btn-head btn-danger btn-space  dodecline">{{lng_estimates.decline}}</button>
			{%endif%}
			
			{%if est.est_status!=1 and est.est_status!=5%}
			<button type="button" class="btn btn-head btn-info btn-space  dopending">{{lng_estimates.pending}}</button>
			{%endif%}

		</div>	
		
		<div class="panel panel-default panel-borders  {%if est.est_status==1%}draft{%endif%} {%if est.est_status==5%}sent{%endif%} ">
	        <div class="panel-body">
              <div class="invoice" style="background: none !important; padding: 20px 20px;">
                <div class="row invoice-header">
                  <div class="col-xs-7">
                    <div class="invoice-logo">
	                	{%if acs.set_logo!="nologo.png"%}
	                    <img class="img-responsive" src="/userfiles/{{acs.user_id}}/pic/{{acs.set_logo}}">
	                    {%endif%}
	                </div>
                  </div>
                  <div class="col-xs-5 invoice-order"><span class="invoice-id">{{invlang.estimate}} #{{est.est_num}}</span><span class="incoice-date">{{est.est_date|date(acs.user_dformat)}}</span></div>
                </div>
                <div class="row invoice-data">
                  <div class="col-sm-5 invoice-person" id="inv-my-addr">
	                  
	                  <span class="name">{{acs.set_billing_name}} </span>
	                  <span>{{acs.set_billing_street}}</span>
	                  <span>{{acs.set_billing_city}}{%if acs.set_billing_state%}, {{acs.set_billing_state}}{%endif%} {{acs.set_billing_zip}}</span>
	                  <span>{{mycountry}}</span>
	                  <a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chMyAddr" >Edit Address</a>
	                
	              </div>
                  <div class="col-sm-2 invoice-payment-direction"><i class="icon mdi mdi-chevron-right"></i></div>
                  <div class="col-sm-5 invoice-person" id="recipient-addr">
	                  
	                  <span class="name"> {{est.est_name}} </span>
	                  <span>{{est.est_street}}</span>
	                  <span>{{est.est_city}}{%if est.est_state%}, {{est.est_state}}{%endif%} {{est.est_zip}}</span>
	                  <span>{{country}}</span>
	                  <a href="#"  class="btn btn-sm " style="padding-right: 0;" id="chAddr" >Edit Address</a>
	                
	              </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <table class="invoice-details">
                      <tr>
                        <th style="width:60%">{{invlang.desc}}</th>
                        <th style="width:15%" class="hours">{{invlang.rate}}</th>
                        <th style="width:10%" class="hours">{{invlang.qty}}</th>
                        <th style="width:15%" class="amount">{{invlang.am}}</th>
                      </tr>
                      {%for l in lines%}
                      <tr>
	                        <td class="description">{{l.estline_srv}}
		                        <br>{{l.estline_desc}}
	                        </td>
	                        <td class="hours">{{l.estline_rate}}</td>
	                        <td class="hours">{{l.estline_qty}}</td>
	                        <td class="amount">{{l.estline_total}}</td>
                      </tr>
                      {%endfor%}
                      <tr>
                        <td></td>
                     
                        <td colspan="2" class="summary">{{invlang.subtotal}} <small style="font-size: smaller">{{est.est_currency}}</small>
	                        
	                        
                        </td>
                        <td class="amount">{{est.est_subtotal}}</td>
                      </tr>
					   {%if est.est_taxes_cash>0%}
                       <tr>
                        <td></td>
                     
                        <td colspan="2" class="summary">{{invlang.taxes}}  <small style="font-size: smaller">{{est.est_currency}}</small></td>
                        <td class="amount">{{est.est_taxes_cash}}</td>
                      </tr>
					  {%endif%}
                      {%if est.est_discount_percentage>0%}
                      
                      <tr>
                        <td></td>
                    
                        <td colspan="2" class="summary">{{invlang.discount}} ({{est.est_discount_percentage}}%)</td>
                        <td class="amount">{{est.est_discount_cash}}</td>
                      </tr>
                      {%endif%}
                      
                      <tr>
                        <td></td>
          
                        <td colspan="2" class="summary total" >
	                        {{invlang.total}} <small style="font-size: smaller">{{est.est_currency}}</small>
	                    </td>

                        <td class="amount total-value" style="color:#000;">{{est.est_total}}</td>
                      </tr>
                        
                        <td></td>
          
                        <td colspan="2" class="summary total">{{invlang.due}} <small style="font-size: smaller">{{est.est_currency}}</small></td>
                        <td class="amount text-danger">{{est.est_total_due}}</td>
                      </tr>
                      
                    </table>
                  </div>
                </div>
				
				{%if est.est_terms %}
                <div class="row" style="margin-top: 20px; ">
                  <div class="col-md-12 invoice-message" style="margin-bottom: 20px;"><span class="title">{{invlang.terms}}</span>
                    <p>{{est.est_terms}}</p>
                  </div>
                </div>
				{%endif%}
				
				{%if est.est_notes%}
                <div class="row" >
                  <div class="col-md-12 invoice-message" style="margin-bottom: 20px;"><span class="title">{{invlang.notes}}</span>
                    <p>{{est.est_notes}}</p>
                  </div>
                </div>
				{%endif%}
{#
                <div class="row invoice-company-info">
                  {%if acs.set_logo!="nologo.png"%}
                  <div class="col-sm-2 logo">
	                	
	                    <img class="img-responsive" src="/userfiles/{{acs.user_id}}/pic/{{acs.set_logo}}">
	                    
	              </div>
	              {%endif%}
                  <div class="col-sm-4 summary"><span class="title">Amaretti Company</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                  </div>
                  <div class="col-sm-3 phone">
                    <ul class="list-unstyled">
                      <li>+1(535)-8999278</li>
                      <li>+1(656)-3558302</li>
                    </ul>
                  </div>
                  <div class="col-sm-3 email">
                    <ul class="list-unstyled">
                      <li>amaretti@company.co</li>
                      <li>amaretti@support.co</li>
                    </ul>
                  </div>
                </div>
         #}
              </div>
	        </div>
		</div>   
		
		{%if flist%}
		<div class="panel panel-default panel-borders ">
<div class="panel-heading">Attached Files</div>
	        <div class="panel-body">	
	        
		        
			<ul id="plist">
				{%for f in flist%}
					<li style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px #ccc dotted;" id="attitem{{f.invatt_id}}">
						<div class="pull-left" style="margin-right: 5px; padding-top: 5px;">{%if f.invatt_type=="image"%}<span class="s7-photo ic-flist"></span>{%else%} <span class="s7-file ic-flist"></span>{%endif%}</div> 
						
						<div class="pull-left" style="padding-top: 7px;">	<a href="{{ihost}}userfiles/{{acs.user_id}}/pic/{{f.invatt_fname}}" {%if f.invatt_type=="image"%}class="venobox"{%else%}target="_blank"{%endif%} >{{f.invatt_fname}}</a></div>
						
						
						<div style="clear: both;"></div>

					</li>

				
				{%endfor%}
			</ul>				
	        
	        </div>
		</div>  
		{%endif%}      	
		   		
	</div>	  
	<div class="col-xs-12 col-md-3">
		<legend style="padding-bottom: 10px;">Account Standing</legend>

		<div class="widget widget-tile" style="margin-bottom: 5px;">
          <div class="data-info">
            <div data-toggle="counter" data-end="156" class="value">{{outstanding|number_format(2,acs.user_dsep,acs.user_tsep)}}</div>
            <div class="desc">Outstanding <span class="text-muted">{{acs.user_currency}}</span></div>
          </div>
          <div class="icon "><span class="s7-clock"></span></div>
        </div>	
 
 		<div class="widget widget-tile" style="margin-bottom: 5px;">
          <div class="data-info">
            <div data-toggle="counter" data-end="156" class="value">{{pastdue|number_format(2,acs.user_dsep,acs.user_tsep)}}</div>
            <div class="desc">Past Due <span class="text-muted">{{acs.user_currency}}</span></div>
          </div>
          <div class="icon "><span class="s7-attention text-danger"></span></div>
        </div>	
  
  		<div class="widget widget-tile">
          <div class="data-info">
            <div data-toggle="counter" data-end="156" class="value">{{paid|number_format(2,acs.user_dsep,acs.user_tsep)}}</div>
            <div class="desc">Paid <span class="text-muted">{{acs.user_currency}}</span></div>
          </div>
          <div class="icon "><span class="s7-piggy text-success"></span></div>
        </div>	
		<legend style="padding-bottom: 10px;">Invoice History</legend>
 		<div class="panel panel-default panel-borders">

	        <div class="panel-body">
		    
			        {%for h in history%}
			        <div class="history-box">
						
				        <small class="text-muted"><time datetime="{{h.esthist_date|date('Y-m-d')}}T{{h.esthist_date|date('H:i:s')}}" class="timeago"></time></small>
				        <br><span {%if h.esthist_action=="pay"%}class="text-success"{%endif%} style="font-weight: 600; font-size: 16px;">{{h.estact_desc}}</span>
				        {%if h.esthist_action=="pay"%}
				        <br><small><span class="text-muted">Amount</span> {{h.esthist_amount|number_format(2,acs.user_dsep,acs.user_tsep)}} {{h.esthist_currency}}</small>
				        {%endif%}
				        
				        {%if h.esthist_action=="email"%}
				        <br><small><span class="text-muted">to</span> {{h.esthist_email}} </small>
				        {%if h.esthist_cc%}
				         <br><small><span class="text-muted">cc</span> {{h.esthist_cc}} </small>
				        {%endif%}
				        {%endif%}
				        
			        </div>	
			        {%endfor%} 
		          
	        </div>
 		</div>    
    </div>
	
	
  </div>
</div>



     <div id="send-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">Email Invoice</h3>
          </div>
           
         <div class="modal-body">
	         
				<form id="femail" class="form-horizontal">
					
		 		<input type="hidden" name="email_est_id" value="{{est.est_id}}">
			 		
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">Email </label>
				    <div class="col-sm-8">
					    <input type="email" name="send_email" value="{{sendemail}}" required=""  class="form-control" data-errorbox="#erremail" data-msg-required="{{lng_estimates.email_req}}" data-msg-email="{{lng_common.nve}}" >
					    <div id="erremail" style="color:red;"></div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">CC:Email  </label>
				    <div class="col-sm-8">
					    <input type="email" name="send_email_cc" value="{{sendemailcc}}" class="form-control" data-errorbox="#erremailcc"  data-msg-email="{{lng_common.nve}}">
					    <div id="erremailcc" style="color:red;"></div>
					</div>
				</div>
		 		</form>	
	               
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" id="doSend"   class="btn btn-primary  btn-md ">Send</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>
          </div>
         
         
         
        </div>
      </div>
     </div>  

     <div id="addr-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_estimates.chaddr}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="bill-adr-box">
		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button"   class="btn btn-primary doChAddr btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
        </div>
      </div>
     </div>  

     <div id="my-addr-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_estimates.chaddr}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="my-bill-adr-box">



		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button"   class="btn btn-primary doChMyAddr btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left" data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  


{%endblock%}


{%block js%}
<script src="/assets/lib/mask/jquery.mask.js"></script>	
<script src="/assets/lib/livequery/jquery.livequery.js"></script>
<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>
<script src="/assets/lib/venobox/venobox.min.js"></script>	
<script src="/js/dev/estimates_view.js?v={{hashver}}"></script>
 
{%endblock%}

