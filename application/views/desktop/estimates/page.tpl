{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs"></th>
			<th>#</th>
			<th>Date</th>
			<th>Recipient</th>
			<th>Status</th>
			<th style="text-align: right;">Total</th>
		
			<th width="1%"></th>
		</tr>
	</thead>
	<tbody>
		{%for r in records%}
		<tr>
			<td class="text-center hidden-xs" style="vertical-align:middle;">
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.est_id}}" class="check" name="row[]" value="{{r.est_id}}">
	              <label for="chk{{r.est_id}}"></label>
	            </div>
			</td>
			<td class="tbl-pointer g-l" data-id="{{r.est_id}}" data-go="/estimates/view/{{r.est_id}}">{{r.est_num}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.est_id}}" data-go="/estimates/view/{{r.est_id}}">{{r.est_date|date(acs.user_dformat)}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.est_id}}" data-go="/estimates/view/{{r.est_id}}" style="font-weight: 600">{{r.est_name}}</td>
			<td class="tbl-pointer g-l" data-id="{{r.est_id}}" data-go="/estimates/view/{{r.est_id}}">
				{%if r.est_arc%}
					<span class="badge">{{lng_common.arc}}</span>
				{%endif%}
				{%if r.est_status==1%}
				<span class="badge">{{r.eststatus_name}}</span>
				{%endif%}
				{%if r.est_status==2%}
				<span class="badge badge-success">{{r.eststatus_name}}</span>
				{%endif%}

				{%if r.est_status==3%}
				<span class="badge badge-warning">{{r.eststatus_name}}</span>
				{%endif%}

				{%if r.est_status==4%}
				<span class="badge badge-danger">{{r.eststatus_name}}</span>
				{%endif%}

				{%if r.est_status==5%}
				<span class="badge badge-primary">{{r.eststatus_name}}</span>
				{%endif%}

			</td>
			<td class="tbl-pointer g-l" data-id="{{r.est_id}}" data-go="/estimates/view/{{r.est_id}}" style="text-align: right;"><span style="font-weight: 600">{{r.est_total|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{r.est_currency}}</small>
				{%if r.est_currency!=acs.user_currency%}
				<br><small>{{r.est_total_base|number_format(2,acs.user_dsep,acs.user_tsep)}}</small> <small>{{acs.user_currency}}</small>
				{%endif%}
			</td>
			<td class="tbl-pointer" data-id="{{r.est_id}}">
				<a href="#" class="g-l" data-go="/estimates/view/{{r.est_id}}" data-id="{{r.est_id}}"><i class="material-icons">visibility</i></a>
			</td>
		</tr>
		{%endfor%}
	</tbody>
	<tfoot>
		
		
		<tr style="background-color: #fff7eb;">
			<td colspan="5"  >Total</td>
			<td  style="text-align: right; "><span style="font-weight: 600; ">{{totalPage|number_format(2,acs.user_dsep,acs.user_tsep)}}</span> <small>{{acs.user_currency}}</small></td>
			<td style="background-color: #fff7eb;"></td>
		</tr>	
	
	</tfoot>	
</table>	

			
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
