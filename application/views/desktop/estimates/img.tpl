<li style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px #ccc dotted;" id="attitem{{id}}">
	<div class="pull-left" style="margin-right: 5px; padding-top: 5px;">{%if ftype=="image"%}<span class="s7-photo ic-flist"></span>{%else%} <span class="s7-file ic-flist"></span>{%endif%}</div> 
	
	<div class="pull-left" style="padding-top: 7px;">	<a href="{{ihost}}userfiles/{{uid}}/pic/{{filename}}" {%if ftype=="image"%}class="venobox"{%else%}target="_blank"{%endif%} >{{filename}}</a></div>
	
	<div class="pull-right">
		<button class="btn btn-sm btn-space btn-default text-danger delatt" style="border: none !important;" type="button" data-id="{{id}}"  ><span class="s7-trash" style="font-size:22px; font-weight: 700;"></span></button>
	</div>	
	
	<div style="clear: both;"></div>
	<input type="hidden" name="attfile[]" id="attfile{{id}}" value="{{filename}}">
	<input type="hidden" name="attfiletype[]" id="attfiletype{{id}}" value="{{ftype}}">
</li>

