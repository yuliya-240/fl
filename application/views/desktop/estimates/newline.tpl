<tr id="line{{id}}">
	<td class="description td-a-t" >
		<input type="text" name="estrow_srv[]" placeholder="{{lng_estimates.ph_srv}}" id="name{{id}}" data-id="{{id}}" data-errorbox="#errsrvline{{id}}" data-msg-required="{{lng_estimates.srv_req}}"  class="form-control td-b linefilter" >
		
		<textarea id="irdesc{{id}}" style="resize: none;" name="estrow_desc[]" rows="2" placeholder="{{lng_estimates.ph_desc}}" class="form-control td-b td-t-n-b"></textarea>
		<div id="errsrvline{{id}}" style="color:red;"></div>
	</td>
	<td class="hours td-a-t" style="text-align: center;">
		<input type="text" id="irrate{{id}}" data-id="{{id}}" name="estrow_rate[]" class="form-control td-b td-l-n-b dec" placeholder="0.00">
		
	</td>
	<td class="hours td-a-t" >
		<input type="text" value="1" id="irqty{{id}}" data-id="{{id}}" name="estrow_qty[]" class="form-control td-b td-l-n-b td-r-n-b dec" >
		
	</td>

	<td class="hours td-a-t" >
		<input type="text" value="0.00" id="irtax1{{id}}" data-id="{{id}}" name="estrow_tax1[]" class="form-control td-b  tax" >
		<input type="text" value="0.00" id="irtax2{{id}}" data-id="{{id}}" name="estrow_tax2[]" class="form-control td-b td-t-n-b tax" >
		<input type="hidden" id="taxcash1{{id}}" value="0" class="tax1" data-id="{{id}}" name="estrow_tax1_cash[]"> 
		<input type="hidden" id="taxcash2{{id}}" value="0" class="tax2" data-id="{{id}}" name="estrow_tax2_cash[]"> 
	</td>

	<td class="amount td-a-t" >
		<input type="text" value="0.00" name="estrow_total[]" id="irtotal{{id}}" data-id="{{id}}" class="form-control td-b td-l-n-b i-txt-r linetotal pull-left" readonly=""  >
		<button class="btn btn-space btn-default text-danger delline" type="button" data-id="{{id}}" style="padding-top: 7px !important; border: none !important; position: absolute; background: none !important;  padding-left: 6px;" ><span class="s7-close-circle" style="font-size:28px; font-weight: 700;"></span></button>

	</td>
	{#<td class="td-a-t">
		<button class="btn btn-space btn-default text-danger delline" type="button" data-id="{{id}}" style="padding-bottom: 7px; border: none !important;" ><span class="s7-close-circle" style="font-size:24px;"></span></button>
		
	</td>#}
	</tr>
