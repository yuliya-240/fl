{% extends skin~"/common/root.tpl" %}
{%block preheadcss%}
     <link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.min.css"/>
    {%endblock%}
{%block headcss%}
<link rel="stylesheet" href="/css/inv.css" type="text/css"/>
<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
<link href="/assets/lib/switch/css/bootstrap-switch.min.css" rel="stylesheet" />
<link href="/assets/lib/venobox/venobox.css" rel="stylesheet" />

<style>
	

.photo{
	border:1px solid #ccc;
	}
	
#plist{
	list-style-type: none;
	}	
	
.ic-flist{
	
	font-size: 22px;
	
}	
	
.form-horizontal .form-group {
	padding: 4px 0 !important; 
	}
</style>	
{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_estimates.title_new}} </h3>
</div>	

<div class="main-content">
  <div class="row">
	  <div class="col-xs-12 col-md-10 col-md-offset-1">
		
		<input type="hidden" id="btnYes" value="{{lng_common.btn_yes}}">
		<input type="hidden" id="btnNo" value="{{lng_common.btn_no}}">
		<input type="hidden" id="_tit" value="{{lng_estimates.errtit}}">
		<input type="hidden" id="_txt" value="{{lng_estimates.errtxt}}">
		<input type="hidden" id="_ctit" value="{{lng_estimates.cerrtit}}">
		<input type="hidden" id="_ctxt" value="{{lng_estimates.cerrtxt}}">
		<input type="hidden" id="_ltit" value="{{lng_estimates.lerrtit}}">
		<input type="hidden" id="_ltxt" value="{{lng_estimates.lerrtxt}}">
		
		<form id="festimate" class="form-horizontal">
			<input type="hidden" name="est_id" id="est_id" value="{{est.est_id}}">			
		<div class="row form-horizontal" style="margin-bottom: 5px;">
			
			<div class="col-xs-12 col-sm-4">
				 <select class="form-control" id="est_lang" name="est_lang" >
				
					 {%for l in langs%}
					 
					 <option {%if l.lang_iso==est.est_lang%}selected{%endif%} value="{{l.lang_iso}}">{{l.lang_name}}</option>
					 {%endfor%}
				</select>
				
			</div>	
			
			<div class="col-xs-12 col-sm-4">
				 <select class="form-control" id="est_currency" name="est_currency" >
					 
					 {%for c in currencies%}
					 <option {%if c.currency_iso==est.est_currency%}selected{%endif%} value="{{c.currency_iso}}"> {{c.currency_iso}} {{c.currency_name}}</option>
					 {%endfor%}
					 </select>
					<div class="input-group xs-mb-15" id="rate-box" style="margin-top: 5px; margin-bottom:0 !important; {%if est.est_currency==acs.user_currency%} display: none;{%endif%}" >
					<span class="input-group-addon" id="currency-label">{{est.est_currency}}/{{acs.user_currency}}</span>
                    <input type="text" id="est_exchange_rate" autocomplete="off" name="est_exchange_rate" class="form-control xr" required="" data-errorbox="#errrate" data-msg-required="{{lng_common.fr}}" value="{{est.est_exchange_rate}}">
                </div>
				<div id="errrate" style="color:red;"></div>


			</div>
			<div class="col-xs-12 col-sm-4" align="right">
				 <select class="form-control" id="est_prj" name="est_prj" >
					 <option value="0">Not assigned to Project</option>
					 {%for p in prj%}
					 
					 <option {%if p.project_id==est.est_prj%}selected{%endif%} value="{{p.project_id}}">{{p.project_name}}</option>
					 {%endfor%}

					 </select>

			</div>	
		</div>

		<input type="hidden" id="est_name" name="est_name" value="{{est.est_name}}" >
		<input type="hidden" id="est_street" name="est_street" value="{{est.est_street}}">
		<input type="hidden" id="est_city" name="est_city" value="{{est.est_city}}">
		<input type="hidden" id="est_state" name="est_state" value="{{est.est_state}}">
		<input type="hidden" id="est_zip" name="est_zip" value="{{est.est_zip}}">
		<input type="hidden" id="est_country" name="est_country" value="{{est.est_country}}">
		

			
		<div class="panel panel-default panel-borders">
	        <div class="panel-body">

			<div class="row">
						
				<div class="col-sm-6">
					<div class="form-horizontal">
				    	<div class="form-group">
				    		<div class="col-sm-10" style="padding-right: 4px;">
		
						    	<select id="client" name="client" class="select2">
							    	
						    		<option value="0">{{lng_estimates.selto}}</option>
						    		{%if clients%}
						    		<optgroup label="{{lng_menu.clients}}">
						    		{%for c in clients%}
						    			<option {%if est.est_client_id > 0 and est.est_client_id == c.client_id%}selected{%endif%} value="{{c.client_id}}@cli">{{c.client_name}}</option>
						    		{%endfor%}
						    		</optgroup>
						    		{%endif%}
						    		{%if collabs%}
						    		<optgroup label="{{lng_menu.colleague}}">
						    		{%for c in collabs%}
						    			<option {%if est.est_collab_id > 0 and est.est_collab_id == c.cb_id%}selected{%endif%} value="{{c.cb_id}}@usr">{{c.cb_billing_name}}</option>
						    		{%endfor%}
						    		</optgroup>
						    		{%endif%}
						    		
						    	</select>
					    	
					    	
				    		</div>
				    		<div class="hidden-xs col-sm-1" style="padding-left: 0;">
					    		
					    		<button class="btn btn-sm btn-success" id="a-c" style="padding: 6px 9px;"><i class="material-icons">person_add</i></button>
				    		</div>
					    </div>
					</div>
						
					    <address id="address" style="margin-top:20px; font-size: 16px; ">
					    
					    <strong>{{est.est_name}} </strong><br>
					    {{est.est_street}} <br>
					    {{est.est_city}}{%if est.est_state%}, {{est.est_state}}{%endif%} {{est.est_zip}}<br>
					    {{est.est_country}}<br>
					    <a href="#"  class="btn btn-sm " style="padding-left: 0;" id="chAddr" >Edit Address</a>
					  
					    </address>
				
				</div>
				
				<div class="col-sm-6">
					<div class="form-horizontal">
					
						<div class="form-group">
							<label class="col-sm-6 control-label no-padding-right invlang_estimate" >{{invlang.estimate}}</label>
							<div class="col-sm-6">
								<div class="input-group">
									<span class="input-group-addon">#</span>
									<input type="text" class="form-control inum" maxlength="7" required="" data-errorbox="#errnum" data-msg-required="{{lng_estimates.num_req}}" name="est_num"  value="{{est.est_num}}" >
								</div>							
		
								<div id="errnum" style="color:red;"></div>
							</div>
						</div>
		
						<div class="form-group">
							<label class="col-sm-6 control-label no-padding-right invlang_di" >{{invlang.di}}</label>
							
								<div class="col-sm-6" >
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
										<input type="text" data-date-format="{{dpformat}}" readonly="" value="{{est.est_date|date(acs.user_dformat)}}" name="date" class="form-control date-picker">
										
									</div>							
								</div>
						</div>
		
				    </div> 
					
			   </div>
			</div>
			
			<div class="row" style="margin-top:40px; margin-bottom: 20px;">		
				<div class="col-xs-12">
				<table class="invoice-details" id="inv-lines">
					<tbody>
						<tr>
							<th style="width:60%" class="invlang_desc">{{invlang.desc}}</th>
							<th class="hours invlang_rate" style="width:10%; text-align: center;">{{invlang.rate}}</th>
							<th class="hours invlang_qty" style="width:9%; text-align: center;">{{invlang.qty}}</th>
							<th class="hours invlang_tax" style="width:10%; text-align: center;">{{invlang.tax}} %</th>
							<th class="amount invlang_am" style="width:10%">{{invlang.am}}</th>		
							{#<th style="width:1%"></th>	#}				
						</tr>	
						
						{%for l in lines%}
						<tr id="line{{id}}">
							<td class="description td-a-t" >
								<input type="text" name="estrow_srv[]" placeholder="{{lng_estimates.ph_srv}}" id="name{{l.est_id}}" data-id="{{l.est_id}}" data-errorbox="#errsrvline{{l.est_id}}" data-msg-required="{{lng_estimates.srv_req}}"  class="form-control td-b linefilter" value="{{l.estline_srv}}" >
								
								<textarea id="irdesc{{l.est_id}}" style="resize: none;" name="estrow_desc[]" rows="2" placeholder="{{lng_estimates.ph_desc}}" class="form-control td-b td-t-n-b">{{l.estline_desc}}</textarea>
								<div id="errsrvline{{l.est_id}}" style="color:red;"></div>
							</td>
							<td class="hours td-a-t" style="text-align: center;">
								<input type="text" id="irrate{{l.est_id}}" data-id="{{l.est_id}}" name="estrow_rate[]" class="form-control td-b td-l-n-b dec" placeholder="0.00" value="{{l.estline_rate}}">
								
							</td>
							<td class="hours td-a-t" >
								<input type="text"  id="irqty{{l.est_id}}" data-id="{{l.est_id}}" name="estrow_qty[]" class="form-control td-b td-l-n-b td-r-n-b dec"  value="{{l.estline_qty}}">
								
							</td>
						
							<td class="hours td-a-t" >
								<input type="text" id="irtax1{{l.est_id}}" data-id="{{l.est_id}}" name="estrow_tax1[]" class="form-control td-b  tax"  value="{{l.estline_tax1_percent}}">
								<input type="text"  id="irtax2{{l.est_id}}" data-id="{{l.est_id}}" name="estrow_tax2[]" class="form-control td-b td-t-n-b tax" value="{{l.estline_tax2_percent}}" >
								<input type="hidden" id="taxcash1{{l.est_id}}"  class="tax1" data-id="{{l.est_id}}" name="estrow_tax1_cash[]" value="{{l.estline_tax1_cash}}"> 
								<input type="hidden" id="taxcash2{{l.est_id}}"  class="tax2" data-id="{{l.est_id}}" name="estrow_tax2_cash[]" value="{{l.estline_tax2_cash}}"> 
							</td>
						
							<td class="amount td-a-t" >
								<input type="text" value="{{l.estline_total}}" name="estrow_total[]" id="irtotal{{l.est_id}}" data-id="{{l.est_id}}" class="form-control td-b td-l-n-b i-txt-r linetotal pull-left" readonly=""  >
								<button class="btn btn-space btn-default text-danger delline" type="button" data-id="{{l.est_id}}" style="padding-top: 7px !important; border: none !important; position: absolute; background: none !important;  padding-left: 6px;" ><span class="s7-close-circle" style="font-size:28px; font-weight: 700;"></span></button>
						
							</td>
								
					
						</tr>
						
						{%endfor%}
					</tbody>	
				</table>	
				</div>	
			</div>		
		
			<div class="row">
				<div class="col-xs-12 text-center m-t-15">
				<button type="button" data-mob="0" class="btn btn-sm btn-success addline"><i class="ace-icon fa fa-plus"></i> {{lng_estimates.add_line}}</button>
				</div>
			</div>
						        
			<br>     
			<div class="row">
				<div class="col-sm-7"></div>	

				<div class="col-sm-5">
					<table class="table">
					<tr>
						<td width="60%"><span class="invlang_subtotal">{{invlang.subtotal}}</span>  <span class="sel-curr">{{est.est_currency}}</span></td>
						<td width="40%" align="right" id="subtotal-txt" >{{est.est_subtotal}}</td>
						<input type="hidden" name="subtotal" id="subtotal" value="{{est.est_subtotal}}">
						<input type="hidden" name="discount_cash" id="disc-cash" value="{{est.est_discount_cash}}">
					</tr>	
					<tr>
						<td><span class="invlang_taxes">{{invlang.taxes}}</span> <span class="sel-curr">{{est.est_currency}}</span></td>
						<td align="right" id="totaltax-txt">{{est.est_taxes_cash}}</td>
						<input type="hidden" name="tataltax_cash" id="totaltax" value="{{est.est_taxes_cash}}">
					</tr>	
					<tr>
						<td class="invlang_discount">{{invlang.discount}} </td>
						<td align="right" style="padding-right: 0;">
							<div class="input-group input-group-sm">
								<span class="input-group-addon">%</span>
								<input style="text-align: right;" class="form-control pricedis dis" id="discount" value="{{est.est_discount_percentage}}" name="discount"  type="text" >
							</div>
						</td>
					</tr>	

					
					<tr class="" style="background:#e6f9ff; font-weight: 600;">
						<td style="font-weight: 600;">
							<span class="invlang_total">{{invlang.total}}</span> <span class="sel-curr">{{est.est_currency}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted">{{lng_estimates.total}} {{acs.user_currency}}</span>
							</div>	
							
						</td>
						<td style="font-weight: 600;" align="right" >
							<span id="total-txt">{{est.est_total}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted" id="total-txt-base">{{est.est_total_base}}</span>
							</div>	
	
						</td>
						<input type="hidden" name="total" id="total" value="{{est.est_total}}">
						<input type="hidden" name="total_base" id="total_base" value="{{est.est_total_base}}">
					</tr>	

	
					<tr class="" style="background: #ffeaea;">
						<td style="font-weight: 600;">
							<span class="invlang_due">{{invlang.due}}</span> <span class="sel-curr">{{est.est_currency}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted">{{lng_estimates.due}} {{acs.user_currency}}</span>
							</div>	
						
						</td>
						<td style="font-weight: 600;" align="right"  style="font-weight: 600;">
							<span id="due-txt">{{est.est_total_due}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span id="due-txt-base" class="text-muted" id="">{{est.est_total_due_base}}</span>
							</div>	
						
						</td>
						<input type="hidden" name="due" id="due" value="{{est.est_total_due}}">
						<input type="hidden" name="due_base" id="due_base" value="{{est.est_total_due_base}}">
					</tr>	

					</table>	
				</div>	

			</div>	
			
			<div class="row">
				<div class="col-sm-6">
				<label class="invlang_terms">{{invlang.terms}}</label>
				<textarea class="form-control" style="resize: none;" name="est_terms">{{est.est_terms}}</textarea>	
				</div>	
				<div class="col-sm-6">
				<label class="invlang_notes">{{invlang.notes}}</label>
				<textarea class="form-control" style="resize: none;" name="est_notes">{{est.est_notes}}</textarea>	

				</div>	
			</div>	
			
			  
	        </div>
		</div> 

		<div class="panel panel-default panel-borders">

			<div class="panel-heading">{{lng_estimates.attf}}</div>
	        <div class="panel-body">
		        
			<ul id="plist">
				{%for f in flist%}
					<li style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px #ccc dotted;" id="attitem{{f.estatt_id}}">
						<div class="pull-left" style="margin-right: 5px; padding-top: 5px;">{%if f.estatt_type=="image"%}<span class="s7-photo ic-flist"></span>{%else%} <span class="s7-file ic-flist"></span>{%endif%}</div> 
						
						<div class="pull-left" style="padding-top: 7px;">	<a href="{{ihost}}userfiles/{{acs.user_id}}/pic/{{f.estatt_fname}}" {%if f.estatt_type=="image"%}class="venobox"{%else%}target="_blank"{%endif%} >{{f.estatt_fname}}</a></div>
						
						<div class="pull-right">
							<button class="btn btn-sm btn-space btn-default text-danger delatt" style="border: none !important;" type="button" data-id="{{f.estatt_id}}"  ><span class="s7-trash" style="font-size:22px; font-weight: 700;"></span></button>
						</div>	
						
						<div style="clear: both;"></div>
						<input type="hidden" name="attfile[]" id="attfile{{f.estatt_id}}" value="{{f.estatt_fname}}">
						<input type="hidden" name="attfiletype[]" id="attfiletype{{f.estatt_id}}" value="{{f.estatt_type}}">
					</li>

				
				{%endfor%}
			</ul>				
		        
			<div class="progress  progress-striped active" id="progress1" style="display:none;">
				<div id="progressSlide" style="width: 0%;" class="progress-bar progress-bar-success">{{lng_estimates.loader}}</div>
			</div>
				
			<div id="containerpic" class="text-center">
				
				<button id="pickfiles" class="btn btn-md btn-success"  ><i class="fa fa-cloud-upload" aria-hidden="true"></i>&nbsp; {{lng_estimates.attach}}</button>
			
			</div>
			        
		        
	    </div>
		</div>
		</form>    
		<button type="button" id="doSaveDraft" class="btn btn-alt4 btn-lg pull-right" >{{lng_common.btn_draft}}</button>
		<button type="button" id="doSend" class="btn btn-primary btn-lg pull-right" style="margin-right: 5px;">{{lng_common.btn_send}}</button>
		<button type="button" id="doPay" class="btn btn-success btn-lg pull-right" style="margin-right: 5px;">{{lng_common.btn_pay}}</button>
		<a href="#" class="btn btn-default btn-lg pull-left g-l" data-go="/estimates">{{lng_common.btn_cancel}}</a>
		<div style="clear:both;margin-bottom: 40px;"></div>	
		
	   
	  </div>	  
  </div>	         
</div>

     <div id="share-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_estimates.new_customer}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12">

				  <form id="fnewcustomerpopup" class="form-horizontal" role="form">
						
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">{{lng_clients.tbl_name}}<span style="color: red;">*</span></label>
	                  <div class="col-sm-9">
	                    <input class="form-control" name="client_name" data-errorbox="#errname" data-msg-required="{{lng_clients.name_req}}"  required="" placeholder="{{lng_projects.ph_name}}" type="text">
	                    <div id="errname" style="color:red;"></div>
	                  </div>
	                </div>
	
					
						<input type="hidden" value="1" name="status">
						<input type="hidden"  name="discount" id="discount" value="0"> 
						<input type="hidden"  name="notes" value="">

					<div class="form-group" >
					<label class="col-sm-3 control-label no-padding-right ">Email</label>
					<div class="col-sm-6">
						<input type="email"  name="client_email" data-errorbox="#errname-email" data-msg-email="{{lng_common.nve}}" class="form-control"> 									<div id="errname-email" style="color:red;"></div>
					</div>
					</div>
						

					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.country}}</label>
					 <div class="col-sm-5">
						<select id="country" class="form-control" name="client_country" >
							{%for cl in countryList%}
							<option {%if cl.country_iso==acs.user_country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
							{%endfor%}
						</select>
					</div>
					</div>
					
					
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.state}}</label>
					<div class="col-sm-5">
					 {%if  statesList%}
						<select title="State"  id="region"  name="client_state" class="form-control">
							{%for sl in statesList%}
							<option {%if sl.state_code == acs.co_state%}selected{%endif%} value="{{sl.state_code}}">{{sl.state_name}}</option>
							{%endfor%}
						</select>
						<input type="text" style="display:none;" id="inputstate" title="State" class="form-control"  name=""  />
					{%else%}
						<input type="text" id="inputstate" title="State" class="form-control"  name="client_state"  value="{{acs.co_state}}">
						<select title="State"  style="display:none;"  id="region"  name="client_state" class="form-control">
						</select>
					{%endif%}	
					</div>
					</div>
				
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{lng_common.address}} </label>
					    <div class="col-sm-9">
						    <input type="text" name="client_street"  class="form-control" >
						</div>
					</div>

					
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.city}} </label>
					<div class="col-sm-5">
	
						<input type="text" name="client_city"   class="form-control" >
					</div>
					</div>
						
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.zip}} </label>
					<div class="col-sm-3">
						<input type="text"  class="form-control" name="client_zip"  > 						
					</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right ">{{lng_common.phone}}</label>
					<div class="col-sm-4">
	
						<input type="text"  name="client_phone" class="form-control " > 
					</div>
					</div>

						
		
		</form>


		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" id="send-invite"   class="btn btn-primary doAddClient btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  

     <div id="addr-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_estimates.chaddr}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="bill-adr-box">



		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button"   class="btn btn-primary doChAddr btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  
	 {%for r in rates%}
	 <input type="hidden" id="rate{{r.curr}}" value="{{r.val}}">
	 {%endfor%}

{%endblock%}


{%block js%}

<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>	
<script src="/assets/lib/livequery/jquery.livequery.js"></script>
<script src="/assets/lib/typeahead/bootstrap3-typeahead.min.js?v={{hashver}}"></script>
<script src="/assets/lib/mask/jquery.mask.js"></script>	
<script src="/assets/lib/switch/js/bootstrap-switch.min.js"></script>
{# <script src="/assets/lib/jquery.magnific-popup/jquery.magnific-popup.min.js" type="text/javascript"></script>#}
{# <script src="/assets/js/app-page-gallery.js" type="text/javascript"></script>#}
<script src="/assets/lib/plupload-2.3.1/js/plupload.full.min.js"></script>	
    <script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/lib/venobox/venobox.min.js"></script>	
<script src="/js/dev/estimates_edit.js?v={{hashver}}"></script>
 
{%endblock%}
