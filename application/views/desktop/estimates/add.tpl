{% extends skin~"/common/root.tpl" %}
{%block preheadcss%}
     <link rel="stylesheet" type="text/css" href="/assets/lib/select2/css/select2.min.css"/>
{%endblock%}
{%block headcss%}
<link rel="stylesheet" href="/css/inv.css" type="text/css"/>
<link rel="stylesheet" href="/assets/lib/date-time/datepicker.css" />
<link href="/assets/lib/switch/css/bootstrap-switch.min.css" rel="stylesheet" />
<link href="/assets/lib/venobox/venobox.css" rel="stylesheet" />

<style>
	

.photo{
	border:1px solid #ccc;
	}
	
#plist{
	list-style-type: none;
	}	
	
.ic-flist{
	
	font-size: 22px;
}	
	
.form-horizontal .form-group {
	padding: 4px 0 !important; 
}
</style>	
{%endblock%}

{%block content%}
<div class="page-head">
	<h3 style="margin-bottom: 2px; margin-top: 2px;">{{lng_estimates.title_new}} </h3>
</div>	

<div class="main-content">
  <div class="row">
	  <div class="col-xs-12 col-md-10 col-md-offset-1">
		
		<input type="hidden" id="btnYes" value="{{lng_common.btn_yes}}">
		<input type="hidden" id="btnNo" value="{{lng_common.btn_no}}">
		<input type="hidden" id="_tit" value="{{lng_estimates.errtit}}">
		<input type="hidden" id="_txt" value="{{lng_estimates.errtxt}}">
		<input type="hidden" id="_ctit" value="{{lng_estimates.cerrtit}}">
		<input type="hidden" id="_ctxt" value="{{lng_estimates.cerrtxt}}">
		<input type="hidden" id="_ltit" value="{{lng_estimates.lerrtit}}">
		<input type="hidden" id="_ltxt" value="{{lng_estimates.lerrtxt}}">
		
		<form id="festimate" class="form-horizontal">			
		<div class="row form-horizontal" style="margin-bottom: 5px;">
			
			<div class="col-xs-12 col-sm-4">
				 <select class="form-control" id="est_lang" name="est_lang" >
					 {%for l in langs%}
					 
					 <option {%if l.lang_iso==cinvlang%}selected{%endif%} value="{{l.lang_iso}}">{{l.lang_name}}</option>
					 {%endfor%}
				</select>
			</div>	
			<div class="col-xs-12 col-sm-4">
				 <select class="form-control" id="est_currency" name="est_currency" >
					 
					 {%for c in currencies%}
					 <option {%if c.currency_iso==acs.user_currency%}selected{%endif%} value="{{c.currency_iso}}"> {{c.currency_iso}} {{c.currency_name}}</option>
					 {%endfor%}
					 </select>
					<div class="input-group xs-mb-15" id="rate-box" style="margin-bottom:0 !important; margin-top: 5px; display: none;" >
					<span class="input-group-addon" id="currency-label">{{acs.user_currency}}</span>
                    <input type="text" id="est_exchange_rate" autocomplete="off" name="est_exchange_rate" class="form-control xr" required="" data-errorbox="#errrate" data-msg-required="{{lng_common.fr}}" value="1">
                </div>
				<div id="errrate" style="color:red;"></div>

			</div>

			<div class="col-xs-12 col-sm-4" align="right">
				 <select class="form-control" id="est_prj" name="est_prj" >
					 <option value="0">Not assigned to Project</option>
					 {%for p in prj%}
					 
					 <option value="{{p.project_id}}">{{p.project_name}}</option>
					 {%endfor%}
					 </select>

			</div>	
		</div>

		<input type="hidden" id="est_name" name="est_name" >
		<input type="hidden" id="est_street" name="est_street" >
		<input type="hidden" id="est_city" name="est_city" >
		<input type="hidden" id="est_state" name="est_state" >
		<input type="hidden" id="est_zip" name="est_zip" >
		<input type="hidden" id="est_country" name="est_country" >
		

			
		<div class="panel panel-default panel-borders">
	        <div class="panel-body">

			<div class="row">
						
				<div class="col-sm-6">
					<div class="form-horizontal">
				    	<div class="form-group">
				    		<div class="col-sm-10" style="padding-right: 4px;">
		
						    	<select id="client" name="client" class="select2">
							    	
						    		<option value="0">{{lng_estimates.selto}}</option>
						    		{%if clients%}
						    		<optgroup label="{{lng_menu.clients}}">
						    		{%for c in clients%}
						    			<option {%if selClient == c.client_id%}selected{%endif%} value="{{c.client_id}}@cli">{{c.client_name}}</option>
						    		{%endfor%}
						    		</optgroup>
						    		{%endif%}
						    		{%if collabs%}
						    		<optgroup label="{{lng_menu.colleague}}">
						    		{%for c in collabs%}
						    			<option {%if selCollab == c.cb_id%}selected{%endif%} value="{{c.cb_id}}@usr">{{c.cb_billing_name}}</option>
						    		{%endfor%}
						    		</optgroup>
						    		{%endif%}
						    		
						    	</select>
					    	
					    	
				    		</div>
				    		<div class="hidden-xs col-sm-1" style="padding-left: 0;">
					    		
					    		<button class="btn btn-sm btn-success" id="a-c" style="padding: 6px 9px;"><i class="material-icons">person_add</i></button>
				    		</div>
					    </div>
					</div>
						
					    <address id="address" style="margin-top:20px; font-size: 16px; display:none;">
					    
					    </address>
				
				</div>
				
				<div class="col-sm-6">
					<div class="form-horizontal">
					
						<div class="form-group">
							<label class="col-sm-6 control-label no-padding-right invlang_estimate" >{{invlang.estimate}}</label>
							<div class="col-sm-6">
								<div class="input-group">
									<span class="input-group-addon">#</span>
									<input type="text" class="form-control inum" maxlength="7" required="" data-errorbox="#errnum" data-msg-required="{{lng_estimates.num_req}}" name="est_num"  value="{{num}}" >
								</div>							
		
								<div id="errnum" style="color:red;"></div>
							</div>
						</div>
		
						<div class="form-group">
							<label class="col-sm-6 control-label no-padding-right invlang_di" >{{invlang.di}}</label>
							
								<div class="col-sm-6" >
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
										<input type="text" data-date-format="{{dpformat}}" readonly="" value="{{today}}" name="date" class="form-control date-picker">
										
									</div>							
								</div>
						</div>
		
				    </div> 
					
			   </div>
			</div>
			
			<div class="row" style="margin-top:40px; margin-bottom: 20px;">		
				<div class="col-xs-12">
				<table class="invoice-details" id="est-lines">
					<tbody>
						<tr>
							<th style="width:60%" class="invlang_desc">{{invlang.desc}}</th>
							<th class="hours invlang_rate" style="width:10%; text-align: center;">{{invlang.rate}}</th>
							<th class="hours invlang_qty" style="width:9%; text-align: center;">{{invlang.qty}}</th>
							<th class="hours invlang_tax" style="width:10%; text-align: center;">{{invlang.tax}} %</th>
							<th class="amount invlang_am" style="width:10%">{{invlang.am}}</th>		
							{#<th style="width:1%"></th>	#}				
						</tr>	
					</tbody>	
				</table>	
				</div>	
			</div>		
		
			<div class="row">
				<div class="col-xs-12 text-center m-t-15">
				<button type="button" data-mob="0" class="btn btn-sm btn-success addline"><i class="ace-icon fa fa-plus"></i> {{lng_estimates.add_line}}</button>
				</div>
			</div>
						        
			<br>     
			<div class="row">
				<div class="col-sm-7"></div>	

				<div class="col-sm-5">
					<table class="table">
					<tr>
						<td width="60%" ><span class="invlang_subtotal">{{invlang.subtotal}}</span> <span class="sel-curr">{{acs.user_currency}}</span></td>
						<td width="40%" align="right" id="subtotal-txt" >0</td>
						<input type="hidden" name="subtotal" id="subtotal" value="0">
						<input type="hidden" name="discount_cash" id="disc-cash" value="0">
					</tr>	
					<tr>
						<td ><span class="invlang_taxes">{{invlang.taxes}}</span> <span class="sel-curr">{{acs.user_currency}}</span></td>
						<td align="right" id="totaltax-txt">0</td>
						<input type="hidden" name="tataltax_cash" id="totaltax" value="0">
					</tr>	
					<tr>
						<td class="invlang_discount">{{invlang.discount}} </td>
						<td align="right" style="padding-right: 0;">
							<div class="input-group input-group-sm">
								<span class="input-group-addon">%</span>
								<input style="text-align: right;" class="form-control pricedis dis" id="discount" value="0.00" name="discount"  type="text" value="0.00">
							</div>
						</td>
					</tr>	


					<tr class="" style="background:#e6f9ff; font-weight: 600;">
						<td style="font-weight: 600;">
							<span class="invlang_total">{{invlang.total}}</span> <span class="sel-curr">{{acs.user_currency}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted">{{lng_estimates.total}} {{acs.user_currency}}</span>
							</div>	
							
						</td>
						<td style="font-weight: 600;" align="right" >
							<span id="total-txt">0.00</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted" id="total-txt-base">0.00</span>
							</div>	
	
						</td>
						<input type="hidden" name="total" id="total" value="0">
						<input type="hidden" name="total_base" id="total_base" value="0">
					</tr>	


					<tr class="" style="background: #ffeaea;">
						<td style="font-weight: 600;">
							<span class="invlang_due">{{invlang.due}}</span> <span class="sel-curr">{{acs.user_currency}}</span>
							<div class="base-currency-totals" style="display: none;">
								<span class="text-muted">{{lng_estimates.due}} {{acs.user_currency}}</span>
							</div>	
						
						</td>
						<td style="font-weight: 600;" align="right"  style="font-weight: 600;">
							<span id="due-txt">0.00</span>
							<div class="base-currency-totals" style="display: none;">
								<span id="due-txt-base" class="text-muted" id="">0.00</span>
							</div>	
						
						</td>
						<input type="hidden" name="due" id="due" value="0">
						<input type="hidden" name="due_base" id="due_base" value="0">
					</tr>	

					</table>	
				</div>	

			</div>	
			
			<div class="row">
				<div class="col-sm-6">
				<label class="invlang_terms">{{invlang.terms}}</label>
				<textarea class="form-control" style="resize: none;" name="est_terms">{{terms}}</textarea>	
				</div>	
				<div class="col-sm-6">
				<label class="invlang_notes">{{invlang.notes}}</label>
				<textarea class="form-control" style="resize: none;" name="est_notes">{{notes}}</textarea>	

				</div>	
			</div>	
			
			  
	        </div>
		</div> 

		<div class="panel panel-default panel-borders">

			<div class="panel-heading">{{lng_estimates.attf}}</div>
	        <div class="panel-body">
		        
			<ul id="plist"></ul>				
		        
			<div class="progress  progress-striped active" id="progress1" style="display:none;">
				<div id="progressSlide" style="width: 0%;" class="progress-bar progress-bar-success">{{lng_estimates.loader}}</div>
			</div>
				
			<div id="containerpic" class="text-center">
				
				<button id="pickfiles" class="btn btn-md btn-success"  ><i class="fa fa-cloud-upload" aria-hidden="true"></i>&nbsp; {{lng_estimates.attach}}</button>
			
			</div>
			        
		        
	    </div>
		</div>

   
		</form>    
		<button type="button" data-act="draft" class="btn btn-alt4 btn-lg pull-right saveEst" >{{lng_common.btn_draft}}</button>
		<button type="button" data-act="send" class="btn btn-primary btn-lg pull-right saveEst" style="margin-right: 5px;">{{lng_common.btn_send}}</button>
				<a href="#" class="btn btn-default btn-lg pull-left g-l" data-go="/estimates">{{lng_common.btn_cancel}}</a>
		<div style="clear:both;margin-bottom: 40px;"></div>	
		
	   
	  </div>	  
  </div>	         
</div>

     <div id="share-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_estimates.new_customer}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12">

				  <form id="fnewcustomerpopup" class="form-horizontal" role="form">
						
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">{{lng_clients.tbl_name}}<span style="color: red;">*</span></label>
	                  <div class="col-sm-9">
	                    <input class="form-control" name="client_name" data-errorbox="#errname" data-msg-required="{{lng_clients.name_req}}"  required="" placeholder="{{lng_projects.ph_name}}" type="text">
	                    <div id="errname" style="color:red;"></div>
	                  </div>
	                </div>
	
					
						<input type="hidden" value="1" name="status">
						<input type="hidden"  name="discount" id="discount" value="0"> 
						<input type="hidden"  name="notes" value="">

					<div class="form-group" >
					<label class="col-sm-3 control-label no-padding-right ">Email</label>
					<div class="col-sm-6">
						<input type="email"  name="client_email" data-errorbox="#errname-email" data-msg-email="{{lng_common.nve}}" class="form-control"> 									<div id="errname-email" style="color:red;"></div>
					</div>
					</div>
						

					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.country}}</label>
					 <div class="col-sm-5">
						<select id="country" class="form-control" name="client_country" >
							{%for cl in countryList%}
							<option {%if cl.country_iso==acs.user_country%}selected{%endif%} value="{{cl.country_iso}}">{{cl.country_printable_name}}</option>
							{%endfor%}
						</select>
					</div>
					</div>
					
					
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.state}}</label>
					<div class="col-sm-5">
					 {%if  statesList%}
						<select title="State"  id="region"  name="client_state" class="form-control">
							{%for sl in statesList%}
							<option {%if sl.state_code == acs.co_state%}selected{%endif%} value="{{sl.state_code}}">{{sl.state_name}}</option>
							{%endfor%}
						</select>
						<input type="text" style="display:none;" id="inputstate" title="State" class="form-control"  name=""  />
					{%else%}
						<input type="text" id="inputstate" title="State" class="form-control"  name="client_state"  value="{{acs.co_state}}">
						<select title="State"  style="display:none;"  id="region"  name="client_state" class="form-control">
						</select>
					{%endif%}	
					</div>
					</div>
				
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">{{lng_common.address}} </label>
					    <div class="col-sm-9">
						    <input type="text" name="client_street"  class="form-control" >
						</div>
					</div>

					
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.city}} </label>
					<div class="col-sm-5">
	
						<input type="text" name="client_city"   class="form-control" >
					</div>
					</div>
						
					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">{{lng_common.zip}} </label>
					<div class="col-sm-3">
						<input type="text"  class="form-control" name="client_zip"  > 						
					</div>
					</div>

					<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right ">{{lng_common.phone}}</label>
					<div class="col-sm-4">
	
						<input type="text"  name="client_phone" class="form-control " > 
					</div>
					</div>

						
		
		</form>


		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" id="send-invite"   class="btn btn-primary doAddClient btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  

     <div id="addr-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_estimates.chaddr}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12" id="bill-adr-box">



		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="button"   class="btn btn-primary doChAddr btn-md ">{{lng_common.btn_save}}</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>  
	 {%for r in rates%}
	 <input type="hidden" id="rate{{r.curr}}" value="{{r.val}}">
	 {%endfor%}


  <div id="send-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">Email Invoice</h3>
          </div>
           
         <div class="modal-body">
	         
				<form id="femail" class="form-horizontal">
			 		
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">Email </label>
				    <div class="col-sm-8">
					    <input type="email" name="send_email" value="{{sendemail}}" required=""  class="form-control" data-errorbox="#erremail" data-msg-required="{{lng_estimates.email_req}}" data-msg-email="{{lng_common.nve}}" id="send-email" >
					    <div id="erremail" style="color:red;"></div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">CC:Email  </label>
				    <div class="col-sm-8">
					    <input type="email" name="send_email_cc" id="cc-send-email" value="{{sendemailcc}}" class="form-control" data-errorbox="#erremailcc"  data-msg-email="{{lng_common.nve}}">
					    <div id="erremailcc" style="color:red;"></div>
					</div>
				</div>
		 		</form>	
	               
          </div>
         
          <div class="modal-footer" >
            
            <button type="button" id="doSend"   class="btn btn-primary  btn-md ">Send</button>
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>
          </div>
         
         
         
        </div>
      </div>
     </div> 
{%endblock%}


{%block js%}

<script src="/assets/lib/date-time/bootstrap-datepicker.min.js"></script>	
<script src="/assets/lib/livequery/jquery.livequery.js"></script>
<script src="/assets/lib/typeahead/bootstrap3-typeahead.min.js?v={{hashver}}"></script>
<script src="/assets/lib/mask/jquery.mask.js"></script>	
<script src="/assets/lib/switch/js/bootstrap-switch.min.js"></script>
{# <script src="/assets/lib/jquery.magnific-popup/jquery.magnific-popup.min.js" type="text/javascript"></script>#}
{# <script src="/assets/js/app-page-gallery.js" type="text/javascript"></script>#}
<script src="/assets/lib/plupload-2.3.1/js/plupload.full.min.js"></script>	
    <script src="/assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="/assets/lib/venobox/venobox.min.js"></script>	
<script src="/js/dev/estimates_add.js?v=23{{hashver}}"></script>
 
{%endblock%}
