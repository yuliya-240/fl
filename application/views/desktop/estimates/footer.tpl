<footer class="hidden-xs">
    <div class="am-checkbox m-l-18 m-b-5 pull-left" style="margin-top:-2px; padding: 0; margin-right: 5px;">
      <input type="checkbox" id="checkall">
      <label for="checkall"></label>
    </div>
	<span >            		
	<a href="#" id="bundleDelete" class="text-danger hidden-xs" style="margin-right: 5px;" >{{lng_common.btn_del}}</a>&nbsp;|&nbsp;
	</span>	
	<span >            		
	<a href="#" id="bundleSend" class="text-primary hidden-xs" style="margin-right: 5px;" >{{lng_common.btn_send}}</a>&nbsp;|&nbsp;
	</span>	
	<span >            		
	<a href="#" id="bundleArc" class=" hidden-xs" style="margin-right: 5px; color: #000;" >{{lng_common.btn_arc}}</a>&nbsp;|&nbsp;
	</span>	
	<span >            		
	<a href="#" id="bundleUnarc" class=" hidden-xs" style="margin-right: 5px; color: #000;" >{{lng_common.btn_unarc}}</a>&nbsp;|&nbsp;
	</span>	

	{%if pagination.previous%} 
	<a href="#" data-page="{{pagination.previous}}" class=" prevp" ><i class="fa fa-arrow-left"></i></a>
	{%endif%}
	<span >{{pagination.info}}</span>
	{%if pagination.next%}
	<a href="#" data-page="{{pagination.next}}" class="nextp" ><i class="fa fa-arrow-right"></i></a>
	{%endif%}

</footer>


