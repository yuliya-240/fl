<div role="alert" class="alert alert-danger alert-icon alert-border-color alert-dismissible">
    <div class="icon"><span class="s7-close-circle"></span></div>
    <div class="message">
      Some estimates haven't been sent due the errors. Please check, fix them and try to send not sent estimates again.
    </div>
</div>
<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>Recipient</th>
			<th>Error</th>
			<th></th>
		</tr>	
	</thead>	
	<tbody>
		{%for e in err%}
		<tr>
			<td>{{e.num}}</td>
			<td>{{e.name}}</td>
			<td>{{e.msg}}</td>
			<td align="right"><a target="_blank" href="/estimates/view/{{e.id}}"><i class="material-icons">visibility</i></a></td>
		</tr>
		{%endfor%}
	</tbody>	
	
</table>	