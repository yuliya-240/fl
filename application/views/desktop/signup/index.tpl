<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{lng_signup.title}}</title>
    <link rel="stylesheet" type="text/css" href="/assets/lib/stroke-7/style.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/lib/jquery.nanoscroller/css/nanoscroller.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
 
    <link rel="stylesheet" href="/assets/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/css/themes/theme-google.min.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/lib/sweetalert/sweetalert.min.css" type="text/css"/>
    <link rel="stylesheet" href="/css/app.css" type="text/css"/>
	<link rel="stylesheet" href="/css/login.css" type="text/css"/>
	{#<script src='https://www.google.com/recaptcha/api.js'></script>#}
  </head>
  <body class="am-splash-screen">

	<div id="process-loader" class="fade">
	    <div class="material-loader">
	        <svg class="circular" viewBox="25 25 50 50">
	            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
	        </svg>
	        <div class="message">{{lng_common.loader}}</div>
	    </div>
	</div>


    <div class="am-wrapper am-login am-signup">
      <div class="am-content">
        <div class="main-content">
          <div class="login-container sign-up">
            <div class="panel panel-default">
              <div class="panel-heading"><img src="/logo/login_signup_black.png" alt="logo"  class="logo-img"><span>{{lng_signup.desc}}</span></div>
              <div class="panel-body">
                <form   id="regForm" class="form-horizontal">
	                
                
                  <div class="sign-up-form"> 


                    <div class="form-group">
                      <div id="email-handler" class="input-group"><span class="input-group-addon"><i class="icon s7-mail"></i></span>
                        <input type="email" name="email" required="" value="{{email}}" placeholder="E-mail" autocomplete="off" data-msg-required="{{lng_signup.email_req}}" data-msg-email="{{lng_signup.not_email}}" class="form-control">
                      </div>
                    </div>
                    <div id="error-box" style="margin-top:5px; margin-bottom: 30px;"></div>
                    {#
                    <div class="form-group">
                      
                        <div id="password-handler" class="input-group"><span class="input-group-addon"><i class="icon s7-lock"></i></span>
                          <input id="password" type="password" data-parsley-errors-messages-disabled="true" placeholder="Password" data-parsley-class-handler="#password-handler" required="" name="password" class="form-control">
                        </div>
                   
                    </div>  
                    <div class="form-group">
                      
                        <div id="confirm-handler" class="input-group"><span class="input-group-addon"><i class="icon s7-lock"></i></span>
                          <input data-parsley-equalto="#password" type="password" data-parsley-errors-messages-disabled="true" data-parsley-class-handler="#confirm-handler" required="" placeholder="Confirm" class="form-control">
                        </div>
                      
                    </div>
                    #}
                    {# <div class="form-group text-center" >
                    <div class="g-recaptcha" data-sitekey="6LfbYRwUAAAAALn7J4GllbS-Rt-W2qRwI77zVRjN" ></div>
                     </div>#}
                  </div>
                  <p class="conditions">{{lng_signup.agree}} <a href="#"> {{lng_signup.terms}}</a>.</p>
                  <button type="submit" class="btn btn-block btn-primary btn-lg">{{lng_signup.btn_signup}}</button>
                </form>
              </div>
            </div>
            <div class="text-center out-links"><a href="/login">{{lng_signup.lnk_login}}</a></div>
          </div>
        </div>
      </div>
    </div>
    <script src="/assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js" type="text/javascript"></script>
    <script src="/assets/js/main.js" type="text/javascript"></script>
    <script src="/assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/assets/lib/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>
	<script src="/assets/lib/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<script src="/js/dev/signup.js" type="text/javascript"></script>

  
  </body>
</html>