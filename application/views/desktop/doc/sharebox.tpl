
<select multiple="multiple" class="tag-input-style form-control" name="collabs[]" id="emplist"  data-placeholder="{{ lng_doc.ch_col }}">
	{%for e in available_collabs%}
		<option {%if e.user_id in selCollab%}selected{%endif%} value="{{e.user_id}}">{{e.user_username}}</option>
	{%endfor%}
</select>								
