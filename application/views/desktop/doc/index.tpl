{% extends skin~"/common/root.tpl" %}

{% block headcss %}
<link href="/assets/lib/multiselect/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
{% endblock %}

{% block content %}
<input type="hidden" id="_tnf" value="{{tnf}}">
<input type="hidden" id="_txtnf" value="{{txtnf}}">
<input type="hidden" id="_pdt" value="{{lng_doc.del_bdoc_title}}">
<input type="hidden" id="_pdm" value="{{lng_doc.del_bdoc_message}}">
<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">

<input type="hidden" id="_fc" value="{{lng_doc.btn_fc}}">
<input type="hidden" id="_cancel" value="{{lng_common.btn_cancel}}">
<input type="hidden" id="cfolder" value="0">
<input type="hidden" id="cshmode" value="1">

	<input type="hidden" id="_tcb" value="{{lng_doc.tcb}}">

	<input type="hidden" id="_avc" value="{{lng_doc.avc}}">
	<input type="hidden" id="_selc" value="{{lng_doc.selc}}">

    <div class="page-head">
        <h3 style="margin-bottom: 2px; margin-top: 2px;">{{ lng_doc.title }}
            <a href="#" class="btn btn-success btn-head pull-right newdoc"  data-toggle="tooltip"
               data-placement="bottom" data-original-title="{{ lng_doc.btn_add }}">{{ lng_doc.btn_add }}
            </a>
            
			{%if sharemode==1%}
            <a href="#" class="btn btn-info btn-head btn-space pull-right nfldr" data-toggle="tooltip"
               data-placement="bottom" data-original-title="{{ lng_doc.btn_add_folder }}">{{ lng_doc.btn_add_folder }}
	              
            </a>
            
            <a href="#" class="btn btn-warning btn-head btn-space pull-right shareldr" data-toggle="tooltip"
               data-placement="bottom" data-original-title="{{ lng_doc.btn_share_folder }}"
               style="display: none;">{{ lng_doc.btn_share_folder }}
            </a>
            
			{%endif%}
			
			
            <a href="#" class="btn btn-dark btn-head btn-space pull-right folder back" data-id="0" data-toggle="tooltip"
               data-placement="bottom" data-original-title="{{ lng_doc.return }}"
               style="display: none;">{{ lng_doc.return }}
            </a>
        </h3>
        <ol class="breadcrumb" id="f-t">
          
            <li><a href="#" class="folder" data-id="0">{{lng_doc.title}}</a></li>
            <li class="active" id="c-f-name"></li>
          </ol>
    </div>
    <div class="main-content">
        <form id="fsearch" class="form-horizontal group-border-dashed">


            <div class="form-group">
                <div class="col-xs-12 col-sm-8">

                    <div class="input-group">

                        <span class="input-group-addon btn-primary"
                              style="background-color:#ccc; color:#fff; font-size: 20px;"><span
                                    class="s7-search"></span></span>
                        <input class="form-control" id="s" placeholder="{{ lng_doc.search_ph }}"
                               {% if searchstr %}value="{{ searchstr }}"{% endif %} type="text"
                               style="border-width:1px;">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-2 " style="padding-left: 0;">
	             
		                <select class="form-control" id="share-mode">
			                <option style="padding: 10px;" value="1">{{ lng_doc.my_doc }}</option>
			                <option {%if sharemode==2%}selected=""{%endif%} style="padding: 10px;" value="2">{{ lng_doc.for_me }}</option>
		                </select>
	               
                </div>    
            </div>
        </form>

		<form id="flist" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
		<div class="panel panel-default panel-borders">

	        <div class="panel-body">
			
				<div id="d-content">
		
				</div>
	
	        </div>
		</div>
		</form>

    </div>
    
    <div id="share-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_doc.btn_share_folder}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12">
					<form id="fcollab" class="form-horizontal" role="form">
						<input type="hidden" name="docid" id="docid"  value="{{cfolder}}">
						<div class="form-group">
							<div class="col-sm-12" id="sh-colab-box" style="padding-left: 10px; padding-right: 10px;">
								<select multiple="multiple" class="tag-input-style form-control" name="collabs[]" id="emplist"  data-placeholder="Choose a Coloborator...">
									{%for e in available_collabs%}
										<option {%if e.user_id in selCollab%}selected{%endif%} value="{{e.user_id}}">{{e.user_username}}</option>
									{%endfor%}
								</select>								
							</div>
						</div>
					</form>

		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="submit" id="send-invite"   class="btn btn-primary doShare btn-md ">{{lng_doc.btn_share}}</button>
            
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
        </div>
      </div>
     </div>  

{% endblock %}

{%block footer%}
<div id="megafooter">
	
</div>	
{%endblock%}



{% block js %}
	<script src="/assets/lib/multiselect/js/jquery.multi-select.js" ></script>
		<script src="/assets/lib/quicksearch/jquery.quicksearch.js" ></script>
    <script src="/js/dev/docs.js?v={{ hashver }}"></script>
{% endblock %}

