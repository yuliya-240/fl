{%if records%}
<div class="table-responsive">
<form id="flist">
<table class="table  table-hover">
	<thead>
		<tr>
			<th width="1%" class="text-center hidden-xs"></th>
			
			
			<th width="1%"></th>
			<th>{{lng_doc.name}}</th>
			<th>{{lng_doc.owner}}</th>
			<th>{{lng_doc.date_change}}</th>
			<th width="1%"></th>
		</tr>
	</thead>
	<tbody>
		{%for r in records%}
		<tr>
			<td class="text-center hidden-xs" style="vertical-align:middle;">
	            {%if sharemode==1%}
	            <div class="am-checkbox">
	              <input type="checkbox" id="chk{{r.doc_id}}" class="check" name="row[]" value="{{r.doc_id}}">
	              <label for="chk{{r.doc_id}}"></label>
	            </div>
	            {%endif%}
			</td>
			<td>
				{%if r.doc_type==2%}
				 {%if r.doc_shared%}<i class="material-icons">folder_shared</i>{%else%}<i class="material-icons">folder</i>{%endif%}
				{%else%}
				<i class="material-icons">description</i> 
				{%endif%}
			</td>	
		
			<td class="tbl-pointer {%if r.doc_type==1  %}g-l{%else%}folder{%endif%}" data-id="{{r.doc_id}}" {%if r.doc_type==1%}data-go="/doc/edit/{{r.doc_id}}"{%endif%}>{%if r.doc_shared and r.doc_type==1%}<span style="float:left;"><i class="material-icons" style="padding-top: 2px; font-size: 16px; margin-right: 5px;">people</i></span>{%endif%} <span style="float:left; font-weight: 600; padding-top: 1px;">{{r.doc_name}} </span></td>
			<td class="tbl-pointer {%if r.doc_type==1%}g-l{%else%}folder{%endif%}" data-id="{{r.doc_id}}" {%if r.doc_type==1%}data-go="/doc/edit/{{r.doc_id}}"{%endif%}>{{r.user_username}}</td>
			<td class="tbl-pointer {%if r.doc_type==1%}g-l{%else%}folder{%endif%}" data-id="{{r.doc_id}}" {%if r.doc_type==1%}data-go="/doc/edit/{{r.doc_id}}"{%endif%}>{{r.doc_changed|date(acs.user_dformat)}}</td>
			<td class="tbl-pointer" data-id="{{r.doc_id}}">
				{%if r.doc_type==1%}
				<a href="#" class="{%if r.doc_type==1%}g-l{%else%}folder{%endif%}" data-id="{{r.doc_id}}" {%if r.doc_type==1%}data-go="/doc/edit/{{r.doc_id}}"{%endif%} >{%if sharemode==1%}<i class="material-icons">create</i>{%else%}<i class="material-icons">visibility</i>{%endif%}</a>
				{%endif%}
			</td>
		</tr>
		{%endfor%}
	</tbody>
</table>				
</form>
</div>
{%else%}
	<div class="text-center">
		{{lng_common.norecords}}
	</div>	

{%endif%}			
