{% extends skin~"/common/root.tpl" %}

{% block headcss %}
		<link rel="stylesheet" href="/css/growl.css">
		<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
		<link href="/assets/lib/multiselect/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<style>

.cke_chrome{
	border-left: none !important;
	border-right: none !important;
}

::-webkit-scrollbar { 
    display: none; 
}
#area{
  height: calc(100vh - 185px);
  overflow: hidden;
}

.editable-buttons .btn{
			padding-top: 7px;
			padding-bottom: 6px;
	}
	
.red{
	color:red;
	}	
	
.green{
	color:green;
	}	
	
</style> 
	
	
	<script src="https://cdn.ckeditor.com/4.7.1/full-all/ckeditor.js"></script>	
{% endblock %}

{% block content %}
 <div class="page-head" style="padding: 10px 25px 15px;">
        <h3 style="margin-bottom: 2px; margin-top: 2px;"><a href="#" {%if sharemode==1%}id="doc_name"{%endif%} data-type="text" data-pk="{{doc.doc_id}}" data-url="/doc/doUpdateName" data-title="{{ doc.doc_name }}">{{ doc.doc_name }}</a>

            {%if sharemode==1%}<a href="#"  class="btn btn-success brn-lg btn-space pull-right savedoc" data-toggle="tooltip"
               data-placement="bottom" data-original-title="{{ lng_doc.save_doc }}" style="padding-top: 8px; padding-bottom: 8px;" >{{lng_common.btn_save}}</a>{%endif%}
			  
			<div class="btn-group btn-hspace pull-right">
              <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle dropdowndoc" aria-expanded="false">{{lng_doc.btn_act}}&nbsp;<span class="caret"></span></button>
              <ul role="menu" class="dropdown-menu">
                {%if sharemode==1%}<li><a href="#" class="share"><i class="fa fa-users "></i>&nbsp;{{lng_doc.btn_share}}</a></li>{%endif%}
                {%if sharemode==1%} <li><a href="#" class=""><i class="fa fa-envelope"></i>&nbsp;{{lng_doc.btn_send}}</a></li>{%endif%}
                <li><a href="#" class=""><i class="fa fa-print"></i>&nbsp;{{lng_doc.btn_print}}</a></li>
                {%if sharemode==1%} <li><a href="#" style="color:red;" class="del"><i class="fa fa-trash"></i>&nbsp;{{lng_doc.btn_del}}</a></li>{%endif%}
                <li class="divider"></li>
                <li><a href="#" class="close-doc"><i class="fa fa-times"></i>&nbsp;{{lng_doc.btn_close}}</a></li>
              </ul>
            </div>
        </h3>
    </div>

    <div class="main-content " style="padding-top: 0;">
	<input type="hidden" id="_ch" value="0">
	<input type="hidden" id="_idt" value="{{lng_doc.del_doc_title}}">
	<input type="hidden" id="_idm" value="{{lng_doc.del_doc_message}}">
	<input type="hidden" id="_yes" value="{{lng_common.btn_yes}}">
	<input type="hidden" id="_cb" value="{{lng_common.btn_cancel}}">

	<input type="hidden" id="_ps" value="{{lng_doc.ps}}">
	<input type="hidden" id="_achs" value="{{lng_doc.achs}}">
	<input type="hidden" id="_chns" value="{{lng_doc.chns}}">
	<input type="hidden" id="_tcb" value="{{lng_doc.tcb}}">

	<input type="hidden" id="_avc" value="{{lng_doc.avc}}">
	<input type="hidden" id="_selc" value="{{lng_doc.selc}}">
	    	
	    
      <div class="row fill" >
          <div class="col-sm-12" style="padding-left: 0; padding-right: 0;" >
	          
		 <input type="hidden" name="doc_id" id="doc_id" value="{{doc.doc_id}}">

		  	<div id="area"><textarea {%if sharemode==2%}disabled=""{%endif%} class="form-control" id="txt" name="txt">{{doc.doc_text}}</textarea></div>
		  
          </div>
      </div>
    </div>

     <div id="share-box" tabindex="-1" role="dialog" class="modal modal-colored-header fade" >
      <div style="width: 100%;" class="modal-dialog">
        <div class="modal-content">
         <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><i class="icon s7-close"></i></button>
            <h3 class="modal-title">{{lng_doc.share_doc}}</h3>
          </div>
           
         <div class="modal-body"  >
	          <div class="row">
		          <div class="col-xs-12">
					<form id="fcollab" class="form-horizontal" role="form">
						<input type="hidden" name="docid"  value="{{doc.doc_id}}">
						<div class="form-group">
							<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
								<select multiple="multiple" class="tag-input-style form-control" name="collabs[]" id="emplist"  data-placeholder="{{ lng_doc.ch_col }}">
									{%for e in available_collabs%}
										<option {%if e.user_id in selCollab%}selected{%endif%} value="{{e.user_id}}">{{e.user_username}}</option>
									{%endfor%}
								</select>								
							</div>
						</div>
					</form>

		          </div>
	          </div>        
          </div>
         
          <div class="modal-footer" >
            
            <button type="submit" id="send-invite"   class="btn btn-primary doShare btn-md ">{{lng_doc.btn_share}}</button>
            
            <button type="button"  class="btn btn-default btn-md  pull-left " data-dismiss="modal">{{lng_common.btn_cancel}}</button>

          </div>
         
         
         
        </div>
      </div>
     </div>             

{% endblock %}

{%block footer%}
<div id="megafooter">
{% include skin~"/doc/editfooter.tpl" %} 
</div>	
{%endblock%}

{% block js %}
    <script src="/assets/lib/bootstrap-growl/bootstrap-growl.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
	<script src="/assets/lib/quicksearch/jquery.quicksearch.js" ></script>
	<script src="/assets/lib/multiselect/js/jquery.multi-select.js" ></script>	
    <script src="/js/dev/docs_edit.js?v={{ hashver }}"></script>
{% endblock %}

